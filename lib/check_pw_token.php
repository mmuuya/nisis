<?php
//Function for checking that a password reset token is good. Returns false if the token is not found in the database, or if it's there but expired. Returns true otherwise.
//The userid will be returned if $returnUser is specified as TRUE and the token is good.
function checkPwToken($token, $returnUser=FALSE){
    global $db, $ora_dt_format, $php_dt_format;
    
    //length should always be 30 characters, because reset_pw generated 15 bytes and then converted using bin2hex
    if(strlen($token) !== 30){
        error_log("pw reset token not 30 characters: '$token'");
        return FALSE;
    }
    
    $token_query = "SELECT userid, to_char(createdate, :qformat) AS createdate, expired FROM NISIS_ADMIN.PWTOKEN WHERE token = :token";
    
    $parsedsql = oci_parse($db, $token_query);
    oci_bind_by_name($parsedsql, ':qformat', $ora_dt_format);
    oci_bind_by_name($parsedsql, ':token', $token);
    oci_execute($parsedsql);

    //single row is guaranteed, token column is unique in db
    $row = oci_fetch_assoc($parsedsql);

    if ($row === FALSE || $row['EXPIRED'] === 'Y') {
        //kill('Token not found in the database.');
        error_log("pw reset token not found in database: '$token'");
        setBadTokenMsg();
        return FALSE;
    }

    $result_userid = $row['USERID'];

    //keep dates separate from unix dates for debugging purposes
    $result_dt = $row['CREATEDATE'];
    $cur_dt = date($php_dt_format);
    $result_dt_unx = (int) strtotime($result_dt);
    $cur_dt_unx = (int) strtotime($cur_dt);

    $reset_limit = 30 * 60; //tokens can't be older than 30mins
    if (($cur_dt_unx - $result_dt_unx) > $reset_limit) {
        error_log("pw reset token older than 30 minutes: '$token'");
        setBadTokenMsg();
        return FALSE;
    }

    //all good!
    if($returnUser){
        return $result_userid;
    }
    return TRUE;
}

//sets the password reset message so the login page picks it up and displays it
function setBadTokenMsg(){
    $_SESSION['pwResetMsg'] = "Token has expired!";
}
?>