<?php
//Contains functions for setting up and checking a user's permissions to modify and read data fields.

//TABLE must be set
if(!isset($TABLE)) {
    kill("FATAL ERROR: [user_field_perms.php] variable TABLE must be set to use this file");
}

//Get the permission for the user on the column name. Check if it's
//writable by the user first. If it isn't, check if it's readable.
//If it is not readable, it is hidden. Note that in order to hide
//a field the user cannot have access to the readable OR writable
//field.
//
//Returns one of the constants: $WRITABLE, $READABLE, or $HIDDEN
function getPermission($colName) {
    // global $WRITABLE, $READABLE, $HIDDEN, $userFieldPerms, $debug;
    global $WRITABLE, $READABLE, $HIDDEN, $TABLE, $debug;

    setupUserFields();
    $userFieldPerms = $_SESSION['profile']['userFields'][$TABLE];
    
    if (array_key_exists($colName, $userFieldPerms["w"])) {
        $debugStr = "W - $colName";
        $ret = $WRITABLE;
    } else if (array_key_exists($colName, $userFieldPerms["r"])) {
        $debugStr = "R - $colName";
        $ret = $READABLE;
    } else {
        $debugStr = "H - $colName";
        $ret = $HIDDEN;
    }

    // if($debug) {
    //     error_log($debugStr);
    // }

    return $ret;
}

//Query all of the user's groups for each field he has read or
//write access to. Then, return a processed array which can
//be easily referenced to check whether the user has read or write
//access to that field.
//The returned array is in the form:
//    array(
//        "w" => array(
//                    "ARP_CLASS",
//                    "ATO_SA",
//               ),
//        "r" => array(
//                    "ATO_SA",
//                    "RES_DSG",
//                    "ADDRESS",
//                    "LONGITUDE",
//               )
//    )
//
//    In this example, the user has write permissions on "ARP_CLASS"
//    and "ATO_SA", and he additionally has read "RES_DSG", "ADDRESS",
//    and "LONGITUDE"
function processUserFields(){
    global $db, $TABLE, $HIDDEN, $READABLE, $WRITABLE;

    $query = 'SELECT UNIQUE PERM, COL_NM, FIELD_DISPLAY_NM
                FROM NISIS_ADMIN.GRP_FIELDS_COMP_VIEW V
                JOIN NISIS_ADMIN.USER_GROUP G
                ON V.GROUPID=G.GROUPID
                WHERE G.USERID=:userid
                AND V.TABLE_NM=:tblnm';

    $parsed = oci_parse($db, $query);
    oci_bind_by_name($parsed, ":userid", $_SESSION['userid']);
    oci_bind_by_name($parsed, ":tblnm", $TABLE);

    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR while processing user fields");
    }

    oci_fetch_all($parsed, $results, 0, -1, OCI_ASSOC+OCI_FETCHSTATEMENT_BY_ROW);

    //"w" and "r" match possible values of $field["PERM"]
    $processed = array(
            $READABLE => array(),
            $WRITABLE => array()
        );
    foreach ($results as $field) {
        $processed[$field["PERM"]][$field["COL_NM"]] = $field["FIELD_DISPLAY_NM"];
    }

    return $processed;
}

//Essentially a wrapper script. Sets the user fields in the session if it's not already there.
function setupUserFields() {
    global $TABLE;
    if(isset($_SESSION['profile']['userFields'][$TABLE])) {
        // error_log("INFO, skipping query: user fields already set for $TABLE");
        return;
    }
    // error_log("INFO, querying: user fields not set up for $TABLE");
    $_SESSION['profile']['userFields'][$TABLE] = processUserFields();
}

?>