<?php

/*
 * TEST REMINDER: CHANGE ALL EXITS TO ECHO
 * RUN WITH: phpunit --process-isolation ReportTest.php
 */

class ReportTests extends PHPUnit_Framework_TestCase {
	
	public function testIf() {
		include_once('../settings.php');
		$_SESSION = array("message_fail"=>array("Test1", "Test2","Test3"));
			include_once('../api/report.php');
			unset($_SESSION);		
	}

	public function testElse() {
		include_once('../settings.php');
		$_SESSION = array("messages"=>array("message" => "Test Message", "type" => 1));
		include_once('../api/report.php');
		unset($_SESSION);		
		}	 
}

?>