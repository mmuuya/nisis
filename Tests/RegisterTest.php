<?php

//include ('../Tests/LoginHandlers/Functions.php');
ob_start();


	class RegisterTests extends PHPUnit_Framework_TestCase {
		
		public function testRegisterCheckAlreadyExists() { 

			include_once('../handler.php');
			$_REQUEST = array('fname' => 'John', 'lname' => 'Evans', 'email' => 'john.evans@concept-solutions.com', 'username' => 'John.Evans', 'password' => 'foo');
			include_once('../api/register.php');
			unset($_REQUEST);
		}
		
		public function testCreateNewUser() {
			
			include_once('../handler.php');
			$_REQUEST=array('fname' => 'Anew', 'lname' => 'Hope', 'email' => 'test@concept-solutions.com', 'username' => 'ANew.Hope', 'password' => 'foolish');
			include_once('../api/register.php');
			unset($_REQUEST);	
		}
	}		
?>
	