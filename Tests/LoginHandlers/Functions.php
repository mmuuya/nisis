<?php

// Test function to kill session
function kill($msg) {
	if(is_string($msg)){
		//exit(json_encode(array('return' => $msg)));
		exit(json_decode(json_encode(array('return' => $msg))));												// Uncomment this during unit testing to get clear passing
	}
	if(is_array($msg)){
		if(!isset($msg['return'])){
			$msg['return'] = 'Unknown Error.';
		}
		//exit(json_encode($msg));
		exit(json_decode(json_encode($msg)));																	// Uncomment this during unit testing to get clear passing
	}
}

	function ociexecutefromlogin() {
		
		$dbuser = 'NISIS';
		$dbpass = 'nisis101';
		$dbhost = '192.168.19.212';
		$dbport = '1521';
		$dbname = 'nisisdev';
		$db = oci_pconnect($dbuser, $dbpass, "$dbhost:$dbport/$dbname");
		$username = $_REQUEST['username'];															// This searches the $_REQUEST array for the username key, then assigns the value of that key to $username
		$sql = "SELECT PASSWORD, SALT, FNAME, LNAME, ACTIVE FROM USERS WHERE USERNAME = :uname";	// Set this bit of SQL to a variable. The :uname is an oracle bind variable, allowing the application to reuse execution plan regardless of user name	
		$parsedsql = oci_parse($db, $sql);															// Prepare the oracle statement for running. $db is the existing database connection, while $sql is the statement to be run
		oci_bind_by_name($parsedsql, ':uname', $username);											// Bind the prepared SQL statement to avoid injection, send the prepared statement above, identify what part of that statement is the placeholder, then identify the php variable to set the bind to.		
		
		if(!oci_execute($parsedsql)){																// If the executed query fails, kill the session. If it doesn't then we're good to go!
			print_r('we got here');
			echo('sql attack');
			kill('Malformed Query');
			error_log("Possible SQL Injection Attack!  Submitted Username: '$username'");			// Print the fact there's a possible sql injection attack into the error log, since someone tried to manipulate the oracle query into the database
		}
}

	function getuserdatafromlogin($passedrowbool) {
		
		$row = $passedrowbool;
				
		//Let's get our User Data!
		//$row = oci_fetch_assoc($parsedsql);														// Get the row of data based on the username
		if ($row === FALSE) {																		// If no rows in statement, false is returned; we have no user name
			kill('Unknown Username');
		} else {
			$pass = hash('sha512', $row['SALT'] . $_REQUEST['password']);							// hash the password w/ the name of hash algorithm, grab the salt row in the db and append the password provided in the POST
			//Clean up behind ourselves so no one's secret is leaked
			unset($_REQUEST['password']);															// Destroy the password located in $_REQUEST array
			if (strcmp($row['PASSWORD'], $pass) == 0) {												// Binary compare of the saved hash password in the db with what the user entered
				if($row['ACTIVE']==1){																// Is the user account active? go do these things
					$_SESSION['username'] = $username;												// Set $_SESSION array key of username to the submitted username
					$_SESSION['name'] = $row['FNAME'] . ' ' . $row['LNAME'];						// Set the $_SESSION array of name to the users first and last name in the database
					$loggedin = TRUE;																// Set the boolean logged in to true, which will then kick off the logic in index.php and load the application
				} else {
					exit(json_encode(array('return' => 'Failure', 'newhtml' => 'Your Account is currently inactive.  Please Contact your Supervisor.')));	// Inactive account stuff
				}
			} else {
				kill('Incorrect Password');															// If the binary password compare is not exact, it ain't right, so wrong password dawg.
			}
		}		
	}

	function activesessiontest($passed) {
		
		$row = $passed;
		$pass = $row['PASSWORD'];
		$username = 'John Evans';
		
		if (strcmp($row['PASSWORD'], $pass) == 0) {																														// Binary compare of the saved hash password in the db with what the user entered
				if($row['ACTIVE']==1){																																	// Is the user account active? go do these things
					$_SESSION['username'] = $username;																													// Set $_SESSION array key of username to the submitted username
					$_SESSION['name'] = $row['FNAME'] . ' ' . $row['LNAME'];																							// Set the $_SESSION array of name to the users first and last name in the database
					$loggedin = TRUE;																																	// Set the boolean logged in to true, which will then kick off the logic in index.php and load the application
					return($loggedin);																																	// Added this just to return a boolean after successful run back to the unit test.
				} else {
					//exit(json_encode(array('return' => 'Failure', 'newhtml' => 'Your Account is currently inactive.  Please Contact your Supervisor.')));				// Inactive account stuff
					exit(json_decode(json_encode(array('return' => 'Failure', 'newhtml' => 'Your Account is currently inactive.  Please Contact your Supervisor.'))));	// added decode to stop thrown error
				}
			} else {
				kill('Incorrect Password');															// If the binary password compare is not exact, it ain't right, so wrong password dawg.
		}
	}

	function incorrectpassword($passed) {
		
		$row = $passed;
		$pass = 'testincorrect';
		$username = 'John Evans';
		
		if (strcmp($row['PASSWORD'], $pass) == 0) {																														// Binary compare of the saved hash password in the db with what the user entered
				if($row['ACTIVE']==1){																																	// Is the user account active? go do these things
					$_SESSION['username'] = $username;																													// Set $_SESSION array key of username to the submitted username
					$_SESSION['name'] = $row['FNAME'] . ' ' . $row['LNAME'];																							// Set the $_SESSION array of name to the users first and last name in the database
					$loggedin = TRUE;																																	// Set the boolean logged in to true, which will then kick off the logic in index.php and load the application
					return($loggedin);																																	// Added this just to return a boolean after successful run back to the unit test.
				} else {
					exit(json_encode(array('return' => 'Failure', 'newhtml' => 'Your Account is currently inactive.  Please Contact your Supervisor.')));				// Inactive account stuff
					//exit(json_decode(json_encode(array('return' => 'Failure', 'newhtml' => 'Your Account is currently inactive.  Please Contact your Supervisor.'))));	// added decode to stop thrown error
				}
			} else {
				kill('Incorrect Password');																																// If the binary password compare is not exact, it ain't right, so wrong password dawg.
		}
	}

 	function setcookies($name, $value = "",  $expire = 0,  $path = "", $domain = "", $secure = false, $httponly = false) {
    	return setcookie($name, $value,  $expire, $path, $domain, $secure, $httponly);
	}
	

?>