<?php
    
include '../handler.php';
include '../settings.php';

/* NOTE:
 * TO RUN THESE HANDLER TESTS, ENSURE IN HANDLER.PHP THAT THE EXITS HAVE BEEN REPLACED WITH ECHO OR COMMENT THE ENCODE OUT AND MAKE THE DECODE ENCODE EXIT LINE ACTIVE IN CODE.
 * THIS PREVENTS ERRORS USING THE PROCESS ISOLATION
 * I DON'T REALLY CARE WHICH METHOD YOU DECIDE TO USE IT JUST VERIFIES THE FUNCTIONS ARE HIT AND THE CODE DOESN'T BREAK THINGS
 * YOU CAN ALSO COMMENT THE LINES OUT IF YOU DONT WANT THE ECHO. THE FACT THE TEST PASSES IS ENOUGH!
 * XOXO
 * EVANS
 * 
 *
 */

class APIHandlerTests extends PHPUnit_Framework_TestCase {  
 
		public $reportFunc;
		public $killFunc;
		
		public function testReportEmpty() {
			
			$this->assertEquals(0, sizeof($_SESSION));									// Assert that $_SESSION array is empty			
		}
		
		public function testReportPopulated() {
				
			$testMessage 	= 'Test Message';
			$testType		= 1;				

			$this->reportFunc = report($msg = $testMessage, $type = $testType);			// Pass $msg and $type data to the report function
			$arraySize = count($_SESSION['messages']);									// Get element count of array
			$firstMessage = $_SESSION['messages'][0]['message'];						// Hold the actual message from the array
			$firstType = $_SESSION['messages'][0]['type'];								// Hold the messsage type from the array is unchanged
			$this->assertTrue($firstMessage === $testMessage);							// Assert that the message is unchanged					
			$this->assertTrue($firstType === $testType);								// Assert that type is unchanged				
			unset($reportFunc);															// Clear reportFunc variable
		}
		
		public function testKillOnString() {
		
			$this->killFunc = kill($msg = 'Kill on String Test');						// Test string kill message			
			unset($this->killFunc);														// Clear killFunc		
		}

		public function testKillOnArrayNoReturnKey() {													// Verify is array when Is Set evaluates false
				
			$arrMsgTest = array('unknown', 'errortest');
			$this->killFunc = kill($msg = $arrMsgTest);									// Pass kill an array with no return key.
			unset($this->killFunc);															// Unset killfunc
			unset($this->arrMsgTest);															// Clear array			
		}
		
		public function testArrayReturnKey() {												// Verify is array when Isset evaluates true
				
			$arrMsgTest = array('return' => array('return'=>'Standard Message'));		// Pass kill an array with a return key
			$this->killFunc = kill($msg = $arrMsgTest);									// Execute kill function with passed data
			unset($this->killFunc);															// unset KillFunc
		}	 
}	 	
?>

