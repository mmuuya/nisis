<?php
include ('../Tests/LoginHandlers/Functions.php');

class APIHandlerTests extends PHPUnit_Framework_TestCase {

	public $db;
	public $_REQUEST;
	public $executefunc;
	
	public function testDBConnectFalse() {											// Set DB connection to false and verify the kill
		$db = FALSE;		
		include_once ('../api/login.php');
		unset($db);
		exit;
	}
	
	public function testArrayUser() {												// Set DB connection to true and verify fail on bad user key
		$db = TRUE;
		$_REQUEST = array('notrightkey' => 'User', 'password' => 'Pass');
		include_once ('../api/login.php');
		unset($db);		
	}
	
	public function testArrayPassword() {											// Set DB connection to true and verify fail on bad password key
		$db = TRUE;
		$_REQUEST = array('username' => 'User', 'notrightpass' => 'Pass');
		include_once ('../api/login.php');
		unset($db);	
	}
	
	public function  testOCI() {													// Verify oci connection
		
		$db = TRUE;
		$_REQUEST = array('username' => 'User', 'password' => 'Pass');
		$this->executefunc = ociexecutefromlogin();
		unset($this->executefunc);
	}
	public function testUserData1 () {												// Unit test for row acknowledging not the right user name
		
		$row = FALSE;
		$this->executefunc = getuserdatafromlogin($row);
		unset($this->executefunc);	
	}
	
	public function testUserData2() {												// This test calls user data login and sets the row to true and populates $_REQUEST global
		
		$row = TRUE;
		$_REQUEST = array('username' => 'User', 'password' => 'Pass');
		$this->executefunc = getuserdatafromlogin($row);
		unset($this->executefunc);
	}
	
	public function testActiveSession1() {
		
		$mybool = FALSE;
		
		$row = array('username' => 'User', 'PASSWORD' => 'Pass', 'ACTIVE' => 1, 'FNAME' => 'John', 'LNAME' => 'Evans');
		$_SESSION = array('username' => 'User');
		$mybool = $this->executefunc = activesessiontest($row);
		$this->assertTrue($mybool);
		unset($this->mybool);
		unset($this->executefunc);
	}
		public function testinactiveaccount() {						
		
		$row = array('username' => 'User', 'PASSWORD' => 'Pass', 'ACTIVE' => 0, 'FNAME' => 'John', 'LNAME' => 'Evans');
		$_SESSION = array('username' => 'User');
		$this->executefunc = activesessiontest($row);
		unset($this->executefunc);
	}
		
		public function testIncorrectPassword() {
			
		$row = array('username' => 'User', 'PASSWORD' => 'Pass', 'ACTIVE' => 0, 'FNAME' => 'John', 'LNAME' => 'Evans');
		$_SESSION = array('username' => 'User');
		$this->executefunc = incorrectpassword($row);		
			
	}
	

	
	
































/*		
		//public $db;
		public $_REQUEST = array('username' => 'User', 'password' => 'Pass');
		
		public function testDBConnectFalse() {		
		
		//$db = FALSE;
		//chdir('LoginHandlers');		
		//print_r(getcwd());
		//Make Sure the DB is wired up correctly
		
		chdir('LoginHandlers');
		require_once('DBHandle.php');
		echo get_include_path();
		echo getcwd()			;
			if ($db === FALSE) {																		// handler.php. Look for $db = oci_pconnect($dbuser, $dbpass, "$dbhost:$dbport/$dbname")
				unset($db);
				kill('No Database Connection');															// If this fails, we have no db connection, it evaluates to false, and we kill the session
			}
		}
		
		public function testDBConnectTrue() {
			
			//$db = TRUE;
			
			if ($db === FALSE) {																		// handler.php. Look for $db = oci_pconnect($dbuser, $dbpass, "$dbhost:$dbport/$dbname")
				unset($db);
				kill('No Database Connection');															// If this fails, we have no db connection, it evaluates to false, and we kill the session
			}
			else {
				
				
				
			}			
		}
 */ 
}  