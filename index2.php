<?php
require ('handler.php');

//check for a valid session
$loggedin = isset($_SESSION['username']);

// if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
//     $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
//     //header("Location: $redirect");
// }

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>NAS Integrated Status Insight System</title>
		<link rel="stylesheet" type="text/css" href="styles/base.css"/>
		<script type="text/javascript" src="settings/settings.js"></script>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

		<!-- Font Awesome for the login page -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

		<!--[if lt IE 9]>
		<script src="deps/html5shiv.js"></script>
		<![endif]-->
		<!-- Libraries -->
		<script src="deps/d3.v3.min.js"></script>
		<script type="text/javascript" src="deps/spin.min.js"></script>
		<script type="text/javascript" src="deps/kendoui/js/jquery.min.js"></script>
		<script type="text/javascript" src="deps/kendoui/js/kendo.all.min.js"></script>
		<script type="text/javascript" src="deps/kendoui/js/jszip.min.js"></script>
		<script type="text/javascript" src="deps/esri/library/3.11/jsapi/init.js"></script>
		<script type="text/javascript" src="deps/jquery.parse-0.5.6.js"></script>
		<script type="text/javascript" src="deps/plugins.js"></script>
		<script type="text/javascript" src="scripts/base.js"></script>
		<script type="text/javascript" src="scripts/ui.js"></script>
	</head>
	<body class="soria">
		<div id="content">
			<?php
			if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') === FALSE) AND (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') === FALSE)) {
				include 'html/ie.html';
			} else {
				if ($loggedin) {
					include 'html/app.php';
				} else {
					include 'html/dotmap-login.php';
				}
			}
			?>
		</div>
        <div id="messages"></div>
	</body>
</html>