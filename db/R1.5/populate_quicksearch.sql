/* Formatted on 9/29/2015 10:19:19 AM (QP5 v5.277) */
-- LOG IN AS NISIS_GEODATA

TRUNCATE TABLE nisis_geodata.quicksearch;

SET SERVEROUTPUT ON SIZE UNLIMITED;

DECLARE
   charset_mismatch   EXCEPTION;
   PRAGMA EXCEPTION_INIT (charset_mismatch, -12704);

   stmt               VARCHAR2 (10000);
   qsfields           VARCHAR2 (1000);
   DT varchar2(30);
BEGIN
   FOR r IN (SELECT *
               FROM nisis_admin.datablock_layers_vw
              )
   LOOP
      BEGIN
         stmt :=
            'INSERT INTO nisis_geodata.quicksearch (facid, objectid, layernm, facinfo) SELECT CAST(';
         qsfields := ' ';

         FOR qs
            IN (  SELECT col_nm, quicksearch_item
                    FROM nisis_admin.acl_fields
                   WHERE tableid = r.tableid AND quicksearch_item IS NOT NULL
                ORDER BY quicksearch_item)
         LOOP
            SELECT data_type
              INTO dt
              FROM user_tab_columns
             WHERE table_name = r.table_nm AND column_name = qs.col_nm;

            IF dt IN ('NUMBER', 'INTEGER')
            THEN
               qsfields :=
                     qsfields
                  || 'CASE
                  WHEN NVL('
                  || qs.col_nm
                  || ', 0) = 0 THEN NULL ELSE '
                  || qs.col_nm
                  || '||'', '' END ||';
            ELSE
               qsfields :=
                     qsfields
                  || 'CASE
                  WHEN NVL('
                  || qs.col_nm
                  || ', '' '') = '' '' THEN NULL ELSE '
                  || qs.col_nm
                  || '||'', '' END ||';
            END IF;
         END LOOP;


         stmt :=
               stmt
            || r.table_pk
            || ' AS VARCHAR2(50)) AS table_pk, objectid,'''
            || r.layer
            || ''' AS layernm, RTRIM('''
            || r.fac_type
            || ': ''||'
            || r.table_pk
            || '||'' - ''||'
            || qsfields
            || ','','') AS facinfo FROM nisis_geodata.'
            || r.table_nm
            || CASE
                  WHEN r.layer_where_clause IS NULL THEN ''
                  ELSE ' WHERE ' || r.layer_where_clause
               END;

         stmt :=
            REPLACE (stmt,
                     '||'', '' END ||,'','') AS facinfo',
                     ' END,'', '') AS facinfo');
         DBMS_OUTPUT.put_line ('---------------START--------------------');
         DBMS_OUTPUT.put_line (r.layer);

         DBMS_OUTPUT.put_line (stmt);

         EXECUTE IMMEDIATE stmt;

         DBMS_OUTPUT.put_line ('---------------DONE--------------------');
      EXCEPTION
         WHEN charset_mismatch
         THEN
            stmt := REPLACE (stmt, 'N''', '''');
            DBMS_OUTPUT.put_line ('---------------RETRY--------------------');

            DBMS_OUTPUT.put_line (stmt);

            EXECUTE IMMEDIATE stmt;

            DBMS_OUTPUT.put_line ('---------------DONE--------------------');
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (SQLERRM);
      END;

      COMMIT;
   END LOOP;
END;
/