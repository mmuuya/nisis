--------------------------------------------------------------------------
-- Run this script in NISIS_GEODATA@NISUATTR to make it look like NISIS_GEODATA@NISRSTR1.
--
-- Please review the script before using it to make sure it won't cause any unacceptable data loss.
--
-- NISIS_GEODATA@NISUATTR schema extracted by user NISIS_GEODATA
-- NISIS_GEODATA@NISRSTR1 schema extracted by user NISIS_GEODATA
--------------------------------------------------------------------------
-- "Set define off" turns off substitution variables.
Set define off;


ALTER TABLE NISIS_GEODATA.MASTER_OBJECTS
MODIFY(READINESS_LEVEL_EXPL VARCHAR2(250 BYTE));


ALTER TABLE NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS
MODIFY(READINESS_LEVEL_EXPL VARCHAR2(250 BYTE));


ALTER TABLE NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VW
MODIFY(READINESS_LEVEL_EXPL VARCHAR2(250 BYTE));


ALTER TABLE NISIS_GEODATA.MASTER_OBJECTS_VW
MODIFY(READINESS_LEVEL_EXPL VARCHAR2(250 BYTE));


CREATE TABLE NISIS_GEODATA.QUICKSEARCH
(
  FACID     VARCHAR2(60 BYTE),
  OBJECTID  NUMBER,
  LAYERNM   VARCHAR2(40 BYTE),
  FACINFO   VARCHAR2(255 BYTE)
);

CREATE INDEX NISIS_GEODATA.QUICKSEARCH_IDX1 ON NISIS_GEODATA.QUICKSEARCH
(FACID);



Set define off;

CREATE OR REPLACE PACKAGE NISIS_GEODATA.pkg_obj_hist
AS
   /******************************************************************************
      NAME:       pkg_obj_hist
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        4/28/2015      Kofi Honu       1. Created this package.
   ******************************************************************************/

   PROCEDURE create_triggers;

   FUNCTION get_status_label (in_tabname   IN VARCHAR2,
                              in_colname      VARCHAR2,
                              in_colval    IN NUMBER)
      RETURN VARCHAR2;

   --   PROCEDURE insert_history (in_tbl_nm        IN VARCHAR2,
   --                             in_col_id        IN NUMBER,
   --                             in_fac_id        IN VARCHAR2,
   --                             in_object_id      IN NUMBER,
   --                             in_status        IN NUMBER,
   --                             in_explanation   IN VARCHAR2,
   --                             in_rtg_code      IN VARCHAR2,
   --                             in_im_role       IN VARCHAR2,
   --                             in_username      IN VARCHAR2,
   --                             in_status_date   IN DATE);
   


   PROCEDURE insert_history (in_tbl_nm           IN VARCHAR2,
                             in_col_nm           IN VARCHAR2,
                             in_st_col_nm        IN VARCHAR2,
                             in_fac_id           IN VARCHAR2,
                             in_object_id        IN NUMBER,
                             in_status           IN NUMBER,
                             in_status_display   IN VARCHAR2,
                             in_explanation      IN VARCHAR2,
                             in_rtg_code         IN VARCHAR2,
                             in_im_role          IN VARCHAR2,
                             in_username         IN VARCHAR2,
                             in_status_date      IN DATE);

/*
   PROCEDURE insert_history (in_tbl_nm   IN VARCHAR2,
                             in_col_nm   IN VARCHAR2,
                             in_log_id   IN NUMBER);

   PROCEDURE insert_history (in_tbl_nm    IN VARCHAR2,
                             in_col_nm    IN VARCHAR2,
                             in_col_val   IN NUMBER,
                             in_log_id    IN NUMBER);
                             */
END pkg_obj_hist;
/

SHOW ERRORS;

CREATE OR REPLACE PACKAGE BODY NISIS_GEODATA.pkg_obj_hist
AS
   /******************************************************************************
      NAME:       pkg_obj_hist
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        4/28/2015      Kofi Honu       1. Created this package body
   ******************************************************************************/
   PROCEDURE create_triggers
   IS
      TYPE full_tab_type IS TABLE OF nisis_admin.status_hist_assoc%ROWTYPE;

      TYPE fac_tab_type
         IS TABLE OF nisis_admin.status_hist_assoc.table_nm%TYPE;

      full_tab         full_tab_type;
      fac_tab          fac_tab_type;
      stmt             VARCHAR2 (32000) := ' ';
      colstmt          VARCHAR2 (2000) := ' ';
      table_pk_col     VARCHAR2 (30);
      track_geom_col   VARCHAR2 (1);
      geom_col_nm      VARCHAR2 (30);
   BEGIN
      SELECT DISTINCT table_nm
        BULK COLLECT INTO fac_tab
        FROM nisis_admin.status_hist_assoc;

      FOR tabindx IN 1 .. fac_tab.COUNT
      LOOP
         table_pk_col := NULL;

         SELECT table_pk, track_geom_col, geom_col_nm
           INTO table_pk_col, track_geom_col, geom_col_nm
           FROM nisis_admin.acl_tables
          WHERE table_nm = (fac_tab (tabindx));

         DBMS_OUTPUT.put_line (fac_tab (tabindx));
         stmt := ' ';
         stmt :=
               stmt
            || 'CREATE OR REPLACE TRIGGER '
            || (fac_tab (tabindx))
            || '_bt BEFORE UPDATE ON nisis_geodata.'
            || (fac_tab (tabindx))
            || ' REFERENCING NEW AS new OLD AS old FOR EACH ROW DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);';

         IF track_geom_col = 'Y'
         THEN
            stmt :=
                  stmt
               || 'IF UPDATING AND (   NVL (:new.latitude, 0) <> NVL (:old.latitude, 0)
OR (NVL (:new.longitude, 0) <> NVL (:old.longitude, 0)))
THEN
IF NVL (:new.latitude, 0) = 0 OR NVL (:new.longitude, 0) = 0
THEN
:new.shape := sde.st_geomfromtext (''point empty'', 4326);
ELSE
:new.shape :=
sde.st_pointfromtext (
''point ('' || :new.longitude || '' '' || :new.latitude || '')'',
4326);
END IF;
END IF;
   ';
         END IF;


         -- FETCH THE TABLE'S STATUS COLUMN META INFO
         SELECT *
           BULK COLLECT INTO full_tab
           FROM nisis_admin.status_hist_assoc
          WHERE table_nm = (fac_tab (tabindx)) AND ex_col_nm IS NOT NULL;

         -- LOOP THRU EACH STATUS COLUMN AND ITS ACCOMPANYING EXPLANATION COLUMNS
         FOR colindx IN 1 .. full_tab.COUNT
         LOOP
            colstmt :=
                  'IF NVL (:new.'
               || full_tab (colindx).col_nm
               || ', 0) <> NVL (:old.'
               || full_tab (colindx).col_nm
               || ', 0) OR '
               || ' NVL (:new.'
               || full_tab (colindx).ex_col_nm
               || ', '' '') <> NVL (:old.'
               || full_tab (colindx).ex_col_nm
               || ', '' '')'
               || 'THEN pkg_obj_hist.insert_history (
               in_tbl_nm => '''
               || (fac_tab (tabindx))
               || ''',
               in_col_nm => '''
               || full_tab (colindx).ex_col_nm
               || ''',
               in_st_col_nm => '''
               || full_tab (colindx).col_nm
               || ''',
               in_fac_id => :new.'
               || table_pk_col
               || ',
               in_object_id => :new.objectid,'
               || '
               in_status => :new.'
               || full_tab (colindx).col_nm
               || ',
               in_status_display => pkg_obj_hist.get_status_label('''
               || fac_tab (tabindx)
               || ''','''
               || full_tab (colindx).col_nm
               || ''',:new.'
               || full_tab (colindx).col_nm
               || '),'
               || '
               in_explanation => :new.'
               || full_tab (colindx).ex_col_nm
               || ',
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;';
            stmt := stmt || colstmt;

            DBMS_OUTPUT.put_line (fac_tab (tabindx) || '---1');
         END LOOP;

         ----------------------------------------------------------------------------
         ----------------------------------------------------------------------------

         -- LOOP THRU EACH SUPPLEMENTAL NOTES COLUMN
         FOR colindx IN 1 .. full_tab.COUNT
         LOOP
            colstmt :=
                  'IF NVL (:new.'
               || full_tab (colindx).sn_col_nm
               || ', '' '') <> NVL (:old.'
               || full_tab (colindx).sn_col_nm
               || ', '' '') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => '''
               || (fac_tab (tabindx))
               || ''',
               in_col_nm => '''
               || full_tab (colindx).sn_col_nm
               || ''',
               in_st_col_nm => '''
               || full_tab (colindx).col_nm
               || ''',
               in_fac_id => :new.'
               || table_pk_col
               || ',
               in_object_id => :new.objectid,'
               || '
               in_status => :new.'
               || full_tab (colindx).col_nm
               || ',
               in_status_display => NULL,
               in_explanation => :new.'
               || full_tab (colindx).sn_col_nm
               || ',
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           ';
            stmt := stmt || colstmt;
            DBMS_OUTPUT.put_line (fac_tab (tabindx) || '---2');
         END LOOP;

         stmt :=
               stmt
            || 'EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,''You cannot edit objectid.'');
            WHEN OTHERS THEN RAISE; END '
            || (fac_tab (tabindx))
            || '_bt;
                           ';

         BEGIN
            EXECUTE IMMEDIATE (stmt);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;
   END create_triggers;


   FUNCTION get_status_label (in_tabname   IN VARCHAR2,
                              in_colname      VARCHAR2,
                              in_colval    IN NUMBER)
      RETURN VARCHAR2
   IS
      slabel   VARCHAR2 (100) := NULL;
      stmt     VARCHAR2 (400) := 'SELECT b.label
  FROM nisis_admin.acl_fields A,
       nisis.picklist_items b,
       nisis_admin.acl_tables C
 WHERE     a.tableid = c.tableid
       AND a.picklist = b.picklist
       AND c.table_nm = :in_tabname
       AND a.col_nm = :in_colname
       AND b.VALUE = :in_colval';
   BEGIN
      IF in_colval IS NULL
      THEN
         RETURN NULL;
      ELSE
         EXECUTE IMMEDIATE stmt
            INTO slabel
            USING in_tabname, in_colname, in_colval;

         RETURN slabel;
      END IF;
   END get_status_label;



   PROCEDURE insert_history (in_tbl_nm           IN VARCHAR2,
                             in_col_nm           IN VARCHAR2,
                             in_st_col_nm        IN VARCHAR2,
                             in_fac_id           IN VARCHAR2,
                             in_object_id        IN NUMBER,
                             in_status           IN NUMBER,
                             in_status_display   IN VARCHAR2,
                             in_explanation      IN VARCHAR2,
                             in_rtg_code         IN VARCHAR2,
                             in_im_role          IN VARCHAR2,
                             in_username         IN VARCHAR2,
                             in_status_date      IN DATE)
   IS
   BEGIN
      INSERT INTO nisis_geodata.master_obj_hist (source_table,
                                                 source_column,
                                                 status_column,
                                                 fac_id,
                                                 objectid,
                                                 status,
                                                 status_display,
                                                 explanation,
                                                 userid,
                                                 status_date,
                                                 rtg_code,
                                                 im_role)
           VALUES (in_tbl_nm,
                   in_col_nm,
                   in_st_col_nm,
                   in_fac_id,
                   in_object_id,
                   in_status,
                   in_status_display,
                   in_explanation,
                   in_username,
                   in_status_date,
                   in_rtg_code,
                   in_im_role);
   END insert_history;
/*
   PROCEDURE insert_history (in_tbl_nm   IN VARCHAR2,
                             in_col_nm   IN VARCHAR2,
                             in_log_id   IN NUMBER)
   IS
   BEGIN
      INSERT INTO nisis_geodata.tracked_obj_log (tabname, colname, logid)
           VALUES (in_tbl_nm, in_col_nm, in_log_id);
   END insert_history;

   PROCEDURE insert_history (in_tbl_nm    IN VARCHAR2,
                             in_col_nm    IN VARCHAR2,
                             in_col_val   IN NUMBER,
                             in_log_id    IN NUMBER)
   IS
      colval   VARCHAR2 (100);
   BEGIN
      IF in_col_val IS NOT NULL
      THEN
         colval := get_status_label (in_tbl_nm, in_col_nm, in_col_val);
      END IF;

      INSERT INTO nisis_geodata.tracked_obj_log (tabname,
                                                 colname,
                                                 colvalue,
                                                 logid)
           VALUES (in_tbl_nm,
                   in_col_nm,
                   colval,
                   in_log_id);
   END insert_history;
   */
END pkg_obj_hist;
/

SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_NAT_GAS_COMP_STA_bt BEFORE UPDATE ON nisis_geodata.HSIP_NAT_GAS_COMP_STA REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.STA_STATUS, 0) <> NVL (:old.STA_STATUS, 0) OR  NVL (:new.STA_STS_EX, ' ') <> NVL (:old.STA_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_NAT_GAS_COMP_STA',
               in_col_nm => 'STA_STS_EX',
               in_st_col_nm => 'STA_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.STA_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('HSIP_NAT_GAS_COMP_STA','STA_STATUS',:new.STA_STATUS),
               in_explanation => :new.STA_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.STA_STS_SN, ' ') <> NVL (:old.STA_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_NAT_GAS_COMP_STA',
               in_col_nm => 'STA_STS_SN',
               in_st_col_nm => 'STA_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.STA_STATUS,
               in_status_display => NULL,
               in_explanation => :new.STA_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END HSIP_NAT_GAS_COMP_STA_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_NAT_GAS_LIQ_LN_bit
   BEFORE INSERT
   ON nisis_geodata.HSIP_NAT_GAS_LIQ_LN
   REFERENCING NEW AS new
   FOR EACH ROW
DECLARE
BEGIN
   IF INSERTING AND :new.objectid IS NULL
   THEN
      :new.objectid := nisis_geodata.HSIP_NAT_GAS_LIQ_LN_objid_seq.NEXTVAL;
   END IF;
END;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_NAT_GAS_LIQ_LN_bt BEFORE UPDATE ON nisis_geodata.HSIP_NAT_GAS_LIQ_LN REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.LN_STATUS, 0) <> NVL (:old.LN_STATUS, 0) OR  NVL (:new.LN_STS_EX, ' ') <> NVL (:old.LN_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_NAT_GAS_LIQ_LN',
               in_col_nm => 'LN_STS_EX',
               in_st_col_nm => 'LN_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.LN_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('HSIP_NAT_GAS_LIQ_LN','LN_STATUS',:new.LN_STATUS),
               in_explanation => :new.LN_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.LN_STS_SN, ' ') <> NVL (:old.LN_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_NAT_GAS_LIQ_LN',
               in_col_nm => 'LN_STS_SN',
               in_st_col_nm => 'LN_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.LN_STATUS,
               in_status_display => NULL,
               in_explanation => :new.LN_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END HSIP_NAT_GAS_LIQ_LN_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_OIL_REFINERY_bit
   BEFORE INSERT
   ON nisis_geodata.HSIP_OIL_REFINERY
   REFERENCING NEW AS new
   FOR EACH ROW
DECLARE
BEGIN
   IF INSERTING AND :new.objectid IS NULL
   THEN
      :new.objectid := nisis_geodata.HSIP_OIL_REFINERY_objid_seq.NEXTVAL;
   END IF;
END;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_OIL_REFINERY_bt BEFORE UPDATE ON nisis_geodata.HSIP_OIL_REFINERY REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.STA_STATUS, 0) <> NVL (:old.STA_STATUS, 0) OR  NVL (:new.STA_STS_EX, ' ') <> NVL (:old.STA_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_OIL_REFINERY',
               in_col_nm => 'STA_STS_EX',
               in_st_col_nm => 'STA_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.STA_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('HSIP_OIL_REFINERY','STA_STATUS',:new.STA_STATUS),
               in_explanation => :new.STA_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.STA_STS_SN, ' ') <> NVL (:old.STA_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_OIL_REFINERY',
               in_col_nm => 'STA_STS_SN',
               in_st_col_nm => 'STA_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.STA_STATUS,
               in_status_display => NULL,
               in_explanation => :new.STA_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END HSIP_OIL_REFINERY_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_TERMS_STOR_FAC_bit
   BEFORE INSERT
   ON nisis_geodata.HSIP_TERMS_STOR_FAC
   REFERENCING NEW AS new
   FOR EACH ROW
DECLARE
BEGIN
   IF INSERTING AND :new.objectid IS NULL
   THEN
      :new.objectid := nisis_geodata.HSIP_TERMS_STOR_FAC_objid_seq.NEXTVAL;
   END IF;
END;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.HSIP_TERMS_STOR_FAC_bt BEFORE UPDATE ON nisis_geodata.HSIP_TERMS_STOR_FAC REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.STA_STATUS, 0) <> NVL (:old.STA_STATUS, 0) OR  NVL (:new.STA_STS_EX, ' ') <> NVL (:old.STA_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_TERMS_STOR_FAC',
               in_col_nm => 'STA_STS_EX',
               in_st_col_nm => 'STA_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.STA_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('HSIP_TERMS_STOR_FAC','STA_STATUS',:new.STA_STATUS),
               in_explanation => :new.STA_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.STA_STS_SN, ' ') <> NVL (:old.STA_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'HSIP_TERMS_STOR_FAC',
               in_col_nm => 'STA_STS_SN',
               in_st_col_nm => 'STA_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.STA_STATUS,
               in_status_display => NULL,
               in_explanation => :new.STA_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END HSIP_TERMS_STOR_FAC_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NISIS_AIRPORTS_bt BEFORE UPDATE ON nisis_geodata.NISIS_AIRPORTS REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF UPDATING AND (   NVL (:new.latitude, 0) <> NVL (:old.latitude, 0)
OR (NVL (:new.longitude, 0) <> NVL (:old.longitude, 0)))
THEN
IF NVL (:new.latitude, 0) = 0 OR NVL (:new.longitude, 0) = 0
THEN
:new.shape := sde.st_geomfromtext ('point empty', 4326);
ELSE
:new.shape :=
sde.st_pointfromtext (
'point (' || :new.longitude || ' ' || :new.latitude || ')',
4326);
END IF;
END IF;
   IF NVL (:new.AFA_STATUS, 0) <> NVL (:old.AFA_STATUS, 0) OR  NVL (:new.AFA_STS_EX, ' ') <> NVL (:old.AFA_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'AFA_STS_EX',
               in_st_col_nm => 'AFA_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.AFA_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','AFA_STATUS',:new.AFA_STATUS),
               in_explanation => :new.AFA_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.ANS_STATUS, 0) <> NVL (:old.ANS_STATUS, 0) OR  NVL (:new.ANS_STS_EX, ' ') <> NVL (:old.ANS_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'ANS_STS_EX',
               in_st_col_nm => 'ANS_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.ANS_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','ANS_STATUS',:new.ANS_STATUS),
               in_explanation => :new.ANS_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.AOO_STATUS, 0) <> NVL (:old.AOO_STATUS, 0) OR  NVL (:new.AOO_STS_EX, ' ') <> NVL (:old.AOO_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'AOO_STS_EX',
               in_st_col_nm => 'AOO_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOO_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','AOO_STATUS',:new.AOO_STATUS),
               in_explanation => :new.AOO_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.CRT_RL, 0) <> NVL (:old.CRT_RL, 0) OR  NVL (:new.CRT_RL_EX, ' ') <> NVL (:old.CRT_RL_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'CRT_RL_EX',
               in_st_col_nm => 'CRT_RL',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_RL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','CRT_RL',:new.CRT_RL),
               in_explanation => :new.CRT_RL_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.OOO_STATUS, 0) <> NVL (:old.OOO_STATUS, 0) OR  NVL (:new.OOO_STS_EX, ' ') <> NVL (:old.OOO_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'OOO_STS_EX',
               in_st_col_nm => 'OOO_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.OOO_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','OOO_STATUS',:new.OOO_STATUS),
               in_explanation => :new.OOO_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.OVR_STATUS, 0) <> NVL (:old.OVR_STATUS, 0) OR  NVL (:new.OVR_STS_EX, ' ') <> NVL (:old.OVR_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'OVR_STS_EX',
               in_st_col_nm => 'OVR_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.OVR_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','OVR_STATUS',:new.OVR_STATUS),
               in_explanation => :new.OVR_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.TSA_STATUS, 0) <> NVL (:old.TSA_STATUS, 0) OR  NVL (:new.TSA_STS_EX, ' ') <> NVL (:old.TSA_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'TSA_STS_EX',
               in_st_col_nm => 'TSA_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.TSA_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_AIRPORTS','TSA_STATUS',:new.TSA_STATUS),
               in_explanation => :new.TSA_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.AFA_STS_SN, ' ') <> NVL (:old.AFA_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'AFA_STS_SN',
               in_st_col_nm => 'AFA_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.AFA_STATUS,
               in_status_display => NULL,
               in_explanation => :new.AFA_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.ANS_STS_SN, ' ') <> NVL (:old.ANS_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'ANS_STS_SN',
               in_st_col_nm => 'ANS_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.ANS_STATUS,
               in_status_display => NULL,
               in_explanation => :new.ANS_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.AOO_STS_SN, ' ') <> NVL (:old.AOO_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'AOO_STS_SN',
               in_st_col_nm => 'AOO_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOO_STATUS,
               in_status_display => NULL,
               in_explanation => :new.AOO_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.CRT_RL_SN, ' ') <> NVL (:old.CRT_RL_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'CRT_RL_SN',
               in_st_col_nm => 'CRT_RL',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_RL,
               in_status_display => NULL,
               in_explanation => :new.CRT_RL_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.OOO_STS_SN, ' ') <> NVL (:old.OOO_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'OOO_STS_SN',
               in_st_col_nm => 'OOO_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.OOO_STATUS,
               in_status_display => NULL,
               in_explanation => :new.OOO_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.OVR_STS_SN, ' ') <> NVL (:old.OVR_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'OVR_STS_SN',
               in_st_col_nm => 'OVR_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.OVR_STATUS,
               in_status_display => NULL,
               in_explanation => :new.OVR_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.TSA_STS_SN, ' ') <> NVL (:old.TSA_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_AIRPORTS',
               in_col_nm => 'TSA_STS_SN',
               in_st_col_nm => 'TSA_STATUS',
               in_fac_id => :new.FAA_ID,
               in_object_id => :new.objectid,
               in_status => :new.TSA_STATUS,
               in_status_display => NULL,
               in_explanation => :new.TSA_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NISIS_AIRPORTS_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NISIS_ANS_bt BEFORE UPDATE ON nisis_geodata.NISIS_ANS REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF UPDATING AND (   NVL (:new.latitude, 0) <> NVL (:old.latitude, 0)
OR (NVL (:new.longitude, 0) <> NVL (:old.longitude, 0)))
THEN
IF NVL (:new.latitude, 0) = 0 OR NVL (:new.longitude, 0) = 0
THEN
:new.shape := sde.st_geomfromtext ('point empty', 4326);
ELSE
:new.shape :=
sde.st_pointfromtext (
'point (' || :new.longitude || ' ' || :new.latitude || ')',
4326);
END IF;
END IF;
   IF NVL (:new.ANS_STATUS, 0) <> NVL (:old.ANS_STATUS, 0) OR  NVL (:new.ANS_STS_EX, ' ') <> NVL (:old.ANS_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ANS',
               in_col_nm => 'ANS_STS_EX',
               in_st_col_nm => 'ANS_STATUS',
               in_fac_id => :new.ANS_ID,
               in_object_id => :new.objectid,
               in_status => :new.ANS_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ANS','ANS_STATUS',:new.ANS_STATUS),
               in_explanation => :new.ANS_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.BPWR_STS, 0) <> NVL (:old.BPWR_STS, 0) OR  NVL (:new.BPWR_S_EX, ' ') <> NVL (:old.BPWR_S_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ANS',
               in_col_nm => 'BPWR_S_EX',
               in_st_col_nm => 'BPWR_STS',
               in_fac_id => :new.ANS_ID,
               in_object_id => :new.objectid,
               in_status => :new.BPWR_STS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ANS','BPWR_STS',:new.BPWR_STS),
               in_explanation => :new.BPWR_S_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.ANS_STS_SN, ' ') <> NVL (:old.ANS_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ANS',
               in_col_nm => 'ANS_STS_SN',
               in_st_col_nm => 'ANS_STATUS',
               in_fac_id => :new.ANS_ID,
               in_object_id => :new.objectid,
               in_status => :new.ANS_STATUS,
               in_status_display => NULL,
               in_explanation => :new.ANS_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.BPWR_S_SN, ' ') <> NVL (:old.BPWR_S_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ANS',
               in_col_nm => 'BPWR_S_SN',
               in_st_col_nm => 'BPWR_STS',
               in_fac_id => :new.ANS_ID,
               in_object_id => :new.objectid,
               in_status => :new.BPWR_STS,
               in_status_display => NULL,
               in_explanation => :new.BPWR_S_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NISIS_ANS_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NISIS_ATM_bt BEFORE UPDATE ON nisis_geodata.NISIS_ATM REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF UPDATING AND (   NVL (:new.latitude, 0) <> NVL (:old.latitude, 0)
OR (NVL (:new.longitude, 0) <> NVL (:old.longitude, 0)))
THEN
IF NVL (:new.latitude, 0) = 0 OR NVL (:new.longitude, 0) = 0
THEN
:new.shape := sde.st_geomfromtext ('point empty', 4326);
ELSE
:new.shape :=
sde.st_pointfromtext (
'point (' || :new.longitude || ' ' || :new.latitude || ')',
4326);
END IF;
END IF;
   IF NVL (:new.ATO_PA_STS, 0) <> NVL (:old.ATO_PA_STS, 0) OR  NVL (:new.ATO_PAT, ' ') <> NVL (:old.ATO_PAT, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'ATO_PAT',
               in_st_col_nm => 'ATO_PA_STS',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.ATO_PA_STS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','ATO_PA_STS',:new.ATO_PA_STS),
               in_explanation => :new.ATO_PAT,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.ATO_PSI, 0) <> NVL (:old.ATO_PSI, 0) OR  NVL (:new.ATO_PSI_EX, ' ') <> NVL (:old.ATO_PSI_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'ATO_PSI_EX',
               in_st_col_nm => 'ATO_PSI',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.ATO_PSI,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','ATO_PSI',:new.ATO_PSI),
               in_explanation => :new.ATO_PSI_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.CRT_RL, 0) <> NVL (:old.CRT_RL, 0) OR  NVL (:new.CRT_RL_EX, ' ') <> NVL (:old.CRT_RL_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'CRT_RL_EX',
               in_st_col_nm => 'CRT_RL',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_RL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','CRT_RL',:new.CRT_RL),
               in_explanation => :new.CRT_RL_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.CRT_SL, 0) <> NVL (:old.CRT_SL, 0) OR  NVL (:new.CRT_SL_EX, ' ') <> NVL (:old.CRT_SL_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'CRT_SL_EX',
               in_st_col_nm => 'CRT_SL',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_SL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','CRT_SL',:new.CRT_SL),
               in_explanation => :new.CRT_SL_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.OPS_SL, 0) <> NVL (:old.OPS_SL, 0) OR  NVL (:new.OPS_SL_EX, ' ') <> NVL (:old.OPS_SL_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'OPS_SL_EX',
               in_st_col_nm => 'OPS_SL',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.OPS_SL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','OPS_SL',:new.OPS_SL),
               in_explanation => :new.OPS_SL_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.OPS_STATUS, 0) <> NVL (:old.OPS_STATUS, 0) OR  NVL (:new.OPS_STS_EX, ' ') <> NVL (:old.OPS_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'OPS_STS_EX',
               in_st_col_nm => 'OPS_STATUS',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.OPS_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','OPS_STATUS',:new.OPS_STATUS),
               in_explanation => :new.OPS_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.PWR_STS, 0) <> NVL (:old.PWR_STS, 0) OR  NVL (:new.PWR_STS_EX, ' ') <> NVL (:old.PWR_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'PWR_STS_EX',
               in_st_col_nm => 'PWR_STS',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.PWR_STS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_ATM','PWR_STS',:new.PWR_STS),
               in_explanation => :new.PWR_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.ATO_PAS_EX, ' ') <> NVL (:old.ATO_PAS_EX, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'ATO_PAS_EX',
               in_st_col_nm => 'ATO_PA_STS',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.ATO_PA_STS,
               in_status_display => NULL,
               in_explanation => :new.ATO_PAS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.ATO_PSI_SN, ' ') <> NVL (:old.ATO_PSI_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'ATO_PSI_SN',
               in_st_col_nm => 'ATO_PSI',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.ATO_PSI,
               in_status_display => NULL,
               in_explanation => :new.ATO_PSI_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.CRT_RL_SN, ' ') <> NVL (:old.CRT_RL_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'CRT_RL_SN',
               in_st_col_nm => 'CRT_RL',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_RL,
               in_status_display => NULL,
               in_explanation => :new.CRT_RL_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.CRT_SL_SN, ' ') <> NVL (:old.CRT_SL_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'CRT_SL_SN',
               in_st_col_nm => 'CRT_SL',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_SL,
               in_status_display => NULL,
               in_explanation => :new.CRT_SL_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.OPS_SL_SN, ' ') <> NVL (:old.OPS_SL_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'OPS_SL_SN',
               in_st_col_nm => 'OPS_SL',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.OPS_SL,
               in_status_display => NULL,
               in_explanation => :new.OPS_SL_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.OPS_STS_SN, ' ') <> NVL (:old.OPS_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'OPS_STS_SN',
               in_st_col_nm => 'OPS_STATUS',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.OPS_STATUS,
               in_status_display => NULL,
               in_explanation => :new.OPS_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.PWR_STS_SN, ' ') <> NVL (:old.PWR_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_ATM',
               in_col_nm => 'PWR_STS_SN',
               in_st_col_nm => 'PWR_STS',
               in_fac_id => :new.ATM_ID,
               in_object_id => :new.objectid,
               in_status => :new.PWR_STS,
               in_status_display => NULL,
               in_explanation => :new.PWR_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NISIS_ATM_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NISIS_OSF_bt BEFORE UPDATE ON nisis_geodata.NISIS_OSF REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF UPDATING AND (   NVL (:new.latitude, 0) <> NVL (:old.latitude, 0)
OR (NVL (:new.longitude, 0) <> NVL (:old.longitude, 0)))
THEN
IF NVL (:new.latitude, 0) = 0 OR NVL (:new.longitude, 0) = 0
THEN
:new.shape := sde.st_geomfromtext ('point empty', 4326);
ELSE
:new.shape :=
sde.st_pointfromtext (
'point (' || :new.longitude || ' ' || :new.latitude || ')',
4326);
END IF;
END IF;
   IF NVL (:new.AOPA_4S, 0) <> NVL (:old.AOPA_4S, 0) OR  NVL (:new.AOPA_4S_EX, ' ') <> NVL (:old.AOPA_4S_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'AOPA_4S_EX',
               in_st_col_nm => 'AOPA_4S',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOPA_4S,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','AOPA_4S',:new.AOPA_4S),
               in_explanation => :new.AOPA_4S_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.AOPS_IMP, 0) <> NVL (:old.AOPS_IMP, 0) OR  NVL (:new.AOPS_IP_EX, ' ') <> NVL (:old.AOPS_IP_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'AOPS_IP_EX',
               in_st_col_nm => 'AOPS_IMP',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOPS_IMP,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','AOPS_IMP',:new.AOPS_IMP),
               in_explanation => :new.AOPS_IP_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.AOS_LVL, 0) <> NVL (:old.AOS_LVL, 0) OR  NVL (:new.AOS_L_EX, ' ') <> NVL (:old.AOS_L_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'AOS_L_EX',
               in_st_col_nm => 'AOS_LVL',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOS_LVL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','AOS_LVL',:new.AOS_LVL),
               in_explanation => :new.AOS_L_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.BOPS_STS, 0) <> NVL (:old.BOPS_STS, 0) OR  NVL (:new.BOPS_EX, ' ') <> NVL (:old.BOPS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'BOPS_EX',
               in_st_col_nm => 'BOPS_STS',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.BOPS_STS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','BOPS_STS',:new.BOPS_STS),
               in_explanation => :new.BOPS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.BPWR_STS, 0) <> NVL (:old.BPWR_STS, 0) OR  NVL (:new.BPWR_EX, ' ') <> NVL (:old.BPWR_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'BPWR_EX',
               in_st_col_nm => 'BPWR_STS',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.BPWR_STS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','BPWR_STS',:new.BPWR_STS),
               in_explanation => :new.BPWR_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.CRT_RL, 0) <> NVL (:old.CRT_RL, 0) OR  NVL (:new.CRT_RL_EX, ' ') <> NVL (:old.CRT_RL_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'CRT_RL_EX',
               in_st_col_nm => 'CRT_RL',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_RL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','CRT_RL',:new.CRT_RL),
               in_explanation => :new.CRT_RL_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.CRT_S_LVL, 0) <> NVL (:old.CRT_S_LVL, 0) OR  NVL (:new.CRT_SL_EX, ' ') <> NVL (:old.CRT_SL_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'CRT_SL_EX',
               in_st_col_nm => 'CRT_S_LVL',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_S_LVL,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_OSF','CRT_S_LVL',:new.CRT_S_LVL),
               in_explanation => :new.CRT_SL_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.AOPA_4S_SN, ' ') <> NVL (:old.AOPA_4S_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'AOPA_4S_SN',
               in_st_col_nm => 'AOPA_4S',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOPA_4S,
               in_status_display => NULL,
               in_explanation => :new.AOPA_4S_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.AOPS_IP_SN, ' ') <> NVL (:old.AOPS_IP_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'AOPS_IP_SN',
               in_st_col_nm => 'AOPS_IMP',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOPS_IMP,
               in_status_display => NULL,
               in_explanation => :new.AOPS_IP_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.AOS_L_SN, ' ') <> NVL (:old.AOS_L_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'AOS_L_SN',
               in_st_col_nm => 'AOS_LVL',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.AOS_LVL,
               in_status_display => NULL,
               in_explanation => :new.AOS_L_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.BOPS_SN, ' ') <> NVL (:old.BOPS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'BOPS_SN',
               in_st_col_nm => 'BOPS_STS',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.BOPS_STS,
               in_status_display => NULL,
               in_explanation => :new.BOPS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.BPWR_SN, ' ') <> NVL (:old.BPWR_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'BPWR_SN',
               in_st_col_nm => 'BPWR_STS',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.BPWR_STS,
               in_status_display => NULL,
               in_explanation => :new.BPWR_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.CRT_RL_SN, ' ') <> NVL (:old.CRT_RL_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'CRT_RL_SN',
               in_st_col_nm => 'CRT_RL',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_RL,
               in_status_display => NULL,
               in_explanation => :new.CRT_RL_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           IF NVL (:new.CRT_SL_SN, ' ') <> NVL (:old.CRT_SL_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_OSF',
               in_col_nm => 'CRT_SL_SN',
               in_st_col_nm => 'CRT_S_LVL',
               in_fac_id => :new.OSF_ID,
               in_object_id => :new.objectid,
               in_status => :new.CRT_S_LVL,
               in_status_display => NULL,
               in_explanation => :new.CRT_SL_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NISIS_OSF_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NISIS_RESP_bt BEFORE UPDATE ON nisis_geodata.NISIS_RESP REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF UPDATING AND (   NVL (:new.latitude, 0) <> NVL (:old.latitude, 0)
OR (NVL (:new.longitude, 0) <> NVL (:old.longitude, 0)))
THEN
IF NVL (:new.latitude, 0) = 0 OR NVL (:new.longitude, 0) = 0
THEN
:new.shape := sde.st_geomfromtext ('point empty', 4326);
ELSE
:new.shape :=
sde.st_pointfromtext (
'point (' || :new.longitude || ' ' || :new.latitude || ')',
4326);
END IF;
END IF;
   IF NVL (:new.RES_STS, 0) <> NVL (:old.RES_STS, 0) OR  NVL (:new.RES_STS_EX, ' ') <> NVL (:old.RES_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_RESP',
               in_col_nm => 'RES_STS_EX',
               in_st_col_nm => 'RES_STS',
               in_fac_id => :new.RES_ID,
               in_object_id => :new.objectid,
               in_status => :new.RES_STS,
               in_status_display => pkg_obj_hist.get_status_label('NISIS_RESP','RES_STS',:new.RES_STS),
               in_explanation => :new.RES_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.RES_STS_SN, ' ') <> NVL (:old.RES_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NISIS_RESP',
               in_col_nm => 'RES_STS_SN',
               in_st_col_nm => 'RES_STS',
               in_fac_id => :new.RES_ID,
               in_object_id => :new.objectid,
               in_status => :new.RES_STS,
               in_status_display => NULL,
               in_explanation => :new.RES_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NISIS_RESP_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.npms_all_bit
   BEFORE INSERT
   ON NISIS_GEODATA.NPMS_ALL
   REFERENCING NEW AS new
   FOR EACH ROW
DECLARE
BEGIN
   IF INSERTING AND :new.objectid IS NULL
   THEN
      :new.objectid := nisis_geodata.npms_objid_seq.NEXTVAL;
   END IF;
END;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NPMS_ALL_bt BEFORE UPDATE ON nisis_geodata.NPMS_ALL REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.PPL_STATUS, 0) <> NVL (:old.PPL_STATUS, 0) OR  NVL (:new.PPL_STS_EX, ' ') <> NVL (:old.PPL_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NPMS_ALL',
               in_col_nm => 'PPL_STS_EX',
               in_st_col_nm => 'PPL_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.PPL_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NPMS_ALL','PPL_STATUS',:new.PPL_STATUS),
               in_explanation => :new.PPL_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.PPL_STS_SN, ' ') <> NVL (:old.PPL_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NPMS_ALL',
               in_col_nm => 'PPL_STS_SN',
               in_st_col_nm => 'PPL_STATUS',
               in_fac_id => :new.OGR_FID,
               in_object_id => :new.objectid,
               in_status => :new.PPL_STATUS,
               in_status_display => NULL,
               in_explanation => :new.PPL_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NPMS_ALL_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.ntad_amtrak_bit
   BEFORE INSERT
   ON nisis_geodata.ntad_amtrak
   REFERENCING NEW AS new
   FOR EACH ROW
DECLARE
BEGIN
   IF INSERTING AND :new.objectid IS NULL
   THEN
      :new.objectid := nisis_geodata.ntad_amtrak_objid_seq.NEXTVAL;
   END IF;
END;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_AMTRAK_bt BEFORE UPDATE ON nisis_geodata.NTAD_AMTRAK REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.RR_STATUS, 0) <> NVL (:old.RR_STATUS, 0) OR  NVL (:new.RR_STS_EX, ' ') <> NVL (:old.RR_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_AMTRAK',
               in_col_nm => 'RR_STS_EX',
               in_st_col_nm => 'RR_STATUS',
               in_fac_id => :new.FRAARCID,
               in_object_id => :new.objectid,
               in_status => :new.RR_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_AMTRAK','RR_STATUS',:new.RR_STATUS),
               in_explanation => :new.RR_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.RR_STS_SN, ' ') <> NVL (:old.RR_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_AMTRAK',
               in_col_nm => 'RR_STS_SN',
               in_st_col_nm => 'RR_STATUS',
               in_fac_id => :new.FRAARCID,
               in_object_id => :new.objectid,
               in_status => :new.RR_STATUS,
               in_status_display => NULL,
               in_explanation => :new.RR_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_AMTRAK_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_AMTRK_STA_bt BEFORE UPDATE ON nisis_geodata.NTAD_AMTRK_STA REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.STN_STATUS, 0) <> NVL (:old.STN_STATUS, 0) OR  NVL (:new.STN_STS_EX, ' ') <> NVL (:old.STN_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_AMTRK_STA',
               in_col_nm => 'STN_STS_EX',
               in_st_col_nm => 'STN_STATUS',
               in_fac_id => :new.STNCODE,
               in_object_id => :new.objectid,
               in_status => :new.STN_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_AMTRK_STA','STN_STATUS',:new.STN_STATUS),
               in_explanation => :new.STN_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.STN_STS_SN, ' ') <> NVL (:old.STN_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_AMTRK_STA',
               in_col_nm => 'STN_STS_SN',
               in_st_col_nm => 'STN_STATUS',
               in_fac_id => :new.STNCODE,
               in_object_id => :new.objectid,
               in_status => :new.STN_STATUS,
               in_status_display => NULL,
               in_explanation => :new.STN_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_AMTRK_STA_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_NHPN_bt BEFORE UPDATE ON nisis_geodata.NTAD_NHPN REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.HWY_STATUS, 0) <> NVL (:old.HWY_STATUS, 0) OR  NVL (:new.HWY_STS_EX, ' ') <> NVL (:old.HWY_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_NHPN',
               in_col_nm => 'HWY_STS_EX',
               in_st_col_nm => 'HWY_STATUS',
               in_fac_id => :new.RECID,
               in_object_id => :new.objectid,
               in_status => :new.HWY_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_NHPN','HWY_STATUS',:new.HWY_STATUS),
               in_explanation => :new.HWY_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.HWY_STS_SN, ' ') <> NVL (:old.HWY_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_NHPN',
               in_col_nm => 'HWY_STS_SN',
               in_st_col_nm => 'HWY_STATUS',
               in_fac_id => :new.RECID,
               in_object_id => :new.objectid,
               in_status => :new.HWY_STATUS,
               in_status_display => NULL,
               in_explanation => :new.HWY_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_NHPN_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_PORTS_MAJOR_bt BEFORE UPDATE ON nisis_geodata.NTAD_PORTS_MAJOR REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.PRT_STATUS, 0) <> NVL (:old.PRT_STATUS, 0) OR  NVL (:new.PRT_STS_EX, ' ') <> NVL (:old.PRT_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_PORTS_MAJOR',
               in_col_nm => 'PRT_STS_EX',
               in_st_col_nm => 'PRT_STATUS',
               in_fac_id => :new.PORT,
               in_object_id => :new.objectid,
               in_status => :new.PRT_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_PORTS_MAJOR','PRT_STATUS',:new.PRT_STATUS),
               in_explanation => :new.PRT_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.PRT_STS_SN, ' ') <> NVL (:old.PRT_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_PORTS_MAJOR',
               in_col_nm => 'PRT_STS_SN',
               in_st_col_nm => 'PRT_STATUS',
               in_fac_id => :new.PORT,
               in_object_id => :new.objectid,
               in_status => :new.PRT_STATUS,
               in_status_display => NULL,
               in_explanation => :new.PRT_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_PORTS_MAJOR_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_RAIL_LINES_bt BEFORE UPDATE ON nisis_geodata.NTAD_RAIL_LINES REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.RLL_STATUS, 0) <> NVL (:old.RLL_STATUS, 0) OR  NVL (:new.RLL_STS_EX, ' ') <> NVL (:old.RLL_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_RAIL_LINES',
               in_col_nm => 'RLL_STS_EX',
               in_st_col_nm => 'RLL_STATUS',
               in_fac_id => :new.FRAARCID,
               in_object_id => :new.objectid,
               in_status => :new.RLL_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_RAIL_LINES','RLL_STATUS',:new.RLL_STATUS),
               in_explanation => :new.RLL_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.RLL_STS_SN, ' ') <> NVL (:old.RLL_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_RAIL_LINES',
               in_col_nm => 'RLL_STS_SN',
               in_st_col_nm => 'RLL_STATUS',
               in_fac_id => :new.FRAARCID,
               in_object_id => :new.objectid,
               in_status => :new.RLL_STATUS,
               in_status_display => NULL,
               in_explanation => :new.RLL_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_RAIL_LINES_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_RAIL_NODES_bt BEFORE UPDATE ON nisis_geodata.NTAD_RAIL_NODES REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.RLN_STATUS, 0) <> NVL (:old.RLN_STATUS, 0) OR  NVL (:new.RLN_STS_EX, ' ') <> NVL (:old.RLN_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_RAIL_NODES',
               in_col_nm => 'RLN_STS_EX',
               in_st_col_nm => 'RLN_STATUS',
               in_fac_id => :new.FRANODEID,
               in_object_id => :new.objectid,
               in_status => :new.RLN_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_RAIL_NODES','RLN_STATUS',:new.RLN_STATUS),
               in_explanation => :new.RLN_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.RLN_STS_SN, ' ') <> NVL (:old.RLN_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_RAIL_NODES',
               in_col_nm => 'RLN_STS_SN',
               in_st_col_nm => 'RLN_STATUS',
               in_fac_id => :new.FRANODEID,
               in_object_id => :new.objectid,
               in_status => :new.RLN_STATUS,
               in_status_display => NULL,
               in_explanation => :new.RLN_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_RAIL_NODES_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_TRANSIT_LINK_bt BEFORE UPDATE ON nisis_geodata.NTAD_TRANSIT_LINK REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.TRL_STATUS, 0) <> NVL (:old.TRL_STATUS, 0) OR  NVL (:new.TRL_STS_EX, ' ') <> NVL (:old.TRL_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_TRANSIT_LINK',
               in_col_nm => 'TRL_STS_EX',
               in_st_col_nm => 'TRL_STATUS',
               in_fac_id => :new.RECID,
               in_object_id => :new.objectid,
               in_status => :new.TRL_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_TRANSIT_LINK','TRL_STATUS',:new.TRL_STATUS),
               in_explanation => :new.TRL_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.TRL_STS_SN, ' ') <> NVL (:old.TRL_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_TRANSIT_LINK',
               in_col_nm => 'TRL_STS_SN',
               in_st_col_nm => 'TRL_STATUS',
               in_fac_id => :new.RECID,
               in_object_id => :new.objectid,
               in_status => :new.TRL_STATUS,
               in_status_display => NULL,
               in_explanation => :new.TRL_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_TRANSIT_LINK_bt;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_GEODATA.NTAD_TRANSIT_STA_bt BEFORE UPDATE ON nisis_geodata.NTAD_TRANSIT_STA REFERENCING NEW AS new OLD AS old FOR EACH ROW
DECLARE 
               cannot_edit_objectid EXCEPTION;
            BEGIN 
            IF :NEW.objectid <> :OLD.objectid THEN 
              RAISE cannot_edit_objectid;
            END IF;
            :NEW.last_edited_date := SYS_EXTRACT_UTC(SYSTIMESTAMP);IF NVL (:new.TRS_STATUS, 0) <> NVL (:old.TRS_STATUS, 0) OR  NVL (:new.TRS_STS_EX, ' ') <> NVL (:old.TRS_STS_EX, ' ')THEN pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_TRANSIT_STA',
               in_col_nm => 'TRS_STS_EX',
               in_st_col_nm => 'TRS_STATUS',
               in_fac_id => :new.RECID,
               in_object_id => :new.objectid,
               in_status => :new.TRS_STATUS,
               in_status_display => pkg_obj_hist.get_status_label('NTAD_TRANSIT_STA','TRS_STATUS',:new.TRS_STATUS),
               in_explanation => :new.TRS_STS_EX,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;IF NVL (:new.TRS_STS_SN, ' ') <> NVL (:old.TRS_STS_SN, ' ') THEN  pkg_obj_hist.insert_history (
               in_tbl_nm => 'NTAD_TRANSIT_STA',
               in_col_nm => 'TRS_STS_SN',
               in_st_col_nm => 'TRS_STATUS',
               in_fac_id => :new.RECID,
               in_object_id => :new.objectid,
               in_status => :new.TRS_STATUS,
               in_status_display => NULL,
               in_explanation => :new.TRS_STS_SN,
               in_rtg_code => :new.rtg_code, 
               in_im_role => :new.im_role,
               in_username => :new.last_edited_user, 
               in_status_date => sys_extract_utc(systimestamp));
                   END IF;
                           EXCEPTION WHEN cannot_edit_objectid THEN RAISE_APPLICATION_ERROR(-20001,'You cannot edit objectid.');
            WHEN OTHERS THEN RAISE; END NTAD_TRANSIT_STA_bt;
/
SHOW ERRORS;


GRANT DELETE, INSERT, SELECT, UPDATE ON NISIS_GEODATA.HSIP_NAT_GAS_COMP_STA TO NISIS;

GRANT DELETE, INSERT, SELECT, UPDATE ON NISIS_GEODATA.HSIP_NAT_GAS_LIQ_LN TO NISIS;

GRANT DELETE, INSERT, SELECT, UPDATE ON NISIS_GEODATA.HSIP_OIL_REFINERY TO NISIS;

GRANT DELETE, INSERT, SELECT, UPDATE ON NISIS_GEODATA.HSIP_TERMS_STOR_FAC TO NISIS;


GRANT SELECT ON NISIS_GEODATA.QUICKSEARCH TO NISIS;


ALTER PACKAGE NISIS_GEODATA.PKG_NTAD_AMTRK_STA COMPILE BODY;

ALTER PACKAGE NISIS_GEODATA.PKG_NTAD_RAIL_LINES COMPILE BODY;