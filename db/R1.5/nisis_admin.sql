/* Formatted on 9/30/2015 9:29:55 AM (QP5 v5.277) */
--------------------------------------------------------------------------
-- Run this script in NISIS_ADMIN@NISUATTR to make it look like NISIS_ADMIN@NISRSTR1.
--
-- Please review the script before using it to make sure it won't cause any unacceptable data loss.
--
-- NISIS_ADMIN@NISUATTR schema extracted by user NISIS_ADMIN
-- NISIS_ADMIN@NISRSTR1 schema extracted by user NISIS_ADMIN
--------------------------------------------------------------------------
-- "Set define off" turns off substitution variables.
SET DEFINE OFF;

REVOKE SELECT ON NISIS_ADMIN.GROUPS_OLD FROM NISIS;

DROP TABLE NISIS_ADMIN.GROUPS_OLD CASCADE CONSTRAINTS;

DROP TABLE NISIS_ADMIN.ACL_TABLES_BK CASCADE CONSTRAINTS;

DROP TABLE NISIS_ADMIN.ACL_FIELDS_TEMP CASCADE CONSTRAINTS;

DROP TABLE NISIS_ADMIN.ACL_FIELDS_BK CASCADE CONSTRAINTS;

CREATE TABLE NISIS_ADMIN.ACL_FIELDSX
(
   FIELDID            NUMBER,
   COL_NM             VARCHAR2 (100 BYTE),
   DISPLAY_NM         VARCHAR2 (100 BYTE),
   TABLEID            NUMBER,
   PICKLIST           NUMBER,
   PROFILE_SECTION    NUMBER,
   FROW               NUMBER,
   FCOL               NUMBER,
   DATABLOCK_ITEM     NUMBER,
   PROFILE_ITEM       NUMBER,
   IWL_ITEM           NUMBER,
   QUICKSEARCH_ITEM   NUMBER
);

CREATE TABLE NISIS_ADMIN.ACL_FIELDS_BK_0924
(
   FIELDID            NUMBER,
   COL_NM             VARCHAR2 (100 BYTE),
   DISPLAY_NM         VARCHAR2 (100 BYTE),
   TABLEID            NUMBER,
   PICKLIST           NUMBER,
   PROFILE_SECTION    NUMBER,
   FROW               NUMBER,
   FCOL               NUMBER,
   DATABLOCK_ITEM     NUMBER,
   PROFILE_ITEM       NUMBER,
   IWL_ITEM           NUMBER,
   QUICKSEARCH_ITEM   NUMBER
);

ALTER TABLE NISIS_ADMIN.ACL_TABLES
 ADD (MAIN_STATUS_COL_NM  VARCHAR2(30 BYTE));

ALTER TABLE NISIS_ADMIN.GIS_LAYERS
 ADD (LAYER_WHERE_CLAUSE  VARCHAR2(500 BYTE));

CREATE TABLE NISIS_ADMIN.TEST_IDX
(
   TABLE_PK   VARCHAR2 (60 BYTE),
   OBJECTID   NUMBER,
   LAYERNM    VARCHAR2 (40 BYTE)
);

CREATE OR REPLACE FORCE VIEW NISIS_ADMIN.DATABLOCK_LAYERS_VW
(
   LAYERID,
   LAYER,
   LAYER_LONG_NAME,
   LAYER_WHERE_CLAUSE,
   TABLEID,
   TABLE_NM,
   TABLE_PK,
   FAC_TYPE
)
AS
   SELECT gl.layerid,
          gl.layer,
          gl.layer_long_name,
          gl.layer_where_clause,
          t.tableid,
          t.table_nm,
          t.table_pk,
          t.fac_type
     FROM acl_tables t, gis_layers gl
    WHERE gl.tableid = t.tableid;

CREATE OR REPLACE FORCE VIEW NISIS_ADMIN.QUICKSEARCH_LAYERS_VW
(
   LAYERID,
   LAYER,
   LAYER_LONG_NAME,
   LAYER_WHERE_CLAUSE,
   TABLEID,
   TABLE_NM,
   TABLE_PK
)
AS
   SELECT gl.layerid,
          gl.layer,
          gl.layer_long_name,
          gl.layer_where_clause,
          t.tableid,
          t.table_nm,
          t.table_pk
     FROM acl_tables t, gis_layers gl
    WHERE gl.tableid = t.tableid;

CREATE OR REPLACE TRIGGER NISIS_ADMIN.PROFILE_SECTION_BIT
   BEFORE INSERT
   ON NISIS_ADMIN.PROFILE_SECTIONS
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
BEGIN
   :new.SECTIONID := PROFILE_SECTION_SEQ.NEXTVAL;
END PROFILE_SECTION_BIT;
/

SHOW ERRORS;

GRANT SELECT ON NISIS_ADMIN.DATABLOCK_LAYERS_VW TO NISIS;

GRANT SELECT ON NISIS_ADMIN.QUICKSEARCH_LAYERS_VW TO NISIS;

GRANT SELECT ON NISIS_ADMIN.DATABLOCK_LAYERS_VW TO NISIS_GEODATA;

GRANT SELECT ON NISIS_ADMIN.QUICKSEARCH_LAYERS_VW TO NISIS_GEODATA;

ALTER TABLE NISIS_ADMIN.ACL_FIELDS
 ADD (PROFILE_ITEM  NUMBER);

ALTER TABLE NISIS_ADMIN.ACL_FIELDS
 ADD (IWL_ITEM  NUMBER);

ALTER TABLE NISIS_ADMIN.ACL_FIELDS
 ADD (QUICKSEARCH_ITEM  NUMBER);

ALTER FUNCTION NISIS_ADMIN.MAP_GROUP_TO_R_TABLE COMPILE;

ALTER FUNCTION NISIS_ADMIN.MAP_GROUP_TO_TABLE COMPILE;

CREATE OR REPLACE FORCE VIEW NISIS_ADMIN.DATABLOCK_FIELDS_COMP_VIEW
(
   TABLEID,
   TABLE_NM,
   TABLE_DISPLAY_NM,
   COL_NM,
   FIELD_DISPLAY_NM,
   FIELDID,
   TABLE_PK,
   FROW,
   PICKLIST,
   DATABLOCK_ITEM,
   PERM
)
AS
   SELECT t.tableid,
          t.table_nm,
          t.display_nm AS table_display_nm,
          f.col_nm,
          f.display_nm AS field_display_nm,
          f.fieldid,
          t.table_pk,
          f.frow,
          f.picklist,
          f.datablock_item,
          p.perm
     FROM acl_perms p, acl_fields f, acl_tables t
    WHERE     p.fieldid = f.fieldid
          AND f.tableid = t.tableid
          AND f.datablock_item IS NOT NULL
          AND p.perm = 'r';

CREATE OR REPLACE FORCE VIEW NISIS_ADMIN.IWL_FIELDS_COMP_VIEW
(
   TABLEID,
   TABLE_NM,
   TABLE_DISPLAY_NM,
   COL_NM,
   FIELD_DISPLAY_NM,
   FIELDID,
   TABLE_PK,
   IWL_ITEM,
   PICKLIST
)
AS
   SELECT t.tableid,
          t.table_nm,
          t.display_nm AS table_display_nm,
          f.col_nm,
          f.display_nm AS field_display_nm,
          f.fieldid,
          t.table_pk,
          f.iwl_item,
          f.picklist
     FROM acl_fields f, acl_tables t
    WHERE f.tableid = t.tableid AND f.iwl_item IS NOT NULL;

CREATE OR REPLACE FORCE VIEW NISIS_ADMIN.QUICKSEARCH_FIELDS_VW
(
   TABLEID,
   TABLE_NM,
   TABLE_DISPLAY_NM,
   COL_NM,
   FIELD_DISPLAY_NM,
   FIELDID,
   TABLE_PK,
   FROW,
   PICKLIST,
   LAYER,
   LAYER_LONG_NAME
)
AS
   SELECT t.tableid,
          t.table_nm,
          t.display_nm AS table_display_nm,
          f.col_nm,
          f.display_nm AS field_display_nm,
          f.fieldid,
          --  g.groupid,
          t.table_pk,
          f.frow,
          f.picklist,
          gl.layer,
          gl.layer_long_name
     FROM acl_fields f, acl_tables t, gis_layers gl
    WHERE     f.tableid = t.tableid
          AND f.datablock_item = 1
          AND gl.tableid = t.tableid;

CREATE OR REPLACE TRIGGER NISIS_ADMIN.ACL_FIELDS_TRG
   BEFORE INSERT
   ON NISIS_ADMIN.ACL_FIELDS
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   :new.fieldid := acl_fields_seq.NEXTVAL;

   INSERT INTO nisis_admin.acl_perms (aclid, fieldid, perm)
        VALUES (acl_perms_seq.NEXTVAL, :new.fieldid, 'r');

   IF :new.col_nm NOT IN ('OBJECTID',
                          'LAST_EDITED_USER',
                          'LAST_EDITED_DATE',
                          'CREATED_USER',
                          'CREATED_DATE',
                          'IM_ROLE',
                          'RTG_CODE')
   THEN
      INSERT INTO nisis_admin.acl_perms (aclid, fieldid, perm)
           VALUES (acl_perms_seq.NEXTVAL, :new.fieldid, 'w');
   END IF;
END acl_fields_trg;
/

SHOW ERRORS;

CREATE OR REPLACE TRIGGER NISIS_ADMIN.STATUS_HIST_ASSOC_BT
   BEFORE INSERT OR UPDATE
   ON NISIS_ADMIN.STATUS_HIST_ASSOC
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
BEGIN
   IF     (INSERTING OR UPDATING)
      AND :new.table_nm IS NOT NULL
      AND :new.col_nm IS NOT NULL
   THEN
      SELECT f.fieldid, f.tableid
        INTO :new.fieldid, :new.tableid
        FROM nisis_admin.acl_fields f, nisis_admin.acl_tables t
       WHERE     f.tableid = t.tableid
             AND t.table_nm = :new.table_nm
             AND f.col_nm = :new.col_nm;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END;
/

SHOW ERRORS;

GRANT SELECT ON NISIS_ADMIN.IWL_FIELDS_COMP_VIEW TO NISIS;

GRANT SELECT ON NISIS_ADMIN.QUICKSEARCH_FIELDS_VW TO NISIS;

GRANT SELECT ON NISIS_ADMIN.IWL_FIELDS_COMP_VIEW TO NISIS_GEODATA;

GRANT SELECT ON NISIS_ADMIN.QUICKSEARCH_FIELDS_VW TO NISIS_GEODATA;


DELETE FROM NISIS_ADMIN.acl_perms
      WHERE fieldid NOT IN (SELECT fieldid
                              FROM NISIS_ADMIN.acl_fields);

COMMIT;

ALTER TABLE NISIS_ADMIN.ACL_PERMS
   ADD CONSTRAINT ACL_PERMS_R01 FOREIGN KEY (FIELDID)
           REFERENCES NISIS_ADMIN.ACL_FIELDS (FIELDID);