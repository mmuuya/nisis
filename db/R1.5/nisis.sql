--------------------------------------------------------------------------
-- Run this script in NISIS@NISUATTR to make it look like NISIS@NISRSTR1.
--
-- Please review the script before using it to make sure it won't cause any unacceptable data loss.
--
-- NISIS@NISUATTR schema extracted by user NISIS
-- NISIS@NISRSTR1 schema extracted by user NISIS
--------------------------------------------------------------------------
-- "Set define off" turns off substitution variables.
Set define off;

DROP FUNCTION NISIS.ITEM_STATUS_TYPE;


CREATE OR REPLACE TYPE NISIS.picklist_item_typ AS OBJECT
(
   picklistid NUMBER,
   itemvalue NUMBER,
   itemlabel VARCHAR2 (200),
   imgid NUMBER,
   display_order NUMBER
);
/

SHOW ERRORS;


ALTER TABLE NISIS.LOGGER
MODIFY(MSG VARCHAR2(4000 BYTE));



CREATE OR REPLACE PACKAGE BODY NISIS.json_util_pkg
AS
   /*

   Purpose:    JSON utilities for PL/SQL

   Remarks:

   Who     Date        Description
   ------  ----------  -------------------------------------
   MBR     30.01.2010  Created

   */


   g_json_null_object   CONSTANT VARCHAR2 (20) := '{ }';

   FUNCTION get_xml_to_json_stylesheet
      RETURN VARCHAR2
   AS
   BEGIN
      /*

      Purpose:    return XSLT stylesheet for XML to JSON transformation

      Remarks:    see http://code.google.com/p/xml2json-xslt/

      Who     Date        Description
      ------  ----------  -------------------------------------
      MBR     30.01.2010  Created
      MBR     30.01.2010  Added fix for nulls

      */


      RETURN '<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
  Copyright (c) 2006, Doeke Zanstra
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:

  Redistributions of source code must retain the above copyright notice, this 
  list of conditions and the following disclaimer. Redistributions in binary 
  form must reproduce the above copyright notice, this list of conditions and the 
  following disclaimer in the documentation and/or other materials provided with 
  the distribution.

  Neither the name of the dzLib nor the names of its contributors may be used to 
  endorse or promote products derived from this software without specific prior 
  written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
  THE POSSIBILITY OF SUCH DAMAGE.
-->

  <xsl:output indent="no" omit-xml-declaration="yes" method="text" encoding="UTF-8" media-type="text/x-json"/>
  <xsl:strip-space elements="*"/>
  <!--contant-->
  <xsl:variable name="d">0123456789</xsl:variable>

  <!-- ignore document text -->
  <xsl:template match="text()[preceding-sibling::node() or following-sibling::node()]"/>

  <!-- string -->
  <xsl:template match="text()">
    <xsl:call-template name="escape-string">
      <xsl:with-param name="s" select="."/>
    </xsl:call-template>
  </xsl:template>
  
  <!-- Main template for escaping strings; used by above template and for object-properties 
       Responsibilities: placed quotes around string, and chain up to next filter, escape-bs-string -->
  <xsl:template name="escape-string">
    <xsl:param name="s"/>
    <xsl:text>"</xsl:text>
    <xsl:call-template name="escape-bs-string">
      <xsl:with-param name="s" select="$s"/>
    </xsl:call-template>
    <xsl:text>"</xsl:text>
  </xsl:template>
  
  
  <!-- Escape the backslash (\) before everything else. -->
  <xsl:template name="escape-bs-string">
    <xsl:param name="s"/>
    <xsl:choose>
      <xsl:when test="contains($s,''\'')">
        <xsl:call-template name="escape-amp-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''\''),''\\'')"/>
        </xsl:call-template>
        <xsl:call-template name="escape-bs-string">
          <xsl:with-param name="s" select="substring-after($s,''\'')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="escape-amp-string">
          <xsl:with-param name="s" select="$s"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!-- Escape the ampersand (&). -->
  <xsl:template name="escape-amp-string">
    <xsl:param name="s"/>
    <xsl:choose>
      <xsl:when test="contains($s,''&amp;'')">
        <xsl:call-template name="escape-quot-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&amp;''),''&amp;amp;'')"/>
        </xsl:call-template>
        <xsl:call-template name="escape-amp-string">
          <xsl:with-param name="s" select="substring-after($s,''&amp;'')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="escape-quot-string">
          <xsl:with-param name="s" select="$s"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!-- Escape the double quote ("). -->
  <xsl:template name="escape-quot-string">
    <xsl:param name="s"/>
    <xsl:choose>
      <xsl:when test="contains($s,''&quot;'')">
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&quot;''),''\&quot;'')"/>
        </xsl:call-template>
        <xsl:call-template name="escape-quot-string">
          <xsl:with-param name="s" select="substring-after($s,''&quot;'')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="$s"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  <!-- Replace tab, line feed and/or carriage return by its matching escape code. Can''t escape backslash
       or double quote here, because they don''t replace characters (&#x0; becomes \t), but they prefix 
       characters (\ becomes \\). Besides, backslash should be seperate anyway, because it should be 
       processed first. This function can''t do that. -->
  <xsl:template name="encode-string">
    <xsl:param name="s"/>
    <xsl:choose>
      
      <!-- tab -->
      <xsl:when test="contains($s,''&#x9;'')">
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&#x9;''),''\t'',substring-after($s,''&#x9;''))"/>
        </xsl:call-template>
      </xsl:when>
      <!-- line feed -->
      <xsl:when test="contains($s,''&#xA;'')">
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&#xA;''),''\n'',substring-after($s,''&#xA;''))"/>
        </xsl:call-template>
      </xsl:when>
      <!-- carriage return -->
      <xsl:when test="contains($s,''&#xD;'')">
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&#xD;''),''\r'',substring-after($s,''&#xD;''))"/>
        </xsl:call-template>
      </xsl:when> 
      <!-- less than -->
      <xsl:when test="contains($s,''&lt;'')">
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&lt;''),''^[^'',substring-after($s,''&lt;''))"/>
        </xsl:call-template>
      </xsl:when> 
      <!-- greater than -->
      <xsl:when test="contains($s,''&gt;'')">
        <xsl:call-template name="encode-string">
          <xsl:with-param name="s" select="concat(substring-before($s,''&gt;''),''^]^'',substring-after($s,''&gt;''))"/>
        </xsl:call-template>
      </xsl:when>      
      <xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- boolean, case-insensitive -->
  <xsl:template match="text()[translate(.,''TRUE'',''true'')=''true'']">true</xsl:template>
  <xsl:template match="text()[translate(.,''FALSE'',''false'')=''false'']">false</xsl:template>

  <!-- item:null -->
  <xsl:template match="*[count(child::node())=0]">
    <xsl:call-template name="escape-string">
      <xsl:with-param name="s" select="local-name()"/>
    </xsl:call-template>
    <xsl:text>:null</xsl:text>
    <xsl:if test="following-sibling::*">,</xsl:if>
    <xsl:if test="not(following-sibling::*)">}</xsl:if> <!-- MBR 30.01.2010: added this line as it appeared to be missing from stylesheet --> 
  </xsl:template>

  <!-- object -->
  <xsl:template match="*" name="base">
    <xsl:if test="not(preceding-sibling::*)">{</xsl:if>
    <xsl:call-template name="escape-string">
      <xsl:with-param name="s" select="name()"/>
    </xsl:call-template>
    <xsl:text>:</xsl:text>
    <xsl:apply-templates select="child::node()"/>
    <xsl:if test="following-sibling::*">,</xsl:if>
    <xsl:if test="not(following-sibling::*)">}</xsl:if>
  </xsl:template>

  <!-- array -->
  <xsl:template match="*[count(../*[name(../*)=name(.)])=count(../*) and count(../*)&gt;1]">
    <xsl:if test="not(preceding-sibling::*)">[</xsl:if>
    <xsl:choose>
      <xsl:when test="not(child::node())">
        <xsl:text>null</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="child::node()"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="following-sibling::*">,</xsl:if>
    <xsl:if test="not(following-sibling::*)">]</xsl:if>
  </xsl:template>
  
  <!-- convert root element to an anonymous container -->
  <xsl:template match="/">
    <xsl:apply-templates select="node()"/>
  </xsl:template>
    
</xsl:stylesheet>';
   END get_xml_to_json_stylesheet;


   FUNCTION ref_cursor_to_json (p_ref_cursor   IN SYS_REFCURSOR,
                                p_max_rows     IN NUMBER := NULL,
                                p_skip_rows    IN NUMBER := NULL,
                                p_rowsettag    IN VARCHAR2 := NULL)
      RETURN CLOB
   AS
      l_ctx           DBMS_XMLGEN.ctxhandle;
      l_num_rows      PLS_INTEGER;
      l_xml           XMLTYPE;
      l_json          XMLTYPE;
      l_returnvalue   CLOB;
   BEGIN
      /*

      Purpose:    generate JSON from REF Cursor

      Remarks:

      Who     Date        Description
      ------  ----------  -------------------------------------
      MBR     30.01.2010  Created

      */

      --logthis ('A');
      l_ctx := DBMS_XMLGEN.newcontext (p_ref_cursor);

      --logthis ('B');
      DBMS_XMLGEN.setnullhandling (l_ctx, DBMS_XMLGEN.empty_tag);

      --logthis ('C');

      IF p_rowsettag IS NOT NULL
      THEN
         DBMS_XMLGEN.setrowsettag (l_ctx, p_rowsettag);
      END IF;

      --logthis ('D');

      -- for pagination

      IF p_max_rows IS NOT NULL
      THEN
         DBMS_XMLGEN.setmaxrows (l_ctx, p_max_rows);
      END IF;

      --logthis ('E');

      IF p_skip_rows IS NOT NULL
      THEN
         DBMS_XMLGEN.setskiprows (l_ctx, p_skip_rows);
      END IF;

      --logthis ('F');
      -- get the XML content
      l_xml := DBMS_XMLGEN.getxmltype (l_ctx, DBMS_XMLGEN.none);

      l_num_rows := DBMS_XMLGEN.getnumrowsprocessed (l_ctx);

      DBMS_XMLGEN.closecontext (l_ctx);

      --logthis ('G');

      CLOSE p_ref_cursor;

      IF l_num_rows > 0
      THEN
         -- perform the XSL transformation
         l_json := l_xml.transform (xmltype (get_xml_to_json_stylesheet));
         l_returnvalue := l_json.getclobval ();
      ELSE
         l_returnvalue := g_json_null_object;
      END IF;

      --logthis ('H');
      l_returnvalue :=
         DBMS_XMLGEN.CONVERT (l_returnvalue, DBMS_XMLGEN.entity_decode);

      --logthis ('I');
      RETURN l_returnvalue;
   END ref_cursor_to_json;


   FUNCTION sql_to_json (p_sql            IN VARCHAR2,
                         p_param_names    IN t_str_array := NULL,
                         p_param_values   IN t_str_array := NULL,
                         p_max_rows       IN NUMBER := NULL,
                         p_skip_rows      IN NUMBER := NULL,
                         p_rowsettag      IN VARCHAR2 := NULL)
      RETURN CLOB
   AS
      l_ctx           DBMS_XMLGEN.ctxhandle;
      l_num_rows      PLS_INTEGER;
      l_xml           XMLTYPE;
      l_json          XMLTYPE;
      l_returnvalue   CLOB;
   BEGIN
      /*

      Purpose:    generate JSON from SQL statement

      Remarks:

      Who     Date        Description
      ------  ----------  -------------------------------------
      MBR     30.01.2010  Created
      MBR     28.07.2010  Handle null value in bind variable value (issue and solution reported by Matt Nolan)

      */


      l_ctx := DBMS_XMLGEN.newcontext (p_sql);

      DBMS_XMLGEN.setnullhandling (l_ctx, DBMS_XMLGEN.empty_tag);

      IF p_rowsettag IS NOT NULL
      THEN
         DBMS_XMLGEN.setrowsettag (l_ctx, p_rowsettag);
      END IF;

      --logthis (1);

      -- bind variables, if any
      IF p_param_names IS NOT NULL
      THEN
         FOR i IN 1 .. p_param_names.COUNT
         LOOP
            DBMS_XMLGEN.setbindvalue (l_ctx,
                                      p_param_names (i),
                                      NVL (p_param_values (i), ''));
         END LOOP;
      END IF;

      --logthis (2);

      -- for pagination

      IF p_max_rows IS NOT NULL
      THEN
         DBMS_XMLGEN.setmaxrows (l_ctx, p_max_rows);
      END IF;

      IF p_skip_rows IS NOT NULL
      THEN
         DBMS_XMLGEN.setskiprows (l_ctx, p_skip_rows);
      END IF;

      --logthis (3);
      -- get the XML content
      l_xml := DBMS_XMLGEN.getxmltype (l_ctx, DBMS_XMLGEN.none);

      l_num_rows := DBMS_XMLGEN.getnumrowsprocessed (l_ctx);

      DBMS_XMLGEN.closecontext (l_ctx);

      --logthis (4);

      -- perform the XSL transformation
      IF l_num_rows > 0
      THEN
         l_json := l_xml.transform (xmltype (get_xml_to_json_stylesheet));
        -- l_json := l_xml.transform (get_jsonml_stylesheet);
         
         l_returnvalue := l_json.getclobval ();
         l_returnvalue :=
            DBMS_XMLGEN.CONVERT (l_returnvalue, DBMS_XMLGEN.entity_decode);
      ELSE
         l_returnvalue := g_json_null_object;
      END IF;

      l_returnvalue := REPLACE( REPLACE (l_returnvalue, '^[^', '<'), '^]^', '>');
      RETURN l_returnvalue;
   END sql_to_json;
END json_util_pkg;
/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION NISIS.get_datablock (in_layernm    IN VARCHAR2,
                                                in_objectid   IN NUMBER)
   RETURN CLOB
IS
   tn                     VARCHAR2 (50);
   main_status_col        VARCHAR2 (50);
   facid_col              VARCHAR2 (50);
   collist                VARCHAR2 (10000);
   result                 CLOB;
   nullresult             VARCHAR2 (1500)
      := '{"DATABLOCK":{"METADATA":[],"MSG":"Unexpected error."}}';
   nulldataresult         VARCHAR2 (1500)
      := '{"DATABLOCK":{"METADATA":[],"MSG":"Unable to query for record."}}';
   nullmetaresult         VARCHAR2 (1500)
      := '{"DATABLOCK":{"METADATA":[],"MSG":"Permissions error."}}';

   metaresult             VARCHAR2 (32767);
   dataresult             VARCHAR2 (32767);
   metaquery              VARCHAR2 (12000) := NULL;
   dataquery              VARCHAR2 (12000) := NULL;

   invalid_params_exc     EXCEPTION;
   invalid_metadata_exc   EXCEPTION;
   nodatafound_exc        EXCEPTION;
   invalid_collist_exc    EXCEPTION;
BEGIN
   EXECUTE IMMEDIATE
      ('ALTER SESSION SET NLS_DATE_FORMAT=''YYYY/MM/DD HH24:MI:SS''');

   EXECUTE IMMEDIATE
      ('ALTER SESSION SET NLS_TIMESTAMP_FORMAT=''YYYY/MM/DD HH24:MI:SS''');


   IF in_layernm IS NULL OR in_objectid IS NULL
   THEN
      RAISE invalid_params_exc;
   END IF;


   BEGIN
      SELECT table_nm, main_status_col_nm, table_pk
        INTO tn, main_status_col, facid_col
        FROM nisis_admin.acl_tables
       WHERE tableid = (SELECT tableid
                          FROM nisis_admin.gis_layers
                         WHERE LOWER (layer) = LOWER (in_layernm));

      metaquery :=
            'SELECT ''OBJECTID'' AS col_nm, ''OBJECTID'' AS field_display_name, 25 AS data_length,
       0 AS sortcol FROM DUAL UNION SELECT a.col_nm, a.field_display_nm, a.data_length, a.sortcol                                     
FROM (SELECT DISTINCT c.column_id,
v.col_nm,
v.field_display_nm,
c.data_length,
v.tableid,
v.fieldid,
v.datablock_item as sortcol
FROM nisis_admin.datablock_fields_comp_view v,
nisis.nisis_geodata_cols_v c
WHERE     v.table_nm = c.table_name
AND v.col_nm = c.column_name
AND v.table_nm = '''
         || tn
         || '''  ) a ORDER BY sortcol';
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE invalid_metadata_exc;
   END;

  -- --DBMS_OUTPUT.put_line (tn);

   BEGIN
      FOR r IN (SELECT *
                  FROM nisis_admin.datablock_fields_comp_view
                 WHERE table_nm = tn order by datablock_item)
      LOOP
         collist := collist || ', f.' || r.col_nm;
      END LOOP;

      IF collist IS NULL
      THEN
         RAISE invalid_collist_exc;
      END IF;

      ----DBMS_OUTPUT.PUT_LINE('INSTR='||INSTR (collist, 'OBJECTID'));
      IF INSTR (collist, 'OBJECTID') = 0
      THEN
         collist := ' f.objectid' || collist;
      ELSE
         collist := LTRIM (collist, ',');
      END IF;

     -- --DBMS_OUTPUT.put_line ('collist = ' || collist);
      dataquery :=
            'SELECT '
         || collist
         || ' FROM nisis_geodata.'
         || tn
         || ' f 
       WHERE  f.objectid = '
         || in_objectid;
   END;

   --DBMS_OUTPUT.put_line ('1.1 ');


   BEGIN
      --DBMS_OUTPUT.put_line (metaquery);
      metaresult :=
         json_util_pkg.sql_to_json (metaquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'METADATA');
      --DBMS_OUTPUT.put_line ('1.31 ');
   EXCEPTION
      WHEN OTHERS
      THEN
         --DBMS_OUTPUT.put_line ('1.3e ' || SQLERRM);
         RAISE invalid_metadata_exc;
   END;

   BEGIN
      --DBMS_OUTPUT.put_line (dataquery);
      dataresult :=
         json_util_pkg.sql_to_json (dataquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'DATA');
      --DBMS_OUTPUT.put_line ('1.5');

      IF dataresult = '{ }'
      THEN
         result := nulldataresult;
      ELSIF metaresult = '{ }'
      THEN
         result := nullmetaresult;
      ELSE
         result :=
               '{"DATABLOCK": '
            || SUBSTR (metaresult, 1, LENGTH (metaresult) - 1)
            || ',';
         result := result || LTRIM (dataresult, '{') || '}';
      END IF;

      --DBMS_OUTPUT.put_line ('2');
   EXCEPTION
      WHEN OTHERS
      THEN
         --DBMS_OUTPUT.put_line ('2e: ' || SQLERRM);
         RAISE nodatafound_exc;
   END;



   RETURN result;
EXCEPTION
   WHEN OTHERS
   THEN
      --DBMS_OUTPUT.put_line ('X: ' || SQLERRM);

      RETURN nullresult;
END;
/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION NISIS.get_item_status_type (in_tablenm   IN VARCHAR2,
                                                    in_colnm     IN VARCHAR2)
   RETURN VARCHAR2
IS
   currcol   VARCHAR2 (40);
   tn        VARCHAR2 (30) := UPPER (in_tablenm);
   cn        VARCHAR2 (30) := UPPER (in_colnm);
BEGIN
   SELECT CASE
             WHEN ex_col_nm = cn THEN 'EX.'||col_nm
             WHEN sn_col_nm = cn THEN 'SN'
             ELSE 'ST.'||ex_col_nm
          END
     INTO currcol
     FROM nisis_admin.status_hist_assoc
    WHERE table_nm = tn AND cn IN (col_nm, ex_col_nm, sn_col_nm);

   RETURN currcol;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
END;
/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION NISIS.get_iwl_data (in_layernm   IN VARCHAR2,
                                               in_iwl_id    IN NUMBER)
   RETURN CLOB
IS
   tn                     VARCHAR2 (50);
   main_status_col        VARCHAR2 (50);
   facid_col              VARCHAR2 (50);
   collist                VARCHAR2 (10000);
   result                 CLOB;
   nullresult             VARCHAR2 (1500)
      := '{"IWL":{"METADATA":[],"MSG":"Unexpected error."}}';
   nulldataresult         VARCHAR2 (1500)
      := '{"IWL":{"METADATA":[],"MSG":"Unable to query for record."}}';
   nullmetaresult         VARCHAR2 (1500)
      := '{"IWL":{"METADATA":[],"MSG":"Permissions error."}}';

   metaresult             VARCHAR2 (32767);
   dataresult             VARCHAR2 (32767);
   metaquery              VARCHAR2 (12000) := NULL;
   dataquery              VARCHAR2 (12000) := NULL;

   invalid_params_exc     EXCEPTION;
   invalid_metadata_exc   EXCEPTION;
   nodatafound_exc        EXCEPTION;
   invalid_collist_exc    EXCEPTION;
BEGIN
   EXECUTE IMMEDIATE
      ('ALTER SESSION SET NLS_DATE_FORMAT=''YYYY/MM/DD HH24:MI:SS''');

   EXECUTE IMMEDIATE
      ('ALTER SESSION SET NLS_TIMESTAMP_FORMAT=''YYYY/MM/DD HH24:MI:SS''');

  -- DBMS_OUTPUT.put_line (1);

   IF in_layernm IS NULL OR in_iwl_id IS NULL
   THEN
      RAISE invalid_params_exc;
   END IF;

   --DBMS_OUTPUT.put_line (2);

   BEGIN
      SELECT table_nm, main_status_col_nm, table_pk
        INTO tn, main_status_col, facid_col
        FROM nisis_admin.acl_tables
       WHERE tableid = (SELECT tableid
                          FROM nisis_admin.gis_layers
                         WHERE LOWER (layer) = LOWER (in_layernm));

      metaquery :=
            'SELECT ''OBJECTID'' AS col_nm, ''OBJECTID'' AS field_display_name, 25 AS data_length,
       0 AS sortcol FROM DUAL UNION SELECT a.col_nm, a.field_display_nm, a.data_length, a.sortcol                                        
FROM (SELECT DISTINCT c.column_id,
v.col_nm,
v.field_display_nm,
c.data_length,
v.tableid,
v.fieldid,
v.iwl_item as sortcol
FROM nisis_admin.iwl_fields_comp_view v,
nisis.nisis_geodata_cols_v c
WHERE     v.table_nm = c.table_name
AND v.col_nm = c.column_name
AND v.table_nm = '''
         || tn
         || ''' ) a ORDER BY sortcol';
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE invalid_metadata_exc;
   END;

   --DBMS_OUTPUT.put_line (tn);

   BEGIN
      FOR r IN (SELECT *
                  FROM nisis_admin.iwl_fields_comp_view
                 WHERE table_nm = tn order by iwl_item)
      LOOP
         collist := collist || ', f.' || r.col_nm;
      END LOOP;

      IF collist IS NULL
      THEN
         RAISE invalid_collist_exc;
      END IF;
--DBMS_OUTPUT.PUT_LINE('INSTR='||INSTR (collist, 'OBJECTID'));
      IF INSTR (collist, 'OBJECTID') = 0
      THEN
         collist := ' f.objectid' || collist;
      ELSE
         collist := LTRIM (collist, ',');
      END IF;

    --  DBMS_OUTPUT.put_line ('collist = ' || collist);
      dataquery :=
            'SELECT '
         || collist
         || ' FROM nisis_geodata.'
         || tn
         || ' f,nisis.nisis_iwl i, nisis.nisis_iwl_objects o 
       WHERE  f.objectid = o.objectid
            AND o.iwl_id = i.id
            AND i.id = '
         || in_iwl_id;
   END;

  -- DBMS_OUTPUT.put_line ('1.1 ');


   BEGIN
   --   DBMS_OUTPUT.put_line (metaquery);
      metaresult :=
         json_util_pkg.sql_to_json (metaquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'METADATA');
     -- DBMS_OUTPUT.put_line ('1.31 ');
   EXCEPTION
      WHEN OTHERS
      THEN
      --   DBMS_OUTPUT.put_line ('1.3e ' || SQLERRM);
         RAISE invalid_metadata_exc;
   END;

   BEGIN
     -- DBMS_OUTPUT.put_line (dataquery);
      dataresult :=
         json_util_pkg.sql_to_json (dataquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'DATA');
    --  DBMS_OUTPUT.put_line ('1.5');

      IF dataresult = '{ }'
      THEN
         result := nulldataresult;
      ELSIF metaresult = '{ }'
      THEN
         result := nullmetaresult;
      ELSE
         result :=
               '{"IWL": '
            || SUBSTR (metaresult, 1, LENGTH (metaresult) - 1)
            || ',';
         result := result || LTRIM (dataresult, '{') || '}';
      END IF;

    --  DBMS_OUTPUT.put_line ('2');
   EXCEPTION
      WHEN OTHERS
      THEN
       --  DBMS_OUTPUT.put_line ('2e: ' || SQLERRM);
         RAISE nodatafound_exc;
   END;



   RETURN result;
EXCEPTION
   WHEN OTHERS
   THEN
   --   DBMS_OUTPUT.put_line ('X: ' || SQLERRM);

      RETURN nullresult;
END;
/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION NISIS.get_profile_statuses (
   in_tablenm    IN VARCHAR2,
   in_objectid   IN NUMBER)
   RETURN VARCHAR2
IS
   tn           VARCHAR2 (30) := UPPER (in_tablenm);
   objname      VARCHAR2 (30) := '"STATUSDATA":';
   nullresult   VARCHAR2 (30) := '"STATUSDATA":{ }';
   result       VARCHAR2 (20000);
   tmp          VARCHAR2 (20000);
   ex           VARCHAR2 (1000) := ' ';
   sn           VARCHAR2 (1000) := ' ';
BEGIN
   result := objname;

   FOR r IN (SELECT col_nm, ex_col_nm, sn_col_nm
               FROM nisis_admin.status_hist_assoc
              WHERE table_nm = tn)
   LOOP
      ex :=
         json_util_pkg.sql_to_json (
               'SELECT status_column,  
   userid,
   status,
   status_display,
   status_date,
   rtg_code,
   im_role,
   explanation 
   FROM nisis_geodata.master_obj_status 
   WHERE objectid='
            || in_objectid
            || ' and source_column = '''
            || r.ex_col_nm
            || '''',
            NULL,
            NULL,
            NULL,
            NULL,
            r.ex_col_nm);

       --DBMS_OUTPUT.put_line (ex);

      IF ex <> '{ }'
      THEN
         IF tmp <> objname
         THEN
            tmp := SUBSTR(tmp, 1, LENGTH(tmp)-1) || ',';
         END IF;

         tmp :=
               tmp
            --|| '{'
            || REPLACE (SUBSTR (ex, 2, LENGTH (ex) - 2), '{"ROW":');
      END IF;



      sn :=
         json_util_pkg.sql_to_json (
               'SELECT status_column,  
   userid,
   status,
   status_display,
   status_date,
   rtg_code,
   im_role,
   explanation 
   FROM nisis_geodata.master_obj_status 
   WHERE objectid='
            || in_objectid
            || ' and source_column = '''
            || r.sn_col_nm
            || '''',
            NULL,
            NULL,
            NULL,
            NULL,
            r.sn_col_nm);

        --DBMS_OUTPUT.put_line (sn);

      IF sn <> '{ }'
      THEN
         IF tmp <> objname
         THEN
            tmp := SUBSTR(tmp, 1, LENGTH(tmp)-1) || ',';
         END IF;

         tmp :=
               tmp
           -- || '{'
            || REPLACE (SUBSTR (sn, 2, LENGTH (sn) - 2), '{"ROW":');
      END IF;
   END LOOP;


   IF tmp <> objname
   THEN
      result := objname || '{' || tmp ;--|| '}';
      --result := objname || tmp ;
   ELSE
      result := nullresult;
   END IF;

   RETURN result;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN nullresult;
END;
/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION NISIS.get_status_expl_col (in_tablenm   IN VARCHAR2,
                                                    in_colnm     IN VARCHAR2)
   RETURN VARCHAR2
IS
   excol   VARCHAR2 (40);
   tn        VARCHAR2 (30) := UPPER (in_tablenm);
   cn        VARCHAR2 (30) := UPPER (in_colnm);
BEGIN
   SELECT ex_col_nm
     INTO excol
     FROM nisis_admin.status_hist_assoc
    WHERE table_nm = tn AND col_nm = cn;

   RETURN excol;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
END;
/

SHOW ERRORS;

ALTER FUNCTION NISIS.GET_USER_FIELD_PERM COMPILE;

CREATE OR REPLACE FORCE VIEW NISIS.LAYER_FIELD_PICKLISTS
(LAYER, LAYERID, TABLEID, TABLE_NM, TABLE_DISPLAY_NM, 
 COL_NM, FIELD_DISPLAY_NM, FIELDID, PICKLIST, DESCRIPTION, 
 LABEL, VALUE, IMGID, DISPLAY_ORDER)
AS 
SELECT gl.layer,
          gl.layerid,
          t.tableid,
          t.table_nm,
          t.display_nm AS table_display_nm,
          f.col_nm,
          f.display_nm AS field_display_nm,
          f.fieldid,
          pp.picklist,
          pp.description,
          pi.label,
          pi.VALUE,
          pi.imgid,
          pi.display_order
     FROM nisis_admin.gis_layers gl,
          nisis_admin.acl_fields f,
          nisis_admin.acl_tables t,
          nisis.picklist pp,
          nisis.picklist_items pi
    WHERE     gl.tableid = t.tableid
          AND f.tableid = t.tableid
          AND f.picklist = pp.picklist
          AND pi.picklist = pp.picklist;

-- No action taken.  This is a column of a view.
-- Changes should be made in underlying objects of the view, not the view itself.

CREATE OR REPLACE FORCE VIEW NISIS.NISIS_AIRPORT_ATM_VW
(ARPT_ID, OBJECTID, ATM_ID, LG_NAME, BASIC_TP, 
 RESP_SU, DTL_TYPE, CATC_VD, ADDRESS, ASSOC_CITY, 
 REF_PC, LATITUDE, LONGITUDE, ASSOC_CNTY, ASSOC_ST, 
 TIME_ZONE, FAA_REG, ATO_SA, ATO_TD, FEMA_REG, 
 OPS_HRS, PHOTO, PRIM_PH, ATC_LEVEL, TOWER_TYPE, 
 APPR_CTRL, ENRTE_ATC, FLIGHT_SVC, MANAGER, ALT_PH, 
 SAT_PH, TD_MGR, TO_DIST, TOD_MGR, STOF_FAC, 
 OPS_STATUS, OPS_STS_AD, OPS_STS_EX, OPS_STS_SN, OPS_STS_AL, 
 EG_SUP, PWR_STS, PWR_STS_EX, PWR_STS_SN, ASS_P_TTL, 
 ASS_P_BEX, OPS_SL, OPS_SL_EX, ATO_OSL, ATO_PA_STS, 
 ATO_PA_TTL, ATO_PAT, ATO_PAS_EX, ATO_PSI, ATO_PSI_EX, 
 ATO_PSI_SN, ANS_ID, ANS_ID_STS, CRT_RL, CRT_RL_EX, 
 CRT_RL_SN, CRT_RL_AL, CRT_SL, CRT_SL_EX, CRT_SL_SN, 
 CRT_SL_AL, FS_INTER, FS_MBER, IWL_DSG, IWL_MBER, 
 LAST_UPDT, ATM_NOTES, NCI_OB_CAT, NCI_CI_REL, SYMBOLOGY, 
 SHAPE, CREATED_USER, CREATED_DATE, LAST_EDITED_USER, LAST_EDITED_DATE)
AS 
SELECT a.arpt_id arpt_id,
          b.objectid,
          b.atm_id,
          b.lg_name,
          b.basic_tp,
          b.resp_su,
          b.dtl_type,
          b.catc_vd,
          b.address,
          b.assoc_city,
          b.ref_pc,
          b.latitude,
          b.longitude,
          b.assoc_cnty,
          b.assoc_st,
          b.time_zone,
          b.faa_reg,
          b.ato_sa,
          b.ato_td,
          b.fema_reg,
          b.ops_hrs,
          b.photo,
          b.prim_ph,
          b.atc_level,
          b.tower_type,
          b.appr_ctrl,
          b.enrte_atc,
          b.flight_svc,
          b.manager,
          b.alt_ph,
          b.sat_ph,
          b.td_mgr,
          b.to_dist,
          b.tod_mgr,
          b.stof_fac,
          b.ops_status,
          b.ops_sts_ad,
          b.ops_sts_ex,
          b.ops_sts_sn,
          b.ops_sts_al,
          b.eg_sup,
          b.pwr_sts,
          b.pwr_sts_ex,
          b.pwr_sts_sn,
          b.ass_p_ttl,
          b.ass_p_bex,
          b.ops_sl,
          b.ops_sl_ex,
          b.ato_osl,
          b.ato_pa_sts,
          b.ato_pa_ttl,
          b.ato_pat,
          b.ato_pas_ex,
          b.ato_psi,
          b.ato_psi_ex,
          b.ato_psi_sn,
          b.ans_id,
          b.ans_id_sts,
          b.crt_rl,
          b.crt_rl_ex,
          b.crt_rl_sn,
          b.crt_rl_al,
          b.crt_sl,
          b.crt_sl_ex,
          b.crt_sl_sn,
          b.crt_sl_al,
          b.fs_inter,
          b.fs_mber,
          b.iwl_dsg,
          b.iwl_mber,
          b.last_updt,
          b.atm_notes,
          b.nci_ob_cat,
          b.nci_ci_rel,
          b.symbology,
          b.shape,
          b.created_user,
          b.created_date,
          b.last_edited_user,
          b.last_edited_date
     --,b.gdb_from_date
     -- ,b.gdb_to_date
     -- ,b.gdb_archive_oid
     --,b.profile_overwrite
     FROM nisis_geodata.nisis_arpt_atm a, nisis_geodata.nisis_atm b
    WHERE a.atm_id = b.atm_id;

DROP SYNONYM NISIS.ADAPT_TRACKS;
CREATE SYNONYM NISIS.ADAPT_TRACKS FOR ADAPT_TRACKS@"NISISDATA";

DROP SYNONYM NISIS.AIDAP_NOTAMS;
CREATE SYNONYM NISIS.AIDAP_NOTAMS FOR AIDAP_NOTAMS@"NISISDATA";

DROP SYNONYM NISIS.NISIS_ARPT_RUNWAY_LINE;
CREATE SYNONYM NISIS.NISIS_ARPT_RUNWAY_LINE FOR NISIS_ARPT_RUNWAY_LINE@"NISISDATA";

DROP SYNONYM NISIS.NISIS_AWY_LINE;
CREATE SYNONYM NISIS.NISIS_AWY_LINE FOR NISIS_AWY_LINE@"NISISDATA";

DROP SYNONYM NISIS.NISIS_AWY_POINTS;
CREATE SYNONYM NISIS.NISIS_AWY_POINTS FOR NISIS_AWY_POINTS@"NISISDATA";

DROP SYNONYM NISIS.NTML_CURR_ARRIV_RATES;
CREATE SYNONYM NISIS.NTML_CURR_ARRIV_RATES FOR NTML_CURR_ARRIV_RATES@"NISISDATA";

DROP SYNONYM NISIS.NTML_ENG_ARRIV_RATES;
CREATE SYNONYM NISIS.NTML_ENG_ARRIV_RATES FOR NTML_ENG_ARRIV_RATES@"NISISDATA";

CREATE OR REPLACE TYPE NISIS.picklist_item_tab AS TABLE OF picklist_item_typ;
/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION NISIS.get_profile_data (in_userid     IN NUMBER,
                                                    in_layernm    IN VARCHAR2,
                                                    in_objectid   IN NUMBER)
   RETURN CLOB
IS
   tn                      VARCHAR2 (50);
   main_status_col         VARCHAR2 (50);
   facid_col VARCHAR2 (50);
   result                  CLOB;
   nullresult              VARCHAR2 (1500)
      := '{"PROFILE":{"METADATA":[],"MSG":"Unexpected error."}}';
   nulldataresult          VARCHAR2 (1500)
      := '{"PROFILE":{"METADATA":[],"MSG":"Unable to query for record."}}';
   nullmetaresult          VARCHAR2 (1500)
      := '{"PROFILE":{"METADATA":[],"MSG":"Permissions error."}}';
   --
   layerresult             VARCHAR2 (4000);
   --sectionresult           VARCHAR2 (1000);
   metaresult              VARCHAR2 (32767);
   dataresult              VARCHAR2 (32767);
   statusresult            VARCHAR2 (32000);
   --
   layerquery              VARCHAR2 (400) := NULL;
   --sectionquery            VARCHAR2 (400) := NULL;
   metaquery               VARCHAR2 (12000) := NULL;
   dataquery               VARCHAR2 (12000) := NULL;
   --statusquery             VARCHAR2 (400) := NULL;
   --
   invalid_params_exc      EXCEPTION;
   invalid_layer_exc       EXCEPTION;
   --invalid_section_exc     EXCEPTION;
   invalid_metadata_exc    EXCEPTION;
   invalid_layerinfo_exc   EXCEPTION;
   invalid_status_exc      EXCEPTION;
   nodatafound_exc         EXCEPTION;
BEGIN
   EXECUTE IMMEDIATE
      ('ALTER SESSION SET NLS_DATE_FORMAT=''YYYY/MM/DD HH24:MI:SS''');

   EXECUTE IMMEDIATE
      ('ALTER SESSION SET NLS_TIMESTAMP_FORMAT=''YYYY/MM/DD HH24:MI:SS''');

   DBMS_OUTPUT.put_line (1);

   IF in_userid IS NULL OR in_layernm IS NULL OR in_objectid IS NULL
   THEN
      RAISE invalid_params_exc;
   END IF;

   DBMS_OUTPUT.put_line (2);

   BEGIN
      SELECT table_nm, main_status_col_nm, table_pk
        INTO tn, main_status_col, facid_col
        FROM nisis_admin.acl_tables
       WHERE tableid = (SELECT tableid
                          FROM nisis_admin.gis_layers
                         WHERE LOWER (layer) = LOWER (in_layernm));

      layerquery :=
            'SELECT layerid, layer, layer_long_name, '''
         || UPPER (tn)
         || ''' as table_name, '''
         || main_status_col
         || ''' as main_status_col_nm, '''
         || facid_col
         || ''' as facid_col FROM nisis_admin.gis_layers WHERE LOWER(layer)= '''
         || LOWER (in_layernm)
         || '''';

      --      sectionquery :=
      --            'SELECT ps.sectionid, ps.sectionname, ps.display_order
      --            FROM nisis_admin.profile_sections ps, nisis_admin.gis_layers gl
      --            WHERE ps.layerid = gl.layerid AND  LOWER(layer)='''
      --         || LOWER (in_layernm)
      --         || ''' order by ps.display_order';


      metaquery :=
            'SELECT a.col_nm, a.field_display_nm, a.data_length,
nisis.get_user_field_perm (a.tableid, a.fieldid, a.userid) AS perm,
nisis.get_field_picklist (a.tableid, a.fieldid) as picklist,
nisis.get_item_status_type ('''
         || UPPER (tn)
         || ''',UPPER(a.col_nm)) as item_status_type                                         
FROM (SELECT DISTINCT c.column_id,
v.col_nm,
v.field_display_nm,
c.data_length,
v.tableid,
v.fieldid,
v.frow,
v.userid
FROM nisis_admin.user_grp_fields_comp_view v,
nisis.nisis_geodata_cols_v c
WHERE     v.table_nm = c.table_name
AND v.col_nm = c.column_name
AND v.userid = '
         || in_userid
         || '
               AND v.table_nm = '''
         || tn
         || ''' ORDER BY v.frow ) a';

      dataquery :=
            'SELECT * FROM nisis_geodata.'
         || tn
         || ' WHERE objectid = '
         || in_objectid;
   --      statusquery := 'SELECT source_column,
   --   objectid,
   --   userid,
   --   status,
   --   status_display,
   --   status_date,
   --   rtg_code,
   --   im_role,
   --   explanation
   --   FROM nisis_geodata.master_obj_status
   --   WHERE objectid=' || in_objectid;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE invalid_layer_exc;
   END;

   DBMS_OUTPUT.put_line (tn);

   BEGIN
      layerresult :=
         json_util_pkg.sql_to_json (layerquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'LAYERINFO');
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('1.1e ' || SQLERRM);
         RAISE invalid_layerinfo_exc;
   END;

   DBMS_OUTPUT.put_line ('1.1 ');

   --   BEGIN
   --      sectionresult :=
   --         json_util_pkg.sql_to_json (sectionquery,
   --                                    NULL,
   --                                    NULL,
   --                                    NULL,
   --                                    NULL,
   --                                    'SECTIONINFO');
   --   EXCEPTION
   --      WHEN OTHERS
   --      THEN
   --         DBMS_OUTPUT.put_line ('1.2e ' || SQLERRM);
   --         RAISE invalid_section_exc;
   --   END;
   --
   --   DBMS_OUTPUT.put_line ('1.2 ');

   BEGIN
      --DBMS_OUTPUT.put_line (metaquery);
      metaresult :=
         json_util_pkg.sql_to_json (metaquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'METADATA');
      DBMS_OUTPUT.put_line ('1.31 ');
      metaresult := REPLACE (metaresult, '"PICKLIST":"\n  "', '"PICKLIST":""');

      DBMS_OUTPUT.put_line ('1.32 ');
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('1.3e ' || SQLERRM);
         RAISE invalid_metadata_exc;
   END;


   BEGIN
      statusresult := get_profile_statuses (tn, in_objectid);
      DBMS_OUTPUT.put_line ('1.4');
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('1.4e ' || SQLERRM);
         RAISE invalid_status_exc;
   END;


   BEGIN
      dataresult :=
         json_util_pkg.sql_to_json (dataquery,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    'DATA');
      DBMS_OUTPUT.put_line ('1.5');

      IF dataresult = '{ }'
      THEN
         result := nulldataresult;
      ELSIF metaresult = '{ }'
      THEN
         result := nullmetaresult;
      ELSE
         result :=
               '{"PROFILE": '
            || SUBSTR (layerresult, 1, LENGTH (layerresult) - 1)
            || ','
            --            || SUBSTR (sectionresult, 2, LENGTH (sectionresult) - 2)
            --            || ','
            || SUBSTR (metaresult, 2, LENGTH (metaresult) - 2)
            || ',';


         result := result || statusresult || ',';


         result := result || LTRIM (dataresult, '{') || '}';
      END IF;

      DBMS_OUTPUT.put_line ('2');
   --DBMS_OUTPUT.put_line (result);
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('2e: ' || SQLERRM);
         RAISE nodatafound_exc;
   END;



   RETURN result;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('X: ' || SQLERRM);

      RETURN nullresult;
END;
/

SHOW ERRORS;