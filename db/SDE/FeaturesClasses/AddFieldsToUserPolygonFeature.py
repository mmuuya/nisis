# Name: AddFieldsToUserPolygonFeature.py
# Description: Add a pair of new fields to a feature class
 
# Import system modules
import arcpy
from arcpy import env
 
# Set environment settings
env.workspace = arcpy.arcpy.GetParameterAsText(0)
 
# INPUT FEATURE
inFeature = arcpy.arcpy.GetParameterAsText(1)
# 1 NAME
field1_name = "NAME"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "NAME"
field1_isNullable = "NON_NULLABLE" #OR "NULLABLE"
field1_isRequired = "NON_REQUIRED" # OR "REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)
# ASSIGN DEFAULT VALUE
arcpy.AssignDefaultToField_management(inFeature, field1_name, "POLYGON")

# 2 REF_NUMBER
field1_name = "REF_NUMBER"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "SHAPE REFERENCE NUMBER"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 3 DESCRIPTION
field1_name = "DESCRIPTION"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 2048
field1_alias = "DESCRIPTION"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 4 SUP_NOTES
field1_name = "SUP_NOTES"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 2048
field1_alias = "SUPPLEMENTAL NOTES"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 5 FS_DESIGN
field1_name = "FS_DESIGN"
field1_type = "SHORT"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "FILTER SHAPE DESIGNATION"
field1_isNullable = "NON_NULLABLE"
field1_isRequired = "NON_REQUIRED"
field1_Domain = "FS_Designation"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired, field1_Domain)

# 6 IWA_DESIGN
field1_name = "IWA_DESIGN"
field1_type = "SHORT"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "IWA DESIGNATION"
field1_isNullable = "NON_NULLABLE"
field1_isRequired = "NON_REQUIRED"
field1_Domain = "IWA_Designation"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired, field1_Domain)

# 7 MIN_A_AGL
field1_name = "MIN_A_AGL"
field1_type = "LONG"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "MINIMUM ALTITUDE (AGL)"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 8 MAX_A_MSL
field1_name = "MIN_A_MSL"
field1_type = "LONG"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "MINIMUM ALTITUDE (MSL)"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 9 MAX_A_AGL
field1_name = "MAX_A_AGL"
field1_type = "LONG"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "MAXIMUM ALTITUDE (AGL)"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 10 MAX_A_MSL
field1_name = "MAX_A_MSL"
field1_type = "LONG"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "MINIMUM ALTITUDE (MSL)"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 11 VERTICES
field1_name = "VERTICES"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 2048
field1_alias = "VERTICES"
field1_isNullable = "NULLABLE"
field1_isRequired = "NON_REQUIRED"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired)

# 12 SYMBOL
field1_name = "SYMBOL"
field1_type = "SHORT"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "SYMBOL"
field1_isNullable = "NON_NULLABLE"
field1_isRequired = "NON_REQUIRED"
field1_Domain = "polygon_symbols"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired, field1_Domain)

# 13 USER_GROUP
field1_name = "USER_GROUP"
field1_type = "SHORT"
field1_precision = ""
field1_scale = ""
field1_length = ""
field1_alias = "USER ACCESS GROUP"
field1_isNullable = "NON_NULLABLE"
field1_isRequired = "NON_REQUIRED"
field1_Domain = "User_Group"
# CREATE FIELD
arcpy.AddField_management(inFeature, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable, field1_isRequired, field1_Domain)