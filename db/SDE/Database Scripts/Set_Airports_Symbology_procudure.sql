declare
l_objectid    sde.nisis_airports.objectid%type; 
l_arp_type    sde.nisis_airports.arp_type%type;
l_basic_use   sde.nisis_airports.basic_use%type;
l_tower       sde.nisis_airports.tower%type;
l_class       sde.nisis_airports.class%type;
l_hub_type    sde.nisis_airports.hub_type%type;
l_symbology   sde.nisis_airports.symbology%type;

CURSOR airports_cursor IS SELECT * FROM SDE.NISIS_AIRPORTS;

TYPE rt_airports IS TABLE OF SDE.NISIS_AIRPORTS%ROWTYPE INDEX BY PLS_INTEGER;
l_rt rt_airports;

begin

open airports_cursor;
loop
    fetch airports_cursor bulk collect into l_rt limit 500;
    exit when l_rt.count = 0;
    for i in 1..l_rt.count  
        loop
        l_objectid := l_rt(i).objectid;
        l_arp_type := l_rt(i).arp_type;
        l_basic_use := l_rt(i).basic_use;
        l_tower := l_rt(i).tower;
        l_class := l_rt(i).class;
        l_hub_type := l_rt(i).hub_type;
        l_symbology := null;
        
        l_symbology := l_symbology || CASE l_arp_type 
        WHEN 'AIRPORT' THEN 'A' 
        WHEN 'HELIPORT' THEN 'H' 
        WHEN 'SEAPLANE BASE' THEN 'S' 
        ELSE 'X' END;
        
        l_symbology := l_symbology || CASE l_basic_use 
        WHEN 'PR' THEN 'PR' 
        WHEN 'PU' THEN 'PU' 
        ELSE 'X' END;
        
        l_symbology := l_symbology || CASE l_tower 
        WHEN 'Y' THEN 'Y' 
        WHEN 'N' THEN 'N' 
        ELSE 'X' END;
        
        l_symbology := l_symbology || CASE l_class 
        WHEN 'P' THEN 'P' 
        WHEN 'GA' THEN 'G' 
        WHEN 'CS' THEN 'C' 
        ELSE 'X' END;
        
        l_symbology := l_symbology || CASE l_hub_type 
        WHEN 'L' THEN 'L' 
        WHEN 'M' THEN 'M' 
        WHEN 'S' THEN 'S' 
        WHEN 'N' THEN 'N' 
        ELSE 'X' END;
        
        dbms_output.put_line('l_objectid = ' || l_objectid || CHR(13) ||
                                 'l_arp_type = ' || l_arp_type || CHR(13) ||
                                 'l_basic_use = ' || l_basic_use || CHR(13) ||
                                 'l_tower = ' || l_tower || CHR(13) ||
                                 'l_class = ' || l_class || CHR(13) ||
                                 'l_hub_type = ' || l_hub_type || CHR(13) ||
                                 'l_symbology = ' || l_symbology
                                 ); 
    end loop;

end loop; 

close airports_cursor;
end;