ALTER TABLE sde.map_airport_taxiways ADD nisis_id NUMBER;

SELECT * FROM sde.map_airport_taxiways ORDER BY nisis_id desc;

DECLARE
BEGIN
UPDATE sde.map_airport_taxiways SET nisis_id = seq_taxiway_id.nextval WHERE nisis_id IS NULL;

END;
/

ALTER TABLE sde.map_airport_taxiways ADD (current_status NUMBER DEFAULT 2 NOT NULL, modified_date TIMESTAMP, modified_by VARCHAR2(255), modification_status NUMBER, modification_approved_by VARCHAR2(255), inactive_flg VARCHAR2(1) DEFAULT 'N' NOT NULL);

update sde.map_airport_taxiways set current_status = 2, inactive_flg = 'N';

--rollback;

--update sde.map_airport_taxiways set nisis_id = null;

commit;