-- Note:
-- There were some typos in the database at the time this script was used
-- The uncommented script reflects those typos as they may still be in place later
-- The commented script is intended to match the correcto spelling, which may be in place when this is used

UPDATE nisis_airports_data SET c30_status='YES' WHERE trim(faa_id) IN (SELECT trim(faa_id) from core30_aiports_import); 
UPDATE nisis_airports_data SET c30_status='NO' WHERE trim(faa_id) NOT IN (SELECT trim(faa_id) from core30_aiports_import); 

-- airports is misspelled on the database as aiports

-- UPDATE nisis_airports_data SET c30_status='YES' WHERE trim(faa_id) IN (SELECT trim(faa_id) from core30_airports_import); 
-- UPDATE nisis_airports_data SET c30_status='NO' WHERE trim(faa_id) NOT IN (SELECT trim(faa_id) from core30_airports_import); 

UPDATE nisis_airports_data SET oep35_sts='YES' WHERE trim(faa_id) IN (SELECT trim(faa_id) from oep35_aiports_import); 
UPDATE nisis_airports_data SET oep35_sts='NO' WHERE trim(faa_id) NOT IN (SELECT trim(faa_id) from oep35_aiports_import); 

-- again airports is misspelled as aiports

-- UPDATE nisis_airports_data SET oep35_sts='YES' WHERE trim(faa_id) IN (SELECT trim(faa_id) from oep35_airports_import); 
-- UPDATE nisis_airports_data SET oep35_sts='NO' WHERE trim(faa_id) NOT IN (SELECT trim(faa_id) from oep35_airports_import); 

UPDATE nisis_airports_data SET ops45_sts='YES' WHERE trim(faa_id) IN (SELECT trim(faa_id) from ospnet45_airports_import); 
UPDATE nisis_airports_data SET ops45_sts='NO' WHERE trim(faa_id) NOT IN (SELECT trim(faa_id) from ospnet45_airports_import); 

-- a little different this time, ops is misspelled as osp in the table name (possibly the other way around, I'm not sure)

-- UPDATE nisis_airports_data SET ops45_sts='YES' WHERE trim(faa_id) IN (SELECT trim(faa_id) from ospnet45_airports_import); 
-- UPDATE nisis_airports_data SET ops45_sts='NO' WHERE trim(faa_id) NOT IN (SELECT trim(faa_id) from ospnet45_airports_import); 

UPDATE nisis_airports_data SET ft_avail = (SELECT fuel FROM airports_5010 where loc_id = faa_id); 
