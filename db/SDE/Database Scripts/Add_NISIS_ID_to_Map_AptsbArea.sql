ALTER TABLE sde.map_aptsbarea ADD nisis_id NUMBER;

SELECT * FROM sde.map_aptsbarea ORDER BY nisis_id desc;

DECLARE
BEGIN
UPDATE sde.map_aptsbarea SET nisis_id = seq_area_id.nextval WHERE nisis_id IS NULL;

END;
/

--rollback;

--update sde.map_aptsbarea set nisis_id = null;

commit;