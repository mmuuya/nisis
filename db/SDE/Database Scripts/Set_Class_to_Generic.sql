-- sde.map_nfdc_airports

select distinct class from sde.map_nfdc_airports order by 1 desc;

select count(*), length(class)  from sde.map_nfdc_airports group by length(class) order by 1 desc;

select distinct class from sde.map_nfdc_airports where class like 'APUN%' order by 1 desc;

update sde.map_nfdc_airports set class = 'APUNXX' where class like 'APUN%';

update sde.map_nfdc_airports set class = 'APRXXX' where class like 'APR%';

update sde.map_nfdc_airports set class = 'AMGYXX' where class like 'AMGY%';

update sde.map_nfdc_airports set class = 'AMGNXX' where class like 'AMGN%';

update sde.map_nfdc_airports set class = 'HXXXXX' where class like 'H%';

update sde.map_nfdc_airports set class = 'SXXXXX' where class like 'S%';

select distinct class from sde.map_nfdc_airports  where class not like '%X' order by 1 desc;

-- SDE.NISIS_AIRPORTS

