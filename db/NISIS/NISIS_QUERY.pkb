CREATE OR REPLACE PACKAGE BODY NISIS.NISIS_QUERY AS
/******************************************************************************
-- Name:                nisis.nisis_query
-- Purpose:             This package is for querying information from NISIS-related
                            tables, and returning the values in GeoJSON format.
-- Developer:           Haeden Howland
-- Create Date:         19-Aug-2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/


FUNCTION f_GeoJSON_Airport
/******************************************************************************
-- Name:                GeoJSON_Airport
-- Purpose:             Formats the results of a query for airport details
                         in GeoJSON-readable format
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_query IN CLOB DEFAULT NULL,
 i_full_json IN BOOLEAN DEFAULT FALSE)
RETURN CLOB
IS
l_query         CLOB;

TYPE cur_typ IS REF CURSOR;
TYPE bulk_row IS TABLE OF AIRPORTS%ROWTYPE;
l_rows       bulk_row;
l_cursor        cur_typ;
l_result        CLOB;
malformed_query EXCEPTION;
BEGIN
IF UPPER(i_query) LIKE UPPER('SELECT % FROM AIRPORTS WHERE %;') AND INSTR(i_query, ';') = LENGTH(i_query) THEN
    l_query := TO_CHAR(SUBSTR(i_query,0,LENGTH(i_query)-1)); --drop trailing ; or else dynamic sql won't execute
ELSE 
    RAISE malformed_query;    
END IF;

IF i_full_json THEN
    l_result := '{"type":"FeatureCollection","features":[';
END IF;
OPEN l_cursor FOR l_query;
LOOP
FETCH l_cursor BULK COLLECT INTO l_rows LIMIT 500;
    FOR i IN 1..l_rows.COUNT LOOP
        IF i > 1 THEN 
            l_result := l_result || ',';
        END IF;
        l_result := l_result || TO_CLOB('{"type":"Feature","properties":{"name":"'||l_rows(i).name ||'"},"geometry":{"type":"Point","coordinates":['||
        f_trim_lat_long_digits(l_rows(i).longitude_deg) ||','||f_trim_lat_long_digits(l_rows(i).latitude_deg) ||']}}'); 
    END LOOP;
EXIT WHEN l_cursor%NOTFOUND;
END LOOP;
CLOSE l_cursor;

IF i_full_json THEN
    l_result := l_result || ']}';
END IF;
/*******************************************************************************
example from openlayers.org/dev/examples/vector-formats.html
********************************************************************************
{"type":"Feature", "properties":{}, "geometry":{"type":"Point", "coordinates":
[147.79687285423, 29.20313000679]}, "crs":{"type":"name", "properties":{"name":
"urn:ogc:def:crs:OGC:1.3:CRS84"}}}
********************************************************************************/   

--insert into haeden_test values (sysdate, l_result);   
RETURN l_result;
EXCEPTION
    WHEN malformed_query THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'Malformed Query: ' || SUBSTR(i_query, 0, 3900)); 
        RETURN NULL;       
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
--        te_logging.p_error(SQLCODE, SQLERRM, te_logging.f_get_procedure, NULL);
END f_GeoJSON_Airport;
  
FUNCTION f_GeoJSON_Line
/******************************************************************************
-- Name:                GeoJSON_Line
-- Purpose:             Formats the results of a query for runway details
                         in GeoJSON-readable format
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_query IN CLOB DEFAULT NULL,
 i_full_json IN BOOLEAN DEFAULT FALSE)
RETURN CLOB
IS
l_query         CLOB;
TYPE cur_typ    IS REF CURSOR;
TYPE bulk_row   IS TABLE OF RUNWAYS%ROWTYPE;
l_rows          bulk_row;
l_cursor        cur_typ;
l_result        CLOB;
malformed_query EXCEPTION;
BEGIN

IF UPPER(i_query) LIKE ('SELECT % FROM RUNWAYS WHERE %;') AND INSTR(i_query, ';') = LENGTH(i_query) THEN
    l_query := TO_CHAR(SUBSTR(i_query,0,LENGTH(i_query)-1)); --drop trailing ; or else dynamic sql won't execute
ELSE 
    RAISE malformed_query;
END IF;

IF i_full_json THEN
    l_result := '{"type":"FeatureCollection","features":[';
END IF;
OPEN l_cursor FOR l_query;
LOOP
FETCH l_cursor BULK COLLECT INTO l_rows LIMIT 500;
    FOR i IN 1..l_rows.COUNT LOOP
        IF i > 1 THEN 
            l_result := l_result || ',';
        END IF;
        l_result := l_result || '{"type":"Feature","properties":{"name":"'||l_rows(i).fullname||'"},"geometry":{"type":"LineString","coordinates":[['||
        f_trim_lat_long_digits(l_rows(i).BaseLong)||','||f_trim_lat_long_digits(l_rows(i).BaseLat)||'],['||f_trim_lat_long_digits(l_rows(i).RecLong)||','||
        f_trim_lat_long_digits(l_rows(i).RecLat)||']]}}'; 
    END LOOP;
EXIT WHEN l_cursor%NOTFOUND;
END LOOP;

CLOSE l_cursor;

IF i_full_json THEN
    l_result := l_result || ']}';
END IF;
/*******************************************************************************
example from openlayers.org/dev/examples/vector-formats.html
********************************************************************************
{"type":"Feature", "properties":{}, "geometry":{"type":"LineString", "coordinates":
[[-112.35937714577, 35.53125500679], [-85.640627145767, 42.56250500679]]}, "crs":
{"type":"name", "properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}}
********************************************************************************/   

--insert into haeden_test values (sysdate, l_result);   
RETURN l_result;
EXCEPTION
    WHEN malformed_query THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'Malformed Query: ' || SUBSTR(i_query, 0, 3900)); 
        RETURN NULL;          
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
--        te_logging.p_error(SQLCODE, SQLERRM, te_logging.f_get_procedure, NULL);
END f_GeoJSON_Line;

FUNCTION f_GeoJSON_Polygon
/******************************************************************************
-- Name:                GeoJSON_Polygon
-- Purpose:             Formats the results of a query for polygon details
                         in GeoJSON-readable format
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_query IN CLOB DEFAULT NULL,
 i_full_json IN BOOLEAN DEFAULT FALSE)
RETURN CLOB
IS
l_query         CLOB;
TYPE cur_typ    IS REF CURSOR;
TYPE bulk_row   IS TABLE OF SDE.MAP_APTSBAREA%ROWTYPE;
l_rows          bulk_row;
l_cursor        cur_typ;
l_result        CLOB;
l_geometry      SDE.ST_GEOMETRY;
l_polygon       CLOB;
malformed_query EXCEPTION;
l_temp          VARCHAR2(100);

l_start         NUMBER := 0;
l_end           NUMBER := 0;
l_count         NUMBER := 0;
BEGIN

IF UPPER(i_query) LIKE ('SELECT % FROM SDE.MAP_APTSBAREA WHERE %;') AND INSTR(i_query, ';') = LENGTH(i_query) THEN
    l_query := TO_CHAR(SUBSTR(i_query,0,LENGTH(i_query)-1)); --drop trailing ; or else dynamic sql won't execute
ELSE 
    RAISE malformed_query;
END IF;

IF i_full_json THEN
    l_result := '{"type":"FeatureCollection","features":[';
END IF;
OPEN l_cursor FOR l_query;
LOOP
FETCH l_cursor BULK COLLECT INTO l_rows LIMIT 500;
    FOR i IN 1..l_rows.COUNT LOOP
        IF i > 1 THEN 
            l_result := l_result || ',';
        END IF;
        l_result := l_result || '{"type":"Feature","properties":{"name":"'||l_rows(i).polygon_nm||'"},"geometry":{"type":"Polygon","coordinates":[[';
            l_geometry := l_rows(i).shape;
            SELECT sde.st_astext(sde.st_transform(l_geometry, 4326, sde.st_srid(l_geometry))) INTO l_polygon FROM dual;
            l_start := instr(lower(l_polygon), 'polygon  (( ', 0);
            l_count := 0;
            WHILE l_end+1 < length(l_polygon) LOOP
                IF l_start = 0 THEN 
                    l_start := 13; --add 13 spaces for string "POLYGON  (( "
                    l_end := instr(l_polygon, ',', l_start);
                ELSE
                    l_start := instr(l_polygon, ',', l_end)+2;                    
                    l_end := instr(l_polygon, ',', l_start);
                    IF l_end =0 THEN
                        l_end := length(l_polygon)-1; --subtract 1 to trim off closing parenthesis
                    END IF;
                END IF;
                
                IF l_count > 0 THEN 
                    l_result := l_result || ',';
                END IF;
                l_temp := substr(l_polygon, l_start, l_end-l_start);
                l_result := l_result || '[' || replace(l_temp, ' ', ',') || ']';
                
                l_count := l_count + 1;
            END LOOP;
        l_result := l_result || ']]}}';
    END LOOP;
EXIT WHEN l_cursor%NOTFOUND;
END LOOP;

CLOSE l_cursor;

IF i_full_json THEN
    l_result := l_result || ']}';
END IF;
--insert into haeden_test values (sysdate, 'start: '|| l_start || CHR(13) || 'end: ' || l_end || CHR(13) || l_polygon || CHR(13) || l_result);
/*******************************************************************************
example from openlayers.org/dev/examples/vector-formats.html
********************************************************************************
{"type":"Feature", "properties":{}, "geometry":{"type":"LineString", "coordinates":
[[-112.35937714577, 35.53125500679], [-85.640627145767, 42.56250500679]]}, "crs":
{"type":"name", "properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}}
********************************************************************************/   

--insert into haeden_test values (sysdate, l_result);   
RETURN l_result;
EXCEPTION
    WHEN malformed_query THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'Malformed Query: ' || SUBSTR(i_query, 0, 3900)); 
        RETURN NULL;         
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
END f_GeoJSON_Polygon;

FUNCTION f_trim_lat_long_digits
/******************************************************************************
-- Name:                f_trim_lat_long_digits
-- Purpose:             Takes a Latitude or Longitude format input, and reduces the 
                            precision of the decimal points by l_points_after_decimal
                            for ease of processing.
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_lat_long IN VARCHAR2)
RETURN VARCHAR2
IS
l_points_after_decimal NUMBER := 3;
l_result        VARCHAR2(10);
BEGIN
    l_result := SUBSTR(i_lat_long, 0, instr(i_lat_long, '.')+l_points_after_decimal);    
    RETURN l_result;
EXCEPTION    
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
END f_trim_lat_long_digits;


FUNCTION f_search
/******************************************************************************
-- Name:                f_search
-- Purpose:             Accepts a search term, and executes a preliminary search 
                            on NISIS tables to return possible matches. Then performs
                            the GeoJSON functions to return a GeoJSON clob of the
                            resulting location
-- Developer:           Haeden Howland
-- Create Date:         8/29/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_search   IN  VARCHAR2,
 i_airport  IN  BOOLEAN DEFAULT TRUE,
 i_runway   IN  BOOLEAN DEFAULT TRUE,
 i_area  IN  BOOLEAN DEFAULT TRUE)
RETURN  CLOB
IS
TYPE search_record IS REF CURSOR;
 

airport_search  search_record;
runway_search   search_record;
area_search     search_record;

l_return        CLOB;
l_section       CLOB;

l_id_column     VARCHAR2(63);
l_id_number     VARCHAR2(63);
l_id_num_rw     NUMBER;
l_name          VARCHAR2(100);
l_icao          VARCHAR2(63);       
l_search        VARCHAR2(100); 
l_record_count  NUMBER := 0;
l_count         NUMBER := 0;    
BEGIN

l_return := '{"type":"FeatureCollection","features":[';
l_search := LOWER(i_search);

IF i_airport THEN
    l_count := 0;
    OPEN airport_search FOR
        SELECT ident icao,
            name name,
            'ID' id_column,
            id id_number
        FROM nisis.airports
        WHERE (LOWER(ident) like '%'||l_search||'%'
            OR LOWER(name) like '%'||l_search||'%'
            OR LOWER(state) like '%'||l_search||'%'
            OR LOWER(municipality) like '%'||l_search||'%')
        ; 
    LOOP
        FETCH airport_search INTO l_icao, l_name, l_id_column, l_id_number;
        EXIT WHEN airport_search%NOTFOUND;
        IF l_count > 0 THEN
            l_return := l_return || ',';
        END IF;
        l_return := l_return || f_GeoJSON_Airport('SELECT * FROM airports WHERE '|| l_id_column || ' = ' || l_id_number || ';');
        l_count := l_count + 1;
    END LOOP; 
    CLOSE airport_search;
END IF;

IF i_runway THEN
    l_count := 0;
    OPEN runway_search FOR
        SELECT locid icao,
            fullname name,
            'OBJECTID' id_column,
            objectid id_number
        FROM nisis.runways
        WHERE (LOWER(locid) like '%'||l_search||'%'
            OR LOWER(fullname) like '%'||l_search||'%'
            OR LOWER(faa_st) like '%'||l_search||'%')
        ;
         
        IF i_airport THEN
            l_return := l_return || ',';
        END IF;
    LOOP
        FETCH runway_search INTO l_icao, l_name, l_id_column, l_id_num_rw; 
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'ROWCOUNT: ' || runway_search%ROWCOUNT);      
        EXIT WHEN runway_search%NOTFOUND;       
        IF l_count > 0 THEN
            l_return := l_return || ',';
        END IF;      
        l_return := l_return || f_GeoJSON_Line('SELECT * FROM runways WHERE '|| l_id_column || ' = ' || l_id_num_rw || ';');
        l_count := l_count + 1;
    END LOOP; 
    -- remove the extraneous comma if empty cursor returned.
    IF runway_search%ROWCOUNT = 0 THEN
        l_return := dbms_lob.substr(l_return, length(l_return)-1);     END IF;
    CLOSE runway_search;
END IF;

IF i_area THEN
    l_count := 0;
    OPEN area_search FOR
        SELECT polygon_nm name,
            'OBJECTID' id_column,
            objectid id_number
        FROM sde.map_aptsbarea
        WHERE (LOWER(polygon_nm) like '%'||l_search||'%'
            OR LOWER(state_nm) like '%'||l_search||'%')
        ; 
        IF (i_airport OR i_runway) THEN
            l_return := l_return || ',';
        END IF;
    LOOP
        FETCH area_search INTO l_name, l_id_column, l_id_number;
        EXIT WHEN area_search%NOTFOUND;
        IF l_count > 0 THEN
            l_return := l_return || ',';
        END IF;
        l_return := l_return || f_GeoJSON_Polygon('SELECT * FROM sde.map_aptsbarea WHERE '|| l_id_column || ' = ' || l_id_number || ';');
        l_count := l_count + 1;
    END LOOP; 
     -- remove the extraneous comma if empty cursor returned.
     IF area_search%ROWCOUNT = 0 THEN
        l_return := dbms_lob.substr(l_return, length(l_return)-1);
    END IF;
    CLOSE area_search;
END IF;

l_return := l_return || ']}';

RETURN l_return;

EXCEPTION    
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
END f_search;


END NISIS_QUERY;
/