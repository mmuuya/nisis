CREATE OR REPLACE PACKAGE NISIS.NISIS_DML AS
/******************************************************************************
-- Name:                nisis.nisis_dml
-- Purpose:             This package is for nisis Data Markup Language (DML) 
                            operations.
-- Developer:           Haeden Howland
-- Create Date:         06-Sep-2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/

SUBTYPE airports_rt IS NISIS.AIRPORTS%ROWTYPE;
SUBTYPE runways_rt IS NISIS.RUNWAYS%ROWTYPE;
TYPE l_cur IS REF CURSOR;

FUNCTION f_new_id
/******************************************************************************
-- Name:            f_new_id
-- Purpose:         Accepts a feature type and (optional) parent ID, or default ID
                        as input and returns a formatted feature id.
-- Developer:       Haeden Howland
-- Create Date:     09/06/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_feature_type     VARCHAR2,
 i_parent_id        VARCHAR2 DEFAULT NULL,
 i_icao_id          VARCHAR2 DEFAULT NULL)
RETURN VARCHAR2;   

PROCEDURE p_add_feature_and_code
/******************************************************************************
-- Name:            p_add_feature_and_code
-- Purpose:         Accepts a full name and code pair and generates a new entry
                        in the feature_codes table
-- Developer:       Haeden Howland
-- Create Date:     09/06/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_feature_name     VARCHAR2,
 i_code             VARCHAR2);
 
FUNCTION f_merge_airports
/******************************************************************************
-- Name:            f_merge_airports
-- Purpose:         Handles the database interaction of adding/updating Airport
                        information via a merge statement.
-- Developer:       Haeden Howland
-- Create Date:     09/11/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_id                       VARCHAR2 DEFAULT NULL,
 i_ident                    VARCHAR2 DEFAULT NULL,
 i_type                     VARCHAR2 DEFAULT NULL,
 i_name                     VARCHAR2 DEFAULT NULL,
 i_latitude_deg             VARCHAR2 DEFAULT NULL,
 i_longitude_deg            VARCHAR2 DEFAULT NULL,
 i_elevation_ft             VARCHAR2 DEFAULT NULL,
 i_continent                VARCHAR2 DEFAULT NULL,
 i_iso_country              VARCHAR2 DEFAULT NULL,
 i_iso_region               VARCHAR2 DEFAULT NULL,
 i_municipality             VARCHAR2 DEFAULT NULL,
 i_state                    VARCHAR2 DEFAULT NULL,
 i_nisis_id                 NUMBER DEFAULT NULL,
 i_current_status           NUMBER DEFAULT NULL,
 i_modified_date            TIMESTAMP DEFAULT NULL,
 i_modified_by              VARCHAR2 DEFAULT NULL,
 i_modification_status      NUMBER DEFAULT NULL,
 i_modification_approved_by VARCHAR2 DEFAULT NULL,
 i_inactive_flg             VARCHAR2 DEFAULT NULL)
-- RETURN airports_rt;
RETURN BOOLEAN; 

PROCEDURE p_merge_aixm_airports
/******************************************************************************
-- Name:            p_merge_aixm_airports
-- Purpose:         Handles the database interaction of adding/updating Airport
                        information based off the 56-day AIXM load via a merge 
                        statement.
-- Developer:       Haeden Howland
-- Create Date:     10/7/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ ;

FUNCTION f_merge_runways
/******************************************************************************
-- Name:            f_merge_runways
-- Purpose:         Handles the database interaction of adding/updating Runway
                        information via a merge statement.
-- Developer:       Haeden Howland
-- Create Date:     09/12/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_ogr_fid                  INTEGER DEFAULT NULL,
i_ora_geometry              SDO_GEOMETRY  DEFAULT NULL,
i_objectid                  NUMBER  DEFAULT NULL,
i_locid                     VARCHAR2  DEFAULT NULL,
i_siteno                    VARCHAR2  DEFAULT NULL,
i_fullname                  VARCHAR2  DEFAULT NULL,
i_faa_st                    VARCHAR2  DEFAULT NULL,
i_id                        VARCHAR2  DEFAULT NULL,
i_length                    VARCHAR2  DEFAULT NULL,
i_width                     VARCHAR2  DEFAULT NULL,
i_srfctype                  VARCHAR2  DEFAULT NULL,
i_srfctrtmnt                VARCHAR2  DEFAULT NULL,
i_paveclas                  VARCHAR2  DEFAULT NULL,
i_rwylghts                  VARCHAR2  DEFAULT NULL,
i_stfips                    VARCHAR2  DEFAULT NULL,
i_stpostal                  VARCHAR2  DEFAULT NULL,
i_baselat                   NUMBER  DEFAULT NULL,
i_baselong                  NUMBER  DEFAULT NULL,
i_reclat                    NUMBER  DEFAULT NULL,
i_reclong                   NUMBER  DEFAULT NULL,
i_version                   VARCHAR2  DEFAULT NULL,
i_shape_leng                NUMBER  DEFAULT NULL,
i_nisis_id                  NUMBER DEFAULT NULL,
i_current_status            NUMBER DEFAULT NULL,
--i_modified_date             TIMESTAMP(6),
i_modified_by               VARCHAR2  DEFAULT NULL,
i_modification_status       NUMBER DEFAULT NULL,
i_modification_approved_by  VARCHAR2  DEFAULT NULL,
i_inactive_flg              VARCHAR2  DEFAULT NULL
)
RETURN BOOLEAN; 

PROCEDURE f_auto_complete
/******************************************************************************
-- Name:            f_autocomplete
-- Purpose:         Accepts a string input, and returns possible matches.
-- Developer:       Haeden Howland
-- Create Date:     10/11/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/
(i_input        VARCHAR2)
--RETURN l_cur
;


END NISIS_DML;
/