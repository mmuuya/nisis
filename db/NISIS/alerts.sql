--------------------------------------
--------------------------------------
-- CREATE SEQUENCES
--------------------------------------
--------------------------------------
DROP SEQUENCE NISIS.ALERTS_SEQ;
CREATE SEQUENCE NISIS.ALERTS_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


--------------------------------------
--------------------------------------
-- CREATE TABLES
--------------------------------------
--------------------------------------
DROP TABLE NISIS.ALERTS;

CREATE TABLE NISIS.ALERTS
(
  ALERTID         NUMBER,
  OBJECTID        INTEGER,
  FAC_ID          VARCHAR2(10 BYTE),
  STATUS_TYPE_ID  NUMBER,
  CURRENT_STATUS  NUMBER,
  FUTURE_STATUS   NUMBER,
  ALERT_DATE      DATE,
  CREATED_BY      NUMBER(20),
  CREATED_DATE    DATE            DEFAULT SYSDATE,
  MODIFIED_BY     NUMBER(20), 
  MODIFIED_DATE   DATE,
  INACTIVATED_BY  NUMBER(20), 
  INACTIVATED_DATE    DATE,
  ACTIVE          VARCHAR2(1 BYTE)
);


CREATE UNIQUE INDEX NISIS.ALARM_PK ON NISIS.ALERTS
(ALERTID);


CREATE OR REPLACE TRIGGER NISIS.ALERTS_TRG
BEFORE INSERT
ON NISIS.ALERTS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
-- For Toad:  Highlight column ALERTID
  :new.ALERTID := ALERTS_SEQ.nextval;
END ALERTS_TRG;
/


ALTER TABLE NISIS.ALERTS ADD (
  CONSTRAINT ALARM_PK
  PRIMARY KEY
  (ALERTID)
  USING INDEX NISIS.ALARM_PK
  ENABLE VALIDATE);


--------------------------------------
--------------------------------------
-- CREATING PICKLIST AND PICKLIST_ITEMS TABLE
--------------------------------------
--------------------------------------
CREATE TABLE NISIS.PICKLIST
(
  PICKLIST     NUMBER,
  DESCRIPTION  VARCHAR2(100 BYTE)
);

CREATE TABLE NISIS.PICKLIST_ITEMS
(
  PICKLIST  NUMBER,
  VALUE     NUMBER,
  LABEL     VARCHAR2(200 BYTE)
);

--------------------------------------
--------------------------------------
-- CREATING FAC_STATUS_TYPE TABLE
--------------------------------------
--------------------------------------

DROP TABLE NISIS.FAC_STATUS_TYPE
CREATE TABLE NISIS.FAC_STATUS_TYPE
(
  STATUS_ID        NUMBER                       NOT NULL,
  STATUS_COLUMN    VARCHAR2(30 BYTE)             NOT NULL,
  STATUS_FULLNAME  VARCHAR2(100 BYTE),
  FAC_TYPE         VARCHAR2(3 BYTE)
);


CREATE UNIQUE INDEX NISIS.NISIS_FAC_ALARM_LOOKUP_PK ON NISIS.FAC_STATUS_TYPE
(STATUS_ID);


ALTER TABLE NISIS.FAC_STATUS_TYPE ADD (
  CONSTRAINT NISIS_FAC_ALARM_LOOKUP_PK
  PRIMARY KEY
  (STATUS_ID)
  USING INDEX NISIS.NISIS_FAC_ALARM_LOOKUP_PK
  ENABLE VALIDATE);

--------------------------------------
--------------------------------------
-- CREATING ALERT_GROUP TABLE
--------------------------------------
--------------------------------------
DROP TABLE NISIS.ALERT_GROUP;

CREATE TABLE NISIS.ALERT_GROUP
(
  ALERTID  NUMBER                               NOT NULL,
  GROUPID  NUMBER                               NOT NULL
);
--------------------------------------
--------------------------------------
-- ALTER TABLES
--------------------------------------
--------------------------------------

ALTER TABLE NISIS.ALERTS DROP COLUMN ACTION
/

ALTER TABLE NISIS.ALERTS DROP COLUMN GROUPS
/

ALTER TABLE NISIS.ALERTS DROP COLUMN DISPLAYED
/

ALTER TABLE NISIS.ALERTS
MODIFY(MODIFIED_DATE  DEFAULT NULL);
/

ALTER TABLE NISIS.ALERTS
MODIFY(INACTIVATED_DATE  DEFAULT NULL);
/

ALTER TABLE NISIS.FAC_STATUS_TYPE
RENAME COLUMN STATUS_CODE TO STATUS_COLUMN;

ALTER TABLE NISIS.FAC_STATUS_TYPE
MODIFY(STATUS_COLUMN VARCHAR2(30 BYTE));

ALTER TABLE NISIS.FAC_STATUS_TYPE
 ADD (PICKLIST  NUMBER);
--------------------------------------
--------------------------------------
-- (RE)CREATE VIEWS
--------------------------------------
--------------------------------------
CREATE OR REPLACE FORCE VIEW NISIS.ALERT_VW
(
   ALERTID,
   OBJECTID,
   FAC_ID,
   FAC_TYPE,
   STATUS_TYPE_ID,
   STATUS_FULLNAME,
   CURRENT_STATUS,
   FUTURE_STATUS,
   ALERT_DATE,
   CREATED_BY,
   CREATED_DATE,
   ACTIVE
)
AS
   SELECT ALERTID,
          OBJECTID,
          FAC_ID,
          FAC_TYPE,
          STATUS_TYPE_ID,
          STATUS_FULLNAME,
          CURRENT_STATUS,
          FUTURE_STATUS,
          ALERT_DATE,
          CREATED_BY,
          CREATED_DATE,
          ACTIVE
     FROM NISIS.ALERTS, NISIS.FAC_STATUS_TYPE_VW
    WHERE     ALERTS.STATUS_TYPE_ID = FAC_STATUS_TYPE_VW.STATUS_ID
          AND ACTIVE = 'Y'
          AND CURRENT_STATUS != FUTURE_STATUS;

CREATE OR REPLACE FORCE VIEW NISIS.ALERT_GROUP_NM_TMP
(
   ALERTID,
   GROUPID,
   NAME
)
AS
   SELECT a.alertid, a.groupid, g.name
     FROM alert_group a JOIN nisis_admin.groups g ON a.groupid = g.groupid;

CREATE OR REPLACE FORCE VIEW NISIS.USER_ALERTS_VW
(
   REF_ALERTID,
   ALERT_GROUPID,
   NISIS_ADMIN.GROUPS_GROUPID,
   NAME,
   USERID
)
AS
   SELECT a.alertid ref_alertid,
          --a.objectid ref_objectid,
          ag.groupid alert_groupid,
          g.groupid NISIS_ADMIN.groups_groupid,
          g.name name,
          u.userid userid
     FROM nisis_admin.user_group u
          JOIN nisis_admin.groups g ON u.groupid = g.groupid
          JOIN nisis.alert_group ag ON ag.groupid = g.groupid
          JOIN nisis.alerts a ON a.alertid = ag.alertid;

GRANT SELECT ON NISIS_ADMIN.USER_GROUP TO NISIS;



--------------------------------------
--------------------------------------
-- FUNCTIONS AND PROCEDURES
--------------------------------------
--------------------------------------

CREATE OR REPLACE FUNCTION NISIS.insert_alert (
   in_objid          IN alerts.objectid%TYPE
  ,in_facid          IN alerts.fac_id%TYPE
  ,in_statustypeid   IN alerts.status_type_id%TYPE
  ,in_status         IN alerts.current_status%TYPE
  ,in_futstatus      IN alerts.future_status%TYPE
  ,in_alertdate      IN VARCHAR2
  ,in_qformat        IN VARCHAR2
  ,in_createdby      IN alerts.created_by%TYPE
  ,in_active         IN alerts.active%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO alerts (
                  objectid
                 ,fac_id
                 ,status_type_id
                 ,current_status
                 ,future_status
                 ,alert_date
                 ,created_by
                 ,active
               )
   VALUES (in_objid
          ,in_facid
          ,in_statustypeid
          ,in_status
          ,in_futstatus
          ,TO_DATE (in_alertdate, in_qformat)
          ,in_createdby
          ,in_active)
   RETURNING alertid
     INTO v;

   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN -1;
END insert_alert;
/




--------------------------------------
--------------------------------------
-- (RE)CREATE VIEWS
--------------------------------------
--------------------------------------


--------------------------------------
--------------------------------------
-- INSERT FAC_STATUS_TYPE VALUES
--------------------------------------
--------------------------------------

Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (18, 'ANS_STATUS', 'OVERALL ANS SYSTEMS STATUS', 'ANS');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (19, 'BPWR_STS', 'ANS BACKUP POWER STATUS', 'ANS');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (1, 'OVR_STATUS', 'AIRPORT OVERALL STATUS', 'APT');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (2, 'AOO_STATUS', 'AVIATION OPERATOR OPS STATUS', 'APT');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (3, 'OOO_STATUS', 'OWNER/OPERATOR OPS STATUS', 'APT');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (4, 'TSA_STATUS', 'TSA SCREENING OPS STATUS', 'APT');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (5, 'ANS_STATUS', 'OVERALL ANS SYSTEMS STATUS', 'APT');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (6, 'OPS_STATUS', 'ATM OPS STATUS', 'ATM');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (7, 'PWR_STS', 'ATM POWER STATUS', 'ATM');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (8, 'OPS_SL', 'ATM OPS STAFFING LEVEL', 'ATM');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (9, 'ATO_PA_STS', 'PERSONNEL ACCOUNTING STATUS', 'ATM');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (10, 'CRT_RL', 'CURRENT READINESS LEVEL', 'ATM');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (11, 'CRT_SL', 'CURRENT SECON LEVEL', 'ATM');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (12, 'BOPS_STS', 'BASIC OPERATING STATUS', 'OSF');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (13, 'AOS_LVL', 'STAFFING LEVEL', 'OSF');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (14, 'AOPA_4S', 'PERSONNEL ACCOUNTING STATUS', 'OSF');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (15, 'BPWR_STS', 'BACKUP POWER STATUS', 'OSF');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (16, 'CRT_R_LVL', 'CURRENT READINESS LEVEL', 'OSF');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (17, 'CRT_S_LVL', 'CURRENT SECON LEVEL', 'OSF');
Insert into NISIS.FAC_STATUS_TYPE
   (status_id, status_column, status_fullname, fac_type)
 Values
   (20, 'RRS', 'RESOURCE STATUS', 'RR');

--------------------------------------
--------------------------------------
-- INSERT PICKLIST VALUES
--------------------------------------
--------------------------------------
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (1, 'YES-NO OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (2, 'OPS STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (3, 'FUNCTION STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (4, 'OPS STATUS OPTIONS ALT');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (5, 'STAFFING LEVEL OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (6, 'PERSONNEL ACCOUNT OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (7, 'READINESS LEVEL OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (8, 'SECON LEVEL OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (9, 'ATC STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (10, 'SERVICE OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (11, 'APT STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (12, 'BACKUP POWER STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (13, 'EG POWER STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (14, 'SIGNIFICANT IMPACT OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (15, 'TSA STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (16, 'SUPPLIES STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (17, 'RESOURCE STATUS OPTIONS');

--------------------------------------
--------------------------------------
-- INSERT PICKLIST_ITEMS VALUES
--------------------------------------
--------------------------------------

Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (1, 0, 'NO');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (1, 1, 'YES');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 0, 'Normal Operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 1, 'Limited/Degraded Operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 2, 'Closed');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 0, 'Normal Functioning');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 1, 'Limited/Degraded Functioning');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 2, 'Out of Service');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 0, 'OTS due to PM');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 1, 'OTS due to Preventive');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 2, 'Shutdown');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 3, 'Working');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 4, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 0, 'Fully Staffed');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 1, 'Limited Staffing');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 2, 'Critically Low Staffing');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 3, 'No Staffing Available');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 0, 'All Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 1, 'Some Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 2, 'Few Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 3, 'None Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 0, 'RL-Alpha');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 1, 'RL-Bravo');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 2, 'RL-Charlie');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 3, 'RL-Delta');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 0, 'SECON-Green');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 1, 'SECON-Blue');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 2, 'SECON-Orange');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 3, 'SECON-Yellow');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 4, 'SECON-Red');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 5, 'N/A');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 0, 'Normal Operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 1, 'ATC Alert');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 2, 'ATC Limited');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 3, 'ATC 0');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 4, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 0, 'Normal Services');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 1, 'Limited Services');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 2, 'No Services');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 0, 'New Airport');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 1, 'Open with normal operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 2, 'Open with limitations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 3, 'Closed for normal operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 4, 'Closed for all operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 5, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (12, 0, 'Standby');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (12, 1, 'In use');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (12, 2, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (13, 0, 'Main Power');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (13, 1, 'Disrupted Power/No EG');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (13, 2, 'Disrupted Power/Using EG');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 0, 'No Known Significant Impact');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 1, 'Somewhat Significant Impact');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 2, 'Widespread Significant Impact');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 3, 'None Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 0, 'Normal Throughput');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 1, 'Constrained Capacity');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 2, 'Screening Suspended');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 0, 'Normal Supplies');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 1, 'Limited Supplies');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 2, 'No Supplies');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (17, 0, 'Resource Status');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (17, 1, 'Other');
