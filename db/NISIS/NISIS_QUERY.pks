CREATE OR REPLACE PACKAGE NISIS.NISIS_QUERY AS
/******************************************************************************
-- Name:                nisis.nisis_query
-- Purpose:             This package is for querying information from NISIS-related
                            tables, and returning the values in GeoJSON format.
-- Developer:           Haeden Howland
-- Create Date:         19-Aug-2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/


FUNCTION f_GeoJSON_Airport
/******************************************************************************
-- Name:                GeoJSON_Airport
-- Purpose:             Formats the results of a query for airport details
                         in GeoJSON-readable format
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_query IN CLOB DEFAULT NULL,
 i_full_json IN BOOLEAN DEFAULT FALSE)
RETURN CLOB;
  
FUNCTION f_GeoJSON_Line
/******************************************************************************
-- Name:                GeoJSON_Line
-- Purpose:             Formats the results of a query for runway details
                         in GeoJSON-readable format
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_query IN CLOB DEFAULT NULL,
 i_full_json IN BOOLEAN DEFAULT FALSE)
RETURN CLOB;

FUNCTION f_GeoJSON_Polygon
/******************************************************************************
-- Name:                GeoJSON_Polygon
-- Purpose:             Formats the results of a query for airport details
                         in GeoJSON-readable format
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_query IN CLOB DEFAULT NULL,
 i_full_json IN BOOLEAN DEFAULT FALSE)
RETURN CLOB;

FUNCTION f_trim_lat_long_digits
/******************************************************************************
-- Name:                f_trim_lat_long_digits
-- Purpose:             Takes a Latitude or Longitude format input, and reduces the 
                            precision of the decimal points by l_points_after_decimal
                            for ease of processing.
-- Developer:           Haeden Howland
-- Create Date:         8/19/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_lat_long IN VARCHAR2)
RETURN VARCHAR2;

FUNCTION f_search
/******************************************************************************
-- Name:                f_search
-- Purpose:             Accepts a search term, and executes a preliminary search 
                            on NISIS tables to return possible matches. Then performs
                            the GeoJSON functions to return a GeoJSON clob of the
                            resulting location
-- Developer:           Haeden Howland
-- Create Date:         8/29/2013
-- Modification History:
--
--  Date        Developer Name      Description
-- MM/DD/YYYY     none            <Version or description>
******************************************************************************/
(i_search   IN  VARCHAR2,
 i_airport  IN  BOOLEAN DEFAULT TRUE,
 i_runway   IN  BOOLEAN DEFAULT TRUE,
 i_area  IN  BOOLEAN DEFAULT TRUE)
RETURN  CLOB;

END NISIS_QUERY;
/