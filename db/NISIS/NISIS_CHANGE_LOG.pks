CREATE OR REPLACE PACKAGE NISIS.NISIS_CHANGE_LOG AS
/******************************************************************************
-- Name:                nisis.nisis_change_log
-- Purpose:             This package is for creating, storing, and querying any
                            changes of features or attributes.
-- Developer:           Haeden Howland
-- Create Date:         06-Sep-2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/

TYPE change_log_cursor IS REF CURSOR;

PROCEDURE p_log_event
/******************************************************************************
-- Name:            p_log_event
-- Purpose:         Adds an entry into the Change_Log table. It accepts a Feature ID,
                        Username, Action Fired and Notes (optional) as parameters.
-- Developer:       Haeden Howland
-- Create Date:     09/09/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/
(i_feature_id   VARCHAR2, 
 i_username     VARCHAR2,
 i_action       VARCHAR2,
 i_notes        VARCHAR2 DEFAULT NULL);

FUNCTION f_query_events
/******************************************************************************
-- Name:            f_query_events
-- Purpose:         Returns a cursor of all events relating to a specific feature
-- Developer:       Haeden Howland
-- Create Date:     09/09/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/
(i_feature_id   VARCHAR2)
RETURN change_log_cursor;

END NISIS_CHANGE_LOG;
/