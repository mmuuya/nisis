CREATE OR REPLACE PACKAGE BODY NISIS.NISIS_CHANGE_LOG AS
/******************************************************************************
-- Name:                nisis.nisis_change_log
-- Purpose:             This package is for creating, storing, and querying any
                            changes of features or attributes.
-- Developer:           Haeden Howland
-- Create Date:         06-Sep-2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/

PROCEDURE p_log_event
/******************************************************************************
-- Name:            p_log_event
-- Purpose:         Adds an entry into the Change_Log table. It accepts a Feature ID,
                        Username, Action Fired and Notes (optional) as parameters.
-- Developer:       Haeden Howland
-- Create Date:     09/09/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/
(i_feature_id   VARCHAR2, 
 i_username     VARCHAR2,
 i_action       VARCHAR2,
 i_notes        VARCHAR2 DEFAULT NULL)
IS

BEGIN

INSERT INTO NISIS.CHANGE_LOG VALUES (SEQ_CHANGE_LOG_ID.nextval, sysdate, i_feature_id, i_username, i_action, i_notes);

EXCEPTION
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
END p_log_event; 

FUNCTION f_query_events
/******************************************************************************
-- Name:            f_query_events
-- Purpose:         Returns a cursor of all events relating to a specific feature
-- Developer:       Haeden Howland
-- Create Date:     09/09/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/
(i_feature_id   VARCHAR2)
RETURN change_log_cursor
IS
l_rt_change_log change_log_cursor;

BEGIN

OPEN l_rt_change_log FOR
    SELECT LOG_ID, EVENT_DATE, FEATURE_ID, USERNAME, ACTION, NOTES
    FROM NISIS.CHANGE_LOG WHERE
    feature_id = i_feature_id
    ORDER BY event_date DESC;
CLOSE l_rt_change_log;

RETURN l_rt_change_log;
EXCEPTION
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
END f_query_events; 

END NISIS_CHANGE_LOG;
/