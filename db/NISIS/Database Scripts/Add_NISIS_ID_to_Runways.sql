ALTER TABLE nisis.runways ADD nisis_id NUMBER;

SELECT * FROM nisis.runways ORDER BY nisis_id desc;

DECLARE
BEGIN
UPDATE NISIS.runways SET nisis_id = seq_runways_id.nextval WHERE nisis_id IS NULL;

END;
/

ALTER TABLE nisis.runways ADD (current_status NUMBER DEFAULT 2 NOT NULL, modified_date TIMESTAMP, modified_by VARCHAR2(255), modification_status NUMBER, modification_approved_by VARCHAR2(255), inactive_flg VARCHAR2(1) DEFAULT 'N' NOT NULL);

update nisis.runways set current_status = 2, inactive_flg = 'N';

--rollback;

--update nisis.runways set nisis_id = null;

commit;