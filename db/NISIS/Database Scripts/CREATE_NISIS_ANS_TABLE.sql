DROP TABLE NISIS.NISIS_ANS_DATA_V2 CASCADE CONSTRAINTS;

CREATE TABLE NISIS.NISIS_ANS_DATA_V2
(
  ANS_ID      VARCHAR2(10 BYTE),
  FSEP_ID     VARCHAR2(10 BYTE),
  BASIC_TP    VARCHAR2(100 BYTE),
  ASSOC_A_RW  VARCHAR2(100 BYTE),
  USE_STATUS  VARCHAR2(100 BYTE),
  BASIC_CAT   VARCHAR2(100 BYTE),
  DESCRIPT    VARCHAR2(100 BYTE),
  RESTO_REQ   NUMBER,
  PWR_CD      VARCHAR2(100 BYTE),
  RTOF_FAC    VARCHAR2(100 BYTE),
  LATITUDE    NUMBER,
  LONGITUDE   NUMBER,
  T_OPS_DIST  VARCHAR2(100 BYTE),
  MTNCE_RESP  VARCHAR2(100 BYTE),
  REF_PC      VARCHAR2(100 BYTE),
  ASSOC_CITY  VARCHAR2(100 BYTE),
  ASSOC_ST    VARCHAR2(100 BYTE),
  ASSOC_CNTY  VARCHAR2(100 BYTE),
  TIME_ZONE   VARCHAR2(100 BYTE),
  FAA_REG     VARCHAR2(100 BYTE),
  ATO_SA      VARCHAR2(100 BYTE),
  FEMA_REG    VARCHAR2(100 BYTE),
  RESP_SU     VARCHAR2(100 BYTE),
  OWNERSHIP   VARCHAR2(100 BYTE),
  REL_ANSS    VARCHAR2(100 BYTE),
  SUP_ARPS    VARCHAR2(100 BYTE),
  SUP_ATMS    VARCHAR2(100 BYTE),
  SUP_RWYS    VARCHAR2(100 BYTE),
  SUP_INSTP   VARCHAR2(100 BYTE),
  ANS_STATUS  INTEGER,
  ANS_STS_AD  VARCHAR2(100 BYTE),
  ANS_STS_EX  VARCHAR2(100 BYTE),
  ANS_STS_SN  VARCHAR2(100 BYTE),
  ANS_STS_AL  DATE,
  RMLS_REP    VARCHAR2(100 BYTE),
  EG_UPS_SUP  VARCHAR2(100 BYTE),
  BPWR_TYPE   VARCHAR2(100 BYTE),
  BPWR_STS    VARCHAR2(100 BYTE),
  BPWR_USTS   VARCHAR2(100 BYTE),
  BPWR_S_EX   VARCHAR2(100 BYTE),
  BPWR_S_SN   VARCHAR2(100 BYTE),
  BPWR_S_AL   VARCHAR2(100 BYTE),
  FS_INTER    VARCHAR2(100 BYTE),
  FS_MBER     VARCHAR2(100 BYTE),
  IWL_DSGN    VARCHAR2(100 BYTE),
  IWL_MBER    VARCHAR2(100 BYTE),
  LAST_UPDT   DATE,
  ANS_NOTES   VARCHAR2(255 BYTE),
  NCI_OB_CAT  VARCHAR2(100 BYTE),
  NCI_OB_REL  VARCHAR2(100 BYTE),
  SYMBOLOGY   VARCHAR2(10 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
