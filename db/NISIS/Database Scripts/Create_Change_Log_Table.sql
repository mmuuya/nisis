drop table NISIS.CHANGE_LOG;

create table NISIS.CHANGE_LOG (
    log_id          NUMBER,
    event_date      TIMESTAMP,
    feature_id      VARCHAR2(400),
    username        VARCHAR2(255),
    action          VARCHAR2(255),
    notes           VARCHAR2(2000));