-- sde.map_nfdc_airports

select * from sde.map_nfdc_airports;

select trunc(dbms_random.value(0,4.9)) value from dual;

update sde.map_nfdc_airports set status = trunc(dbms_random.value(0,4.9));

select count(*), status from sde.map_nfdc_airports group by status order by status asc;

commit;

-- SDE.NISIS_AIRPORTS

select * from SDE.NISIS_AIRPORTS;

select trunc(dbms_random.value(0,4.9)) value from dual;

update SDE.NISIS_AIRPORTS set OVR_STATUS = trunc(dbms_random.value(0,4.9));

select count(*), OVR_STATUS from SDE.NISIS_AIRPORTS group by OVR_STATUS order by OVR_STATUS asc;

commit;