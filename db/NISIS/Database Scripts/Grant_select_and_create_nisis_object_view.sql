grant select on sde.map_nfdc_airports to nisis;

grant select on sde.map_nfdc_artcc to nisis;

create view nisis.nisis_objects_vw as select 'map_nfdc_aiports' SourceTable, objectid, sitenum designator, arptype class, facname, shape, status 
from sde.map_nfdc_airports
union select 'map_nfdc_artcc' SourceTable, objectid, id designator, class, scopeloc facname, shape, status from sde.map_nfdc_artcc
order by objectid;

select * from sde.map_nfdc_airports;