select * from airports_5010;

select * from nisis_airports_data_v2;

select * from airports_ntad_imports;

create table airports_5010_bk as select * from airports_5010;

create table nisis_airports_data_merge as select * from nisis_airports_data_v2;

create table nisis_airports_data_v2_bk as select * from nisis_airports_data_v2;

create table airports_ntad_imports_bk as select * from airports_ntad_imports;

select nd.faa_id, nd.icao_id, nd.iata_id, a5.loc_id from nisis_airports_data_merge nd right join airports_5010_bk a5 on nd.faa_id = a5.loc_id
where nd.faa_id is  null;

select ani.locid, a5.loc_id from airports_ntad_imports_bk ani right join airports_5010_bk a5 on ani.locid = a5.loc_id
where ani.locid = 'W99';

select * from nisis_airports_data_v2 nd where nd.faa_id in (select a5.loc_id from airports_ntad_imports_bk ani right join airports_5010_bk a5 on ani.locid = a5.loc_id
where ani.locid is null);

--drop table airports_5010_ntad_merge;

--drop table nisis_airports_data_merge;

create table airports_5010_ntad_merge as select * from airports_5010_bk a full join airports_ntad_imports_bk b on a.loc_id = b.locid;

select * from airports_5010_ntad_merge;

select * from airports_5010_ntad_merge where longitude is null;

select * from airports_5010_ntad_merge anm left join nisis_airports_data_merge nd on (anm.loc_id = nd.faa_id ) where anm.locid is null and nd.faa_id is null;

select * from airports_5010_ntad_merge anm left join nisis_airports_data_merge nd on (anm.loc_id = nd.faa_id ) where anm.locid = 'W99';

select * from airports_5010_ntad_merge anm where anm.loc_id = 'AUM';

select * from nisis_airports_data_merge where faa_id = 'AUM';

--create table nisis_airports_data_merge_bk as select * from nisis_airports_data_merge;

--drop table nisis_airports_data_merge;

--create table nisis_airports_data_merge as select * from nisis_airports_data_merge_bk;

MERGE INTO nisis_airports_data_v2 a
     USING airports_5010_ntad_merge b
        ON (a.faa_id = b.loc_id)
      WHEN MATCHED THEN   
        UPDATE SET a.arp_type = nvl(a.arp_type, b.lan_fa_ty), a.lg_name = b.airport_name, a.assoc_city = b.associated_city, a.assoc_st = nvl(b.faa_st, b.state), 
                   a.ref_pc = nvl(a.ref_pc, round(b.latitude, 5) || ' / ' || round(b.longitude, 5)), a.latitude = nvl(a.latitude, round(b.latitude, 5)),
                   a.longitude = nvl(a.longitude, round(b.longitude, 5)), a.elevation = nvl(a.elevation, b.elev), a.county = nvl(a.county, b.county_nam), a.faa_reg = nvl(a.faa_reg, b.faa_region), 
                   a.basic_use = nvl(a.basic_use, b.use), 
                   a.hub_type = nvl(a.hub_type, substr(b.npias_hub_type, 1, 1)),
                   a.tower = nvl(a.tower,b.cntl_twr), a.t_sch_ops = b.total_ops, a.ft_avail = b.fuel  
      WHEN NOT MATCHED THEN
        INSERT (faa_id, arp_type, lg_name, assoc_city, assoc_st, ref_pc, latitude, longitude, elevation, county, faa_reg, basic_use, hub_type, tower, t_sch_ops, ft_avail)
        VALUES (nvl(b.loc_id, b.locid), b.lan_fa_ty, nvl(b.airport_name, b.fullname), nvl(b.associated_city, b.city_name), nvl(b.state, b.county_st),
                round(b.latitude, 5) || ' / ' || round(b.longitude, 5), round(b.latitude, 5), round(b.longitude, 5), 
                b.elev, b.county_nam, nvl(b.region, b.faa_region), nvl(b.use, b.fac_use), 
        substr(b.npias_hub_type, 1, 1), b.cntl_twr, b.total_ops, b.fuel);
        
        
        commit;
        
select count(*) from nisis_airports_data_merge;

select count(*) from nisis_airports_data_merge_bk;  


select * from airports_5010 where loc_id is null;

select * from airports_ntad_imports where locid is null;

select * from airports_5010_ntad_merge where loc_id is null and locid is null;


select a.faa_id, a.arp_type, a.lg_name, a.assoc_city, a.assoc_st, a.ref_pc, a.latitude, a.longitude, a.elevation, a.county, a.faa_reg, a.basic_use, a.hub_type, a.tower, a.t_sch_ops, a.ft_avail, 
       b.faa_id, b.arp_type, b.lg_name, b.assoc_city, b.assoc_st, b.ref_pc, b.latitude, b.longitude, b.elevation, b.county, b.faa_reg, b.basic_use, b.hub_type, b.tower, b.t_sch_ops, b.ft_avail   
from nisis_airports_data_merge a LEFT JOIN (
                SELECT faa_id, arp_type, lg_name, assoc_city, assoc_st, ref_pc, latitude, longitude, elevation, county, faa_reg, basic_use, hub_type, tower, t_sch_ops, ft_avail 
                FROM nisis_airports_data_merge_bk ) b on a.faa_id = b.faa_id
WHERE (a.arp_type <> b.arp_type OR          (b.arp_type IS NULL and a.arp_type IS NOT NULL)) OR
      (a.lg_name <> b.lg_name OR            (b.lg_name  IS NULL and a.lg_name  IS NOT NULL)) OR
      (a.assoc_city <> b.assoc_city OR      (b.assoc_city IS NULL and a.assoc_city IS NOT NULL)) OR 
      (a.ref_pc <> b.ref_pc OR              (b.ref_pc IS NULL and a.ref_pc IS NOT NULL)) OR
      (a.latitude <> b.latitude OR          (b.latitude IS NULL and a.latitude IS NOT NULL)) OR
      (a.longitude <> b.longitude OR        (b.longitude IS NULL and a.longitude IS NOT NULL)) OR --truncate to 5 decimal points.    
      (a.elevation <> b.elevation OR        (b.elevation IS NULL and a.elevation IS NOT NULL)) OR    
      (a.county <> b.county OR              (b.county IS NULL and a.county IS NOT NULL) )OR
      (a.faa_reg <> b.faa_reg OR            (b.faa_reg IS NULL and a.faa_reg IS NOT NULL)) OR
      (a.basic_use <> b.basic_use OR        (b.basic_use IS NULL and a.basic_use IS NOT NULL)) OR
      (a.hub_type <> b.hub_type OR          (b.hub_type IS NULL and a.hub_type IS NOT NULL)) OR
      (a.tower <> b.tower OR                (b.tower IS NULL and a.tower IS NOT NULL)) OR
      (a.t_sch_ops <> b.t_sch_ops) OR        
      (a.ft_avail <> b.ft_avail OR          (b.ft_avail IS NULL and a.ft_avail IS NOT NULL)) OR
      (b.faa_id IS NULL);                           
      
      
      --(a.assoc_st <> b.assoc_st OR         ( b.assoc_st IS NULL and a.assoc_st IS NOT NULL)) OR
      select a.faa_id, a.arp_type, a.lg_name, a.assoc_city, a.assoc_st, a.ref_pc, a.latitude, a.longitude, a.elevation, a.county, a.faa_reg, a.basic_use, a.hub_type, a.tower, a.t_sch_ops, a.ft_avail, 
       b.faa_id, b.arp_type, b.lg_name, b.assoc_city, b.assoc_st, b.ref_pc, b.latitude, b.longitude, b.elevation, b.county, b.faa_reg, b.basic_use, b.hub_type, b.tower, b.t_sch_ops, b.ft_avail   
from nisis_airports_data_merge a LEFT JOIN (
                SELECT faa_id, arp_type, lg_name, assoc_city, assoc_st, ref_pc, latitude, longitude, elevation, county, faa_reg, basic_use, hub_type, tower, t_sch_ops, ft_avail 
                FROM nisis_airports_data_merge_bk ) b on a.faa_id = b.faa_id
WHERE a.faa_id = 'WV76';

select * from airports_ntad_imports where locid = 'TVR';

select * from airports_5010 where loc_id = 'TVR';

select * from nisis_airports_data_v2 where faa_id = '55I';

select * from nisis_airports_data_merge_bk where assoc_st is null;