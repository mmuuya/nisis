    
--SELECT * FROM ALL_TABLES WHERE OWNER = 'NISIS';

/*
Start of NISIS_IWL_BK Table creation
*/  
DECLARE
l_count NUMBER;
BEGIN

SELECT COUNT(*) INTO l_count FROM ALL_TABlES WHERE OWNER = 'NISIS' AND TABLE_NAME = 'NISIS_IWL_BK';

IF l_count > 0 THEN
--    execute immediate 'ALTER TABLE NISIS.NISIS_IWL_BK
--     DROP PRIMARY KEY CASCADE';

    execute immediate 'DROP TABLE NISIS.NISIS_IWL_BK CASCADE CONSTRAINTS';

END IF;

END;
/

CREATE TABLE NISIS.NISIS_IWL_BK AS SELECT * FROM NISIS.NISIS_IWL;

/*
END of NISIS_IWL_BK Table creation
*/

/*
Start of NISIS_IWL Table re-creation
*/  
DECLARE
l_count NUMBER;
BEGIN

SELECT COUNT(*) INTO l_count FROM ALL_TABLES WHERE OWNER = 'NISIS' AND TABLE_NAME = 'NISIS_IWL';

IF l_count > 0 THEN
    execute immediate 'DROP TABLE NISIS.NISIS_IWL CASCADE CONSTRAINTS';
    
--    execute immediate 'ALTER TABLE NISIS.NISIS_IWL
--     DROP PRIMARY KEY CASCADE';


END IF;

END;
/

CREATE TABLE NISIS.NISIS_IWL
(
  ID                 NUMBER                     NOT NULL,
  NAME               VARCHAR2(100 BYTE)         NOT NULL,
  LAYER              VARCHAR2(255 BYTE)         NOT NULL,
  DESCRIPTION        VARCHAR2(255 BYTE),
  CREATED_USER       VARCHAR2(255 BYTE),
  CREATED_DATE       DATE                       DEFAULT SYSDATE,
  LAST_UPDATED_USER  VARCHAR2(255 BYTE),
  LAST_UPDATED_DATE  DATE,
  USER_GROUP         NUMBER                     DEFAULT 0                     NOT NULL
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX NISIS.NISIS_IWL_PK ON NISIS.NISIS_IWL
(ID)
LOGGING
TABLESPACE NISIS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER NISIS.TRG_NISIS_IWL_ID 
  BEFORE INSERT ON NISIS.NISIS_IWL
  FOR EACH ROW
BEGIN
  :new.ID := SEQ_NISIS_IWL_ID.NEXTVAL;
END;
/


CREATE OR REPLACE TRIGGER NISIS.TRG_NISIS_IWL_OID 
  AFTER INSERT OR UPDATE ON NISIS.NISIS_IWL
  FOR EACH ROW
DISABLE
BEGIN
  IF :new.LAYER = 'airports' THEN
    UPDATE SDE.NISIS_AIRPORTS
    SET IWL_DSG = 1,
    IWL_MBER = :old.NAME || ', ' || :new.NAME
    WHERE SDE.NISIS_AIRPORTS.OBJECTID = :new.OBJECTID;
  END IF;
END;
/


ALTER TABLE NISIS.NISIS_IWL ADD (
  CONSTRAINT NISIS_IWL_PK
  PRIMARY KEY
  (ID)
  USING INDEX NISIS.NISIS_IWL_PK
  ENABLE VALIDATE);

/*
END of NISIS_IWL Table re-creation
*/

/*
Start of NISIS_IWL_OBJECTS Table creation
*/  
DECLARE
l_count NUMBER;
BEGIN

SELECT COUNT(*) INTO l_count FROM ALL_TABLES WHERE OWNER = 'NISIS' AND TABLE_NAME = 'NISIS_IWL_OBJECTS';

IF l_count > 0 THEN
    execute immediate 'DROP TABLE NISIS.NISIS_IWL_OBJECTS CASCADE CONSTRAINTS';
    
--    execute immediate 'ALTER TABLE NISIS.NISIS_IWL_OBJECTS
--     DROP PRIMARY KEY CASCADE';


END IF;

END;
/

CREATE TABLE NISIS.NISIS_IWL_OBJECTS
(
  ID        NUMBER                              NOT NULL,
  OBJECTID  NUMBER                              NOT NULL,
  IWL_ID    NUMBER,
  LAYER     VARCHAR2(100 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX NISIS.NISIS_IWL_OBJECTS_PK ON NISIS.NISIS_IWL_OBJECTS
(ID)
LOGGING
TABLESPACE NISIS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX NISIS.NISIS_IWL_OBJECTS_UK ON NISIS.NISIS_IWL_OBJECTS
(OBJECTID, IWL_ID, LAYER)
LOGGING
TABLESPACE NISIS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER NISIS.TRG_NISIS_IWL_OBJECTS_ID 
  BEFORE INSERT ON NISIS.NISIS_IWL_OBJECTS
  FOR EACH ROW
BEGIN
  :new.ID := SEQ_NISIS_IWL_OBJECTS_ID.NEXTVAL;
END;
/


ALTER TABLE NISIS.NISIS_IWL_OBJECTS ADD (
  CONSTRAINT NISIS_IWL_OBJECTS_PK
  PRIMARY KEY
  (ID)
  USING INDEX NISIS.NISIS_IWL_OBJECTS_PK
  ENABLE VALIDATE,
  CONSTRAINT NISIS_IWL_OBJECTS_UK
  UNIQUE (OBJECTID, IWL_ID, LAYER)
  USING INDEX NISIS.NISIS_IWL_OBJECTS_UK
  ENABLE VALIDATE);

ALTER TABLE NISIS.NISIS_IWL_OBJECTS ADD (
  FOREIGN KEY (IWL_ID) 
  REFERENCES NISIS.NISIS_IWL (ID)
  ENABLE VALIDATE);
  
/*
End of NISIS_IWL_OBJECT Table creation
*/


/*
Start of populating new tables with data from backup table
*/
--select * from nisis_iwl;
--
--select * from nisis_iwl_objects;
--
--select * from nisis_iwl_bk;

drop sequence nisis.seq_nisis_iwl_id;

CREATE SEQUENCE NISIS.SEQ_NISIS_IWL_ID
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;
  
drop sequence nisis.seq_nisis_iwl_objects_id;  
  
CREATE SEQUENCE NISIS.SEQ_NISIS_IWL_OBJECTS_ID
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;  
  
--select distinct a.name, a.layer, 'Automatic Import' description, a.created_user, (select max(created_date)from nisis_iwl_bk where name = a.name) created_date from nisis_iwl_bk a;  

--truncate table nisis.nisis_iwl;
  
declare
l_id number;
begin

select seq_nisis_iwl_id.nextval into l_id from dual;

MERGE INTO nisis_iwl n
    USING (select  distinct a.name name, a.layer layer, 'Automatic Import' description, a.created_user created_user, 
        (select max(created_date)from nisis_iwl_bk where name = a.name) created_date, 
        null last_updated_user, null last_updated_date, a.user_group user_group
    from nisis_iwl_bk a)  i
    ON (i.name = n.name)
    WHEN NOT MATCHED THEN
         insert (id, name, layer, description, created_user, created_date, last_updated_user, last_updated_date, user_group) 
         values (l_id, i.name, i.layer, i.description, i.created_user, i.created_date, i.last_updated_user, i.last_updated_date, i.user_group);
   
end;
/


declare
l_id number;
begin

select seq_nisis_iwl_id.nextval into l_id from dual;   

MERGE INTO nisis_iwl_objects n
    USING (select distinct a.id objectid, (select id from nisis_iwl where name = a.name and objectid = a.objectid) iwl_id, 
     (select layer from nisis_iwl where name = a.name and objectid = a.objectid) layer
     from nisis_iwl_bk a)  i
    ON (i.objectid = n.objectid and i.iwl_id = n.iwl_id)
    WHEN NOT MATCHED THEN
         insert (id, objectid, iwl_id, layer) 
         values (l_id, i.objectid, i.iwl_id, i.layer);         
end;
/  

update nisis_iwl set layer = 'airports' where lower(layer) like '%airports%';

update nisis_iwl set name = replace(name, '_', ' ');

delete from nisis_iwl_objects where lower(layer) like '%artcc%';

delete from nisis_iwl where lower(layer) like '%artcc%';

update nisis_iwl_objects set layer = 'airports' where lower(layer) like '%airports%';

select * from nisis_iwl order by 1;

--select * from nisis_iwl_bk;

select * from nisis_iwl_objects;

select distinct iwl_id from nisis_iwl_objects order by 1;

/*
End of populating new tables with data from backup table
*/

commit;