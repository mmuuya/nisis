drop table NISIS.FEATURE_CODES;

create table NISIS.FEATURE_CODES (
    id              NUMBER,
    fullname        VARCHAR2(200),
    code            VARCHAR2(4));
    
create sequence Feature_Code_Seq
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;  