create table nisis_atm_data_bk as select * from nisis_atm_data;

drop table nisis_data_field_picklist;

create table nisis_data_field_picklist (id number, data_field_id number, code varchar2(50), value varchar2(100));

drop sequence NISIS.SEQ_PICKLIST_ID;

CREATE SEQUENCE NISIS.SEQ_PICKLIST_ID
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


declare
begin
insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '0', 'New Airport');
insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '1', 'Unknown Status');
insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '2', 'Open with Normal Operations');
insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '3', 'Open with Limitations');
insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '4', 'Closed for Normal Ops');
insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '5', 'Closed');
commit;
end;
/


insert into nisis_data_field_picklist values (SEQ_PICKLIST_ID.nextval, 74, '0', 'New Airport');

commit;

select * from nisis_data_field_picklist where data_field_id = 74;