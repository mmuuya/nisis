
drop table data_fields;

create table data_fields ( id number not null unique, 
                           Data_Field_group varchar2(100), 
                           GIS_Field_name varchar2(100), 
                           Field_name varchar2(200), 
                           Data_Field_Shared varchar2(10), 
                           User_entry varchar2(10),
                           data_field_type varchar2(100), 
                           data_source varchar2(100),
                           tracked_object varchar2(100),
                           pick_list    varchar2(100)
                           )
                           ;

DROP SEQUENCE NISIS.SEQ_data_field_id;

CREATE SEQUENCE NISIS.SEQ_data_field_id
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


SET DEFINE OFF;
declare
begin


--Airports
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'FAA_ID', 'Airport ID (seq_data_field_id.nextval, FAA) (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ICAO_ID', 'Airport ID (seq_data_field_id.nextval, ICAO) (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'IATA_ID', 'Airport ID (seq_data_field_id.nextval, IATA) (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'LG_NAME', 'Airport Long Name (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ASSOC_CITY', 'Airport Associated City', 'No', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'REF_PC', 'Airport Reference Point Coordinates', 'No', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ELEVATION', 'Airport Elevation', 'No', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ADDRESS', 'Airport Street Address', 'No', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'COUNTY', 'Airport - County ', 'No', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'LOCATION', 'Airport - GIS Defined Location (seq_data_field_id.nextval, reference center point of airport)', 'No', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'BOUNDARY', 'Airport Boundary (seq_data_field_id.nextval, GIS)', 'No', 'No', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'RUNWAYS', 'Runways (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Set of Multiple', 'AMR-5010 and NFDC; TBD for GIS polygon',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'SUP_PROCS', 'Supporting Instrument Procedures (seq_data_field_id.nextval, data field shared with tower ATM Facility and ANS System, which are navaids)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', '',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'H_AREA', 'Heliport area', 'No', 'No', 'Set of Multiple', 'AMR-5010 and NFDC; TBD for GIS polygon',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'TIME_ZONE', 'Airport - Time Zone ', 'No', 'No', 'Automatic Lookup', 'NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'FAA_REG', 'Airport - FAA Region ', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ATO_SA', 'Airport - FAA ATO Service Area ', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'FEMA_REG', 'Airport - FEMA Region  ', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'BASIC_USE', 'Basic Use', 'No', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ATTENDANCE', 'Attendance', 'No', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'CLASS', 'Airport Classification', 'No', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'HUB_TYPE', 'Hub Type ', 'No', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'CARGO_SVC', 'Cargo Service', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'REL_STATUS', 'Reliever Status', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'INT_SVC', 'Airport International Service', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'OWNER', 'Airport Owner', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'MANAGER', 'Airport Manager', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'FBO', 'FBO ', 'No', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'RSP_TSAFSD', 'Responsible TSA FSD', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'DIAGRAM', 'Airport Diagram', 'No', 'No', 'Hyperlink', 'NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'SECT_CHART', 'Overlying Sectional Chart', 'No', 'No', 'Hyperlink', 'NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'AFD_INFO', 'Airport A/FD Information', 'No', 'No', 'Hyperlink', 'Digital A/FD database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'GGLE_MAP', 'Map Using GoogleMaps', 'No', 'No', 'Hyperlink', 'Googlemaps',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'NFDC_LINK', 'Link to NFDC', 'No', 'No', 'Hyperlink', 'NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'C30_STATUS', 'Core 30 Status', 'Yes', 'No', 'Automatic Lookup', 'ASPM',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'OEP35_STS', 'OEP 35 Status', 'Yes', 'No', 'Automatic Lookup', 'ASPM',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'OPS45_STS', 'OPSNET 45 Status', 'Yes', 'No', 'Automatic Lookup', 'ASPM',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'OP_EG_INF', 'Airport Operator Emergency Contact Info ', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'AMR-5010 and/or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'OP_CTR_INF', 'Airport Operations Center Contact Info', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'AMR-5010 and/or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'TOWER', 'Airport Tower Presence', 'Yes', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'TOWER_ID', 'ATM Facility ID - Airport Tower (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'APPR_CTRL', 'ATM Facility ID - Approach Control (seq_data_field_id.nextval, data field shared with approach control ATM facility)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'ENRTE_ATC', 'ATM Facility ID - Enroute ATC (seq_data_field_id.nextval, data field shared with en route ATM Facility)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'FLIGHT_SVC', 'ATM Facility ID - Flight Services (seq_data_field_id.nextval, data field shared with flight services ATM Facility)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'BL_AAR', 'Baseline AAR (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'CRT_AAR', 'Current AAR (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'VAR_BC_AAR', 'Variance Between Baseline AAR and Current AAR (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'AVG_OPS_N', 'Average Operations Number (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'T_OPS_PDAY', 'Total Operations Previous Day (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'VAR_AVG_PD', 'Variance Between Average Operations Number and Total Operations Previous Day (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS and NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'EST_CO_CRT', 'Total Estimated Completed Operations for Current Day (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'T_SCH_OPS', 'Total Scheduled Operations for Current Day (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'PROJ_OPS', 'Total Projected Operations for Current Day (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'VAR_AO_TO', 'Variance Between Average Operations Number and Total Operations Previous Day (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS and NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'ARFF_SVC', 'ARFF Services', 'No', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'FT_AVAIL', 'Fuel Type Available', 'No', 'No', 'Automatic Lookup', 'AMR-5010',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'AFA_STATUS', 'Aircraft Fuel Availability Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'AFA_STS_CH', 'Aircraft Fuel Availability Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'AFA_STS_EX', 'Aircraft Fuel Availability Status Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'SUP_ASR', 'Supporting ANS System (seq_data_field_id.nextval, Surveillance - ASR) - (seq_data_field_id.nextval, data field shared with tower ATM Facility and ANS System)', 'Yes', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'SUP_AG_COM', 'Supporting ANS System (seq_data_field_id.nextval, A-G Communications) - (seq_data_field_id.nextval, data field shared with tower ATM Facility and ANS System)', 'Yes', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'SUP_NAVAID', 'Supporting ANS System (seq_data_field_id.nextval, Navigational Aids) - (seq_data_field_id.nextval, data field shared with tower ATM Facility and ANS System)', 'Yes', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'SUP_ANS_O', 'Supporting ANS Systems (seq_data_field_id.nextval, Other) - (seq_data_field_id.nextval, data field shared with tower ATM Facility and ANS System)', 'Yes', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'OVR_STATUS', 'Airport Overall Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'OVR_STS_CH', 'Airport Overall Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'OVR_STS_EX', 'Airport Overall Status Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'OVR_STS_AD', 'Airport Overall Status - Additional Info Tags', 'No', 'Yes', 'Additional Info Tags', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'OVR_STS_SN', 'Airport Overall Status Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'OVR_STS_AL', 'Airport Overall Status Future Status Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'CRT_NOTAMS', 'Airport Current NOTAMs (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FAA NOTAM database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'CRT_M_TAFS', 'Airport Current METARs and TAFs (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NWS ADDS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'CRT_DELAYS', 'Current Delays (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'OIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Status Info', 'AV_OP_PLAN', 'Aviation Operator Plans (seq_data_field_id.nextval, data field shared with tower ATM Facility)', 'Yes', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'OOO_STATUS', 'Airport Owner/Operator Operations Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'OOO_STS_CH', 'Airport Owner/Operator Operations Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'OOO_STS_EX', 'Airport Owner/Operator Operations Status Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'OOO_STS_SN', 'Airport Owner/Operator Operations Status Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Ops Status Info', 'OOO_STS_AL', 'Airport Owner/Operator Operations Future Status Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'TSA_STATUS', 'Airport TSA Screening Ops Status', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Picklist)', 'TSA database or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'TSA_STS_CH', 'Airport TSA Screening Ops Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'TSA_STS_EX', 'Airport TSA Screening Ops Status Explanation', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'TSA database or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'TSA_STS_SN', 'Airport TSA Screening Ops Status Supplemental Notes', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'TSA database or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'TSA_STS_AL', 'Airport TSA Screening Ops Future Status Alarm', 'No', 'Possible', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'CBP_STATUS', 'Airport CBP Processing Ops Status', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Picklist)', 'CBP database or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'CBP_STS_CH', 'Airport CBP Processing Ops Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'CBP_STS_EX', 'Airport CBP Processing Ops Status Explanation', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'CBP database or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'CBP_STS_SN', 'Airport CBP Processing Ops Status Supplemental Notes', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'CBP database or IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Overall Status Info', 'CBP_STS_AL', 'Airport CBP Processing Ops Future Status Alarm', 'No', 'Possible', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ANS_ID', 'ANS System ID (seq_data_field_id.nextval, data field shared with ATM Facility and ANS System)', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP, NFDC, NISIS, and IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STATUS', 'Overall ANS Systems Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_AD', 'Overall ANS Systems Status - Incident Impact Additional Info Tags', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_CH', 'Overall ANS Systems Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_EX', 'Overall ANS Systems Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_SN', 'Overall ANS Systems Status - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_AL', 'Overall ANS Systems Status - Future Status Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWA_INT', 'Master IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWA_INT', 'Ad Hoc IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWL_DSG', 'Master IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWL_DSG', 'Ad Hoc IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'RES_DSG', 'Response Airport Designation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'RES_DSG_EX', 'Response Airport Designation Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'LAST_UPDT', 'Last update to Airport Profile', 'No', 'No', 'Automatic Lookup', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'ARP_NOTES', 'Airport Other Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'NCI_REL_ID', 'NISIS CI Object Relationships', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'SYMBOLOGY', '', '', '', '', '',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'LATITUDE', 'Airport - Latitude', '', '', '', '',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'LONGITUDE', 'Airport - Longitude', '', '', '', '',null,null);

--ATM Facility
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATM_ID', 'ATM Facility ID (seq_data_field_id.nextval, data field shared with Airports if referring to an ATC tower - Airport Tower ID)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'FAA_ARP_ID', 'Airport ID (seq_data_field_id.nextval, FAA) (seq_data_field_id.nextval, data field shared with Airportsif the facility is a tower)', 'Yes', 'No', 'Automatic Lookup', 'AMR-5010 and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'LG_NAME', 'ATM Facility Long Name', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'BASIC_TYPE', 'Basic Type of ATM Facility', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'RP_SVC_UT', 'Responsible Service Unit', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'DTL_TYPE', 'Detailed Type of ATM Facility', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'CTRT_ATC_V', 'Contract ATC Vendor', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ADDRESS', 'ATM Facility Address', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ASSOC_CITY', 'ATM Facility Associated City', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'LOCATION', 'ATM Facility - GIS Defined Location', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'COUNTY', 'ATM Facility - County', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'TIME_ZONE', 'ATM Facility - Time Zone', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'FAA_REG', 'ATM Facility - FAA Region', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATO_SA', 'ATM Facility - ATO Service Area', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'FEMA_REG', 'ATM Facility - FEMA Region', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'HRS_OF_OPS', 'ATM Facility Hours of Operation ', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'PHOTO', 'ATM Facility Photo', 'No', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'PRIM_PH', 'ATM Facility Primary Phone Nu', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATC_LEVEL', 'ATC Level', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'APPR_CTRL', 'ATM Facility ID - Approach Control  (seq_data_field_id.nextval, data field shared with Airports)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ENRTE_ATC', 'ATM Facility ID - Enroute ATC (seq_data_field_id.nextval, data field shared with Airports)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'FLT_SVC', 'ATM Facility ID - Flight Services (seq_data_field_id.nextval, data field shared with Airports)', 'Yes', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'MANAGER', 'ATM Facility Manager', 'No', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'OPS_NUM', 'ATM Facility Ops Number', 'No', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport Baseline Info', 'SATPH_NUM', 'ATM Facility Satphone Number', 'No', 'Possible', 'Manual (seq_data_field_id.nextval, Free Text)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'TRM_OP_DST', 'Terminal Ops District', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'TRM_OP_DM', 'Terminal Ops District Manager', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'TEC_OP_FAC', 'ATM Facility - Supporting Tech Ops Field Facilities (seq_data_field_id.nextval, data field shared with Tech Ops Field Facility)', 'Yes', 'No', 'Set of Multiple', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'OPS_STATUS', 'ATM Operations Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'OPS_STS_AD', 'ATM Operations Status - Incident Impact Additional Info Tags', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'OPS_STS_CH', 'ATM Operations Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'OPS_STS_EX', 'ATM Operations Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'OPS_STS_SN', 'ATM Operations Status - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'OPS_STS_AL', 'ATM Operations Status - Future Status Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'EG_SUPPORT', 'ATM Facility EG Support', 'No', 'Possible', 'Automatic Lookup', 'FSEP',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Ops - ANS System Status Info', 'PWR_STATUS', 'ATM Facility Power Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Ops - ANS System Status Info', 'PWR_STS_CH', 'ATM Facility Power Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Ops - ANS System Status Info', 'PWR_STS_EX', 'ATM Facility Power Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Ops - ANS System Status Info', 'PWR_STS_SN', 'ATM Facility Power Status - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATO_PA', 'ATO Personnel Assigned to Facility', 'No', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL', 'ATO Ops Staffing Level ', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL_CH', 'ATO Ops Staffing Level (seq_data_field_id.nextval, CHECK TIME)', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL_EX', 'ATO Ops Staffing Level - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL_SN', 'ATO Ops Staffing Level - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS', 'ATO Personnel Accounted For Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS_CH', 'ATO Personnel Accounted For Status', 'Yes', 'Yes', 'Hyperlink', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS_EX', 'ATO Personnel Accounted For Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS_SN', 'ATO Personnel Accounted For Status- Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PSI', 'ATO Personnel Significant Impact', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PSI_EX', 'ATO Personnel Significant Impact - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PSI_SN', 'ATO Personnel Significant Impact - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ANS_ID', 'ANS System ID (seq_data_field_id.nextval, data field shared with ANS System and Airports)', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP, NFDC, NISIS, and IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL', 'Current Readiness Level ', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_CH', 'Current Readiness Level - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_EX', 'Current Readiness Level - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_SN', 'Current Readiness Level - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_AL', 'Readiness Level - Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL', 'Current SECON Level', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_CH', 'Current SECON Level - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_EX', 'Current SECON Level - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_SN', 'Current SECON Level - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_AL', 'SECON Level Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWA_INT', 'Master IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWA_INT', 'Ad Hoc IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWL_DSG', 'Master IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWL_DSG', 'Ad Hoc IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'LAST_UPDT', 'Last update to ATM Facility Profile', 'No', 'No', 'Automatic Lookup', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'ATM_NOTES', 'ATM Facility Other Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'NCI_REL_ID', 'NISIS CI Object Relationships', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NISIS',null,null);

--ANS System
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ANS_ID', 'ANS System ID (seq_data_field_id.nextval, data field shared with Airports and ATM Facility)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP and NFDC',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'BASIC_CAT', 'ANS System Basic Category', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP and NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ANS_CLASS', 'ANS System Class', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP and NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'MODEL', 'ANS System Model', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP and NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'REST_REQ', 'Restoration Requirement', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'TEC_OP_FAC', 'ANS System - Supporting Tech Ops Field Facilities (seq_data_field_id.nextval, data field shared with Tech Ops Field Facility)', 'Yes', 'No', 'Set of Multiple', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'SUP_RWYS', 'Supported Runway(seq_data_field_id.nextval, s)', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'LOCATION', 'ANS System - GIS Defined Location', 'No', 'No', 'Automatic Lookup', 'FSEP',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ASSOC_CITY', 'ANS System Associated City', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'COUNTY', 'ANS System - County', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'TIME_ZONE', 'ANS System - Time Zone', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'FAA_REG', 'ANS System - FAA Region', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATO_SA', 'ANS System - ATO Service Area', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'AMR-5010, NFDC, and DTRB',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'FEMA_REG', 'ANS System - FEMA Region', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Airport-ANS Baseline Info', 'SUP_ARPS', 'Supported Airports', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP?',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'SUP_ATMS', 'Supported ATM Facilities (seq_data_field_id.nextval, data field shared with ATM Facility as applicable)', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP?',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'SUP_RWYS2', 'Supported Runways (seq_data_field_id.nextval, data field shared with Airports if the system is a terminal navaid )', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP, NFDC, and TERPS database?',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'SUP_PROCS', 'Supported Instrument Procedures (seq_data_field_id.nextval, data field shared with Airports and ATM Facility if the system is a terminal navaid )', 'Yes', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'FSEP, NFDC, and TERPS database?',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STATUS', 'ANS System Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_AD', 'ANS System Status - Incident Impact Additional Info Tags', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_CH', 'ANS System Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_EX', 'ANS System Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_SN', 'ANS System Status - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS System Status Info', 'ANS_STS_AL', 'ANS System Status - Future Status Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATM_EG_SUP', 'ATM Facility EG Support', 'No', 'Possible', 'Automatic Lookup', 'FSEP',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWA_INT', 'Master IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWA_INT', 'Ad Hoc IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWL_DSG', 'Master IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWL_DSG', 'Ad Hoc IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'LAST_UPDT', 'Last update to ANS System Profile', 'No', 'No', 'Automatic Lookup', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'ANS_NOTES', 'ANS System Facility Other Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'NCI_REL_ID', 'NISIS CI Object Relationships', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NISIS',null,null);

--Other Staffed Facility
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'OSF_ID', 'OSF ID ', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'LG_NAME', 'OSF Long Name', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'BASIC_TYPE', 'Basic Type of OSF', 'No', 'No', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'ADDRESS', 'OSF Address', 'No', 'No', 'Automatic Lookup', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'ASSOC_CITY', 'OSF Associated City', 'No', 'No', 'Automatic Lookup', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'LOCATION', 'OSF - GIS Defined Location', 'No', 'No', 'Automatic Lookup', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'COUNTY', 'OSF - County', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'TIME_ZONE', 'OSF - Time Zone', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'FAA_REG', 'OSF - FAA Region', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'ATO_SA', 'OSF - ATO Service Area', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'FEMA_REG', 'OSF - FEMA Region', 'No', 'No', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'OPS_HOURS', 'OSF Hours of Operation ', 'No', 'Possible', 'Automatic Lookup', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'PHOTO', 'OSF Photo', 'No', 'Possible', 'Automatic Lookup', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'PHONE', 'OSF Front Office Main Nu', 'No', 'No', 'Automatic Lookup', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'MANAGER', 'OSF Facility Manager', 'No', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'LDAP or facility management database',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_STATUS', 'OSF Operations Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_STS_AD', 'OSF Operations Status - Incident Impact Additional Info Tags', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_STS_CH', 'OSF Operations Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_STS_EX', 'OSF Operations Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_STS_SN', 'OSF Operations Status - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_STS_AL', 'OSF Operations Status - Future Status Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'OSF_EG_SUP', 'OSF Facility EG Support', 'No', 'Possible', 'Automatic Lookup', 'FSEP',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'PWR_STATUS', 'OSF Power Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'PWR_STS_CH', 'OSF Power Status - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'PWR_STS_EX', 'OSF Power Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Status Info', 'PWR_STS_SN', 'OSF Power Status - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Other Staffed Facility Baseline Info', 'FAA_PA', 'Total FAA Personnel Assigned to OSF', 'No', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'ATO_PA', 'ATO Personnel Assigned to Facility', 'No', 'Possible', 'Automatic Lookup', 'TBD',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL', 'ATO OSF Staffing Level ', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL_CH', 'ATO OSF Staffing Level ', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL_EX', 'ATO OSF  Staffing Level - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_SL_SN', 'ATO OSF Staffing Level - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS', 'ATO OSF Personnel Accounted For Status', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS_CH', 'ATO OSF Personnel Accounted For Status', 'Yes', 'Yes', 'Hyperlink', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS_EX', 'ATO OSF Personnel Accounted For Status - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PAS_SN', 'ATO OSF Personnel Accounted For Status- Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PSI', 'ATO Personnel Significant Impact', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PSI_EX', 'ATO OSF Personnel Significant Impact - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ATM Status Info', 'ATO_PSI_SN', 'ATO OSF Personnel Significant Impact - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_STS', 'Current Readiness Level ', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_CH', 'Current Readiness Level - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_EX', 'Current Readiness Level - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_SN', 'Current Readiness Level - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_RL_ALM', 'Readiness Level - Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_STS', 'Current SECON Level', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Picklist)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_CH', 'Current SECON Level - Status Change Time Check', 'No', 'Yes', 'Other', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_EX', 'Current SECON Level - Explanation', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_SN', 'Current SECON Level - Supplemental Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'CRT_SL_ALM', 'SECON Level Alarm', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWA_INT', 'Master IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWA_INT', 'Ad Hoc IWA Intersection', 'No', 'Yes', 'Automatic (seq_data_field_id.nextval, GIS data lookup)', 'NISIS + IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'M_IWL_DSG', 'Master IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'AD_IWL_DSG', 'Ad Hoc IWL Designation', 'No', 'Yes', 'Set of Multiple', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'LAST_UPDT', 'Last update to OSF Profile', 'No', 'No', 'Automatic Lookup', 'NISIS',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'Incident Management Info', 'OSF_NOTES', 'OSF Other Notes', 'No', 'Yes', 'Manual (seq_data_field_id.nextval, Free Text)', 'IM Watch-Stander',null,null);
insert into data_fields values (seq_data_field_id.nextval, 'ANS Baseline Info', 'NCI_REL_ID', 'NISIS CI Object Relationships', 'Yes', 'Possible', 'Automatic (seq_data_field_id.nextval, lookup & synthesis)', 'NISIS',null,null);

commit;

end;
/

select * from data_fields;

commit;