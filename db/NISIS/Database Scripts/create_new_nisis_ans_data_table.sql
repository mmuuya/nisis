/*
Merge ANS data into the NISIS_ANS_DATA table.
*/

drop table nisis_ans_data;

create table nisis_ans_data (
    ans_id varchar2(10),
    fsep_id number,
    basic_ty varchar2(5),
    ans_class varchar2(10),
    assoc_a_rw varchar2(10),
    use_status varchar2(50),
    basic_cat varchar2(10),
    descript varchar2(10),
    resto_req number,
    pwr_cd varchar2(1),
    rtof_fac varchar2(1),
    latitude number,
    longitude number,
    t_ops_dist varchar2(5),
    mtnce_resp varchar2(1),
    ref_pc varchar2(20),
    assoc_city varchar2(40),
    assoc_st varchar2(2),
    assoc_cnty varchar2(40),
    time_zone varchar2(5),
    faa_reg varchar2(2),
    ato_sa varchar2(10),
    fema_reg varchar2(10),
    resp_su varchar2(5),
    ownership varchar2(50),
    rel_anss varchar2(10),
    sup_arps varchar2(10),
    sup_atms varchar2(10),
    sup_rwys varchar2(10), 
    sup_instp varchar2(10),
    ans_status integer,
    ans_sts_ad varchar2(10),
    ans_sts_ex varchar2(50),
    ans_sts_sn varchar2(100),
    ans_sts_al varchar2(10),
    rmls_rep varchar2(10),
    eg_ups_sup varchar2(2),
    bpwr_type varchar2(10),
    bpwr_sts varchar2(10),
    bpwr_usts varchar2(10),
    bpwr_s_ex varchar2(20),
    bpwr_s_sn varchar2(50),
    bpwr_s_al varchar2(10),
    fs_inter varchar2(100),
    fs_mber varchar2(100),
    iwl_dsgn varchar2(100),
    iwl_mber varchar2(100),
    last_updt date,
    ans_notes varchar2(50),
    nci_ob_cat varchar2(10),
    nci_ob_rel varchar2(10),
    symbology varchar2(10)
    );
    
--select * from ans_facserv_ref;    

--select * from ans_validtypestatus_ref;  

--select * from ans_status_code_ref;  

--select max(length(fac_code_sysid)) from ans_facserv_ref;

--select max(length(description)) from ans_status_code_ref;

commit;