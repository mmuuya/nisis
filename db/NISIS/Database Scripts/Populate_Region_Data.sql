
--drop table stateDistrictTemp;

create table stateDistrictTemp (
    stateName varchar2(100), stateAbbrev varchar2(2), femaDistrict varchar2(5), 
    atoService varchar2(50), atoTerminal varchar2(100),   
    faaRegion varchar2(3));
    
insert into stateDistrictTemp values ('Alabama','AL','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('Alaska','AK','X','Western Service Center','Alaskan','AAL');
insert into stateDistrictTemp values ('Arizona','AZ','IX','Western Service Center','Western-Pacific','AWP');
insert into stateDistrictTemp values ('Arkansas','AR','VI','Central Service Center','Southwest','ASW');
insert into stateDistrictTemp values ('California','CA','IX','Western Service Center','Western-Pacific','AWP');
insert into stateDistrictTemp values ('Colorado','CO','VIII','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('Connecticut','CT','I','Eastern Service Center','New England','ANE');
insert into stateDistrictTemp values ('Delaware','DE','III','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('Florida','FL','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('Georgia','GA','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('Hawaii','HI','IX','Western Service Center','Western-Pacific','AWP');
insert into stateDistrictTemp values ('Idaho','ID','X','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('Illinois','IL','V','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Indiana','IN','V','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Iowa','IA','VII','Central Service Center','Central','ACE');
insert into stateDistrictTemp values ('Kansas','KS','VII','Central Service Center','Central','ACE');
insert into stateDistrictTemp values ('Kentucky','KY','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('Louisiana','LA','VI','Central Service Center','Southwest','ASW');
insert into stateDistrictTemp values ('Maine','ME','I','Eastern Service Center','New England','ANE');
insert into stateDistrictTemp values ('Maryland','MD','III','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('Massachusetts','MA','I','Eastern Service Center','New England','ANE');
insert into stateDistrictTemp values ('Michigan','MI','V','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Minnesota','MN','V','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Mississippi','MS','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('Missouri','MO','VII','Central Service Center','Central','ACE');
insert into stateDistrictTemp values ('Montana','MT','VIII','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('Nebraska','NE','VII','Central Service Center','Central','ACE');
insert into stateDistrictTemp values ('Nevada','NV','IX','Western Service Center','Western-Pacific','AWP');
insert into stateDistrictTemp values ('New Hampshire','NH','I','Eastern Service Center','New England','ANE');
insert into stateDistrictTemp values ('New Jersey','NJ','II','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('New Mexico','NM','VI','Central Service Center','Southwest','ASW');
insert into stateDistrictTemp values ('New York','NY','II','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('North Carolina','NC','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('North Dakota','ND','VIII','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Ohio','OH','V','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Oklahoma','OK','VI','Central Service Center','Southwest','ASW');
insert into stateDistrictTemp values ('Oregon','OR','X','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('Pennsylvania','PA','III','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('Rhode Island','RI','I','Eastern Service Center','New England','ANE');
insert into stateDistrictTemp values ('South Carolina','SC','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('South Dakota','SD','VIII','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Tennessee','TN','IV','Eastern Service Center','Southern','ASO');
insert into stateDistrictTemp values ('Texas','TX','VI','Central Service Center','Southwest','ASW');
insert into stateDistrictTemp values ('Utah','UT','VIII','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('Vermont','VT','I','Eastern Service Center','New England','ANE');
insert into stateDistrictTemp values ('Virginia','VA','III','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('Washington','WA','X','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('West Virginia','WV','III','Eastern Service Center','Eastern','AEA');
insert into stateDistrictTemp values ('Wisconsin','WI','V','Central Service Center','Great Lakes','AGL');
insert into stateDistrictTemp values ('Wyoming','WY','VIII','Western Service Center','Northwest Mountain','ANM');
insert into stateDistrictTemp values ('District of Columbia','DC','III','Eastern Service Center','Eastern','AEA');  

commit;

--select * from stateDistrictTemp;
--
--select distinct faaregion from stateDistrictTemp order by 1 desc;
--
--truncate table stateDistrictTemp;

/**************************************************************
** Service Area Updates
**************************************************************/


select * from nisis_airports_data_v2;

--update nisis_airports_data_v2 set fema_reg ='temp_test' where faa_id = 'IS69';

--update nisis_airports_data_v2 set fema_reg ='temp_test', ato_td = 'temp_test', ato_sa = 'temp_test', faa_reg ='temp_test' where faa_id = 'IS69';

--rollback;

select faa_id, icao_id, iata_id, lg_name, assoc_st, fema_reg, ato_td, ato_sa, faa_reg from nisis_airports_data_v2;

select n.faa_id, n.icao_id, n.iata_id, n.lg_name, n.assoc_st, 
    n.fema_reg, T.FEMADISTRICT, 
    n.ato_td, T.ATOTERMINAL, 
    n.ato_sa, T.ATOSERVICE,
    n.faa_reg, T.FAAREGION 
    from nisis_airports_data_v2 n left join statedistricttemp t on
    T.STATEABBREV = N.ASSOC_ST
    where n.faa_reg <> t.faaregion;    
    
merge into nisis_airports_data_v2 n
using statedistricttemp t
on (t.stateabbrev = n.assoc_st)
when matched then
update set 
    n.fema_reg = t.femadistrict, 
    n.ato_td = t.atoterminal, 
    n.ato_sa = t.atoservice,
    n.faa_reg = t.faaregion ;
    
commit;    

select * from nisis_ans_data_v2;

drop table nisis_ans_data_v2_bk;

create table nisis_ans_data_v2_bk as select * from nisis_ans_data_v2;

merge into nisis_ans_data_v2 n
using statedistricttemp t
on (t.stateabbrev = n.assoc_st or upper(t.statename) = upper(n.assoc_st))
when matched then
update set 
    n.fema_reg = t.femadistrict, 
--    n.ato_td = t.atoterminal, 
    n.ato_sa = t.atoservice,
    n.faa_reg = t.faaregion ;
    
select * from nisis_atm_data_v2;   

drop table nisis_atm_data_v2_bk;

create table nisis_atm_data_v2_bk as select * from nisis_atm_data_v2; 
    
merge into nisis_atm_data_v2 n
using statedistricttemp t
on (t.stateabbrev = n.assoc_st or upper(t.statename) = upper(n.assoc_st))
when matched then
update set 
    n.fema_reg = t.femadistrict, 
    n.ato_td = t.atoterminal, 
    n.ato_sa = t.atoservice,
    n.faa_reg = t.faaregion ;
    
select * from nisis_osf_data_v2; 

drop table nisis_osf_data_v2_bk;

create table nisis_osf_data_v2_bk as select * from nisis_osf_data_v2;   
    
merge into nisis_osf_data_v2 n
using statedistricttemp t
on (t.stateabbrev = n.assoc_st or upper(t.statename) = upper(n.assoc_st))
when matched then
update set 
    n.fema_reg = t.femadistrict, 
--    n.ato_td = t.atoterminal, 
    n.ato_reg = t.atoservice,
    n.faa_reg = t.faaregion ;
    
rollback;    

commit;

drop table stateDistrictTemp;