drop table ARTCC_FACILITY_DATA_IMPORTS;

create table ARTCC_FACILITY_DATA_IMPORTS (Facility_Name varchar2(100), 
                                                  ID varchar2(100), 
                                                  Street_Address varchar2(100), 
                                                  City varchar2(100), 
                                                  State varchar2(100), 
                                                  County varchar2(100), 
                                                  Zip varchar2(100), 
                                                  Latitude number, 
                                                  Longitude number, 
                                                  ATM varchar2(100), 
                                                  Front_Desk varchar2(100), 
                                                  Watch_Desk varchar2(100), 
                                                  After_Hours varchar2(100), 
                                                  Cong_District varchar2(100), 
                                                  District_Rep varchar2(100), 
                                                  SA varchar2(100), 
                                                  Facility_Level number, 
                                                  AJE_Employees number, 
                                                  CPC_Numbers number, 
                                                  AJW_Employees number, 
                                                  EnRoute_Radar_Indents number, 
                                                  Terminal_Radar_Idents number, 
                                                  AREAS number, 
                                                  Operational_Sectors number, 
                                                  R_Positions number, 
                                                  A_Positions number, 
                                                  D_Positions number, 
                                                  Facility_Commissioned date);
                                                  
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Albuquerque ARTCC', 'ZAB', '8000 Louisiana Blvd., NE', 'Albuquerque', 'NM', 'Bernalillo', '87019-5645', '35.17338', '-106.56752', 'Terry Locke', '(505) 856-4601', '(505) 856-4500', '(505) 728-8415 BB', '1' , 'Martin Heinrich (D)', 'C', 10, 358, 195, null, 16, 4, 5, 39, 39, 13, 39, to_date('4/20/1963', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Chicago ARTCC', 'ZAU', '619 Indian Trail Road', 'Aurora', 'IL', 'Kane', '60506-2188', '41.783269', '-88.324735', 'Bill Cound', '(630) 906-8223', '(630) 906-8341', '(708) 805-5075', '14' , 'Bill Foster (D)', 'C', 12, 549, 359, 63, 9, 0, 9, 48, 48, 16, 48, to_date('12/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Boston ARTCC', 'ZBW', '35 Northeastern Blvd.', 'Nashua', 'NH', 'Hillsborough', '03062-3126', '42.73611', '-71.48056', 'Rick Garceau (a)', '(603) 879-6638', '(603) 879-6655', '(425) 367-3454', '2' , 'Paul Hodes (D)', 'E', 11, 389, 241, 88, 11, 8, 5, 32, 32, 10.6666666666667, 32, to_date('3/19/1961', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Washington ARTCC', 'ZDC', ' 825 East Market St.', 'Leesburg', 'VA', 'Loudon', '20176-4404', '39.101675', '-77.543038', 'Robert K Jones', '(703) 771-3403', '(703) 771-3470', '(954) 670-7094', '10' , 'Frank Wolf / R', 'E', 12, 478, 290, 90, 10, 9, 8, 46, 46, 15.3333333333333, 46, to_date('4/28/1963', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Denver ARTCC', 'ZDV', '2211 17th Ave.', 'Longmont', 'CO', 'Boulder', '80501-9763', '40.187308', '-105.12694', 'Kevin Stark', '(303) 651-4101', '(303) 651-4248', '(303) 651-4102', '4' , 'Betsy Markey (D)', 'W', 10, 438, 246, 65, 15, 4, 5, 41, 41, 13.6666666666667, 41, to_date('5/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Fort Worth ARTCC', 'ZFW', '13800 FAA Road', 'Fort Worth', 'TX', 'Tarrant', '76155-2104', '32.83096', '-97.06612', 'Andi Ramaker', '(817) 858-7500', '(817) 858-7503', '(817) 366-7593', '24' , 'Kenny Marchant /R', 'C', 12, 441, 274, 64, 14, 6, 7, 42, 42, 14, 42, to_date('4/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Houston ARTCC', 'ZHU', '16600 John F. Kennedy Boulevard', 'Houston', 'TX', 'Harris', '77032-6514', '29.96077', '-95.33077', 'Jim D�Ambrosio', '(281) 230-5600', '(281) 230-5560', '(832) 693-5266 ', '18' , 'Sheila Jackson-Lee (D)', 'C', 11, 400, null, 63, 16, 2, 6, 42, 42, 14, 42, to_date('7/10/1963', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Indianapolis ARTCC', 'ZID', '1850 South Sigsbee Street', 'Indianapolis', 'IN', 'Marion', '46241-3640', '39.73838', '-86.29804', 'Randy C. Smith (a)', '(317) 247-2234', '(317) 247-2242', '(317) 927-7195', '7' , 'Andre Carson (D)', 'C', 12, 508, 325, 65, 8, 1, 7, 42, 42, 14, 42, to_date('11/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Jacksonville ARTCC', 'ZJX', '811 E. Second St. ', 'Hillard', 'FL', 'Nassau', '32046-6484', '30.70301', '-81.90816', 'Robin Badger (a)', '(904) 549-1501', '(904) 549-1537 ', '(904) 298-4338', '4' , 'Ander Crenshaw /R', 'E', 11, 455, 262, 62, 13, 3, 5, 40, 40, 13.3333333333333, 40, to_date('2/25/1961', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Kansas City ARTCC', 'ZKC', '1801 E. Loula  ', 'Olathe', 'KS', 'Johnson', '66062-1708', '38.880195', '-94.790728', 'Paul Infanti (a)', '(913) 254-8403', '(913) 254-8500', '(913) 669-6341', '3' , 'Dennis Moore (D)', 'C', 11, 401, 270, 72, 15, 5, 6, 45, 45, 15, 45, to_date('4/30/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Los Angeles ARTCC', 'ZLA', '2555 East Avenue ''P''', 'Palmsdale', 'CA', 'Los Angeles', '93550-2112', '34.60304', '-118.08562', 'Tommy Graham', '(661) 265-8201', '(661) 265-8205', '(661) 265-8202', '25' , 'Howard McKeon / R', 'W', 12, 395, 231, 87, 11, 3, 6, 36, 36, 12, 36, to_date('8/23/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Salt Lake City ARTCC', 'ZLC', '2150 West 700 North', 'Salt Lake City', 'UT', 'Salt Lake', '84116-2952', '40.783913', '-111.952411', 'Matt Csicsery', '(801) 320-2501', '(801) 320-2560', '(801) 320-2503', '1' , 'Rob Bishop / R', 'W', 10, 303, 182, 66, 19, 4, 4, 27, 27, 9, 27, to_date('10/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Miami ARTCC', 'ZMA', '7500 N. W. 58th St. ', 'Miami', 'FL', 'Miami-Dade', '33166-3724', '25.82538', '-80.31977', 'Mark Rios (a)', '(305) 716-1500', '(305) 716-1588', '(305) 716-1602', '21' , 'Lincoln Diaz-Balart / R', 'E', 11, 410, 226, 60, 10, 2, 6, 36, 36, 12, 36, to_date('9/2/1963', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Memphis ARTCC', 'ZME', '3229 Democrat Road  ', 'Memphis', 'TN', 'Shelby', '38118-1513', '35.06676', '-89.95842', 'Timothy J. Nelson', '(901) 368-8160', '(901) 368-8234', ' (901) 848-8661', '9' , 'Steve Cohen (D)', 'E', 12, 421, 247, 57, 11, 0, 5, 37, 37, 12.3333333333333, 37, to_date('5/5/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Minneapolis ARTCC', 'ZMP', '512 Division St.', 'Farmington', 'MN', 'Dakota', '55024-1258', '44.6375', '-93.1525', 'Kelly Nelson', '(651) 463-5510', '(651) 463-5580', '(651) 494-2174', '2' , 'John Kline / R', 'C', 11, 421, 280, 56, 14, 3, 6, 39, 39, 13, 39, to_date('7/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('New York ARTCC', 'ZNY', '4205 Johnson Ave', 'Ronkonkoma', 'NY', 'Suffolk', '11779-7339', '40.78389', '-73.09694', 'Frank Black (a)', '(631) 468-1001', '(631) 468-5959', '(631) 300-6948', '2' , 'Steve Israel (D)', 'E', 12, 440, 248, 51, 10, 9, 6, 29, 29, 9.66666666666667, 29, to_date('7/21/1963', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Oakland ARTCC', 'ZOA', '5125 Central Ave. ', 'Fremont', 'CA', 'Alameda', '94536-6531', '37.54244', '-122.01579', 'Randy Park', '(510) 745-3301', '(510) 745-3331', '(510) 745-3321', '13' , 'Fortney Stark (D)', 'W', 11, 330, 187, 97, 10, 0, 4, 21, 21, 7, 21, to_date('10/9/1960', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Cleveland ARTCC', 'ZOB', '326 East Lorain St.', 'Oberlin', 'OH', 'Lorain', '44074-1216', '41.24583', '-82.20556', 'Cindy Alexander (a)', '(440) 774-0320', '(440) 774-0426', '(330) 360-2617 ', '9' , 'Marcy Kaptur (D)', 'C', 12, 532, 361, 62, 9, 2, 8, 49, 49, 16.3333333333333, 49, to_date('2/7/1961', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Seattle ARTCC', 'ZSE', '3101 Auburn Way South', 'Auburn', 'WA', 'King', '98092-7950', '47.287002', '-122.188219', 'Jamie Erdt', '(253) 351-3501', '(253) 351-3520', '(253) 351-3502', '8' , 'Dave Reichert / R', 'W', null, 271, 166, 63, 14, 4, 4, 26, 26, 8.66666666666667, 26, to_date('8/1/1962', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Atlanta ARTCC', 'ZTL', '299 Woolsey Road', 'Hampton', 'GA', 'Henry', '30228-2106', '33.379682', '-84.296798', 'Eric Fox', '(770) 210-7630', '(770) 210-7622', '(603) 765-6602', '3' , 'Lynn Westmoreland / R', 'E', 12, 589, 353, 63, 11, 1, 7, 46, 46, 15.3333333333333, 46, to_date('10/15/1960', 'MM/DD/YYYY')); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Anchorage', 'ZAN', '700 North Boniface Parkway', 'Anchorage', 'AK', 'Anchorage', '99506-1612', '61.89309', '-149.8314', 'Bob Watkins', '(907) 269-1137', '(907) 269-1103', '(907) 269-1137', '' , 'Donald Young / R', 'W', 10, 186, 90, 97, 18, 6, 3, 16, 16, 5.33333333333333, 16, null); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('San Juan', 'ZSU', '5000 Carr. 190', 'Carolina', 'PR', 'Carolina', '979', '', '', 'Felipe Fraticelli', '(787) 253-8663', '(787) 253-8664', '(787) 253-8695', 'N/A' , 'Pedro Pierluisi (D)', 'E', 9, 80, 36, 64, 2, 2, null, null, 0, 0, 0, null); 
insert into ARTCC_FACILITY_DATA_IMPORTS values ('Guam', 'ZUA', '1775 Admiral Sherman Boulevard', 'Barrigada', 'GU', 'N/A', '96913', '', '', 'Tim Cornelison', '(671) 473-1200', '(671) 366-5151', '(671) 473-1200', 'N/A' , 'Madeleine Bordallo (D)', 'W', 8, 29, 15, 10, 1, 1, null, null, 0, 0, 0, null); 

commit;