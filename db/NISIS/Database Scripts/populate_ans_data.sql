create table nisis_ans_data_bk as select * from nisis_ans_data;

truncate table nisis_ans_data;

insert into nisis_ans_data  (ans_id, fsep_id, assoc_a_rw, use_status, descript, resto_req, pwr_cd, rtof_fac, latitude, longitude,
    t_ops_dist, mtnce_resp, 
    ref_pc, assoc_city, assoc_st, faa_reg, resp_su, sup_rwys, last_updt)
    select fac_ident, fsrefid, assoc_airport, fac_status, fac_code_facility, rest_code, power_code, fac_class, latitude_deg, longitude_deg,
    atow_group, fac_class, 
    latitude_deg ||'/'|| longitude_deg, fac_location, fac_state, fac_region, 'AJW', runway, sysdate
from ans_facserv_ref
;

select * from nisis_ans_data;

select * from ans_facserv_ref;

select * from ans_validtypestatus_ref;

create or replace view basic_type_temp_vw as select distinct f.fsrefid, f.fac_type original_type, v.fac_type, v.wr_area, v.capability, CASE WHEN v.capability LIKE 'SURVEILLANCE%' THEN 'SUR' 
                                                  WHEN v.capability LIKE 'WEATHER%' THEN 'WX' 
                                                  WHEN v.capability LIKE 'COMMUNICATION%' THEN 'COM' 
                                                  WHEN v.capability LIKE 'NAV%' THEN 'NAV' 
                                                  ELSE NULL END basic_ty
from ans_validtypestatus_ref v right join ans_facserv_ref f on f.fac_type = v.fac_type;  

select * from basic_type_temp_vw;

--The following code block takes about an hour to run on the 88k+ entries in the nisis_ans_data table, but it does work, rest assured.

declare
i number := 0;
begin
for r in (select fsep_id from nisis_ans_data)
loop
   update nisis_ans_data set basic_ty = (select v.basic_ty from basic_type_temp_vw v where v.fsrefid = r.fsep_id),
                             basic_cat = (select v.wr_area from basic_type_temp_vw v where v.fsrefid = r.fsep_id) 
   where fsep_id = r.fsep_id; 

    i := i+1;
    if mod(i, 1000) = 0 then
        dbms_output.put_line('commit reached!: ' || i || '| ' || r.fsep_id);
        commit;
    end if;
end loop;
commit;
end;
/

drop view basic_type_temp_vw;   

select * from nisis_ans_data;

--update nisis_ans_data set fsep_id = null;

alter table nisis_ans_data modify fsep_id varchar2(10);

select * from ans_facserv_ref;

select n.ans_id, n.fsep_id, n.assoc_a_rw, n.sup_arps, n.sup_rwys, n.descript,
       a.fac_ident, a.runway, a.second_runway, a.assoc_airport, a.assoc_airport_2, a.assoc_airport_3, a.assoc_airport_4, a.assoc_airport_5, a.fac_code_facility
from nisis_ans_data n join ans_facserv_ref a on n.ans_id = a.fac_ident and n.descript = a.fac_code_facility;

update nisis_ans_data set fsep_id = ans_id;

rollback;

update nisis_ans_data n set n.sup_arps = n.assoc_a_rw; 

update nisis_ans_data n set n.assoc_a_rw = n.sup_arps || '/' || n.sup_rwys;

update nisis_ans_data n set n.assoc_a_rw = CASE WHEN n.assoc_a_rw LIKE '/' THEN null ELSE n.assoc_a_rw END;

create sequence nisis.ans_data_seq
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;
  
declare
newId number;
begin
for r in (select ans_id, fsep_id, descript from nisis_ans_data)
loop
    newId := ans_data_seq.nextval();
    update nisis_ans_data set ans_id = newId where fsep_id = r.fsep_id and descript = r.descript; 
end loop;
end;
/  

commit;


