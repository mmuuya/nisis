create table nisis.feature_status (id number, name varchar2(20));

create sequence nisis.seq_feature_status
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE;

DECLARE
BEGIN
insert into nisis.feature_status values (nisis.seq_feature_status.nextval, 'New');

insert into nisis.feature_status values (nisis.seq_feature_status.nextval, 'Unknown');

insert into nisis.feature_status values (nisis.seq_feature_status.nextval, 'Good');

insert into nisis.feature_status values (nisis.seq_feature_status.nextval, 'Pending');

insert into nisis.feature_status values (nisis.seq_feature_status.nextval, 'Down');
END;
/

commit;