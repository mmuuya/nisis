create or replace directory pictures_dir as 'C:\Users\oracle\Pictures\';

create or replace procedure upload_snapshot
(   i_blob_file IN BLOB,
    i_bfile IN BFILE,
    i_file_id IN INTEGER,
    i_file_nm IN VARCHAR2,
    i_table_nm IN VARCHAR2,
    i_obj_id IN INTEGER,
    i_user_id IN INTEGER)
is
l_blob_file BLOB            := i_blob_file;
l_bfile BFILE               := i_bfile;
l_file_id INTEGER           := i_file_id;
l_file_nm VARCHAR2(255)     := i_file_nm;
l_table_nm VARCHAR2(255)    := i_table_nm;
l_obj_id INTEGER            := i_obj_id;
l_user_id INTEGER           := i_user_id;
begin
select snapshot_id_seq.nextval into l_file_id from dual;
l_file_nm := i_file_nm;


--select file_contents into blob_file from nisis.snapshot_uploads where fileid = file_id; 

l_bfile := BFILENAME('PICTURES_DIR', l_file_nm);
dbms_lob.createtemporary(l_blob_file, false);
dbms_lob.open(l_bfile, DBMS_LOB.LOB_READONLY);
dbms_lob.loadfromfile(
            l_blob_file,
            l_bfile, 
            dbms_lob.getLength(l_bfile));
            
dbms_lob.close(l_bfile);  

insert into nisis.snapshot_uploads values (l_file_id, l_file_nm, l_blob_file, l_table_nm, l_obj_id, null, l_user_id, sysdate);

end;
/

select * from nisis.snapshot_uploads;

create or replace procedure download_snapshot
(i_table varchar2,
 i_objid integer
)
IS
l_file UTL_FILE.FILE_TYPE;
l_file_nm VARCHAR2(255);
l_buffer RAW(32767);
l_amount BINARY_INTEGER := 32767;
l_pos INTEGER := 1;
l_blob BLOB;
l_blob_len INTEGER;
BEGIN
-- Get LOB locator
SELECT doc
INTO l_blob
FROM mydocs
WHERE rownum = 1;

l_blob_len := DBMS_LOB.getlength(l_blob);

select file_nm into l_file_nm from nisis.snapshot_uploads where ref_table = i_table and ref_objid = i_objid;

-- Open the destination file.
l_file := UTL_FILE.fopen('PICTURES_DIR', l_file_nm,'WB', 32767);

-- Read chunks of the BLOB and write them to the file
-- until complete.
WHILE l_pos < l_blob_len LOOP
DBMS_LOB.read(l_blob, l_amount, l_pos, l_buffer);
UTL_FILE.put_raw(l_file, l_buffer, TRUE);
l_pos := l_pos + l_amount;
END LOOP;

-- Close the file.
UTL_FILE.fclose(l_file);

EXCEPTION
WHEN OTHERS THEN
-- Close the file if something goes wrong.
IF UTL_FILE.is_open(l_file) THEN
UTL_FILE.fclose(l_file);
END IF;
RAISE;
END;
/ 