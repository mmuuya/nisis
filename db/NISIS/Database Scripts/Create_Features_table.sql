drop table NISIS.FEATURES;

create table NISIS.FEATURES (
    nisis_id        VARCHAR2(400),
    fullname        VARCHAR2(200),
    status          VARCHAR2(10),
    modified_date   TIMESTAMP,
    modified_by     VARCHAR2(255),
    notes           VARCHAR2(500),
    active_flg      VARCHAR2(1),
    deactivate_date TIMESTAMP,
    deactivate_by   VARCHAR2(255)
    );
