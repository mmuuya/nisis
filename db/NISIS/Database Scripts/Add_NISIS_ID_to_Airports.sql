ALTER TABLE nisis.airports ADD nisis_id NUMBER;

SELECT * FROM nisis.airports ORDER BY nisis_id desc;

DECLARE
BEGIN
UPDATE NISIS.AIRPORTS SET nisis_id = seq_airports_id.nextval WHERE nisis_id IS NULL;

END;
/

ALTER TABLE nisis.airports ADD (current_status NUMBER DEFAULT 2 NOT NULL, modified_date TIMESTAMP, modified_by VARCHAR2(255), modification_status NUMBER, modification_approved_by VARCHAR2(255), inactive_flg VARCHAR2(1) DEFAULT 'N' NOT NULL);

update nisis.airports set current_status = 2, inactive_flg = 'N';

--rollback;

--update nisis.airports set nisis_id = null;

commit;

--
--DECLARE
--TYPE rc IS REF CURSOR;
--airports_rc rc;
--TYPE bulk_row IS TABLE OF NISIS.AIRPORTS%ROWTYPE;
--l_rows bulk_row; 
--l_count NUMBER;
--BEGIN
--
--SELECT COUNT(*) INTO l_count FROM NISIS.AIRPORTS WHERE nisis_id IS NULL;
--
--OPEN airports_rc FOR SELECT * FROM NISIS.AIRPORTS ORDER BY NAME ASC;
--LOOP
--    SELECT nisis_id BULK COLLECT INTO l_rows FROM nisis.airports LIMIT 500; 
--    FORALL i IN 1..l_rows.COUNT
--            UPDATE nisis.airports 
--            SET nisis_id = seq_airports_id.nextval 
--            WHERE nisis_id IS NULL;
--    EXIT WHEN airports_rc%NOTFOUND;
--END LOOP;
--CLOSE airports_rc;
--
--END;
--/ 
