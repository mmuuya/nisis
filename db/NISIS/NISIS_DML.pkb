CREATE OR REPLACE PACKAGE BODY NISIS.NISIS_DML AS
/******************************************************************************
-- Name:                nisis.nisis_dml
-- Purpose:             This package is for nisis Data Markup Language (DML) 
                            operations.
-- Developer:           Haeden Howland
-- Create Date:         06-Sep-2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/

FUNCTION f_new_id
/******************************************************************************
-- Name:            f_new_id
-- Purpose:         Accepts a feature type and (optional) parent ID, or default ID
                        as input and returns a formatted feature id.
-- Developer:       Haeden Howland
-- Create Date:     09/06/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_feature_type     VARCHAR2,
 i_parent_id        VARCHAR2 DEFAULT NULL,
 i_icao_id          VARCHAR2 DEFAULT NULL)
RETURN VARCHAR2
IS
l_id_check          NUMBER;
l_max_feature_id    NUMBER;
l_feature_code      VARCHAR2(4);
l_feature_id        VARCHAR2(8);
to_return           VARCHAR2(400);



duplicate_icao      EXCEPTION;
invalid_parent      EXCEPTION;
missing_feature     EXCEPTION;
BEGIN

IF i_parent_id IS NULL AND i_icao_id IS NOT NULL THEN
    SELECT count(*) INTO l_id_check FROM AIRPORTS WHERE ident = i_icao_id;
    IF l_id_check = 0 THEN
        to_return := i_icao_id;
    ELSE
       RAISE duplicate_icao; 
    END IF;
ELSE
    IF i_parent_id IS NOT NULL THEN
        SELECT code INTO l_feature_code FROM FEATURE_CODES WHERE lower(fullname) = lower(i_feature_type);
        IF l_feature_code IS NULL THEN
            RAISE missing_feature;
        END IF;
        /**TODO: Create NISIS_ID field in all tables that can be referenced as features**/
--        SELECT count(*) INTO l_current_features FROM FEATURES WHERE nisis_id LIKE i_parent_id||'-'||code||'%';
        SELECT NVL(MAX(SUBSTR(nisis_id, LENGTH(nisis_id)-4)), 0) INTO l_max_feature_id FROM FEATURES WHERE nisis_id LIKE i_parent_id||'-'||l_feature_code||'%';  
        
        to_return := i_parent_id||'-'||l_feature_code||to_char(l_max_feature_id+1,'0000');
        to_return := regexp_replace(to_return, '[[:space:]]*','');
    ELSE
        RAISE invalid_parent;
    END IF;
END IF;

RETURN to_return;
EXCEPTION
    WHEN duplicate_icao THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'ICAO code already in use. Invalid code: ' || i_icao_id);
        RETURN NULL;  
    WHEN missing_feature THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'A feature with the specified name could not be found: ' || i_feature_type);
        RETURN NULL; 
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
        RETURN NULL;
END f_new_id;

PROCEDURE p_add_feature_and_code
/******************************************************************************
-- Name:            p_add_feature_and_code
-- Purpose:         Accepts a full name and code pair and generates a new entry
                        in the feature_codes table
-- Developer:       Haeden Howland
-- Create Date:     09/06/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_feature_name     VARCHAR2,
 i_code             VARCHAR2)
IS
i_feature_check     NUMBER;
invalid_feature     EXCEPTION;
BEGIN 

SELECT count(*) INTO i_feature_check FROM nisis.feature_codes 
    WHERE (fullname = i_feature_name OR code = i_code);
    
IF  i_feature_check > 0 THEN
    RAISE invalid_feature;
END IF;

INSERT INTO nisis.feature_codes VALUES (feature_code_seq.nextval, i_feature_name, i_code);

EXCEPTION
    WHEN invalid_feature THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'Feature name or code already used. Fullname: ' ||i_feature_name|| ' Code: ' ||i_code);
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, NULL); 
END p_add_feature_and_code;

FUNCTION f_merge_airports
/******************************************************************************
-- Name:            f_merge_airports
-- Purpose:         Handles the database interaction of adding/updating Airport
                        information via a merge statement.
-- Developer:       Haeden Howland
-- Create Date:     09/11/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_id                       VARCHAR2 DEFAULT NULL,
 i_ident                    VARCHAR2 DEFAULT NULL,
 i_type                     VARCHAR2 DEFAULT NULL,
 i_name                     VARCHAR2 DEFAULT NULL,
 i_latitude_deg             VARCHAR2 DEFAULT NULL,
 i_longitude_deg            VARCHAR2 DEFAULT NULL,
 i_elevation_ft             VARCHAR2 DEFAULT NULL,
 i_continent                VARCHAR2 DEFAULT NULL,
 i_iso_country              VARCHAR2 DEFAULT NULL,
 i_iso_region               VARCHAR2 DEFAULT NULL,
 i_municipality             VARCHAR2 DEFAULT NULL,
 i_state                    VARCHAR2 DEFAULT NULL,
 i_nisis_id                 NUMBER DEFAULT NULL,
 i_current_status           NUMBER DEFAULT NULL,
 i_modified_date            TIMESTAMP DEFAULT NULL,
 i_modified_by              VARCHAR2 DEFAULT NULL,
 i_modification_status      NUMBER DEFAULT NULL,
 i_modification_approved_by VARCHAR2 DEFAULT NULL,
 i_inactive_flg             VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN
IS
l_rt                       airports_rt;
BEGIN

l_rt.id := i_id;
l_rt.ident := i_ident;
l_rt.type := i_type;
l_rt.name := i_name;
l_rt.latitude_deg := i_latitude_deg;
l_rt.longitude_deg := i_longitude_deg;
l_rt.elevation_ft := i_elevation_ft;
l_rt.continent  := i_continent;
l_rt.iso_country := i_iso_country;
l_rt.iso_region := i_iso_region;
l_rt.municipality := i_municipality;
l_rt.state := i_state;
l_rt.nisis_id := i_nisis_id;
l_rt.current_status := i_current_status;
--         l_rt.modified_date,
l_rt.modified_by := i_modified_by;
--         l_rt.modification_status,
--         l_rt.modification_approved_by,
l_rt.inactive_flg := i_inactive_flg;
 
MERGE INTO nisis.airports a
USING (
    SELECT 
--         i_id,
--         i_ident,
--         i_type,
--         i_name,
--         i_latitude_deg,
--         i_longitude_deg,
--         i_elevation_ft,
--         i_continent,
--         i_iso_country,
--         i_iso_region,
--         i_municipality,
--         i_state,
         l_rt.nisis_id nisis_id
--         ,
--         i_current_status,
--         i_modified_date,
--         i_modified_by,
--         i_modification_status,
--         i_modification_approved_by,
--         i_inactive_flg 
    FROM dual) b
ON (a.nisis_id = b.nisis_id)
WHEN MATCHED THEN
    UPDATE SET 
--         a.id = NVL(b.i_id, a.id),
         a.ident = NVL(l_rt.ident, a.id),
         a.type = NVL(l_rt.type, a.type),
         a.name = NVL(l_rt.name, a.name),
         a.latitude_deg = NVL(l_rt.latitude_deg, a.latitude_deg),
         a.longitude_deg = NVL(l_rt.longitude_deg, a.longitude_deg),
         a.elevation_ft = NVL(l_rt.elevation_ft, a.elevation_ft),
         a.continent = NVL(l_rt.continent, a.continent),
         a.iso_country = NVL(l_rt.iso_country, a.iso_country),
         a.iso_region = NVL(l_rt.iso_region, a.iso_region),
         a.municipality = NVL(l_rt.municipality, a.municipality),
         a.state = NVL(l_rt.state, a.state),
         a.current_status = NVL(l_rt.current_status, a.current_status),
         a.modified_date = SYSDATE,
         a.modified_by = l_rt.modified_by
--         ,
--         a.modification_status = NVL(b.i_modification_status, a.modification_status),
--         a.modification_approved_by = NVL(b.i_modification_approved_by, a.modification_approved_by),
--         a.inactive_flg = NVL(b.i_inactive_flg, a.inactive_flg) 
--    WHERE a.nisis_id = b.i_nisis_id
WHEN NOT MATCHED THEN
    INSERT (
--         a.id,
         a.ident,
         a.type,
         a.name,
         a.latitude_deg,
         a.longitude_deg,
         a.elevation_ft,
         a.continent,
         a.iso_country,
         a.iso_region,
         a.municipality,
         a.state,
         a.nisis_id,
         a.current_status,
--         a.modified_date,
--         a.modified_by,
--         a.modification_status,
--         a.modification_approved_by,
         a.inactive_flg)
    VALUES (
--         l_rt.id,
         l_rt.ident,
         l_rt.type,
         l_rt.name,
         l_rt.latitude_deg,
         l_rt.longitude_deg,
         l_rt.elevation_ft,
         l_rt.continent,
         l_rt.iso_country,
         l_rt.iso_region,
         l_rt.municipality,
         l_rt.state,
         seq_airports_id.nextval,
         1, --Default it to 'New' status
--         l_rt.modified_date,
--         l_rt.modified_by,
--         l_rt.modification_status,
--         l_rt.modification_approved_by,
         l_rt.inactive_flg);

--SELECT * INTO l_rt FROM nisis.airports WHERE nisis_id = i_nisis_id;          
--      a.id,
--         a.ident,
--         a.type,
--         a.name,
--         a.latitude_deg,
--         a.longitude_deg,
--         a.elevation_ft,
--         a.continent,
--         a.iso_country,
--         a.iso_region,
--         a.municipality,
--         a.state,
--         a.nisis_id,
--         a.current_status,
----         a.modified_date,
----         a.modified_by,
----         a.modification_status,
----         a.modification_approved_by,
--         a.inactive_flg
RETURN true;
EXCEPTION
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'i_nisis_id: ' ||i_nisis_id); 
        RETURN NULL;
END f_merge_airports;

PROCEDURE p_merge_aixm_airports
/******************************************************************************
-- Name:            p_merge_aixm_airports
-- Purpose:         Handles the database interaction of adding/updating Airport
                        information based off the 56-day AIXM load via a merge 
                        statement.
-- Developer:       Haeden Howland
-- Create Date:     10/7/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
IS
TYPE        l_aixm_aptheliport IS TABLE OF NISIS.APTHELIPORT%ROWTYPE;
l_rt        l_aixm_aptheliport;
CURSOR      l_cur IS
    SELECT * FROM APTHELIPORT;
BEGIN

--SELECT * INTO l_rt FROM APTHELIPORT;
/**
TODO: Add a check to see if record exists prior to attempting merge - if it does,
update a local variable for objectid. Otherwise, even if insert statement does not run,
SDE.NFDC_AIRPORTS_OBJECTID.nextval still evaluates and increments the sequence number,
resulting in incorrect numbering for new objects.
**/

OPEN l_cur;
LOOP    
    FETCH l_cur BULK COLLECT INTO l_rt LIMIT 1000;

FORALL x IN 1..l_rt.COUNT
    MERGE INTO SDE.MAP_NFDC_AIRPORTS a
    USING (
        SELECT
        l_rt(x).SITENUMBER "SITENUMBER",
        l_rt(x).TYPE "TYPE",
        l_rt(x).LOCATIONID "LOCATIONID",
        l_rt(x).EFFECTIVEDATE "EFFECTIVEDATE",
        l_rt(x).REGION "REGION",
        l_rt(x).DISTRICTOFFICE "DISTRICTOFFICE",
        l_rt(x).STATE "STATE",
        l_rt(x).STATENAME "STATENAME",
        l_rt(x).COUNTY "COUNTY",
        l_rt(x).COUNTYSTATE "COUNTYSTATE",
        l_rt(x).CITY "CITY",
        l_rt(x).FACILITYNAME "FACILITYNAME",
        l_rt(x).OWNERSHIP "OWNERSHIP",
        l_rt(x).USE "USE",
        l_rt(x).OWNER "OWNER",
        l_rt(x).OWNERADDRESS "OWNERADDRESS",
        l_rt(x).OWNERCSZ "OWNERCSZ",
        l_rt(x).OWNERPHONE "OWNERPHONE",
        l_rt(x).MANAGER "MANAGER",
        l_rt(x).MANAGERADDRESS "MANAGERADDRESS",
        l_rt(x).MANAGERCSZ "MANAGERCSZ",
        l_rt(x).MANAGERPHONE "MANAGERPHONE",
        l_rt(x).ARPLATITUDE "ARPLATITUDE",
        l_rt(x).ARPLATITUDES "ARPLATITUDES",
        l_rt(x).ARPLONGITUDE "ARPLONGITUDE",
        l_rt(x).ARPLONGITUDES "ARPLONGITUDES",
        l_rt(x).ARPMETHOD "ARPMETHOD",
        l_rt(x).ARPELEVATION "ARPELEVATION",
        l_rt(x).ARPELEVATIONMETHOD "ARPELEVATIONMETHOD",
        l_rt(x).MAGNETICVARIATION "MAGNETICVARIATION",
        l_rt(x).MAGNETICVARIATIONYEAR "MAGNETICVARIATIONYEAR",
        l_rt(x).CHARTNAME "CHARTNAME",
        l_rt(x).DISTANCEFROMCBD "DISTANCEFROMCBD",
        l_rt(x).DIRECTIONFROMCBD "DIRECTIONFROMCBD",
        l_rt(x).LANDAREACOVEREDBYAIRPORT "LANDAREACOVEREDBYAIRPORT",
        l_rt(x).BOUNDARYARTCCID "BOUNDARYARTCCID",
        l_rt(x).BOUNDARYARTCCCOMPUTERID "BOUNDARYARTCCCOMPUTERID",
        l_rt(x).BOUNDARYARTCCNAME "BOUNDARYARTCCNAME",
        l_rt(x).TIEINFSS "TIEINFSS",
        l_rt(x).TIEINFSSID "TIEINFSSID",
        l_rt(x).TIEINFSSNAME "TIEINFSSNAME",
        l_rt(x).TIEINFSSTOLLFREENUMBER "TIEINFSSTOLLFREENUMBER",
        l_rt(x).NOTAMFACILITYID "NOTAMFACILITYID",
        l_rt(x).NOTAMSERVICE "NOTAMSERVICE",
        l_rt(x).ACTIVIATIONDATE "ACTIVIATIONDATE",
        l_rt(x).AIRPORTSTATUSCODE "AIRPORTSTATUSCODE",
        l_rt(x).CERTIFICATIONTYPEDATE "CERTIFICATIONTYPEDATE",
        l_rt(x).FEDERALAGREEMENTS "FEDERALAGREEMENTS",
        l_rt(x).AIRSPACEDETERMINATION "AIRSPACEDETERMINATION",
        l_rt(x).CUSTOMSAIRPORTOFENTRY "CUSTOMSAIRPORTOFENTRY",
        l_rt(x).CUSTOMSLANDINGRIGHTS "CUSTOMSLANDINGRIGHTS",
        l_rt(x).MILITARYJOINTUSE "MILITARYJOINTUSE",
        l_rt(x).MILITARYLANDINGRIGHTS "MILITARYLANDINGRIGHTS",
        l_rt(x).INSPECTIONMETHOD "INSPECTIONMETHOD",
        l_rt(x).INSPECTIONGROUP "INSPECTIONGROUP",
        l_rt(x).LASTINSPECTIONDATE "LASTINSPECTIONDATE",
        l_rt(x).FUELTYPES "FUELTYPES",
        l_rt(x).AIRFRAMEREPAIR "AIRFRAMEREPAIR",
        l_rt(x).POWERPLANTREPAIR "POWERPLANTREPAIR",
        l_rt(x).BOTTLEDOXYGENTYPE "BOTTLEDOXYGENTYPE",
        l_rt(x).BULKOXYGENTYPE "BULKOXYGENTYPE",
        l_rt(x).BEACONSCHEDULE "BEACONSCHEDULE",
        l_rt(x).ATCT "ATCT",
        l_rt(x).UNICOMFREQUENCIES "UNICOMFREQUENCIES",
        l_rt(x).SEGMENTEDCIRCLE "SEGMENTEDCIRCLE",
        l_rt(x).BEACONCOLOR "BEACONCOLOR",
        l_rt(x).NONCOMMERCIALLANDINGFEE "NONCOMMERCIALLANDINGFEE",
        l_rt(x).SINGLEENGINEGA "SINGLEENGINEGA",
        l_rt(x).MULTIENGINEGA "MULTIENGINEGA",
        l_rt(x).JETENGINEGA "JETENGINEGA",
        l_rt(x).HELICOPTERSGA "HELICOPTERSGA",
        l_rt(x).OPERATIONSCOMMERCIAL "OPERATIONSCOMMERCIAL",
        l_rt(x).OPERATIONSAIRTAXI "OPERATIONSAIRTAXI",
        l_rt(x).OPERATIONSGALOCAL "OPERATIONSGALOCAL",
        l_rt(x).OPERATIONSGAITIN "OPERATIONSGAITIN",
        l_rt(x).OPERATIONSMILITARY "OPERATIONSMILITARY",
        l_rt(x).OPERATIONSDATE "OPERATIONSDATE",
        l_rt(x).AIRPORTPOSITIONSOURCE "AIRPORTPOSITIONSOURCE",
        l_rt(x).AIRPORTPOSITIONSOURCEDATE "AIRPORTPOSITIONSOURCEDATE",
        l_rt(x).AIRPORTELEVATIONSOURCE "AIRPORTELEVATIONSOURCE",
        l_rt(x).AIRPORTELEVATIONSOURCEDATE "AIRPORTELEVATIONSOURCEDATE",
        l_rt(x).TRANSIENTSTORAGE "TRANSIENTSTORAGE",
        l_rt(x).OTHERSERVICES "OTHERSERVICES",
        l_rt(x).WINDINDICATOR "WINDINDICATOR" 
        FROM dual) b
    ON (a.sitenum = b.sitenumber and a.locid = b.locationid)
    WHEN MATCHED THEN
        UPDATE SET 
--        -- a.OBJECTID = ,
--        -- a.SITENUM = b.SITENUMBER ,
        a.ARPTYPE = b.TYPE ,
--        -- a.LOCID = b.LOCATIONID ,
        a.EFFECDT = to_date(trim(b.EFFECTIVEDATE), 'MM/DD/YYYY'),
        a.REGION = b.REGION ,
        a.DISTOFFICE = b.DISTRICTOFFICE ,
        a.STATE = b.STATE ,
        a.STATENAME = b.STATENAME ,
        a.COUNTY = b.COUNTY ,
        a.CONTYSTATE = b.COUNTYSTATE ,
        a.CITY = b.CITY ,
        a.FACNAME = b.FACILITYNAME ,
        a.OWNERSHIP = b.OWNERSHIP ,
        a.USE = b.USE ,
        a.OWNER = b.OWNER ,
        a.OWNERADDR = b.OWNERADDRESS ,
        a.OWNERCSZ = b.OWNERCSZ ,
        a.OWNERPHONE = b.OWNERPHONE ,
        a.MANAGER = b.MANAGER ,
        a.MGRADDRESS = b.MANAGERADDRESS ,
        a.MANAGERCSZ = b.MANAGERCSZ ,
        a.MGRPHONE = b.MANAGERPHONE ,
        a.ARPLAT = b.ARPLATITUDE ,
        a.ARPLATS = b.ARPLATITUDES ,
----        a.ARPLATDD = ,
        a.ARPLON = b.ARPLONGITUDE ,
        a.ARPLONS = b.ARPLONGITUDES ,
----        a.ARPLONDD = ,
        a.ARPMETHOD = b.ARPMETHOD ,
        a.ARPELEV = b.ARPELEVATION ,
        a.ARPELMETH = b.ARPELEVATIONMETHOD ,
        a.MAGNVAR = b.MAGNETICVARIATION ,
        a.MAGNVARYR = b.MAGNETICVARIATIONYEAR ,
        a.CHARTNAME = b.CHARTNAME ,
        a.DISTFRMCBD = b.DISTANCEFROMCBD ,
        a.DIRFRMCBD = b.DIRECTIONFROMCBD ,
----        = b.LANDAREACOVEREDBYAIRPORT ,
        a.BDARTCCID = b.BOUNDARYARTCCID ,
        a.BDARTCCCID = b.BOUNDARYARTCCCOMPUTERID ,
        a.BDARTCCN = b.BOUNDARYARTCCNAME ,
----        a.RPARTCCID = ,
----        a.RPARTCCCID = ,
----        a.RPARTCCN = ,
        a.TIEINFSS = b.TIEINFSS ,
        a.TIEINFSSID = b.TIEINFSSID ,
        a.TIEINFSSN = b.TIEINFSSNAME ,
----        a.ARPTOFSSPH = ,
        a.TIEINFSSTF = b.TIEINFSSTOLLFREENUMBER ,
----        a.ALTFSSID = ,
----        a.ALTFSSNAME = ,
----        a.ALTFSSTF = ,
        a.NOTAMFACID = b.NOTAMFACILITYID ,
        a.NOTAMSVC = b.NOTAMSERVICE ,
        a.ACTIVDATE = to_date(trim(b.ACTIVIATIONDATE), 'MM/DD/YYYY'),
        a.ARPSTTUSCD = b.AIRPORTSTATUSCODE ,
        a.CERTTYPEDT = b.CERTIFICATIONTYPEDATE ,
        a.FEDAGREE = b.FEDERALAGREEMENTS ,
        a.AIRSPCEDET = b.AIRSPACEDETERMINATION ,
        a.CTMARPOFE = b.CUSTOMSAIRPORTOFENTRY ,
        a.CTMLDRGHTS = b.CUSTOMSLANDINGRIGHTS ,
        a.MILJTUSE = b.MILITARYJOINTUSE ,
        a.MILLDRGHTS = b.MILITARYLANDINGRIGHTS ,
        a.INSPMETH = b.INSPECTIONMETHOD ,
        a.INSPECGRP = b.INSPECTIONGROUP ,
        a.LASTINSPDT = b.LASTINSPECTIONDATE ,
----        a.LASTOWNDT = ,
        a.FUELTYPES = b.FUELTYPES ,
        a.AIRFREPAIR = b.AIRFRAMEREPAIR ,
        a.PWRPLTRPR = b.POWERPLANTREPAIR ,
        a.BOTTLDOXTP = b.BOTTLEDOXYGENTYPE ,
        a.BULKOXTP = b.BULKOXYGENTYPE ,
----        a.LIGHTSCHED = ,
        a.BEACNSCHED = b.BEACONSCHEDULE ,
        a.ATCT = b.ATCT ,
        a.CTAFFREQ = b.UNICOMFREQUENCIES ,
        a.SEGMCIRCLE = b.SEGMENTEDCIRCLE ,
        a.BEACNCOLOR = b.BEACONCOLOR ,
----        = b.NONCOMMERCIALLANDINGFEE ,
        a.SINGLENGGA = b.SINGLEENGINEGA ,
        a.MULTIENGGA = b.MULTIENGINEGA ,
        a.JETENGINEGA = b.JETENGINEGA ,
        a.HELICOGA = b.HELICOPTERSGA ,
        a.OPERCOMM = b.OPERATIONSCOMMERCIAL ,
        a.OPAIRTAXI = b.OPERATIONSAIRTAXI ,
        a.OPGALOCAL = b.OPERATIONSGALOCAL ,
        a.OPGAITIN = b.OPERATIONSGAITIN ,
        a.OPMILITARY = b.OPERATIONSMILITARY ,
        a.OPERDATE = to_date(trim(b.OPERATIONSDATE), 'MM/DD/YYYY'),
----        = b.AIRPORTPOSITIONSOURCE ,
----        = b.AIRPORTPOSITIONSOURCEDATE ,
----        = b.AIRPORTELEVATIONSOURCE ,
----        = b.AIRPORTELEVATIONSOURCEDATE ,
        a.TRANSSTRGE = b.TRANSIENTSTORAGE ,
        a.OTHERSVCES = b.OTHERSERVICES ,
        a.WINDIND = b.WINDINDICATOR,
--        a.SERVICES = ,
--        a.HUB = ,
--        a.CLASS = ,
--        a.STATUS = 3,
        a.STATUS = trunc(dbms_random.value(0,4.9)),
--        a.CREATED_USER = 'AIXM Data Load',
--        a.CREATED_DATE = SYSDATE,
        a.LAST_EDITED_USER = 'AIXM Data Load',
        a.LAST_EDITED_DATE = SYSDATE
    WHEN NOT MATCHED THEN
        INSERT( 
            OBJECTID, 
            SITENUM, 
            ARPTYPE, 
            LOCID, 
            EFFECDT, 
            REGION,  
            DISTOFFICE,  
            STATE,  
            STATENAME,  
            COUNTY, 
            CONTYSTATE,  
            CITY,  
            FACNAME,  
            OWNERSHIP,  
            USE, 
            OWNER,  
            OWNERADDR,  
            OWNERCSZ,  
            OWNERPHONE,  
            MANAGER, 
            MGRADDRESS,  
            MANAGERCSZ,  
            MGRPHONE,  
            ARPLAT,  
            ARPLATS, 
            ARPLATDD,  
            ARPLON,  
            ARPLONS,  
            ARPLONDD,  
            ARPMETHOD, 
            ARPELEV,  
            ARPELMETH, 
            MAGNVAR,  
            MAGNVARYR,  
            CHARTNAME, 
            DISTFRMCBD,  
            DIRFRMCBD,  
            BDARTCCID,  
            BDARTCCCID,  
            BDARTCCN, 
            RPARTCCID,  
            RPARTCCCID,  
            RPARTCCN,  
            TIEINFSS,  
            TIEINFSSID, 
            TIEINFSSN,  
            ARPTOFSSPH,  
            TIEINFSSTF,  
            ALTFSSID,  
            ALTFSSNAME, 
            ALTFSSTF,  
            NOTAMFACID,  
            NOTAMSVC,  
            ACTIVDATE,  
            ARPSTTUSCD, 
            CERTTYPEDT,  
            FEDAGREE,  
            AIRSPCEDET,  
            CTMARPOFE,  
            CTMLDRGHTS, 
            MILJTUSE, 
            MILLDRGHTS,  
            INSPMETH,  
            INSPECGRP,  
            LASTINSPDT, 
            LASTOWNDT,  
            FUELTYPES,  
            AIRFREPAIR,  
            PWRPLTRPR,  
            BOTTLDOXTP, 
            BULKOXTP,  
            LIGHTSCHED,  
            BEACNSCHED,  
            ATCT,  
            CTAFFREQ, 
            SEGMCIRCLE,  
            BEACNCOLOR,  
            SINGLENGGA,  
            MULTIENGGA,  
            JETENGINEGA, 
            HELICOGA,  
            OPERCOMM,  
            OPAIRTAXI,  
            OPGALOCAL,  
            OPGAITIN, 
            OPMILITARY,  
            OPERDATE,  
            TRANSSTRGE,  
            OTHERSVCES,  
            WINDIND, 
            SERVICES,  
            HUB,  
            CLASS,  
            STATUS,
            CREATED_USER,
            CREATED_DATE)
        VALUES(  
            SDE.NFDC_AIRPORTS_OBJECTID.nextval,    
            b.SITENUMBER, 
            b.TYPE, 
            b.LOCATIONID, 
            to_date(b.EFFECTIVEDATE, 'MM/DD/YYYY'), 
            b.REGION, 
            b.DISTRICTOFFICE, 
            b.STATE, 
            b.STATENAME, 
            b.COUNTY, 
            b.COUNTYSTATE, 
            b.CITY, 
            b.FACILITYNAME, 
            b.OWNERSHIP, 
            b.USE, 
            b.OWNER, 
            b.OWNERADDRESS, 
            b.OWNERCSZ, 
            b.OWNERPHONE, 
            b.MANAGER, 
            b.MANAGERADDRESS, 
            b.MANAGERCSZ, 
            b.MANAGERPHONE, 
            b.ARPLATITUDE, 
            b.ARPLATITUDES,
            CASE WHEN (b.ARPLATITUDES LIKE '%S%') THEN -1 ELSE 1 END * (TO_NUMBER(SUBSTR(b.ARPLATITUDES, 0, LENGTH(b.ARPLATITUDES)-1)/3600)), 
            b.ARPLONGITUDE, 
            b.ARPLONGITUDES,
            CASE WHEN (b.ARPLONGITUDES LIKE '%W%') THEN -1 ELSE 1 END * (TO_NUMBER(SUBSTR(b.ARPLONGITUDES, 0, LENGTH(b.ARPLONGITUDES)-1)/3600)),
            b.ARPMETHOD, 
            to_number(b.ARPELEVATION), 
            b.ARPELEVATIONMETHOD, 
            b.MAGNETICVARIATION, 
            to_number(b.MAGNETICVARIATIONYEAR), 
            b.CHARTNAME, 
            to_number(b.DISTANCEFROMCBD), 
            b.DIRECTIONFROMCBD, 
--            b.LANDAREACOVEREDBYAIRPORT, 
            b.BOUNDARYARTCCID, 
            b.BOUNDARYARTCCCOMPUTERID, 
            b.BOUNDARYARTCCNAME, 
            null,
            null,
            null,
            b.TIEINFSS, 
            b.TIEINFSSID, 
            b.TIEINFSSNAME,
            null, 
            b.TIEINFSSTOLLFREENUMBER, 
            null,
            null,
            null,
            b.NOTAMFACILITYID, 
            b.NOTAMSERVICE, 
            to_date(b.ACTIVIATIONDATE, 'MM/DD/YYYY'), 
            b.AIRPORTSTATUSCODE, 
            b.CERTIFICATIONTYPEDATE, 
            b.FEDERALAGREEMENTS, 
            b.AIRSPACEDETERMINATION, 
            b.CUSTOMSAIRPORTOFENTRY, 
            b.CUSTOMSLANDINGRIGHTS, 
            b.MILITARYJOINTUSE, 
            b.MILITARYLANDINGRIGHTS, 
            b.INSPECTIONMETHOD, 
            b.INSPECTIONGROUP, 
            to_number(b.LASTINSPECTIONDATE), 
            null,
            b.FUELTYPES, 
            b.AIRFRAMEREPAIR, 
            b.POWERPLANTREPAIR, 
            b.BOTTLEDOXYGENTYPE, 
            b.BULKOXYGENTYPE,
            null, 
            b.BEACONSCHEDULE, 
            b.ATCT, 
            to_number(b.UNICOMFREQUENCIES),
            b.SEGMENTEDCIRCLE, 
            b.BEACONCOLOR, 
--            b.NONCOMMERCIALLANDINGFEE, 
            to_number(b.SINGLEENGINEGA), 
            to_number(b.MULTIENGINEGA), 
            to_number(b.JETENGINEGA), 
            to_number(b.HELICOPTERSGA), 
            to_number(b.OPERATIONSCOMMERCIAL), 
            to_number(b.OPERATIONSAIRTAXI), 
            to_number(b.OPERATIONSGALOCAL), 
            to_number(b.OPERATIONSGAITIN), 
            to_number(b.OPERATIONSMILITARY), 
            to_date(b.OPERATIONSDATE, 'MM/DD/YYYY'), 
--            b.AIRPORTPOSITIONSOURCE, 
--            b.AIRPORTPOSITIONSOURCEDATE, 
--            b.AIRPORTELEVATIONSOURCE, 
--            b.AIRPORTELEVATIONSOURCEDATE, 
            b.TRANSIENTSTORAGE, 
            b.OTHERSERVICES, 
            b.WINDINDICATOR,
            null,
            null,
            (substr(b.TYPE, 0, 1) || b.USE || b.ATCT ),
            trunc(dbms_random.value(0,4.9)),
            'AIXM Data Load',
            SYSDATE)
            ; 
        EXIT WHEN l_cur%NOTFOUND;
        END LOOP;
    CLOSE l_cur;            
                        
    EXECUTE IMMEDIATE 'DROP TABLE aptheliport_prev_aixm_load';
    EXECUTE IMMEDIATE 'CREATE TABLE aptheliport_prev_aixm_load AS SELECT * from aptheliport';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE aptheliport';
                      
    COMMIT;                

EXCEPTION
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, 'AIXM Merge Statement'); 
END p_merge_aixm_airports;

FUNCTION f_merge_runways
/******************************************************************************
-- Name:            f_merge_runways
-- Purpose:         Handles the database interaction of adding/updating Runway
                        information via a merge statement.
-- Developer:       Haeden Howland
-- Create Date:     09/12/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/ 
(i_ogr_fid                  INTEGER DEFAULT NULL,
i_ora_geometry              SDO_GEOMETRY  DEFAULT NULL,
i_objectid                  NUMBER  DEFAULT NULL,
i_locid                     VARCHAR2  DEFAULT NULL,
i_siteno                    VARCHAR2  DEFAULT NULL,
i_fullname                  VARCHAR2  DEFAULT NULL,
i_faa_st                    VARCHAR2  DEFAULT NULL,
i_id                        VARCHAR2  DEFAULT NULL,
i_length                    VARCHAR2  DEFAULT NULL,
i_width                     VARCHAR2  DEFAULT NULL,
i_srfctype                  VARCHAR2  DEFAULT NULL,
i_srfctrtmnt                VARCHAR2  DEFAULT NULL,
i_paveclas                  VARCHAR2  DEFAULT NULL,
i_rwylghts                  VARCHAR2  DEFAULT NULL,
i_stfips                    VARCHAR2  DEFAULT NULL,
i_stpostal                  VARCHAR2  DEFAULT NULL,
i_baselat                   NUMBER  DEFAULT NULL,
i_baselong                  NUMBER  DEFAULT NULL,
i_reclat                    NUMBER  DEFAULT NULL,
i_reclong                   NUMBER  DEFAULT NULL,
i_version                   VARCHAR2  DEFAULT NULL,
i_shape_leng                NUMBER  DEFAULT NULL,
i_nisis_id                  NUMBER DEFAULT NULL,
i_current_status            NUMBER DEFAULT NULL,
--i_modified_date             TIMESTAMP(6),
i_modified_by               VARCHAR2  DEFAULT NULL,
i_modification_status       NUMBER DEFAULT NULL,
i_modification_approved_by  VARCHAR2  DEFAULT NULL,
i_inactive_flg              VARCHAR2  DEFAULT NULL
)
RETURN BOOLEAN 
IS
l_rt                       runways_rt;
BEGIN

l_rt.ogr_fid := i_ogr_fid;
l_rt.ora_geometry := i_ora_geometry;
l_rt.objectid := i_objectid;
l_rt.locid := i_locid;
l_rt.siteno := i_siteno;
l_rt.fullname := i_fullname;
l_rt.faa_st := i_faa_st;
l_rt.id  := i_id;
l_rt.length := i_length;
l_rt.width := i_width;
l_rt.srfctype := i_srfctype;
l_rt.srfctrtmnt := i_srfctrtmnt;
l_rt.paveclas := i_paveclas;
l_rt.rwylghts := i_rwylghts;
l_rt.stfips := i_stfips;
l_rt.stpostal := i_stpostal;
l_rt.baselat := i_baselat;
l_rt.baselong := i_baselong;
l_rt.reclat := i_reclat;
l_rt.reclong := i_reclong;
l_rt.version := i_version;
l_rt.shape_leng := i_shape_leng;
l_rt.nisis_id := i_nisis_id;
l_rt.current_status := i_current_status;
--         l_rt.modified_date,
l_rt.modified_by := i_modified_by;
--         l_rt.modification_status,
--         l_rt.modification_approved_by,
l_rt.inactive_flg := i_inactive_flg;
 
MERGE INTO nisis.runways a
USING (
    SELECT 
         l_rt.nisis_id nisis_id 
    FROM dual) b
ON (a.nisis_id = b.nisis_id)
WHEN MATCHED THEN
    UPDATE SET 
            a.ogr_fid = NVL(l_rt.ogr_fid, a.ogr_fid),
            a.ora_geometry = NVL(l_rt.ora_geometry, a.ora_geometry),
            a.objectid = NVL(l_rt.objectid, a.objectid),
            a.locid = NVL(l_rt.locid, a.locid),
            a.siteno = NVL(l_rt.siteno, a.siteno),
            a.fullname = NVL(l_rt.fullname, a.fullname),
            a.faa_st = NVL(l_rt.faa_st, a.faa_st),
            a.id = NVL(l_rt.id, a.id),
            a.length = NVL(l_rt.length, a.length),
            a.width = NVL(l_rt.width, a.width),
            a.srfctype = NVL(l_rt.srfctype, a.srfctype),
            a.srfctrtmnt = NVL(l_rt.srfctrtmnt, a.srfctrtmnt),
            a.paveclas = NVL(l_rt.paveclas, a.paveclas),
            a.rwylghts = NVL(l_rt.rwylghts, a.rwylghts),
            a.stfips = NVL(l_rt.stfips, a.stfips),
            a.stpostal = NVL(l_rt.stpostal, a.stpostal),
            a.baselat = NVL(l_rt.baselat, a.baselat),
            a.baselong = NVL(l_rt.baselong, a.baselong),
            a.reclat = NVL(l_rt.reclat, a.reclat),
            a.reclong = NVL(l_rt.reclong, a.reclong),
            a.version = NVL(l_rt.version, a.version),
            a.shape_leng = NVL(l_rt.shape_leng, a.shape_leng),
--            a.nisis_id = NVL(l_rt.nisis_id, a.nisis_id),
            a.current_status = NVL(l_rt.current_status, a.current_status),
            a.modified_date = SYSDATE,
            a.modified_by = NVL(l_rt.modified_by, a.modified_by),
            a.modification_status = NVL(l_rt.modification_status, a.modification_status),
            a.modification_approved_by = NVL(l_rt.modification_approved_by, a.modification_approved_by)
            --a.inactive_flg = NVL(l_rt.inactive_flg, a.inactive_flg),
WHEN NOT MATCHED THEN
    INSERT (
        ogr_fid,
        ora_geometry,
        objectid,
        locid,
        siteno,
        fullname,
        faa_st,
        id,
        length,
        width,
        srfctype,
        srfctrtmnt,
        paveclas,
        rwylghts,
        stfips,
        stpostal,
        baselat,
        baselong,
        reclat,
        reclong,
        version,
        shape_leng,
        nisis_id,
        current_status,
--        modified_date,
--        modified_by,
--        modification_status,
--        modification_approved_by,
        inactive_flg
)
    VALUES (
         l_rt.ogr_fid,
l_rt.ora_geometry,
l_rt.objectid,
l_rt.locid,
l_rt.siteno,
l_rt.fullname,
l_rt.faa_st,
l_rt.id,
l_rt.length,
l_rt.width,
l_rt.srfctype,
l_rt.srfctrtmnt,
l_rt.paveclas,
l_rt.rwylghts,
l_rt.stfips,
l_rt.stpostal,
l_rt.baselat,
l_rt.baselong,
l_rt.reclat,
l_rt.reclong,
l_rt.version,
l_rt.shape_leng,
seq_runways_id.nextval,
1, --Default current status to 'New'
--l_rt.modified_date,
--l_rt.modified_by,
--l_rt.modification_status,
--l_rt.modification_approved_by,
'N' --Default inactive to 'N'
);

RETURN true;
EXCEPTION
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, null); 
        RETURN NULL;
END f_merge_runways;

PROCEDURE f_auto_complete
/******************************************************************************
-- Name:            f_auto_complete
-- Purpose:         Accepts a string input, and returns possible matches.
-- Developer:       Haeden Howland
-- Create Date:     10/11/2013
-- Modification History:
--
--  Date        Developer Name      Description
******************************************************************************/
(i_input        VARCHAR2)
--RETURN l_cur
IS

CURSOR l_cur IS
    SELECT * FROM (
        SELECT facname, state, city 
        FROM SDE.MAP_NFDC_AIRPORTS
        WHERE UPPER(facname) LIKE UPPER(i_input)||'%'
        OR UPPER(state) LIKE UPPER(i_input)||'%'
        OR UPPER(city) LIKE UPPER(i_input)||'%'
        ORDER BY state ASC
        ) WHERE ROWNUM <= 5
        ;    
l_total     NUMBER := 1;
BEGIN

IF LENGTH(i_input) >= 3 THEN
FOR x IN l_cur LOOP
--    te_inputs.p_auto_option_add(
--        i_value     => x.id,
--        i_line_1    => initcap(x.state),
--        i_line_2    => x.abbrev);
    dbms_output.put_line(l_total||'. '||CHR(13)||x.facname || CHR(13)|| x.state ||CHR(13) ||x.city||CHR(13));
    l_total := l_total + 1;
END LOOP;

--RETURN l_cur;
END IF;

--RETURN NULL;
EXCEPTION
    WHEN others THEN
        nisis_logging.log_error(SQLCODE, SQLERRM, nisis_logging.f_get_procedure, null); 
END f_auto_complete;

END NISIS_DML;
/