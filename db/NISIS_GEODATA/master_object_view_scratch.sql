-- Master_object_view_scratch
-- from Haeden's work, these should be used as a reference when fixing the shapeless view. Many
-- datatypes likely need to change, this will show what was changed in the past (but never made it to UAT)

--------------------------------
-- SCRATCH 0 (from Haeden)
select * from nisis_airports_data_v2;

select * from nisis_atm_data;

select * from nisis_ans_data;

select * from nisis_osf_data;

--drop table nisis_atm_data_bk;

create table nisis_atm_data_bk as select * from nisis_atm_data;


CREATE OR REPLACE FORCE VIEW NISIS.MASTER_OBJECTS_VW
(
   SOURCETABLE,
   FAA_ID,
   ICAO_ID,
   AIRPORT_LONG_NAME,
   FAA_REGION,
   ATO_SERVICE_AREA,
   FEMA_REGION,
   TOWER_ID,
   TOWER_STATUS,
   APPROACH_CONTROL,
   APPROACH_CONTROL_STATUS,
   EN_ROUTE_ATC,
   EN_ROUTE_ATC_STATUS,
   OVERALL_STATUS,
   OVERALL_STATUS_EXPLANATION,
   FILTER_SHAPE_INTERSECTION,
   IWL_DESIGNATION,
   IWL_MEMBERSHIP,
   LAST_UPDATE_TO_PROFILE,
   NISIS_CI_OBJECT_CATEGORY
)
AS
select * from ( select 'nisis_airports_data_v2' SOURCETABLE,  
                        faa_id FAA_ID,  
                        icao_id ICAO_ID,  
                        lg_name AIRPORT_LONG_NAME,  
                        faa_reg FAA_REGION, 
                        ato_sa ATO_SERVICE_AREA,  
                        fema_reg FEMA_REGION,  
                        tower_id TOWER_ID,  
                        tower_sts TOWER_STATUS,  
                        appr_ctrl APPROACH_CONTROL,  
                        a_ctrl_sts APPROACH_CONTROL_STATUS,
                        enrte_atc EN_ROUTE_ATC,  
                        er_atc_sts EN_ROUTE_ATC_STATUS,  
                        ovr_status OVERALL_STATUS,  
                        ovr_sts_ex OVERALL_STATUS_EXPLANATION,
                        fs_inter FILTER_SHAPE_INTERSECTION,  
                        iwl_dsg IWL_DESIGNATION,  
                        iwl_mber IWL_MEMBERSHIP,  
                        last_updt LAST_UPDATE_TO_PROFILE,  
                        nci_ob_cat NISIS_CI_OBJECT_CATEGORY
                from nisis_airports_data_v2) 
                UNION
                (select 'nisis_atm_data' SOURCETABLE,  
                        faa_arp_id FAA_ID, null ICAO_ID,  
                        lg_name AIRPORT_LONG_NAME,  
                        faa_reg FAA_REGION, 
                        ato_sa ATO_SERVICE_AREA,  
                        fema_reg FEMA_REGION,  
                        atm_id TOWER_ID,  
                        null TOWER_STATUS,  
                        null APPROACH_CONTROL,  
                        null APPROACH_CONTROL_STATUS,
                        null EN_ROUTE_ATC,  
                        null EN_ROUTE_ATC_STATUS,  
                        ops_status OVERALL_STATUS,  
                        ops_sts_ex  OVERALL_STATUS_EXPLANATION, 
                        'No Applicable Field found' FILTER_SHAPE_INTERSECTION,  
                        m_iwl_dsg IWL_DESIGNATION, 
                        'No Applicable Field found' IWL_MEMBERSHIP,  
                        last_updt LAST_UPDATE_TO_PROFILE,  
                        nci_rel_id NISIS_CI_OBJECT_CATEGORY 
                from nisis_atm_data)
                UNION
                (select 'nisis_ans_data' SOURCETABLE,
                        ans_id FAA_ID,
                        null  ICAO_ID,
                        tec_op_fac  AIRPORT_LONG_NAME,
                        faa_reg  FAA_REGION,
                        ato_sa  ATO_SERVICE_AREA,
                        fema_reg  FEMA_REGION,
                        null  TOWER_ID,
                        null  TOWER_STATUS,
                        null  APPROACH_CONTROL,
                        null  APPROACH_CONTROL_STATUS,
                        null  EN_ROUTE_ATC,
                        null  EN_ROUTE_ATC_STATUS,
                        ans_status  OVERALL_STATUS,
                        ans_sts_ex  OVERALL_STATUS_EXPLANATION,
                        'No Applicable Field found'  FILTER_SHAPE_INTERSECTION,
                        m_iwl_dsg  IWL_DESIGNATION,
                        'No Applicable Field found'  IWL_MEMBERSHIP,
                        last_updt  LAST_UPDATE_TO_PROFILE,
                        nci_rel_id  NISIS_CI_OBJECT_CATEGORY
                from nisis_ans_data)
                UNION
                (select 'nisis_osf_data' SOURCETABLE,
                        osf_id FAA_ID,
                        null  ICAO_ID,
                        lg_name  AIRPORT_LONG_NAME,
                        null  FAA_REGION,
                        null  ATO_SERVICE_AREA,
                        null  FEMA_REGION,
                        null  TOWER_ID,
                        null  TOWER_STATUS,
                        null  APPROACH_CONTROL,
                        null  APPROACH_CONTROL_STATUS,
                        null  EN_ROUTE_ATC,
                        null  EN_ROUTE_ATC_STATUS,
                        osf_status  OVERALL_STATUS,
                        osf_sts_ex  OVERALL_STATUS_EXPLANATION,
                        'No Applicable Field found'  FILTER_SHAPE_INTERSECTION,
                        m_iwl_dsg  IWL_DESIGNATION,
                        'No Applicable Field found'  IWL_MEMBERSHIP,
                        last_updt  LAST_UPDATE_TO_PROFILE,
                        nci_rel_id  NISIS_CI_OBJECT_CATEGORY
                from nisis_osf_data)
                ;
                
select table_name, column_name, data_type, data_length from all_tab_columns where (lower(table_name) = 'nisis_airports_data_v2' or lower(table_name) = 'nisis_atm_data' or lower(table_name) = 'nisis_ans_data')
    and (lower(column_name) = 'ops_status' OR lower(column_name) = 'ovr_status'
        OR lower(column_name) = 'ans_status' ) 
    ORDER BY DATA_LENGTH, COLUMN_NAME DESC
    ;                 
    
    
alter table nisis_atm_data modify faa_arp_id VARCHAR2(10);

alter table nisis_atm_data modify atm_id VARCHAR2(10);

alter table nisis_atm_data modify last_updt DATE;

alter table nisis_atm_data modify ops_status NUMBER;

commit;

alter table nisis_ans_data modify ans_status number;

alter table nisis_ans_data modify last_updt date;

commit;

alter table nisis_osf_data modify osf_status number;

alter table nisis_osf_data modify last_updt date;

commit;

select * from master_objects_vw where faa_id = 'BWI';
    
----------------------------------------------------
-- SCRATCH 3 (from Haeden)
create table nisis_airports_bak as select * from nisis_airports;

update nisis_airports set fs_inter = null;

alter table nisis_airports modify fs_inter number; 

update nisis_airports set iwl_dsg = null;

alter table nisis_airports modify iwl_dsg number; 

--select objectid, faa_id, fs_inter from nisis_airports_bak;

--select distinct fs_inter from nisis_airports_bak;

--select count(*), objectid from nisis_airports_bak group by objectid order by 1 desc;

select distinct * from  nisis_airports_bak where objectid = 13858;

update nisis_airports n set fs_inter = (select to_number(fs_inter) from nisis_airports_bak where n.objectid <> 13858 and objectid = n.objectid); 

update nisis_airports n set fs_inter = 0 where n.objectid = 13858;

update nisis_airports n set iwl_dsg = (select to_number(iwl_dsg) from nisis_airports_bak where n.objectid <> 13858 and objectid = n.objectid); 

update nisis_airports n set iwl_dsg = 0 where n.objectid = 13858;

commit;

create table nisis_ans_bak as select * from nisis_ans;

update nisis_ans set fs_inter = null, iwl_dsgn = null;

alter table nisis_ans modify (fs_inter number, iwl_dsgn number);

--select count(*), objectid from nisis_ans_bak group by objectid order by 1 desc

update nisis_ans n set fs_inter = (select to_number(fs_inter) from nisis_ans_bak where objectid = n.objectid), iwl_dsgn = (select to_number(iwl_dsgn) from nisis_ans_bak where objectid = n.objectid);

commit; 


alter table nisis_resp modify (fs_inter number, iwl_dsg number);

commit;

grant select on nisis_airports to nisis;

grant select on nisis_atm to nisis;

grant select on nisis_ans to nisis;

grant select on nisis_osf to nisis;

grant select on nisis_resp to nisis;
