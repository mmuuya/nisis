-- shapeless views couldn't be compiled because the column data types were different across the different facility types
-- had to modify them so the data types were consistent

--do this to check the datatypes of the column that failed, they should all be the same
select * from all_tab_cols where owner = 'NISIS_GEODATA' 
and column_name = 'FS_INTER';

--select * from nisis_geodata.nisis_airports;

--select * from nisis_geodata.nisis_airports_bk;

--airports, osf, ans, atm

--backup tables
create table nisis_geodata.nisis_airports_bk as select * from nisis_geodata.nisis_airports;
create table nisis_geodata.nisis_resp_bk as select * from nisis_geodata.nisis_resp;
create table nisis_geodata.nisis_ans_bk as select * from nisis_geodata.nisis_ans;

--empty original column (can't change datatype for a non-empty column)
update nisis_geodata.nisis_airports set fs_inter = null;
update nisis_geodata.nisis_resp set fs_inter = null;
update nisis_geodata.nisis_ans set fs_inter = null;

update nisis_geodata.nisis_airports set iwl_dsg = null;
update nisis_geodata.nisis_resp set iwl_dsg = null;
update nisis_geodata.nisis_ans set iwl_dsgn = null;

--change datatypes
alter table nisis_geodata.nisis_airports modify fs_inter number;
alter table nisis_geodata.nisis_resp modify fs_inter number;
alter table nisis_geodata.nisis_ans modify fs_inter number;

alter table nisis_geodata.nisis_airports modify iwl_dsg number;
alter table nisis_geodata.nisis_resp modify iwl_dsg number;
alter table nisis_geodata.nisis_ans modify iwl_dsgn number;

--fill in the column from backup
update nisis_geodata.nisis_airports a set fs_inter = (select b.fs_inter from nisis_geodata.nisis_airports_bk b where a.objectid = b.objectid
    and a.gdb_to_date = b.gdb_to_date);
update nisis_geodata.nisis_resp a set fs_inter = (select b.fs_inter from nisis_geodata.nisis_resp_bk b where a.objectid = b.objectid
    and a.gdb_to_date = b.gdb_to_date);
update nisis_geodata.nisis_ans a set fs_inter = (select b.fs_inter from nisis_geodata.nisis_ans_bk b where a.objectid = b.objectid
    and a.gdb_to_date = b.gdb_to_date);

update nisis_geodata.nisis_airports a set iwl_dsg = (select b.iwl_dsg from nisis_geodata.nisis_airports_bk b where a.objectid = b.objectid
    and a.gdb_to_date = b.gdb_to_date);
update nisis_geodata.nisis_resp a set iwl_dsg = (select b.iwl_dsg from nisis_geodata.nisis_resp_bk b where a.objectid = b.objectid
    and a.gdb_to_date = b.gdb_to_date);
update nisis_geodata.nisis_ans a set iwl_dsgn = (select b.iwl_dsg from nisis_geodata.nisis_ans_bk b where a.objectid = b.objectid
    and a.gdb_to_date = b.gdb_to_date);

commit;

-- backups are varchar, now mains are number, check the data was pulled from the backups correctly with something like:
--select CAST(fs_inter AS VARCHAR2(20)) from nisis_geodata.nisis_airports minus select  CAST(fs_inter AS VARCHAR2(20)) from nisis_geodata.nisis_airports_bk;
