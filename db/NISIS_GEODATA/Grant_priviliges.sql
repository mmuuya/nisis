select * from user_tab_columns where table_name = 'NISIS_AIRPORTS' order by column_id asc;

grant select on nisis_airports to nisis_admin;

grant select on nisis_ans to nisis_admin;

grant select on nisis_artcc to nisis_admin;

grant select on nisis_osf to nisis_admin;

grant select on nisis_resp to nisis_admin;

grant select on nisis_atm to nisis_admin;

grant select on nisis_iwa to nisis_admin;

revoke select on nisis_airports from nisis_admin;

revoke select on nisis_ans from nisis_admin;

revoke select on nisis_artcc from nisis_admin;

revoke select on nisis_osf from nisis_admin;

revoke select on nisis_resp from nisis_admin;

revoke select on nisis_atm from nisis_admin;

revoke select on nisis_iwa from nisis_admin;