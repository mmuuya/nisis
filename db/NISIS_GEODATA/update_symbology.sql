UPDATE nisisdata.diff_nisis_airports
     SET symbology =
           (CASE
               WHEN arp_type = 'AIRPORT' THEN 'A'
               WHEN arp_type = 'HELIPORT' THEN 'H'
               WHEN arp_type = 'SEAPLANE BASE' THEN 'S'
               WHEN arp_type = 'BALLOONPORT' THEN 'B'
               WHEN arp_type = 'GLIDERPORT' THEN 'G'
               WHEN arp_type = 'ULTRALIGHT' THEN 'U'
               ELSE 'X' --> These won't be displayed because we don't have icons
            END)
        || (CASE WHEN basic_use = 'PR' THEN 'PR' WHEN basic_use = 'PU' THEN 'PU' --  WHEN basic_use = 'MG' THEN 'MG'
               ELSE 'XX' END)
        || (CASE WHEN tower = 'Y' THEN 'Y' ELSE 'N' END)
        || (CASE
               WHEN arp_class = 'P' THEN 'P'
               WHEN arp_class = 'GA' THEN 'G'
               WHEN arp_class = 'CS' THEN 'C'
               ELSE 'X'
            END)
        || (CASE
               WHEN hub_type = 'L' THEN 'L'
               WHEN hub_type = 'M' THEN 'M'
               WHEN hub_type = 'S' THEN 'S'
               WHEN hub_type = 'N' THEN 'N'
               ELSE 'X'
            END)
       WHERE arp_type IS NOT NULL;

      COMMIT;

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'APUYXX'
       WHERE symbology LIKE 'APUYG%' OR symbology LIKE 'APUYC%' OR symbology LIKE 'APUYX%';

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'APUNXX'
       WHERE symbology LIKE 'APUN%';

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = REPLACE (symbology, 'PR', 'MG')
       WHERE faa_id IN (SELECT faa_id FROM nisisdata.military_airports);

      COMMIT;

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'AMGYXX'
       WHERE symbology LIKE 'AMGY%';

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'AMGNXX'
       WHERE symbology LIKE 'AMGN%';
       
      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'APRXXX'
       WHERE symbology LIKE 'APR%';

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'HXXXXX'
       WHERE symbology LIKE 'H%';

      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'SXXXXX'
       WHERE symbology LIKE 'S%';
       
      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'UXXXXX'
       WHERE symbology LIKE 'U%';
       
      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'GXXXXX'
       WHERE symbology LIKE 'G%';
       
      UPDATE nisisdata.diff_nisis_airports
         SET symbology = 'BXXXXX'
       WHERE symbology LIKE 'B%';
       
      COMMIT;