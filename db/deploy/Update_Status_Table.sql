/*
    Updates the tfr_status table to use the newly-defined list of statuses:
    Draft, Submitted, Rejected, Published, Pending, Expired.
    
    Haeden 4/3/15
*/

--select * from tfruser.lookup_status;

truncate table tfruser.lookup_status;

insert into tfruser.lookup_status values (1, 'DRAFT');

insert into tfruser.lookup_status values (2, 'SUBMITTED');

insert into tfruser.lookup_status values (3, 'REJECTED');

insert into tfruser.lookup_status values (4, 'PUBLISHED');

insert into tfruser.lookup_status values (5, 'PENDING');

insert into tfruser.lookup_status values (6, 'EXPIRED');

commit;