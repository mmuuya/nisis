select type, template from tfruser.lookup_tfr_types;

select type, replace(template, '{COMMON NAME}, ', '') from tfruser.lookup_tfr_types;

update tfruser.lookup_tfr_types set template = replace(template, '{COMMON NAME}, ', '');

commit;