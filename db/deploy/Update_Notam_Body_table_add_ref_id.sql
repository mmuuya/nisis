/*
    Creates a backup of the notams_body table and then adds a new field for tracking
    reference ID returned upon successful submission to USNOF. Restores records from backup 
    into new table.
    
    Haeden 04/03/2015
    
    Updated script to add a column to track USNS Work ID and also the Closest VOR selected from TFR Builder.
    Haeden 04/14/2015
*/

declare
l_count number := 0;
l_owner varchar2(200);
l_table_name varchar2(200);
begin

l_owner := 'TFRUSER';
l_table_name := 'BAK_NOTAM_BODY';

select count(*) into l_count from all_tables where owner = l_owner and table_name = l_table_name;

if l_count > 0 then
    execute immediate 'drop table ' || l_owner || '.' || l_table_name;
end if;


end;
/

create table tfruser.bak_notam_body as select * from tfruser.notam_body;

ALTER TABLE TFRUSER.NOTAM_BODY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TFRUSER.NOTAM_BODY CASCADE CONSTRAINTS;

CREATE TABLE TFRUSER.NOTAM_BODY
(
  NOTAM_ID             NUMBER                   NOT NULL,
  NOTAM_TYPE           VARCHAR2(100 BYTE)       NOT NULL,
  NOTAM_NAME           VARCHAR2(200 BYTE)       NOT NULL,
  STATUS               NUMBER,
  USER_ID              NUMBER                   NOT NULL,
  LOAD_DATE            TIMESTAMP(6),
  UPDATED_BY           NUMBER,
  UPDATE_DATE          TIMESTAMP(6),
  ARTCC_ID             VARCHAR2(4 BYTE),
  ARTCC_NAME           VARCHAR2(100 BYTE),
  EFFECTIVE_DATE_UTC   NUMBER                   DEFAULT 0,
  EXPIRATION_DATE_UTC  NUMBER                   DEFAULT 0,
  CUSTOM_TEXT          INTEGER                  DEFAULT 0,
  WORK_NUMBER_ID       VARCHAR2(20 BYTE),
  USNS_STATUS          VARCHAR2(200 BYTE),
  REFERENCE_VOR        VARCHAR2(200 BYTE)
);


CREATE UNIQUE INDEX TFRUSER.PK_NOTAM_ID ON TFRUSER.NOTAM_BODY
(NOTAM_ID);

ALTER TABLE TFRUSER.NOTAM_BODY ADD (
  CONSTRAINT BOOLEAN_CT
  CHECK (custom_text in (0,1))
  ENABLE VALIDATE,
  CONSTRAINT PK_NOTAM_ID
  PRIMARY KEY
  (NOTAM_ID)
  USING INDEX TFRUSER.PK_NOTAM_ID
  ENABLE VALIDATE);
  
select * from tfruser.notam_body;
  
insert into tfruser.notam_body (notam_id, notam_type, notam_name, status, user_id, load_date, updated_by, update_date, artcc_id, artcc_name,
effective_date_utc, expiration_date_utc, custom_text) select notam_id, notam_type, notam_name, status, user_id, load_date, updated_by, update_date, artcc_id, artcc_name,
effective_date_utc, expiration_date_utc, custom_text from tfruser.bak_notam_body; 

commit;