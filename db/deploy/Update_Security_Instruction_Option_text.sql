/*
    Script to update the lookup instruction option verbiage for Security 99.7-type TFRs
    to the text provided by SOSC.
    
    Haeden 4/20/15
*/

select * from tfruser.lookup_inst_options where ID = 3;

update tfruser.lookup_inst_options set name = '1. AIRCRAFT ARRIVING OR DEPARTING AIRPORTS WITHIN THIS TFR. AIRCRAFT MUST HAVE FILED AN IFR/VFR FLIGHT PLAN, BE SQUAWKING AN ATC DISCRETE CODE PRIOR TO DEPARTURE AND AT ALL TIMES WHILE IN THE TFR AND MUST REMAIN IN TWO-WAY RADIO COMMUNICATIONS WITH ATC.' where ID = 3;

commit;

