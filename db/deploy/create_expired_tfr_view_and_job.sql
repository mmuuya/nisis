/*
    This script creates a view for expired TFRs which are not in 'DRAFT' or 'EXPIRED'
    status.
    It then creates procedures that use the contents of that view to expire applicable
    TFRs.
    Lastly, it creates a procedure for creating a job to run the execution every 4 hours,
    and a secondary procedure for dropping the job (if necessary).
    
    Haeden 04/03/2015
*/


--Run as SYS or SYSTEM
Grant create job to TFRUSER;

CREATE OR REPLACE VIEW TFRUSER.EXPIRED_TFRS_VW
AS
select a.notam_id, (select status_text from tfruser.lookup_status where status_id = b.status) status, to_date('1970-01-01', 'YYYY-MM-DD') + 
        numtodsinterval(a.expiration_date_time/1000, 'SECOND') expiration_date_time 
from
    (select 
        notam_id, max(expiration_date_time) expiration_date_time 
    from tfruser.notam_tfr_date_time 
    group by notam_id) a join tfruser.notam_body b
    on a.notam_id = b.notam_id
    where to_date('1970-01-01', 'YYYY-MM-DD') + 
        numtodsinterval(a.expiration_date_time/1000, 'SECOND') < sysdate
    and b.status not in (select status_id from TFRUSER.LOOKUP_STATUS where status_text in ('DRAFT', 'EXPIRED')) 
order by notam_id asc;



CREATE OR REPLACE PROCEDURE TFRUSER.EXPIRE_TFRS
/*******************************************************************************
-- Name:            EXPIRE_TFRS
-- Purpose:         Queries the Expired_TFRs_vw view and updates the status of the 
                    returned records
-- Developer:       Haeden Howland
-- Create Date:     04/03/2015
-- Last Modified:   none
*******************************************************************************/
IS
TYPE expired_tfrs_t IS TABLE OF tfruser.expired_tfrs_vw.notam_id%TYPE;
l_expired_tfrs expired_tfrs_t;
l_expired NUMBER;
BEGIN
SELECT notam_id
  BULK COLLECT INTO l_expired_tfrs
  FROM tfruser.expired_tfrs_vw;

SELECT STATUS_ID INTO l_expired FROM TFRUSER.LOOKUP_STATUS WHERE STATUS_TEXT LIKE 'EXPIRED';

FORALL t IN 1..l_expired_tfrs.COUNT
    UPDATE TFRUSER.NOTAM_BODY 
    SET STATUS = l_expired 
    WHERE NOTAM_ID = l_expired_tfrs(t);

COMMIT;
END EXPIRE_TFRS;




CREATE OR REPLACE PROCEDURE tfruser.create_tfr_expiration_job
/*******************************************************************************
-- Name:            create_tfr_expiration_job
-- Purpose:         Creates DBMS Job Schedule for querying the expired TFRs view
                    and then updating the status of the results to show expired.
-- Developer:       Haeden Howland
-- Create Date:     04/03/2015
-- Last Modified:   none
*******************************************************************************/
IS
BEGIN

BEGIN
    dbms_scheduler.drop_job('TFR_EXPIRE_JOB', TRUE);
EXCEPTION
    WHEN OTHERS THEN NULL;

END;

dbms_scheduler.create_job(
    job_name        => 'TFR_EXPIRE_JOB',
    job_type        => 'STORED_PROCEDURE',
    job_action      => 'TFRUSER.EXPIRE_TFRS',
    start_date      => dbms_scheduler.stime,
    repeat_interval => 'FREQ=HOURLY;INTERVAL=4',
    --end_date      => dbms_scheduler.stime+1,
    enabled         => TRUE,
    auto_drop       => FALSE,
    comments        => 'Expire TFRs job');

END create_tfr_expiration_job;


declare
begin
tfruser.create_tfr_expiration_job();
end;
/