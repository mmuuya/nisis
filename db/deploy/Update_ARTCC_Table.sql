/*
    Script for updating the lookup_artccs table in the NISIS Schema to remove the "None" option as well as
    update the Lat/Long for Guam and San Juan.
    
    Haeden - 3/31/2015
*/


select * from nisis.lookup_artccs;

delete from nisis.lookup_artccs where artcc_id like 'NONE';

update nisis.lookup_artccs set lat = 18.55518, lon = -65.17301 where artcc_id = 'ZSU';

update nisis.lookup_artccs set lat = 13.50000, lon = 144.80000 where artcc_id = 'ZUA';

commit;