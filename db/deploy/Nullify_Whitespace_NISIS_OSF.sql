/*
	Script for resolving an issue with OSF Profiles not saving due to
	invalid whitespace being saved into various fields

	Kofi Honu 4/21/15
*/

UPDATE NISIS_GEODATA.NISIS_OSF SET AOS_LVL = NULL WHERE AOS_LVL = ' ';
COMMIT;

UPDATE NISIS_GEODATA.NISIS_OSF SET CRT_S_LVL = NULL WHERE CRT_S_LVL = ' ';
COMMIT;

UPDATE NISIS_GEODATA.NISIS_OSF SET AOPA_4S = NULL WHERE AOPA_4S = ' ';
COMMIT;