--------------------------------------
--------------------------------------
-- CREATE SEQUENCES
--------------------------------------
--------------------------------------
DROP SEQUENCE NISIS.ALERTS_SEQ;
CREATE SEQUENCE NISIS.ALERTS_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


--------------------------------------
--------------------------------------
-- CREATE TABLES
--------------------------------------
--------------------------------------
DROP TABLE NISIS.ALERTS;

CREATE TABLE NISIS.ALERTS
(
  ALERTID         NUMBER,
  OBJECTID        INTEGER,
  FAC_ID          VARCHAR2(10 BYTE),
  STATUS_TYPE_ID  NUMBER,
  CURRENT_STATUS  NUMBER,
  FUTURE_STATUS   NUMBER,
  ALERT_DATE      DATE,
  CREATED_BY      NUMBER(20),
  CREATED_DATE    DATE            DEFAULT SYSDATE,
  MODIFIED_BY     NUMBER(20), 
  MODIFIED_DATE   DATE,
  INACTIVATED_BY  NUMBER(20), 
  INACTIVATED_DATE    DATE,
  ACTIVE          VARCHAR2(1 BYTE)
);


CREATE UNIQUE INDEX NISIS.ALARM_PK ON NISIS.ALERTS
(ALERTID);


CREATE OR REPLACE TRIGGER NISIS.ALERTS_TRG
BEFORE INSERT
ON NISIS.ALERTS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
-- For Toad:  Highlight column ALERTID
  :new.ALERTID := ALERTS_SEQ.nextval;
END ALERTS_TRG;
/


ALTER TABLE NISIS.ALERTS ADD (
  CONSTRAINT ALARM_PK
  PRIMARY KEY
  (ALERTID)
  USING INDEX NISIS.ALARM_PK
  ENABLE VALIDATE);


--------------------------------------
--------------------------------------
-- (RE)CREATE VIEWS
--------------------------------------
--------------------------------------
DROP VIEW NISIS.ALERT_VW;
/* Formatted on 6/20/2014 2:55:15 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.ALERT_VW
(
   ALERTID,
   OBJECTID,
   FAC_ID,
   FAC_TYPE,
   STATUS_TYPE_ID,
   STATUS_FULLNAME,
   CURRENT_STATUS,
   FUTURE_STATUS,
   ALERT_DATE,
   CREATED_BY,
   CREATED_DATE,
   ACTIVE
)
AS
   SELECT ALERTID,
          OBJECTID,
          FAC_ID,
          FAC_TYPE,
          STATUS_TYPE_ID,
          STATUS_FULLNAME,
          CURRENT_STATUS,
          FUTURE_STATUS,
          ALERT_DATE,
          CREATED_BY,
          CREATED_DATE,
          ACTIVE
     FROM NISIS.ALERTS, NISIS.FAC_STATUS_TYPE
    WHERE     ALERTS.STATUS_TYPE_ID = FAC_STATUS_TYPE.STATUS_ID
          AND ACTIVE = 'Y'
          AND CURRENT_STATUS != FUTURE_STATUS;



DROP VIEW NISIS.ALERT_GROUP_NM_TMP;
/* Formatted on 6/20/2014 3:01:28 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.ALERT_GROUP_NM_TMP
(
   ALERTID,
   GROUPID,
   NAME
)
AS
   SELECT a.alertid, a.groupid, g.name
     FROM alert_group a JOIN nisis_admin.groups g ON a.groupid = g.groupid;


DROP VIEW NISIS.USER_ALERTS_VW;
/* Formatted on 6/20/2014 3:03:07 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.USER_ALERTS_VW
(
   REF_ALERTID,
   ALERT_GROUPID,
   NISIS_ADMIN.GROUPS_GROUPID,
   NAME,
   USERID
)
AS
   SELECT a.alertid ref_alertid,
          --a.objectid ref_objectid,
          ag.groupid alert_groupid,
          g.groupid NISIS_ADMIN.groups_groupid,
          g.name name,
          u.userid userid
     FROM nisis_admin.user_group u
          JOIN nisis_admin.groups g ON u.groupid = g.groupid
          JOIN nisis.alert_group ag ON ag.groupid = g.groupid
          JOIN nisis.alerts a ON a.alertid = ag.alertid;

-- need grant nisis.user_group -> nisis select
GRANT SELECT ON NISIS_ADMIN.USER_GROUP TO NISIS;



--------------------------------------
--------------------------------------
-- FUNCTIONS AND PROCEDURES
--------------------------------------
--------------------------------------

CREATE OR REPLACE FUNCTION NISIS.insert_alert (
   in_objid          IN alerts.objectid%TYPE
  ,in_facid          IN alerts.fac_id%TYPE
  ,in_statustypeid   IN alerts.status_type_id%TYPE
  ,in_status         IN alerts.current_status%TYPE
  ,in_futstatus      IN alerts.future_status%TYPE
  ,in_alertdate      IN VARCHAR2
  ,in_qformat        IN VARCHAR2
  ,in_createdby      IN alerts.created_by%TYPE
  ,in_active         IN alerts.active%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO alerts (
                  objectid
                 ,fac_id
                 ,status_type_id
                 ,current_status
                 ,future_status
                 ,alert_date
                 ,created_by
                 ,active
               )
   VALUES (in_objid
          ,in_facid
          ,in_statustypeid
          ,in_status
          ,in_futstatus
          ,TO_DATE (in_alertdate, in_qformat)
          ,in_createdby
          ,in_active)
   RETURNING alertid
     INTO v;

   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN -1;
END insert_alert;
/

-- CREATE NEW NISIS_ADMIN.GROUPS

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (8,'AJR-2', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (9,'ASH / AEO', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (10,'ATO / AJW', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (11,'ATO / ESA', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (12,'ATO / CSA', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (13,'ATO / WSA', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (14,'FAA Regions', SYSDATE);

INSERT INTO NISIS_ADMIN.GROUPS (GROUPID, NAME, DATECREATED) VALUES (15,'USDOT ERT', SYSDATE);


DROP SEQUENCE NISIS_ADMIN.SEQ_GROUP_ID;
CREATE SEQUENCE NISIS_ADMIN.SEQ_GROUP_ID
  START WITH 16
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


GRANT SELECT ON NISIS_ADMIN.SEQ_GROUP_ID TO NISIS;


-- create map groups to read table

CREATE OR REPLACE FUNCTION NISIS_ADMIN.map_group_to_r_table (in_gid IN NUMBER, in_tid IN NUMBER)
   RETURN VARCHAR2
IS
cnt NUMBER := 0;
BEGIN
   MERGE INTO nisis_admin.acl_groups g
        USING (SELECT f.fieldid,
                      f.col_nm,
                      f.display_nm,
                      f.tableid,
                      p.aclid aclid,
                      p.fieldid perm_fieldid,
                      p.perm,
                      (SELECT in_gid FROM DUAL) gid
                 FROM nisis_admin.acl_fields f, nisis_admin.acl_perms p
                WHERE     p.fieldid = f.fieldid
                      AND f.tableid = in_tid
                      AND p.perm = 'r') h
           ON (g.groupid = h.gid AND g.aclid = h.aclid)
   WHEN NOT MATCHED
   THEN
      INSERT     (groupid, aclid)
          VALUES (h.gid, h.aclid);
   cnt := SQL%ROWCOUNT;
   COMMIT;
   RETURN cnt;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN 'ERROR: '||SQLERRM;
END;
/

--needs grant EXECUTE on nisis
GRANT EXECUTE ON NISIS_ADMIN.map_group_to_r_table TO NISIS;


-- recreate "w" and "r" perms in nisis_admin.perms table
-- [1000+ inserts removed]


--------------
---- FINISH - COMMIT
--------------
COMMIT;