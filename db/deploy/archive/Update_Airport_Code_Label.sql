/*
    9/2/2014 - Haeden Howland
    Script updates "New Airport" text for airport picklists to read as "Unknown" instead.
*/


declare
picklist_id number;

begin
    --Results should be picklist id = 11
    select picklist into picklist_id from nisis.picklist where description = 'APT STATUS OPTIONS';

    update nisis.picklist_items set label = 'Unknown' where label = 'New Airport' and picklist = picklist_id;
    
    --The following can be used to test for correctness.      
--    select * from nisis.picklist_items where picklist = 11 order by value asc;

    commit;
end;
/    
