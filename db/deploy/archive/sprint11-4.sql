----------
--- ACL_TABLES CHANGES TO SUPPORT ALERTS
----------
--data needs to stay the same between dev & uat, so don't track the id's anymore - some of these may not exist in UAT, feel free to ignore
drop trigger nisis_admin.acl_tables_trg;
drop trigger nisis_admin.acl_perms_trg;

drop sequence nisis_admin.acl_fields_seq;
drop sequence nisis_admin.acl_perms_seq;
drop sequence nisis_admin.acl_tables_seq;

DROP TABLE NISIS_ADMIN.ACL_TABLES CASCADE CONSTRAINTS;

CREATE TABLE NISIS_ADMIN.ACL_TABLES
(
  TABLEID     NUMBER,
  TABLE_NM    VARCHAR2(100 BYTE),
  DISPLAY_NM  VARCHAR2(100 BYTE),
  TABLE_PK    VARCHAR2(30 BYTE),
  FAC_TYPE    VARCHAR2(6 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

GRANT SELECT ON NISIS_ADMIN.ACL_TABLES TO NISIS;

Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM)
 Values
   (3, 'NISIS_ARTCC', 'NISIS_ARTCC');
Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM, TABLE_PK, FAC_TYPE)
 Values
   (4, 'NISIS_AIRPORTS', 'NISIS_AIRPORTS', 'FAA_ID', 'APT');
Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM, TABLE_PK, FAC_TYPE)
 Values
   (5, 'NISIS_ATM', 'NISIS_ATM', 'ATM_ID', 'ATM');
Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM)
 Values
   (6, 'NISIS_IWA', 'NISIS_IWA');
Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM, FAC_TYPE)
 Values
   (7, 'NISIS_RESP', 'NISIS_RESP', 'RR');
Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM, TABLE_PK, FAC_TYPE)
 Values
   (8, 'NISIS_OSF', 'NISIS_OSF', 'OSF_ID', 'OSF');
Insert into NISIS_ADMIN.ACL_TABLES
   (TABLEID, TABLE_NM, DISPLAY_NM, TABLE_PK, FAC_TYPE)
 Values
   (9, 'NISIS_ANS', 'NISIS_ANS', 'ANS_ID', 'ANS');


----------
--- ALERTS CHANGES
----------

grant select on nisis_admin.acl_tables to nisis;

grant select on nisis_admin.acl_fields to nisis;

CREATE OR REPLACE FORCE VIEW NISIS.FAC_STATUS_TYPE_VW
(
   STATUS_ID,
   STATUS_COLUMN,
   STATUS_FULLNAME,
   FAC_TYPE,
   PICKLIST
)
AS
    SELECT a.fieldid AS status_id,
              a.col_nm AS status_column,
              a.display_nm AS status_fullname,
              b.fac_type,
              a.picklist
         FROM NISIS_ADMIN.ACL_FIELDS a, NISIS_ADMIN.ACL_TABLES b
         WHERE a.tableid = b.tableid;

CREATE OR REPLACE FUNCTION NISIS.get_fac_sts (in_fac_type               VARCHAR2
                                       ,in_colname                VARCHAR2
                                       ,in_fac_id                 VARCHAR2
                                       ,in_status_type_id         NUMBER
                                       ,in_table_nm         IN VARCHAR2
                                       ,in_table_pk   IN VARCHAR2)
   RETURN VARCHAR2
IS
   current_status   VARCHAR2 (200) := NULL;
   stmt             VARCHAR2 (4000)
      :=    'SELECT b.value||'':''||b.label 
            FROM nisis_admin.acl_fields a, nisis.picklist_items b, nisis_admin.acl_tables c 
            WHERE a.tableid = c.tableid 
            AND a.picklist = b.picklist 
            AND c.fac_type = :in_fac_type 
            AND a.fieldid=:in_status_type_id 
            AND b.VALUE = (SELECT '
                             || in_colname
                             || ' FROM nisis_geodata.'
                             || in_table_nm
                             || ' WHERE '
                             || in_table_pk
                             || ' = :in_fac_id AND gdb_to_date > sysdate)';
BEGIN
   EXECUTE IMMEDIATE stmt
      INTO current_status
      USING in_fac_type, in_status_type_id, in_fac_id;

   RETURN current_status;
END;
/
--*************************************************************

CREATE OR REPLACE FUNCTION NISIS.get_fac_sts_future (
   in_fac_type           IN VARCHAR2
  ,in_status_type_id     IN NUMBER
  ,in_future_status_id   IN NUMBER)
   RETURN VARCHAR2
IS
   future_status   VARCHAR2 (200) := NULL;
   stmt            VARCHAR2 (4000)
      := 'SELECT b.label 
            FROM nisis_admin.acl_fields a, nisis.picklist_items b, nisis_admin.acl_tables c 
            WHERE a.tableid = c.tableid 
            AND a.picklist = b.picklist 
            AND c.fac_type = :in_fac_type 
            AND a.fieldid = :in_status_type_id 
            AND b.VALUE = :in_future_status_id';
BEGIN
   EXECUTE IMMEDIATE stmt
      INTO future_status
      USING in_fac_type, in_status_type_id, in_future_status_id;

   RETURN future_status;
END;
/

--*************************************************************


CREATE OR REPLACE FORCE VIEW NISIS.ALERT_VW
(
   ALERTID,
   OBJECTID,
   FAC_ID,
   STATUS_TYPE_ID,
   FUTURE_STATUS,
   FUTURE_STATUS_LABEL,
   ALERT_DATE,
   CREATED_BY,
   CREATED_DATE,
   INACTIVATED_BY,
   INACTIVATED_DATE,
   ACTIVE,
   FAC_TYPE,
   COL_NM,
   DISPLAY_NM,
   CURRENT_STATUS,
   CURRENT_STATUS_LABEL
)
AS
   SELECT alertid,
          objectid,
          fac_id,
          status_type_id,
          future_status,
          future_status_label,
          alert_date,
          created_by,
          created_date,
          inactivated_by,
          inactivated_date,
          active,
          fac_type,
          col_nm,
          display_nm,
          SUBSTR (current_status, 1, INSTR (current_status, ':') - 1)
             AS current_status,
          SUBSTR (current_status, INSTR (current_status, ':') + 1)
             AS current_status_label
     FROM (SELECT a.alertid,
                  a.objectid,
                  a.fac_id,
                  a.status_type_id,
                  a.future_status,
                  get_fac_sts_future (c.fac_type,
                                      a.status_type_id,
                                      a.future_status)
                     AS future_status_label,
                  a.alert_date,
                  a.created_by,
                  a.created_date,
                  a.inactivated_by,
                  a.inactivated_date,
                  a.active,
                  c.fac_type,
                  b.col_nm,
                  b.display_nm,
                  get_fac_sts (c.fac_type,
                               b.col_nm,
                               a.fac_id,
                               a.status_type_id,
                               c.table_nm,
                               c.table_pk)
                     AS current_status
             FROM nisis.alerts a, nisis_admin.acl_fields b, nisis_admin.acl_tables c
            WHERE b.tableid = c.tableid 
            AND a.status_type_id = b.fieldid);

---------------------------------------------------------------
            TRUNCATE TABLE nisis.alerts;
            TRUNCATE TABLE nisis.alert_group;
            TRUNCATE TABLE NISIS.ALERT_STATUS_COLS;
---------------------------------------------------------------


Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (107, 'APT');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (264, 'ATM');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (77, 'APT');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (82, 'APT');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (96, 'APT');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (161, 'ANS');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (169, 'ANS');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (91, 'APT');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (427, 'APT');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (267, 'ATM');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (276, 'ATM');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (280, 'ATM');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (346, 'OSF');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (360, 'OSF');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (364, 'OSF');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (370, 'OSF');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (374, 'OSF');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (353, 'OSF');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (259, 'ATM');
Insert into NISIS.ALERT_STATUS_COLS
   (DATAFIELD_ID, FAC_TYPE)
 Values
   (253, 'ATM');
COMMIT;