-- add procedures to external links

insert into nisis.nisis_external_links (LINK_NAME, URL)
    values ('procedures', 'http://airnav.com/airport/[FAA_ID]#ifr');

--DROP VIEW NISIS.ALL_FIELD_PICKLISTS;

/*
Kofi's all_field_picklist view needs to be imported into UAT as the dashboard
functionality I have been working on depends on this view.

-Haeden 9/10/2014
*/

/* Formatted on 9/10/2014 12:51:43 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.ALL_FIELD_PICKLISTS
(
   TABLEID,
   TABLE_NM,
   TABLE_DISPLAY_NM,
   COL_NM,
   FIELD_DISPLAY_NM,
   FIELDID,
   PICKLIST,
   DESCRIPTION,
   LABEL,
   VALUE,
   IMGID
)
AS
   SELECT t.tableid,
          t.table_nm,
          t.display_nm AS table_display_nm,
          f.col_nm,
          f.display_nm AS field_display_nm,
          f.fieldid,
          pp.picklist,
          PP.DESCRIPTION,
          PI.LABEL,
          pi.VALUE,
          PI.IMGID
     FROM nisis_admin.acl_fields f,
          nisis_admin.acl_tables t,
          nisis.picklist pp,
          nisis.picklist_items pi
    WHERE     f.tableid = t.tableid
          AND f.picklist = pp.picklist
          AND pi.picklist = pp.picklist;
