/*
    Script for generating extra tables for NOTAM saving/loading post push to Test
    -Haeden 02/25/15
*/

declare
l_count number := 0;
begin
select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_AREA_SHAPES';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_area_shapes cascade constraints';
end if;

--select count(*) into l_count from all_tab_cols where owner like 'TFRUSER' and table_name like 'NOTAM_AREA' and column_name like 'SHAPE_ID'; 
--if l_count > 0 then
--    execute immediate 'update tfruser.notam_area_shapes set shape_id value null';
--end if;
end;
/
create table tfruser.notam_area_shapes (notam_id number, area_id number, shape_id number, foreign key (notam_id) references tfruser.notam_body(notam_id));

select * from all_tab_cols where owner like 'TFRUSER' and table_name like 'NOTAM_AREA' and column_name like 'SHAPE_ID';

select * from tfruser.notam_area;

--truncate table tfruser.notam_tfr_date_time;

--ALTER TABLE TFRUSER.NOTAM_TFR_DATE_TIME
--MODIFY(EFFECTIVE_DATE_TIME TIMESTAMP(6));

--ALTER TABLE TFRUSER.NOTAM_TFR_DATE_TIME
--MODIFY(EXPIRATION_DATE_TIME TIMESTAMP(6));

commit;

--update tfruser.notam_area_shapes set shape_id =null;  