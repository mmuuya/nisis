--ALERTS STUFF

--recreate picklist
--recreate picklist_items
CREATE TABLE NISIS.PICKLIST
(
  PICKLIST     NUMBER,
  DESCRIPTION  VARCHAR2(100 BYTE)
);

CREATE TABLE NISIS.PICKLIST_ITEMS
(
  PICKLIST  NUMBER,
  VALUE     NUMBER,
  LABEL     VARCHAR2(200 BYTE)
);

-- Modify some columns
ALTER TABLE NISIS.ALERTS DROP COLUMN CURRENT_STATUS;

ALTER TABLE NISIS.ALERTS MODIFY(MODIFIED_DATE  DEFAULT NULL);

ALTER TABLE NISIS.ALERTS MODIFY(INACTIVATED_DATE  DEFAULT NULL);

ALTER TABLE NISIS.FAC_STATUS_TYPE RENAME COLUMN STATUS_CODE TO STATUS_COLUMN;

ALTER TABLE NISIS.FAC_STATUS_TYPE MODIFY(STATUS_COLUMN VARCHAR2(30 BYTE));

ALTER TABLE NISIS.FAC_STATUS_TYPE ADD (PICKLIST  NUMBER);


-- Recreate the ALERT_VW [leave out, unless there's a problem]
-- CREATE OR REPLACE FORCE VIEW NISIS.ALERT_VW
-- (
--    ALERTID,
--    OBJECTID,
--    FAC_ID,
--    FAC_TYPE,
--    STATUS_TYPE_ID,
--    STATUS_FULLNAME,
--    CURRENT_STATUS,
--    FUTURE_STATUS,
--    ALERT_DATE,
--    CREATED_BY,
--    CREATED_DATE,
--    ACTIVE
-- )
-- AS
--    SELECT ALERTID,
--           OBJECTID,
--           FAC_ID,
--           FAC_TYPE,
--           STATUS_TYPE_ID,
--           STATUS_FULLNAME,
--           CURRENT_STATUS,
--           FUTURE_STATUS,
--           ALERT_DATE,
--           CREATED_BY,
--           CREATED_DATE,
--           ACTIVE
--      FROM NISIS.ALERTS, NISIS.FAC_STATUS_TYPE_VW
--     WHERE     ALERTS.STATUS_TYPE_ID = FAC_STATUS_TYPE_VW.STATUS_ID
--           AND ACTIVE = 'Y'
--           AND CURRENT_STATUS != FUTURE_STATUS;


--Recreate the ALert_VWXX view
CREATE OR REPLACE FORCE VIEW NISIS.ALERT_VWXX
(
   ALERTID,
   OBJECTID,
   FAC_ID,
   STATUS_TYPE_ID,
   FUTURE_STATUS,
   ALERT_DATE,
   CREATED_BY,
   CREATED_DATE,
   INACTIVATED_BY,
   INACTIVATED_DATE,
   ACTIVE,
   FAC_TYPE,
   FIELD_NAME,
   GIS_FIELD_NAME,
   DATA_FIELD_GROUP,
   DATA_FIELD_TYPE,
   CURRENT_STATUS,
   CURRENT_STATUS_LABEL
)
AS
   SELECT alertid,
          objectid,
          fac_id,
          status_type_id,
          future_status,
          alert_date,
          created_by,
          created_date,
          inactivated_by,
          inactivated_date,
          active,
          fac_type,
          field_name,
          gis_field_name,
          data_field_group,
          data_field_type,
          SUBSTR (current_status, 1, INSTR (current_status, ':') - 1)
             AS current_status,
          SUBSTR (current_status, INSTR (current_status, ':') + 1)
             AS current_status_label
     FROM (SELECT a.alertid,
                  a.objectid,
                  a.fac_id,
                  a.status_type_id,
                  a.future_status,
                  a.alert_date,
                  a.created_by,
                  a.created_date,
                  a.inactivated_by,
                  a.inactivated_date,
                  a.active,
                  b.fac_type,
                  b.field_name,
                  b.gis_field_name,
                  b.data_field_group,
                  b.data_field_type -- ,get_fac_status (b.fac_type, a.status_type_id, a.fac_id)
                                   ,
                  get_fac_sts (b.fac_type,
                               b.gis_field_name,
                               a.fac_id,
                               a.status_type_id,
                               b.sourcetable,
                               b.sourcetable_pkcol)
                     AS current_status
             FROM nisis.alerts a, nisis.data_fields b
            WHERE a.status_type_id = b.id);

-- replace function nisis.insert_alert [get from toad]
CREATE OR REPLACE FUNCTION NISIS.insert_alert (
   in_objid          IN alerts.objectid%TYPE
  ,in_facid          IN alerts.fac_id%TYPE
  ,in_statustypeid   IN alerts.status_type_id%TYPE
  ,in_futstatus      IN alerts.future_status%TYPE
  ,in_alertdate      IN VARCHAR2
  ,in_qformat        IN VARCHAR2
  ,in_createdby      IN alerts.created_by%TYPE
  ,in_active         IN alerts.active%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO alerts (
                  objectid
                 ,fac_id
                 ,status_type_id
                 ,future_status
                 ,alert_date
                 ,created_by
                 ,active
               )
   VALUES (in_objid
          ,in_facid
          ,in_statustypeid
          ,in_futstatus
          ,TO_DATE (in_alertdate, in_qformat)
          ,in_createdby
          ,in_active)
   RETURNING alertid
     INTO v;

   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN -1;
END insert_alert;
/

-- create get_fac_sts function
CREATE OR REPLACE FUNCTION NISIS.get_fac_sts (in_fac_type               VARCHAR2
                                       ,in_colname                VARCHAR2
                                       ,in_fac_id                 VARCHAR2
                                       ,in_status_type_id         NUMBER
                                       ,in_sourcetable         IN VARCHAR2
                                       ,in_sourcetable_pkcol   IN VARCHAR2)
   RETURN VARCHAR2
IS
   current_status   VARCHAR2 (200) := NULL;
   stmt             VARCHAR2 (4000)
      :=    'SELECT b.value||'':''||b.label FROM data_fields a, picklist_items b WHERE a.pick_list = b.picklist AND a.fac_type = :in_fac_type AND a.id=:in_status_type_id AND b.VALUE = (SELECT '
         || in_colname
         || ' FROM nisis.'
         || in_sourcetable
         || ' WHERE '
         || in_sourcetable_pkcol
         || ' = :in_fac_id)';
BEGIN
   EXECUTE IMMEDIATE stmt
      INTO current_status
      USING in_fac_type, in_status_type_id, in_fac_id;

   RETURN current_status;
END;
/

-- create get_fac_sts_future function
CREATE OR REPLACE FUNCTION NISIS.get_fac_sts_future (
   in_fac_type           IN VARCHAR2
  ,in_status_type_id     IN NUMBER
  ,in_future_status_id   IN NUMBER)
   RETURN VARCHAR2
IS
   future_status   VARCHAR2 (200) := NULL;
   stmt            VARCHAR2 (4000)
      := 'SELECT b.label FROM data_fields a, picklist_items b WHERE a.pick_list = b.picklist 
      AND a.fac_type = :in_fac_type AND a.id=:in_status_type_id AND b.VALUE = :in_future_status_id';
BEGIN
   EXECUTE IMMEDIATE stmt
      INTO future_status
      USING in_fac_type, in_status_type_id, in_future_status_id;

   RETURN future_status;
END;
/


-- create alert_vwxxx view 
CREATE OR REPLACE FORCE VIEW NISIS.ALERT_VWXXX
(
   ALERTID,
   OBJECTID,
   FAC_ID,
   STATUS_TYPE_ID,
   FUTURE_STATUS,
   FUTURE_STATUS_LABEL,
   ALERT_DATE,
   CREATED_BY,
   CREATED_DATE,
   INACTIVATED_BY,
   INACTIVATED_DATE,
   ACTIVE,
   FAC_TYPE,
   FIELD_NAME,
   GIS_FIELD_NAME,
   DATA_FIELD_GROUP,
   DATA_FIELD_TYPE,
   CURRENT_STATUS,
   CURRENT_STATUS_LABEL
)
AS
   SELECT alertid,
          objectid,
          fac_id,
          status_type_id,
          future_status,
          future_status_label,
          alert_date,
          created_by,
          created_date,
          inactivated_by,
          inactivated_date,
          active,
          fac_type,
          field_name,
          gis_field_name,
          data_field_group,
          data_field_type,
          SUBSTR (current_status, 1, INSTR (current_status, ':') - 1)
             AS current_status,
          SUBSTR (current_status, INSTR (current_status, ':') + 1)
             AS current_status_label
     FROM (SELECT a.alertid,
                  a.objectid,
                  a.fac_id,
                  a.status_type_id,
                  a.future_status,
                  get_fac_sts_future (b.fac_type,
                                      a.status_type_id,
                                      a.future_status)
                     AS future_status_label,
                  a.alert_date,
                  a.created_by,
                  a.created_date,
                  a.inactivated_by,
                  a.inactivated_date,
                  a.active,
                  b.fac_type,
                  b.field_name,
                  b.gis_field_name,
                  b.data_field_group,
                  b.data_field_type -- ,get_fac_status (b.fac_type, a.status_type_id, a.fac_id)
                                   ,
                  get_fac_sts (b.fac_type,
                               b.gis_field_name,
                               a.fac_id,
                               a.status_type_id,
                               b.sourcetable,
                               b.sourcetable_pkcol)
                     AS current_status
             FROM nisis.alerts a, nisis.data_fields b
            WHERE a.status_type_id = b.id);

--------------------------------------
--------------------------------------
-- INSERT PICKLIST VALUES
--------------------------------------
--------------------------------------
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (1, 'YES-NO OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (2, 'OPS STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (3, 'FUNCTION STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (4, 'OPS STATUS OPTIONS ALT');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (5, 'STAFFING LEVEL OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (6, 'PERSONNEL ACCOUNT OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (7, 'READINESS LEVEL OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (8, 'SECON LEVEL OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (9, 'ATC STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (10, 'SERVICE OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (11, 'APT STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (12, 'BACKUP POWER STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (13, 'EG POWER STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (14, 'SIGNIFICANT IMPACT OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (15, 'TSA STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (16, 'SUPPLIES STATUS OPTIONS');
Insert into NISIS.PICKLIST
   (picklist, description)
 Values
   (17, 'RESOURCE STATUS OPTIONS');

--------------------------------------
--------------------------------------
-- INSERT PICKLIST_ITEMS VALUES
--------------------------------------
--------------------------------------
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (1, 0, 'NO');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (1, 1, 'YES');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 0, 'Normal Operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 1, 'Limited/Degraded Operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 2, 'Closed');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (2, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 0, 'Normal Functioning');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 1, 'Limited/Degraded Functioning');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 2, 'Out of Service');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (3, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 0, 'OTS due to PM');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 1, 'OTS due to Preventive');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 2, 'Shutdown');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 3, 'Working');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (4, 4, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 0, 'Fully Staffed');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 1, 'Limited Staffing');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 2, 'Critically Low Staffing');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (5, 3, 'No Staffing Available');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 0, 'All Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 1, 'Some Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 2, 'Few Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (6, 3, 'None Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 0, 'RL-Alpha');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 1, 'RL-Bravo');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 2, 'RL-Charlie');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (7, 3, 'RL-Delta');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 0, 'SECON-Green');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 1, 'SECON-Blue');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 2, 'SECON-Orange');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 3, 'SECON-Yellow');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 4, 'SECON-Red');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (8, 5, 'N/A');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 0, 'Normal Operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 1, 'ATC Alert');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 2, 'ATC Limited');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 3, 'ATC 0');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (9, 4, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 0, 'Normal Services');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 1, 'Limited Services');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 2, 'No Services');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (10, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 0, 'New Airport');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 1, 'Open with normal operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 2, 'Open with limitations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 3, 'Closed for normal operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 4, 'Closed for all operations');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (11, 5, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (12, 0, 'Standby');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (12, 1, 'In use');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (12, 2, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (13, 0, 'Main Power');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (13, 1, 'Disrupted Power/No EG');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (13, 2, 'Disrupted Power/Using EG');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 0, 'No Known Significant Impact');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 1, 'Somewhat Significant Impact');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 2, 'Widespread Significant Impact');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (14, 3, 'None Accounted For');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 0, 'Normal Throughput');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 1, 'Constrained Capacity');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 2, 'Screening Suspended');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (15, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 0, 'Normal Supplies');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 1, 'Limited Supplies');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 2, 'No Supplies');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (16, 3, 'Other');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (17, 0, 'Resource Status');
Insert into NISIS.PICKLIST_ITEMS
   (picklist, value, label)
 Values
   (17, 1, 'Other');


COMMIT;