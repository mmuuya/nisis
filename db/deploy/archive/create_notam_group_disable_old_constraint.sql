/*
    Script to create the NOTAM_GROUP table, which will contain information about
    which groups can view NOTAMs upon creation
    
    Haeden 
    3/11/2015
*/

create table tfruser.notam_groups (notam_id number, group_id number);

declare
l_count number;
begin
select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'BAK_NOTAM_BODY';

if (l_count > 0) then
    execute immediate 'drop table TFRUSER.BAK_NOTAM_BODY cascade constraints';
end if;
end;
/

create table tfruser.bak_notam_body as select * from tfruser.notam_body;

--remove constraint that user group cannot be null as that is going into a different table entirely.

DROP TABLE TFRUSER.NOTAM_BODY CASCADE CONSTRAINTS;

create table tfruser.notam_body (notam_id number not null, notam_type varchar2(100) not null, notam_name varchar2(200) not null, 
                            status number, user_id number not null, load_date timestamp, updated_by number, 
                            update_date timestamp, artcc_id varchar2(4), artcc_name varchar2(100), 
                            effective_date_utc number default 0, expiration_date_utc number default 0, 
                            custom_text smallInt default 0,
                            constraint pk_notam_id primary key (notam_id),
                            constraint boolean_ct check(custom_text in (0,1)));
  
insert into tfruser.notam_body b (notam_id, notam_type, notam_name, status, user_id, load_date, updated_by, update_date,
        artcc_id, artcc_name, effective_date_utc, expiration_date_utc, custom_text) select n.notam_id, n.notam_type, n.notam_name, n.status, n.user_id, n.load_date, n.updated_by, n.update_date,
        n.artcc_id, n.artcc_name, n.effective_date_utc, n.expiration_date_utc, n.custom_text  from tfruser.bak_notam_body n;
        
select * from tfruser.notam_body;        


commit;