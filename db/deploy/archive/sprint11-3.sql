--//////////////////////
-- add current readiness levels in ACL
--//////////////////////
insert into nisis_admin.acl_fields (FIELDID, COL_NM, DISPLAY_NM, TABLEID, PICKLIST)
values (427, 'CRT_RL', 'CRT_RL', 4, 7);
insert into nisis_admin.acl_perms (ACLID, FIELDID, PERM)
values (868, 427, 'r');
insert into nisis_admin.acl_perms (ACLID, FIELDID, PERM)
values (869, 427, 'w');

insert into nisis_admin.acl_fields (FIELDID, COL_NM, DISPLAY_NM, TABLEID)
values (428, 'CRT_RL_EX', 'CRT_RL_EX', 4); 
insert into nisis_admin.acl_perms (ACLID, FIELDID, PERM)
values (870, 428, 'r');
insert into nisis_admin.acl_perms (ACLID, FIELDID, PERM)
values (871, 428, 'w');

--//////////////////////
--create picklist_images
--//////////////////////
DROP TABLE NISIS.PICKLIST_IMAGES CASCADE CONSTRAINTS;

CREATE TABLE NISIS.PICKLIST_IMAGES
(
  IMGID  NUMBER,
  PATH   VARCHAR2(100 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--//////////////////////
--inserts into picklist_images
--//////////////////////
Insert into NISIS.PICKLIST_IMAGES
   (IMGID, PATH)
 Values
   (0, 'status_closed.png');
Insert into NISIS.PICKLIST_IMAGES
   (IMGID, PATH)
 Values
   (1, 'status_open.png');
Insert into NISIS.PICKLIST_IMAGES
   (IMGID, PATH)
 Values
   (2, 'status_unknown.png');
Insert into NISIS.PICKLIST_IMAGES
   (IMGID, PATH)
 Values
   (3, 'status_warnings.png');

--//////////////////////
--recreate picklist_items with new column
--//////////////////////
DROP TABLE NISIS.PICKLIST_ITEMS CASCADE CONSTRAINTS;

CREATE TABLE NISIS.PICKLIST_ITEMS
(
  PICKLIST  NUMBER,
  VALUE     NUMBER,
  LABEL     VARCHAR2(200 BYTE),
  IMGID     NUMBER
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--//////////////////////
--insert into picklist_items
--//////////////////////
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (3, 4, 'Full Interruption', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (1, 0, 'NO', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (1, 1, 'YES', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (2, 0, 'Normal Operations', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (2, 1, 'Limited/Degraded Operations', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (2, 2, 'Closed', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (2, 3, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (3, 0, 'Normal Operation', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (3, 1, 'Line Only Interruption', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (3, 2, 'Reduced System Equipment', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (3, 3, 'Reduced Facility/Service Interruption', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (4, 0, 'OTS due to PM', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (4, 1, 'OTS due to Preventive', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (4, 2, 'Shutdown', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (4, 3, 'Working', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (4, 4, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (5, 0, 'Fully Staffed', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (5, 1, 'Limited Staffing', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (5, 2, 'Critically Low Staffing', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (5, 3, 'No Staffing Available', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (6, 0, 'All Accounted For', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (6, 1, 'Some Accounted For', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (6, 2, 'Few Accounted For', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (6, 3, 'None Accounted For', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (7, 0, 'RL-Alpha', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (7, 1, 'RL-Bravo', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (7, 2, 'RL-Charlie', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (7, 3, 'RL-Delta', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (8, 0, 'SECON-Green', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (8, 1, 'SECON-Blue', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (8, 2, 'SECON-Orange', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (8, 3, 'SECON-Yellow', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (8, 4, 'SECON-Red', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (8, 5, 'N/A', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (9, 0, 'Normal Operations', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (9, 1, 'ATC Alert', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (9, 2, 'ATC Limited', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (9, 3, 'ATC 0', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (9, 4, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (10, 0, 'Normal Services', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (10, 1, 'Limited Services', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (10, 2, 'No Services', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (10, 3, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (11, 0, 'New Airport', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (11, 1, 'Open with normal operations', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (11, 2, 'Open with limitations', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (11, 3, 'Closed for normal operations', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (11, 4, 'Closed for all operations', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (11, 5, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (12, 0, 'Standby', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (12, 1, 'In use', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (12, 2, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (13, 0, 'Main Power', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (13, 1, 'Disrupted Power/No EG', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (13, 2, 'Disrupted Power/Using EG', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (14, 0, 'No Known Significant Impact', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (14, 1, 'Somewhat Significant Impact', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (14, 2, 'Widespread Significant Impact', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (14, 3, 'None Accounted For', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (15, 0, 'Normal Throughput', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (15, 1, 'Constrained Capacity', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (15, 2, 'Screening Suspended', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (15, 3, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (16, 0, 'Normal Supplies', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (16, 1, 'Limited Supplies', 3);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (16, 2, 'No Supplies', 0);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (16, 3, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (17, 0, 'Resource Status', 1);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (17, 1, 'Other', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (7, 4, 'Unknown', 2);
Insert into NISIS.PICKLIST_ITEMS
   (PICKLIST, VALUE, LABEL, IMGID)
 Values
   (7, 5, 'N/A', 2);

--//////////////////////
-- and commit!
--//////////////////////
COMMIT;
