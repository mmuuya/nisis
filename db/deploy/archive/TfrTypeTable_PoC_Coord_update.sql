/*
    TFR Template update script, building off of Baboyma's new template table design.
    
    Haeden 3/16/15
*/

--select * from tfruser.lookup_tfr_types;

--8 records have this text. 
select * from tfruser.lookup_tfr_types where template like '%{COORDINATION FACILITY NAME}%';

-- run four times because a "/" was missed in key spots during several insertion prevoiusly. This covers all situations.
update tfruser.lookup_tfr_types set template = replace(template, 'THE {COORDINATION FACILITY NAME}/{COORDINATION FACILITY ID}/{COORDINATION FACILITY TYPE}/{COORDINATION FACILITY PHONE}{COORDINATION FACILITY FREQUENCY} IS THE COORDINATION FACILITY.','{COORDINATION FACILITY TEMPLATE}');

update tfruser.lookup_tfr_types set template = replace(template, 'THE {COORDINATION FACILITY NAME}/{COORDINATION FACILITY ID}/{COORDINATION FACILITY TYPE}/{COORDINATION FACILITY PHONE}/{COORDINATION FACILITY FREQUENCY} IS THE COORDINATION FACILITY.','{COORDINATION FACILITY TEMPLATE}');

update tfruser.lookup_tfr_types set template = replace(template, 'THE {COORDINATION FACILITY NAME}/{COORDINATION FACILITY ID}/{COORDINATION FACILITY TYPE}{COORDINATION FACILITY PHONE}/{COORDINATION FACILITY FREQUENCY} IS THE COORDINATION FACILITY.','{COORDINATION FACILITY TEMPLATE}');

update tfruser.lookup_tfr_types set template = replace(template, 'THE {COORDINATION FACILITY NAME}/{COORDINATION FACILITY ID}/{COORDINATION FACILITY TYPE}{COORDINATION FACILITY PHONE}{COORDINATION FACILITY FREQUENCY} IS THE COORDINATION FACILITY.','{COORDINATION FACILITY TEMPLATE}');

--This should now result in 0 records returned
select * from tfruser.lookup_tfr_types where template like '%{COORDINATION FACILITY NAME}%'; 

--7 records have this text
select * from tfruser.lookup_tfr_types where template like '%{POC ORGANIZATION}%';

update tfruser.lookup_tfr_types set template = replace(template, 'THE {POC ORGANIZATION}, {POC NAME}/{POC PHONE}/{POC FREQUENCY} IS IN CHARGE OF THE OPERATION.','{POC TEMPLATE}');

update tfruser.lookup_tfr_types set template = replace(template, 'THE {POC ORGANIZATION}, {POC NAME}{POC PHONE}{POC FREQUENCY} IS IN CHARGE OF THE OPERATION.','{POC TEMPLATE}');

update tfruser.lookup_tfr_types set template = replace(template, 'THE {POC ORGANIZATION}, {POC NAME}{POC PHONE}{POC FREQUENCY} IS IN CHARGE OFTHE OPERATION.','{POC TEMPLATE}');

--This should now result in 0 records returned
select * from tfruser.lookup_tfr_types where template like '%{POC ORGANIZATION}%';

commit;

--rollback;