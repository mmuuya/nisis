-- don't need this table anymore (but we'll keep the view)
DROP TABLE FAC_STATUS_TYPE;
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

CREATE SEQUENCE NISIS.NOTIFICATIONS_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

 -----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-- Create a table for the notifications
CREATE TABLE NISIS.NOTIFICATIONS
(
  NOTID             NUMBER,
  FAC_TYPE          VARCHAR2(6 BYTE),
  FAC_ID            VARCHAR2(20 BYTE),
  COLUMN_NM         VARCHAR2(30 BYTE),
  OLD_VALUE         NUMBER,
  NEW_VALUE         NUMBER,
  CREATED_BY        NUMBER(20),
  CREATED_DATE      DATE                        DEFAULT SYSDATE
);

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
CREATE OR REPLACE TRIGGER nisis.notifications_bt
   BEFORE INSERT
   ON nisis.notifications
   REFERENCING NEW AS new OLD AS old
   FOR EACH ROW
BEGIN
   :new.notid := nisis.notifications_seq.NEXTVAL;
END notifications_bt;
/

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
CREATE OR REPLACE FUNCTION NISIS.insert_notification (
   in_fac_type     IN notifications.fac_type%TYPE,
   in_fac_id       IN notifications.fac_id%TYPE,
   in_column_nm    IN notifications.column_nm%TYPE,
   in_old_value    IN notifications.old_value%TYPE,
   in_new_value    IN notifications.new_value%TYPE,
   in_created_by   IN notifications.created_by%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO nisis.notifications (fac_type,
                                    fac_id,
                                    column_nm,
                                    old_value,
                                    new_value,
                                    created_by,
                                    created_date)
        VALUES (in_fac_type,
                in_fac_id,
                in_column_nm,
                in_old_value,
                in_new_value,
                in_created_by,
                SYSDATE)
     RETURNING notid
          INTO v;

   --COMMIT;
   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      --DBMS_OUTPUT.put_line ('Error = ' || SQLERRM);
      RETURN -1;
END insert_notification;
/

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
CREATE TABLE NISIS.NOTIFICATION_GROUP
(
  NOTID  NUMBER                               NOT NULL,
  GROUPID  NUMBER                             NOT NULL
);

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
CREATE OR REPLACE FORCE VIEW NISIS.NOTIFICATIONS_VW
(
   NOTID,
   FAC_TYPE,
   FAC_ID,
   COLUMN_NM,
   DISPLAY_NM,
   OLD_VALUE,
   OLD_LABEL,
   NEW_VALUE,
   NEW_LABEL,
   PICKLIST,
   CREATED_BY,
   CREATED_DATE
)
AS
   SELECT n.notid,
          n.fac_type,
          n.fac_id,
          n.column_nm,
          f.display_nm,
          n.old_value,
          (SELECT pio.label
             FROM nisis.picklist_items pio
            WHERE pio.picklist = p.picklist AND pio.VALUE = n.old_value)
             old_label,
          n.new_value,
          (SELECT pin.label
             FROM nisis.picklist_items pin
            WHERE pin.picklist = p.picklist AND pin.VALUE = n.new_value)
             new_label,
          p.picklist,
          n.created_by,
          n.created_date
     FROM nisis.notifications n, nisis_admin.acl_fields f, nisis.picklist p
    WHERE n.column_nm = f.col_nm AND f.picklist = p.picklist;


-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------
CREATE TABLE NISIS.NOTIFICATION_COLS
(
  FIELDID      NUMBER,
  FAC_TYPE     VARCHAR2(8 BYTE),
  COLUMN_NM    VARCHAR2(10 BYTE),
  DESCRIPTION  VARCHAR2(28 BYTE)
);

Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (427, 'APT', 'CRT_RL', 'Current Readiness Level');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (82, 'APT', 'AOO_STATUS', 'Aviation Operator Ops Status');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (91, 'APT', 'OOO_STATUS', 'Operating Status');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (77, 'APT', 'OVR_STATUS', 'Overall Status');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (161, 'ANS', 'ANS_STATUS', 'Overall ANS Status');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (253, 'ATM', 'OPS_STATUS', 'Operating Status');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (276, 'ATM', 'CRT_RL', 'Current Readiness Level');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (370, 'OSF', 'CRT_RL', 'Current Readiness Level');
Insert into NISIS.NOTIFICATION_COLS
   (FIELDID, FAC_TYPE, COLUMN_NM, DESCRIPTION)
 Values
   (346, 'OSF', 'BOPS_STS', 'Basic Operating Status');
COMMIT;

-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------

CREATE OR REPLACE FORCE VIEW NISIS.NOTIFICATIONS_VW
(
   NOTID,
   FAC_TYPE,
   FAC_ID,
   COLUMN_NM,
   DISPLAY_NM,
   TABLEID,
   OLD_VALUE,
   OLD_LABEL,
   NEW_VALUE,
   NEW_LABEL,
   PICKLIST,
   DESCRIPTION,
   CREATED_BY,
   CREATED_DATE
)
AS
   SELECT n.notid,
          n.fac_type,
          n.fac_id,
          n.column_nm,
          f.display_nm,
          f.tableid,
          n.old_value,
          (SELECT pio.label
             FROM nisis.picklist_items pio
            WHERE pio.picklist = p.picklist AND pio.VALUE = n.old_value)
             old_label,
          n.new_value,
          (SELECT pin.label
             FROM nisis.picklist_items pin
            WHERE pin.picklist = p.picklist AND pin.VALUE = n.new_value)
             new_label,
          p.picklist,
          p.description,
          n.created_by,
          n.created_date
     FROM nisis.notifications n,
          nisis_admin.acl_fields f,
          nisis.picklist p,
          nisis.notification_cols cc
    WHERE     N.COLUMN_NM = F.COL_NM
          AND F.FIELDID = cc.fieldid
          AND F.PICKLIST = p.picklist;
