/*
    Script to add Instruction field to each area. This allows the user's selected field to be saved
    and automatically set when a NOTAM has been loaded.
    Also sets up a new table for saving the text associated with the individual instructions for reloading
    later.
    
    3/12/15
    Haeden 
*/

select * from tfruser.notam_area;

--drop table tfruser.notam_area_inst;

create table tfruser.notam_area_inst (notam_id number, area_id number, instruction_id number, instruction_text varchar2(2000));

declare
l_count number;
begin
select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'BAK_NOTAM_AREA';

if (l_count > 0) then
    execute immediate 'drop table TFRUSER.BAK_NOTAM_AREA cascade constraints';
end if;
end;
/

create table tfruser.bak_notam_area as select * from tfruser.notam_area;

drop table tfruser.notam_area cascade constraints;

CREATE TABLE TFRUSER.NOTAM_AREA
(
  NOTAM_ID              NUMBER                  NOT NULL,
  SHAPE_ID              NUMBER,
  AREA_ID               NUMBER                  NOT NULL,
  INST_ID               NUMBER,
  AREA_NAME             VARCHAR2(200 BYTE),
  FRD_SHAPE_DEFINITION  INTEGER,
  DEFINITION_OR_NAME    VARCHAR2(100 BYTE),
  INCLUDE_LOCAL_TIME    INTEGER,
  ALLOW_AUTHORIZATION   INTEGER,
  UTC_TIME_ZONE         VARCHAR2(100 BYTE),
  SCHEDULED             VARCHAR2(100 BYTE),
  MONDAY                INTEGER,
  TUESDAY               INTEGER,
  WEDNESDAY             INTEGER,
  THURSDAY              INTEGER,
  FRIDAY                INTEGER,
  SATURDAY              INTEGER,
  SUNDAY                INTEGER
);

insert into tfruser.notam_area (notam_id, shape_id, area_id, area_name, frd_shape_definition, 
    definition_or_name, include_local_time, allow_authorization, utc_time_zone, scheduled, monday,
    tuesday, wednesday, thursday, friday, saturday, sunday) select notam_id, shape_id, area_id, area_name, frd_shape_definition, 
    definition_or_name, include_local_time, allow_authorization, utc_time_zone, scheduled, monday,
    tuesday, wednesday, thursday, friday, saturday, sunday from tfruser.bak_notam_area;
    
--select * from tfruser.notam_area;

commit;   