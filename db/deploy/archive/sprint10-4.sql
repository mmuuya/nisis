-- rename some views
--Rename:
--       ALERT_VWXXX --> ALERT_VW 
--       ALERT_GROUP_NM_TMP --> ALERT_GROUPNAME_VW

-- recreate get_fac_sts
--Changed get_fac_sts dynamic sql stament to point to NISIS_GEODATA schema.
CREATE OR REPLACE FUNCTION NISIS.get_fac_sts (in_fac_type               VARCHAR2
                                       ,in_colname                VARCHAR2
                                       ,in_fac_id                 VARCHAR2
                                       ,in_status_type_id         NUMBER
                                       ,in_sourcetable         IN VARCHAR2
                                       ,in_sourcetable_pkcol   IN VARCHAR2)
   RETURN VARCHAR2
IS
   current_status   VARCHAR2 (200) := NULL;
   stmt             VARCHAR2 (4000)
      :=    'SELECT b.value||'':''||b.label FROM nisis.data_fields a, nisis.picklist_items b WHERE a.pick_list = b.picklist AND a.fac_type = :in_fac_type AND a.id=:in_status_type_id AND b.VALUE = (SELECT '
         || in_colname
         || ' FROM nisis_geodata.'
         || in_sourcetable
         || ' WHERE '
         || in_sourcetable_pkcol
         || ' = :in_fac_id AND gdb_to_date > sysdate)';
BEGIN
   EXECUTE IMMEDIATE stmt
      INTO current_status
      USING in_fac_type, in_status_type_id, in_fac_id;

   RETURN current_status;
END;
/