/*
    Update ARTCC names to drop the " ARTCC" appended to *some* of the records.
    -Haeden 2/19/15
*/
--select * from nisis.lookup_artccs;

--select artcc_name, replace(artcc_name, ' ARTCC', '') from nisis.lookup_artccs where artcc_name like '% ARTCC';

update nisis.lookup_artccs set artcc_name = replace(artcc_name, ' ARTCC', '') where artcc_name like '% ARTCC';

insert into nisis.lookup_artccs (ID, artcc_id, artcc_name) values (24, 'NONE', ' ');

commit;