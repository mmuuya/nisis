-- change new airport to unknown
update nisis.picklist_items set label = 'Unknown' where label = 'New Airport';

-- Get rid of old tables and update display name in nisi_admin.acl_tables

update nisis_admin.acl_tables
set table_pk = 'RES_ID'
where table_nm = 'NISIS_RESP';

update nisis_admin.acl_tables
set display_nm = 'Airports'
where table_nm = 'NISIS_AIRPORTS';

update nisis_admin.acl_tables
set display_nm = 'ATM Facilites'
where table_nm = 'NISIS_ATM';

update nisis_admin.acl_tables
set display_nm = 'Response Resources'
where table_nm = 'NISIS_RESP';

update nisis_admin.acl_tables
set display_nm = 'Other Staffed Facilities'
where table_nm = 'NISIS_OSF';

update nisis_admin.acl_tables
set display_nm = 'ANS Systems'
where table_nm = 'NISIS_ANS';

delete from nisis_admin.acl_tables
where table_nm not in ('NISIS_AIRPORTS','NISIS_ATM','NISIS_RESP','NISIS_OSF','NISIS_ANS');

COMMIT;

-- notifications view
CREATE OR REPLACE FORCE VIEW NISIS.NOTIFICATIONS_VW
(
   NOTID,
   FAC_TYPE,
   FAC_ID,
   COLUMN_NM,
   DISPLAY_NM,
   TABLEID,
   OLD_VALUE,
   OLD_LABEL,
   NEW_VALUE,
   NEW_LABEL,
   PICKLIST,
   DESCRIPTION,
   CREATED_BY,
   CREATED_DATE
)
AS
   SELECT n.notid,
          n.fac_type,
          n.fac_id,
          n.column_nm,
          f.display_nm,
          f.tableid,
          n.old_value,
          (SELECT pio.label
             FROM nisis.picklist_items pio
            WHERE pio.picklist = p.picklist AND pio.VALUE = n.old_value)
             old_label,
          n.new_value,
          (SELECT pin.label
             FROM nisis.picklist_items pin
            WHERE pin.picklist = p.picklist AND pin.VALUE = n.new_value)
             new_label,
          p.picklist,
          p.description,
          n.created_by,
          n.created_date
     FROM nisis.notifications n,
          nisis_admin.acl_fields f,
          nisis.notification_cols cc,
          nisis.picklist p
    WHERE     n.column_nm = f.col_nm
          AND f.fieldid = cc.fieldid
          AND f.picklist = p.picklist;


--Insert new tables to support new Symbols menu. Don't bother dropping because they don't exist in UAT yet...

--DROP TABLE NISIS.SYMBOL_CARTOGRAPHICLINE CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_CARTOGRAPHICLINE
(
  CAP         VARCHAR2(100 BYTE),
  COLOR       VARCHAR2(100 BYTE),
  JOIN        VARCHAR2(100 BYTE),
  MITERLIMIT  VARCHAR2(100 BYTE),
  STYLE       VARCHAR2(100 BYTE),
  TYPE        VARCHAR2(100 BYTE),
  WIDTH       NUMBER
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_FONT CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_FONT
(
  DECORATION  VARCHAR2(100 BYTE),
  FAMILY      VARCHAR2(100 BYTE),
  SYMBOLSIZE  NUMBER,
  STYLE       VARCHAR2(100 BYTE),
  VARIANT     VARCHAR2(100 BYTE),
  WEIGHT      VARCHAR2(100 BYTE),
  FONTID      VARCHAR2(100 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_PICTUREFILL CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_PICTUREFILL
(
  COLOR    VARCHAR2(100 BYTE),
  HEIGHT   NUMBER,
  OUTLINE  VARCHAR2(100 BYTE),
  TYPE     VARCHAR2(100 BYTE),
  URL      VARCHAR2(2083 BYTE),
  WIDTH    NUMBER,
  XOFFSET  NUMBER,
  XSCALE   NUMBER,
  YOFFSET  NUMBER,
  YSCALE   NUMBER
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_PICTUREMARKER CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_PICTUREMARKER
(
  ANGLE       NUMBER,
  COLOR       VARCHAR2(100 BYTE),
  HEIGHT      NUMBER,
  SYMBOLSIZE  NUMBER,
  TYPE        VARCHAR2(100 BYTE),
  URL         VARCHAR2(2083 BYTE),
  WIDTH       NUMBER,
  XOFFSET     NUMBER,
  YOFFSET     NUMBER
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_SIMPLEFILL CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_SIMPLEFILL
(
  COLOR     VARCHAR2(100 BYTE),
  OUTLINE   VARCHAR2(100 BYTE),
  STYLE     VARCHAR2(100 BYTE),
  TYPE      VARCHAR2(100 BYTE),
  OBJECTID  VARCHAR2(100 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_SIMPLELINE CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_SIMPLELINE
(
  COLOR      VARCHAR2(100 BYTE),
  STYLE      VARCHAR2(100 BYTE),
  TYPE       VARCHAR2(100 BYTE),
  WIDTH      NUMBER,
  OUTLINEID  VARCHAR2(100 BYTE),
  OBJECTID   VARCHAR2(100 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_SIMPLEMARKER CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_SIMPLEMARKER
(
  ANGLE       NUMBER,
  COLOR       VARCHAR2(100 BYTE),
  OUTLINE     VARCHAR2(100 BYTE),
  STYLE       VARCHAR2(100 BYTE),
  TYPE        VARCHAR2(100 BYTE),
  XOFFSET     NUMBER,
  YOFFSET     NUMBER,
  SYMBOLSIZE  NUMBER,
  OBJECTID    VARCHAR2(100 BYTE)
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.SYMBOL_TEXT CASCADE CONSTRAINTS;

CREATE TABLE NISIS.SYMBOL_TEXT
(
  ALIGN                VARCHAR2(100 BYTE),
  ANGLE                NUMBER,
  COLOR                VARCHAR2(100 BYTE),
  DECORATION           VARCHAR2(100 BYTE),
  FONT                 VARCHAR2(100 BYTE),
  HORIZONTALALIGNMENT  VARCHAR2(100 BYTE),
  KERNING              CHAR(1 BYTE),
  ROTATED              CHAR(1 BYTE),
  TEXT                 VARCHAR2(4000 BYTE),
  TYPE                 VARCHAR2(100 BYTE),
  VERTICALALIGNMENT    VARCHAR2(100 BYTE),
  XOFFSET              NUMBER,
  YOFFSET              NUMBER
)
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--DROP TABLE NISIS.DYNAMIC_SYMBOLS CASCADE CONSTRAINTS;

CREATE TABLE NISIS.DYNAMIC_SYMBOLS
(
  PATH        CLOB,
  VALUE       NUMBER,
  LABEL       VARCHAR2(100 BYTE),
  SYMBOLNAME  VARCHAR2(100 BYTE),
  FONTLETTER  CHAR(100 BYTE)
)
LOB (PATH) STORE AS (
  TABLESPACE  NISIS
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ))
TABLESPACE NISIS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--insert into dynamic symbols
SET DEFINE OFF;
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (1, 'Pushpin 1', 'pushpin1');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (2, 'Pushpin 2', 'pushpin2');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (3, 'Pushpin 3', 'pushpin3');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (4, 'Circle', 'circle');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (5, 'Square', 'square');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (6, 'Diamond', 'diamond');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (7, 'Pentagon', 'pentagon');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (8, 'Hexagon', 'hexagon');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (9, 'Triangle', 'triangle');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (10, 'Star', 'star');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (11, 'Plus', 'plus');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME)
 Values
   (12, 'Cross', 'cross');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2001, 'After Shock', 'natA', 'A                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2002, 'Avalanche', 'natB', 'B                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2003, 'Earthquake Epicenter', 'natC', 'C                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3008, 'Bomb Explosion', 'incH', 'H                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3009, 'Looting', 'incI', 'I                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3010, 'Poisoning', 'incJ', 'J                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3011, 'Shooting', 'incK', 'K                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3012, 'Fire Incident', 'incL', 'L                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3013, 'Hot Spot', 'incM', 'M                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3014, 'Non-Residential Fire', 'incN', 'N                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3015, 'Fire Origin', 'incO', 'O                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3016, 'Residential Fire', 'incP', 'P                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3017, 'School Fire', 'incQ', 'Q                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3018, 'Smoke', 'incR', 'R                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3019, 'Special Needs Fire', 'incS', 'S                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3020, 'Wild Fire', 'incT', 'T                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3021, 'Hazardous Material Incident', 'incU', 'U                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3022, 'Chemical Agents', 'incV', 'V                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2004, 'Landslide', 'natD', 'D                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2005, 'Subsidence', 'natE', 'E                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2006, 'Volcanic Eruption', 'natF', 'F                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2007, 'Volcanic Threat', 'natG', 'G                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2008, 'Drizzle', 'natH', 'H                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2009, 'Drought', 'natI', 'I                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2010, 'Flood', 'natJ', 'J                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2011, 'Fog', 'natK', 'K                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2012, 'Hail', 'natL', 'L                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2013, 'Inversion', 'natM', 'M                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2014, 'Rain', 'natN', 'N                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2015, 'Sand Dust Storm', 'natO', 'O                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2016, 'Snow', 'natP', 'P                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2017, 'Thunder Storm', 'natQ', 'Q                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2018, 'Tornado', 'natR', 'R                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2019, 'Tropical Cyclone', 'natS', 'S                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2020, 'Tsunami', 'natT', 'T                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2021, 'Bird Infestation', 'natU', 'U                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2022, 'Insect Infestation', 'natV', 'V                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2023, 'Microbial Infestation', 'natW', 'W                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2024, 'Reptile Infestation', 'natX', 'X                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (2025, 'Rodent Infestation', 'natY', 'Y                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3001, 'Civil Disturbance Incident', 'incA', 'A                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3002, 'Civil Demonstrations', 'incB', 'B                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3003, 'Civil Displaced Population', 'incC', 'C                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3004, 'Civil Rioting', 'incD', 'D                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3005, 'Criminal Activity Incident', 'incE', 'E                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3006, 'Bomb Threat', 'incF', 'F                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3007, 'Bomb', 'incG', 'G                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3024, 'Hazardous When Wet', 'incX', 'X                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3025, 'Explosive', 'incY', 'Y                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3026, 'Flammable Gas', 'incZ', 'Z                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4001, 'Flammable Liquid', 'inca', 'a                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4002, 'Flammable Solid', 'incb', 'b                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4003, 'Non-Flammable Gas', 'incc', 'c                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4004, 'Organic Peroxides', 'incd', 'd                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4005, 'Oxidizers', 'ince', 'e                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4006, 'Radioactive Material', 'incf', 'f                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4007, 'Spontaneously Combustible', 'incg', 'g                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4008, 'Toxic Gas', 'inch', 'h                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4009, 'Toxic and Infectious', 'inci', 'i                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4010, 'Unexploded Ordnance', 'incj', 'j                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4011, 'Air Incident', 'inck', 'k                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4012, 'Air Accident', 'incl', 'l                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4013, 'Air Hijacking', 'incm', 'm                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4014, 'Marine Incident', 'incn', 'n                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4015, 'Marine Accident', 'inco', 'o                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4016, 'Marine Hijacking', 'incp', 'p                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4017, 'Rail Incident', 'incq', 'q                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4018, 'Rail Accident', 'incr', 'r                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4019, 'Rail Hijacking', 'incs', 's                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4020, 'Vehicle Incident', 'inct', 't                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4021, 'Vehicle Accident', 'incu', 'u                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (4022, 'Vehicle Hijacking', 'incv', 'v                                                                                                   ');
Insert into NISIS.DYNAMIC_SYMBOLS
   (VALUE, LABEL, SYMBOLNAME, FONTLETTER)
 Values
   (3023, 'Corrosive Material', 'incW', 'W                                                                                                   ');
COMMIT;

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------

CREATE OR REPLACE FORCE VIEW NISIS.NOTIFICATIONS_VW
(
   NOTID,
   FAC_TYPE,
   FAC_ID,
   COLUMN_NM,
   DISPLAY_NM,
   TABLEID,
   OLD_VALUE,
   OLD_LABEL,
   NEW_VALUE,
   NEW_LABEL,
   PICKLIST,
   DESCRIPTION,
   CREATED_BY,
   CREATED_DATE
)
AS
   SELECT n.notid,
          n.fac_type,
          n.fac_id,
          n.column_nm,
          f.display_nm,
          f.tableid,
          n.old_value,
          (SELECT pio.label
             FROM nisis.picklist_items pio
            WHERE pio.picklist = p.picklist AND pio.VALUE = n.old_value)
             old_label,
          n.new_value,
          (SELECT pin.label
             FROM nisis.picklist_items pin
            WHERE pin.picklist = p.picklist AND pin.VALUE = n.new_value)
             new_label,
          p.picklist,
          p.description,
          n.created_by,
          n.created_date
     FROM nisis.notifications n,
          nisis_admin.acl_fields f,
          nisis.notification_cols cc,
          nisis.picklist p
    WHERE     n.column_nm = f.col_nm
          AND f.fieldid = cc.fieldid
          AND f.picklist = p.picklist;

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------

CREATE OR REPLACE FUNCTION NISIS.insert_notification (
   in_fac_type     IN notifications.fac_type%TYPE,
   in_fac_id       IN notifications.fac_id%TYPE,
   in_column_nm    IN notifications.column_nm%TYPE,
   in_old_value    IN notifications.old_value%TYPE,
   in_new_value    IN notifications.new_value%TYPE,
   in_created_by   IN notifications.created_by%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO nisis.notifications (fac_type,
                                    fac_id,
                                    column_nm,
                                    old_value,
                                    new_value,
                                    created_by,
                                    created_date)
        VALUES (in_fac_type,
                in_fac_id,
                in_column_nm,
                in_old_value,
                in_new_value,
                in_created_by,
                SYSDATE)
     RETURNING notid
          INTO v;

   COMMIT;
   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      --DBMS_OUTPUT.put_line ('Error = ' || SQLERRM);
      RETURN -1;
END insert_notification;

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------

CREATE OR REPLACE FORCE VIEW NISIS.USER_NOTIFICATIONS_VW
(
   REF_NOTID,
   NOTIFICATION_GROUPID,
   GROUPS_GROUPID,
   NAME,
   USERID
)
AS
   SELECT n.notid ref_notid,
          ng.groupid notification_groupid,
          g.groupid groups_groupid,
          g.name name,
          u.userid userid
     FROM nisis_admin.user_group u
          JOIN nisis_admin.groups g ON u.groupid = g.groupid
          JOIN nisis.notification_group ng ON ng.groupid = g.groupid
          JOIN nisis.notifications n ON n.notid = ng.notid;
