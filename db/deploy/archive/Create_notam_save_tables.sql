/*
    Script for generating all tables for NOTAM saving/loading
    -Haeden 1/6/15
    1/29/15 - added Hawaii and Emergency Traffic tables
*/



declare
l_count number := 0;
begin

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_BODY';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_body cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'LOOKUP_STATUS';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.lookup_status cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_LOCATIONS';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_locations cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TEXT';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_text cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_AREA';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_area cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TFR_DATE_TIME';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_tfr_date_time cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_HAZARDS';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_hazards cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_VIP';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_vip cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_AIRSHOW';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_airshow cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_SECURITY';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_security cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_SPACE_OPERATIONS';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_space_operations cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_HAWAII';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_hawaii cascade constraints';
end if;

select count(*) into l_count from all_tables where owner like 'TFRUSER' and table_name like 'NOTAM_TYPE_EMERGENCY_TRAFFIC';
if (l_count > 0) then 
    execute immediate 'drop table tfruser.notam_type_emergency_traffic cascade constraints';
end if;
end;
/


create table tfruser.notam_body (notam_id number not null, notam_type varchar2(100) not null, notam_name varchar2(200) not null, 
                            status number, user_id number not null, user_group number default 0 not null, load_date timestamp, updated_by number, 
                            update_date timestamp, artcc_id varchar2(4), artcc_name varchar2(100), 
                            effective_date_utc number default 0, expiration_date_utc number default 0, 
                            custom_text smallInt default 0,
                            constraint pk_notam_id primary key (notam_id),
                            constraint boolean_ct check(custom_text in (0,1)));

create table tfruser.lookup_status (status_id number, status_text varchar2(100));  

create table tfruser.notam_locations (notam_id number not null, state_abbrev varchar2(4), state_territory varchar2(100), city varchar2(100), 
                            insert_order number,
                            foreign key (notam_id) references tfruser.notam_body(notam_id));

create table tfruser.notam_text (notam_id number not null, text clob, foreign key (notam_id) references tfruser.notam_body(notam_id));

create table tfruser.notam_area (notam_id number not null, shape_id number, area_id number not null, area_name varchar2(200), 
                            frd_shape_definition smallInt default 0, definition_or_name varchar2(100), 
                            include_local_time smallInt default 0, allow_authorization smallInt default 0,
                            utc_time_zone varchar2(100), scheduled varchar2(100), monday smallInt default 0,
                            tuesday smallInt default 0, wednesday smallInt default 0, thursday smallInt default 0,
                            friday smallInt default 0, saturday smallInt default 0, sunday smallInt default 0,
--                            primary key (area_id),
                            foreign key (notam_id) references tfruser.notam_body(notam_id),
                            constraint boolean_frd_shape check(frd_shape_definition in (0,1)),
                            constraint boolean_local_time check(include_local_time in (0,1)),
                            constraint boolean_allow_auth check(allow_authorization in (0,1)),
--                            constraint boolean_utc_tz check(utc_time_zone in (0,1)),
--                            constraint boolean_scheduled check(scheduled in (0,1)),
                            constraint boolean_mon check(monday in (0,1)),
                            constraint boolean_tues check(tuesday in (0,1)),
                            constraint boolean_wed check(wednesday in (0,1)),
                            constraint boolean_thurs check(thursday in (0,1)),
                            constraint boolean_fri check(friday in (0,1)),
                            constraint boolean_sat check(saturday in (0,1)),
                            constraint boolean_sun check(sunday in (0,1)));
                              
                            /** TODO: Add foreign key for shape_id once table for it is built **/
                            
create table tfruser.notam_tfr_date_time (notam_id number not null, area_id number not null, date_time_id number not null , effective_date_time timestamp, 
                            expiration_date_time timestamp);
                            
create table tfruser.notam_type_hazards (notam_id number not null, point_of_contact_name varchar2(200),
                            poc_organization varchar2(200), poc_phones varchar2(400), 
                            poc_frequencies varchar2(400), coordinating_facility_id varchar2(400),
                            coord_fac_name varchar2(200), coord_fac_type varchar2(200), coord_fac_type_id number, 
                            coord_fac_phones varchar2(400), coord_fac_frequencies varchar2(400), 
                            reason varchar2(4000), modify_id number, modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));
                          
create table tfruser.notam_type_vip (notam_id number not null,
                            modify_id number, modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));
                          
create table tfruser.notam_type_airshow (notam_id number not null, point_of_contact_name varchar2(200),
                            poc_organization varchar2(200), poc_phones varchar2(400), 
                            poc_frequencies varchar2(400), coordinating_facility_id varchar2(400),
                            coord_fac_name varchar2(200), coord_fac_type  varchar2(200), coord_fac_type_id number,
                            coord_fac_phones varchar2(400), coord_fac_frequencies varchar2(400), 
                            controlling_facility_name varchar2(200), cntrl_fac_type varchar2(200), cntrl_fac_freq varchar2(400), 
                            reason varchar2(4000), modify_id number, modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));
                          
create table tfruser.notam_type_security (notam_id number not null, point_of_contact_name varchar2(200),
                            poc_organization varchar2(200), poc_phones varchar2(400), 
                            poc_frequencies varchar2(400), coordinating_facility_id varchar2(400),
                            coord_fac_name varchar2(200), coord_fac_type  varchar2(200), coord_fac_type_id number, 
                            coord_fac_phones varchar2(400), coord_fac_frequencies varchar2(400), 
                            reason varchar2(4000), modify_id number, modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));
                          
create table tfruser.notam_type_space_operations (notam_id number not null, coordinating_facility_id varchar2(400), coord_fac_name varchar2(200), 
                            coord_fac_type  varchar2(200), coord_fac_type_id number, coord_fac_phones varchar2(400),
                            coord_fac_frequencies varchar2(400), reason varchar2(4000), modify_id number, 
                            modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));    
                            
create table tfruser.notam_type_hawaii (notam_id number not null,
                            modify_id number, modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));       
                            
create table tfruser.notam_type_emergency_traffic (notam_id number not null,
                            modify_id number, modify_reason varchar2(4000),
                            foreign key (notam_id) references tfruser.notam_body(notam_id));                                                  
                                                     
                            