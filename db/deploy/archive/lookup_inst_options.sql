
SELECT * FROM TFRUSER.LOOKUP_INST_OPTIONS;

--CREATE TABLE

CREATE TABLE TFRUSER.LOOKUP_INST_OPTIONS
(
    ID      NUMBER,
    NAME    VARCHAR2 (2048)
);

--INSERT DATA

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Standard');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Only relief aircraft operations under the direction of ARTCC are authorized in the airspace.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Except as specified below and/or unless authorized by ATC:');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '1. All aircraft entering or exiting the TFR must be on an active IFR or VFR flight plan with a discrete code assigned by an Air Traffic Control (ATC) facility.  Aircraft must be squawking the discrete code prior to departure and at all times while in the TFR.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '2. All aircraft entering or exiting the TFR must remain in two-way radio communications with ATC.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Except as specified below and/or unless authorized by ATC in consultation with the air traffic security coordinator via the domestic events network (DEN):');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'A. ALL AIRCRAFT OPERATIONS WITHIN THE 10 NMR AREA(S) LISTED ABOVE, KNOWN AS THE INNER CORE(S), ARE PROHIBITED EXCEPT FOR: MILITARY AIRCRAFT DIRECTLY SUPPORTING THE UNITED STATES SECRET SERVICE (USSS) AND THE OFFICE OF THE PRESIDENT OF THE UNITED STATES. COORDINATED AND APPROVED LAW ENFORCEMENT, AIR AMBULANCE AND FIREFIGHTING OPERATIONS MUST RECEIVE APPROVAL PRIOR TO ENTERING THIS AIRSPACE AT XXX-XXX-XXXX TO AVOID POTENTIAL DELAYS. REGULARLY SCHEDULED COMMERCIAL PASSENGER AND ALL-CARGO CARRIERS OPERATING UNDER ONE OF THE FOLLOWING TSA-APPROVED STANDARD SECURITY PROGRAMS/PROCEDURES: AIRCRAFT OPERATOR STANDARD');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'B. For operations within the airspace between the 10 nmr and 30 nmr area(s) listed above, known as the outer ring(s): All aircraft operating within the outer ring(s) listed above are limited to aircraft arriving or departing local airfields, and workload permitting, ATC may authorize transit operations. Aircraft may not loiter. All aircraft must be on an active IFR or VFR flight plan with a discrete code assigned by an air traffic control (ATC) facility. Aircraft must be squawking the discrete code prior to departure and at all times while in the TFR and must remain in two-way radio communications with ATC.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'C. THE FOLLOWING OPERATIONS ARE NOT AUTHORIZED WITHIN THIS TFR: FLIGHT TRAINING, PRACTICE INSTRUMENT APPROACHES, AEROBATIC FLIGHT, GLIDER OPERATIONS, SEAPLANE OPERATIONS, PARACHUTE OPERATIONS, ULTRALIGHT, HANG GLIDING, BALLOON OPERATIONS, AGRICULTURE/CROP DUSTING, ANIMAL POPULATION CONTROL FLIGHT OPERATIONS, BANNER TOWING OPERATIONS, SIGHTSEEING OPERATIONS, MAINTENANCE TEST FLIGHTS, RADIO CONTROLLED MODEL AIRCRAFT OPERATIONS, MODEL ROCKETRY, UNMANNED AIRCRAFT SYSTEMS (UAS), AND UTILITY AND PIPELINE SURVEY OPERATIONS.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'D. FAA recommends that all aircraft operators check notams frequently for possible changes to this TFR prior to operations within this region.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Except the flight operations listed below:');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '1. All IFR arrivals or departures to/from airports within this TFR.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '2. Approved; law enforcement, fire fighting, military aircraft directly supporting the United States Secret Service (USSS) and the office of the Vice President of the United States, and MEDEVAC/air ambulance flights.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '3. Aircraft operations necessitated for safety or emergency reasons.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '4. Aircraft that receive ATC authorization in consultation with the air traffic security coordinator (ATSC) via the domestic events network(DEN).');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Unless authorized by ATC.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, 'Space operations area: aircraft operations are prohibitied.');

INSERT INTO TFRUSER.LOOKUP_INST_OPTIONS (ID, NAME) VALUES (1, '');

-- RESET THE ID

UPDATE TFRUSER.LOOKUP_INST_OPTIONS
SET ID = ROWNUM
WHERE ID IS NOT NULL;
