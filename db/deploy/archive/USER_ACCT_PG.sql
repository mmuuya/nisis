CREATE OR REPLACE PACKAGE APPUSER.user_acct_pg
AS
   /******************************************************************************
      NAME:      appuser.user_acct_pg
      PURPOSE:

      REVISIONS:
      Ver        Date        Author              Description
      ---------  ----------  ---------------     ------------------------------------
      1.0        12/15/2014  Kofi Honu           1. Created this package.
      1.1        02/24/2015  Cristhian Castro    2. Modified to receive an array as parameter in the functions
   ******************************************************************************/
   err_no_app       NUMBER := -1;
   err_no_userid    NUMBER := -2;
   err_no_username  NUMBER := -3;
   err_dup_usename  NUMBER := -4;
   err_empty_field  NUMBER := -5;
   err_other        NUMBER := -6;
   TYPE array_t IS TABLE OF VARCHAR2(3) INDEX BY BINARY_INTEGER;

--   FUNCTION create_user (i_username   IN VARCHAR2
--                        ,i_fname      IN VARCHAR2
--                        ,i_lname      IN VARCHAR2
--                        ,i_email      IN VARCHAR2
--                        ,i_password   IN VARCHAR2
--                        ,i_salt       IN VARCHAR2
--                        ,i_app        IN VARCHAR2 DEFAULT ' ')
--      RETURN NUMBER;
--
--   FUNCTION update_user (i_userid     IN NUMBER
--                        ,i_fname      IN VARCHAR2
--                        ,i_lname      IN VARCHAR2
--                        ,i_email      IN VARCHAR2
--                        ,i_password   IN VARCHAR2
--                        ,i_salt       IN VARCHAR2
--                        ,i_app        IN VARCHAR2
--                        ,i_active     IN NUMBER)
--      RETURN NUMBER;
   FUNCTION create_user (i_username   IN VARCHAR2
                        ,i_fname      IN VARCHAR2
                        ,i_lname      IN VARCHAR2
                        ,i_email      IN VARCHAR2
                        ,i_password   IN VARCHAR2
                        ,i_salt       IN VARCHAR2
                        ,i_apps       IN array_t )
      RETURN NUMBER;

   FUNCTION update_user (i_userid     IN NUMBER
                        ,i_fname      IN VARCHAR2
                        ,i_lname      IN VARCHAR2
                        ,i_email      IN VARCHAR2
                        ,i_password   IN VARCHAR2
                        ,i_salt       IN VARCHAR2
                        ,i_apps       IN array_t
                        ,i_active     IN NUMBER)
      RETURN NUMBER;
END user_acct_pg;
/

CREATE OR REPLACE PACKAGE BODY APPUSER.user_acct_pg
AS
   /******************************************************************************
      NAME:      appuser.user_acct_pg
      PURPOSE:

      Ver        Date        Author              Description
      ---------  ----------  ---------------     ------------------------------------
      1.0        12/15/2014  Kofi Honu           1. Created this package.
      1.1        02/24/2015  Cristhian Castro    2. Modified to receive an array as parameter in the functions
   ******************************************************************************/
--   FUNCTION create_user (i_username   IN VARCHAR2,
--                         i_fname      IN VARCHAR2,
--                         i_lname      IN VARCHAR2,
--                         i_email      IN VARCHAR2,
--                         i_password   IN VARCHAR2,
--                         i_salt       IN VARCHAR2,
--                         i_app        IN VARCHAR2)
--      RETURN NUMBER
--   IS
--      o_userid   NUMBER := -1;
--      v_app      VARCHAR2 (100);
--      v_username      VARCHAR2 (255);
--      v_fname      VARCHAR2 (50);
--      v_lname      VARCHAR2 (50);
--      v_email      VARCHAR2 (255);
--      
--   BEGIN
--      v_app := UPPER (TRIM (i_app));
--
--      IF NVL (TRIM (v_app), ' ') = ' '
--      THEN
--         RETURN err_no_app;
--      ELSIF v_app NOT IN ('NISIS', 'TFR')
--      THEN
--         RETURN err_no_app;
--      END IF;
--
--     -- DBMS_OUTPUT.PUT_LINE ('1');
--
--      IF    NVL (TRIM (i_username), ' ') = ' '
--         OR NVL (TRIM (i_fname), ' ') = ' '
--         OR NVL (TRIM (i_lname), ' ') = ' '
--         OR NVL (TRIM (i_email), ' ') = ' '
--         OR NVL (TRIM (i_password), ' ') = ' '
--         OR NVL (TRIM (i_salt), ' ') = ' '
--      THEN
--         RETURN err_empty_field;
--      END IF;
--
--      --DBMS_OUTPUT.PUT_LINE ('2');
--
--      BEGIN
--         o_userid := appuser.seq_user_id.NEXTVAL;
--         v_username := TRIM(i_username);
--         v_fname := TRIM(i_fname);
--         v_lname := TRIM(i_lname);
--         v_email:= TRIM(i_email);
--         
--         INSERT INTO appuser.users (userid,
--                                    username,
--                                    fname,
--                                    lname,
--                                    email,
--                                    password,
--                                    salt,
--                                    datecreated)
--              VALUES (o_userid,
--                      v_username,
--                      v_fname,
--                      v_lname,
--                      v_email,
--                      i_password,
--                      i_salt,
--                      SYSDATE);
--
--        -- DBMS_OUTPUT.PUT_LINE ('3');
--
--         INSERT INTO appuser.users_app (userid, app, active)
--              VALUES (o_userid, v_app, 1);
--
--         --DBMS_OUTPUT.PUT_LINE ('4');
--         COMMIT;
--      EXCEPTION
--         WHEN DUP_VAL_ON_INDEX
--         THEN
--            RETURN err_dup_usename;
--      END;
--
--      RETURN o_userid;
--   EXCEPTION
--      WHEN OTHERS
--      THEN
--         ROLLBACK;
--         RETURN err_other;
--   END create_user;

FUNCTION create_user (i_username   IN VARCHAR2,
                         i_fname      IN VARCHAR2,
                         i_lname      IN VARCHAR2,
                         i_email      IN VARCHAR2,
                         i_password   IN VARCHAR2,
                         i_salt       IN VARCHAR2,
                         i_apps       IN array_t)
      RETURN NUMBER
   IS
      o_userid     NUMBER := -1;
      v_username   VARCHAR2 (255);
      v_fname      VARCHAR2 (50);
      v_lname      VARCHAR2 (50);
      v_email      VARCHAR2 (255);
      v_apps       array_t := i_apps;
      v_appid      NUMBER;
      
   BEGIN
      
      IF v_apps IS NULL 
      THEN
         RETURN err_no_app;
      ELSE
          FOR i IN 1..v_apps.count LOOP
            v_appid := TO_NUMBER(v_apps(i));
            IF v_appid NOT IN (0,1)
                THEN RETURN err_no_app;
            END IF;
          END LOOP; 
      END IF;

     -- DBMS_OUTPUT.PUT_LINE ('1');

      IF    NVL (TRIM (i_username), ' ') = ' '
         OR NVL (TRIM (i_fname), ' ') = ' '
         OR NVL (TRIM (i_lname), ' ') = ' '
         OR NVL (TRIM (i_email), ' ') = ' '
         OR NVL (TRIM (i_password), ' ') = ' '
         OR NVL (TRIM (i_salt), ' ') = ' '
      THEN
         RETURN err_empty_field;
      END IF;

      --DBMS_OUTPUT.PUT_LINE ('2');

      BEGIN
         o_userid := appuser.seq_user_id.NEXTVAL;
         v_username := TRIM(i_username);
         v_fname := TRIM(i_fname);
         v_lname := TRIM(i_lname);
         v_email:= TRIM(i_email);
         
         INSERT INTO appuser.users (userid,
                                    username,
                                    fname,
                                    lname,
                                    email,
                                    password,
                                    salt,
                                    datecreated)
              VALUES (o_userid,
                      v_username,
                      v_fname,
                      v_lname,
                      v_email,
                      i_password,
                      i_salt,
                      SYSDATE);

        -- DBMS_OUTPUT.PUT_LINE ('3');
         
         FOR i IN 1..v_apps.count LOOP
            v_appid := TO_NUMBER(v_apps(i));
            INSERT INTO appuser.users_app (userid, appid, active)
              VALUES (o_userid, v_appid, 1);
         END LOOP;

         --DBMS_OUTPUT.PUT_LINE ('4');
         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            RETURN err_dup_usename;
      END;

      RETURN o_userid;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         RETURN err_other;
   END create_user;

   FUNCTION update_user (i_userid     IN NUMBER,
                         i_fname      IN VARCHAR2,
                         i_lname      IN VARCHAR2,
                         i_email      IN VARCHAR2,
                         i_password   IN VARCHAR2,
                         i_salt       IN VARCHAR2,
                         i_apps       IN array_t,
                         i_active     IN NUMBER)
      RETURN NUMBER
   IS
      -- o_userid   NUMBER := -1;
      v_apps    array_t := i_apps;
      v_appid   NUMBER;
      stmt      VARCHAR2 (2000) := ' ';
      stmt1     VARCHAR2 (2000) := ' ';
      stmt2     VARCHAR2 (2000) := ' ';
   BEGIN
      
      IF    NVL (TRIM (i_userid), 0) = 0
      THEN
         RETURN err_no_userid;
      END IF;

      
      IF v_apps IS NULL 
      THEN
         RETURN err_no_app;
      ELSE
          FOR i IN 1..v_apps.count LOOP
            v_appid := TO_NUMBER(v_apps(i));
            IF v_appid NOT IN (0,1)
                THEN RETURN err_no_app;
            END IF;
          END LOOP; 
      END IF;

      IF    NVL (TRIM (i_fname), ' ') = ' '
         OR NVL (TRIM (i_lname), ' ') = ' '
         OR NVL (TRIM (i_email), ' ') = ' '
      THEN
         RETURN err_empty_field;
      END IF;

      stmt :=
         'UPDATE appuser.users SET fname=:fname, lname=:lname, email=:email';
         
      IF (NVL (TRIM (i_password), ' ') != ' ')
      THEN
         stmt := stmt || ',password=:pwhash, salt=:salt';
      END IF;

      stmt := stmt || ' WHERE userid=:userid';

      IF (NVL (TRIM (i_password), ' ') != ' ')
      THEN
         EXECUTE IMMEDIATE (stmt)
            USING i_fname,
                  i_lname,
                  i_email,
                  i_password,
                  i_salt,
                  i_userid;
      ELSE
         EXECUTE IMMEDIATE (stmt)
            USING i_fname,
                  i_lname,
                  i_email,
                  i_userid;
      END IF;
      
      stmt1 := 'DELETE FROM appuser.users_app WHERE userid=:userid';
      EXECUTE IMMEDIATE (stmt1)
            USING i_userid;
      
      FOR i IN 1..v_apps.count LOOP
        v_appid := TO_NUMBER(v_apps(i));
        stmt2 := 'INSERT INTO appuser.users_app (userid, appid, active) VALUES (:userid, :appid, :active)';
        EXECUTE IMMEDIATE (stmt2)
            USING i_userid,
                  v_appid,
                  i_active;
      END LOOP;

      --DBMS_OUTPUT.PUT_LINE (STMT);
      COMMIT;
      RETURN 1;
   EXCEPTION
      WHEN OTHERS
      THEN
         --DBMS_OUTPUT.PUT_LINE (SQLERRM);
         ROLLBACK;
         RETURN err_other;
   END update_user;
END user_acct_pg;
/
