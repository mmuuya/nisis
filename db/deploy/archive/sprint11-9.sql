--- Update the RMLS views

CREATE OR REPLACE FORCE VIEW NISIS.AIRPORT_RMLS_OPEN_VIEW
(
	 ANS_ID,
	 LOG_ID,
	 FAC_IDENT,
	 LOG_STATUS,
	 CODE_CATEGORY,
	 START_DATETIME,
	 END_DATETIME,
	 MODIFIED_DATETIME,
	 EQUIPMENT_ID,
	 EVENT_TYPE_CODE,
	 ERROR_CODE,
	 INTERRUPT_CONDITION,
	 LOG_SUMMARY
)
AS
	 SELECT b.ans_id,
					b.log_id,
					b.fac_ident,
					b.log_status,
					b.code_category,
					b.start_datetime,
					b.end_datetime,
					b.modified_datetime,
					b.equipment_id,
					b.event_type_code,
					b.ERROR_CODE,
					b.interrupt_condition,
					b.log_summary
		 FROM nisis_geodata.nisis_airports a, nisisdata.nisis_rmls b
		WHERE a.faa_id = b.fac_ident AND b.log_status = 'O' AND a.is_curr_rec = 1;

/* Formatted on 9/25/2014 3:30:15 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.AIRPORT_RMLS_VOID_VIEW
(
	 ANS_ID,
	 LOG_ID,
	 FAC_IDENT,
	 LOG_STATUS,
	 CODE_CATEGORY,
	 START_DATETIME,
	 END_DATETIME,
	 MODIFIED_DATETIME,
	 EQUIPMENT_ID,
	 EVENT_TYPE_CODE,
	 ERROR_CODE,
	 INTERRUPT_CONDITION,
	 LOG_SUMMARY
)
AS
	 SELECT b.ans_id,
					b.log_id,
					b.fac_ident,
					b.log_status,
					b.code_category,
					b.start_datetime,
					b.end_datetime,
					b.modified_datetime,
					b.equipment_id,
					b.event_type_code,
					b.ERROR_CODE,
					b.interrupt_condition,
					b.log_summary
		 FROM nisis_geodata.nisis_airports a, nisisdata.nisis_rmls b
		WHERE     a.faa_id = b.fac_ident
					AND b.log_status IN ('C', 'V')
					AND a.is_curr_rec = 1
					AND b.start_datetime > (SYSDATE - 30);

/* Formatted on 9/25/2014 3:37:15 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.ANS_RMLS_OPEN_VIEW
(
	 ANS_ID,
	 LOG_ID,
	 FAC_IDENT,
	 LOG_STATUS,
	 CODE_CATEGORY,
	 START_DATETIME,
	 END_DATETIME,
	 MODIFIED_DATETIME,
	 EQUIPMENT_ID,
	 EVENT_TYPE_CODE,
	 ERROR_CODE,
	 INTERRUPT_CONDITION,
	 LOG_SUMMARY
)
AS
	 SELECT b.ans_id,
					b.log_id,
					b.fac_ident,
					b.log_status,
					b.code_category,
					b.start_datetime,
					b.end_datetime,
					b.modified_datetime,
					b.equipment_id,
					b.event_type_code,
					b.ERROR_CODE,
					b.interrupt_condition,
					b.log_summary
		 FROM nisis_geodata.nisis_ans a, nisisdata.nisis_rmls b
		WHERE a.ans_id = b.ans_id AND b.log_status = 'O' AND a.is_curr_rec = 1;

		----------------
		
		/* Formatted on 9/25/2014 3:36:34 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW NISIS.ANS_RMLS_VOID_VIEW
(
	 ANS_ID,
	 LOG_ID,
	 FAC_IDENT,
	 LOG_STATUS,
	 CODE_CATEGORY,
	 START_DATETIME,
	 END_DATETIME,
	 MODIFIED_DATETIME,
	 EQUIPMENT_ID,
	 EVENT_TYPE_CODE,
	 ERROR_CODE,
	 INTERRUPT_CONDITION,
	 LOG_SUMMARY
)
AS
	 SELECT b.ans_id,
					b.log_id,
					b.fac_ident,
					b.log_status,
					b.code_category,
					b.start_datetime,
					b.end_datetime,
					b.modified_datetime,
					b.equipment_id,
					b.event_type_code,
					b.ERROR_CODE,
					b.interrupt_condition,
					b.log_summary
		 FROM nisis_geodata.nisis_ans a, nisisdata.nisis_rmls b
		WHERE     a.ans_id = b.ans_id
					AND b.log_status IN ('C', 'V')
					AND a.is_curr_rec = 1
					AND b.start_datetime > (SYSDATE - 30);