-- sprint 10, 1st uat deployment

-- ALERTS changes

DROP TABLE NISIS.ALERTS;
CREATE TABLE NISIS.ALERTS
(
  ALERTID         NUMBER,
  OBJECTID        INTEGER,
  FAC_ID          VARCHAR2(10 BYTE),
  STATUS_TYPE_ID  NUMBER,
  CURRENT_STATUS  NUMBER,
  FUTURE_STATUS   NUMBER,
  ALERT_DATE      DATE,
  ACTION          VARCHAR2(100 BYTE),
  GROUPS          INTEGER,
  CREATED_BY      NUMBER(20),
  CREATED_DATE    DATE            DEFAULT SYSDATE,
  MODIFIED_BY     NUMBER(20), 
  MODIFIED_DATE   DATE            DEFAULT SYSDATE,
  INACTIVATED_BY  NUMBER(20), 
  INACTIVATED_DATE    DATE,
  ACTIVE          VARCHAR2(1 BYTE),
  DISPLAYED       VARCHAR2(1 BYTE)
);
CREATE OR REPLACE TRIGGER NISIS.ALERTS_TRG
BEFORE INSERT
ON NISIS.ALERTS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
-- For Toad:  Highlight column ALERTID
  :new.ALERTID := ALERTS_SEQ.nextval;
END ALERTS_TRG;
/

DROP TABLE NISIS.ALERT_GROUP;
CREATE TABLE NISIS.ALERT_GROUP
(
  ALERTID  NUMBER                               NOT NULL,
  GROUPID  NUMBER                               NOT NULL
);

DROP VIEW NISIS.ALERT_GROUP_NM_TMP;
CREATE OR REPLACE FORCE VIEW NISIS.ALERT_GROUP_NM_TMP
(
   ALERTID,
   GROUPID,
   NAME
)
AS
   SELECT a.alertid, a.groupid, g.name
     FROM alert_group a JOIN nisis_admin.groups g ON a.groupid = g.groupid;

-- allow nisis to see that view
GRANT SELECT ON NISIS_ADMIN.GROUPS TO NISIS;

CREATE OR REPLACE FUNCTION NISIS.insert_alert (
   in_objid          IN alerts.objectid%TYPE
  ,in_facid          IN alerts.fac_id%TYPE
  ,in_statustypeid   IN alerts.status_type_id%TYPE
  ,in_status         IN alerts.current_status%TYPE
  ,in_futstatus      IN alerts.future_status%TYPE
  ,in_alertdate      IN VARCHAR2
  ,in_qformat        IN VARCHAR2
  ,in_createdby      IN alerts.created_by%TYPE
  ,in_active         IN alerts.active%TYPE
  ,in_displayed      IN alerts.displayed%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO alerts (
                  objectid
                 ,fac_id
                 ,status_type_id
                 ,current_status
                 ,future_status
                 ,alert_date
                 ,created_by
                 ,active
                 ,displayed
               )
   VALUES (in_objid
          ,in_facid
          ,in_statustypeid
          ,in_status
          ,in_futstatus
          ,TO_DATE (in_alertdate, in_qformat)
          ,in_createdby
          ,in_active
          ,in_displayed)
   RETURNING alertid
     INTO v;

   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN -1;
END insert_alert;
/


--IWL changes

ALTER TABLE NISIS.NISIS_IWL
    ADD UG VARCHAR2(100) NULL;
    
UPDATE NISIS.NISIS_IWL
    SET UG = USER_GROUP
    WHERE ID IS NOT NULL;

ALTER TABLE NISIS.NISIS_IWL
    DROP COLUMN USER_GROUP;
    
ALTER TABLE NISIS.NISIS_IWL
    RENAME COLUMN UG TO USER_GROUP;
    
UPDATE NISIS.NISIS_IWL
    SET USER_GROUP = NULL
    WHERE ID IS NOT NULL;

COMMIT;