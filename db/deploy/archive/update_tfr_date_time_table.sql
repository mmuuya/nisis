/*
    Created 3/10/15
    -Haeden Howland
    This script addresses an issue with the database saving date/times and converting their input based
    on the user's timezone in relation to GMT.
    Instead of storing a date object (which was being incorrectly parsed in PHP), it is now storing a 
    number which represents the ticks. Kendo utilizes this format, so maintaining it in this manor
    seems easier.
*/

create table tfruser.bak_notam_tfr_date_time as select * from tfruser.notam_tfr_date_time;

update tfruser.notam_tfr_date_time set effective_date_time = null, expiration_date_time = null;

alter table tfruser.notam_tfr_date_time modify effective_date_time number modify expiration_date_time number;

commit; 