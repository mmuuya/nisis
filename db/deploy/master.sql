SET DEFINE OFF;

/******************************************************************************
-- Name:            master.sql
-- Purpose:         This script is intented to be the only script executed during
					package deployment to Test/Prod. As such, it automatically
					executes all other scripts and compiles new source code 
					distributed with the deployment package.
					Must be saved in /db/deploy/
-- Release Version: 1.1			
-- Developer:       Haeden Howland
-- Create Date:     3/17/2015
******************************************************************************/

--Format: @<sub-director(ies)/><filename.extension>;

@Add_Fields_Hawaii_Emergency_tables.sql;
@add_TFR_Reason_variable_Hazards_and_Airshow.sql;
@Update_ARTCC_Table.sql;
@Update_Status_Table.sql;
@create_expired_tfr_view_and_job.sql;
@Update_Security_Instruction_Option_text.sql;

-- added to master script 4/15/2015 gnewman
@insert_honolulu_artcc.sql;
@Update_Notam_Body_table_add_ref_id.sql

-- added to master script 4/21/2015 hhowland
@Nullify_Whitespace_NISIS_OSF.sql;
@remove_Emergency_new_traffic_rules_variable.sql;

-- added to master script 5/19/2015 hhowland
@Remove_CommonName_from_template.sql;

-- added to master script 5/20/2015 hhowland
@Update_NOTAM_TYPE_VIP_table.sql;
