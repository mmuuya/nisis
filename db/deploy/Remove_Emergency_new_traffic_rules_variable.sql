/*
    Minor update to the tfr templates to remove {NEW AIR TRAFFIC RULES SECTION}, which is unused 
    and has no rules pertaining it's handling. This should ONLY affect Emergency-type NOTAM templates.
    
    Haeden 4/21/2015
*/


select * from tfruser.lookup_tfr_types where template like '%{NEW AIR TRAFFIC RULES SECTION}%';

update tfruser.lookup_tfr_types set template = '!FDC Y/NNNN {ARTCC} {TERRITORY/STATE}..AIRSPACE {COMMON NAME}, {CITY, STATE}.. TEMPORARY FLIGHT RESTRICTION. PURSUANT TO 14 CFR SECTION 91.139, EMERGENCY AIR TRAFFIC RULES, AIRCRAFT OPERATIONS ARE PROHIBITED  {TFR AREA SECTION} {TFR EFFECTIVE TIMES SECTION} {COORDINATION FACILITY TEMPLATE} {EARLIEST START DTG}-{LATEST END DTG}'
where template like '%{NEW AIR TRAFFIC RULES SECTION}%' and name like 'EMERGENCY';

commit;