/*
    Script to update the Hawaii and Emergency extra tables with Coordinating Facility information.
    
    Haeden
    3/20/15
*/

--drop backup tables, if any
declare
l_count number;
l_owner varchar2(30);
l_table_name varchar2(50);
l_drop_sql varchar2(500);
begin

l_owner := 'TFRUSER';
l_table_name := 'BAK_NOTAM_TYPE_HAWAII';
select count(*) into l_count from all_tables where owner = l_owner and table_name = l_table_name;
dbms_output.put_line(l_table_name || ':' || l_count);
if (l_count > 0) then
    l_drop_sql := 'DROP TABLE ' || l_owner || '.' || l_table_name || ' CASCADE CONSTRAINTS';
    execute immediate l_drop_sql;
    dbms_output.put_line('Dropped table ' || l_table_name);
end if;

l_owner := 'TFRUSER';
l_table_name := 'BAK_NOTAM_TYPE_EMERGENCY_TRAFFIC';
select count(*) into l_count from all_tables where owner = l_owner and table_name = l_table_name;
dbms_output.put_line(l_table_name || ':' || l_count);
if (l_count > 0) then
    l_drop_sql := 'DROP TABLE ' || l_owner || '.' || l_table_name || ' CASCADE CONSTRAINTS';
    execute immediate l_drop_sql;
    dbms_output.put_line('Dropped table ' || l_table_name);
end if;

end;

--create backup tables to pull from later.

create table tfruser.bak_notam_type_hawaii as select * from TFRUSER.NOTAM_TYPE_HAWAII;

create table tfruser.bak_notam_type_emergency as select * from tfruser.notam_type_emergency_traffic;

drop table tfruser.notam_type_hawaii;

drop table tfruser.notam_type_emergency_traffic;

CREATE TABLE TFRUSER.NOTAM_TYPE_HAWAII
(
  NOTAM_ID       NUMBER                         NOT NULL,
  COORDINATING_FACILITY_ID   VARCHAR2(400 BYTE),
  COORD_FAC_NAME             VARCHAR2(200 BYTE),
  COORD_FAC_TYPE             VARCHAR2(200 BYTE),
  COORD_FAC_TYPE_ID          NUMBER,
  COORD_FAC_PHONES           VARCHAR2(400 BYTE),
  COORD_FAC_FREQUENCIES      VARCHAR2(400 BYTE),
  MODIFY_ID      NUMBER,
  MODIFY_REASON  VARCHAR2(4000 BYTE)
);

CREATE TABLE TFRUSER.NOTAM_TYPE_EMERGENCY_TRAFFIC
(
  NOTAM_ID       NUMBER                         NOT NULL,
  COORDINATING_FACILITY_ID   VARCHAR2(400 BYTE),
  COORD_FAC_NAME             VARCHAR2(200 BYTE),
  COORD_FAC_TYPE             VARCHAR2(200 BYTE),
  COORD_FAC_TYPE_ID          NUMBER,
  COORD_FAC_PHONES           VARCHAR2(400 BYTE),
  COORD_FAC_FREQUENCIES      VARCHAR2(400 BYTE),
  MODIFY_ID      NUMBER,
  MODIFY_REASON  VARCHAR2(4000 BYTE)
);

insert into tfruser.notam_type_hawaii (notam_id, modify_id, modify_reason) 
select b.notam_id, b.modify_id, b.modify_reason from tfruser.bak_notam_type_hawaii b; 

insert into tfruser.notam_type_emergency_traffic (notam_id, modify_id, modify_reason) 
select b.notam_id, b.modify_id, b.modify_reason from tfruser.bak_notam_type_emergency b; 

drop table tfruser.bak_notam_type_hawaii;

drop table tfruser.bak_notam_type_emergency;

update tfruser.lookup_tfr_types set 
    template = '!FDC Y/NNNN {ARTCC} {TERRITORY/STATE}..AIRSPACE {COMMON NAME}, {CITY, STATE}.. TEMPORARY FLIGHT RESTRICTION. PURSUANT TO 14 CFR SECTION 91.138, TEMPORARY FLIGHT RESTRICTIONS ARE IN EFFECT {TFR AREA SECTION} {TFR EFFECTIVE TIMES SECTION} {COORDINATION FACILITY TEMPLATE} {EARLIEST START DTG}-{LATEST END DTG}' 
where name = 'HAWAII';

update tfruser.lookup_tfr_types set 
    template = '!FDC Y/NNNN {ARTCC} {TERRITORY/STATE}..AIRSPACE {COMMON NAME}, {CITY, STATE}.. TEMPORARY FLIGHT RESTRICTION. PURSUANT TO 14 CFR SECTION 91.139, EMERGENCY AIR TRAFFIC RULES, AIRCRAFT OPERATIONS ARE PROHIBITED  {TFR AREA SECTION} {TFR EFFECTIVE TIMES SECTION} {NEW AIR TRAFFIC RULES SECTION} {COORDINATION FACILITY TEMPLATE} {EARLIEST START DTG}-{LATEST END DTG}'
where name = 'EMERGENCY';

commit;
