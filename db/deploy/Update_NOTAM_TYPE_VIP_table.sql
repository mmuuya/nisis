/*
    Creates a backup of the notam_type_vip table and then adds new fields for coordinating
    facility. Restores records from backup into new table.
    
    Haeden 05/20/2015
*/

declare
l_count number := 0;
l_owner varchar2(200);
l_table_name varchar2(200);
begin

l_owner := 'TFRUSER';
l_table_name := 'BAK_NOTAM_TYPE_VIP';

select count(*) into l_count from all_tables where owner = l_owner and table_name = l_table_name;

if l_count > 0 then
    execute immediate 'drop table ' || l_owner || '.' || l_table_name;
end if;


end;
/

CREATE TABLE TFRUSER.BAK_NOTAM_TYPE_VIP AS SELECT * FROM TFRUSER.NOTAM_TYPE_VIP;

DROP TABLE TFRUSER.NOTAM_TYPE_VIP CASCADE CONSTRAINTS;

CREATE TABLE TFRUSER.NOTAM_TYPE_VIP
(
  NOTAM_ID       NUMBER                         NOT NULL,
  COORDINATING_FACILITY_ID  VARCHAR2(400 BYTE),
  COORD_FAC_NAME            VARCHAR2(200 BYTE),
  COORD_FAC_TYPE            VARCHAR2(200 BYTE),
  COORD_FAC_TYPE_ID         NUMBER,
  COORD_FAC_PHONES          VARCHAR2(400 BYTE),
  COORD_FAC_FREQUENCIES     VARCHAR2(400 BYTE),
  MODIFY_ID      NUMBER,
  MODIFY_REASON  VARCHAR2(4000 BYTE)
);

insert into tfruser.notam_type_vip (notam_id, modify_id, modify_reason) select notam_id, modify_id, modify_reason from tfruser.bak_notam_type_vip;

drop table tfruser.bak_notam_type_vip;

update TFRUSER.LOOKUP_TFR_TYPES set template = replace(template, '{EARLIEST START DTG}', '{COORDINATION FACILITY TEMPLATE} {EARLIEST START DTG}') where ref = '91.141' and template like '%{TFR EFFECTIVE TIMES SECTION} {EARLIEST START DTG}%';

commit;
