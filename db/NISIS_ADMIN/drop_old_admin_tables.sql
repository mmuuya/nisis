--these tables are no longer referenced in the code, delete them!

drop table accesscats;
drop table accessgroup;
drop table acpermissions;
drop table blacklist;
drop table group_accesscat;
drop table permissionvalues;
drop table resets;
drop table users_bk;
drop table user_accesscat;
drop table whitelist;