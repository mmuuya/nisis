select * from acl_fields;

select * from acl_groups;

select * from acl_perms order by fieldid asc;

select a.aclid, a.fieldid, a.perm, b.fieldid, 'r' read_perm from acl_perms a join acl_perms b on a.fieldid = b.fieldid;

create table acl_perms_bak as select * from acl_perms;

select * from acl_perms_bak;


insert into acl_perms (fieldid, perm)  select fieldid, 'r' from acl_perms; 

commit;


