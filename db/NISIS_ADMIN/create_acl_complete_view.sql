CREATE OR REPLACE VIEW GRP_FIELDS_COMP_VIEW AS
SELECT
t.tableid,
t.table_nm,
t.display_nm AS table_display_nm,
f.col_nm,
f.display_nm AS field_display_nm,
f.fieldid,
g.aclid,
g.groupid,
grp.name,
p.perm
FROM acl_groups g, acl_perms p, acl_fields f, acl_tables t, groups grp
WHERE g.aclid = p.aclid
AND g.groupid = grp.groupid
AND p.fieldid = f.fieldid
AND f.tableid = t.tableid;