CREATE OR REPLACE FUNCTION map_group_to_table (in_gid IN NUMBER, in_tid IN NUMBER)
   RETURN VARCHAR2
IS
cnt NUMBER := 0;
BEGIN
   MERGE INTO nisis_admin.acl_groups g
        USING (SELECT f.fieldid,
                      f.col_nm,
                      f.display_nm,
                      f.tableid,
                      p.aclid aclid,
                      p.fieldid perm_fieldid,
                      p.perm,
                      (SELECT in_gid FROM DUAL) gid
                 FROM nisis_admin.acl_fields f, nisis_admin.acl_perms p
                WHERE     p.fieldid = f.fieldid
                      AND f.tableid = in_tid
                      AND p.perm = 'w') h
           ON (g.groupid = h.gid AND g.aclid = h.aclid)
   WHEN NOT MATCHED
   THEN
      INSERT     (groupid, aclid)
          VALUES (h.gid, h.aclid);
   cnt := SQL%ROWCOUNT;
   COMMIT;
   RETURN cnt;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN 'ERROR: '||SQLERRM;
END;
/

grant execute on nisis_admin.map_group_to_table to nisis;


CREATE OR REPLACE FUNCTION NISIS.insert_alert (
   in_objid          IN alerts.objectid%TYPE
  ,in_facid          IN alerts.fac_id%TYPE
  ,in_statustypeid   IN alerts.status_type_id%TYPE
  ,in_status         IN alerts.current_status%TYPE
  ,in_futstatus      IN alerts.future_status%TYPE
  ,in_alertdate      IN VARCHAR2
  ,in_qformat        IN VARCHAR2
  ,in_createdby      IN alerts.created_by%TYPE
  ,in_active         IN alerts.active%TYPE)
   RETURN NUMBER
IS
   v   NUMBER;
BEGIN
   INSERT INTO alerts (
                  objectid
                 ,fac_id
                 ,status_type_id
                 ,current_status
                 ,future_status
                 ,alert_date
                 ,created_by
                 ,active
               )
   VALUES (in_objid
          ,in_facid
          ,in_statustypeid
          ,in_status
          ,in_futstatus
          ,TO_DATE (in_alertdate, in_qformat)
          ,in_createdby
          ,in_active)
   RETURNING alertid
     INTO v;

   RETURN v;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN -1;
END insert_alert;
/

GRANT EXECUTE ON NISIS_ADMIN.map_group_to_r_table TO NISIS;




/* Formatted on 5/22/2014 3:58:30 PM (QP5 v5.240.12305.39446) */
SELECT *
  FROM acl_groups g,
       acl_perms p,
       acl_fields f,
       acl_tables t,
       groups grp
 WHERE     g.aclid = p.aclid
       AND g.groupid = grp.groupid
       AND p.fieldid = f.fieldid
       AND f.tableid = t.tableid;


CREATE OR REPLACE VIEW grp_fields_comp_view
AS
   SELECT t.tableid,
          t.table_nm,
          t.display_nm AS table_display_nm,
          f.col_nm,
          f.display_nm AS field_display_nm,
          f.fieldid,
          g.aclid,
          g.groupid,
          grp.name,
          p.perm
     FROM acl_groups g,
          acl_perms p,
          acl_fields f,
          acl_tables t,
          groups grp
    WHERE     g.aclid = p.aclid
          AND g.groupid = grp.groupid
          AND p.fieldid = f.fieldid
          AND f.tableid = t.tableid;
          
          
          
          
SELECT * FROM grp_fields_comp_view x where x.aclid = 427;