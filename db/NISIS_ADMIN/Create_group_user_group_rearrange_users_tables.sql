CREATE TABLE NISIS_ADMIN.GROUPS
(
  GROUPID     NUMBER                 NOT NULL,
  NAME        VARCHAR2(127 BYTE)                 NOT NULL,
  DATECREATED  DATE                 NOT NULL
);


CREATE TABLE NISIS_ADMIN.USER_GROUPS
(
  USERID     NUMBER                 NOT NULL,
  GROUPID       NUMBER              NOT NULL,
  DATECREATED  DATE                 NOT NULL
);

drop table nisis_admin.groups;


ALTER TABLE NISIS_ADMIN.USERS
    ADD DATECREATED DATE;
    
UPDATE NISIS_ADMIN.USERS SET DATECREATED = TO_DATE('01/31/2014 11:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM');    
    
CREATE TABLE NISIS_ADMIN.USERS_BK AS SELECT * FROM NISIS_ADMIN.USERS;

DROP TABLE NISIS_ADMIN.USERS;

CREATE TABLE NISIS_ADMIN.USERS
(
  USERID    NUMBER                              NOT NULL,
  USERNAME  VARCHAR2(255 BYTE)                  NOT NULL,
  FNAME     VARCHAR2(50 BYTE)                   NOT NULL,
  LNAME     VARCHAR2(50 BYTE)                   NOT NULL,
  EMAIL     VARCHAR2(255 BYTE)                  NOT NULL,
  PASSWORD  VARCHAR2(128 BYTE)                  NOT NULL,
  SALT      VARCHAR2(13 BYTE)                   NOT NULL,
  ACTIVE    NUMBER(1)                           DEFAULT 1                     NOT NULL
);

SET DEFINE OFF;
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('Glen', 'Newman', 'glen.newman@concept-solutions.com', 'gnewman', 17, 
    '299961d84f0dd8b95cbf870d6a832f2056348e7aeab569e64c81f5f3e9b7cb74b9f1fbd14c38627544e8be2ad602c2f40cf95021873ddf8d37795aae777683b1', '52e13f7f0ebfb', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('System', 'Admin', 'alan.truong@concept-solutions.com', 'SysAdmin', 1, 
    'cab5644224a0b4909a197f8727c2799d587ed6c28b74df0aece87bbda7c74c30bf11a1208edde5635e0b4f47a87c975e4a1fc1aa93dd8d399096ef1cdcac0172', '520271537fa79', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('John', 'Evans', 'john.evans@concept-solutions.com', 'test1', 2, 
    '31f5458d23d8beda1a22bc757a30a4e57e9e5e791669fb3da3307a890b80916c39eccce80b6ad1e5f2aa24a76fce2317985aa3d315f3cbde848d7dcf43d159a8', '520271537fa78', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('Chris', 'Petrolino', 'AABoyles@gmail.com', 'ChrisP', 4, 
    '18ae6b758684a73bf057bbf95fa0d9a78aee638bad669f0427a1485cacb49fea9f249481b878a1e1f48064be596188c20fd0b976144b809640bebd801b2ba15f', '520271537fa80', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('Alan', 'Truong', 'alan.truong@concept-solutions.com', 'atruong', 5, 
    '6dc05755d9a60284eda2d0a078d39c0af32fbc36132dd479032ee265a67cde544362b0310c5a5f6b1bc1aae25ecc917bb1776ddde8fd0f88f7ddaef3a697898b', '520271537fa84', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('Haeden', 'Howland', 'haeden.howland@concept-solutions.com', 'hhowland', 6, 
    '6622a698a00005963f0f7a39a44bb16ccbad206d9852a3a50ddb95b64cdf637eda15ad4d0c1330dda7d3ec537788a70a122bf35deabcaf6e7593436d100eafb9', '5202802d51bf6', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('John', 'Evans', 'john.evans@concept-solutions.com', 'John.Evans', 8, 
    '31f5458d23d8beda1a22bc757a30a4e57e9e5e791669fb3da3307a890b80916c39eccce80b6ad1e5f2aa24a76fce2317985aa3d315f3cbde848d7dcf43d159a8', '520271537fa78', 1);
Insert into NISIS_ADMIN.USERS
   (FNAME, LNAME, EMAIL, USERNAME, USERID, 
    PASSWORD, SALT, ACTIVE)
 Values
   ('Baboyma', 'Kagniniwa', 'baboyma.kagniniwa@concept-solutions.com', 'baboyma', 9, 
    'cab5644224a0b4909a197f8727c2799d587ed6c28b74df0aece87bbda7c74c30bf11a1208edde5635e0b4f47a87c975e4a1fc1aa93dd8d399096ef1cdcac0172', '520271537fa79', 1);
COMMIT;
    
    
COMMIT;    