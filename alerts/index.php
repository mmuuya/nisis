<?php
// require ('../handler.php');

// check for a valid session
$loggedin = isset($_SESSION['username']);

if ($loggedin) {
	include ('html/main_panel.php');
} else {
	error_log("redirecting from Alerts");
	include ('../html/redirect.php');
}
?>
