<?php 
$dateFormat = 'YYYY/MM/DD HH24:MI:SS';

//Make Sure the DB is wired up correctly
if ($db === FALSE) {
    kill('No Database Connection');
}

//Get alerts by ObjectID
// function getAlertsByObjectID($objectID) {
//     $query = "SELECT * FROM ALERTS
//                 WHERE OBJECTID=:objId";

//     $parsed = parse($query);
//     oci_bind_by_name($parsed, ":objId", $objectID);
//     oci_execute($parsed);
//     fetch_all_rows($parsed, $results);
    
//     return $results;
// }

function getAlertsByObjectID($objectID) {
    global $dateFormat;        

    $query = "SELECT a.alertid,
        a.objectid,
        a.fac_id,
        a.fac_type,
        a.status_type_id,
        a.display_nm,
        a.current_status_label,
        a.future_status,
        a.future_status_label,
        TO_CHAR (alert_date, :qformat) AS alert_date,
        x.groups,
        a.active,
        (b.fname || ' ' || b.lname) AS created_by,
        TO_CHAR (created_date, :qformat) AS created_date,
        (c.fname || ' ' || c.lname) AS inactivated_by,
        TO_CHAR (inactivated_date, :qformat)
        AS inactivated_date
        FROM nisis.alert_vw a
        LEFT OUTER JOIN nisis.fac_status_type_vw
        ON a.status_type_id = fac_status_type_vw.status_id
        LEFT OUTER JOIN appuser.users b ON a.created_by = b.userid
        LEFT OUTER JOIN appuser.users c ON a.inactivated_by = c.userid
        LEFT OUTER JOIN
        (  SELECT alertid AS aid,
        LISTAGG (groupid, ',') WITHIN GROUP (ORDER BY groupid)
        AS groups
        FROM nisis.alert_groupname_vw
        GROUP BY alertid) x
        ON a.alertid = x.aid        
        WHERE a.objectid = :oid";


    $parsed = parse($query);
    oci_bind_by_name($parsed, ':qformat', $dateFormat);
    oci_bind_by_name($parsed, ':oid', $objectID);

    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    return $results;
}

//Get All alerts
function getAllAlerts() {
    global $dateFormat;

    // $query = "SELECT 
    //             ALERTID,
    //             OBJECTID,
    //             FAC_ID, 
    //             FAC_TYPE, 
    //             STATUS_TYPE_ID,
    //             DISPLAY_NM,
    //             CURRENT_STATUS_LABEL,
    //             FUTURE_STATUS,
    //             FUTURE_STATUS_LABEL,
    //             to_char(ALERT_DATE, :qformat) as ALERT_DATE,
    //             (SELECT LISTAGG(GROUPID, ',') WITHIN GROUP (ORDER BY GROUPID)
    //                 FROM NISIS.ALERT_GROUPNAME_VW 
    //                 WHERE ALERTID = A.ALERTID) as GROUPS,
    //             A.ACTIVE,
    //             (B.FNAME || ' ' || B.LNAME) as CREATED_BY,
    //             to_char(CREATED_DATE, :qformat) as CREATED_DATE,
    //             (C.FNAME || ' ' || C.LNAME) as INACTIVATED_BY, 
    //             to_char(INACTIVATED_DATE, :qformat) as INACTIVATED_DATE
    //             FROM ALERT_VW A
    //             LEFT OUTER JOIN FAC_STATUS_TYPE_VW ON A.STATUS_TYPE_ID = FAC_STATUS_TYPE_VW.STATUS_ID
    //             LEFT OUTER JOIN APPUSER.USERS B ON A.CREATED_BY = B.USERID
    //             LEFT OUTER JOIN APPUSER.USERS C ON A.INACTIVATED_BY = C.USERID";


    // KOFI: NISIS-2139 - created a materialized view for this cumbersome query.
    // $query = "SELECT 
    //             ALERTID, 
    //             OBJECTID, 
    //             FAC_ID, 
    //             FAC_TYPE, 
    //             STATUS_TYPE_ID, 
    //             DISPLAY_NM, 
    //             CURRENT_STATUS_LABEL, 
    //             FUTURE_STATUS, 
    //             FUTURE_STATUS_LABEL, 
    //             ALERT_DATE, 
    //             GROUPS, 
    //             ACTIVE, 
    //             CREATED_BY, 
    //             CREATED_DATE, 
    //             INACTIVATED_BY, 
    //             INACTIVATED_DATE
    //         FROM NISIS.ALERTS_MV";

    // KOFI: NISIS-2389 - Reversed the alerts_mv materialized view idea for now.  
    // Was taking too long to refresh mview. Original is a cumbersome query, but works.
    // At least autoBind = false, so user only sees the slowness if they want to really see REminders.
    $query = "SELECT a.alertid,
        a.objectid,
        a.fac_id,
        a.fac_type,
        a.status_type_id,
        a.display_nm,
        a.current_status_label,
        a.future_status,
        a.future_status_label,
        TO_CHAR (alert_date, :qformat) AS alert_date,
        x.groups,
        a.active,
        (b.fname || ' ' || b.lname) AS created_by,
        TO_CHAR (created_date, :qformat) AS created_date,
        (c.fname || ' ' || c.lname) AS inactivated_by,
        TO_CHAR (inactivated_date, :qformat)
        AS inactivated_date
        FROM nisis.alert_vw a
        LEFT OUTER JOIN nisis.fac_status_type_vw
        ON a.status_type_id = fac_status_type_vw.status_id
        LEFT OUTER JOIN appuser.users b ON a.created_by = b.userid
        LEFT OUTER JOIN appuser.users c ON a.inactivated_by = c.userid
        LEFT OUTER JOIN
        (  SELECT alertid AS aid,
        LISTAGG (groupid, ',') WITHIN GROUP (ORDER BY groupid)
        AS groups
        FROM nisis.alert_groupname_vw
        GROUP BY alertid) x
        ON a.alertid = x.aid";


    $parsed = parse($query);
    oci_bind_by_name($parsed, ':qformat', $dateFormat);

    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    return $results;
}

//Create the alerts
function createAlerts($data) {
    $i = 0;
    global $dateFormat;
    
    foreach ($data as $record) {
        $j = 0;
        $alertId = "";
        $objId = $record['OBJECTID'];
        $facId = $record['FAC_ID'];
        
        $statusTypeId = $record['STATUS_TYPE_ID'];
        $futStatus = $record['FUTURE_STATUS'];
        $alertDate = $record['ALERT_DATE'];
        $groups = $record['GROUPS'];
        $createdBy = $_SESSION['userid'];
        $active = $record['ACTIVE'];

        $query = 'BEGIN :alertId := INSERT_ALERT (
                    :objId,
                    :facId,
                    :statusTypeId,
                    :futStatus,
                    :alertDate,
                    :qformat,
                    :createdBy,
                    :active
                    );
                    END;';

        $parsed = parse($query);

        oci_bind_by_name($parsed, ":alertId", $alertId, 2000);
        oci_bind_by_name($parsed, ":objId", $objId);
        oci_bind_by_name($parsed, ":facId", $facId);
        oci_bind_by_name($parsed, ":statusTypeId", $statusTypeId);
        oci_bind_by_name($parsed, ":futStatus", $futStatus);
        oci_bind_by_name($parsed, ":alertDate", $alertDate);
        oci_bind_by_name($parsed, ':qformat', $dateFormat);
        oci_bind_by_name($parsed, ":createdBy", $createdBy);
        oci_bind_by_name($parsed, ":active", $active);

        if(!oci_execute($parsed)) {
            kill("DATABASE ERROR executing createAlerts method with the record number: ". $i . ".");
        }
        $i++;

        //Inserting the groups in ALERT_GROUP table.
        if ($groups != null){
            foreach ($groups as $gr){
                $aId = $alertId;
                
                $queryGroups = 'INSERT INTO ALERT_GROUP (ALERTID, GROUPID) VALUES(:aId, :groupId)';
                
                $parsedGroups = parse($queryGroups);

                oci_bind_by_name($parsedGroups, ":aId", $aId);
                oci_bind_by_name($parsedGroups, ":groupId", $gr);

                if(!oci_execute($parsedGroups)) {
                    kill("DATABASE ERROR Inserting ALERT_GROUP record in createAlerts method with the record number: ". $j . ".");
                }
                $j++;
            }
        }
    }//first for loop
}

//Update the alerts
function updateAlerts($data) {
    global $dateFormat;

    $i = 0;
    foreach ($data as $record) {
        $j = 0;
        $alertId = $record['ALERTID'];
        $objId = $record['OBJECTID'];
        $statusTypeId = $record['STATUS_TYPE_ID'];
        $futStatus = $record['FUTURE_STATUS'];
        $alertDate = $record['ALERT_DATE'];
        $createdDate = $record['CREATED_DATE'];
        $groups = isset($record['GROUPS']) ? $record['GROUPS'] : null;
        $modifiedBy = $_SESSION['userid'];
        $active = $record['ACTIVE'];

        $query = 'UPDATE ALERTS SET
                    OBJECTID=:objId,
                    STATUS_TYPE_ID=:statusTypeId,
                    FUTURE_STATUS=:futStatus,
                    ALERT_DATE=to_date(:alertDate, :qformat),
                    MODIFIED_BY=:modifiedBy,
                    MODIFIED_DATE=SYS_EXTRACT_UTC(SYSTIMESTAMP),
                    ACTIVE=:active
                WHERE ALERTID=:alertId';

        $parsed = parse($query);

        oci_bind_by_name($parsed, ":alertId", $alertId);
        oci_bind_by_name($parsed, ":objId", $objId);
        oci_bind_by_name($parsed, ":statusTypeId", $statusTypeId);
        oci_bind_by_name($parsed, ":futStatus", $futStatus);
        oci_bind_by_name($parsed, ":alertDate", $alertDate);
        oci_bind_by_name($parsed, ':qformat', $dateFormat);
        oci_bind_by_name($parsed, ":modifiedBy", $modifiedBy);
        oci_bind_by_name($parsed, ":active", $active);
        
        if(!oci_execute($parsed)) {
            kill("DATABASE ERROR executing updateAlerts method with the record number: ". $i . ".");
        }
        $i++;

        //In order to update the ALERT_GROUP table we need to remove the old records where ALERTID=$alertId and inserting the updated groups as new records
        //Deleting all the records with alertid=$alertId
        $deleteQuery = 'DELETE FROM ALERT_GROUP WHERE ALERTID = :aId';
        $parsedDelete = parse($deleteQuery);
        oci_bind_by_name($parsedDelete, ":aId", $alertId);

        if(!oci_execute($parsedDelete)) {
            kill("DATABASE ERROR deleting records from ALERT_GROUP table in updateAlerts method where ALERTID=".$aId." and GROUPID=".$gr.".");
        }
        
        //Updating the groups in ALERT_GROUP table: Inserting the new records with the new groups
        //If $groups is null the inserting won't be executed
        if ($groups != null){
            foreach ($groups as $gr){
                $aId = $alertId;

                $queryGroups = 'INSERT INTO ALERT_GROUP (ALERTID, GROUPID) VALUES(:aId, :groupId)';
                $parsedGroups = parse($queryGroups);

                oci_bind_by_name($parsedGroups, ":aId", $aId);
                oci_bind_by_name($parsedGroups, ":groupId", $gr);

                if(!oci_execute($parsedGroups)) {
                    kill("DATABASE ERROR Updating ALERT_GROUP records in updateAlerts method with the record number: ". $j . ".");
                }
                $j++;
            }
        }
    }

}

//Get all facility types
function getAllFacilityTypes() {
    $query = "SELECT DISTINCT FAC_TYPE AS ID, FAC_TYPE AS TYPE FROM FAC_STATUS_TYPE_VW WHERE FAC_TYPE NOT IN ('INC', 'RT')";

    $parsed = parse($query);
    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    
    return $results;
}

//Get all status types
function getAllStatusTypes() {
    // NISIS-2795 - kOFI HONU
    // $query = "SELECT A.STATUS_ID AS ID,
    //                A.STATUS_COLUMN,
    //                A.STATUS_FULLNAME,
    //                A.FAC_TYPE
    //         FROM FAC_STATUS_TYPE_VW A, ALERT_STATUS_COLS B
    //         WHERE B.DATAFIELD_ID = A.STATUS_ID";
    $query = "SELECT A.STATUS_ID AS ID,
                   A.STATUS_COLUMN,
                   A.STATUS_FULLNAME,
                   A.FAC_TYPE
            FROM FAC_STATUS_TYPE_VW A";

    $parsed = parse($query);
    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    
    return $results;
}

//Get all status types by fac type
function getStatusTypesByFacType($facType) {
    //NISIS-2795 kOFI HONU
    // $query = "SELECT A.STATUS_ID AS ID,
    //                A.STATUS_COLUMN,
    //                A.STATUS_FULLNAME,
    //                A.FAC_TYPE
    //         FROM FAC_STATUS_TYPE_VW A, ALERT_STATUS_COLS B
    //         WHERE B.DATAFIELD_ID = A.STATUS_ID AND A.FAC_TYPE = :facType";

    $query = "SELECT A.STATUS_ID AS ID,
                   A.STATUS_COLUMN,
                   A.STATUS_FULLNAME,
                   A.FAC_TYPE
            FROM FAC_STATUS_TYPE_VW A
            WHERE A.FAC_TYPE = :facType";

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":facType", $facType);
    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    
    return $results;
}

//function to get the status values for the status dropdown
function getStatusValues($statusId){
    $query = "SELECT VALUE, LABEL FROM PICKLIST_ITEMS P
                JOIN FAC_STATUS_TYPE_VW F ON P.PICKLIST = F.PICKLIST
                WHERE F.STATUS_ID = :statusId";

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":statusId", $statusId);
    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    
    return $results;
}

//Get all status types
function getAllGroups() {
    $query = "SELECT GROUPID, NAME FROM NISIS_ADMIN.GROUPS";

    $parsed = parse($query);
    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    
    return $results;
}

//Get the reminders per user and groups where the user belong and per Interval
function getRemindersPerUserPerInterval() {
    global $dateFormat;
    // if (isset($_SESSION['lastRemindersCheck'])){
    //     $startTime = $_SESSION['lastRemindersCheck'];
    // }else{
        // Getting the current sysdate
        // $queryDate = "SELECT to_char(SYS_EXTRACT_UTC(SYSTIMESTAMP) - 5/86400, :qformat) DT FROM DUAL";
        // $parsedDate = parse($queryDate);
        // oci_bind_by_name($parsedDate, ':qformat', $dateFormat);
        // oci_execute($parsedDate);
        // fetch_all_rows($parsedDate, $dateResults);

        // if(count($dateResults) === 1){
        //     $sysdate = $dateResults[0]['DT'];
        // }else{
        //     error_log("ERROR: Trying to get the sysdate. The query returns more than 1 record in getRemindersPerUserPerInterval()");
        // }

        //Start time would be the current time minus 5 seconds
        // $st = strtotime($sysdate) - 5;
        // $startTime = date("Y/m/d H:i:s", $st);
    //}
    
    // $et = strtotime($startTime) + 5;
    // $endTime = date("Y/m/d H:i:s", $et);
    //$_SESSION['lastRemindersCheck'] = $endTime;
    $userId = $_SESSION['userid'];

    $query = "SELECT A.ALERTID, A.OBJECTID, A.FAC_ID, A.DISPLAY_NM, A.FUTURE_STATUS, A.FUTURE_STATUS_LABEL, to_char(A.ALERT_DATE, :qformat) as ALERT_DATE
                FROM ALERT_VW A 
                JOIN USER_ALERTS_VW U ON A.ALERTID = U.REF_ALERTID 
                WHERE U.USERID = :userId
                AND A.ALERT_DATE < SYSDATE 
                --AND A.ALERT_DATE < to_date(:endTime, :qformat)
                AND A.ACTIVE = 'Y'
                AND NVL(A.CURRENT_STATUS,999) != A.FUTURE_STATUS
                ORDER BY A.ALERTID DESC";

    $parsed = parse($query);
    oci_bind_by_name($parsed, ':qformat', $dateFormat);
    oci_bind_by_name($parsed, ":userId", $userId);
    //oci_bind_by_name($parsed, ":startTime", $startTime);
    //oci_bind_by_name($parsed, ":endTime", $endTime);

    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
    error_log(var_export($results, true));

    return $results;
}

//Get the missed alerts per user and groups where the user belong since the application was launched until now
function getMissedAlerts() {
    global $dateFormat;
   
    // Getting the current sysdate
    $queryDate = "SELECT to_char(SYS_EXTRACT_UTC(SYSTIMESTAMP), :qformat) DT FROM DUAL";
    $parsedDate = parse($queryDate);
    oci_bind_by_name($parsedDate, ':qformat', $dateFormat);
    oci_execute($parsedDate);
    fetch_all_rows($parsedDate, $dateResults);

    if(count($dateResults) === 1){
        $sysdate = $dateResults[0]['DT'];
    }else{
        error_log("ERROR: Trying to get the sysdate returns more than 1 record in getRemindersPerUserPerInterval()");
    }

    //Should not be Reminders (Alerts) before this date
    $st = strtotime("2014/06/01 00:00:00");
    $startDate = date("Y/m/d H:i:s", $st);

    //End time will be the current time
    $et = strtotime($sysdate);
    $endDate = date("Y/m/d H:i:s", $et);
    $userId = $_SESSION['userid'];

    $query = "SELECT A.ALERTID, A.OBJECTID, A.FAC_ID, A.DISPLAY_NM, A.FUTURE_STATUS, A.FUTURE_STATUS_LABEL, to_char(A.ALERT_DATE, :qformat) as ALERT_DATE
                FROM ALERT_VW A 
                JOIN USER_ALERTS_VW U ON A.ALERTID = U.REF_ALERTID 
                WHERE U.USERID = :userId
                AND A.ALERT_DATE >= to_date(:startDate, :qformat) 
                AND A.ALERT_DATE < to_date(:endDate, :qformat)
                AND A.ACTIVE = 'Y'
                AND NVL(A.CURRENT_STATUS,999) != A.FUTURE_STATUS
                ORDER BY A.ALERTID DESC";

    $parsed = parse($query);
    oci_bind_by_name($parsed, ':qformat', $dateFormat);
    oci_bind_by_name($parsed, ":userId", $userId);
    oci_bind_by_name($parsed, ":startDate", $startDate);
    oci_bind_by_name($parsed, ":endDate", $endDate);

    oci_execute($parsed);
    fetch_all_rows($parsed, $results);
// error_log(var_export($results, true));
    return $results;
}

// function inactivateAlert($id){
//     $inactivatedBy = $_SESSION['userid'];

//     $selectQuery = "SELECT INACTIVATED_BY FROM ALERTS WHERE ALERTID=:aId"; 
//     $parsedSelect = parse($selectQuery);

//     oci_bind_by_name($parsedSelect, ":aId", $id);

//     if(!oci_execute($parsedSelect)) {
//         kill("DATABASE ERROR executing inactivateAlert method when getting inactivatedBy column");
//     }

//     fetch_all_rows($parsedSelect, $results);

//     if(count($results) === 1){
//         $user = $results[0]['INACTIVATED_BY'];
//         if($user === null){
//             $query = "UPDATE ALERTS SET
//                     ACTIVE='N',
//                     INACTIVATED_BY=:inactivatedBy,
//                     INACTIVATED_DATE=SYS_EXTRACT_UTC(SYSTIMESTAMP)
//                 WHERE ALERTID=:alertId";

//             $parsed = parse($query);

//             oci_bind_by_name($parsed, ":inactivatedBy", $inactivatedBy);
//             oci_bind_by_name($parsed, ":alertId", $id);

//             if(!oci_execute($parsed)) {
//                 kill("DATABASE ERROR executing inactivateAlert method when updating the alert");
//             }
//         }

//     }else{
//         error_log(var_export($results, true));
//         kill("Error in inactivateAlert method because the SELECT query returned more than 1 record");
//     }
// }



// NISIS-2947 KOFI HONU - fixes to alert inactivation
function inactivateAlert($id){
    $inactivatedBy = $_SESSION['userid'];
    $query = "UPDATE ALERTS SET
            ACTIVE='N',
            INACTIVATED_BY=:inactivatedBy,
            INACTIVATED_DATE=SYSDATE
        WHERE ALERTID=:alertId AND INACTIVATED_BY IS NULL";

    $parsed = parse($query);

    oci_bind_by_name($parsed, ":inactivatedBy", $inactivatedBy);
    oci_bind_by_name($parsed, ":alertId", $id);

    if(!oci_execute($parsed, OCI_COMMIT_ON_SUCCESS )) {
        kill("DATABASE ERROR executing inactivateAlert method when updating the alert");
    }
}

//Get the notifications
function getNotifications() {
    global $dateFormat;

    if (isset($_SESSION['lastNotificationsCheck'])){
        $startTime = $_SESSION['lastNotificationsCheck'];
    }else{
        // Getting the current sysdate
        $queryDate = "SELECT to_char(SYS_EXTRACT_UTC(SYSTIMESTAMP), :qformat) DT FROM DUAL";
        $parsedDate = parse($queryDate);
        oci_bind_by_name($parsedDate, ':qformat', $dateFormat);
        oci_execute($parsedDate);
        fetch_all_rows($parsedDate, $dateResults);

        if(count($dateResults) === 1){
            $sysdate = $dateResults[0]['DT'];
        }else{
            error_log("ERROR: Trying to get the sysdate returns more than 1 record in getNotifications()");
        }

        //Start time would be the current time minus 5 seconds
        $st = strtotime($sysdate) - 5;
        $startTime = date("Y/m/d H:i:s", $st);
    }
    
    $et = strtotime($startTime) + 5;
    $endTime = date("Y/m/d H:i:s", $et);
    $_SESSION['lastNotificationsCheck'] = $endTime;
    $userId = $_SESSION['userid'];

    $query= "SELECT A.NOTID, A.FAC_TYPE, A.FAC_ID, A.COLUMN_NM, A.DISPLAY_NM, A.OLD_VALUE, A.OLD_LABEL, A.NEW_VALUE, A.NEW_LABEL, to_char(A.CREATED_DATE, :qformat) as CREATED_DATE
                FROM NISIS.NOTIFICATIONS_VW A 
                JOIN (SELECT DISTINCT REF_NOTID, USERID FROM NISIS.USER_NOTIFICATIONS_VW) U ON A.NOTID = U.REF_NOTID 
                WHERE U.USERID = :userId
                AND A.CREATED_DATE >= to_date(:startTime, :qformat)";

    // error_log("startTime: $startTime - endTime: $endTime");
    // error_log($query);

    $parsed = parse($query);
    oci_bind_by_name($parsed, ':userId', $userId);
    oci_bind_by_name($parsed, ':qformat', $dateFormat);
    oci_bind_by_name($parsed, ':startTime', $startTime);
    // oci_bind_by_name($parsed, ':endTime', $endTime);

    oci_execute($parsed);
    fetch_all_rows($parsed, $results);

    // error_log("$dateFormat $userId $startTime $endTime");
    // error_log(var_export($results, true));

    return $results;
}

//Flatten a multi dimensional associative array into a much simpler
//array with int indices. Loops over the outer array, pushing the value
//of the key specified to the resulting array.
//
//$assArray is the associative array to flatten
//$key specifies which item on each row will get added to the array
function flattenAss($assArray, $key) {
    $result = array();

    foreach ($assArray as $item) {
        array_push($result, $item[$key]);
    }

    return $result;
}

//wrap oci_fetch_all with flags to fetch by row and get an associative array
function fetch_all_rows($statementID, &$output) {
    oci_fetch_all($statementID, $output, 0, -1, OCI_FETCHSTATEMENT_BY_ROW+OCI_ASSOC);
}

//wrap oci_parse() - don't need to reference $db everywhere
function parse($queryStr) {
    global $db;
    return oci_parse($db, $queryStr);
}

?>