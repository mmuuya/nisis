<?php
require "query_functions.php";

//ACTIONS
$function = $_POST['function'];
if (isset($function)) {
    $result = array("result" => "Success");
    
    switch($function) {
        case "getAlertsByOid":
            $objectID = $_POST['oid'];
            $result['alerts'] = getAlertsByObjectID($objectID);
            break;
        case "getAlerts":
            $result['alerts'] = getAllAlerts();
            break;
        case "updateAlerts":
            $data = $_POST['data'];
            updateAlerts($data);
            break;
        case "createAlerts":
            $data = $_POST['data'];
            createAlerts($data);
            break;
        case "getFacilityTypes":
            $result['facilityTypes'] = getAllFacilityTypes();
            break;
        case "getStatusTypes":
            $result['statusTypes'] = getAllStatusTypes();
            break;
        case "getStatusTypesByFacType":
            $facType = $_POST['facType'];
            $result['statusTypes'] = getStatusTypesByFacType($facType);
            break;
        case "getStatusValues":
            $statusId = $_POST['statusId'];
            $result['statusValues'] = getStatusValues($statusId);
            break;
        case "getGroups":
            $result['groups'] = getAllGroups();
            break;
        case "getMissedAlerts":
            // $startDate = $_POST['startDate'];
            // $endDate = $_POST['endDate'];
            $result['missedAlerts'] = getMissedAlerts();
            break;
        case "getRemindersPerUserPerInterval":
            // $startTime = $_POST['currentTime'];
            // $endTime = $_POST['nextTime'];
            $result['reminders'] = getRemindersPerUserPerInterval();
            break;
        case "inactivateAlert":
            $id = $_POST['id'];
            inactivateAlert($id);
            break;
        case "getNotifications":
            // $startTime = $_POST['currentTime'];
            // $endTime = $_POST['nextTime'];
            $result['notifications'] = getNotifications();
            break;
        }

    //remember, kill args:
    //    0: array or string to encode as json
    //    1: specify whether to write it to the error log (TRUE by default)
    kill($result, FALSE);
}

kill("Bad call to actions.php.");
?>