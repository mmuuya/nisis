<?php
//db must be connected
if ($db === FALSE) {
    kill('No Database Connection');
}

require "alerts/api/query_functions.php";
?>

<div id="reminders" class="k-content">
    <div id="popupAlertDiv"></div>
    <div id="popupNotificationDiv"></div>
    <div id="alerts-div" class="k-content">
        <div id="alertTable"></div>
        <div id="readOnlyGrid" class="hideGrid"></div>
    </div>

    <style>
        #alerts-div{
            overflow: auto;
        }
        .hideGrid {
            display: none;
        }
        .k-grid tbody tr{
            height: 30px;
        }
        #readOnlyGrid td, #alertTable td {
            white-space: nowrap;
        }
    </style>

    <script>
        /**
         *
         * The js scripts has been moved  
         * to alerts/scripts/alerts.js for debugging purpuses
         * Feel free to move it back 
         * if it works for you
         */
    </script>
    <script type="text/javascript" src="alerts/scripts/alerts.js"></script>
</div>