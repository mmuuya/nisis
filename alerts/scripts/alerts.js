$(document).ready(function() {
    /**
     * Alerts Datasource
     *
     * This has been moved here for debugging purposes
     * Feel free to move it back 
     * if it works for you
     */
    var alertsDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            update: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            create: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            destroy: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            parameterMap: function(options, operation) {
                if (operation === "update" || operation === "create") {
                    $.each(options.models, function(i, val){
                        val.ALERT_DATE = kendo.toString(new Date(val.ALERT_DATE), "yyyy/MM/dd HH:mm:ss");
                        val.CREATED_DATE = kendo.toString(new Date(val.CREATED_DATE), "yyyy/MM/dd HH:mm:ss");
                    });
                }

                if (operation === 'read') {
                    return { 
                        //NISIS-2531 KOFI HONU - why are we querying ALL alerts in whole db?!
                        "action" : "actions", 
                        "function" : "getAlerts"
                    };
                }
                else if (operation === 'update') {
                    return { 
                        "action" : "actions", 
                        "function" : "updateAlerts", 
                        "data" : options.models
                    };
                }
                else if (operation === 'create') {
                    return { 
                        "action" : "actions", 
                        "function" : "createAlerts", 
                        "data" : options.models
                    };
                } else if (operation === 'destroy') {
                    //  return { 
                    //     "action" : "actions", 
                    //     "function" : "deleteAlertByOid", 
                    //     "oid" : "1111"
                    // };
                }
            }
        },
        schema: {
            model: {
                id: "ALERTID", 
                fields: {
                    ALERTID: {editable: false},
                    OBJECTID: {editable: false, nullable: true},
                    FAC_ID: {editable: false},
                    FAC_TYPE: {editable: false},
                    STATUS_TYPE_ID: {editable: false},
                    DISPLAY_NM: {editable: false},
                    CURRENT_STATUS_LABEL: {editable: false},
                    FUTURE_STATUS: {validation: {required: {message: "Future Status is required"}}},
                    FUTURE_STATUS_LABEL: {editable: false},
                    ALERT_DATE: {type: "date", validation: {required: {message: "Date/Time for alert is required"}}},
                    GROUPS: {nullable: true },
                    ACTIVE: {type: "string", defaultValue: "Y"}, 
                    CREATED_BY: {editable: false},
                    CREATED_DATE: {editable: false},
                    INACTIVATED_BY: {editable: false},
                    INACTIVATED_DATE: {editable: false}
                }
            },
            data: "alerts",
            total: function (response) {
                return response.alerts.length;
            },
        },
        batch: true,
        pageSize: 10,
        change: function(e) {

            // NISIS-2531 - KOFI HONU - Had to chain up the datasource reads to ensure data is available for subsequent fetches.

            // if (nisis.alerts.facType !== '') {
            //     statusTypeDatasource.read();
            // }

            // if (nisis.alerts.statusType !== ''){
                //statusValuesDatasource.read();
            // }

            if (nisis.alerts.facType !== '') {
                statusTypeDatasource.read().then(function() {
                    if (nisis.alerts.statusType !== ''){
                        statusValuesDatasource.read().then(function() {
                            if (nisis.alerts.statusType !== ''){
                                 if (e.action === 'add'){ 
                                    e.items[0].ALERTID = newRowId;
                                    e.items[0].OBJECTID = nisis.alerts.oid;
                                    e.items[0].FAC_ID = nisis.alerts.facId;
                                    e.items[0].FAC_TYPE = nisis.alerts.facType;
                                    e.items[0].STATUS_TYPE_ID = nisis.alerts.statusType;
                                    e.items[0].DISPLAY_NM = statusTypeDatasource.get(nisis.alerts.statusType).STATUS_FULLNAME;
                                    //NISIS-2795 KOFI HONU
                                    e.items[0].ALERT_DATE = "";//getCurrentUTCDateTime();
                                    colIdx = -1;
                                    rowIdx = -1;
                                    // NISIS-2531 KOFI HONU - Had to add this here to force row 0 items to display.
                                    alertsGrid.dataSource.sort({ field: "ALERTID", dir: "asc" });
                                } else if (e.action === 'sync') {
                                    alertsGrid.dataSource.read();
                                }
                            }
                        });
                    }
                    else {
                        console.error("statusType is null");
                    }
                });
            }                     
        }
    });

    var newRowId = 0;

    //Facility Type Datasource
    var facTypeDatasource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            parameterMap: function(options, operation) {
                if (operation === 'read') {
                    return { 
                        "action" : "actions", 
                        "function" : "getFacilityTypes", 
                    };
                }
            }
        },
        schema: {
            model: {
                id: "ID", 
                fields: {
                    ID: {editable: false},
                    TYPE: {editable: false}
                    }
            },
            data: "facilityTypes"
        }
    });
                                    
    // NISIS-2531 KOFI HONU - Had to make this an ajax call.
    //Status Type Datasource
    var statusTypeDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.ajax({
                    url: "alerts/api/",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "action": "actions",
                        "function": "getStatusTypesByFacType",
                        "facType": nisis.alerts.facType
                    },
                    success: function(result) {
                        //var resp = JSON.parse(result);
                        //console.log("resp",resp);

                        options.success(result);
                    },
                    error: function(result) {
                        console.error("Error: ", result);
                        options.error(result);
                    }
                });                
            }
            //,
            // parameterMap: function(options, operation) {
            //     if (operation === 'read') {
            //         return { 
            //             "action" : "actions", 
            //             "function" : "getStatusTypesByFacType",
            //             "facType": nisis.alerts.facType
            //         };
            //     }
            // }
        },
        schema: {
            model: {
                id: "ID", 
                fields: {
                    ID: {editable: false},
                    STATUS_COLUMN: {editable: false},
                    STATUS_FULLNAME: {editable: false},
                    FAC_TYPE: {editable: false}
                }
            },
            data: "statusTypes"
        },
    });

    // NISIS-2531 KOFI HONU - Had to make this an ajax call.
    //Status Values Datasource
    var statusValuesDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.ajax({
                    url: "alerts/api/",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "action" : "actions", 
                        "function" : "getStatusValues",
                        "statusId": nisis.alerts.statusType
                    },
                    success: function(result) {
                        //var resp = JSON.parse(result);
                        //console.log("resp",resp);

                        options.success(result);
                    },
                    error: function(result) {
                        console.error("Error: ", result);
                        options.error(result);
                    }
                });                
            }       

            // ,
            // parameterMap: function(options, operation) {
            //     if (operation === 'read') {
            //         return { 
            //             "action" : "actions", 
            //             "function" : "getStatusValues",
            //             "statusId": nisis.alerts.statusType
            //         };
            //     }
            // }
        },
        schema: {
            model: {
                id: "VALUE", 
                fields: {
                    VALUE: {editable: false},
                    LABEL: {editable: false}
                }
            },
            data: "statusValues"
        }
    });

    //groups Datasource
    var groupsDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            parameterMap: function(options, operation) {
                if (operation === 'read') {
                    return { 
                        "action" : "actions", 
                        "function" : "getGroups"
                    }
                }
            }
        },
        schema: {
            model: {
                id: "GROUPID", 
                fields: {
                    GROUPID: {editable: false},
                    NAME: {editable: false}
                }
            },
            data: "groups"
        },
    });

    groupsDataSource.read();
                
    //Editors
    function timeEditor(container, options) {
        $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
        .appendTo(container)
        .kendoTimePicker({});
    }
     
    function dateTimeEditor(container, options) {
        $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
        .appendTo(container)
        .kendoDateTimePicker({});
    }

    function yesNoDropDownEditor(container, options){
        $('<input id="yesNoDropDownEditor" data-bind="value:' + options.field + '" />')
        .appendTo(container)
        .kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: [{ Value: "Y", Text: "Yes" }, { Value: "N", Text: "No" }]
        });
    }

    function facTypeEditor(container, options){
        $('<input name="facTypeEditor" data-bind="value:' + options.field + '" required="required" validationMessage="Facility Type is required"/>')
        .appendTo(container)
        .kendoDropDownList({
            optionLabel: "Select Facility Type...",
            dataTextField: 'TYPE',
            dataValueField: 'ID',
            dataSource: facTypeDatasource,
            change: function(e){
                nisis.alerts.facType = this.value();
                if (nisis.alerts.facType !== ''){
                    getSelectedRow().STATUS_TYPE_ID = '';
                    var statusTypeCell = $('#alertTable tr:eq(' + (rowIdx + 1) + ') td:eq(' + (colIdx + 1) + ')');
                    // var statusTypeCell = $('td:eq(3)', rowClicked);
                    statusTypeDatasource.read();
                    statusTypeCell.text("Select Status Type...");
                    statusTypeCell.click();
                }
            }
        });

        $('<span class="k-invalid-msg" data-for="facTypeEditor"></span>').appendTo(container);
    }

    function statusTypeEditor(container, options){
        $('<input name="statusTypeEditor" data-bind="value:' + options.field + '" required="required" validationMessage="Status Type is required"/>')
        .appendTo(container)
        .kendoDropDownList({
            optionLabel: "Select Status...",
            cascadeFrom: 'facTypeEditor',
            cascadeFromField: 'FAC_TYPE',
            dataTextField: 'STATUS_FULLNAME',
            dataValueField: 'ID',
            dataSource: statusTypeDatasource,
            change: function(e){
                getSelectedRow().DISPLAY_NM = this.text();
                getSelectedRow().FUTURE_STATUS = '';
                nisis.alerts.statusType = this.value();
                statusValuesDatasource.read();
                var futureStatusCell = $('#alertTable tr:eq(' + (rowIdx + 1) + ') td:eq(' + (colIdx + 2) + ')');
                futureStatusCell.click();
            }
        });

        $('<span class="k-invalid-msg" data-for="statusTypeEditor"></span>').appendTo(container);
    }

    function futureStatusEditor(container, options){
        $('<input name="futureStatusEditor" data-bind="value:' + options.field + '" required="required" validationMessage="Future Status is required"/>')
        .appendTo(container)
        .kendoDropDownList({
            optionLabel: "Select Future Status...",
            dataTextField: 'LABEL',
            dataValueField: 'VALUE',
            dataSource: statusValuesDatasource,
            change: function(e){
                getSelectedRow().FUTURE_STATUS_LABEL = this.text();
            }
        });

        $('<span class="k-invalid-msg" data-for="futureStatusEditor"></span>').appendTo(container);
    }

    // NISIS-2531 KOFI HONU - This is probably decommissioned now that we don't get future status from here.
    function futureStatusTemplate(data) {
        if (data.FUTURE_STATUS === "") {
            return "Select Future Status...";
        } else {
            if (statusValuesDatasource.data().length > 0) {
                var row = statusValuesDatasource.get(data.FUTURE_STATUS);
                return row.LABEL;
            }else {
                return "";
            }
        } 
    }

    function groupsEditor(container, options) {
        $('<select name="groupsEditor" multiple="multiple" data-bind="value:GROUPS" required="required" validationMessage="At least one group is required"/>')
        .appendTo(container)
        .kendoMultiSelect({
            dataTextField: 'NAME',
            dataValueField: 'GROUPID',
            dataSource: groupsDataSource,
            change: function(e){
                var text = "";
                var length = this.dataItems().length;
                $.each(this.dataItems(), function(i, element){
                    if (i === length-1)
                        text = text + element.NAME;
                    else
                        text = text + element.NAME + ", ";
                }); 

                getSelectedRow().GROUPS = this.value();

                if (length === 0){
                    /**
                     * Due to a Kendo Bug it is needed to marked 
                     * this row as dirty in order to save this row 
                     * when the user removes the only group assigned 
                     */
                    options.model.dirty = true;
                    var groupsCellClicked = $('#alertTable tr:eq(' + (rowIdx + 1) + ') td:eq(' + (colIdx) + ')');
                    groupsCellClicked.addClass('k-dirty-cell');
                }
            }
        });

        $('<span class="k-invalid-msg" data-for="groupsEditor"></span>').appendTo(container);
    }

    function groupsTemplate(data) {
        var names = [], 
            ids = [];

        $.each($.trim(data.GROUPS).split(','), function (idx, elem) {
            
            var id = parseInt(elem);

            if ( !isNaN(id) ) {

                var temp = groupsDataSource.get(id);
                
                if ( temp ) {
                    ids.push(id);
                    names.push(temp.NAME);
                }
            }
        });

        if ( ids.length !== 0 ) {
            //Set the value only if ids is not an empty array
            data.GROUPS = ids;
        }

        return names.join(", ");
    }


    $("#alertTable").kendoGrid({
        autoBind: false,
        dataSource: alertsDataSource,
        columns: [
            {field: "ALERTID", title:"ID", hidden: true },
            {field: "OBJECTID", title:"Object ID", hidden: true },
            {field: "FAC_ID", title:"FacID", width: 60},
            {field: "FAC_TYPE", title:"Type", width: 60, editor: facTypeEditor},
            {field: "STATUS_TYPE_ID", title:"Status Field",  width: 140, editor: statusTypeEditor, template: "#=DISPLAY_NM#"},
            {field: "CURRENT_STATUS_LABEL", title:"Current Status", hidden: true },
            //NISIS-2531 KOFI HONU
           // {field: "FUTURE_STATUS", title:"Future Status", width: 150, editor: futureStatusEditor, template: futureStatusTemplate},
            {field: "FUTURE_STATUS", title:"Future Status", width: 150, editor: futureStatusEditor, template: "#=FUTURE_STATUS_LABEL#"},
            {field: "ALERT_DATE", title:"Alert Date/Time(Z)", width: 200, editor: dateTimeEditor, format: "{0:yyyy/MM/dd HH:mm:ss}"},
            {field: "GROUPS", title:"Groups", width: 100, template: groupsTemplate, editor: groupsEditor },
            {field: "ACTIVE", title:"Active", width: 80, editor: yesNoDropDownEditor},
            {field: "CREATED_BY", width: 80, title:"Created By"},
            {field: "CREATED_DATE", width: 80, title:"Created Date(Z)"},
            {field: "INACTIVATED_BY", title:"Inactivated By"},
            {field: "INACTIVATED_DATE", title:"Inactivated Date(Z)"}
        ],
        selectable:true,
        //scrollable:true,
        editable: true,
        sortable: true,
        resizable: true,
        reorderable: true,
        filterable: true,
        columnMenu: true,
        pageable: {
            refresh: true,
            pageSizes: [5, 10, 15, 20],
            buttonCount: 10
        },
        toolbar: ["create", "save", "cancel"]                
    });

    var alertsGrid = $("#alertTable").data("kendoGrid");
    var rowIdx = -1;
    var colIdx = -1;
    var selectedRow = -1;

    //This is the read-only grid
    $("#readOnlyGrid").kendoGrid({
        autoBind: false,
        dataSource: alertsDataSource,
        columns: [
            {field: "ALERTID", title:"ID", hidden: true },
            {field: "OBJECTID", title:"Object ID", hidden: true },
            {field: "FAC_ID", title:"FacID", width: 60 },
            {field: "FAC_TYPE", title:"Type", width: 60, editor: facTypeEditor},
            {field: "STATUS_TYPE_ID", title:"Status Field", width: 140, editor: statusTypeEditor, template: "#=DISPLAY_NM#"},
            {field: "CURRENT_STATUS_LABEL", title:"Current Status", hidden: true },
            {field: "FUTURE_STATUS_LABEL", title:"Future Status", width: 140},
            {field: "ALERT_DATE", title:"Alert Date/Time(Z)", width: 140, editor: dateTimeEditor, format: "{0:yyyy/MM/dd HH:mm:ss}"},
            {field: "GROUPS", title:"Groups", width: 200, template: groupsTemplate, editor: groupsEditor },
            {field: "ACTIVE", title:"Active", width: 80, editor: yesNoDropDownEditor},
            {field: "CREATED_BY", title:"Created By"},
            {field: "CREATED_DATE", title:"Created Date(Z)"},
            {field: "INACTIVATED_BY", title:"Inactivated By"},
            {field: "INACTIVATED_DATE", title:"Inactivated Date(Z)"}
        ],
        selectable:true,
        editable: false,
        sortable: true,
        resizable: true,
        reorderable: true,
        filterable: true,
        columnMenu: true,
        pageable: {
            refresh: true,
            pageSizes: [5, 10, 15, 20],
            buttonCount: 10
        },
    });

    function getSelectedRow(){
        return alertsGrid.dataItem(alertsGrid.select());
    }
    
    $(alertsGrid.tbody).on("click", "td", function (e) {
            var rowClicked = $(this).closest("tr");
            //rowIdx = $("tr", alertsGrid.tbody).index(rowClicked);
            rowIdx = rowClicked.index();
            colIdx = $("td", rowClicked).index(this);
            // alert(rowIdx + '-' + colIdx);

    });

    //Reminders Datasource
    var remindersDatasource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            parameterMap: function(options, operation) {
                if (operation === 'read') {
                    // var currentTime = new Date();
                    // var nextTime = new Date();
                    // var nextTime = new Date(nextTime.setSeconds(nextTime.getSeconds() + 5));
                    // var currentUTCTime = getUTCDateTimeFrom(currentTime);
                    // var nextUTCTime = getUTCDateTimeFrom(nextTime);
                    // console.log(currentUTCTime, nextUTCTime);
                    return { 
                        "action" : "actions", 
                        "function" : "getRemindersPerUserPerInterval",
                        //"currentTime" : currentUTCTime,
                        //"nextTime" : nextUTCTime
                    };
                }
            }
        },
        schema: {
            model: {
                id: "ALERTID", 
                fields: {
                    ALERTID: {editable: false},
                    OBJECTID: {editable: false},
                    FAC_ID: {editable: false},
                    DISPLAY_NM: {editable: false},
                    FUTURE_STATUS: {editable: false},
                    FUTURE_STATUS_LABEL: {editable: false},
                    ALERT_DATE: {editable: false}
                }
            },
            data: "reminders"
        },
    });

    // function getMissedAlerts(missedAlertsData){
    //     // if(nisis.ui.mapview.map.loaded){
    //     $.each(missedAlertsData, function(i, element){
    //         if ($.inArray(element.ALERTID, nisis.alerts.displayedAlerts) === -1) {
    //             popupAlert.element.prop("alertId", element.ALERTID);
    //             popupAlert.error("Missed reminder: The " + element.DISPLAY_NM + " has not been changed to " + element.FUTURE_STATUS_LABEL + " for " + element.FAC_ID + " on " + kendo.toString(element.ALERT_DATE, "yyyy/MM/dd HH:mm:ss"));
    //         }
    //     });
    // }

    //Missed Alerts Datasource
    // var missedAlertsDatasource = new kendo.data.DataSource({
    //     transport: {
    //         read: {
    //             url: "alerts/api/",
    //             type: "POST",
    //             dataType: "json",
    //         },
    //         parameterMap: function(options, operation) {
    //             if (operation === 'read') {
    //                 // var startDate = new Date('2014/06/01 00:00:00');
    //                 // var endDate = new Date();
    //                 // var startUTCDate = getUTCDateTimeFrom(startDate);
    //                 // var endUTCTime = getUTCDateTimeFrom(endDate);
    //                 return { 
    //                     "action" : "actions", 
    //                     "function" : "getMissedAlerts",
    //                     // "startDate" : startUTCDate,
    //                     // "endDate" : endUTCTime
    //                 };
    //             }
    //         }
    //     },
    //     schema: {
    //         model: {
    //             id: "ALERTID", 
    //             fields: {
    //                 ALERTID: {editable: false},
    //                 OBJECTID: {editable: false},
    //                 FAC_ID: {editable: false},
    //                 DISPLAY_NM: {editable: false},
    //                 FUTURE_STATUS: {editable: false},
    //                 FUTURE_STATUS_LABEL: {editable: false},
    //                 ALERT_DATE: {editable: false}
    //             }
    //         },
    //         data: "missedAlerts",
    //         total: function (response) {
    //             return response.missedAlerts.length;
    //         },
    //     },
    //     requestEnd: function(e) {
    //         getMissedAlerts(e.response.missedAlerts);
    //     }
    // });

    //Reading all the reminders missed by the user
    //missedAlertsDatasource.read();

    var popupAlert = $("#popupAlertDiv").kendoNotification({
        autoHideAfter: 0,
        button: true,
        // width: 300, 
        show: function(e){
            //Getting ID from the widget (popupAlert)
            var id = this.element.prop('alertId');
            //Setting the property alertId to the e.element which is being showed
            e.element.prop('alertId', id);
            nisis.alerts.displayedAlerts.push(id);        
        },
        hide: function(e){
            //Getting ID from the e.element which is being hid, not from the popup alert widget
            var id = e.element.prop('alertId');        
            $.ajax({
                url: "alerts/api/",
                type: "POST",
                data: {
                    "action": "actions",
                    "function": "inactivateAlert",
                    "id": id
                },
                success: function(data, status, req) {
                    var resp = JSON.parse(data);
                    if (resp['result'] === 'Success') {
                        console.log("The alert was inactivated successfully");
                        var index =  nisis.alerts.displayedAlerts.indexOf(id);
                        if (index > -1) {
                            nisis.alerts.displayedAlerts.splice(index, 1);
                        }
                    } else {
                        console.log("The result in response is:" + resp['result']);
                    }
                    alertsGrid.dataSource.read();
                },
                error: function(req, status, err) {
                    console.log("ERROR in ajax call to inactivate the alerts. Status: " + status);
                },
            });
        }
    }).data("kendoNotification");

    //Notifications Datasource
    var notificationsDatasource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "alerts/api/",
                type: "POST",
                dataType: "json",
            },
            parameterMap: function(options, operation) {
                if (operation === 'read') {
                    // var currentTime = new Date();
                    // var nextTime = new Date();
                    // var nextTime = new Date(nextTime.setSeconds(nextTime.getSeconds() + 5));
                    // var currentUTCTime = getUTCDateTimeFrom(currentTime);
                    // var nextUTCTime = getUTCDateTimeFrom(nextTime);
                    // console.log(currentUTCTime, nextUTCTime);
                    return { 
                        "action" : "actions", 
                        "function" : "getNotifications",
                        // "currentTime" : currentUTCTime,
                        // "nextTime" : nextUTCTime
                    };
                }
            }
        },
        schema: {
            model: {
                id: "NOTID", 
                fields: {
                    NOTID: {editable: false},
                    FAC_TYPE: {editable: false},
                    FAC_ID: {editable: false},
                    COLUMN_NM: {editable: false},
                    DISPLAY_NM: {editable: false},
                    OLD_VALUE: {editable: false},
                    OLD_LABEL: {editable: false},
                    NEW_VALUE: {editable: false},
                    NEW_LABEL: {editable: false}
                }
            },
            data: "notifications"
        },
    });

    var popupNotification = $("#popupNotificationDiv").kendoNotification({
        autoHideAfter: 5000,
        // button: true,
        // width: 300, 
        show: function(e){
            var id = this.element.prop('notId');
            e.element.prop('notId', id);
            nisis.displayedNotifications.push(id);
        }
    }).data("kendoNotification");

    //Reading reminders every interval of time
    setInterval(function() {
        remindersDatasource.read().then(function(){
            var remindersData = remindersDatasource.data().toJSON();
            $.each(remindersData, function(i, element){
                if ($.inArray(element.ALERTID, nisis.alerts.displayedAlerts) === -1){
                    popupAlert.element.prop("alertId", element.ALERTID);
                    popupAlert.error("The " + element.DISPLAY_NM + " has not been changed to " + element.FUTURE_STATUS_LABEL + " for " + element.FAC_ID + " on " + kendo.toString(element.ALERT_DATE, "yyyy/MM/dd HH:mm:ss"));
                }
            });
        });                
        
        notificationsDatasource.read().then(function(){
            var notificationsData = notificationsDatasource.data().toJSON();
            $.each(notificationsData, function(i, element){
                if ($.inArray(element.NOTID, nisis.displayedNotifications) === -1){
                    popupNotification.element.prop("notId", element.NOTID);
                    popupNotification.info('The '+ element.DISPLAY_NM +' for '+ element.FAC_ID +' was modified from '+ element.OLD_LABEL +' to '+ element.NEW_LABEL);
                }
            });
        });


    }, 5000); //Executes every 5 seconds

    function getCurrentUTCDateTime(){
        var d = new Date();
        var now_utc = new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(),  d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds());
        return kendo.toString(now_utc, "yyyy/MM/dd HH:mm:ss");
    }

    function getUTCDateTimeFrom(date){
        var now_utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        return kendo.toString(now_utc, "yyyy/MM/dd HH:mm:ss");
    }
});