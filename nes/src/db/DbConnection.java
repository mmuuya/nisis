/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author irena.bucci
 */

public class DbConnection {
    private static Connection conn;

    public static Connection getConnection(Properties props) {
        try {
            if (conn == null || conn.isClosed()) {                
                 
                DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
                              
                //get the property value and print it out
                String url = props.getProperty("nisis_database");
                String dbuser = props.getProperty("nisis_user");
                String dbpassword = props.getProperty("nisis_password");
        
                //creating connection to Oracle database using JDBC
                conn = DriverManager.getConnection(url, dbuser, dbpassword);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());   
            ex.printStackTrace();
        } 
        
        return conn;
    }
}
