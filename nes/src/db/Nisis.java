/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import notam.NISISNotam;
import org.apache.log4j.LogManager;

/**
 *
 * @author irena.bucci
 */
public class Nisis {
    public static String getNOTAMQueryFormat ="select text from notam_text where notam_id=%s";
    public static String getUSNSNotamIDAndArtccFormat ="select usns_notam_id, artcc_id from notam_body where notam_id=%s";
    public static String storeNOTAMStatement ="update notam_body set work_number_id=? , usns_ref_number=?, usns_status=?, status=? where notam_id=?";
    //irena : also need to save USNS status and if status == Submitted to USNS - Active NOTAM - need to save NOTAM ID as well.
    public static String updateNOTAMStatusStatement ="update notam_body set usns_status=? where work_number_id=?";
    public static String updateNOTAMStatusAndUSNSNotamIDStatement ="update notam_body set usns_status=?, usns_notam_id=? where work_number_id=?";
    public static String selectWorkNumberIdsQuery ="select work_number_id from notam_body where work_number_id IS NOT NULL";    
    public static String generateNextRefNumberFormat ="select tfruser.seq_usns_ref_number.nextval as new_ref_number from dual"; 
    public static Integer SUBMITTED_SUCCESSFULLY_STATUS_CODE=2;
    
    private static final org.apache.log4j.Logger logger = LogManager.getLogger(Nisis.class);
    
    public static NISISNotam getNotamTextAndNewRefNumber(String notamId, Properties props) throws SQLException {
        NISISNotam notam = null;
        PreparedStatement preStatement = null;
        try {
            //create sql
            String sql = String.format(getNOTAMQueryFormat, notamId);

            //execute query
            preStatement = DbConnection.getConnection(props).prepareStatement(sql);   
            ResultSet result = preStatement.executeQuery();

            while(result.next()){
                notam = new NISISNotam();
                notam.setNotamId(notamId);                
                String nisisNotamText=result.getString("text");
                if (nisisNotamText!=null) {
                    notam.setNotamText(nisisNotamText);
                }                           
            }   
            
            //generate ref number to send to USNS
            String refNumber = getNewRefNumber(props);
            if (notam!=null && refNumber!=null) {
                notam.setRefNumber(refNumber);
            }   
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (preStatement != null) {
                preStatement.close();
            }
 
            if (DbConnection.getConnection(props) != null) {
                DbConnection.getConnection(props).close();
            } 
	}
        return notam;
    }    
    
    public static NISISNotam getNotamArtccAndNewRefNumber(String notamId, Properties props) throws SQLException {
        NISISNotam notam = null;
        PreparedStatement preStatement = null;
        try {
            //create sql
            String sql = String.format(getUSNSNotamIDAndArtccFormat, notamId);

            //execute query
            preStatement = DbConnection.getConnection(props).prepareStatement(sql);   
            ResultSet result = preStatement.executeQuery();

            while(result.next()){
                notam = new NISISNotam();                
                notam.setNotamId(notamId);
                String usnsNotamId=result.getString("usns_notam_id");
                if (usnsNotamId!=null) {
                    notam.setUsnsNotamId(usnsNotamId);
                }
                String artcc=result.getString("artcc_id");
                if (artcc!=null) {
                    notam.setArtccId(artcc);
                }                                            
            }               
            
            //generate ref number to send to USNS
            String refNumber = getNewRefNumber(props);
            if (notam!=null && refNumber!=null) {
                notam.setRefNumber(refNumber);
            }   
                                
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (preStatement != null) {
                preStatement.close();
            }
 
            if (DbConnection.getConnection(props) != null) {
                DbConnection.getConnection(props).close();
            } 
	}
        return notam;
    }   
    
    public static String getNewRefNumber(Properties props) {
        String newRefNumber = null;
        PreparedStatement preStatement = null;        
        try {
            //create sql
            String  sql = String.format(generateNextRefNumberFormat);

            //execute query
            preStatement = DbConnection.getConnection(props).prepareStatement(sql);   
            ResultSet result = preStatement.executeQuery();

            while(result.next()){               
                String refNumber=result.getString("new_ref_number");
                if (refNumber!=null) {
                    newRefNumber = refNumber;
                }                                     
            }  
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());           
        }       
        return newRefNumber;
    }
     
    public static void storeUSNSResponseToDraftRequest(String notamId, String notamWorkNumberId, Integer notamRefNumber, String notamUSNSStatus, Properties props) throws SQLException {    
        PreparedStatement preStatement = null;
        try {
            //create sql update statement                       
            logger.info("Storing NOTAM work number id on successful submission, notamWorkNumberId =" + notamWorkNumberId + " and notamRefNumber: " + notamRefNumber +  " and notamId: " + notamId +  " and notamUSNSStatus: " + notamUSNSStatus);
            //execute update statement            
            preStatement = DbConnection.getConnection(props).prepareStatement(storeNOTAMStatement);   
            preStatement.setString(1, notamWorkNumberId);
            preStatement.setInt(2, notamRefNumber);
            preStatement.setString(3, notamUSNSStatus);
            preStatement.setInt(4, SUBMITTED_SUCCESSFULLY_STATUS_CODE);
            preStatement.setInt(5, Integer.parseInt(notamId));            
            preStatement.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());           
        }
        finally {
            if (preStatement != null) {
                preStatement.close();
            }
 
            if (DbConnection.getConnection(props) != null) {
                DbConnection.getConnection(props).close();
            } 
	}
    }
    
    public static void updateUSNSStatus(String notamWorkNumberId, String notamUSNSStatus, String USNSNotamID, Properties props) throws SQLException {    
        PreparedStatement preStatement = null;
        try {
            //create sql update statement                       
            logger.info("Updating NOTAM status notamWorkNumberId =" + notamWorkNumberId + " with notamUSNSStatus: " + notamUSNSStatus + " and USNSNotamID: " + USNSNotamID);
            if (USNSNotamID == null) {
                //execute update statement            
                preStatement = DbConnection.getConnection(props).prepareStatement(updateNOTAMStatusStatement);
                preStatement.setString(1, notamUSNSStatus);
                preStatement.setString(2, notamWorkNumberId);        
            }
            else {
                //execute update statement            
                preStatement = DbConnection.getConnection(props).prepareStatement(updateNOTAMStatusAndUSNSNotamIDStatement);
                preStatement.setString(1, notamUSNSStatus);
                preStatement.setString(2, USNSNotamID);
                preStatement.setString(3, notamWorkNumberId);   
            }
            preStatement.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());           
        }
        finally {
            if (preStatement != null) {
                preStatement.close();
            }
 
            if (DbConnection.getConnection(props) != null) {
                DbConnection.getConnection(props).close();
            } 
	}
    }
    
    public static ArrayList<String> getAllSubmittedNOTAMs(Properties props) throws SQLException {        
        ArrayList<String> ids = new ArrayList<String>();
        PreparedStatement preStatement = null;
        try {
            //create sql
            String sql = String.format(selectWorkNumberIdsQuery);

            //execute query
            preStatement = DbConnection.getConnection(props).prepareStatement(sql);   
            ResultSet result = preStatement.executeQuery();

            while(result.next()){
                String workNumberId = result.getString(1);                
                ids.add(workNumberId);
            }   
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (preStatement != null) {
                preStatement.close();
            }
 
            if (DbConnection.getConnection(props) != null) {
                DbConnection.getConnection(props).close();
            } 
	}
        return ids;
    }    
}
