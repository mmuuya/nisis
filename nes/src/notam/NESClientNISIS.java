/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import db.Nisis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
// Import log4j classes.
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;



/**
 *
 * @author irena.bucci
 */
public class NESClientNISIS {
    public static String NES_ERROR="Unable to get response from NES.";
    public static String NES_FAIL="Failure to submit to NES: ";
    public static String NES_SUCCESS="success";
    public static String NES_SUCCESS_MESSAGE="NOTAM has been submitted successfully";
    public static String NES_CANCEL_SUCCESS_MESSAGE="NOTAM cancellation request has been submitted successfully";
    public static String NES_QUERIED_STATUS_MESSAGE="Received NOTAM status from NES successfully for work_number_id: ";
    public static String NES_UNABLE_TO_FETCH_NOTAM_ID="Unable to fetch a notam from the database for notamId=";
    public static String NES_UNABLE_TO_FIND_PROPS_FILE="Unable to find properties file";
    public static String NES_INITIAL_SUBMISSION_STATUS="Pending edit/review by USNOF";
    public static String NES_INVALID_ARGUMENTS="Invalid arguments passed in to nes.jar";
    
    private static final Logger logger = LogManager.getLogger(NESClientNISIS.class);
    public static void main(String[] args)  {           
        
        Properties props = new Properties();
        //load a properties file that is located at the same place where the jar is 
        try {
            File jarPath=new File(NESClientNISIS.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            String propertiesPath=jarPath.getParentFile().getAbsolutePath();        
            String fileSeparator=System.getProperty("file.separator");        
            props.load(new FileInputStream(propertiesPath+fileSeparator+"nes.properties"));
        } catch (Exception e) {
            System.out.println(NES_UNABLE_TO_FIND_PROPS_FILE+e);
        }
       
        //do not bundle the nes.properties with the jar anymore
        //props.load(NESClientNISIS.class.getClassLoader().getResourceAsStream("nes.properties"));

        //get the properties
        String branch = props.getProperty("branch");
        String user_initials = props.getProperty("user_initials");
        String password = props.getProperty("password");
        String author = props.getProperty("author");
        String approver = props.getProperty("approver");
        String cc = props.getProperty("cc");
        String author_comments = props.getProperty("author_comments");
        URL urlNESService = null;
        try {
            urlNESService=new URL(props.getProperty("nes_service")); 
        } catch (Exception e) {
            logger.error(NES_UNABLE_TO_FIND_PROPS_FILE+e.getMessage());
        }
        
        //setup logger        
        PropertyConfigurator.configure(props);
        
        //set up truststore and keystore for NES connection
        configureNESConnection(props);
                      
        String comments = "AUTHOR: " + author + "\n"+
                          "APPROVER: " + approver + "\n"+
                          "CC: " + cc + "\n"+ 
                          "AUTHOR COMMENTS: " + author_comments + "\n";
        NISISNotam nisisNotam = null;
        NESDraftRequest draft = null;       
        try {   
            if (args.length == 0) {
                //this is a status request                
                NESStatusRequest status = new NESStatusRequest();
                status.setBranch(branch);
                status.setUser_initials(user_initials);
                status.setPassword(password);          
                
                //get array of valid work_numbers from the database
                ArrayList<String> ids = Nisis.getAllSubmittedNOTAMs(props);
                for (String workNumberId: ids) {
                    status.setWork_number(workNumberId);  
                    //submit status request
                    submitStatusRequest(urlNESService, status, props);                      
                }                
            }
            else if (args.length == 2) {                
                //see if this is a draft to submit or a cancel to already submitted TFR
                String notamId = args[0];
                String action = args[1];                
                //this is submit draft or cancel notam request
                if (action == null)  {
                    logger.error("action is null");
                    logger.error(NES_INVALID_ARGUMENTS);
                }
                if (action.equalsIgnoreCase("DRAFT") ) {                    
                    nisisNotam = Nisis.getNotamTextAndNewRefNumber(args[0], props);
                    if (nisisNotam==null) {
                        logger.error(NES_UNABLE_TO_FETCH_NOTAM_ID+args[0]);
                        return;
                    }
                    draft = new NESDraftRequest();
                    draft.setBranch(branch);
                    draft.setUser_initials(user_initials);
                    draft.setPassword(password);
                    draft.setRef_number(nisisNotam.getRefNumber());  

                    nisisNotam.formatNotamText();
                    draft.setText(nisisNotam.getNotamText());
                    draft.setComments(comments);

                    //submit notam draft
                    submitNotamDraft(urlNESService, draft, action, notamId, props);  
                }
                else if (action.equalsIgnoreCase("CANCEL")) {
                    String notamIdToCancel = args[0];
                    nisisNotam = Nisis.getNotamArtccAndNewRefNumber(args[0], props);
                    if (nisisNotam==null) {
                        logger.error(NES_UNABLE_TO_FETCH_NOTAM_ID+args[0]);
                        return;
                    }
                    String usnsNotamID = nisisNotam.getUsnsNotamId();  //5/3243                
                    String refNumber = nisisNotam.getRefNumber();//Unique ref_number off the sequence: select tfruser.seq_usns_ref_number.nextval as new_ref_number from dual
                    String artccID = nisisNotam.getArtccId(); //ZAB   
                    
                    draft = new NESDraftRequest();
                    draft.setBranch(branch);
                    draft.setUser_initials(user_initials);
                    draft.setPassword(password);
                    draft.setComments(comments);
                    draft.setRef_number(refNumber);  

                    String text = "!FDC Y/NNNN FDC CANCEL " + usnsNotamID +" "+ artccID;
                    draft.setText(text);                    

                    //submit notam draft ( a cancel request )
                    submitNotamDraft(urlNESService, draft, action, notamIdToCancel, props);                
                }            
            }
            else {
                logger.error(NES_INVALID_ARGUMENTS);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }           
    }
    
    public static void submitNotamDraft(URL url, NESDraftRequest draft, String action, String notamId, Properties props)  {
        HttpsURLConnection conn = null;
        try {
            conn = (HttpsURLConnection) url.openConnection();

            HostnameVerifier hv = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            conn.setHostnameVerifier(hv);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "plain/text");            
            conn.setRequestMethod("POST");
            conn.connect();

            /* send request */
            PrintWriter out = new PrintWriter(new BufferedWriter(
                 new OutputStreamWriter(conn.getOutputStream())));

            /* get data */
            JAXBContext context = JAXBContext.newInstance(NESDraftRequest.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                   
            m.marshal(draft, out);
                      
            logger.info("Sending NOTAM request:"+draft.toString());
             
            out.flush();
            out.close();

            /* read response */
            BufferedReader in = new BufferedReader(new InputStreamReader(
                   conn.getInputStream()));
           
            //create unmarshaller
            JAXBContext contextResponse = JAXBContext.newInstance(NESDraftResponse.class);
            Unmarshaller unmarshaller = contextResponse.createUnmarshaller();
            NESDraftResponse response = (NESDraftResponse) unmarshaller.unmarshal(in);
            
            logger.info("Got NOTAM response:"+response.toString());
            //when issuing a cancellation NOTAM, we're simply creating a new NOTAM which is responsible for cancelling the original one
            if (response!=null) {
                String status = response.getStatus();
                if (status!=null && status.equalsIgnoreCase(NES_SUCCESS)) {  
                    response.setUsns_status(NES_INITIAL_SUBMISSION_STATUS);     
                    if (action.equalsIgnoreCase("CANCEL")) {
                        logger.error(NES_CANCEL_SUCCESS_MESSAGE);
                        //update the work_number_id for this notam. We are cancelling this notam, so in order to get the status, we need to send up new work_number
                        //suppose we wanted to cancel Notam Id 100 with USNS notam id of 5/9393. Then we generate new ref_number (as new seq from dual ) and 
                        //send up  <ref_number>239</ref_number>  <text>!FDC Y/NNNN FDC CANCEL 5/9133 ZAN</text>
                        //we get back new work number: <work_number>15-006342</work_number>. Associate it with the original notam id for which we send up a new CANCEL request
                        //so that when we ask for status, it will be "Pending edit/review by USNOF". That way they wont try to cancel it twice (the status will be we are working on the cancelling request for this NOTAM                        
                    }
                    else { //DRAFT
                        logger.error(NES_SUCCESS_MESSAGE);
                        //save the work_number_id. ref_number and status that we got from USNS into the notam_body table                                               
                    }                    
                    Nisis.storeUSNSResponseToDraftRequest(notamId, response.getWork_number(), Integer.parseInt(response.getRef_number()), response.getUsns_status(), props);  
                }
                else {                 
                    logger.error(NES_FAIL+response.getError());
                }
            }
            else {                
                logger.error(NES_ERROR);
            }
            /* cleanup before exit */
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            logger.error(NES_ERROR);
            logger.error( "NES Submit failed! ", e );
        }
    }
    
    public static void submitStatusRequest(URL url, NESStatusRequest status, Properties props)  {
        HttpsURLConnection conn = null;        
        try {
            conn = (HttpsURLConnection) url.openConnection();

            HostnameVerifier hv = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            conn.setHostnameVerifier(hv);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "plain/text");            
            conn.setRequestMethod("POST");
            conn.connect();

            /* send request */
            PrintWriter out = new PrintWriter(new BufferedWriter(
                 new OutputStreamWriter(conn.getOutputStream())));

            /* get data */
            JAXBContext context = JAXBContext.newInstance(NESStatusRequest.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                   
            m.marshal(status, out);
                      
            logger.info("Getting status for :"+status.toString());
             
            out.flush();
            out.close();

            /* read response */
            BufferedReader in = new BufferedReader(new InputStreamReader(
                   conn.getInputStream()));
           
            //create unmarshaller
            JAXBContext contextResponse = JAXBContext.newInstance(NESStatusResponse.class);
            Unmarshaller unmarshaller = contextResponse.createUnmarshaller();
            NESStatusResponse response = (NESStatusResponse) unmarshaller.unmarshal(in);
            
            logger.info("Got NES Status response:"+response.toString());
            //no errors?
            if (response.getError() == null) {                    
                //logger.error(NES_QUERIED_STATUS_MESSAGE+response.getWork_number() + ", response: " + response.getStatus());                    
                
                //save the status into the notam_body table                    
                Nisis.updateUSNSStatus(response.getWork_number(), response.getStatus(), response.getNotamid(), props);                  
            }
            else {                 
                logger.error(NES_FAIL+response.getError());
            }
            
            /* cleanup before exit */
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            logger.error(NES_ERROR);
            logger.error( "NES Submit failed! ", e );
        }       
    }
    
  
    public static void configureNESConnection(Properties props) {
        logger.info("Setting up NES configuration");
        logger.info("Setting up NES configuration: nes_local_truststore: "+props.getProperty("nes_local_truststore"));
        logger.info("Setting up NES configuration:nes_local_keystore: "+props.getProperty("nes_local_keystore"));
        logger.info("Setting up NES configuration: proxy_host: "+props.getProperty("proxy_host"));
        logger.info("Setting up NES configuration:proxy_port_number: "+props.getProperty("proxy_port_number"));
               
        System.setProperty("javax.net.ssl.trustStore",props.getProperty("nes_local_truststore"));
        System.setProperty("javax.net.ssl.trustStorePassword", props.getProperty("nes_local_truststore_password"));

        System.setProperty("javax.net.ssl.keyStore", props.getProperty("nes_local_keystore"));
        System.setProperty("javax.net.ssl.keyStorePassword", props.getProperty("nes_local_keystore_password"));
        System.setProperty("javax.net.ssl.keyStoreType",  props.getProperty("nes_local_keystore_type"));
        
        if (props.getProperty("proxy_host")!=null & props.getProperty("proxy_port_number")!=null) {
            
           System.setProperty("https.useProxy", "true");
           System.setProperty("https.proxyHost", props.getProperty("proxy_host"));
           System.setProperty("https.proxyPort", props.getProperty("proxy_port_number"));
        }
    }
}
