/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author irena.bucci
 */

@XmlRootElement (name = "nes-draft-request")
public class NESDraftRequest {
    private String branch;
    private String user_initials;
    private String password;    
    private String ref_number; //REF_NUMBER - unique request reference to send to USNS
    private String text; //NOTAM_TEXT
    private String comments; //required

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUser_initials() {
        return user_initials;
    }

    public void setUser_initials(String user_initials) {
        this.user_initials = user_initials;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRef_number() {
        return ref_number;
    }

    public void setRef_number(String ref_number) {
        this.ref_number = ref_number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "NESDraftRequest{" + "branch=" + branch + ", user_initials=" + user_initials + ", password=" + password + ", ref_number=" + ref_number + ", text=" + text + ", comments=" + comments + '}';
    }

    

 
}
