/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

/**
 *
 * @author irena.bucci
 */
public class NISISNotam {
    public String refNumber;
    public String notamId;
    public String notamText;
    public String usnsNotamId;
    public String artccId;   

    public String getNotamId() {
        return notamId;
    }

    public void setNotamId(String notamId) {
        this.notamId = notamId;
    }

    public String getNotamText() {
        return notamText;
    }

    public void setNotamText(String notamText) {
        this.notamText = notamText;
    }

    public String getUsnsNotamId() {
        return usnsNotamId;
    }

    public void setUsnsNotamId(String usnsNotamId) {
        this.usnsNotamId = usnsNotamId;
    }

    public String getArtccId() {
        return artccId;
    }

    public void setArtccId(String artccId) {
        this.artccId = artccId;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }
    
    
    //get rid of whitespaces inside the NOTAM text
    public void formatNotamText() {
        //get rid of <br></br>        
        if (notamText!=null) {        
            notamText = notamText.replace("<BR/><BR/>", "\n"); 
            notamText = notamText.replace("<br></br>", "\n"); 
        }
    }

    @Override
    public String toString() {
        return "NISISNotam{" + "refNumber=" + refNumber + ", notamId=" + notamId + ", notamText=" + notamText + ", usnsNotamId=" + usnsNotamId + ", artccId=" + artccId + '}';
    }

    
    
}
