/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author irena.bucci
 */

@XmlRootElement (name = "nes-draft-response")
public class NESDraftResponse {
    private String branch;
    private String user_initials;
    private String password;
    private String ref_number; //NOTAM_ID
    private String text; //NOTAM_TEXT
    private String comments; //
    private String error; //required
    private String status; //required (success or failure)
    private String work_number; //assigned by NES
    private String usns_status; // “Initial draft", "Pending edit/review by USNOF", "Pending entry into USNS", "Deleted by approver", ...
        
    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUser_initials() {
        return user_initials;
    }

    public void setUser_initials(String user_initials) {
        this.user_initials = user_initials;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRef_number() {
        return ref_number;
    }

    public void setRef_number(String ref_number) {
        this.ref_number = ref_number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWork_number() {
        return work_number;
    }

    public void setWork_number(String work_number) {
        this.work_number = work_number;
    }

    public String getUsns_status() {
        return usns_status;
    }

    public void setUsns_status(String usns_status) {
        this.usns_status = usns_status;
    }

    @Override
    public String toString() {
        return "NESDraftResponse{" + "branch=" + branch + ", user_initials=" + user_initials + ", password=" + password + ", ref_number=" + ref_number + ", text=" + text + ", comments=" + comments + ", error=" + error + ", status=" + status + ", work_number=" + work_number + ", usns_status=" + usns_status + '}';
    }
    
    
}
