/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author irena.bucci
 */

@XmlRootElement (name = "nes-status-response")
public class NESStatusResponse {
    /* current-time */
    private String ref_number; //NOTAM_ID
    private String work_number; //assigned by NES
    private String text; //NOTAM_TEXT
    private String comments; //
    private String error; //required
    private String status;  // “Initial draft", "Pending edit/review by USNOF", "Pending entry into USNS", "Submitted to USNS - Active NOTAM", "Deleted by approver", ...
                            //    "Error" - then see <error>...</error>
    private String notamid; //assigned by USNS when the status is Submitted to USNS - Active NOTAM; 

    public String getRef_number() {
        return ref_number;
    }

    public void setRef_number(String ref_number) {
        this.ref_number = ref_number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWork_number() {
        return work_number;
    }

    public void setWork_number(String work_number) {
        this.work_number = work_number;
    }

    public String getNotamid() {
        return notamid;
    }

    public void setNotamid(String notamid) {
        this.notamid = notamid;
    }

    @Override
    public String toString() {
        return "NESStatusResponse{" + "ref_number=" + ref_number + ", work_number=" + work_number + ", text=" + text + ", comments=" + comments + ", error=" + error + ", status=" + status + ", notamid=" + notamid + '}';
    }

     
}
