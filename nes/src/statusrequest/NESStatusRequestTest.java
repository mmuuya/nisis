/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statusrequest;

import deleterequest.*;
import register.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Properties;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
/**
 *
 * @author irena.bucci
 */
public class NESStatusRequestTest {
    public static void main(String[] args) throws Exception {
        
        Properties props = new Properties();
        //load a properties file from class path, inside static method
        props.load(NESStatusRequestTest.class.getClassLoader().getResourceAsStream("main/resources/nes.properties"));

        //get the properties
        String nes_delete_request_file =  "c:/projects/nisis/nes/src/main/resources/nes_status_request.xml"; //props.getProperty("nes_delete_request_file");
                
        URL urlNESService=new URL(props.getProperty("nes_service")); 
                       
        //set up truststore and keystore for NES connection
        configureNESConnection(props);
        
        //delete request 
        deleteNotam(urlNESService, nes_delete_request_file);       
    }
    
    public static void deleteNotam(URL url, String nes_delete_request_file)  {
        HttpsURLConnection conn = null;
        try {
            conn = (HttpsURLConnection) url.openConnection();

            HostnameVerifier hv = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            conn.setHostnameVerifier(hv);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "plain/text");            
            conn.setRequestMethod("POST");
            conn.connect();

            /* send request */
            PrintWriter out = new PrintWriter(new BufferedWriter(
                 new OutputStreamWriter(conn.getOutputStream())));
            String sdata;
            File inFile = new File(nes_delete_request_file);
            BufferedReader flin = new BufferedReader(new FileReader(inFile));
            while((sdata = flin.readLine()) != null){
                System.out.println(sdata);
                 out.println(sdata);
            }
            flin.close();
                      
            out.flush();
            out.close();

            /* read response */
            BufferedReader in = new BufferedReader(new InputStreamReader(
                   conn.getInputStream()));
           
            String inputLine;
            while ((inputLine = in.readLine()) != null)
               System.out.println(inputLine);

            /* cleanup before exit */
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void configureNESConnection(Properties props) {
        System.setProperty("javax.net.ssl.trustStore", props.getProperty("nes_local_truststore"));
        System.setProperty("javax.net.ssl.trustStorePassword", props.getProperty("nes_local_truststore_password"));

        System.setProperty("javax.net.ssl.keyStore", props.getProperty("nes_local_keystore"));
        System.setProperty("javax.net.ssl.keyStorePassword", props.getProperty("nes_local_keystore_password"));
        System.setProperty("javax.net.ssl.keyStoreType", props.getProperty("nes_local_keystore_type"));
    }
}
