/*
 * Map utilities
 */
require(["dojo/_base/Color","dojo/_base/lang", "dojo/on", "dojo/dom",
	"esri/layers/FeatureLayer","esri/graphic","esri/graphicsUtils",
	"esri/InfoTemplate","esri/toolbars/draw","esri/toolbars/edit",
	"esri/tasks/GeometryService","esri/tasks/BufferParameters","esri/tasks/query","esri/tasks/QueryTask",
	"esri/tasks/FindTask","esri/tasks/FindParameters","esri/SpatialReference",
	"esri/tasks/PrintTask","esri/tasks/PrintTemplate","esri/tasks/PrintParameters",
	"esri/geometry/Geometry","esri/geometry/Point","esri/geometry/Polyline",
	"esri/geometry/Polygon","esri/geometry/Circle","esri/graphicsUtils",
	"esri/geometry/Extent","esri/geometry/webMercatorUtils","esri/tasks/AreasAndLengthsParameters","esri/tasks/LengthsParameters",
	"esri/symbols/SimpleMarkerSymbol","esri/symbols/PictureMarkerSymbol","esri/symbols/MarkerSymbol",
	"esri/symbols/SimpleLineSymbol","esri/symbols/LineSymbol","esri/symbols/CartographicLineSymbol",
	"esri/symbols/SimpleFillSymbol","esri/symbols/PictureFillSymbol","esri/symbols/FillSymbol",
	"esri/symbols/Font","esri/symbols/TextSymbol",
	"esri/layers/GraphicsLayer","esri/layers/KMLLayer",
	"app/renderers","app/appConfig","app/appUtils","app/mapview/geomUtils","app/windows/shapetools",
	"libs/geo", "libs/latlong", "app/iconsConfig", "app/appConfig", "app/historyNav",
	"app/windows/tfrShapesGrid", "app/windows/tfrBuilder", "app/windows/shapesManager", "app/windows/features","app/symbols/symbolsPaths"],

function(Color,lang, on, dom,
	FeatureLayer,Graphic,graphicsUtils,
	InfoTemplate,Draw,Edit,
	GeometryService,BufferParameters,Query,QueryTask,
	FindTask,FindParameters,SpatialReference,
	PrintTask,PrintTemplate,PrintParameters,
	Geometry,Point,Polyline,
	Polygon,Circle,graphicsUtils,
	Extent,webMercatorUtils,AreasAndLengthsParameters,LengthsParameters,
	SimpleMarkerSymbol,PictureMarkerSymbol,MarkerSymbol,
	SimpleLineSymbol,LineSymbol,CartographicLineSymbol,
	SimpleFillSymbol,PictureFillSymbol,FillSymbol,
	Font,TextSymbol,GraphicsLayer,KMLLayer,
	renderers,appConfig,appUtils,geomUtils,shapetools,
	Geo,LatLon,iconsConfig,appConfig,historyNav,
	tfrShapesGrid, tfrBuilder, shapesManager, featuresManager, paths
){

nisis.ui.mapview.util = (function() {
	var mapview = nisis.ui.mapview;
	var util = {};
	//display generic error messages
	util.displayProcessesErrorMessage = function(error){
		console.log('Geometry Service Error: ',error);
		var detail, summary = 'Process Error!', severity = 'error';
		if(error && error.message){
			details = error.message;
		} else if (error && error.error && error.error.message) {
			details = error.error.message;
		} else {
			details = 'Unknown error message...';
		}
		nisis.ui.displayMessage(details,summary,severity);
	};
	//update mouse position coordinates
	util.updateMousePosition = function(evt){
		//console.log(evt.mapPoint);
		var unit = nisis.ui.widgets.mouseCoords.unit, mousePos, lat, lon;
		if (unit === "esriDecimalDegrees") {
			mousePos = esri.geometry.webMercatorToGeographic(evt.mapPoint);
			lat = mousePos.y.toFixed(5);
			lon = mousePos.x.toFixed(5);
		} else {
			mousePos = evt.mapPoint;
			lat = util.convertLengthUnits(mousePos.y,unit).toFixed(2);
			lon = util.convertLengthUnits(mousePos.x,unit).toFixed(2);
		}
		//update the lat/lon on the screen
		nisis.ui.widgets.mouseCoords.lat.html(lat);
		nisis.ui.widgets.mouseCoords.lon.html(lon);
	};
	//activate drawing tools
	util.activateDraw = function(tool,tooltip) {
		mapview.draw.activeTool = tool;
		var options = {
			showTooltips: true
		};
		if(!tool){
			console.error('Drawing tool required a tool name.');
			return;
		}
		//mapview.draw.tools.activate(Draw[tool.toUpperCase()], options);
		mapview.draw.tools.activate(tool, options);
		nisis.ui.disableZoomBoxes();
	};
	//deactivate drawing tools
	util.deactivateDraw = function() {
		mapview.draw.activeTool = false;
		mapview.draw.tools.deactivate();
		nisis.ui.enableZoomBoxes();

		$('.nisis-item').each(function(){
			if($(this).hasClass('nisis-item-active')){
				$(this).removeClass('nisis-item-active');
			}
		});
	};
	//activate selection by area tools
	util.activateDrawSelect = function(tool){
		mapview.draw.selectTools.activate(Draw[tool.toUpperCase()]);
		nisis.ui.disableZoomBoxes();
	};
	//deactivate selection by area tools
	util.deactivateDrawSelect = function(){
		mapview.draw.selectTools.deactivate();
		nisis.ui.enableZoomBoxes();
	};
	//create symbol
	util.getGraphicSymbol = function(geomType, symbType){
		var symbol = null;



		return symbol;
	};
	//create submenu on right click
	util.createContextMenu = function(evt){
		console.log(evt);
		return;
		var graphic = evt.graphic;
		//mapview.map.infoWindow = new nfoTemplate();
		mapview.map.infoWindow.setFeatures([graphic]);
		mapview.map.infoWindow.setTitle('Contextual Menu');
		//mapview.layers.NFDC_Airports.selectFeatures([graphic]);
		var content = "<select id='ctxt-menu'>"
			+ "<option value=''>All</option>"
			+ "<option value=''>Airports</option>"
			+ "</ul>";
		content += "<div id='context-menu'>" + content + "</div>";
		mapview.map.infoWindow.setContent(content);

		//add style to the windows
		var stylesLink = dojo.create('a', {
			'id': 'stylesLink',
			'class': 'action',
			'innerHTML': 'Styles',
			'href': 'javascript:void(0);'
		}, dojo.query('.actionList', mapview.map.infoWindow.domNode)[0]);
		//add delete to the windows
		var saveLink = dojo.create('a', {
			'id': 'saveLink',
			'class': 'action',
			'innerHTML': 'Save as',
			'href': 'javascript:void(0);'
		}, dojo.query('.actionList', mapview.map.infoWindow.domNode)[0]);

		//mapview.map.infoWindow.show(evt.screenPoint, mapview.map.getInfoWindowAnchor(evt.screenPoint));
		//console.log(mapview.map.infoWindow);
	};
	//create a buffer
	util.createBuffer = function(geom){
		$('#drawing-div').show();
		$('#draw-tools').click();
		$('#createBuffer').on('click', function(){
			var geometry = geom;
			//setting butter parameter
			var gsUrl = nisis.url.arcgis.services + 'Utilities/Geometry/GeometryServer';
			var geometryService = new GeometryService(gsUrl);
			geometryService.on('buffer-complete', function(geoms){
				util.showBufferResults(geoms);
			});
			var bufferParam = BufferParameters();
			bufferParam.geometries = [geometry];
			bufferParam.distances = [parseInt($('#bufferRadius').val(), 10)];
			bufferParam.outSpatialReference = mapview.map.spatialReference;
			bufferParam.unit = GeometryService[$('#bufferRadiusUnit').val()];
			geometryService.buffer(bufferParam);
		});
	};
	util.createGeometryBuffer = function(geoms, dist, unit){
		//console.log('Buffer: ', geoms, dist, unit);
		if(!geoms || !dist){
			console.log('Missing arcguments for buffer:', arguments);
			return;
		}

		//mapview.geometryService = new GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");

		var def = new $.Deferred(),
			params = new BufferParameters();

		if (geoms) {
			params.geometries = geoms;
		} else {
			def.reject('A geometry object is required.');
		}

		if (dist) {
			params.distances = [dist];
		} else {
			def.reject('The buffer distance is required.');
		}

		if (unit) {
			params.unit = GeometryService[unit];
		} else {
			params.unit = GeometryService.UNIT_METER;
			//params.unit = GeometryService.UNIT_STATUTE_MILE;
		}
		//default params
		//params.bufferSpatialReference = mapview.map.spatialReference;
		params.outSpatialReference = mapview.map.spatialReference;
		//keep this to true in order to get plannar distance
		params.geodesic = true;
		params.unionResults = true;
		//execute buffer
		mapview.geometryService.buffer(params,
		function(geoms){
			if (geoms && geoms.length ) {
				//try to simplify the result
				mapview.geometryService.simplify(geoms,
				function(geometries) {
					def.resolve(geometries);
				},
				function() {
					//if simplify fails, return the original geoms
					def.resolve(geoms);
				});

			} else {
				def.resolve(geoms);
			}
		},
		function(e){
			def.reject(e.message);
		});
		//return option;
		return def.promise();
	};
	//create service area
	util.createServiceArea = function(geom, dist, unit){
		nisis.ui.displayMessage('Service Area is not implemented yet.', 'Not Ready!');
	};
	//Union geoms
	util.unionGeometries = function(geoms){
		//console.log(geoms);
		var def = new $.Deferred();
		//var geomService new GeometryService();
		if(geoms && geoms.length && geoms.length > 1){
			nisis.ui.mapview.geometryService.union(geoms,
			function(g){
				//console.log(g);
				if(g){
					def.resolve(g);
				}else{
					def.reject('The union operation failed.');
				}
			},
			function(e){
				console.log(e);
				var msg = e.message ? e.message : 'The union operation failed.';
				def.reject(e.message);
			});
		} else {
			def.reject('Invalid input for union operation');
		}
		return def.promise();
	};
	//intersects geoms
	util.intersectGeometries = function(geoms, geom){
		var def = new $.Deferred();
		if(geoms && geoms.length && geoms.length > 0 && geom){
			nisis.ui.mapview.geometryService.intersect(geoms,geom,
			function(geometries){
				if(geometries){
					def.resolve(geometries);
				} else {
					def.reject('The intersection in util.intersectGeometries() function did not return any results.');
				}
			},
			function(e){
				//console.log(e);
				def.reject(e.message);
			});
		} else {
			def.reject('Invalid inputs for intersection.');
		}
		return def.promise();
	};
	//create polygon from center
	util.createPolygon = function(type, center){
		var pts, angle, distance, rings = [],
			units = $('#polygon-size-unit').val();

		geom = new Polygon(mapview.map.spatialReference);

		if (type == 'triangle') {
			pts = 3;
			var side = parseInt($('#side-size').val(), 10);
			distance = util.convertLengthUnits(side, units);

			angle = 360 / pts;
			for (var i = 0; i < pts; i++) {
				var rad = i * angle * Math.PI / 180;
				var x = center.x + distance * Math.cos(rad);
				var y = center.y + distance * Math.sin(rad);

				rings.push([x, y]);
			}
			rings.push(rings[0]);
		}
		else
			if (type == 'rectangle') {
				pts = 4;
				var rw = parseInt($('#width-size').val(), 10);
				rw = util.convertLengthUnits(rw, units);
				var rh = parseInt($('#height-size').val(), 10);
				rh = util.convertLengthUnits(rh, units);
				distance = Math.pow(rw / 2, 2) + Math.pow(rh / 2, 2);
				distance = Math.sqrt(distance);

				var pt1 = {};
				pt1.x = center.x;
				pt1.y = center.y + rh / 2;
				rings.push([pt1.x, pt1.y]);

				var pt2 = {};
				pt2.x = pt1.x + rw / 2;
				pt2.y = pt1.y;
				rings.push([pt2.x, pt2.y]);

				var pt3 = {};
				pt3.x = pt2.x;
				pt3.y = pt2.y - rh;
				rings.push([pt3.x, pt3.y]);

				var pt4 = {};
				pt4.x = pt3.x - rw;
				pt4.y = pt3.y;
				rings.push([pt4.x, pt4.y]);

				var pt5 = {};
				pt5.x = pt4.x;
				pt5.y = pt4.y + rh;
				rings.push([pt5.x, pt5.y]);

				rings.push(rings[0]);
				geom.addRing(rings);
			}
			else
				if (type == 'circle') {
					pts = 360;
					var radius = parseInt($('#radius-size').val(), 10);
					distance = util.convertLengthUnits(radius, units);
					angle = 360 / pts;
					for (var i = 0; i < pts; i++) {
						var rad = i * angle * Math.PI / 180;
						var x = center.x + distance * Math.cos(rad);
						var y = center.y + distance * Math.sin(rad);

						rings.push([x, y]);
					}
					rings.push(rings[0]);
					geom.addRing(rings);
				}
		/*else if (type == 'ellipse') {
		 pts = 360;
		 var ew = parseInt($('#width-size').val(),10);
		 ew = util.convertUnits(ew, units);
		 var eh = parseInt($('#height-size').val(),10);
		 eh = util.convertUnits(eh, units);

		 angle = (2 * Math.PI) / pts;

		 for (var l=0; l<pts; l++) {
		 var w = Math.cos(l * angle);
		 var h = Math.sin(l * angle);
		 var pt = mapview.map.toMap({
		 x: ew * w + center.x,
		 y: eh * h + center.y
		 });
		 rings.push(pt);
		 }
		 rings.push(rings[0]);

		 var ellipse = new esri.geometry.createEllipse({
		 center: center,
		 longAxis: ew,
		 shortAxis: eh,
		 numberOfPoints: pts,
		 map: mapview.map
		 });

		 geom.addRing(rings);
		 }*/
		//final geometry
		return geom;
	};
	//create circle
	util.createCircleGeometry = function(center, radius) {
		var polygon = new Polygon();

	    var points = [];

	    for ( var i = 0; i <= 360; i += 10) {

			var radian = i * (Math.PI / 180.0);
			var x = center.x + radius * Math.cos(radian);
			var y = center.y + radius * Math.sin(radian);

			points.push(new Point(x, y));
	    }

		polygon.addRing(points);

		//polygon = webMercatorUtils.geographicToWebMercator(polygon);
		polygon.spatialReference = nisis.ui.mapview.map.spatialReference;

        return polygon;
	};
	//create circle
	util.createArcGeometry = function(start, end, center, radius) {
		var polyline = new Polyline();

	    var points = [],
	    	center = {};

	    points.push(start);

	    for ( var i = 0; i <= 180; i += 10) {

			var radian = i * (Math.PI / 180.0);
			var x = center.x + radius * Math.cos(radian);
			var y = center.y + radius * Math.sin(radian);
			points.push(new Point(x, y));
	    }

	    points.push(end);

		polyline.addPath(points);

		//polyline = webMercatorUtils.geographicToWebMercator(polyline);
		polyline.spatialReference = nisis.ui.mapview.map.spatialReference;

        return polyline;
	};
	//get arc points ==> [-76,38], [-76,38], [-76,38],
	util.getArcPoints = function(start, end, center, dir) {
		if (!start || !end) {
			return;
		}

		var sr = new SpatialReference(4326),
			polyline = new Polyline(sr),
			ptA = new Point(start, sr),
			pt1 = webMercatorUtils.geographicToWebMercator(ptA),
			ptB = new Point(end, sr),
			pt2 = webMercatorUtils.geographicToWebMercator(ptB),
			path = [],
			len = 0,
			radius = 0;

		path.push(start);
		path.push(end);
		polyline.addPath(path);

		//len = esri.geometry.geodesicLengths([Polyline], esri.Units.Meters);
		len = esri.geometry.getLength(ptA, ptB);
		radius = len / 2;

		var lenParams = new LengthsParameters();
		lenParams.polylines = [polyline];
		lenParams.lengthUnit = GeometryService.UNIT_METER;
		lenParams.geodesic = false;

		nisis.ui.mapview.geometryService.lengths(lenParams)
		.then(
		function(result){
			console.log('Get arc points length: ', result.lengths);
		},
		function() {
			console.log('Get arc points length: ', arguments);
		});

		return;

		console.log('Get arc points: ', len, radius);

	    var points = [];

	    points.push(start);

	    for ( var i = 0; i <= 180; i += 10) {

			var radian = i * (Math.PI / 180.0);

			var x = center[0] + radius * Math.cos(radian);
			var y = center[1] + radius * Math.sin(radian);

			points.push([x, y]);
	    }

	    points.push(end);

	    console.log('Get arc points: ', points);

        return points;
	};
	//zoom to layer extent
	util.zoomToLayerExtent = function(layer){
		console.log('TODO: Zoom to layer extent', layer);
		var map = nisis.ui.mapview.map;
		if(map.spatialReference.wkid === layer.fullExtent.spatialReference.wkid){
			map.setExtent(layer.fullExtent);
		} else {
			var extent = layer.fullExtent,
			sr = new SpatialReference(102100);
			extent.set(sr);
			map.setExtent(extent);
		}
	};
	//get polygons area / length
	util.getShapesAreaLength = function(shapes, units){
		if(!shapes){
			return;
		}
		var def = new $.Deferred();
		//console.log('Inputs: ',arguments);
		var area,
			apParams = new AreasAndLengthsParameters();

		if(!units) {
			apParams.areaUnit = esri.tasks.GeometryService.UNIT_SQUARE_MILES;
			//apParams.areaUnit = esri.tasks.GeometryService.UNIT_SQUARE_KILOMETERS;

			//apParams.lengthUnit = esri.tasks.GeometryService.UNIT_METER;
			apParams.lengthUnit = esri.tasks.GeometryService.UNIT_STATUTE_MILE;
			//apParams.lengthUnit = esri.tasks.GeometryService.UNIT_KILOMETER;
		}
		apParams.polygons = shapes;
		apParams.calcutionType = "geodesic"; //planar, geodesic, preserveShape
		//nisis.ui.mapview.geometryService = new GeometryService("http://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer");
		nisis.ui.mapview.geometryService.areasAndLengths(apParams
		,function(result){
			//console.log('Result: ', result);
			def.resolve(result);
		},function(error){
			console.log('Error: ', error);
			def.reject(error);
		});

		return def.promise();
	};
	//get polyline lengths
	util.getShapesLength = function(shapes, units) {
		if(!shapes){
			return;
		}
		var def = new $.Deferred();
		//console.log('Inputs: ',arguments);
		var lParams = new LengthsParameters();
		lParams.calculationType = "geodesic";
		if(!units){
			lParams.lengthUnit = esri.tasks.GeometryService.UNIT_STATUTE_MILE;
		}
		lParams.polylines = shapes;
		lParams.geodesic = true;

		nisis.ui.mapview.geometryService.lengths(lParams
		,function(result){
			console.log('Result: ', result);
			def.resolve(result);
		},function(error){
			console.log('Error: ', error);
			def.reject(error);
		});

		return def.promise();
	};
	//convert map units
	util.convertLengthUnits = function(input/*meters*/, units/*output unit*/){
		var output, ratio;
		switch (units) {
		case 'esriMillimeters':
			ratio = 1000;
			output = input * ratio;
			break;
		case 'esriCentimeters':
			ratio = 100;
			output = input * ratio;
			break;
		case 'esriInches':
			ratio = 39.3701;
			output = input * ratio;
			break;
		case 'esriFeet':
			ratio = 3.28084;
			output = input * ratio;
			break;
		case 'esriYards':
			ratio = 1.09361;
			output = input * ratio;
			break;
		case 'esriKilometers':
			ratio = 0.001;
			output = input * ratio;
			break;
		case 'esriMiles':
			ratio = 0.000621371;
			output = input * ratio;
			break;
		case 'esriNauticalMiles':
			ratio = 0.000539957;
			output = input * ratio;
			break;
		default:
			ratio = 1;
			output = input * ratio;
		}

		return output;
	};
	/*
	 * convert units
	*/
	util.convertUnits = function(input, from, to){
		//console.log(arguments);
		var result = {
		nmin: input * 72913.4,
		nmft: input * 6076.12,
		nmyd: input * 2025.37183,
		nmmi: input * 1.15078,
		nmnm: input * 1,
		nmkm: input * 1.852,
		nmme: input * 1852,
		nmcm: input * 185200,
		nmmm: input * 1852000,

		miin: input * 63360,
		mift: input * 5280,
		miyd: input * 1760,
		mimi: input * 1,
		minm: input * 0.868976,
		mikm: input * 1.60934,
		mime: input * 1609.34,
		micm: input * 160934,
		mimm: input * 1609344,

		ydin: input * 36,
		ydft: input * 3,
		ydyd: input * 1,
		ydmi: input * 0.000568182,
		ydnm: input * 0.000493737,
		ydkm: input * 0.0009144,
		ydme: input * 0.9144,
		ydcm: input * 91.44,
		ydmm: input * 914.4,

		ftin: input * 12,
		ftft: input * 1,
		ftyd: input * 0.333333,
		ftmi: input * 0.000189394,
		ftnm: input * 0.000164579,
		ftkm: input * 0.0003048,
		ftme: input * 0.3048,
		ftcm: input * 30.48,
		ftmm: input * 304.8,

		inin: input * 1,
		inft: input * 0.0833333,
		inyd: input * 0.0277778,
		inmi: input * 0.0000157828283,
		innm: input * 0.0000137149028,
		inkm: input * 0.0000254,
		inme: input * 0.0254,
		incm: input * 2.54,
		inmm: input * 25.4,

		kmin: input * 39370.1,
		kmft: input * 3280.84,
		kmyd: input * 1093.6133,
		kmmi: input * 0.621371192,
		kmnm: input * 0.539957,
		kmkm: input * 1,
		kmme: input * 1000,
		kmcm: input * 100000,
		kmmm: input * 1000000,

		mein: input * 39.3701,
		meft: input * 3.28084,
		meyd: input * 1.0936133,
		memi: input * 0.000621371192,
		menm: input * 0.000539957,
		mekm: input * 0.001,
		meme: input * 1,
		mecm: input * 100,
		memm: input * 1000,

		cmin: input * 0.393701,
		cmft: input * 0.0328084,
		cmyd: input * 0.010936133,
		cmmi: input * 0.00000621371192,
		cmnm: input * 0.00000539957,
		cmkm: input * 0.00001,
		cmme: input * 0.01,
		cmcm: input * 1,
		cmmm: input * 10,

		mmin: input * 0.0393701,
		mmft: input * 0.00328084,
		mmyd: input * 0.0010936133,
		mmmi: input * 0.000000621371192,
		mmnm: input * 0.000000539957,
		mmkm: input * 0.000001,
		mmme: input * 0.001,
		mmcm: input * 0.1,
		mmmm: input * 1
	   	};

	   	if(result[from + to]){
		   return Number(result[from + to].toPrecision(6));
	   	}
	   	else if(input == 0) {
	   		return input;
	   	}
	   	else {
		   console.log(result[from + to], " has not been defined.");
		   return "unknown";
	   	}
	};
	//convert lat/lon ==> -76.48522
	util.convertDDtoDMS = function(input/*decimal degrees*/){
		//get ride of the +/-
		var input = Math.abs(input);
		//get degrees
		var d =  Math.floor(input);
		//get minutes
		var min = (input - d) * 60;
		var m = Math.floor(min);
		//get seconds
		var sec = (min - m) * 60;
		var s = Math.round(sec);
		//check for the max allowed. 60min = 1deg
		if(m >= 60){
			d += 1;
			m -= 60;
		}
		//check for the max allowed. 60sec = 1min
		if(s >= 60){
			m += 1;
			s -= 60;
		}
		//format ther results
		output = d + '\u00B0 ' + m + "' " + s + '" ';

		return output;
	};
	util.convertDMStoDD = function(deg, min, sec, dir){
		var degNum = parseFloat(deg);
		var minNum = parseFloat(min);
		var secNum = parseFloat(sec, 10);

		var errStr = "",
			lats = ['N', 'S'],
			lons = ['E', 'W'],
			lls = lats.concat(lons),
			validLat = $.inArray(dir.toUpperCase(), lats) != -1 ? true : false;
			validLon = $.inArray(dir.toUpperCase(), lons) != -1 ? true : false;


		if ( validLat && (degNum < 0 || degNum > 90) ) {
			errStr += "Latitude degrees must be positive and no greater than 90. ";
		}

		if ( validLon && (degNum < 0 || degNum > 180) ) {
			errStr += "Longitude degrees must be positive and no greater than 180. ";
		}

		if (minNum < 0 || minNum >= 60) {
			errStr += "Minutes must be positive and less than 60. ";
		}

		if (secNum < 0 || secNum >= 60) {
			errStr += "Seconds must be positive and less than 60. ";
		}

		if (dir === "") {
			errStr += "Direction must be specified. ";
		}

		//check for valid inputs
		if (errStr !== ""){
			nisis.ui.displayMessage(errStr, "", "error");
			return "ERROR";
		}

		var result = parseFloat(deg);
		result += parseFloat(min) / 60;
		result += parseFloat(sec, 10) / 3600;

		if(dir === "S" || dir === "W") {
			result *= -1;
		}

		return result;
	};
	//convert lat/lon
	util.convertDDtoDDM = function(input, ll) {
		var input = Math.abs(input);
		var d =  Math.floor(input);
		var min = (input - d) * 60;
		var m = Math.floor(min);
		var sec = (min - m) * 60;
		var s = Math.round(sec);

		if(m >= 60){
			d += 1;
			m -= 60;
		}

		output = d + '\u00B0 ' + min.toFixed(2) + "' ";

		return output;
	};
	//convert DD to UTM
	util.convertDDtoUTM = function(input/*decimal degrees*/){
		var input = Math.abs(input);
		var d =  Math.floor(input);
		var min = (input - d) * 60;
		var m = Math.floor(min);
		var sec = (min - m) * 60;
		var s = Math.round(sec);

		if(m >= 60){
			d += 1;
			m -= 60;
		}

		output = d + '\u00B0 ' + min.toFixed(2) + "' ";

		return output;
	};
	//convert DD to GARS ==> /*decimal degrees*/
	util.convertDDtoGARS = function(long, lat){
		var ret = "";
		if(long==180&&lat==90) return "1QZ11";
		var cellLongID = convertToCellNum(long+180);
			if(long==180) cellLongID = 1;
		var cellLatID = getCellLatID(lat+90);
		var quadID = getQuadID(long+180, lat+90);
		var keyID = getKeyID(long+180, lat+90);
		ret += cellLongID + cellLatID + quadID + keyID;

		return ret;
	}
	//GARS==> Cell Num
	function convertToCellNum(par){
		var ret = Math.floor(par)*2;
		if(par%1>=.5) ret++;
		return ++ret;
	}
	//GARS==> Cell Lat ID
	function getCellLatID(par){
		var ret = "";
		var n = convertToCellNum(par);
		ret += getFirstLetter(n)+getSecondLetter(n);
		return ret;
	}
	//GARS==> First Letter
	function getFirstLetter(par){
		var ret = "";
		var n = 65+Math.floor((par-1)/24);
		if(n>=73) n++;
		if(n>=79) n++;
		ret += String.fromCharCode(n);
		return ret;
	}
	//GARS==> Second Letter
	function getSecondLetter(par){
		var ret = "";
		var n = 64+(Math.floor(par)%24);
		if(n>=73) n++;
		if(n>=79) n++;
		if(n==64) n=90;
		ret += String.fromCharCode(n);
		return ret;
	}
	//GARS==> Quad ID
	function getQuadID(long, lat){
		var longDec = ((long%1)*10)%5;
		var latDec = ((lat%1)*10)%5;
		if(longDec<2.5){
			if(latDec<2.5) return "3";
			else return "1";
			}
		if(latDec<2.5) return "4";
		return "2";
	}
	//GARS==> Key ID
	function getKeyID(long,lat){
		var longDec = ((long%1)*100)%25;
		var latDec = ((lat%1)*100)%25;
		if(longDec<(25/3)){
			if(latDec>(50/3)) return "1";
			else if(latDec>(25/3)) return "4";
			else return "7";
		}
		else if(longDec<(50/3)){
			if(latDec>(50/3)) return "2";
			else if(latDec>(25/3)) return "5";
			else return "8";
		}
		else{
			if(latDec>(50/3)) return "3";
			else if(latDec>(25/3)) return "6";
			else return "9";
		}
	}
	//convert DD to GARS ==> /*decimal degrees*/
	util.convertGARStoDD = function(gars){
		var arr = gars.split("");
		var cellLongID = "";
		var cellLatID = "";
		var quadID;
		var keyID;
		var long;
		var lat = 0;
		var x = 0;

		while(!isNaN(arr[x])){
			cellLongID+=arr[x];
			x++;}
		for(y=0;y<2;y++){
			cellLatID+=arr[x];
			x++}
		quadID = Number(arr[x++]);
		keyID = Number(arr[x]);

		long = (Number(cellLongID)/2)-180.5;
		if(quadID==2||quadID==4) long+=.25;
		if(keyID==2||keyID==5||keyID==8) long+=Number((.25/3).toPrecision(6));
		if(keyID==3||keyID==6||keyID==9) long+=Number((.5/3).toPrecision(6));

		var code1 = cellLatID.charCodeAt(0);
		if(code1>=79) code1--;
		if(code1>=73) code1--;

		var code2 = cellLatID.charCodeAt(1);
		if(code2>=79) code2--;
		if(code2>=73) code2--;

		lat = ((code1-65)*12)-90;
		lat += (code2-65)/2;

		if(quadID==1||quadID==2) lat+=.25;
		if(keyID==4||keyID==5||keyID==6) lat+=Number((.25/3).toPrecision(6));
		if(keyID==1||keyID==2||keyID==3) lat+=Number((.5/3).toPrecision(6));

		//long and lat contain the coordinates of the GARS cell's origin - not sure how you wanted it returned
		return {'lon':long, 'lat':lat};
	};
	//create an array for an autocomplete search
	util.getFieldValues = function(url, field){
		var values = [];
		if(url && field){
			var query = new Query();
			query.returnGeometry = false;
			query.outFields = [field];

			var queryTask = new QueryTask(url);

			queryTask.execute(query, function(results){
				var features = results.features;
				console.log(features);
				$.each(results.features, function(idx,feat){
					var v = feat.attributes[field];
					if ($.trim(v) !== '') {
						values.push(feat.attributes[field]);
					} else {
						values.push('null');
					}
				});
				console.log(values);
				return values;
			});
		} else {
			values.push('No values');
		}
		return values;
	};
	//setup the profile window
	var setProfileWindow = function(options){
		if(!options){
			throw new Error('Profile Window takes options as arguments. None passed');
		}

		var id = options.id,
		title = options.title,
		content = options.content,
		layer = options.layer;

		var profile = $(content);
		profile.attr('id',id);
		profile.attr('title',title);
		profile.appendTo('body');
		var msg = $(profile).find('.p-msg')[0];
		//build the dialog
		profile.puidialog({
			showEffect: 'fade',
			hideEffect: 'fade',
			minimizable: true,
			maximizable: true,
			resizable: false,
			visible: true,
			modal: false,
			width: 800,
			minWidth: 800,
			minHeight: 300,
			height: '80%',
			location: 'top',
			afterHide: function(evt){
				//console.log(evt);
				$('#'+evt.target.id).remove();
			}
		});
		profile.spin(true);
		//hide iwl sign
		$('.p-iwl').hide();
		//build section panels
		$('.p-panel').each(function(i,el){
			$(this).puipanel({
				toggleable: true
				,closable: false
				,collapsed: true
				,toggleOrientation: 'vertical'
				,afterToggle: function(evt){
					console.log(evt,'toggle');
				}
			}).css({
				'margin-top':'5px',
				'margin-bottom':'5px'
			});
			if(i === 0){
				$(this).find('.p-sec-header a').css('display','block');
				$(this).find('.p-sec-content').css('display','block');
				$(this).puipanel('expand');
			}
		});
		//handle profile view resize
		profile.resize(function(evt){
			console.log(evt);
			$(this).css('height','auto');
		});
		//expand and collapse sub-sections
		$('span.p-sec-title').each(function(){
			$(this).click(function(){
				$(this).siblings().slideToggle();
				$(this).parent().siblings().slideToggle();
			});
		});
		//add event to panels edit buttons
		profile.find('.p-sec-edit').each(function(){
			$(this).click(function(){
				if($(this).children().hasClass('ui-icon-pencil')){
					var copy = $.extend({},profile.data('target')),
					target = profile.data('target');
					profile.data('copy',copy);
					console.log(profile.data('target'));
					$(this).children().removeClass('ui-icon-pencil').addClass('ui-icon-disk');
					$(this).parent().siblings()
					.find('.f-value').each(function(){
						var f = this;
						if($(f).hasClass('edit')){
							var field = $(f).data('field');
							if($(f).hasClass('select') && field){
								var select = $(f).find('select'),
								img = $(f).find('img'),
								field = select.attr('name');
								select.prop('disabled',false);
								select.change(function(e){
									target.attributes[field] = parseInt(select.val(),10);
									var sOpt = e.target.options[e.target.selectedIndex],
									oVal = $(sOpt).val(),
									icon = sOpt.dataset.icon;
									$(img).attr('src', 'images/status/'+icon+'.png');
								});
							} else if ($(f).hasClass('text') && field) {
								var input = $("<input type='text' name='" + field + "' class='form-control' value='" + $(f).text() + "'/>");
								$(f).html(input);
								input.blur(function(){
									if(target.attributes[field]){
										target.attributes[field] = input.val();
									}
								});
							} else if ($(f).hasClass('textarea' && field)){
								var textarea = $("<textarea name='" + field + "' class='form-control' value=''></textarea>").val($(f).text());
								$(f).html(textarea);
								textarea.blur(function(){
									console.log(field);
									if(target.attributes[field]){
										target.attributes[field] = textarea.val();
									}
								});
							}
						}
					});
				}else{
					var $this = $(this);
					layer.applyEdits(null,[profile.data('target')],null)
					.then(function(d){
						profile.find('.p-msg').html('Update was successful')
						.fadeIn(200)
						.fadeOut(1000);
						layer.refresh();
						$this.children().removeClass('ui-icon-disk').addClass('ui-icon-pencil');
						$this.parent().siblings()
						.find('.f-value').each(function(){
							var f = this;
							if($(f).hasClass('edit')){
								if($(f).hasClass('select')){
									var field = $(f).find('select'),
									label = field.attr('name'),
									value = field.val();
									field.prop('disabled',true);
								} else if ($(f).hasClass('text')) {
									var field = $(f).find('input'),
									label = field.attr('name'),
									value = field.val();
									$(f).html(value);
								} else if ($(f).hasClass('textarea')){
									var field = $(f).find('textarea'),
									label = field.attr('name'),
									value = field.val();
									$(f).html(value);
								}
							}
						});
					},function(e){
						profile.find('.p-msg').html(e.message)
						.css('color','red')
						.fadeIn(200)
						.faseOut(500);
					});
				}
			});
		});
	};
	//fly to a specity feature
	util.buildFeatureProfile = function(feature) {
		//console.log("Profile feature: ", feature);
		$('body').spin(true);

		var fId = feature.oid,
			layer = feature.layer,
			wId = layer.id + '_' + fId,
			profile,
			template = layer.apprenderer,
			msg,
			ansIcons = {
				0: iconsConfig.ans.nav.good,
				1: iconsConfig.ans.nav.warn,
				2: iconsConfig.ans.nav.warn,
				3: iconsConfig.ans.nav.warn,
				4: iconsConfig.ans.nav.down
			},
			ansLabels = {
				0: 'Normal Operation',
				1: 'Line Only Interruption',
				2: 'Reduced System/Equipment',
				3: 'Reduced Facility/Service Interruption',
				4: 'Full Interruption'
			};

		if(!feature || !layer || !layer.id){
			$('body').spin(false);
			throw new Error('Profile View require a feature. None passed');
		}
		//get profile markup, build the profile view
		$.ajax({
			url : 'api/',
			type : 'GET',
			data : {
				action: "get_profile",
				facId: feature.oid,
				type: template
			},
			success: function(data, status, xhr) {
				var resp = JSON.parse(data);
				if (resp['return'] == 'success') {
					require(["app/profile"], function(profileActions) {
						var vm = profileActions;

						//empty the profile area, append new html there
						$("#profileArea").html("");
						var profile = $($.parseHTML(resp.newhtml)[0]).appendTo($('#profileArea'));

						if($("#" + wId).length === 0) {
							profile.attr('id', wId);
						} else {
							nisis.ui.displayMessage("This facility profile is already open.", "warn");
							$('body').spin(false);
							profile.remove();
							return;
						}

						//initialize the kendo window now that HTML is in place
						var profileWindow = profile.kendoWindow({
							actions: ["Minimize","Maximize","Close"],
							resizable: true,
							visible: false,
							width: 1200,
							height: "auto",
							open: function() {
								//this.center();
								this.wrapper.css({top:50});
							},
							close: function(){
								//gotta remove it from the DOM
								this.destroy();
							}
						}).data('kendoWindow');

						//get feat data
						var facId = ""; //must be outside the query function for alerts
						util.queryFeatures({
							layer: layer,
							geom: feature.geom,
							returnGeom: true,
							outFields: ["*"],
							where: "OBJECTID = " + feature.oid
						})
						.then(function(data) {
							if(data && data.features.length > 0){
								var fields = data.features[0].attributes,
									ids = {
										'airports': 'FAA_ID',
										'ans': 'AND_ID',
										'atm': 'ATM_ID',
										'osf': 'OSF_ID',
										'tof': 'TOF_ID'
										//NISIS-2754 KOFI HONU
										//'resp': 'RESP_ID'
									};
								//console.log('Fields:', fields);
								vm.feat = data.features[0];
								//has to be set so profile saves, since we don't return geom in the query above
								vm.feat.geometry = feature.geom;
								vm.layer = feature.layer;
								vm.facType = template;
								//set fields data types
								vm.fields = {};
								$.each(data.fields, function(i, field) {
									vm.fields[field.name] = {
										alias: field.alias,
										type: field.type
									};
								});

								//setting the title and also facID (used in various places when profile opened - such as alerts)
								var title = "";
								var isArpt = false;
								var isANS = false;
								var isATM = false;
								var isOSF = false;
								var isTOF = false;

								switch (template){
									case "airports":
										isArpt = true;
										title = "Airport ";
										facId = fields.FAA_ID;
									break;
									case "atm":
										isATM = true;
										title = "ATM Facility ";
										facId = fields.ATM_ID;
									break;
									case "ans":
										isANS = true;
										title = "ANS System ";
										facId = fields.ANS_ID;
									break;
									case "osf":
										isOSF = true;
										title = "OSF ";
										facId = fields.OSF_ID;
									break;
									// NISIS-2754 KOFI HONU
									case "tof":
										isTOF = true;
										title = "TOF ";
										facId = fields.TOF_ID;
									break;
									default:
										title = layer.name + " ";
									break;
								}
								title += "Profile";

								kendo.init(profile);
								kendo.bind(profile, vm);

								//save the attributes before they're changed
								vm.origAttributes = $.extend({}, fields);

								//set up the profile's panelBar
								$("#profilePanelBar-" + template + feature.oid).kendoPanelBar({
									expandMode: "multiple",
									expand: function(e) {
										//expandMode "single" doesn't let you collapse the open panel (which is super lame)
										if(this.curPanel){
											this.collapse(this.curPanel);
										}
										this.curPanel = this.select();
									},
								});

								profileWindow.title(title);
								profileWindow.open();

								//this function saves typing out a billion datasources for the associated views
								function assocDS(typ){
									return new kendo.data.DataSource({
										transport: {
											read: {
												url: "api/",
												type: "POST",
												dataType: "json",
											},
											parameterMap: function(data, type) {

												var idField = ids[template],
													id = fields[idField];
												//TODO: stop using objectid as primary key
												if ( id && (typ === 'arpt-gbpa' || typ === 'arpt-gbpa-com') ) {
													//id = 'IAD';
												} else {
													id = facId;
												}

												if (type === "read") {
													return { action: "get_assoc_vw",
														type: typ,
														main_id: id
													};
												}
											}
										},
										schema: {
											data: "associated",
											total: "count"
										}
										,pageSize: 5
										,serverPaging: true
									});
								}
								//format ans system
								function formatGBPA(dataItem) {
									//console.log('GBPA dataItem:', dataItem['ANS_STATUS']);
									//check for blank results
									if (!dataItem || !dataItem['RUNWAY']) {
										return "";
									}
									var assAns = dataItem['ANS_STATUS'],
										systems = assAns.split(', ');
										value = "";

									$.each(systems, function(i, sys) {
										var v = sys.split(":"),
											ans_id = v[0],
											ans_status = v[1];

										var val = "<span class='gbpa-label'>";
										val += "<img class='gbpa-icon img-m c-pt' src='";
										val += ansIcons[ans_status] + "' ";
										val += "width='20px' height='20px' ";
										val += "data-bind='click:openGbpa' ";
										val += "data-id='" + ans_id + "' ";
										val += "data-status='" + ans_status + "' ";
										val += "data-type='" + dataItem['CATEGORY'] + "' ";
										val += "title='" + ansLabels[ans_status] + "' />";
										//val += " - " + ans_id;
										val += " - ";
										val += ans_id;
										val += "</span>";

										value += val;
									});

									//return the formatted text
									return value;
								}

								//set up any associated views
								var colWd = 100;
								var rmlsColumns = [
									{field: "ANS_ID", title: "ANS ID", width: colWd},
									{field: "LOG_ID", title: "LOG ID", width: colWd},
									{field: "FAC_IDENT", title: "FACILITY ID", width: colWd},
									{field: "LOG_STATUS", title: "LOG STATUS", width: colWd},
									{field: "CODE_CATEGORY", title: "CODE CATEGORY", width: colWd},
									{field: "START_DATETIME", title: "START DATE/TIME", width: colWd},
									{field: "END_DATETIME", title: "END DATE/TIME", width: colWd},
									{field: "MODIFIED_DATETIME", title: "MODIFIED DATE/TIME", width: colWd},
									{field: "EQUIPMENT_ID", title: "EQUIPMENT ID", width: colWd},
									{field: "EVENT_TYPE_CODE", title: "EVENT TYPE CODE", width: colWd},
									{field: "ERROR_CODE", title: "ERROR CODE", width: colWd},
									{field: "INTERRUPT_CONDITION", title: "INTERRUPTION CONDITION", width: colWd},
									{field: "LOG_SUMMARY", title: "LOG SUMMARY", width: "300px"},
								];

								if (isArpt) {
									$("#assoc-ans-" + template + fId).kendoGrid({
										columns: [
											{field: "BASIC_CAT", title: "Category", width: colWd},
											{field: "ANS_ID", title: "ID", width: colWd},
											{field: "LG_NAME", title: "Name", width: colWd},
											{field: "BASIC_TP", title: "Type", width: colWd},
											{field: "ANS_STS", title: "Status", width: colWd},
											{field: "DESCRIPT", title: "Description", width: colWd}
										],
										dataSource: assocDS("arpt-ans"),
										scrollable: false,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

									$("#assoc-atm-" + template + fId).kendoGrid({
										columns: [
											{field: "ATM_ID", title: "ID", width: colWd},
											{field: "LG_NAME", title: "Name", width: colWd},
											{field: "OPS_STATUS", title: "Status", width: colWd},
										],
										dataSource: assocDS("arpt-atm"),
										scrollable: false,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

									$("#assoc-rmls_open-" + template + fId).kendoGrid({
										dataSource: assocDS("arpt-rmls-open"),
										scrollable: true,
										columns : rmlsColumns,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

									$("#assoc-rmls_void-" + template + fId).kendoGrid({
										dataSource: assocDS("arpt-rmls-void"),
										scrollable: true,
										columns : rmlsColumns,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

									//track number of clicks
									var gbpaClicks = 0,
										gbpaDblClick = null;
									//Associated Ground Based Precision Approaches header
									$("#assoc-gbpa-com-" + template + fId).kendoGrid({
										dataSource: assocDS("arpt-gbpa-com"),
										columns: [
											{
												field: 'ANS_ID',
												title: 'Common ANS Systems',
												template: formatGBPA
											}
										],
										dataBound: function(e) {
											e.sender.wrapper.find('img.gbpa-icon')
											.each(function() {
												$(this).on('click', function(evt) {

													gbpaClicks++;

													if ( gbpaDblClick ) {
														return;
													}

													gbpaDblClick = setTimeout(function() {

														profileWindow.minimize();

														if ( gbpaClicks === 1 ) {
															vm.openGbpa(evt, 'infowin');
														} else if ( gbpaClicks === 2 ) {
															vm.openGbpa(evt, 'profile');
														}

														gbpaClicks = 0;
														gbpaDblClick = null;

													}, 500);
												});
											});
										}
									});

									//Associated Ground Based Precision Approaches
									$("#assoc-gbpa-" + template + fId).kendoGrid({
										dataSource: assocDS("arpt-gbpa"),
										columns: [
											{field: 'RUNWAY', title: 'Runway', width: "100px"},
											{field: 'TYPE', title: 'Type', width: "100px"},
											{field: 'CATEGORY', title: 'Category', width: "100px"},
											{
												field: 'ANS_ID',
												title: 'Associated ANS Systems',
												width: 'auto',
												template: formatGBPA
											}
										],
										scrollable: true,
										resizable: true,
										groupable: true,
										sortable: true
						                //pageable: true,
						                //pageSize: 5
						                /*pageable: {
						                	pageSize: 5
						                	,pageSizes: true
						                	,buttonCount: 3
						                }*/
						                ,dataBound: function(e) {
											e.sender.wrapper.find('img.gbpa-icon')
											.each(function() {

												$(this).on('click', function(evt) {

													gbpaClicks++;

													if ( gbpaDblClick ) {
														return;
													}

													gbpaDblClick = setTimeout(function() {

														profileWindow.minimize();

														if ( gbpaClicks === 1 ) {
															vm.openGbpa(evt, 'infowin');
														} else if ( gbpaClicks === 2 ) {
															vm.openGbpa(evt, 'profile');
														}

														gbpaClicks = 0;
														gbpaDblClick = null;

													}, 500);
												});
											});
										}
									});

									$("#assoc-arr_rates-" + template + fId).kendoGrid({
										dataSource: assocDS("arpt-arriv-rates"),
										columns: [
											{field: "APCFGTIME", title: "Airport Config Time", width: colWd},
											{field: "APPROACH", title: "Approach In Use", width: colWd},
											{field: "ARRRWY", title: "Arrival Runway", width: colWd},
											{field: "DEPRWY", title: "Departure Runway", width: colWd},
											{field: "AAR", title: "Adjusted Arrival Rate", width: colWd},
											{field: "ADR", title: "Adjusted Departure Rate", width: colWd},
											{field: "WEATHER", title: "Weather", width: colWd},
											{field: "STRATAAR", title: "Strategic Arrival Rate", width: colWd},
											{field: "AARADJUST", title: "Reason", width: colWd},
										],
									});

									$("#assoc-eng_rates-" + template + fId).kendoGrid({
										dataSource: assocDS("arpt-eng-rates"),
										columns: [
											{field: "ARRRWY", title: "Arrival Runway", width: colWd},
											{field: "DEPRWY", title: "Departure Runway", width: colWd},
											{field: "VMC", title: "VMC", width: colWd},
											{field: "LOW_VMC", title: "Low VMC", width: colWd},
											{field: "IMC", title: "IMC", width: colWd},
											{field: "LOW_IMC", title: "Low IMC", width: colWd},
										],
									});

									$("#assoc-runways-" + template + fId).kendoGrid({
										columns: [
											{field: "RWY_ID", title: "ID", width: colWd},
											{field: "RWY_LENGTH", title: "Length", width: colWd},
											{field: "RWY_WIDTH", title: "Width", width: colWd},
											{field: "BE_ELEVATION", title: "Base End Elevation", width: colWd},
											{field: "RE_ELEVATION", title: "Reciprocal End Elevation", width: colWd},
											{field: "RWY_SURFACE_TYPE", title: "Surface Type", width: colWd},
											{field: "RWY_SURFACE_TREATMENT", title: "Surface Treatment", width: colWd},
										],
										dataSource: assocDS("arpt-runways"),
										scrollable: false,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

								} else if(isANS) {
									$("#assoc-rmls_open-" + template + fId).kendoGrid({
										dataSource: assocDS("ans-rmls-open"),
										scrollable: true,
										columns: rmlsColumns,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

									$("#assoc-rmls_void-" + template + fId).kendoGrid({
										dataSource: assocDS("ans-rmls-void"),
										scrollable: true,
										columns: rmlsColumns,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});

								} else if(isATM) {
									$("#assoc-atm_arpts-" + template + fId).kendoGrid({
										columns: [
											{field: "FAA_ID", title: "Airport ID", width: colWd},
											{field: "LG_NAME", title: "Airport Name", width: colWd},
											{field: "OVR_STATUS", title: "Overall Status", width: colWd}
										],
										dataSource: assocDS("atm-arpts"),
										scrollable: false,
										resizable: true,
										sortable: {
						                    mode: 'single'
						                }
									});
								}

							} else {
								console.log(data.features.length);
								nisis.ui.displayMessage("No feature returned", '', 'error');
							}
							$('body').spin(false);

						},function(e){
							console.log(e);
							nisis.ui.displayMessage(e.message, '', 'error');
							$('body').spin(false);
						});

						var alert = $('.alertsButton');
						if(alert){
							alert.each(function(i, element) {
								$(element).click(function(evt){
									nisis.alerts.oid = feature.oid;
									nisis.alerts.facId = facId; //setting nisis.alerts.facId with local variable facId
									nisis.alerts.facType = element.getAttribute('facType');
									nisis.alerts.statusType = element.getAttribute('statusType');
									//var field = $(this).data('field');
									var alertsGrid = $("#alertTable").data('kendoGrid');
									var dataSource = alertsGrid.dataSource;
									//var aData = dataSource.data();
									var newRowFilter = {field: "ALERTID", operator: "eq", value: 0};
									var objFilter = { field: "OBJECTID", operator: "eq", value: nisis.alerts.oid};
									var facIdFilter = { field: "FAC_ID", operator: "eq", value: nisis.alerts.facId };
									var facTypeFilter = { field: "FAC_TYPE", operator: "eq", value: nisis.alerts.facType };
									var statusFilter = { field: "STATUS_TYPE_ID", operator: "eq", value: nisis.alerts.statusType };

									var facTypeAndStatusFilter = {
										logic: "and",
										filters: [ objFilter, statusFilter ]
									}

									//clearing the filters
									dataSource.filter({});

									//Adding the facType and statusType filters
									dataSource.filter({
										logic: "or",
										filters: [newRowFilter, facTypeAndStatusFilter]
									});
									dataSource.cancelChanges();

									var alertsWindow = $('#alerts-div').data('kendoWindow');
									dataSource.read();
									alertsWindow.restore();
									alertsWindow.center().open();

									$('.k-grid-toolbar').show();
									$("#alertTable").removeClass("hideGrid");
									$("#readOnlyGrid").addClass("hideGrid");
								});
							});
						}//end if(alert)

						var statusHistory = $('.statusHistoryButton');
						if(statusHistory){
							statusHistory.each(function(i,element){
								$(element).click(function(evt){
									historyNav.openFieldHistory(facId, element.getAttribute('data-field'),element.getAttribute('data-label'),element.getAttribute('data-status-explanation-type'));
        						});
							});
						}
						var facHistory = $('.facHistoryButton');
						if(facHistory){
							facHistory.each(function(i,element){
								$(element).click(function(evt){
									historyNav.openFacilityHistory(facId);
        						});
							});
						}

						//profile = $("#"+wId)[0];
					});
				} else {
					nisis.ui.displayMessage(status, '', 'error');
				}
			},
			error : function(xhr, status, err) {
				nisis.ui.displayMessage(status, '', 'error');
			}
		});
		//query options
		var options = {
			layer: layer,
			returnGeom: true,
			objectIds: [fId],
			outFields: ["*"]
		};
		//get features attributes
		var target, attrs, feats, fields, headers;
	};
	//find features with findTask
	util.findFeatures = function(resUrl, fields, values){
		console.log('Find featues: ',arguments);
		//return;
		//test data: Virginia,Maryland,Texas,Delaware
		var def = new $.Deferred();
		def.notify('Processing ...');
		//Set find task
		if(!resUrl){
			def.reject('Missing resources URL');
		}
		var url = resUrl.substr(0, resUrl.lastIndexOf('/'));
		url = url.replace('FeatureServer','MapServer');
		var layerIds = [],
		id = resUrl.substr(resUrl.lastIndexOf('/') + 1);
		layerIds.push(parseInt(id, 10));

		var findTask = new FindTask(url);
		var findParam = new FindParameters();
		//set find params
		findParam.returnGeometry = true;
		findParam.maxAllowableOffset = 10;
		findParam.outSpatialReference = mapview.map.spatialReference;
		findParam.layerIds = layerIds;
		if(!fields){
			def.reject('Missing resources FIELDS');
		}
		findParam.searchFields = fields;
		if(!values){
			def.reject('Missing search text');
		}
		//
		var len = values.length;
		var reqNum = 0, taskNum = 0;
		var done = false, results = [];
		//
		var taskSucceed = function(result){
			//console.log('Find features success: ',result);
			if ( ! result ) {
				return;
			}
			taskNum++;
			done = taskNum === reqNum ? true : false;
			var l = result.length - 1;
			if (!result || !result.length){
				def.reject('No Features found.');
			}
			$.each(result,function(i,feat){
				results.push(feat);
				if(i === l && done){
					console.log('Find features Results: ',results.length);
					def.resolve(results);
				}
			});
		};
		//
		var taskFailed = function(error){
			console.log('Find features error: ',error);
			taskNum++;
			done = taskNum === reqNum ? true : false;
			def.notify(values[taskNum-1] + ': ' + error.message);
			if (done) {
				if (results.length > 0) {
					def.resolve(results);
				}else{
					def.reject('App could not retreive any any feature. Check spelling ...');
				}
			}else{
				def.notify('Could not retreive ' + values[taskNum-1]);
			}
			//console.log(taskNum - 1 + ': ',error);
		};
		//
		if(len > 0){
			$.each(values,function(idx,value){
				//console.log('Find feature value: ', value);
				reqNum++;
				findParam.searchText = value;
				findTask.execute(findParam, taskSucceed, taskFailed);
			});
		}else{
			def.reject('Search text is empty');
		}

		return def.promise();
	};
	//query features does not give me back correct intersection. Sigh. Will fix later.
	/*
	 * returns all user shapes that intersect User Shapes on the Polygon_Features layer
	 *
	 */
	util.findIntersectedUserShapes = function( graphic ) {
		var def = new $.Deferred(),
		layer = nisis.ui.mapview.map.getLayer('Polygon_Features'),
        url = nisis.ui.mapview.map.getLayer('Polygon_Features').url,
        query = new esri.tasks.Query(),
        queryTask = new esri.tasks.QueryTask(url);

        //need to restrict the query to the current query definition
        var qDef;
	    if(layer && layer.getDefinitionExpression){
	    	qDef = layer.getDefinitionExpression();
	    }
	    if(qDef !== undefined || !qDef === ""){
	    	query.where = qDef;
	    }

        query.geometry = graphic.geometry;
        query.returnGeometry = true;
        query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_INTERSECTS;
        query.outFields = ['OBJECTID'];
        queryTask.execute(query, function(results){
            	var intersectedShapes = [];
                var features = results.features;
                $.each(results.features, function(idx,feat){
                    if (feat.attributes.hasOwnProperty("OBJECTID")) {
                        //dont keep the graphic that I searched intersections for
                        if (feat.attributes["OBJECTID"] != graphic.attributes["OBJECTID"]) {
                        	var newShape = {};
	                        newShape.OBJECTID = feat.attributes["OBJECTID"];
	                        intersectedShapes.push(newShape);
                        }
                    }
                });
             	//resolve it
                def.resolve(intersectedShapes);

        }, function(error) {
        	console.log("findIntersectedUserShapes error:", error);
        	nisis.ui.displayMessage('Unable to determine polygons intersection', '', 'error');
        });

        return def.promise();
    };
	//query features
	/* Options:
	 * layer <layer>
	 * returnGeom <boolean>
	 * where <string>
	 */
	util.queryFeatures = function(options) {
		//console.log('Query Features Options: ',options);
		var def = new $.Deferred();
		def.notify('Preparing request ...');
		var queryTask, url;

		if(options.layer){
			url = options.layer.url;

			var p = url.split('/'),
				lyrId = p[p.length-1];

			if(!$.isNumeric(lyrId)){
				//for dymanic layers
				url += "/0";
			}

			queryTask = new QueryTask(url);
		}else{
			def.reject('No layer specified.');
		}

		var query = new Query();

		if(options.spatialRelationship){
			if(options.layer.id == 'Incident_Watch_Areas' || options.layer.id == 'Polygon_Features'){
				query.spatialRelationship = query.SPATIAL_REL_WITHIN;
			}else {
				//query.spatialRelationship = query[options.spatialRelationship];
				query.spatialRelationship = query.SPATIAL_REL_WITHIN;
			}
		}else{
			query.spatialRelationship = query.SPATIAL_REL_WITHIN;
		}

		if (options.returnGeom) {
			query.returnGeometry = options.returnGeom;
		} else {
			query.returnGeometry = false;
		}

		if(options.outFields){
			query.outFields = options.outFields;
		}else{
			query.outFields = options.layer._outFields;
		}

		if(options.text){
			query.text = options.text;
		}

		if(options.where){
			query.where = options.where;
		}

		if(options.geom){
			//where and geom can't be set together
			if(!query.where){
				query.geometry = options.geom;
			}
		} else {
			if(!query.where){
			   //query.geometry = options.layer.fullExtent;
			   query.geometry = options.layer.fullExtent.expand(1.2);
			}
		}
		//console.log('Query Options: ',options);
		//get only ids
		var subset, feats, dLen,
		getObjectIDs = function(){
			def.notify('Getting records count ...');
			//console.log("Query options: ", query);
			//in case the user specifies the objectids
			var objectIds = null;
			if(options.objectIds) {
				objectIds = options.objectIds;
			}
			//check for the size limit (1000) ==> just for performance
			if(objectIds && objectIds.length < 1000) {
				//console.log("Objectids: ", objectIds);
				query.objectIds = objectIds.sort();
				sendRequest(query);
			} else if (objectIds && objectIds.length > 1000) {
				//console.log("Objectids: ", objectIds);
				dLen = objectIds.length;
				feats = optionsIds.sort();
				splitRequests();
				def.notify('Result: ' + dLen + ' features ...');
			} else {
				//get objectids
				//console.log("Objectids: ", objectIds);
				//console.log('Query for OIDs: ', query, options);
				//use layer default filters
				if( options.layer.query && options.layer.query !== "" ) {
					if( query.where && query.where !== "" ){
						query.where = options.layer.query + " AND (" + options.where + ")";
					} else {
						query.where = options.layer.query;
					}
				}
				//console.log('Query for OIDs: ', query, options);
				//var filter = "SIGNT1 IN ('U', 'S') OR SIGNT2 IN ('U', 'S') OR SIGNT3 IN ('U', 'S')";
				// query.where = "SIGN1 = 'U72'";
				queryTask.executeForIds(query,
				function(oids){
					//console.log('Objectids: ', oids);
					//response may be null
					if(oids && oids.length){
						dLen = oids.length;
						feats = oids.sort();
						//console.log('Objectids: ', oids.length);
						def.notify('Results: ' + dLen + ' features ...');
						splitRequests();
					} else {
	              		def.resolve(null);
	              		return;
					}
				},
				function(e){
					console.log('OBJECTID Query Error: ', e);
					def.reject(e.message);
				});
			}
		};
		//split the requests into chunks of 999
		var n = 0,
		requests = [],
		splitRequests = function(){
			var len = feats.length;
			//splits oids
			while ( feats.length ) {
				//console.log('OBJECTIDs: ', feats.length);
				requests.push(feats.splice(0, 999));
			}
			n = requests.length;
			//console.log('Request chuncks: ', n, requests);
			//Send multiple requests and let the Deferred object handle the results
			$.each(requests,function(i, request){
				var q = $.extend({}, query);
				q.objectIds = request;
				sendRequest(q);
				//console.log('Request #', i+1, q);
			});
		};
		//
		var sendRequest = function(query) {
			//def.notify("Retreiving " + feats.length + " from server ...");
			var execute = queryTask.execute(query);
			execute.then(function(d){
				//console.log('Request data #: ', n, d.features.length);
				n--;
				if (n > 0) {
					def.notify({
						more: true,
						data: d
					});
				} else {
					def.resolve(d);
				}
			}, function(e){
				//console.log('Request data error: #', n, e);
				n--;
				if( n > 0 ) {
					def.notify(e.message);
				} else {
					def.reject(e.message);
				}
			}, function(p){
				def.notify(p);
			});
		};
		//
		if(options.objectIds && options.objectIds.length < 1000){
			feats = [];
			query.objectIds = options.objectIds;
			sendRequest(query);
		} else {
			getObjectIDs(options.layer.id);
		}
		//
		return def.promise();
	};
	//filter existing features
	util.filterFeatures = function(layer, query){
		//console.log(layer, query);
		if (layer && typeof layer == 'object' && layer.length) {
			for (var i = 0, len = layer.length; i < len; i++) {
				mapview.layers[layer[i]].setDefinitionExpression(query);
				if (!mapview.layers[layer[i]].visible) {
					//mapview.layers[layer[i]].show();
					nisis.ui.changeOverlaysVisibility(mapview.layers[layer[i]].id, true);
				}
			}
		}
		else
			if (layer && typeof layer == 'string') {
				mapview.layers[layer].setDefinitionExpression(query);
				if (!mapview.layers[layer].visible) {
					//mapview.layers[layer].show();
					nisis.ui.changeOverlaysVisibility(mapview.layers[layer].id, true);
				}
				setTimeout(function(){
					if (mapview.layers[layer].graphics.length === 0) {
						nisis.ui.displayMessage('Filter expression returned 0 features', 'No Features', 'info');
					}
					else {
						mapview.map.setExtent(util.getGraphicsExtent(mapview.layers[layer].graphics));
					}
				}, 1000);

			}
			else {

			}
	};
	//query existing features
	util.clearFeaturesQuery = function(layer){
		if (layer === "*") {
			$.each(mapview.layers.overlays, function(i, lyr){
				lyr.setDefinitionExpression();
				lyr.clearSelection();
			});
		}
		else {
			mapview.layers[layer].setDefinitionExpression();
			mapview.layers[layer].clearSelection();
		}
		mapview.map.graphics.clear();
	};
	//query cluster
	util.queryCluster = function(center, position){
		//console.log('Querying Clusters ...');
		if(!center){
			console.log('Missing arguments for queryCluster function');
			return;
		}
		//cluster layers ids
		//NISIS-2754 - KOFI HONU
		//var nto = ['airports','airports_pu','airports_pr','airports_others','atm','ans','ans1','ans2','osf','teams'];
		var nto = ['airports','airports_pu','airports_pr','airports_others','airports_military','atm','ans','ans1','ans2','osf','tof'];

		var scale = appConfig.scaleValue,
			scaleUnit = appConfig.scaleUnit,
			scalebar = $('.esriScalebarRuler').width(),
			radius = scale * 30 / scalebar;
			radius = util.convertUnits(radius, scaleUnit, 'mi');

		var geom = new Circle(center, {
			geodesic: true,
			radius: radius,
			radiusUnit: esri.Units.MILES
		});
		//create a graphic
		var symbol = renderers.symbols.cluster,
			graphic = new Graphic(geom, symbol);
		//add graphic to the map
		mapview.map.graphics.add(graphic);
		//flash cluster
		var visible = true, time = 0;
		flash = setInterval(function(){
			time += .5;
			if(visible){
				graphic.hide();
			} else {
				graphic.show();
			}
			visible = !visible;
		},500);
		//stop flashing
		var stopFlash = function(){
			clearInterval(flash);
			graphic.show();
		};

		var cluster = $("<div id='cluster-div' class='dialog'></div>");

		if( $('#cluster-div').length > 0 ) {
			$('#cluster-div').data('kendoWindow').close();
		}

		var result = [],
			list = $("<div class='cluster'></div>"),
			item = $('<li>Processing ...</li>'),
			tasks = 0,
			featId = {
				'airports': 'FAA_ID',
				'atm': 'ATM_ID',
				'ans': 'ANS_ID',
				'osf': 'OSF_ID',
				//NISIS-2754 - KOFI HONU
				'tof': 'TOF_ID'
				//'teams': 'RES_ID'
			};

		//cluster symbols
		var drawClusterSymbols = function(){
			if ( !result.length ) {
				return;
			}

			$('div.cluster-item').each(function(){
				var r = $(this).data('renderer'),
					s = $(this).data('symbol'),
					a = $(this).data('attr'),
					l = $(this).data('label');

				var st = s.style,
					sp = d3.select(this)
					.append('svg');

				var label = $("<div class='cluster-label clear'></div>");
				label.text(a[l]);
				$(this).append(label);

				if(st && st === 'circle'){
					var	w = s.size + s.outline.width,
						h = w,
						r = s.size / 2,
						cx = w / 2,
						cy = h / 2,
						t = s.outline.width;
					sp.attr('width', w)
					.attr('height', h)
					.append(s.style)
					.attr('cx', cx)
					.attr('cy', cy)
					.attr('r', r)
					.style('fill', s.color.toHex())
					.style('stroke', s.outline.color.toHex())
					.style('stroke-width', s.outline.width);
				} else if (st && st === 'square'){
					var	t = s.outline.width
						w = s.size + (t*2),
						h = w;

					sp.attr('width', w)
					.attr('height', h)
					.append('rect')
					.attr('x',t)
					.attr('y',t)
					.attr('width', s.size)
					.attr('height', s.size)
					.style('fill', s.color.toHex())
					.style('stroke', s.outline.color.toHex())
					.style('stroke-width', s.outline.width);
				} else {

					if(s.url){
						var t = 3,
							w = s.width,
							h = s.height;

						sp.attr('width', w+t)
						.attr('height', h+t)
						.append('svg:image')
						.attr('x',t)
						.attr('y',t)
						.attr('width', w)
						.attr('height', h)
						.attr('xlink:href', s.url);
					} else {
						console.log('Unknown Symbol');
					}
				}
			});
		};

		var showClusterList = function(){
			var w, len = result.length;

			if(len === 0){
				var item = $('<div>Result: 0</div>');
				list.append(item);
			} else {
				if(len > 5 && len < 31){
					w = 30 * 5 + 10;
				} else if (len > 30 && len < 61) {
					w = 30 * 10 + 10;
				} else if (len > 60 && len < 101) {
					w = 30 * 15 + 10;
				} else if (len > 100) {
					w = 30 * 20 + 10;
				} else {
					w = 25 * len + 10;
				}
			}

			//build cluster window
			$('body').append(cluster);
			cluster.append(list);
			cluster.kendoWindow({
				title: 'Clustered Features',
				visible: false,
				actions: ['Minimize','Close'],
				width: 400,
				height: 'auto',
				maxHeight: 500,
				position: {
					top: 50,
					left: 50
				}
				,open: function(e){
					//reposition the window
					var W = $('#map').width(),
						w = $(e.sender.wrapper).width(),
						H = $('#map').height(),
						h = $(e.sender.wrapper).height(),
						top = position[0],
						left = position[1],
						radius = 30;
					//initial position
					$(e.sender.wrapper).css({
						top: top,
						left: left
					});
					//recenter if window out of body
					if( W - left < w || H - top < h){
						e.sender.center();
					}
					//stop flashing
					stopFlash();
					//update symbols
					drawClusterSymbols();
				},
				close: function(e){
					this.destroy();
					mapview.map.graphics.remove(graphic);
				}

			}).data("kendoWindow").open();
		};
		//query each tracked objects layer
		$.each(nto,function(i,l){
			var layer = mapview.map.getLayer(l);
			if(layer && layer.visible){
				tasks += 1;
				var options = {
					layer: layer,
					geom: geom,
					returnGeom: false,
				};
				//send query request
				util.queryFeatures(options)
				.then(function(d){
					var feats = d.features,
						labelField = '',
						p = 0;

					if(feats.length > 0){
						for (field in feats[0].attributes){
							p++;
							if(p === 2){
								labelField = field;
							}
						}
						//sort result
						feats.sort(function(a, b){
							if (a.attributes[labelField] < b.attributes[labelField]){
								return -1;
							} else if (a.attributes[labelField] > b.attributes[labelField]){
								return 1;
							} else {
								return 0;
							}
						});
					}

					//check if there is a query
					var qField, qValue;
					/*if(layer.query && layer.query != null){
						var q = layer.query.split('=');
						qField = $.trim(q[0]);
						qValue = $.trim(q[1]);
						qValue = qValue.replace(/["']/g, '');
					}*/
					//group clusters by layer
					var wrap = $('<div class="cluster-layer"></div>');
					if ( result.length > 0 ) {
						wrap.addClass('cluster-line');
					}
					$.each(feats,function(i,f){
						var item = $('<div class="cluster-item pointer"></div>');
						var renderer = layer.renderer,
							sym = layer.renderer.getSymbol(f),
							attr = f.attributes;
						//save this in the dom for later use
						item.data('symbol', sym);
						item.data('renderer', renderer);
						item.data('attr',attr);
						item.data('label',labelField);

						//filter out queries
						if(qField && qValue){
							if(attr[qField] && attr[qField] === qValue){
								result.push({
									renderer: layer.renderer,
									symbol: sym,
									feature: f
								});
							}
						}
						else {
							result.push({
								renderer: layer.renderer,
								symbol: sym,
								feature: f
							});
						}
						//mouse over for datablocks
						item.on('mouseover', function(evt){
							nisis.ui.displayDataBlocks(layer, f.attributes, evt);
						});
						item.on('mouseout', function(evt){
							nisis.ui.hideDataBlocks();
						});
						//click for info window
						item.on('click', function(evt){
							//bring up profile (?or summary window for layer who do not have profile?)
							nisis.ui.hideDataBlocks();
							util.displayInfoWindow(f, layer);
						});
						wrap.append(item);

					});
					list.append(wrap);
					tasks -= 1;
					//console.log(tasks);
					if(tasks === 0){
						showClusterList();
					}
				},function(err){
					console.log(err);
					tasks -= 1;
					//console.log(tasks);
					if(tasks === 0){
						showClusterList();
					}
				});
			} else {
				//console.log(l + ': is not visible');
			}

			/*if(i === nto.length -1){
				//console.log('Query result: ',result);
				//console.log('Number to queries: ',tasks);
				if(tasks === 0){
					showClusterList();
				}
			}*/
		});
	};
	//display feature info window
	util.displayInfoWindow = function(feature, lyr){

		if ( !feature ) {
			console.log('Missing argument: feature');
			return;
		}
		console.log('Selected Cluster Feature: ', feature);

		var layer = feature.getLayer();
		//check for valid layer
		if ( !layer ) {
			if (lyr){
				layer = lyr;
			} else {
				console.log('Missing argument: layer');
				return;
			}
		}
		//get selected graphic
		var graphics = layer.graphics,
			oid = feature.attributes.OBJECTID;

		if ( oid && graphics ) {
			var stop = false;
			$.each(graphics,function(i,graphic){
				var id = graphic.attributes.OBJECTID;
				//console.log('Cluster Feature ' + i + ': ', id);
				if(id && id === oid){
					//stop searching
					stop = true;
					//console.log('Cluster Selected Matched Graphic: ', graphic);
					//clear old selection
					layer.clearSelection();
					mapview.map.infoWindow.hide();
					//add new selection to the feature layer
					var query = new Query();
					query.geometry = graphic.geometry;
					//this is needed in order to select only one feature at that location
					query.objectIds = [parseInt(oid)];
					//execute new selection based on query
					layer.selectFeatures(query, FeatureLayer.SELECTION_NEW)
					.then(function(results, selMethod){
						if(results && results.length > 0){
							var attr = $('#btnAttributes');
							if(attr){
								if(layer.group === 'nisisto'){
									feature = results[0];
			                        var evt = [];
			                        //evt.screenPoint =  new ScreenPoint(e.event.pageX, e.event.pageY);
			                        evt.graphic = feature;
			                        evt.graphic._layer = layer;
			                        //convert from screen point to map point
			                        evt.mapPoint = nisis.ui.mapview.util.getGeometryMapPoint(feature.geometry);
			                        //bring up NISIS Tracked Object Profile
			                        nisis.ui.mapview.util.bringUpNISISTOProfile(evt);
								}
								else {
									//do this obscure thing
									attr.trigger('click');
								}
							} else {
								var msg = 'Feature Attributes Inspector has not been defined';
								nisis.ui.displayMessage(msg, '', 'error');
							}

						} else {
							var msg = 'App could not find the selected feature at the specified location.';
							nisis.ui.displayMessage(msg, '', 'error');
						}
					},function(error){
						console.log('Selected Features error: ', error);
						var msg = 'App could not identify the selected feature.';
						nisis.ui.displayMessage(msg, '', 'error');
					});
				}
				if(stop){
					//exit the loop
					return false;
				}
			});
			//feature may be out of mapview
			if ( !stop ) {
				var msg = 'App could not find the selected feature within current mapview.';
				nisis.ui.displayMessage(msg, '', 'error');
				//nisis.mapview.map.centerAt(feature);
			}
		} else {
			var msg = 'Selected Feature is missing OBJECTID attributes';
			nisis.ui.displayMessage(msg, '', 'error');
		}
	};
	//get layer fields
	util.getLayerFields = function(layer){
		if(!layer){
			return;
		}
		var def = new $.Deferred();
		var url = layer.url;

		if(!url){
			def.reject('There is not url in your layer.');
		}

		var urlParts = url.split('/');
		var lyrIdx = parseInt(urlParts[-1]);

		if(!$.isNumeric(lyrIdx)){
			url += "/0";
		}

		//console.log(url);

		$.ajax({
			url: url,
			data: {f:'json'},
			dataType: 'json',
			success: function(data,status,xhr){
				console.log(arguments);
				if(status === 'success'){
					def.resolve(data.fields);
				} else {
					def.reject('App could not get layer fields.');
				}
			},
			error: function(xhr,status,error){
				console.log(arguments);
				def.reject(error);
			}
		});

		return def.promise();
	};
	//set spatial query geom and param
	util.setSpatialQueryGeom = function(geom){
		$('#filter-div').show();
		$('#filterArea').click();
		//$('#runSpFilter').prop("disabled", false);
		//$('#runSpFilter').button();
		$('#runSpFilter').on('click',function(){
			var layer = $('#filterLayers').val();
			var	type = $('#fSahpeCriteria').val();
			if (layer !== '*' || layer !== ''){
				util.selectFeaturesByArea(geom,layer,type);
			} else {
				nisis.ui.displayMessage('Please select a layer to filter', 'Missing Layer!', 'info');
			}
		});
	};
	//select by area
	util.selectFeaturesByArea = function(widget){ //geom,lyr,type
		var def = new $.Deferred();
		def.notify('Processing ...');
		mapview.map.graphics.clear();
		//
		nisis.ui.enableZoomBoxes();
		//check for the coller
		var targetLyr, tLyr, gqps, type, union, fback;
		if (widget == 'filter') {
			$('.shapeFilter').css('border-color', '#333');
			tLyr = $('#filterLayers').val();
			gqps = $('#fShapeList').val();
			type = $('fShapeCriteria').val();
			union = $('#unionGQPs').is(':checked');
			fback = $('#filterMsg');
		} else if (widget == 'table'){
			tLyr = nisis.ui.widgets.tableview.panel.layers.val();
			//gqps = nisis.ui.widgets.tableview.panel.gqpsList.val();
			gqps = $('#tbl-gqps-list').val();
			type = nisis.ui.widgets.tableview.panel.spRel.val();
			union = $('#tbl-unionGQPs').is(':checked');
			fback = nisis.ui.widgets.tableview.panel.filterMsg;
		}
		//console.log(tLyr,gqps,type,widget);
		//layer to be filtered
		var gLyr = 'NisisTO_Incident_Watch_Areas';
		//get geom to be used for filter
		var getGeom = function(gLyr,gqps,union){
			def.notify('Looking for GQPs ...');
			var gqpsLyr = mapview.layers[gLyr];
			if(gqpsLyr.graphics.length > 0){
				var geoms = [];
				$.each(gqpsLyr.graphics,function(idx,graphic){
					$.each(gqps,function(idx,gqp){
						if(graphic.attributes.OBJECTID == gqp){
							geoms.push(graphic.geometry);
						}
					});
				});
				//console.log(geoms);
				if(geoms.length > 0){
					if (union) {
						if (geoms.length > 1) {
							fback.html('Grouping selected GQP(s)').css('color', 'green');
							def.notify('Grouping selected GQP(s) ...');
						}
						mapview.geometryService.union(geoms, function(unionGeom){
							//console.log(unionGeom);
							fback.html('Selecting features ...').css('color', 'green');
							queryFeatures(unionGeom);
						}, function(error){
							//console.log(error);
							fback.html(error.message).css('color', 'red');
						});
					}
					else {
						var intGeom = null, intGeoms = [];
						if (geoms.length > 1) {
							fback.html('Intersecting selected GQP(s)').css('color', 'green');
							def.notify('Intersecting selected GQP(s) ...');
							//console.log(intGeom, intGeoms);
							$.each(geoms, function(idx, geom){
								if (idx === 0) {
									intGeom = geom;
								//console.log(intGeom, intGeoms);
								} else {
									intGeoms.push(geom);
									//console.log(intGeom, intGeoms);
									if (idx === (geoms.length - 1)) {
										//console.log(intGeom, intGeoms);
										mapview.geometryService.intersect(intGeoms, intGeom, function(resGeoms){
											if (resGeoms.length === 0) {
												fback.html('There is no intersection for selected GQP(s) ...').css('color', 'red');
												def.notify('There is no intersection for selected GQP(s) ...');
											} else if (resGeoms.length > 1) {
													mapview.geometryService.union(resGeoms,
														function(unionGeom){
															//console.log(unionGeom);
															if (unionGeom.rings.length > 0) {
																fback.html('Preparing to select features ...').css('color', 'green');
																def.notify('Preparing to select features ...');
																queryFeatures(unionGeom);
															} else {
																fback.html('The intersection for selected GQP(s) is not valid.').css('color', 'red');
																def.notify('The intersection for selected GQP(s) is not valid.');
															}
														}, function(error){
															//console.log(error);
															fback.html(error.message).css('color', 'red');
															def.notify(error.message);
													});
												}
												else {
													fback.html('Preparing to select features ...').css('color', 'green');
													//console.log(resGeoms[0]);
													if (resGeoms[0].rings.length > 0) {
														queryFeatures(resGeoms[0]);
													}
													else {
														fback.html('The intersection for selected GQP(s) is not valid.').css('color', 'red');
														def.notify('The intersection for selected GQP(s) is not valid.');
													}
												}
										}, function(error){
											//console.log(error);
											fback.html(error.message).css('color', 'red');
											def.notify(error.message);
										});
									}
								}
							});
						} else {
							fback.html('Preparing to select features ...').css('color', 'green');
							def.notify('Preparing to select features ...');
							intGeom = geoms[0];
							queryFeatures(intGeom);
						}
					}
				} else {
					fback.html('Could not find the selected GQP(s)').css('color','red');
					def.notify(error.message);
				}
			} else {
				fback.html('The GQP Layer is empty!').css('color','red');
				def.notify('The GQP Layer is empty!');
			}
		};

		var lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 0, 0]), 3);

		var polySymbol = new SimpleFillSymbol().setColor(new Color([0, 255, 0, 0])).setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 0, 0, 0.75]), 3));

		//create a query task
		var queryFeatures = function(geom){
			fback.html('Looking up features ...').css('color', 'green');
			def.notify('Looking up features ...');
			//clear previous Def. Exp.
			targetLyr.setDefinitionExpression();
			//zoom to the targetted area
			mapview.map.setExtent(geom.getExtent(), true);
			//build query for the area
			var query = new Query();
			query.geometry = geom;
			query.returnGeometry = false;
			query.spatialRelationship = Query[type];
			query.outFields = ['OBJECTID'];
			//run the query on the selected layer
			targetLyr.selectFeatures(query, targetLyr.MODE_ONDEMAND, handleFilterSuccess, handleFilterError);
		};
		//process the query results
		var handleFilterSuccess = function(qResults){
			var fLen = qResults.length;
			if (fLen > 0) {
				fback.html(fLen + ' feature(s) found.')
				.css('color', 'green');
				def.notify(fLen + ' feature(s) found.');
				//build the def. expression based on unique ids
				var defExp = "";
				$.each(qResults, function(idx,feat){
					if(idx != (qResults.length -1)){
						defExp += '"OBJECTID" = ' + feat.attributes.OBJECTID + ' OR ';
					} else {
						defExp += '"OBJECTID" = ' + feat.attributes.OBJECTID;
					}
				});
				//clear selection and set def. query
				targetLyr.clearSelection();
				targetLyr.setDefinitionExpression(defExp);
				renderers.updateFeatureSymbols();
				if (!targetLyr.visible) {
					targetLyr.setVisibility(true);
				}
				//rebuild table at the end
				if (widget == 'table') {
					nisis.ui.rebuildTable(targetLyr);
				}
			}
			else {
				fback.html('No Features found.')
				.css('color', 'red');
				nisis.ui.displayMessage('No features found based on the filter criteria.', 'Filter Results!', 'info');
				def.notify('No features found based on the filter criteria.');
			}
		};
		//in case of error alert the user
		var handleFilterError = function(error){
			console.log('Selection Error: ', error);
			nisis.ui.displayMessage(error.message, 'Error!', 'warn');
			def.reject(error.message);
		};
		//check for valid inputs
		if (tLyr === '') {
			fback.html('Select the target layer')
			.css('color','red');
			def.reject('Select the target layer');
			return;
		} else if (tLyr === '*') {
			fback.html('Filter one layer at the time')
			.css('color','red');
			def.reject('Filter one layer at the time');
			return;
		} else {
			if (gqps) {
				targetLyr = mapview.layers[tLyr];
				fback.html('Selecting features within ' + gqps.length + ' GQP(s) ...')
				.css('color', 'green');
				def.notify('Selecting features within ' + gqps.length + ' GQP(s) ...');
				getGeom(gLyr,gqps,union);
			} else {
				fback.html('Select at least 1 GQP')
				.css('color', 'red');
				def.reject('Select at least 1 GQP');
				return;
			}
		}
		//return def object
		def.promise();
	};
	//quick search for features
	util.searchFeatures = function(searchText,returnGeom,searchFields,table){
		var def = new $.Deferred();
		var searchUrl = nisis.url.arcgis.services;
		//var searchUrl = nisis.url.arcgis.services + "nisisv2/nisis_master_objects/MapServer";
		//var searchUrl = nisis.url.arcgis.services + "nisisv2/nisis_ans/featureserver/0";
		var fields,
		layers = {
			nisis_airports: "nisisv2/nisis_airports/MapServer",
			nisis_atm: "nisisv2/nisis_atm/MapServer",
			nisis_ans: "nisisv2/nisis_ans/MapServer",
			nisis_osf: "nisisv2/nisis_osf/MapServer",
			// NISIS-2803 KOF HONU
			nisis_tof: "nisisv2/nisis_tof/MapServer",
			//NISIS-2742 - KOFI HONU
			//nisis_resp: "nisisv2/nisis_resp/MapServer",
			ntad_amtrak: "dot/ntad_amtrak/Mapserver",
			ntad_amtrk_sta: "dot/ntad_amtrk_sta/Mapserver",
			ntad_nhpn_ushwy: "dot/ntad_nhpn_ushwy/Mapserver",
			ntad_ports: "dot/ntad_ports/Mapserver",
			ntad_rail_ln: "dot/ntad_rail_ln/Mapserver",
			ntad_rail_st: "dot/ntad_rail_st/Mapserver",
			ntad_trans_ln: "dot/ntad_trans_ln/Mapserver",
			ntad_trans_st: "dot/ntad_trans_st/Mapserver",
			hsip_terms_store_fac: "dot/hsip_terms_store_fac/Mapserver",
			hsip_oil_refinery: "dot/hsip_oil_refinery/Mapserver",
			hsip_nat_gas_comp_stations: "dot/hsip_nat_gas_comp_stations/Mapserver",
			hsip_nat_gas_liquid_pipelines: "dot/hsip_nat_gas_liquid_pipelines/Mapserver",
			ntad_nhpn_ishwy: "dot/ntad_nhpn_ishwy/Mapserver",
			pipelines_pg: "dot/npms_pipelines/Mapserver",
			pipelines_sg: "dot/npms_pipelines/Mapserver",
			pipelines_prd: "dot/npms_pipelines/Mapserver",
			pipelines_ngl: "dot/npms_pipelines/Mapserver",
			pipelines_otg: "dot/npms_pipelines/Mapserver",
			pipelines_ohv: "dot/npms_pipelines/Mapserver",
			pipelines_ng: "dot/npms_pipelines/Mapserver",
			pipelines_lpg: "dot/npms_pipelines/Mapserver",
			pipelines_hg: "dot/npms_pipelines/Mapserver",
			pipelines_eth: "dot/npms_pipelines/Mapserver",
			pipelines_epl: "dot/npms_pipelines/Mapserver",
			pipelines_epg: "dot/npms_pipelines/Mapserver",
			pipelines_crd: "dot/npms_pipelines/Mapserver",
			pipelines_co2: "dot/npms_pipelines/Mapserver",
			pipelines_aa: "dot/npms_pipelines/Mapserver"
		};

		if(table && layers[table]){
			//console.log(table);
			searchUrl += layers[table];
			fields = ["OBJECTID"];
		} else {
			// searchUrl += "nisisv2/nisis_resp/MapServer";
			// fields = ["LOOKUPID"];
			nisis.ui.displayMessage('App could not identify layerid: [' + table + ']', 'error');
		}

		var findTask = new FindTask(searchUrl),
		findParams = new FindParameters();
		findParams.layerIds = [0];
		findParams.searchText = searchText;
		findParams.returnGeometry = returnGeom;

		if(searchFields && searchFields.length && searchFields.length > 0){
			findParams.searchFields = fields;
		}

		var search = findTask.execute(findParams);
		search.then(
			//success handler
			function(results){
				//console.log(results);
				def.resolve(results);
			},
			//error handler
			function(error){
				//console.log(error);
				def.reject(error.message);
			}
		);
		return def.promise();
	};
	//autocomplete features
	util.getMatchedFeatures = function(evt){
		var input = evt.term;
		var feats = util.searchFeatures(input);
		feats.then(function(data){
			console.log(data);
			if(data && data.length && data.length > 0){
				var features = data, results = [];
				$.each(features, function(i,feat){
					results.push({
						label: feat.feature.attributes.FacName,
						source: feat.feature.attributes.SOURCETABLE,
						id: feat.feature.attributes.OBJECTID,
						category: feat.layerName
					});
				});
				return results;
			}

		},function(error){
			console.log(error.message);
		},function(p){
			console.log(p);
		});
	};
	util.getGeometryMapPoint = function(loc){
		console.log("Getting map point at:", loc);
		var lat, lon, pt
			spRef = new SpatialReference({wkid: 102100}),
			spRef2 = new SpatialReference({wkid: 4326});

		if(loc && !loc.length){
			lat = loc.y;
			lon = loc.x;
		} else if (loc && loc.length && loc.length == 2){
			lat = loc[1];
			lon = loc[0];
		}

		if (!lat || !lon) {
			return;
		}
		//create a point graphic with no fill color
		if (lat > 90 || lat < -90) {
			pt = new Point(lon,lat,spRef);
		} else {
			pt = new Point(lon,lat,spRef2);
			pt = webMercatorUtils.geographicToWebMercator(pt);
		}

		return pt;
	};
	//flash at a location
	util.flashAtLocation = function(loc, focus){
		// console.log("Flashing at:", loc);
		var lat, lon, pt
			spRef = new SpatialReference({wkid: 102100}),
			spRef2 = new SpatialReference({wkid: 4326});

		if(loc && !loc.length){
			lat = loc.y;
			lon = loc.x;
		} else if (loc && loc.length && loc.length == 2){
			lat = loc[1];
			lon = loc[0];
		}

		if (!lat || !lon) {
			return;
		}
		//create a point graphic with no fill color
		if (lat > 90 || lat < -90) {
			pt = new Point(lon,lat,spRef);
		} else {
			pt = new Point(lon,lat,spRef2);
			pt = webMercatorUtils.geographicToWebMercator(pt);
		}

		var symbol = renderers.symbols.flashPoint,
			graphic = new Graphic(pt,symbol);
		//add the point to map
		mapview.map.graphics.add(graphic);

		//flash the point to draw user intention
		var i = 0,
		flashInterval,
		flash = function(){
			i++;
			var visible = i%2;
			if(visible == 1){
				graphic.show();
			} else {
				graphic.hide();
			}
			if(i==10){
				clearInterval(flashInterval);
				mapview.map.graphics.remove(graphic);
			}
		};

		//center to location
		if (focus) {
			mapview.map.centerAt(pt)
			.then(function() {
				flashInterval = setInterval(flash, 300);
			}, function() {
				nisis.ui.displayMessage('App could not center at: [' + lon + ', ' + lat + ']', 'error');
			});
		} else {
			flashInterval = setInterval(flash, 300);
		}
	};
	//search for locations ==> using geocoding services
	util.findLocations = function(input,currExtent){
		//setup the spatial references
		mapview.locator.outSpatialReference = mapview.map.spatialReference;
		//console.log(input);
		nisis.ui.displayMessage('Geocoding "' + input + ' ..."', "Please Wait!");
		//user input
		var address = {
			'SingleLine': input
		};
		//locator options
		var options = {
			address: address,
			outFields: ['*']
		};

		if(currExtent){
			options.searchExtent = mapview.map.extent;
		}
		//
		mapview.locator.addressToLocations(options);
	};
	//display the geocoding results
	util.showFindLocResults = function(evt){
		//console.log('Geocoding result: ', arguments);
		//define infoTemplate
		var infoTemplate = new InfoTemplate("Location", "Address: ${address}<br />Score: ${score}<br />Source locator: ${locatorName}");
		//define symbol
		var symbol = new SimpleMarkerSymbol();
		symbol.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
		symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));
		//Define the font of the labels
		var font = new Font("14px", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_BOLD, "Helvetica");

		//store all geocoding candidates
		var results = [],
			graphics = [];

		if (evt.addresses.length > 0) {
			//console.log(evt.addresses[0]);
			var addresses = dojo.map(evt.addresses, function(address){
				return {
					address: address,
					fullAddress: address.address,
					city: address.attributes.PlaceName,
					county: address.attributes.Subregion,
					state: address.attributes.Region,
					country: address.attributes.Country,
					location: address.attributes.DisplayY + ' / ' + address.attributes.DisplayX,
					score: address.score
				};
			});
			//nisis.ui.createLocationsList(addresses);
			//loop through possible address
			$.each(evt.addresses, function(i, candidate){
				//clear old graphics
				if (candidate.score > 50) {
					//console.log(candidate.location);
					var attributes = {
						address: candidate.address,
						score: candidate.score,
						locatorName: candidate.attributes.Loc_name
					};
					var geom = candidate.location;
					results.push(geom);
					//location graphic
					var graphic = new Graphic(geom, symbol, attributes, infoTemplate);
					graphics.push(graphic);
					//add a text symbol to the map listing the location of the matched address.
					var displayText = candidate.address;
					//define label
					var textSymbol = new TextSymbol(displayText, font, new Color("#000"));
					textSymbol.setOffset(0, 15);
					var label = new Graphic(geom, textSymbol);
					//add label to the map
					if(mapview.layers.User_Graphics){
						mapview.layers.User_Graphics.add(graphic);
						mapview.layers.User_Graphics.add(label);
					} else {
						mapview.map.graphics.add(graphic);
						mapview.map.graphics.add(label);
					}
				}
			});

			if (results.length > 0) {
				//console.log(results.length);
				//mapview.map.setExtent(util.getGraphicsExtent(mapview.map.graphics.graphics));
				var ext = null;
				if ( graphics.length) {
					ext = graphicsUtils.graphicsExtent(graphics);
				}

				if (ext) {
					mapview.map.setExtent(ext, true);
				} else {
					mapview.map.centerAndZoom(results[0], 12);
				}
				//mapview.map.centerAndZoom(results[0], 12);
				$.each(results, function(i, loc){
					util.flashAtLocation(loc);
				});
			}
			else {
				nisis.ui.displayMessage('No creadible locations found', '', 'warn');
			}
		}
		else {
			nisis.ui.displayMessage('0 locations found', '', 'error');
		}
	};
	//display buffer results
	util.showBufferResults = function(geoms){
		//console.log(geoms);
		if(geoms){
			$.each(geoms.geometries, function(idx, geom){
				console.log(geom);
				var bufferSymbol = new SimpleFillSymbol()
					.setColor(new Color([0, 255, 50, 0.3]))
					.setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 0, 0, 0.75]), 3));
				var buffer = new Graphic(geom, bufferSymbol, null, null);
				if (buffer) {
					mapview.map.graphics.add(buffer);
					$('#createBuffer').off('click');
				}
			});
		}
	};
	//display locate results
	util.showSearchResults = function(results){
		//mapview.layers.AirportsData.hide();
		console.log(results.length);
		//console.log(results);
		var len = results.length;
		if (len > 0) {
			console.log(results[0]);
			for (var i = 0; i < len; i++) {
				if (results[i].score > 80) {
					console.log(results[i].score);
					var attr = {
						address: results[i].address,
						score: results[i].score,
						latitude: results[i].location.y,
						latitude: results[i].location.x
					};
					//console.log(attr);
					var geom = new Point(results[i].location.x, results[i].location.y, 3857);//results[i].location,
					//console.log(geom);
					var attrAll = results[i].attributes;
					//console.log(attrAll);
					var infoTemplates = new InfoTemplate("Attributes", "${*}");
					//console.log(infoTemplates);
					var graphic = new Graphic(geom, renderers.symbols.point, attrAll, infoTemplate);
					console.log(graphic);
					mapview.map.graphics.add(graphic);
				}
				else {
					console.log(results[i].score);
				}
			}
			mapview.map.setExtent(util.getGraphicsExtent(mapview.map.graphics.graphics));
		}
		else {
			console.log("Empty results");
		}
	};
	//display only find task results
	util.showFindFeaturesResults = function(results){
		//console.log(results);
		if (results && results.length > 0) {
			var len = results.length;
			//clear current graphics and features
			mapview.map.graphics.clear();

			var graphics = [];
			//loop thru result and add graphics to map
			$.each(results, function(idx, feat){
				var graphic = feat.feature;
				if ( feat.feature.geometry.spatialReference.wkid !== 102100 ) {
					var geom = webMercatorUtils.geographicToWebMercator(feat.feature.geometry);
					graphic.geometry = geom;
				}
				graphics.push(graphic);
				//console.log(graphic);
				graphic.setSymbol(renderers.symbols[feat.feature.geometry.type]);
				graphic.setAttributes(feat.feature.attributes);
				//console.log(feat.feature.attributes);
				var infoTemplate = new InfoTemplate("Attributes", "${*}");
				//graphic.setInfoTemplate(infoTemplate);
				mapview.map.getLayer('User_Graphics').add(graphic);

				if(idx === len -1){
					mapview.map.setExtent(util.getGraphicsExtent(graphics),true);
				}
			});
		}
		else {
			nisis.ui.displayMessage('No features found ...', 'No Results');
		}
	};
	//get all graphics extent
	util.getGraphicsExtent = function(graphics){
		var geometry, extent, ext;
		dojo.forEach(graphics, function(graphic, i){
			geometry = graphic.geometry;

			if (geometry instanceof esri.geometry.Point) {
				ext = new Extent(geometry.x - 1, geometry.y - 1, geometry.x + 1, geometry.y + 1, geometry.spatialReference);
			}
			else
				if (geometry instanceof esri.geometry.Extent) {
					ext = geometry;
				}
				else {
					ext = geometry.getExtent();
				}

			if (extent) {
				extent = extent.union(ext);
			}
			else {
				extent = new Extent(ext);
			}
		});
		return extent;
	};
	util.getGeometriesExtent = function(geoms){
		var extent, ext;
		if(geoms && geoms.length > 0){
			$.each(geoms,function(i, geometry){
				//console.log(geometry);
				if (geometry instanceof esri.geometry.Point) {
					ext = new Extent(geometry.x - 1, geometry.y - 1, geometry.x + 1, geometry.y + 1, geometry.spatialReference);
				}
				else if (geometry instanceof esri.geometry.Extent) {
					ext = geometry;
				}
				else {
					ext = geometry.getExtent();
				}

				if (extent) {
					extent = extent.union(ext);
				}
				else {
					extent = new Extent(ext);
				}
			});
			return extent;
		}
	};

	var getHexColor = function(col){
		var parts = col.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		delete (parts[0]);
		for (var i = 1; i <= 3; ++i) {
			parts[i] = parseInt(parts[i], 10).toString(16);
			if (parts[i].length == 1) {
				parts[i] = '0' + parts[i];
			}
		}
		var color = '#' + parts.join('');
		//console.log(color);
	};
	var getRgbColor = function(col){
		//console.log(col);
		var start = col.indexOf('(') + 1;
		col = col.substr(start);
		var end = col.indexOf(')');
		col = col.substr(0, end);
		col = col.split(',');
		var color = [];
		for (var i = 0; i < col.length; i++) {
			color.push(parseInt(col[i], 10));
		}
		//console.log(col);
		return color;
	};
	//create geometry
	util.createGeometry = function(options, app){
		//console.log('Create Geometry options: ', app, options);
		if(!options){
			return;
		}

		var geoms = [],
			gOpt = null,
			sref = new SpatialReference({ wkid: 4326 }), //102100
			app = app ? app : "";

		//check for the type of shapes
		if(options.type === 'point') {
			//console.log(options.data);
			$.each(options.data, function(i, pt){
				var geom = new Point(pt, sref);
				geom = webMercatorUtils.geographicToWebMercator(geom);
				geoms.push({geom: geom});
			});

		} else if (options.type === 'polyline') {
			var geom = new Polyline(options.data, sref);
			geom = webMercatorUtils.geographicToWebMercator(geom);
			geoms.push({geom: geom});

		} else if (options.type === 'polygon') {
			var pts = options.data;
			pts.push(options.data[0]);
			var geom = new Polygon(options.data, sref);
			geom = webMercatorUtils.geographicToWebMercator(geom);
			geoms.push({geom: geom});

		} else if (options.type === 'circle' && options.radius) {
			//radius are in meters
			$.each(options.data, function(i, pt) {
				/*var geom = new Point(pt, sref);
				geom = webMercatorUtils.geographicToWebMercator(geom);
				geom = new Circle(geom, {
					radius: options.radius
					,radiusUnit: esri.Units.METER
					//,geodesic: false
					,spatialReference: sref
				});
				geoms.push({geom: geom});*/

				//let try LatLon bc arcgis js api does not convert well
				var center = new LatLon(pt[1], pt[0]),
					pts = [],
					npts = 360,
					brg = 0,
					r = options.radius / 1000, //convert meters to km
                    start = center.destinationPoint(brg, r);

                //add the first point
                pts.push([start.lon, start.lat]);

                for (var i = 0; i < npts; i++) {
                    brg++;

                    var curvePt = center.destinationPoint(brg, r);

                    pts.push([curvePt.lon, curvePt.lat]);
                }
                //add the first point to close the circle
                pts.push([start.lon, start.lat]);

                var geom = new Polygon(pts, sref);
                geom = webMercatorUtils.geographicToWebMercator(geom);
				geoms.push({
					geom: geom,
					options: {
						type: 'circle',
						radius: options.radius,
						center: pt
					}
				});
			});

		} else if ( options.type === 'pie' ) {
			//console.log('Pie Geom:', options);
			var pts = options.data;
			pts.push(options.data[0]);
			var geom = new Polygon(options.data, sref);
			geom = webMercatorUtils.geographicToWebMercator(geom);
			geoms.push({
				geom: geom,
				options: {
					type: options.type,
					center: options.piec,
					radius: options.pier,
					pointa: options.piea,
					pointb: options.pieb
				}
			});

		} else {
			nisis.ui.displayMessage(options.type + ' is not yet implemented', 'error');
		}

		//prepare data
		$.each(geoms, function(i, geom) {

			if ( options.buffer && options.radius ) {
				//add original shape
				if ( options.keepOriginal ) {

					if (app === "") {
						util.addGraphicToMap(geom.geom);
					} else {
						util.addGraphicToUserLayers(geom.geom, app);
					}
				}
				//generate buffer shape
				util.createGeometryBuffer([geom.geom], options.radius)
				.then(function(geometries) {

					if ( !geometries || !geometries.length ) {
						console.log('Create Geometry buffer failed: ', geometries);
						return;
					}

					$.each(geometries, function(i, g) {
						if (app === "") {
							util.addGraphicToMap(g);

						} else {
							var opts = {
								type: 'buffer',
								radius: options.radius,
								center: geom.geom
							};

							util.addGraphicToUserLayers(g, app, opts);
						}

						geoms.push({geom: g});
					});

					var extents = $.map(geoms, function(g) {
						return g.geom;
					});

					mapview.map.setExtent(util.getGeometriesExtent(extents).expand(1.2), true);

				}, function(err) {
					//console.log('Create Geometry buffer error: ', err);
					nisis.ui.displayMessage(err, 'err');
				});

			} else {

				if (app === "") {
					util.addGraphicToMap(geom.geom);

				} else {
					var g = geom.geom,
						opt = geom.options ? geom.options : null;
					util.addGraphicToUserLayers(g, app, opt);
				}
			}
		});

		//zoom to graphics extend
		if (geoms.length) {
			var extents = $.map(geoms, function(g) {
				return g.geom;
			});

			mapview.map.setExtent(util.getGeometriesExtent(extents).expand(1.2), true);
		}
	};
	//Draw feature on the map
	util.addGraphicToMap = function(geom, ops){
		//console.log(geom);
		//reset click evt mapPoint to null
		mapview.draw.geomCenter = null;
		//detect when to add textSymbol
		var textGraphic = false;
		//add shapes to corresponding feature layer type
		var glyr = 'graphics', //'User_Graphics',
			graphicLayer = mapview.map.getLayer(glyr);

		if(!graphicLayer && mapview.map.graphics) {
			graphicLayer = mapview.map.graphics;
		}
		//console.log(geom);
		mapview.draw.tools.deactivate();
		nisis.ui.enableZoomBoxes();
		//create shape symbol
		var symbol = null;

		if (geom.type == 'point') {
			symbol = renderers.symbols.point;

		} else if (geom.type == 'polyline') {
			symbol = renderers.symbols.polyline;

		} else if (geom.type == 'polygon') {
			symbol = renderers.symbols.polygon;

			if (ops) {
				symbol = renderers.symbols[ops];
			}

		} else {

			textGraphic = true;
			var text = $('#label-text').val();
			var fSize = $('#font-size').val() + 'pt';
			var font, ftColor,
				fStyle, fDeco, fWeight,
				fVariant = Font.VARIANT_NORMAL,
				fFamily = $('#label-font').val();

			ftColor = $('#font-color').css('background-color');
			ftColor = getRgbColor(ftColor);
			ftColor.push(parseFloat($('#font-trans').val(), 10));

			if ($('#font-bold').is(':checked')) {
				fWeight = esri.symbol.Font.WEIGHT_BOLD;
			}
			else {
				fWeight = esri.symbol.Font.WEIGHT_NORMAL;
			}

			if ($('#font-italic').is(':checked')) {
				fStyle = esri.symbol.Font.STYLE_ITALIC;
			}
			else {
				fStyle = esri.symbol.Font.STYLE_NORMAL;
			}

			if ($('#font-underline').is(':checked')) {
				fDeco = esri.symbol.TextSymbol.DECORATION_UNDERLINE;
			}
			else {
				fDeco = esri.symbol.TextSymbol.DECORATION_NONE;
			}

			if (text.length > 0) {
				font = esri.symbol.Font(fSize, fStyle, fVariant, fWeight, fFamily);
				symbol = new TextSymbol(text);
				symbol.setFont(font);
				symbol.setColor(new dojo.Color(ftColor));
				//text decoration not supported in IE7, FF, and Opera 9
				symbol.setDecoration(fDeco);
			}
		}
		//mapview.navTools.activate(esri.toolbars.Navigation.PAN);


		var infoWindow = new InfoTemplate('${Label}', '${*}');
		var id = graphicLayer.graphics.length + 1;
		var graphicAttributes = {
			Type: geom.type,
			Label: geom.type,
			Name: geom.type + ' Graphic ' + id
		};

		var graphic;

		if (textGraphic) {
			graphic = new Graphic(geom, symbol, null, null);
		}
		else {
			graphic = new Graphic(geom, symbol);
		}

		var newGraphic;
		//try to add graphic to map graphics layer
		try {
			newGraphic = graphicLayer.add(graphic);
		}
		catch (err) {
			console.log('Add graphic to map failed: ', err);
		}
		// Do something with the graphic ==> cut other geoms or extract data
		if ( newGraphic ) {

			if (ops && ops.toLowerCase() === "cut") {
				geomUtils.cutGeometries(graphic);
			}
			else if (ops && ops.toLowerCase() === "extract") {
				geomUtils.extractData(graphic);
			}

		}
		else {
			var msg = "App could not add your shape to the map. Try again.";

			nisis.ui.displayMessage(msg, 'error');
		}
		//deactivate draw tool
		util.deactivateDraw();
	};
	util.getCurrentTFRShapeIds = function() {
		return tfrShapesGrid.getCurrentTFRShapeIds();
	};
	util.getCurrentTfrNotamId= function(){
		return tfrBuilder.getCurrentTfrNotamId();
    };
    util.updateCurrentTFRShapeAttribute = function(shapeId, attributeName, attributeValue) {
		tfrBuilder.updateCurrentTFRShapeAttribute(shapeId, attributeName, attributeValue);
	};
	util.updateFeatureManagerShapeName = function(shapeId, itemType, attributeName, attributeValue) {
		features.updateFeatureManagerShapeName(shapeId, itemType, attributeName, attributeValue);
	};
	util.updateCurrentTfrVor = function(shapeId, newVor) {
		//console.log("updateCurrentTfrVor:", shapeId, newVor);
		tfrBuilder.updateCurrentTfrVor(shapeId, newVor);
	};
	//this function is used by custom feature templates
	util.addGraphicToUserLayers = function(geom, app, opt) {
		if(!geom || !app){
			return;
		}

		var layer = geom.type,
		target = {
			"nisis": {
				text: mapview.layers.text_features,
				point: mapview.layers.Point_Features,
				polyline: mapview.layers.Line_Features,
				polygon: mapview.layers.Polygon_Features
			},
			"tfr": {
				point: mapview.layers.tfr_point_features,
				polyline: mapview.layers.tfr_line_features,
				polygon: mapview.layers.tfr_polygon_features
			}
		};

		if (app === "nisis") {
			//console.log("in nisis mode");
			//changed shape size of point features on map
			var symObjs = target.nisis.point.renderer._symbols;
			for (var key in symObjs) {
			  	if (symObjs.hasOwnProperty(key)) {
			    	symObjs[key].symbol.setSize(30);
			  	}
			}
		}
		else if(app === "tfr"){
			//console.log("in tfr mode");
		}

		var tool = shapetools.featureTool,
			symbol = shapetools.featureSymbol,
			symbolValue = shapetools.featureSymbolValue,
			date = nisis.ui.formatDateTime(new Date());

		if ( !$.isNumeric(symbolValue) ) {
			symbolValue = 0;
		}

		var vertices = {
			'polygon': 'rings',
			'polyline': 'paths'
		};

		var attr = {
			"nisis": {
				NAME: geom.type.toUpperCase() + " - " + date,
				//REF_NUMBER: "",
				REF_NUMBER: (opt!=null && opt.reference!=null)?opt.reference:"",
				DESCRIPTION: geom.type + ' feature',
				SUP_NOTES: geom.type,
				//SYMBOL: symbolValue,
				//NISIS-2222: KOFI - commented out
				//USER_GROUP: '9999'
			},
			"tfr": {
				NAME: geom.type.toUpperCase() + " - " + date,
				TFR_SHAPE: 'base',
				FLOOR: '0',
				IN_FLOOR: 'yes',
				FL_TYPE: 'agl',
				CEILING: '0',
				IN_CEIL: 'yes',
				CEIL_TYPE: 'agl',
				GEOM_TYPE: geom.type,
				USER_GROUP: 9999
				//test
			}
		};
		//set filter shapes
		if(geom.type === 'polygon' && app === 'nisis') {
			attr[app].FS_DESIGN = 1;
			attr[app].IWA_DESIGN = 0;
		}
		//overrite all attributes for text
		if(symbol && symbol.type && symbol.type === 'textsymbol') {
			layer = 'text';
			attr['nisis'] = {
				LABEL: 'FREE TEXT LABEL',
				SYMBOL: 0,
				//NISIS-2222: KOFI - commented out
				//USER_GROUP: 9999
			};

		} else {
			var g = webMercatorUtils.webMercatorToGeographic(geom);

			if(g && g.type === 'point') {
				attr[app]['VERTICES'] = "(" + g.x + ", " + g.y + ")";

			} else {

				var str = appUtils.arrayToString(g[vertices[g.type]]),
					strL = str ? str.length : 0;
				/**
				 * VERTICES field is set for 254 chars
				 * Check to make sure the value is not greater than the max
				 */
				if (strL > 250) {
					str = appUtils.arrayToString(g[vertices[g.type]], 5);
					strL = str ? str.length : 0

					if (strL > 250) {
						str = appUtils.arrayToString(g[vertices[g.type]], 1);
						strL = str ? str.length : 0

						if (strL > 250) {
							str = "";
						}
					}
				}
				//update vertices info
				if (str) {
					attr[app]['VERTICES'] = str;
				}

				//overrite polygon attr
				if ( g.type === 'polygon' && opt && opt.type) {
					//opt contain custom flags from vertices
					if (opt.type == 'circle') {

						str = "lat:";
						str += opt.center[1];
						str += ", ";

						str += "lon:";
						str += opt.center[0];
						str += ", ";

						str += "radius: ";
						str += util.convertUnits(opt.radius, 'me', 'nm');
						str += "nm";

						attr[app]['VERTICES'] = str;
						//TFR Needs to know the geometry type
						if (app === 'tfr') {
							attr[app]['GEOM_TYPE'] = 'Circle';
						}

					} else if (opt.type == 'buffer') {
						var center = webMercatorUtils.webMercatorToGeographic(opt.center),
							type = center.type,
							geomType = 'buffered ' + type;

						if (type == 'point') {
							geomType = 'Circle'; //Buffering a point will return a circle

							str = "lat:";
							str += center.y.toFixed(5);
							str += ", ";

							str += "lon:";
							str += center.x.toFixed(5);
							str += ", ";

							str += "radius: ";
							str += util.convertUnits(opt.radius, 'me', 'nm');
							str += "nm";
						}
						else if (type == 'polyline') {
							var str = "",
								radius = util.convertUnits(opt.radius, 'me', 'nm').toFixed(1),
								vertices = center[vertices[type]][0];

							$.each(vertices, function(i, v) {
								str += 'V' + (i+1) + ":";
								str += v[1].toFixed(5) + "/" + v[0].toFixed(5);
								str += ", ";
							});

							str += "Radius:" + radius;
							str += "nm";

							//console.log('Buffered polyline: ', str);
						}
						else if (type == 'polygon') {
							var str = "",
								radius = util.convertUnits(opt.radius, 'me', 'nm').toFixed(1),
								vertices = center[vertices[type]][0];

							$.each(vertices, function(i, v) {
								str += 'V' + (i+1) + ":";
								str += v[1].toFixed(5) + "/" + v[0].toFixed(5);
								str += ", ";
							});

							str += "Radius:" + radius;
							str += "nm";

							//console.log('Buffered polygon: ', str);
						} else {
							nisis.log('Unknown buffered center shape type', opt);
						}
						//update the geom type for tfr only
						if (app === 'tfr') {
							attr[app]['GEOM_TYPE'] = geomType;
						}

					} else if (opt.type == 'pie') {

						str = "Center: ";
						str += opt.center[1].toFixed(5);
						str += "/";
						str += opt.center[0].toFixed(5);

						str += ", PointA: ";
						str += opt.pointa[1].toFixed(5);
						str += "/";
						str += opt.pointa[0].toFixed(5);

						str += ", PointB: ";
						str += opt.pointb[1].toFixed(5);
						str += "/";
						str += opt.pointb[0].toFixed(5);

						str += ", Radius: ";
						str += opt.radius.toFixed(1);

						if (app === 'tfr') {
							attr[app]['GEOM_TYPE'] = 'Arc';
						}
					} else {
						nisis.log('Unknown buffered shape type', opt);
					}

					//update vertices info
					if (str) {
						attr[app]['VERTICES'] = str;
					}

				} else if ( g.type === 'polygon' && tool && tool === 'circle' ) {
					var center = null,
						lat = null,
						lon = null,
						edge = null,
						r = 0;

					if (g.center) {
						center = center;

					} else {
						center = g.getCentroid();

						if (center && center.x && center.y) {
							lat = center.y;
							lon = center.x;
							center = [center.x, center.y];
							//edge = g.rings[0][0];
							edge = g.getPoint(0,0);
							edge = [edge.x, edge.y];

						} else {
							center == null;
						}
					}

					if (!center || !edge) return;

					var c = new LatLon(center[1], center[0]),
						e = new LatLon(edge[1], edge[0]),
						r = e.distanceTo(c);

					if (!r || !$.isNumeric(r) ) return;

					str = "lat:";
					str += center[1].toFixed(5);
					str += ", ";

					str += "lon:";
					str += center[0].toFixed(5);
					str += ", ";

					str += "radius: ";
					str += util.convertUnits(r, 'km', 'nm').toFixed(1); //LatLon & distanceTo result is in km
					str += "nm";

					attr[app]['VERTICES'] = str;
					//TFR Needs to know the geometry type
					if (app === 'tfr') {
						attr[app]['GEOM_TYPE'] = 'Circle';
					}
				}
			}
		}
		//create graphic
		var graphic = new Graphic(geom, symbol, attr[app]);

		var graphicName = attr[app]['NAME'];

		if(target[app] && target[app][layer]){
			var layerid = target[app][layer].id;

			target[app][layer].applyEdits([graphic])
			.then(function(adds, deletes, updates) {

				if ( !adds || !adds.length || !adds[0].success) {
					console.log('Apply Edits Failure: ', arguments);
					var msg = "App could not add your shape to " + layerid;
					nisis.ui.displayMessage(msg, 'error');
				}
				else {
					//save order
					var objid = adds[0].objectId;
					var itemtype = null;
					if (appConfig.app == "nisis") {
						itemtype = features.getItemTypeByLayerId(layerid);
                    	shapesManager.createShape(objid,itemtype,graphic,symbolValue);
                    } else if (appConfig.app == "tfr") {
                    	//if tfr layer only
						//set tfr polygon shape opacity using shape type
						var tfrShapeType = document.getElementById("dijit_form_FilteringSelect_15");
						/*
						 * BK: This will be executed each time a shape is created.
						 * There is no need to repeat this if using a renderer object.
						 */
						/*if (tfrShapeType === "Add area" || "Base area" || "Subtract area" || "None") {
							//commented out shape fill color
							//target.tfr.polygon.renderer._symbols.add.symbol.setColor(new Color([211,211,211,0.5]));
							target.tfr.polygon.renderer._symbols.add.symbol.outline.setColor(new Color([211,211,211,1]));

							//target.tfr.polygon.renderer._symbols.base.symbol.setColor(new Color([211,211,211,0.5]));
							target.tfr.polygon.renderer._symbols.base.symbol.outline.setColor(new Color([211,211,211,1]));

							//target.tfr.polygon.renderer._symbols.subtract.symbol.setColor(new Color([211,211,211,0.5]));
							target.tfr.polygon.renderer._symbols.subtract.symbol.outline.setColor(new Color([211,211,211,1]));

							//target.tfr.polygon.renderer._symbols.none.symbol.setColor(new Color([211,211,211,0.5]));
							target.tfr.polygon.renderer._symbols.none.symbol.outline.setColor(new Color([211,211,211,1]));
						}*/
						//add new shape to tfr shapes grid
						var objid = adds[0].objectId;
						//add it to the grid
						var shapesArray  = tfrShapesGrid.add(objid, graphicName);
						//update vertices for the all shapes in the area where we added shapes
						tfrBuilder.updateAreaShape({}, shapesArray);

						//Reset the active shapes
						renderers.tfrActiveShapes.push(objid);
					}

				}

				// nisis.ui.mapview.map.setExtent(nisis.ui.mapview.map.extent);

				util.deactivateDraw();

				//new tfr - set opacity/color of each shape///////////////
				//BK: 01/08/2016 => These styles won't last. Any refresh of the layer will remove them.

				//function newTfrPolygonColorChange(){
					/*var shapeSelected = target.tfr.polygon._selectedFeatures;

					//array of current tfr polygon object Ids
					var currentPolygonsArray = tfrShapesGrid.getCurrentTFRShapeIds();

					//array of all polygon graphics on the map
					var graphicObjectsArray = target.tfr.polygon.graphics;

					//array for found matching graphics
					var graphicObjectIdMatchedArray = [];

					//if shape is selected then run function of displaying active shape color/opacity
					if (Object.keys(shapeSelected).length === 1){

						var tfrId = $('[data-bind="text:tfrTitle"]').text().slice(4,$('[data-bind="text:tfrTitle"]').text().indexOf(" "));
						tfrId;

						//loop through both arrays and push the matched objects into graphicObjectIdMatchedArray
						for(var i in graphicObjectsArray){
							for(var x in currentPolygonsArray){
								if(currentPolygonsArray[x] === graphicObjectsArray[i].attributes.OBJECTID){
									graphicObjectIdMatchedArray.push(graphicObjectsArray[i]);
								}
							}
						}

						//loop through graphicObjectIdMatchedArray to change the color/opactiy of those selected ones
						for(var f in graphicObjectIdMatchedArray){
							if(graphicObjectIdMatchedArray.length < 2){
								graphicObjectIdMatchedArray[f].symbol.setColor(new Color([0,255,255,.5]));
							} else if(graphicObjectIdMatchedArray.length > 1){
								var storeSymbol = graphicObjectIdMatchedArray[graphicObjectIdMatchedArray.length - 1].symbol;
								console.log("storeSymbol:", storeSymbol);
								if(graphicObjectIdMatchedArray[f].symbol === null){
									graphicObjectIdMatchedArray[f].setSymbol(storeSymbol);
									//console.log("graphicObjectIdMatchedArray:::", graphicObjectIdMatchedArray);
								}
							}
						}
					} else{
						console.log("selectedFeatures !=== 1");
					}*/
				//}

			}, function(e){
				console.log('Apply Edits Failure: ', e);
				nisis.ui.displayMessage(e.message, 'error');
				util.deactivateDraw();
			});

		} else {
			nisis.ui.displayMessage('Unable to identify ' + geom.type + "' layer", "error");
		}
	};

	//NISIS-2400 - KOFI - modified for createShape
	//copy features to User Feature layers
	util.addFeatureToUserLayers = function(feature, lyr, operation) {
		console.log('Copying shape');
		//validate arguments
		if(!feature){
			return;
		}
		//create a def object
		var def = new $.Deferred(),
		target = {
			'nisis': {
				text: mapview.layers.text_features,
				point: mapview.layers.Point_Features,
				polyline: mapview.layers.Line_Features,
				polygon: mapview.layers.Polygon_Features
			},
			'tfr': {
				point: mapview.layers.tfr_point_features,
				polyline: mapview.layers.tfr_line_features,
				polygon: mapview.layers.tfr_polygon_features
			}
		},
		attrs = {
			'nisis': {
				NAME: 'Copy ###',
				REF_NUMBER: "",
				DESCRIPTION: 'Feature copied from : ',
				SUP_NOTES: '',
				//SYMBOL: 0,
				//NISIS-2222: KOFI - commented out
				//USER_GROUP: 9999
			},
			'tfr': {
				NAME: 'Copy ###',
				DESCRIPT: 'Feature copied from : ',
				TFR_SHAPE: null, //feature.attributes.TFR_SHAPE ? feature.attributes.TFR_SHAPE : 'none',
				FLOOR: 0,
				IN_FLOOR: 'yes',
				FL_TYPE: 'agl',
				CEILING: 0,
				IN_CEIL: 'yes',
				CEIL_TYPE: 'agl',
				GEOM_TYPE: null, //feature.attributes.GEOM_TYPE ? feature.attributes.GEOM_TYPE : "",
				VERTICES: null, //feature.attributes.VERTICES ? feature.attributes.VERTICES : "",
				USER_GROUP: 9999
			}
		},
		geom = feature.geometry,
		attr = feature.attributes,
		symbol = feature.symbol;

		var graphic = null,
			layer = feature.getLayer(),
			app = appConfig.app,
			group = layer && layer.group ? layer.group : null,
			oidToCopy = attr.OBJECTID;

		console.log("oidToCopy", attr.OBJECTID);

		//some feature don't have a layer attached to them
		if (lyr) {
			layer = lyr;
		}

		//check for valid layer and app
		if ( !layer || !app ) {
			var msg = 'App could not identify the target layer';
			nisis.ui.displayMessage(msg, 'error');
			return;
		}

		//keep attr and symbol if copying from same layer
		if(app && target[app] && target[app][geom.type] && target[app][geom.type].id && layer && layer.id === target[app][geom.type].id) {
			//get attributes
			if (!attr) {
				attr = attrs[app];
			}
			//update shape name
			if(attr.NAME) {
				attr.NAME = 'Copy of ' + attr.NAME;
			}

		} else {
			attr = attrs[app];
		}

		if ( geom.type == 'polygon' && app == 'nisis' ) {
			attr.FS_DESIGN = 1;
			attr.IWA_DESIGN = 0;
		}

		if ( attr.DESCRIPTION && app == 'nisis' ) {
			attr.DESCRIPTION += layer.name ? layer.name : layer.id;
		}

		if ( attr.DESCRIPT && app == 'tfr' ) {
			attr.DESCRIPT += layer.name ? layer.name : layer.id;
			attr.TFR_SHAPE = feature.attributes && feature.attributes.TFR_SHAPE ? feature.attributes.TFR_SHAPE : 'none'
			attr.GEOM_TYPE += feature.attributes && feature.attributes.GEOM_TYPE ? feature.attributes.GEOM_TYPE : "";
			attr.VERTICES += feature.attributes && feature.attributes.VERTICES ? feature.attributes.VERTICES : "";
		}

		//simplify source geometry
		mapview.geometryService.simplify([geom]
		,function(geoms) {
			//check for valid geoms
			if (!geoms || (geoms && !geoms.length) ) {
				def.reject('Geometry simplification operation failed.');
			}
			//get the update version of the attributes
			var gAttr = attr;
			//
			if (operation) {
				//update name
				gAttr['NAME'] = operation + " - " + Date.parse(new Date());
				//update description
				gAttr['DESCRIPT'] = operation + " result from: " + (layer.name ? layer.name : layer.id);
				gAttr['DESCRIPTION'] = operation + " result from: " + (layer.name ? layer.name : layer.id);
			}
			//create a graphic using the simplified geom
			graphic = new Graphic(geoms[0], symbol, gAttr);

			//add new shape to the target layer
			target[app][geom.type].applyEdits([graphic], null, null)
			.then(function(adds, deletes, updates) {
				
				//call create shape stored proc
				var objid = adds[0].objectId;
				if (appConfig.app == "nisis") {
				    if (oidToCopy != null) {
						var ptSymbolValue = null;
						if (geom.type == 'point'){
							var pointInFM = features.getItemByObjectId(oidToCopy, graphic.geometry.type);
							if (pointInFM  != null && pointInFM.STYLE != 'undefined') {
								var ptType = pointInFM.STYLE.PT_TYPE;
								ptSymbolValue = paths[ptType].val;
							}
						}
	                   	shapesManager.createShape(objid, graphic.geometry.type, graphic,ptSymbolValue);
                    }
                   else {
                   		shapesManager.createShape(objid, graphic.geometry.type, graphic, shapetools.featureSymbolValue);
                   }
                }

				def.resolve(adds);

			},function(e){
				def.reject(e);
			});

		},function(error) {
			def.reject(error);
		});

		//return def obj
		return def.promise();
	};


//copy features to User Feature layers
	util.FeatureLayerSelectionComplete = function(evt){
		console.log('FeatureLayerSelectionComplete: ', evt);
		//irena
			console.log('FeatureLayerSelectionComplete:  mapElements.featureEditor._currentGraphic is: ', nisis.ui.mapview.featureEditor._currentGraphic );

			console.log('FeatureLayerSelectionComplete: dojo.byId("map_infowindow")', dojo.byId("map_infowindow"));

			// var orderedFeatures = [];
			// if (nisis.ui.mapview.map.infoWindow.features) {
			// 	 for(var i = 0; i< nisis.ui.mapview.map.infoWindow.features.length; i++) {
   //          	console.log('FeatureLayerSelectionComplete:nisis.ui.map.infoWindow.features', nisis.ui.mapview.map.infoWindow.features);
   //              if ( nisis.ui.mapview.map.infoWindow.features[i]._layer.id === 'Polygon_Features') {
   //                  orderedFeatures.unshift( nisis.ui.map.infoWindow.features[i]);
   //              }
   //              else {
   //                  orderedFeatures.push( nisis.ui.mapview.map.infoWindow.features[i]);
   //              }
   //          }
			// }

   //          nisis.ui.map.infoWindow.features = orderedFeatures;

			       //brings to front
         	// if (evt.features.length==1) {

         	// 	//nisis.ui.mapview.featureEditor._currentGraphic = evt.features[0];
         	// 	var graphic = evt.features[1];
		        //  	var layer = nisis.ui.mapview.map.getLayer('Polygon_Features');
		        //  	console.log("selectFeatures ", graphic);
		        //  	layer.selectFeatures(graphic);
         	// }

	function flipObject(obj){
		var newobj = {};
	    var arr = [];

	   	var t1 = Object.keys(obj);

	 	var t2 =Object.getOwnPropertyNames(obj);


		var objIrena = {};
		objIrena[0] = obj["14081"];
		objIrena[1] = obj["13729"];
		newobj=objIrena;

	    return newobj;
	}
		//more then 1 feature selected
		if (evt.features.length>1) {

			var layer = nisis.ui.mapview.map.getLayer('Polygon_Features');

         	console.log('FeatureLayerSelectionComplete: selected features', layer._selectedFeatures);

			//FeatureLayer.SELECTION_NEW
			//var feats = layer.getSelectedFeatures();
			//console.log('FeatureLayerSelectionComplete: selected features - feats', feats);

            //var reversed = flipObject(layer._selectedFeatures);
           // layer._selectedFeatures = reversed;

            //nisis.ui.mapview.map.infoWindow.onSetFeatures();
                        //nisis.ui.mapview.map.infoWindow.select(1);//irena

            var attrInsp = nisis.ui.mapview.featureEditor.attributeInspector;

		     		// mapElements.featureEditor.attributeInspector.refresh();
					// for (var i=0; i<=attrInsp._selection.length; i++) {
					// 	if (attrInsp && attrInsp._selection[i] && attrInsp._selection[i].hasOwnProperty("attributes") == true) {
					// 	    var objectId = attrInsp._selection[i].attributes.OBJECTID;
					// 	    console.log("updateInfoWindow shape name ", attrInsp._selection[i].attributes.NAME);
					// 	}
					// }
		}
	};

	//copy features to User Feature layers
	util.toggleEditing = function(evt){
		//console.log('toggleEditing: ', evt);

        //check if its FM read only feature
        if (nisis.ui.isReadOnlyFeature(evt.graphic.attributes.OBJECTID, evt.graphic._layer.id)) {
        	evt.stopPropagation();
        	evt.preventDefault();
        }
        //irena for now....
        //else nisis.ui.mapview.irenaCreateAttributeInspector(evt.graphic._layer, evt.graphic, evt.screenPoint);
	};


	// NISIS-2754 - KOFI HONU
	//copy features to User Feature layers
	// util.addFeatureToRespTeamsLayer = function(geom){
	// 	//console.log('Response Team: ', geom);
	// 	var g = webMercatorUtils.webMercatorToGeographic(geom);
	// 	var layer = mapview.map.getLayer('teams');
	// 	var attr = {
	// 		RES_ID: 'T-XYZ',
	// 		LG_NAME: 'Team ABC',
	// 		LATITUDE: g.y,
	// 		LONGITUDE: g.x,
	// 		RES_STS: 0,
	// 		DESCRIPT: 'Response Team',
	// 		SYMBOLOGY: 0,
	// 		NCI_OB_CAT: 'Response Resources'
	// 	};

	// 	var graphic = new Graphic(geom, null, attr);

	// 	//Make sure to load the layer if not already
	// 	if (!layer && mapview.layers.teams) {
	// 		mapview.map.addLayer(mapview.layers.teams);
	// 		layer = mapview.map.getLayer('teams');
	// 	}

	// 	if(layer){
	// 		//turn resp teams layers on
	// 		if(!layer.visible){
	// 			layer.setVisibility(true);
	// 		}
	// 		//add new team to layer
	// 		layer.applyEdits([graphic],null,null)
	// 		.then(function(d){
	// 			//console.log(d);
	// 			if(d[0].success){
	// 				util.deactivateDraw();
	// 			} else {
	// 				var msg = 'An error occured while adding a new response team.';
	// 				nisis.ui.displayMessage(msg, '', 'error');
	// 			}
	// 		},function(e){
	// 			console.log(e);
	// 			nisis.ui.displayMessage('Response Team error: ' + e.message, '', 'error');
	// 		});

	// 	} else {
	// 		var msg = 'App could not add a new team to Response Team layer. Layer does not seem to be loaded.';
	// 		nisis.ui.displayMessage(msg, '', 'error');
	// 	}
	// };
	//remove features to User Feature layers
	util.removeFeatureFromUserLayers = function(feature, dontAsk){
	    console.log("removeFeatureFromUserLayers", feature);
		if (!feature) {
			return;
		}

		//BK: Use this to track when the delete was really successful
		var def = new $.Deferred();

		var layer = feature.getLayer(),
			rmLyrGrps = ['user', 'tfr'];

		//delete only user features or map graphics
		if (layer.group && $.inArray(layer.group, rmLyrGrps) == -1 ) {

			var msg = 'Features from ' + layer.name + ' cannot be deleted';
			if (nisis.user.name) {
				msg += " by " + nisis.user.name;
			}

			nisis.ui.displayMessage(msg, 'error');
			return;
		}

		//no Confirm dialog
		if (dontAsk) {
			if(layer.applyEdits){
				if (layer.id == 'Polygon_Features' || layer.id == 'Point_Features' || layer.id == 'Line_Features' || layer.id == 'text_features') {
					    var featName = feature.attributes.NAME;
					    if (layer.id == 'text_features') {
					    	featName = feature.attributes.LABEL;
					    }
						featuresManager.deleteShape(feature.attributes.OBJECTID, featName, dontAsk, feature.geometry.type);
						//delete Shape will handle errors, so resolved as no errors
						def.resolve("Graphic removed");
				}
				else {
					layer.applyEdits(null,null,[feature])
					.then(function(d){
						def.resolve(d);
					},function(e){
						console.log(e);
						def.reject(e);
					});
				}
				return def.promise();
			} else {
				layer.remove(feature);
				def.resolve("Graphic removed");
			}

			return;
		}


		//dialog
		var confirm = $('<div id="confirm-feat-delete"></div>');
		msg = $("<p>Are your sure you want to remove this feature?</p>");
		var yes = $('<button class="k-button marg">Remove</button>');
		yes.click(function(){
			dialog.close();
			if(layer.applyEdits){
				if (layer.id == 'Polygon_Features' || layer.id == 'Point_Features' || layer.id == 'Line_Features' || layer.id == 'text_features') {
					    //already asked, dont ask again
					    var featName = feature.attributes.NAME;
					    if (layer.id == 'text_features') {
					    	featName = feature.attributes.LABEL;
					    }
					    var itemType = featuresManager.getItemTypeByLayerId(layer.id);
						featuresManager.deleteShape(feature.attributes.OBJECTID, featName, true, itemType);
						//delete Shape will handle errors, so resolved as no errors
						def.resolve("Graphic removed");
						dialog.destroy();

				}
				else {
					layer.applyEdits(null,null,[feature])
					.then(function(d){
						def.resolve(d);
						dialog.destroy();
					},function(e){
						console.log(e);
						def.reject(e);
					});
				}
			}else{
				layer.remove(feature);
				def.resolve("Graphic removed");
				dialog.destroy();
			}
		});
		var no = $('<button class="k-button">Cancel</button>');
		no.click(function(){
			def.resolve("Action canceled");
			dialog.close();
			dialog.destroy();
			return;
		});

		confirm.append(msg,yes,no);

		$('#app-content').append(confirm);

		$(confirm).css({
			'zindex':99999
		});
		var dialog = confirm.kendoWindow({
			title: 'Confirm Action!',
			visible: false,
			modal: true,
			width: '300',
			height: 'auto'
		}).data('kendoWindow');

		dialog.open().center();

		return def.promise();
	};
	//keep track of history
	util.updateTrackingInfo = function(evt){
		//console.log('Apply Edits: ',arguments);
	};
	//activate editing tools based on the type of graphic selected
	util.activateEditTools = function(evt){
		//console.log('Activate editing tools: ', evt);
		//avoid dbl-clicks on graphic layers
		if (evt.type === 'dblclick') {
			return;
		}
		//prevent default
		evt.preventDefault();
		evt.stopPropagation();
		//hide infowindow before enabling shape edits
		var gLayer = evt.graphic.getLayer();
		if(gLayer.id === "User_Graphics"){
			return;
		}

		if (mapview.map.infoWindow) {
			mapview.map.infoWindow.hide();
		}
		//setup editing options
		var tools = 0;
		tools = tools | Edit.MOVE;
		tools = tools | Edit.SCALE;
		tools = tools | Edit.ROTATE;
		tools = tools | Edit.EDIT_VERTICES;
		//edit options
		var options = {
			allowAddVertices: true,
			allowDeleteVertices: true,
			allowScaling: true
		};

		if (evt.graphic.symbol && evt.graphic.symbol.type == 'textsymbol') {
			console.log('Editing text graphics ...');
		} else {
			mapview.edit.tools.activate(tools, evt.graphic, options);
			var msg = 'Editing tool is now active. Click out to deactivate.';
			nisis.ui.displayMessage(msg, '', 'info');
		}
	};
	//handle overlays layer checkbox
	util.toggleOverlays = function(evt){
		var checkBox = evt.target;
		var layer = checkBox.id.split('-');
		layer = layer[0];
		if (checkBox.checked) {
			mapview.layers[layer].show();
		}
		else {
			mapview.layers[layer].hide();
		}
	};
	//re-ajust the map when container resizes
	util.fitMapContainer = function(){
		mapview.map.reposition();
		mapview.map.resize();
		//nisis.ui.adjustViewSize();
	};
	//resize map container
	util.resize = function(dbd, tbl){
		var z = mapview.map.getZoom(), center = mapview.map.extent.getCenter();
		mapview.map.reposition();
		mapview.map.resize();
		mapview.map.centerAndZoom(center, z);
	};
	//process input files
	util.processInputFile = function(evt){
		//console.log(arguments);
		var form = $('#ft-uploadForm'),
		fileInput = $('#ft-uploadFile'),
		process = $('#ft-processMsg'),

		formData = new FormData(form), file = null;

		if(evt && evt.type == 'change'){
			file = evt.target.files[0];
			//file = evt.target.value; //==> use this when listening for onchange event on form
		} else if (evt && evt.type == 'click') {
			//file = fileInput.val(); //==> use this when using click event to submit file
			//file = document.getElementById(fileInput.attr('id')).files[0];
			file = fileInput.get(0).files[0];
		} else {
			return;
		}
		//
		if (file) {
			var filename = file.name;
			//check file extensions
			var zip = filename.toLowerCase().indexOf('.zip') !== -1 ? true : false;
			var csv = filename.toLowerCase().indexOf('.csv') !== -1 ? true : false;
			var kml = (filename.toLowerCase().indexOf('.kml') !== -1) ? true : false;
			//forwar request to the right function
			if (zip) {
				util.zipFeatureCollection(filename);
			} else if (csv){
				process.html('Processing file ...');
				$.when(util.csvFeatureCollection(file))
				.then(
					function(data){
						process.append('Done reading file content.');
					},
					function(e){
						process.html(e.message);
					},
					function(p){
						process.html(p);
					}
				);
			} else if (kml){
				$.when(util.kmlFeatureCollection(file))
				.then(
					function(data){
						mapview.map.addLayer(data);
					},
					function(e){
						process.html(e.message);
					},
					function(p){
						process.html(p);
					}
				);
			} else {
				process.html('The file is not valid');
			}
		} else {
			process.html('No file was uploaded');
		}
	};
	//analyse file
	util.analyseInputFile = function(file){
		var fReader = new FileReader(),
		data = fReader.readAsText(file);
		console.log(data);
		//esri request
		esri.request({
			url: 'http://www.arcgis.com/sharing/rest/content/features/analyse',
			content: {
				'file': file,
				'type': 'csv',
				'f': 'json',
				'callback.html': 'textarea'
			},
			form: dojo.byId('ft-uploadForm'),
			handleAs: 'json',
			load: dojo.hitch(this, function(response){
				console.log(response);

			}),
			error: dojo.hitch(this, function(error){
				console.log(error);

			})
		});
	};
	//generate Feature collection from zipped shapefile
	util.zipFeatureCollection = function(file){
		formData = new FormData(dojo.byId('ft-uploadForm'));
		$('#ft-processMsg').html('Loading ' + file + ' ...');
		var filename = file.split('\\');
			filename = filename[filename.length-1];
			filename = filename.substr(0,filename.lastIndexOf('.'));
		var params = {
			'name': filename,
			'targetSR': mapview.map.spatialReference,
			'maxRecordCount': 1000,
			'enforceInputFileSizeLimit': true,
			'enforceOutputJsonSizeLimit': true
		};
		//request params
		var extent = esri.geometry.getExtentForScale(mapview.map, 40000);
		var resolution = extent.getWidth() / mapview.map.width;
		params.generalize = true;
		params.maxAllowableOffset = resolution;
		params.reducePrecision = true;
		params.numberOfDigitsAfterDecimal = 5;
		//request content
		var content = {
			'filetype': 'shapefile',
			'publishParameters': dojo.toJson(params),
			'f': 'json',
			//'callback.html': 'textarea'
		};
		//ajax request for feature collection
		esri.request({
			url: nisis.url.arcgis.portal + 'sharing/rest/content/features/generate',
			content: content,
			form: formData,
			handleAs: 'json',
			load: lang.hitch(this, function(response, request){
				//console.log(response, request);
				if (response && response.error) {
					$('#ft-processMsg').html(response.error.message)
					.css('color','red');
					return;
				}
				if(response && response.featureCollection){
					$('#ft-processMsg').html('<b>Loaded: </b>' + response
					.featureCollection.layers[0].layerDefinition.name)
					.css('color','black');
					util.addShapefileToMap(response.featureCollection);
				}
				else {
					$('#ft-processMsg').html('The app could not process your request.')
					.css('color','red');
				}
			}),
			error: lang.hitch(this, function(error, request){
				$('#ft-processMsg').html(error.message).css('color','red');
			})
		});
	};
	//csv feature collection
	util.csvFeatureCollection = function(file){
		var def = new $.Deferred();
		var reader = new FileReader();
		reader.onload = function(e){
			//console.log(e);
			def.notify('Parsing file content ...');
			var points = null;
			$.when(
				$.parse(reader.result,{
					delimiter: ',',
					header: true,
					dynamicTyping: true
				})
			).done(function(points){
				//console.log(points);
				if(points){
					def.notify('Done parsing file content.');
					if(points.errors.length > 0){
						def.notify('There were ' + points.errors.length + ' errors during parsing:');
					}
					var fields = points.results.fields, lat = null, lon = null;
					$.when(
						$.each(fields,function(i,f){
							if(f.toLowerCase().indexOf('y') !==-1 || f.toLowerCase().indexOf('lat') !==-1 || f.toLowerCase().indexOf('latitude') !==-1){
								lat = f;
							}
							else if (f.toLowerCase().indexOf('x') !==-1 || f.toLowerCase().indexOf('lon') !==-1 || f.toLowerCase().indexOf('longitude') !==-1){
								lon = f;
							}
							if((i==fields.length-1) && lat && lon){
								def.notify('Location fields are: ' + lat + ' / ' + lon);
							}
						})
					).done(function(){
						def.notify('Adding features to map ...');
						var features = points.results.rows,
						featLayer = mapview.map.getLayer('User_Graphics');
						graphics = [];
						$.each(features,function(j,feat){
							var geom = new Point(feat[lon], feat[lat], new SpatialReference({wkid:4326}));
							geom = esri.geometry.geographicToWebMercator(geom);
							var attributes = {}, symbol = renderers.symbols.point;
							for(var attr in feat){
								attributes[attr] = feat[attr];
							}
							var graphic = new Graphic(geom,symbol,attributes);
							graphics.push(graphic);
							featLayer.add(graphic);
							if(j==features.length-1){
								def.notify(features.length + ' features generated');

								mapview.map.setExtent(mapview.util.getGraphicsExtent(graphics),true);
							}
						});
					});
				} else {
					def.reject('Parsing file content failed.');
				}
			});
		};
		reader.onprogress = function(e){
			//console.log(e);
			var perc = ((e.loaded / e.total) * 100).toFixed(0);
			def.notify('Reading ... ' + perc + '%');
		};
		reader.readAsText(file);

		return def.promise();
	};
	//private function
	var getKMLGeometry = function(coordinates, gType) {
		if (!coordinates) {
			return;
		}

		var geom = [],
			geometry = null,
			spRef = new SpatialReference({wdid:4326}),
			gType = gType || 'Point';

		switch (gType) {

			case "Point":
				geom.push(coordinates[0]);
				geom.push(coordinates[1]);

				geometry = new Point(geom, spRef);
				geometry = webMercatorUtils.geographicToWebMercator(geometry);

			break;

			case "LineString":
				var ring = [],
					len = coordinates.length;

				if (len < 2) {
					return;
				}

				$.each(coordinates, function(j, coord) {
					var pt = [];

					pt.push(coord[0]);
					pt.push(coord[1]);

					ring.push(pt);
				});

				if (ring.length < 2) {
					return;
				}

				geometry = new Polyline(ring, spRef);
				geometry = webMercatorUtils.geographicToWebMercator(geometry);

			break;

			case "Polygon":
				var rings = [];

				$.each(coordinates, function(i, coords) {

					var ring = [],
						len = coords.length;

					if (len < 3) {
						return;
					}

					$.each(coords, function(j, coord) {
						var pt = [];

						pt.push(coord[0]);
						pt.push(coord[1]);

						ring.push(pt);

						if (j === (len -1) && ring.length > 3) {
							ring.push(ring[0]);
						}
					});
					rings.push(ring);
				});

				geometry = new Polygon(rings, spRef);
				geometry = webMercatorUtils.geographicToWebMercator(geometry);

			break;

			case "Track":
				console.log('Geometry Type: ', gType);

			break;
		}

		if (geometry) {
			//console.log('Final geometry:', geometry);
			return geometry;
		}
	};

	//kml to feature collection
	util.kmlFeatureCollection = function(file){
		//console.log(file);
		var def = new $.Deferred();
		def.notify('Reading file ...');
		var reader = new FileReader();
		reader.onprogress = function(p){
			var prog = 'Loading: ' + (p.loaded / p.total * 100).toFixed(0) + '%';
			def.notify(prog);
		};
		reader.onerror = function(e){
			console.log('Error: ',e);
			def.notify(e.message);
		};
		reader.onload = function(r){
			//console.log(r);
			//console.log(typeof(reader.result));
			def.notify('File is loaded: ' + r.total + ' Bites.');
			var features = reader.result;
			//console.log(features);
			//var features = $.parseJSON(features);
			var fXml = $.parseXML(features);
			var fJson = $.xml2json(fXml);
			//console.log(fJson);
			var xmlParser = new DOMParser();
			var feats = xmlParser.parseFromString(features,"text/xml");
			//console.log("XML File:", feats);
			var geojson = toGeoJSON.kml(feats);
			//console.log(geojson);
			convertKMLFeatures(geojson);
			//processKMLContent(fJson);
		};
		//define a feature collection for kml layer
		var featColl = {
			'layerDefinition': {
				"geometryType": "esriGeometryPoint",
				"objectIdField": "ObjectID",
				"fields": [{
					"name": "ObjectID",
					"alias": "ObjectID",
					"type": "esriFieldTypeOID"
				}]
			},
			'featureSet': {
				'features': [],
				'geometryType': 'esriGeometryPoint'
			}
		};
		//get graphic layer from the map
		var featLayer = mapview.map.getLayer('User_Graphics');
		//process kml features
		var convertKMLFeatures = function(geojson){
			//console.log('KML GeoJson:', geojson);

			var gj = geojson,
			spRef = new SpatialReference({wdid:4326}),
			graphics = [];

			if (!gj.features || !gj.features.length ) {
				def.notify('There is no features in your kml file. Please double check your file.');
				return;
			}

			var feats = gj.features,
				validGeoms = ["Point", "LineString", "Polygon", "Track", "GeometryCollection"],
				len = gj.features.length;

			$.each(feats, function(i, feat){

				var geom = feat.geometry,
					geometry = null,
					symbol = null,
					infoTemplate = new InfoTemplate('Attributes','${*}'),
					graphic = null,
					attr = feat.properties ? feat.properties : {};

				if(attr){
					var fields = featColl.layerDefinition.fields;
					for(var key in attr){
						var field = {};
						field.name = key;
						field.alias = key;
						field.type = "esriFieldTypeString";
						fields.push(field);
					}
				}

				if (geom.type === "GeometryCollection") {

					if ( !geom.geometries || !geom.geometries.length) {
						return;
					}

					$.each(geom.geometries, function(n, g) {

						var coords = g.coordinates;

						geometry = getKMLGeometry(coords, g.type);
						symbol = renderers.symbols[geometry.type];
						graphic = new Graphic(geometry, symbol, attr);

						graphics.push(graphic);
						featLayer.add(graphic);

					});

				} else {

					if ( $.inArray(geom.type, validGeoms) === -1 ) {
						console.log('KML GeoJson Unknown Geometry: ' + (i+1), geom.type, feat);
						return;
					}

					geometry = getKMLGeometry(geom.coordinates, geom.type);
					symbol = renderers.symbols[geometry.type];
					graphic = new Graphic(geometry, symbol, attr);

					graphics.push(graphic);
					featLayer.add(graphic);
				}

				//check the end of the loop
				if(i == len-1){
					def.notify('Done processing features in your kml file.');
					mapview.map.setExtent(mapview.util.getGraphicsExtent(graphics), true);
					def.resolve(featLayer);
				}
			});
		};
		//read the file as text
		reader.readAsText(file);
		//reader.readAsDataURL(file);
		return def.promise();
	};

	//add shapefile to map
	util.addShapefileToMap = function(featColl){
		console.log('Adding feature collections to map: ',featColl.layers.length);
		var extent, layers = [];

		dojo.forEach(featColl.layers, function(featLayer, index){
			//console.log(featLayer);
			var infoTemp = new InfoTemplate('Attributes', '${*}');
			var symbol = renderers.symbols.polygon;

			var graphicLayer = mapview.map.getLayer('User_Graphics');
			graphics = [];
			if(graphicLayer){
				var type = featLayer.featureSet.geometryType;
				//console.log('Geometry Type: ',type);
				$.each(featLayer.featureSet.features, function(idx, feature){
					//console.log(feature.geometry);
					var geom;
					if(type === 'esriGeometryPoint'){
						geom = new Point({
							x: feature.geometry.x,
							y: feature.geometry.y,
							spatialReference: feature.geometry.spatialReference
						});
					}
					else if(type === 'esriGeometryPolyline') {
						geom = new Polyline({
							paths: feature.geometry.paths,
							spatialReference: feature.geometry.spatialReference
						});
					}
					else {
						geom = new Polygon({
							rings: feature.geometry.rings,
							spatialReference: feature.geometry.spatialReference
						});
					}

					var graphic = new Graphic(geom, symbol, feature.attributes);
					//var graphic = new Graphic(geom);
					graphicLayer.add(graphic);
					graphics.push(graphic);
				});
				//console.log(graphics);
				var extent = util.getGraphicsExtent(graphics);
				//console.log(extent);
				mapview.map.setExtent(extent.expand(1.25), true);
			} else {
				var msg = "App could not find User Graphics Layer.";
				nisis.ui.displayMessage(msg, '', 'error');
			}
		});
		//display result message and clear file from the form
		dojo.byId('ft-processMsg').innerHTML = 'Done. Your data should be on the map!';
		dojo.byId('ft-uploadFile').value = '';
	};
	//get editing field infos
	util.getEditingFieldInfos = function(layer){
		console.log(layer.id);
	};
	//get print file
	util.generatePrintFile = function(options){
		if(!options){
			return;
		}
		var def = new $.Deferred(),
		//print service link
		pUrl = nisis.url.arcgis.services + 'Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task',
		//define print object
		printTask = new PrintTask(pUrl);
		//set up print template
		var printTemplate = new PrintTemplate();
		printTemplate.format = options.file;
		printTemplate.layout = options.page;
		printTemplate.preserveScale = true;
		printTemplate.layoutOptions = {
			titleText: options.title,
			authorText: 'Produced by ' + nisis.user.name,
			copyrightText: 'FAA-NISIS',
			scalebarUnit: 'Miles'
			//,legendLayers: []
		};
		printTemplate.exportOptions = {
			width: 800,
			height: 600,
			dpi: 96
		};
		//set print params
		var printParam = new PrintParameters();
		printParam.map = mapview.map;
		printParam.outSpatialReference = mapview.map.spatialReference;
		printParam.template = printTemplate;
		//execute the print task
		printTask.execute(printParam,
			function(url){
				def.resolve(url);
			},function(e){
				def.reject(e);
			}
		);
		//returning a deferred obj
		return def.promise();
	};
	//update acl field alias
	//util.updateFieldsAlias = function(token){ //==> Global function
	var updateFieldsAlias = function(token){ //==> Local function
		var t,
			lyrIds = ['airports','atm','osf','ans','teams'];

		if(token){
			t = token;
		} else {
			if(window.localStorage){
				var cred = localStorage.getItem('nisis-services-credentials');
				cred = JSON.parse(cred);
				console.log(cred);
				if(cred){
					t = cred.credentials[0].token;
				}
			}
		}

		if(!t){
			console.error('App could not retreive server token');
			return;
		}

		if(nisis.user.username !== "baboyma"){
			console.log('You are not authorized to run this function');
			return;
		}

		var getFields = function(url, id){
			console.log('Retreiving fields for: ', id);
			$.ajax({
				url: url,
				type: 'GET',
				data: {f: 'json',token: t},
				success: function(data){
					var resp = JSON.parse(data);
					if(resp.fields){
						//console.log('Fields: ', resp.fields);
						updateFields(resp.fields, id);
					} else {
						console.log('App could not get ' + id + '\'s fields', resp);
					}
				},
				error: function(xhr, status, err){
					console.log('App could not get ' + id + '\'s fields',err);
				}
			});
		};

		var updateFields = function(fields, layerid){
			console.log('Updating fields for: ', layerid);
			$.ajax({
				url: 'api/',
				type: 'POST',
				data: {
					'action': 'admin_processes',
					'task': 'update_fields_alias',
					'fields': fields,
					'layer': layerid
				},
				dataType: 'json',
				success: function(data){
					console.log('Data: ', data);
					//var resp = JSON.parse(data);
					//console.log('Update Result for ' + layerid +': ', resp);
				},
				error: function(xhr, status, err){
					console.log('App could not update ' + layerid + '\'s fields',xhr.responseText);
					console.log('App could not update ' + layerid + '\'s fields',status);
				}
			});
		};

		$.each(lyrIds,function(i, lyrId){
			var layer = mapview.map.getLayer(lyrId);
			if(layer){
				getFields(layer.url, lyrId);
			}else{
				console.log('App could not retreive layer: ', lyrId);
			}
		});
	};
	// util.flyToShape = function(oid) {
	// 	//query geometry
 //            var query = nisis.ui.mapview.util.queryFeatures({
 //                layer: nisis.ui.mapview.map.getLayer('Polygon_Features'),
 //                returnGeom: true,
 //                outFields: ["*"],
 //                where: "OBJECTID = " + oid
 //            });
 //            //success handler
 //            query.done(function(data){
 //                var feature;
 //                if(data.features.length > 0){
 //                    feature = data.features[0];
 //                    var target = feature.geometry;
 //                    var pt = target.getCentroid();
	// 				mapview.map.centerAt(pt);
 //                } else {
 //                    nisis.ui.displayMessage("App could not find the feature's location", '', 'error');
 //                }
 //            });
 //            //failed handler
 //            query.fail(function(err){
 //                nisis.ui.displayMessage(err, '', 'error');
 //            });
	// };
	util.flyToShape = function(geomtype,oid) {
		var def = new $.Deferred();
		var layerid;
		if (!geomtype || geomtype == "" || !oid || oid == ""){
			return;
		}

		if (geomtype === "polygon"){
			layerid = 'Polygon_Features';
		} else if (geomtype === "line"){
			layerid = 'Line_Features';
		} else if (geomtype === "point"){
			layerid = 'Point_Features';
		} else if (geomtype === "text"){
			layerid = 'text_features';
		}
		//query geometry
        var query = nisis.ui.mapview.util.queryFeatures({
            layer: nisis.ui.mapview.map.getLayer(layerid),
            returnGeom: true,
            outFields: ["*"],
            where: "OBJECTID = " + oid
        });
        //success handler
        query.done(function(data){
            var feature;
            if(data.features.length > 0){
                feature = data.features[0];
                var target = feature.geometry;
                if (geomtype === "point" || geomtype === "text"){
                	mapview.map.centerAt(target);
                } else if (geomtype === "line") {
                	//var pt = target.getPoint(0,0);
                	var pt = target.getExtent().getCenter();
					mapview.map.centerAt(pt);
            	}else {
                	var pt = target.getCentroid();
					mapview.map.centerAt(pt);
				}
				def.resolve(data);
            } else {
                nisis.ui.displayMessage("App could not find the feature's location", '', 'error');
            	def.reject("App could not find the feature's location");
            }
        });
        //failed handler
        query.fail(function(err){
            nisis.ui.displayMessage(err, '', 'error');
            def.reject('The query in util.flyToShape() function in mapview.util.js failed');
        });

        return def.promise();
	};


    util.bringUpNISISTOProfile = function(evt){
        //set screenpoint
        var screenPoint = evt.screenPoint;

		var getPayloads= {
			        'airports': { action:"get_profiles_airports", id: evt.graphic.attributes.FAA_ID},
		            'airports_pr': { action:"get_profiles_airports", id: evt.graphic.attributes.FAA_ID},
		            'airports_others': { action:"get_profiles_airports", id: evt.graphic.attributes.FAA_ID},
		            'airports_pu': { action:"get_profiles_airports", id: evt.graphic.attributes.FAA_ID},
		            'airports_military': { action:"get_profiles_airports", id: evt.graphic.attributes.FAA_ID},
		            'atm': { action:"get_profiles_atm", id: evt.graphic.attributes.ATM_ID},
		            'ans': { action:"get_profiles_ans", id: evt.graphic.attributes.ANS_ID},
		            'ans1': { action:"get_profiles_ans", id: evt.graphic.attributes.ANS_ID},
		            'ans2': { action:"get_profiles_ans", id: evt.graphic.attributes.ANS_ID},
		            'osf': { action:"get_profiles_osf", id: evt.graphic.attributes.OSF_ID},
		            'tof': { action:"get_profiles_tof", id: evt.graphic.attributes.TOF_ID}
		            };
		var getPayload = getPayloads[evt.graphic._layer.id];

		//spinner
		var dTitle = "Loading " + getPayload.id + " profile",
			box = $('<div/>', {"class": "profile-spin"}),
		    spinner = $("<i/>", {"class": "fa fa-cog fa-spin fa-4x"});

		//remove kendo styles
		spinner.appendTo(box);

		//append dialog to app content
		$('#app-content').append(box);

		//kendo dialog
		var dialog = box.kendoWindow({
						actions: ["Close"],
						title: dTitle,
						visible: false,
						modal: true,
						width: '300',
						height: 'auto',
						animation: {
							open: {
								effects: "fade:in"
							},
							close: {
								effects: "fade:out"
							}
						}
		}).data('kendoWindow');

		if (getPayload == null || (getPayload!=null && getPayload.id==null)) {
			//console.log("Invalid primary key given for profile");
			nisis.ui.displayMessage('Unable to display profile', 'error');
			if(dialog.is(':visible')){
				dialog.close();
			}
			return;
		};
		$.ajax({
	        url:"api/",
	        type:"POST",
	        data: {
	            "action": "get_profiles",
                "dataType": "json",
	            "get_what": getPayload.action,//get_profiles_xxx
	            "id": getPayload.id, //'DCA' , 'PCT-TRACON'
		    },
		    beforeSend: function(req, e){
		        //run spinning
		       // console.log('opening profile for ' + getPayload.id);
		        $('.profile-spin').removeClass('k-window-content k-content');
		        dialog.center().open();
		        var $el = $(dialog.element).parent();
		        var $closeBtn = $el.find('.k-i-close')
		        console.log($closeBtn);
		        $closeBtn.on('click', function(){
		        	console.log('closed...');
		        	req.abort();
		        });
		        //dialog.close();


		    },
	        success: function(data, status, req) {
	            var profileData = JSON.parse(data);
	            if(profileData['profile'].length>0){
	                var infobox = new NISISProfile(profileData['profile'], evt);
	                var $box = $('.nisis-profile-wrapper');
	                	lastBoxIdx = $box.length - 1;
	                var $win = $(window);
	                // console.log('window', $win)
	                // console.log('window outerHeight', $win[0].outerHeight)
	                // console.log('window outerWidth', $win[0].outerWidth)
	                // console.log('window screenleft', $win[0].screenLeft)
	                // console.log('window screenX', $win[0].screenX)
	                // console.log('body', $('body'))
	                $box.each(function(i){
	                	$box.css({
							'box-shadow': '0px 0px 18px 3px rgba(0,0,0,0.35)'
						});

       //          		if($win[0].outerHeight <= 728 && $win[0].outerWidth <= 1366){
       //          			$box.css({
							// 	'top': ($win.height() / 2) - ($box.outerHeight() / 2),
							// 	'left': ($win.width() / 2) - ($box.outerWidth() / 2)
							// });
       //          		}
	                	if($box[i].offsetLeft > 967){
							$(this).css({
								'left': ($win.width()) - ($box.outerWidth())
							});
	                	} else if ($box[i].offsetTop > 440){
	                		$(this).css({
								'top': ($win.height()) - ($box.outerHeight())
							});
	                	}else{

	                		//make sure last launched profile window is centered
                			$($box[lastBoxIdx]).css({
								'top': ($win.height() / 2) - ($box.outerHeight() / 2),
								'left': ($win.width() / 2) - ($box.outerWidth() / 2)
							});
                			                		
	                	}

	      //           		$box.css({
							// 	'top': ($win.height() / 2) - ($box.outerHeight() / 2),
							// 	'left': ($win.width() / 2) - ($box.outerWidth() / 2)
							// });


	                })



    		// 			$box.css({
						// 	'top': ($win.height() / 2) - ($box.outerHeight() / 2),
						// 	'left': ($win.width() / 2) - ($box.outerWidth() / 2)
						// });
	            } else{
	              	nisis.ui.displayMessage(profileData["MSG"], 'error');
	            }
	        },
	        error: function(req, status, err) {
	                console.error("ERROR in ajax call to get Airports map objects");
	        },
	        complete: function(){
	            //close spinner
		        dialog.close();
	        }
	    });
	};

    util.refreshMapLegend = function() {
    	console.log("refreshing Map Legend, preserving collapsed items ");
        var arr = this.saveMapLegendCollapsedItems();
        $('#legend-content').hide();
        nisis.ui.mapview.legend.refresh();
        this.expandMapLegend(arr);
        $('#legend-content').show();
    };            
	util.saveMapLegendCollapsedItems = function() {
		var ids = [];
		$("#legend-div .esriLegendServiceLabel.collapsed").each(function(){			
			ids.push($(this).closest('div').attr('id'));
		});
		return ids;
	};
    //keep the ids, since when the map legend refreshes, it might blow away the custom classes i added
	util.expandMapLegend = function(collapsedNodesArray) {		
		for (var i=0; i < collapsedNodesArray.length; i++) {				
			var cId = collapsedNodesArray[i];
			$header = $('#' + cId);
			$label = $header.find('.esriLegendServiceLabel');			
			$content = $header.find("div:first");
		    //collapse   
		    $content.slideToggle();
		    $label.toggleClass('collapsed');		
		};
	};

	//public object
	return util;
})();
});
