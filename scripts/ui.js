//User Interface manipulation
require(['app/appConfig', 'app/mapConfig', 'app/windows/tableview', 'app/windows/shapesManager']

	,function(appConfig, mapConfig, tableview, shapesManager){
    
nisis.ui = (function(){
	var mapview = nisis.ui.mapview;
	var oid = '';
	var facId = '';
	//console.log(mapview);
	var uiEls = {};

    var templates = {};

	$.getScript( "./html/profiles_fr23/js/airport_json_no_data_values.js", function( data, textStatus, jqxhr ) {
          templates['airports_pr'] = apt_json_template;
          templates['airports_others'] = apt_json_template;
          templates['airports_pu'] = apt_json_template;
          templates['airports_military'] = apt_json_template;
    });
	$.getScript( "./html/profiles_fr23/js/ans_json_no_data_values.js", function( data, textStatus, jqxhr ) {
          templates['ans1'] = ans_json_template;
          templates['ans2'] = ans_json_template;
    });
	$.getScript( "./html/profiles_fr23/js/atm_json_no_data_values.js", function( data, textStatus, jqxhr ) {
          templates['atm'] = atm_json_template;
    });
	$.getScript( "./html/profiles_fr23/js/osf_json_no_data_values.js", function( data, textStatus, jqxhr ) {
          templates['osf'] = osf_json_template;
    });
	$.getScript( "./html/profiles_fr23/js/tof_json_no_data_values.js", function( data, textStatus, jqxhr ) {
          templates['tof'] = tof_json_template;
    });
	
	uiEls.flyToShape = function(e){
        //fly to feature
        var featureId = Number($(e).parent().find('span').text());
        var layer = nisis.ui.mapview.map.getLayer('Polygon_Features');
        nisis.ui.mapview.util.flyToShape(featureId);	       
    };
	//uiEls.msg = $('#messages').kendoNotification().data("kendoNotification");
    //display alert messages, each arg is optional
    //	'detail' is main body, the long message
    //	'summary' is essentially a title
    //	'severity' is the type of message: "info" || "success" || "warn" || "error"
    uiEls.displayMessage = function(detail, summary, severity){
        //Alerts messages
        var args = arguments;

        if (!args.length){
        	return;
        }
        //content
        var det = detail ? detail : 'No message added ...';
        var sum = summary ? summary : '';
        //var sev = severity ? severity : 'info';
        var sev = 'info';

        if(args.length > 1){
        	sev = args[args.length-1];
        }
   
        uiEls.msg.show(det, sev);
    };
    //processing wheels
    uiEls.startProcess = function(title, msg) {
    	var dTitle = "Loading application ...",
    	process = $('<div/>', {
    		"id": "ui-process",
    		"class": "dialog"
    	}),
		spin = $("<p/>", {
			"class": "txt-ctr"
		}),
		spinner = $("<i/>", {
			"class": "fa fa-cog fa-spin fa-4x"
		});
		spin.append(spinner).appendTo(process);
		//title
		if ( title ) {
			dTitle = title;
		}
		//message
		if ( msg ) {
			var message = $("<p/>", {
				"class": "txt-ctr",
				"text": msg
			});
			spin.append(message).css('z-index', '99');
		}
    	//append dialog to app content
		$('#app-content').append(process);
		//kendo dialog
		var dialog = process.kendoWindow({
			actions: [],
			title: dTitle,
			visible: false,
			modal: true,
			width: '300',
			height: 'auto',
			animation: {
				open: {
					effects: "fade:in"
				},
				close: {
					effects: "fade:out"
				}
			}
		}).data('kendoWindow');
		//open the dialog
		dialog.center().open();

		if (!title) {
			setTimeout(function(){
				if ( $('#ui-process').length ) {
					uiEls.stopProcess();
				}
			}, 60000);
		}
    };

    //stop processing
    uiEls.stopProcess = function() {
    	var process = $('#ui-process'),
    		dialog = process.length ? process.data('kendoWindow') : null;

    	if ( dialog ) {
    		dialog.close();
    		setTimeout(function() {
    			dialog.destroy();
    		}, 300);
    	}
    };

    //confirimation boxes ==> options is a json object with 
    /*
     * <string> title
     * <string> message
     * <string array> buttons
     */
    uiEls.displayConfirmation = function(options) {
    	var def = new $.Deferred();
    	if(!options){
    		def.reject('Invalid options.');
    	}
    	
		//dialog
		var confirm = $('<div id="confirm" class="nisis-dialog"></div>');
			msg = $("<p></p>"), 
			title = null;

		if(options.title){
			title = options.title;
		} else {
			title = 'Confirm Action!';
		} 
		//confirm message
		if(options.message){
			msg.text(options.message);
			confirm.append(msg);
		}
		//confirm buttons
		if(options.buttons && options.buttons.length){
			$.each(options.buttons, function(i,btn){
				var button = $("<button class='k-button marg nisis-dialog-button'></button>");
				button.text(btn);
				button.click(function(e){
					def.resolve(btn);
					dialog.close();
					dialog.destroy();
				});
				confirm.append(button);
			});
		}
		//append dialog to app content
		$('#app-content').append(confirm);
		//kendo dialog
		var dialog = confirm.kendoWindow({
			title: title,
			visible: false,
			modal: true,
			width: '300',
			height: 'auto',
			close: function(e){
				def.resolve('close');
			}
		}).data('kendoWindow');
		//open the dialog
		dialog.center().open();
		// new styles have to put it here to find parent elements and make pretTAY
		//find parent elements
    	var $kDialog = $('.nisis-dialog');
    	console.log($kDialog);
    	var $kDialogWrapper = $kDialog.parent('div.k-widget.k-window');
    	console.log($kDialogWrapper);
    	var $kDialogWrapperHeader = $kDialogWrapper.find('.k-window-titlebar.k-header');
    	console.log($kDialogWrapperHeader);
    	// remove default kendo styles barf
    	$kDialogWrapper.removeAttr('style');
    	//add new class to wrapper
    	$kDialogWrapper.removeClass('k-widget k-window').addClass('nisis-dialog-wrapper');
    	// remove header and title attr
    	$kDialogWrapperHeader.removeAttr('style');
    	// remove kendo classes add ours
    	$kDialogWrapperHeader.removeClass('k-window-titlebar k-header').addClass('titlebar header');


		//deferred object
		return def.promise();
    };

    uiEls.displayPrompt = function(options) { 
    	var def = new $.Deferred();
    	if(!options || typeof options !== 'object'){
    		def.reject('Invalid options for Prompt Dialog.');
    	}

		//dialog
		var prompt = $('<div />', {
			"id": "nisis-prompt",
			"class": "dialog"
		}),
		w = 300,
		h = 100,
		title = "User Input",
		desc = $("<p/>", {
			text: "Enter or update your input"
		}),
		input = null;

		//dialog title
		if(options.title){
			title = options.title;
		}
		//dialog description
		if(options.desc){
			desc.text(options.desc);
		}
		//dialog textarea size
		if(options.width && $.isNumeric(options.width)) {
			w = $.trim(options.width);
		}
		if(options.height && $.isNumeric(options.height)) {
			h = $.trim(options.height);
		}

		input = $("<textarea/>", {
			"class": "b-sq b-shd m-b clr",
			"width": w,
			"height": h,
			"placeholder": "Type your input goes here ..."
		});
		//input message
		if(options.message){
			input.text(options.message);
		}

		prompt.append(desc, input);
		//confirm buttons
		if(options.buttons && options.buttons.length){
			var clss = "k-button",
			btns = $('<div/>', {
				"class": "clr of-h"
			});

			$.each(options.buttons, function(i, btn){
				var c = clss;

				if (i !== 0){
					c += " m-l";
				}

				var button = $("<button />", {
					"class": c
				});

				button.text(btn.toUpperCase());
				button.click(function(e){
					var txt = input.val();
					def.resolve({'action':btn, 'input':txt});
					dialog.close();
					dialog.destroy();
				});
				btns.append(button);
			});

			prompt.append(btns);
		}
		//append dialog to app content
		$('#app-content').append(prompt);
		//kendo dialog
		var dialog = prompt.kendoWindow({
			title: title,
			visible: false,
			modal: true,
			width: "auto",
			height: "auto",
			close: function(e){
				def.resolve({button:'close', input:null});
			}
		}).data('kendoWindow');
		//open the dialog
		dialog.center().open();
		//deferred object
		return def.promise();
    };
    //selection box
    /* options (requred) ==> json object with these keys
    <string> title
    <string> label
    <string> message
    <boolean> filter
    <string> ftext
    <string> fvalue
    <string | objets array> dataSource
    */
    uiEls.displaySelections = function(options) {
    	//console.log('Selection options:', options);
    	if (!options) {
    		return;
    	}
    	//create deferred object
    	var def = new $.Deferred();
    	//set default options
    	var dialog,
			selector,
			selType = "single",
			title = "Selection!",
			message = "Provide your selection(s) and click done.",
			filter = false,
			filters = ['contains', 'endswith', 'startswith'],
			label = " - None - ",
			ftext = "name",
			fvalue = "value",
			dataSource = [];

		if(options && options.type) {
			selType = options.type;
		}

		if(options && options.title) {
			title = options.title;
		}

		if(options && options.message) {
			message = options.message;
		}

		if(options && options.filter) {
			if ($.inArray(options.filter, filters) != -1 ) {
				filter = options.filter;
			} else {
				filter = "contains";
			}
		} 

		if(options && options.label) {
			label = options.label;
		}

		if(options && options.ftext) {
			ftext = options.ftext;
		}

		if(options && options.fvalue) {
			fvalue = options.fvalue;
		}

		if(options && options.dataSource && options.dataSource.length) {
			dataSource = options.dataSource;
		}

    	var	selsDialog = $('<div/>', {
			"id": "selection-dialog",
			"class": "dialog"
		}),
		selsCont = $('<div/>',{
			"class": "marg clr"
		});
		selsDialog.append(selsCont);

		var msgDiv = $("<div/>", {
			"class": "marg clr",
			"text": message
		});
		selsCont.append(msgDiv);

		var selDiv = $("<div/>", {
			"class": "w-400 marg clr"
		}),
		choices = $('<select/>', {
			"class": "w-400"
		});
		selDiv.append(choices);

		var btnsDiv = $("<div/>", {
			"class": "marg clr"
		});
		selsCont.append(btnsDiv);

		var select = $("<button/>", {
			"class": "w-80",
			"text": "Done"
		}),
		cancel = $("<button/>", {
			"class": "w-80 m-l10",
			"text": "Cancel"
		});
		btnsDiv.append(select, cancel);

		selsCont.append(msgDiv,selDiv,btnsDiv);
		
		//append dialog to app content
		$('#app-content').append(selsDialog);

		//kendo dialog
		dialog = selsDialog.kendoWindow({
			title: title,
			visible: false,
			modal: true,
			width: 'auto',
			height: 'auto',
			close: function(e){
				this.destroy();
				def.resolve('close');
			}
		}).data('kendoWindow');

		//dd options
		var ddOptions = {
			optionLabel: label,
			dataTextField: ftext,
			dataValueField: fvalue,
			dataSource: dataSource
		};

		if (filter) {
			ddOptions.filter = filter
		}
		
		//set the multiselect for groups
		if (selType === "multiple") {
			selector = choices.kendoMultiSelect(ddOptions).data('kendoMultiSelect');
		} else if (selType === "single") {
			selector = choices.kendoDropDownList(ddOptions).data('kendoDropDownList');
		} else {
			dialog.destroy();
			var msg = "App could not identify widget type.";
			nisis.ui.displayMessage(msg, 'error');
		}

		//handle selection
		select.kendoButton({
			enable: false
			,click: function(e){
				var sel = selector.value();
				//console.log('Selection: ', sel);
				

				selsDialog.hide();

				dialog.destroy();

				def.resolve(sel);
			}
		});
		cancel.kendoButton({
			enable: true
			,click: function(evt) {
				selsDialog.hide();

				dialog.destroy();
				def.resolve('cancel');
			}
		});
		//validate selection
		selector.bind('change', function(e){
			var values = this.value();
			if (values && values.length > 0){
				select.data('kendoButton').enable(true);
			} else {
				select.data('kendoButton').enable(false);
			}
		});

		//open the dialog
		if (dialog) {
			dialog.center().open();
		} else {
			var msg = "App could not build your selection widget.";
			nisis.ui.displayMessage(msg, 'error');
		}

    	return def.promise();
    };
    //share access groups ==> options is a json object with 
    uiEls.displayUserAccessGroups = function(){
    	var def = new $.Deferred();
    	//get user groups 
    	$.ajax({
    		type: "POST",
    		url: "admin/api/",
    		data: {
    			'action': "acl",
    			//'function': 'getallgrps'
    			'function': 'getusergroups',
    			id: nisis.user.userid
    		},
    		success: function(response){
    			//console.log('User Groups: ',arguments);
	    		//console.log('User Groups: ',response);
	    		var resp;
	    		try {
	    			resp = JSON.parse(response);
	    			//console.log(resp);
	    		} catch (err){
	    			def.reject(err.message);
	    		}

	    		if(resp.result && resp.result !== 'Success'){
	    			def.reject('resp.message');
	    		}

	    		var shareDialog = $('<div id="share-dialog"></div>');
	    		var cDiv = $('<div class="marg clear"></div>');
	    		shareDialog.append(cDiv);
	    		var mDiv = $("<div class='marg clear'></div>");
	    		var msg = $("<span>Select groups to share the feature with</span>");
	    		mDiv.append(msg);
	    		var gDiv = $("<div class='marg clear'></div>");
	    		var groups = $('<select id="sharing-groups"></select>');
	    		gDiv.append(groups);
	    		var bDiv = $("<div class='marg clear'></div>");
	    		var share = $("<button class='k-button'>Share</button>");
	    		var cancel = $("<button class='margLeft k-button'>Cancel</button>");
	    		bDiv.append(share,cancel);
	    		cDiv.append(mDiv,gDiv,bDiv);
	    		
	    		//append dialog to app content
				$('#app-content').append(shareDialog);
				//kendo dialog
				var dialog = shareDialog.kendoWindow({
					title: "User Groups",
					visible: false,
					modal: true,
					width: '300',
					height: 'auto',
					close: function(e){
						this.destroy();
						def.resolve('close');
					}
				}).data('kendoWindow');
				//sort response
				uiEls.sortArrayOfObjects(resp.items, 'GNAME');
				//set the multiselect for groups
				var g = groups.kendoMultiSelect({
					placeholder: "User access groups",
					dataTextField: "GNAME",
					dataValueField: "GROUPID",
					dataSource: resp.items
				}).data('kendoMultiSelect');
				//handle sharing
				share.kendoButton({
					enable: false
					,click: function(e){
						var gps = g.dataItems();
						shareDialog.hide();
						dialog.destroy();
						def.resolve(gps);
					}
				});
				cancel.on('click',function(evt){
					shareDialog.hide();
					dialog.destroy();
					def.resolve('cancel');
				});
				g.bind('change',function(e){
					var values = this.value();
					if (values.length > 0){
						share.data('kendoButton').enable(true);
					} else {
						share.data('kendoButton').enable(false);
					}
				});
				//handle cancel
				//open the dialog
				dialog.center().open();
			},
			error: function(error){
	    		console.log(error);
	    		nisis.ui.displayMessage(error,'','error');
	    		def.reject(error);
	    	}
    	});

    	return def.promise();
    };
    //re-adjust div size;
    uiEls.adjustSize = function(id){
        var el = $('#'+id);
        var w = $(el).width()-2;
        var h = $(el).height()-2;
        $(el).width(w);
        $(el).height(h);  
    };
    //track window resize
    uiEls.collapseAppMenu = function(evt){
    	console.log('Window resized:', evt);
    };
	//display app menu bar
	uiEls.displayAppMenuBar = function() {
		$(".menu-item>img").click(function(evt){
			var dialog = $('#'+this.id+'-div');
			if(dialog && dialog.length > 0){
				dialog.slideToggle();
				if(this.id == 'drawing' && dialog.is(':visible') && nisis.ui.mapview.templatePicker) {
				 	nisis.ui.mapview.templatePicker.update();
				}
			}else{
				nisis.ui.displayMessage(this.id + "'s functionality has not yet been implemented.", "Functionality");
			}
		});
		//title with bootstrap
		$('.menu-item').tooltip({
			trigger: 'hover',
			placement: 'bottom',
			container: false
		});
	};
	//Build main menu
	uiEls.buildAppMenu = function(){
		var mItems = appConfig.header.menubar,
			appMenu = $('#menu-bar'),
			apps = nisis.user.apps;

		if (!apps.length) {
			return;
		}
		//console.log('TFR User Check Point: ', nisis.user.tfrAccess);

		$.each(mItems,function(i, item){
			if (item.add) {
				var mItem = $("<div class='menu-item'></div>");
				mItem.attr('id', "m-" + item.id);
				mItem.attr('data-title', item.title);
				mItem.attr('data-target', item.target);

				var iEvents = item.events.toJSON(),
					events = "events:{";

				for (var key in iEvents) {
					events += key;
					events += ":";
					events += iEvents[key];
					events += ",";
				}

				//get rid of the last comma
				events = events.substr(0, events.length-1);
				events += "}";

				mItem.attr('data-bind', events);

				var img = $("<img width='26' height='26' />");
				img.attr('id', item.id);
				img.attr('src', item.icon);

				mItem.append(img);

				if (!nisis.user.tfrAccess && item.id === 'tfr'){
					return;
				}

				if (nisis.user.tfrAccess){
					//flag tfr items
					mItem.attr('data-tfr', item.tfr);
				}
				
				appMenu.append(mItem);
			}
		});
		//this breaks the app
		kendo.bind($('#menu-bar'), appConfig);
		//kendo.init('#menu-bar');
	}
	//display user controls
	uiEls.displayUserControls = function() {
		var getUserMenu = function(){
			var content = $($.parseHTML($('#user-menu').html()));
			content.find('#user-profile').click(function(evt){
				//TODO: wrap user admin stuff in a dialog
				var dialog = $('#profile-div');
			    if(dialog && dialog.length > 0){
			   		dialog.slideToggle();
					$('#user-name').popover('hide');
			   		//TODO - see menu-item click event handlers...
		   			//does this similarly need to update the mapview.templatePicker?
			    }else{
			   		//TODO: What happens when the dialog is not built?
			    }
			});
			content.find('#user-pref').click(function(evt){
				nisis.ui.displayMessage('User Preferences not implemented yet','Dialog!','warn');
				$('#user-name').popover('hide');
			});
			content.find('#user-admin').click(function(evt){
				nisis.ui.displayMessage('The admin page should be a dialog','Dialog!','warn');
				$('#user-name').popover('hide');
			});
			return content;
		};
		$('#logout').click(function(evt){
			nisis.user.logout();
		});
		//App screen sizing
		$('#full-screen').click(function(){
			if (!document.fullscreenElement &&    // alternative standard method
				!document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
		    	if (document.documentElement.requestFullscreen) {
		    		document.documentElement.requestFullscreen();
		    	} else if (document.documentElement.mozRequestFullScreen) {
		    		document.documentElement.mozRequestFullScreen();
		    	} else if (document.documentElement.webkitRequestFullscreen) {
		    		document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		    	}
				$(this).children().removeClass('fa-arrows-alt')
				.addClass('fa-times-circle-o');
		    } else {
	  		    if (document.cancelFullScreen) {
	  		    	document.cancelFullScreen();
	  		    } else if (document.mozCancelFullScreen) {
	  		    	document.mozCancelFullScreen();
		    	} else if (document.webkitCancelFullScreen) {
		    		document.webkitCancelFullScreen();
		    	}
				$(this).children().removeClass('fa-times-circle-o')
				.addClass('fa-arrows-alt');
		    }
		});
		$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange',function(){
			if (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement) {
				$("#full-screen").children().removeClass('fa-arrows-alt')
                .addClass('fa-times-circle-o');
			} else {
		        $("#full-screen").children().removeClass('fa-times-circle-o')
                .addClass('fa-arrows-alt');
			}
		});
		//display user controls
		//$('#user-controls').show();
		//custom category-based autocomplete
		$.widget("custom.catcomplete", $.ui.autocomplete, {
			_renderMenu : function(ul, items) {
				var that = this, currentCategory = "";
				$.each(items, function(index, item) {
					if (item.category != currentCategory) {
						ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
						currentCategory = item.category;
					}
					that._renderItemData(ul, item);
				});
			}
		});
    };
	//display search field
	//I don't know what is the function of this code - This should Be Removed. Meanwhile is commented out. The code that is being used by the quicksearch is in appConfig.js in searchFeatures(e) method. --CC
	/*uiEls.displaySearchField = function() {
		$('#search-box').catcomplete({
			source: 'api/auto_complete.php',
			minLength: 2,
			'class': 'ui-autocomplete-text',
			position: {
				my: 'right top',
				at: 'right bottom'
			},
			select: function(evt, ui){
				var category = ui.item.category, label = ui.item.label,
				value = ui.item.value, id = ui.item.id;
				var search = nisis.ui.mapview.util.searchFeatures(id,false,['LOOKUPID']);
				search.then(function(data){
						var features = data, len = features.length;
						if(len === 0){
							uiEls.displayMessage("The system couldn't find the selected features.","No Results!","warn");
						} else if (len === 1){
							uiEls.displayMessage("The map will get you to the feature.","1 Result!");
							var target = features[0].feature.geometry;
							nisis.ui.mapview.map.centerAndZoom(target,12);
							nisis.ui.mapview.util.flashAtLocation(target);
						} else {
							uiEls.displayMessage("There are " + len + " features found.",len +" Results!");
							var target1 = features[0].feature.geometry;
							nisis.ui.mapview.map.centerAndZoom(target1,12);
							nisis.ui.mapview.util.flashAtLocation(target);
						}
					},function(error){
						uiEls.displayMessage(error.message, "Error!","error");
					},function(p){
						uiEls.displayMessage(p, "Processing!");
					}
				);
			}
		}).keypress(function(evt){
			if(evt.keyCode == 13){
				var input = $(this).val();
				if (input.length > 0) {
					nisis.ui.mapview.util.findLocations(input);
				} else {
					nisis.ui.nisis.ui.displayMessage('Enter a keyword','Keyword!','warn');
				}
			}
		});
	};*/
	//display basemaps handler
	uiEls.displayBasemapsTool = function(){
		$('#basemaps').button()
		.fadeTo('slow', 0.5)
		.on('click', function(){
			$('#basemaps-box').toggle();
		}).on('mouseenter', function(){
			$(this).fadeTo('slow', 1);
		}).on('mouseout', function(){
			$(this).fadeTo('slow', 0.5);
		});
		$('#basemaps').show();
	};
	//display numeric scale
	uiEls.displayNumericScale = function(){		
		var numScale = $('#numScale'),
		numScaleEls = $("<span id='numScaleCst' class=''>1:</span> <span id='numScaleVal' class=''></span> <span id='numScaleUnits' class=''>Meters</span>");
		numScale.append(numScaleEls);
		//expose numeric scale to the whole app
		nisis.ui.widgets.numScale = numScale;
		nisis.ui.widgets.numScale.scale = $('#numScaleVal');
		nisis.ui.widgets.numScale.units = $('#numScaleUnits');
		nisis.ui.widgets.numScale.unit = esri.Units.Meters;
		//wrap
		var opts = $('<div></div>');
		var options = $('<select></select>'),
		data = [{
			label: 'Meters',
			value: esri.Units.METERS
		},{
			label: 'Feet',
			value: esri.Units.FEET
		},{
			label: 'Kilometers',
			value: esri.Units.KILOMETERS
		},{
			label: 'Miles',
			value: esri.Units.MILES
		},{
			label: 'Nautical Miles',
			value: esri.Units.NAUTICAL_MILES
		}];
		opts.appendTo(numScale).append(options);
		options.puilistbox({
			data: data,
			itemSelect: function(evt,items){
				//console.log(evt,items);
				//console.log(items[0].value);
				nisis.ui.widgets.numScale.units.html(items[0].label);
				nisis.ui.widgets.numScale.unit = items[0].value;
				uiEls.updateNumericScale();
				opts.hide();
			}
		});
		//overwrite the size of pui listbox
		$(opts.find('.pui-listbox')).css({width: '120px',height:'auto'});
		//add a click event to show unit options
		nisis.ui.widgets.numScale.units.css('cursor','pointer')
		.on('click',function(evt){
			//uiEls.displayMessage('Changing mouse coords units is not implemented yet','Not ready!');
			opts.css({
				position: 'absolute',
				left: 0,
				bottom: '120%',
				zIndex: 9999
			}).toggle();
		});
		//hide the option once it's been built
		opts.hide();
	};
	//set initial map numeric scale
	uiEls.setNumericScale = function(){
		//console.log('Setting up the numeric scale ...');
		var map = nisis.ui.mapview.map, 
		sVal = nisis.ui.widgets.numScale.scale, 
		sUnits = nisis.ui.widgets.numScale.units,
		sUnit = nisis.ui.widgets.numScale.unit,
		scale = map.getScale();
		if (sVal.length && sUnits.length) {
			//sVal.html(scale.toFixed(2));
			sVal.html(Math.round(scale));
			sUnits.html('Meters');
		} else {
			uiEls.setNumericScale();
		}
	};
	//update map numeric scale
	uiEls.updateNumericScale = function(evt){
		//console.log('Updating numeric scale ...',evt);
		var map = nisis.ui.mapview.map, 
		//element holding the map scale value
		sVal = nisis.ui.widgets.numScale.scale,
		//element holding the map scale units
		sUnits = nisis.ui.widgets.numScale.units, 
		//the current scale unit
		sUnit = nisis.ui.widgets.numScale.unit, 
		//get the map scale at the end of zoom
		scale = map.getScale();
		if (evt && evt.lod) {
			//scale can also be grabbed from the map if the event is being returned
			scale = evt.lod.scale;
		}
		//check if the scale value and units elements exist
		if (sVal.length && sUnits.length) {
			//convert the scale into to current units
			scale = Math.round(nisis.ui.mapview.util.convertLengthUnits(scale,sUnit));//.toFixed(2);
			//display the scale on the ui
			sVal.html(Math.round(scale));
		}
	};
	//display mouse position
	uiEls.displayMouseCoordinates = function() {
		var mouseCoords = $('#mouseLatLon'),
		mouseLatLonUnit = 'Lat: <span id="mouseLat"></span> / Lon: <span id="mouseLon"></span> (<span id="mCoordsUnit">Decimal Degrees</span>)';
		mouseCoords.html(mouseLatLonUnit);
		//export these elements to the rest of the application
		nisis.ui.widgets.mouseCoords = mouseCoords;
		nisis.ui.widgets.mouseCoords.lat = $('#mouseLat');
		nisis.ui.widgets.mouseCoords.lon = $('#mouseLon');
		nisis.ui.widgets.mouseCoords.units = $('#mCoordsUnit');
		nisis.ui.widgets.mouseCoords.unit = esri.Units.DECIMAL_DEGREES;
		//possible units
		var options = $("<select id='mcUnits'></select>"),
		data = [{
			label: 'Decimal Degrees',
			value: esri.Units.DECIMAL_DEGREES
		},{
			label: 'Meters',
			value: esri.Units.METERS
		},{
			label: 'Feet',
			value: esri.Units.FEET
		},{
			label: 'Kilometers',
			value: esri.Units.KILOMETERS
		},{
			label: 'Miles',
			value: esri.Units.MILES
		},{
			label: 'Nautical Miles',
			value: esri.Units.NAUTICAL_MILES
		}];
		//used a div to wrap primui listbox
		var opts = $("<div></div>");
		opts.appendTo(mouseCoords).append(options);
		options.puilistbox({
			data: data,
			itemSelect: function(evt,items){
				//console.log(evt,items);
				console.log(items[0].value);
				nisis.ui.widgets.mouseCoords.units.html(items[0].label);
				nisis.ui.widgets.mouseCoords.unit = items[0].value;
				opts.hide();
			}
		});
		//overwrite the size of pui listbox
		$(opts.find('.pui-listbox')).css({width: 'auto',height:'auto'});
		//add a click event to show unit options
		nisis.ui.widgets.mouseCoords.units.css('cursor','pointer')
		.on('click',function(evt){
			//uiEls.displayMessage('Changing mouse coords units is not implemented yet','Not ready!');
			opts.css({
				position: 'absolute',
				right: 0,
				bottom: '120%',
				zIndex: 9999
			}).toggle();
		});
		//hide the option once it's been built
		opts.hide();
	};
    //create dialog box
    uiEls.createDialogBox = function(id, title, content){
        if($('#'+id).length === 0){
            $("<div id='" + id + "' title='" + title + "'>" + content + "</div>").appendTo("body").dialog({
                width: 600,
                height: 400,
                modal: false,
                buttons: [
                	{
                    	text: "Export",
                    	click: function(){
                    	    nisis.ui.displayMessage("The report has not been generated yet","The report has not been generated yet",0);
                    	}
                    }, {
                    	text: "Print",
                    	click: function(){
                    		nisis.ui.displayMessage("The report has not been generated yet","The report has not been generated yet",0);
                    	}
                    }, {
                    	text: "Exit",
                    	click: function(){
                        	$(this).dialog("close");
                        }
                    }
                ]
            });
        } else {
            $('#'+id).dialog();
        }
    };
    //create layer tree 
    uiEls.createLayerTree = function(name, visible){
        var li = document.createElement('li');
        li.className = "ui-state-default";
        var check = document.createElement('input');
        check.type = 'checkbox';
        check.id = name + '-checkbox';
        check.className = "overlays";
		//check.setAttribute('checked', visible);
		$(check).attr('checked', visible);
        check.addEventListener('change', function(evt){
            var checkBox = evt.target;
			var layer = checkBox.id.split('-');
			layer = layer[0];
            if(checkBox.checked){
                nisis.ui.mapview.layers[layer].show();
            } else {
                nisis.ui.mapview.layers[layer].hide();
            }
        });
		var label = name.replace(/_/g,' ');
        var txt1 = document.createTextNode(label);
        var txt2 = document.createTextNode("  ");
        li.appendChild(check);
        li.appendChild(txt2);
        li.appendChild(txt1);
        var btn = document.createElement('button');
        btn.id = name + '-config';
        btn.className = "layerConfig";
        btn.innerHTML = label + " Config";
        li.appendChild(btn);
        $('#overlays-toggle').append(li);
        //overlays config icons
        $('#'+btn.id).css({
            'width':'17px',
            'height':'17px',
            'float': 'right'
        }).button({
            text: false,
            icons: {
                primary: "ui-icon-gear"
            }
        }).click(function(){
			//console.log(this);
            $(this).toggleClass('activeLayerConfig');
			if ($(this).hasClass('activeLayerConfig')) {
				uiEls.setTransLayer(this.id);
			} else {
				uiEls.setTransLayer();
			}
        });
    };
	//check/uncheck overlays checkboxes
	uiEls.changeLayerVisibility = function(lyrId, checked){
		//get the layer tree node
		var node = nisis.ui.mapview.layersTree.getNodeByParam('id',lyrId, null);
		//update the node check status if the layer visisbility has been change internally
		if(node){
			nisis.ui.mapview.layersTree.checkNode(node,checked);
		}
		
	};
	//display layer tansparency slider
	uiEls.setTransLayer = function(lyr){
		if (lyr) {
			if(lyr.length){
				var layers = [];
				$.each(lyr, function(idx, l){
					var layer = lyr.split('-');
					layer = layer[0];
					layers.push(nisis.ui.mapview.layers[layer]);
				});
				nisis.ui.mapview.transLayer = layers;
			} else {
				var layer = lyr.split('-');
				layer = layer[0];
				nisis.ui.mapview.transLayer = nisis.ui.mapview.layers[layer];
			}
		} else {
			nisis.ui.mapview.transLayer = null;
		}
	};
	//sort array of object
	var _sortArrayOfObjects = function(arr, prop){
		arr.sort(
			function(a, b){
				if (a[prop] < b[prop]){
	                return -1;
	            } else if (a[prop] > b[prop]){
	                return 1;
	            } else {
	                return 0;   
	            }
			}
		);
	};
	//sort array of json objects
	uiEls.sortArrayOfObjects = function(arr, prop){
		arr.sort(
			function(a, b){
				if (a[prop] < b[prop]){
	                return -1;
	            } else if (a[prop] > b[prop]){
	                return 1;
	            } else {
	                return 0;   
	            }
			}
		);
	};
	//populate Filter shapes
	uiEls.populateFilterShapes = function(listElId){
		var def = new $.Deferred();
		def.notify('Processing ...');
		var listEl = $('#'+listElId), 
		listElWrap = $('#'+listElId+'-wrap'),
		fsList = [], fsLayers = ['User_Polygon_Features'], //'User_Incident_Watch_Areas'
		//this function loads polygon features from layer
		loadGQP = function(layer){
			//query options
			var opts = {
				layer: layer,
				returnGeom: false,
				outFields: ['OBJECTID','NAME']
				,where: "FS_DESIGN = '1'"
			};
			//query request
			var getFSs = nisis.ui.mapview.util.queryFeatures(opts);
			getFSs.then(
			function(d){
				var fsList = [], 
				feats = d.features,
				len = feats.length;
				def.notify(len + ' FS(s) found.');
				if(feats.length > 0){
					$.each(feats,function(i,feat){
					   fsList.push({
							label: feat.attributes.NAME,
							value: feat.attributes.OBJECTID
						});
					});	
				} else {
					fsList.push({
						label: 'No FSs available.',
						value: ''
					});
				}
				var list = $("<select class='listBox' multiple='multiple'></select>");
				list.attr('id', listElId);
			
				listElWrap.empty();
				listElWrap.append(list);
			
				list.puilistbox({
					data: fsList
				});
			},
			function(e){
				def.reject(e.message);
			},function(p){
				def.notify(p);
			});
		};
		//loop thru fslayers (IWAs or any other polygons)
		$.each(fsLayers, function(idx,fsLyr){
			var layer = nisis.ui.mapview.layers[fsLyr];
			if (!layer.visible) {
				layer.setVisibility(true);
				//wait for the layer to be visible and then load GQPs
				while (!layer.visible) {
					loadGQP(layer);
				}
			}
			else {
				loadGQP(layer);
			}
		});
		return def.promise();
	};
	//populates field values
	uiEls.populateFieldValues = function(lyrId, fieldsEl, valuesEl){
		var layer, field, fieldType, values = [], fEl = $('#'+fieldsEl), 
		vEl = $('#'+valuesEl), vElWrap = $('#'+valuesEl+'-wrap');
		var newlb = $("<select id='fField-values' class='dropDown'></select>");
		if(lyrId !== '' && lyrId !== '*'){
			layer = nisis.ui.mapview.map.getLayer(lyrId);
			field = fEl.val(); 
			console.log(layer.url, field);
			
			$.each(layer.fields, function(idx,f){
				if(f.name == field){
					fieldType = f.type;
				}
			});
			//values = nisis.ui.mapview.util.getFieldValues(layer.url, field);
			//console.log(values);
			var query = new esri.tasks.Query();
			query.returnGeometry = false;
			query.outFields = [field];
			
			var queryTask = new esri.tasks.QueryTask(layer.url);
			
			queryTask.execute(query, function(results){
				var features = results.features;
				console.log(features);
				$.each(results.features, function(idx,feat){
					var v = feat.attributes[field];
					
					if ($.inArray(v, values) == -1) {
						//console.log(fieldType);
						if(fieldType == 'esriFieldTypeDate'){
							var d = new Date(v);
							d = d.getMonth()+1 + '/' + d.getDate() + '/' + d.getFullYear();
							v = d;
						}
						values.push(v);
					}
				});
				console.log(values);
				vElWrap.empty().append(newlb);
				$(newlb).puilistbox({
					data: values
				});
			}, function(error){
				console.log(error.message);
				values.push(error.message);
				vElWrap.empty().append(newlb);
				$(newlb).puilistbox({
					data: values
				});
			});
		} else {
			values.push('Select a layer');
			vElWrap.empty().append(newlb);
			$(newlb).puilistbox({
				data: values
			});
		}
		
	};
	//polulate fields
	uiEls.populateFields = function(lyrId, fieldsEl){
		var layer, source, fEl = $('#'+fieldsEl), fElWrap = $('#'+fieldsEl + '-wrap');
		//recreate a dropdown with new options
		var newdd = $("<select id='filterLayersFields' name='filterLayersFields' class='dropDown'></select>");	
		if ((lyrId !== "") && (lyrId !== "*")){
			source = [];
			layer = nisis.ui.mapview.map.getLayer(lyrId);
			$.each(layer.fields, function(i, field) {
				source.push({
					label: field.alias.replace(/_/g, ' '),
					value: field.name
				});
			});
		} else {
			source = [];
			source.push({
				label: 'Select a field',
				value: ''
			});
		}
		//sort the options based on layer
		_sortArrayOfObjects(source, 'label');
		//remove the old dd and append the new one
		fElWrap.empty().append(newdd);
		$(newdd).puidropdown({
			data: source
		});	
	};
	//populate layers drop down list //==> the function takes an array element ids
	uiEls.populateLayers = function(layers){
	    
		if(!layers){
			throw new Error('Array of layers loaded required. None passed.');
		}
		if(!layers.length || layers.length === 0){
			throw new Error('Layers is not an array or is empty');
		}
		//get IWLs
		var len = layers.length;
		var tblLyrs = [];
		tblLyrs.push({name:'Select a layer', value:"", iwl:''});
		tblLyrs.push({name:'All NISIS Tracked Objects', value:"All", iwl:''});
		//console.log(tblLyrs);
		//populate dd
		$.each(layers,function(i,lyr){
			if(lyr.success && lyr.layer.type == 'Feature Layer'){
				var label = lyr.layer.id.replace('_', '__').split('__')[1].replace(/_/g, ' '),
				value = lyr.layer.id;
				//create new select option
				var option = new Option(label,value);
				if(value.toLowerCase().indexOf('nisisto') !== -1){
					//$('#tbl-lyrs').append(option);
					//nisis.ui.mapview.tableLayers.push({name:label, value:value});
					//tableview.layers.push({name:label, value:value});
					tblLyrs.push({name:label, value:value, iwl:''});
				}else{
					//$('#f-lyrs').append(option);
				}
			}
			if(i === len-1){
			    //console.log('Tableview Layers: ', tblLyrs);
			    $('#tbl-lyrs').data('kendoDropDownList').dataSource.data(tblLyrs);
				// var getIWL = uiEls.populateIWL(nisis.user.username,nisis.user.groups);
				var getIWL = uiEls.populateIWL();
				getIWL.then(
				function(d){
				    //console.log(d);
				    nisis.ui.displayMessage(d.length + ' IWL(s) Retreived');
					$.each(d,function(i,opt){
						var iwlname = opt.NAME,
						label = 'IWL - ' + iwlname.replace(/_/g, ' ');
						var layer = opt.LAYER;
						var value = 'iwl-' + layer;
						var iwl = new Option(label, value);
						//$(iwl).attr('data-iwlname',iwlname);
						//$('#tbl-lyrs').append(iwl);
						//nisis.ui.mapview.tableLayers.push({name:label, value:value, iwl:iwlname});
						tblLyrs.push({name:label, value:value, iwl:iwlname});
					});
					//console.log(tblLyrs);
					$('#tbl-lyrs').data('kendoDropDownList').dataSource.data(tblLyrs);
				},function(e){
				    console.log(e);
				    $('#tbl-lyrs').data('kendoDropDownList').dataSource.data(tblLyrs);
					uiEls.displayMessage(e.message,'IWL Error!','error');
				});
			}
		});
		//console.log(tblLyrs);
	};
	//create an IWL
	uiEls.createIWL = function(layer,name,desc,oids){
		var def = new $.Deferred();

		if(!layer || !name || !desc || !oids){
			def.reject('Missing arguments for the request.');
			console.log('Missing arguments for the request.');
		}

		if(layer === "all_objects"){
			def.reject("Cannot create an IWL for multiple object types. Please select a different layer in the table.");
			return def.promise();
		}

		//chunk the object id's before ajax calls - avoid PHP max input limit
		var chunkSize = 900;
		var start = 0;
		var end;
		var chunkedOids = [];
		while(start < oids.length){
			end = start + chunkSize;
			chunkedOids.push(oids.slice(start, end));
			start = end;
		}
		console.log("chunked oids: ", chunkedOids);

		//create overall iwl
		$.ajax({
			url : 'api/',
			type : 'POST',
			data : {
				action: 'create_iwl',
				username: nisis.user.username,
				iwl_name: name,
				iwl_desc: desc,
				iwl_layer: layer,
				//iwl_ids: oids
			},
			success : function(data, status, xhr) {
				//console.log(data);
				var d = JSON.parse(data);
				if(d.error || d.error === null){
					def.reject(d.error);
					//console.log(d.error);
				} else {
					//our IWL exists, now add the objects - one chunk at a time if there's too many
					$.each(chunkedOids, function(index, curChunk){
						console.log("IWL id: ", d.iwlid);
						$.ajax({
							url: 'api/',
							type: 'POST',
							data: {
								action: 'add_to_iwl',
								iwl_id: d.iwlid,
								iwl_layer: layer,
								objects: curChunk
							},
							success: function(data, status, xhr) {
								var resp = JSON.parse(data);
								if(resp.error) {
									console.log("ERROR with all or some objects being added to new IWL: ", resp.error);
									def.reject(resp.error);
								} else {
									console.log(resp);
									def.resolve(resp.result);
								}
							},
							error: function(xhr, status, err) {
								console.log(err);
								def.reject(err);
							}
						}); //end inner ajax to add IWL oids
					});
				}
			},
			error : function(xhr, status, err) {
				data.reject(err);
				console.log(err);
			}
		}); //end outer ajax to create IWL
		
		return def.promise();
	};
	//populate user IWLs
	uiEls.populateIWL = function(){
		var def = new $.Deferred();
		
		def.notify('Retreiving IWLs ...');

		//only get iwl's for selected layer type in table view
		var filter = "NONE";
		switch($('#tbl-lyrs').data('kendoDropDownList').value())
		{
			case "airports_pu":
			case "airports_pr":
			case "airports_others":
			case "airports_military":
			case "airports":
				filter = "airports";
			break;
			case "atm":
				filter = "atm";
			break;
			case "ans1":
			case "ans2":
			case "ans":
				filter = "ans";
			break;
			case "osf":
				filter = "osf";
			break;
			//NISIS-2754 - KOFI HONU
			// case "teams":
			// 	filter = "teams";
			// break;
			case "tof":
				filter = "tof";
			break;
			default:
			    filter = $('#tbl-lyrs').data('kendoDropDownList').value();
		}
		$.ajax({
			url : 'api/',
			type : 'POST',
			data : {
				action: 'get_iwls',
				filter: filter,
			},
			success : function(data, status, xhr) {
				//console.log('IWL Names: ', data);
				var resp;
				try {
					resp = JSON.parse(data);
				}
				catch(err){
					def.reject(err.message);
				}
				//console.log(resp);
				if(resp && resp['return'] && resp['return'] === 'Success'){
					def.resolve(resp.results);
				} else {
					def.reject('Something went wrong while retreiving IWL names');
				}
			},
			error : function(xhr, status, err) {
				//console.log(err);
				def.reject(err);
			}
		});
		//return promise
		return def.promise();
	};
	//get oids in an IWL
	uiEls.getIncidentWatchList = function(options /*IWLname, Layer*/){
		var def = new $.Deferred();
		//validate input
		if(!options){
			throw new Error("Function 'getIncidentWatchList' requires options. None passed.");
		}
		//prepare request
		def.notify('Retreiving IWLs ...');
		//request input
		var data = {
			action: 'get_iwl_objectids',
			username: nisis.user.username,
			name: options.name,
			layer: options.layer
		};
		//get list of IWL Objects
		$.ajax({
			url : 'api/',
			type : 'GET',
			data : data,
			success : function(data, status, xhr) {
				console.log('IWL Objects: ', data);
				var resp = null;
				try {
					resp = JSON.parse(data);
				}
				catch(err){
					console.log('Error retrieving IWL Objects: ', err);
					def.reject(err);
				}

				if(resp && resp['return'] && resp['return'] === 'Success'){
					var oids = $.map(resp.results.OBJECTID,function(id,i){
						return parseInt(id,10);
					});
					def.resolve(oids);
				} else {
					def.reject('Something went wrong while retreiving IWL object ids');
				}
			},
			error : function(xhr, status, err) {
				console.log(err);
				def.reject(err);
			}
		});
		//return promise
		return def.promise();
	};
	//disable zoom box
	uiEls.disableZoomBoxes = function() {
		$('#nav-zoominbox').off('click');
		$('#nav-zoomoutbox').off('click');
	};
	//enable zoom box
	uiEls.enableZoomBoxes = function() {
		$('#nav-zoominbox').on('click', function(){
			$(this).css('border-color','#666')
			.siblings().css('border-color','transparent');
			nisis.ui.mapview.navTools.activate(esri.toolbars.Navigation.ZOOM_IN);
		});
		$('#nav-zoomoutbox').on('click', function(){
			$(this).css('border-color','#666')
			.siblings().css('border-color','transparent');
			nisis.ui.mapview.navTools.activate(esri.toolbars.Navigation.ZOOM_OUT);
		});
	};
	//tableview
	uiEls.rebuildTable = function(lyr,userOptions) {
		//console.log(userOptions);
		var uOptions = userOptions;
		$('#tableview-div').spin(true);
		var layer = lyr;
		if (!layer) {
			if(nisis.ui.currentLayer){
				layer = nisis.ui.currentLayer;
			} else if($.trim(nisis.ui.widgets.tableview.panel.layers.val() !== "")) {
				//attempt to get the layer directly from the tbl layers dropdown
				var id = nisis.ui.widgets.tableview.panel.layers.val();
				if(id.indexOf('iwl-') !== -1){
					id = id.substr(4);
				}
				layer = nisis.ui.mapview.layers[id];
			} else {
				return;
			}
		}
		nisis.ui.currentLayer = layer;
		
		var tblOptions = $('#tbl-options'),
		tblLimit = $('#tbl-map-extent'),
		tblGQPs = $('#tbl-fs-list'),
		tblGQPsUnion = $('#tbl-unionGQPs'),
		tblMsg = $('#tbl-filter-msg'),
		tblExports = $('#tbl-exports').empty(),
		tblHeaders = $('#tbl-select-fields').empty(),
		tblColumns = $('#tbl-columns').empty(),
		tblWrapper = $('#tablewrapper'),
		table = $('#table').empty();

		var download = $('<a id="dataDownload" download="NISISData.csv" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 12px;">Export</a>')
		;//.appendTo(header);
		var buttonHTML = '<select id="buttons">';
		//subset of features for delayed rendering
		var chunk = 999, features2; 
		//use this for AMD callback
		var buildTable = function(layer,options){
			var opts = {
				returnGeom: false
			};
			if(layer){
				opts.layer = layer;
			} else {
				return;
			}
			if(uOptions.objectIds){
			    opts.objectIds = uOptions.objectIds;
			}
			if(options){
				$.extend(opts,options);
			}
			
			var start = false, end = false;
			//query features from server
			var getData = nisis.ui.mapview.util.queryFeatures(opts);
			getData.done(function(data){
				if(!start){
					start = true;
					processData(data);
				}else{
					loadMoreData(data.features);
				}
				end = true;
			});
			getData.fail(function(error){
				console.log(error);
				tblMsg.html(error).css('color','red');
				$('#tableview-div').spin(false);
				return;
			});
			getData.progress(function(update){
				if(update.data && start){
					loadMoreData(update.data.features);
				} else if (update.data && !start){
					start = true;
					processData(update.data);
				}else{
					tblMsg.html(update).css('color','black');
				}
			});
			var processData = function(data){
				//console.log(data);
				var fields = data.fields,
				fLen = fields.length,
				features = data.features,
				ftLen = features.length;
				tblMsg.html("Processing " + ftLen + ' features ...').css('color','black');
				var d = new Date();
				//console.log(ftLen + " rows received at:" + d);
				var features1;
				if(ftLen > chunk){
					features1 = features.slice(0,chunk);
					features2 = features.slice(chunk);
				}else{
					features1 = features;
				}
				var ftLen1 = features1.length, columns = [], rows = [], 
				tHead = "<thead><tr>", tBody = "<tbody>", tFoot = "<tfoot><tr>",
				headers = "<option value=''>Select a field ...</option>";
				$.each(fields,function(i,field){
					tHead += "<th>" + field.alias + "</th>";
					//tFoot += "<th>" + field.alias + "</th>";
					headers += "<option value='" + field.alias + "'>" + field.alias + "</option>";
					/*columns.push({
						'sTitle': field.alias,
						'mData': field.name
					});*/
					if(i == fLen-1){
						tHead += "</tr></thead>";
						//tFoot += "</tr></tfoot>";
						tblHeaders.append(headers);
						$.each(features1,function(j,feat){
							//console.log(features1[0]);
							var tr = "<tr>";
							$.each(feat.attributes,function(key,attr){
								var value = attr == 'null' ? '' : attr;
								if(key === 'OBJECTID'){
									value = '<span data-oid="' + value 
									+ '" class="tbl-profile-link" title="Click to open profile">' 
									+ value + '</span>';
								}
								if(value === null){
									value = '';
								}
								tr += "<td>" + value + "</td>";
							});
							tr += "</tr>";
							tBody += tr;
							if(j == ftLen1-1){
								tBody += "</tbody>";
								//initialize the table
								initDataTable(columns,rows,tHead,tBody,tFoot);
								var d = new Date();
								//console.log(ftLen1 + " rows initialized:" + d);
							}
						});
					}
				});
			};
			//adjust table scroll height
			var adjustScrollHeight = function(){
				var tblWrap = $('.dataTables_wrapper')[0],
				tblHeader = $('.dataTables_wrapper>.fg-toolbar.ui-widget-header')[0],
				tblScrollH = $('.dataTables_wrapper .dataTables_scrollHead')[0],
				tblScroll = $('.dataTables_wrapper>.dataTables_scroll')[0],
				tblScrollF = $('.dataTables_wrapper .dataTables_scrollFoot')[0],
				tblFooter = $('.dataTables_wrapper>.fg-toolbar.ui-widget-header')[1];
				
				var thH = $(tblHeader).height(),
				tfH = $(tblFooter).height(),
				tsH = $(tblScroll).height(),
				tshH = $(tblScrollH).height(),
				tsfH = $(tblScrollF).height();
				
				var exH =  (thH + tfH + tshH + tsfH + 5),
				wrapH = $(tblWrap).height(),
				sY = (wrapH - exH) + 'px';
				
				$('.dataTables_wrapper .dataTables_scrollBody').css('height',sY);
			};
			//init table
			var initDataTable = function(columns,rows,tHead,tBody,tFoot){
				if (nisis.ui.tableView && nisis.ui.tableView.hasOwnProperty("fnDestroy")) {
					nisis.ui.tableView.fnDestroy(true);
					var tbl = $("<table id='table' class='tablesorter'></table>");
					tblWrapper.append(tbl);
					table = tbl;
					table.append(tHead,tBody,tFoot);
				} else {
					table.append(tHead,tBody,tFoot);
				}
				//build the table
				nisis.ui.tableView = table.dataTable({
					"sDom": 'WTR<"H"lfip>t<"F"r>',
					"bDeferRender": false,
					'bJQueryUI': true,
					"bProcessing": true,
					"sPaginate": true,
					"sPaginationType": 'full_numbers',
					"bAutoWidth": false,
					'bScrollCollapse': true,
					"sScrollY": ($('#tablewrapper').height() - 170),
					"sScrollX": '100%',
					"sScrollXinner": '110%',
					"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
					"oLanguage": {
						"sLengthMenu": "Display _MENU_ records",
						"sInfo": "Displaying _START_ to _END_ of _TOTAL_ records",
						"oPaginate": {
							"sFirst": " |<< ",
							"sPrevious": " << ",
							"sNext": " >> ",
							"sLast": " >>| "
						}
					},
					"oTableTools": {
						//"aButtons": ['csv','xls','pdf'],
						"aButtons": [{'sExtends': 'xls','sButtonText': 'Export as Excel'}],
						"sRowSelect": 'single',
						"sRowSelectedClass": 'row_selected',
						"fnRowSelected": function(nodes){
							//console.log(nodes);
						},
						"fnRowDeselected": function(nodes){
							//console.log(nodes);
						}
					},
					"oColumnFilterWidgets": {
						//sSeparator: ', ',
						//bGroupTerms: true
					},
					"fnInitComplete": function(oSettings){
						var lyr = nisis.ui.currentLayer;
						$('.tbl-profile-link').on('click',function(){
							var feature = {
								oid: $(this).data('oid'),
								layer: layer
							};
							nisis.ui.mapview.util.buildFeatureProfile(feature);
							//console.log('Opening profile for oid: ' + $(this).data('oid'));
						});
						//console.log(oSettings);
						tblMsg.html('Tableview has been initialized.').css('color','black');
						//readjust table headers
						var hWidths = [];
						$(".tablesorter thead tr th, .tablesorter tfoot tr th").each(function(el){
							$(this).css("min-width","100px");
							var w = $(this).css('width');
							//console.log(w);
							w = parseInt(w.split('px')[0],10) + 50 + 'px';
							hWidths.push(w);
							$(this).css("width",w);
							
							var iCol = $('th',this.parentNode.parentNode).index(this.parentNode) + 1;
							$('td:nth-child('+iCol+')', table.$('tbody tr')).css('width',w);
						});
						
						$(".tablesorter tbody tr").each(function(i,tr){
							$(tr).find('td').each(function(j,td){
								//console.log('Row '+ i + ': ' + hWidths[j]);
								$(td).css('width', hWidths[j]);
							});
						});
						//add mouse hover effects
						$(".tablesorter tbody tr td").hover(function(){
							var iCol = $('td', this.parentNode).index(this);
        					$('td:nth-child('+(iCol+1)+')', table.$('tbody tr')).addClass('highlighted_row');
							$('td', this.parentNode).addClass('highlighted_row');
						},function(){
							$(".tablesorter tbody tr td.highlighted_row").removeClass('highlighted_row');
						});
						//adjust column sizing
						setTimeout(function(){
							//nisis.ui.tableView.fnAdjustColumnSizing();
							//nisis.ui.tableView.fnDraw();
						},100);
						//move filter divs to panel
						tblExports.append($('#table_wrapper').find($('.DTTT_container')[0]));
						tblColumns.append($("#table_wrapper").find('.column-filter-widgets'));
						//give a bootstrap look to attr names
						$('.column-filter-widget select').addClass('form-control');
						$('.dataTables_filter, .dataTables_filter label').css('margin-bottom', '0px');
						$('.dataTables_filter input').addClass('form-control');
						//add table toggle icons
						var toggle = $("<span><span class='ui-corner-all glyphicon glyphicon-resize-horizontal'></span></span>")
						.css({
							'margin': '2px 10px 0 0',
							'padding': '2px',
							'cursor': 'pointer',
							'border-width': '1px',
							'border-color': '#222222',
							'border-style': 'solid',
							'border-radius': '3px'
						}).click(function(evt){
							var panel = $('#tbl-panel'), 
							tbl = $('#tablewrapper');
							panel.slideToggle(400, function(){
								if(panel.is(':visible')){
									tbl.animate({
										'left': '305px'
									},500);
								}else{
									tbl.animate({
										'left': '0px'
									},500);
								}
							});
						});
						//
						$('.dataTables_length').prepend(toggle);
						//adjust table height
						adjustScrollHeight();
						tblOptions.show();
						//hide spinner
						$('#tableview-div').spin(false);
						//add more rows if any
						if (features2) {
							loadMoreData(features2);
						}
						if(end){
							tblMsg.html('Done processing tableview data.').css('color','black');
						}
					}
				});
			};
		};
		//
		var loadMoreData = function(features){
			var feats = features ? features : features2;
			var len = feats.length;
			tblMsg.html('Processing ' + len + ' more rows.').css('color','black');
			var d = new Date();
			//console.log("Processing the rest:" + d);
			if(nisis.ui.tableView){
				setTimeout(function(){
					var tOpts = nisis.ui.tableView.fnSettings();
					//console.log(tOpts);
					var rows = [];
					$.each(feats,function(i,feat){
						var row = [];
						$.each(feat.attributes, function(j, value){
							row.push(value);
						});
						rows.push(row);
						if(i === len-1){
							tblMsg.html('Loading ' + len + ' more rows.').css('color','black');
							nisis.ui.tableView.fnAddData(rows);
							var d = new Date();
							console.log("Done done with the rest:" + d);
							tblMsg.html('Loaded ' + len + chunk + ' more rows.').css('color','black');
						}
					});
					if(end){
						tblMsg.html('Done processing tableview data.').css('color','black');
					}
				},500);
			}else{
				tblMsg.html('Error processing ' + len + ' rows. Wait ...').css('color','red');
				setTimeout(function(){
					loadMoreData();
				},1000);
			}
		};
		//get query geometry
		var getFilterShapes = function(options){
			var opts, where = "", fs, outFields,
			gqps = tblGQPs.val();
			if(options.filterShapes){
				fs = options.filterShapes;
			}
			if(options.outFields){
				outFields = options.outFields;
			}else{
				outFields = ["*"];
			}
			if(fs && fs.length && fs.length > 0){
				var len = fs.length;
				$.each(fs,function(i,id){
					if (i == len - 1) {
						where += 'OBJECTID = ' + id;
					} else {
						where += 'OBJECTID = ' + id  + ' OR ';
					}
				});
				//
				opts = {
					layer: nisis.ui.mapview.layers.User_Polygon_Features,
					returnGeom: true,
					where: where
				};
				//
				var geoms = nisis.ui.mapview.util.queryFeatures(opts);
				geoms.then(
				function(d){
					var feats = d.features, geom, 
					len = feats.length;
					if(len > 0 && len == 1){
						geom = feats[0].geometry;
						var opt = {
							geom: geom,
							outFields: outFields
						};
						//console.log(options);
						if(options.objectIds){
						    //console.log(options.objectIds);
							opt.objectIds = options.objectIds;
						}
						buildTable(nisis.ui.currentLayer,opt);
					} else {
						var geoms = [];
						$.each(feats,function(i,feat){
							if (i === 0) {
								geom = feat.geometry;
							}else{
								geoms.push(feat.geometry);
							}
						});
						if(options.union){
							geoms.push(geom);
							var union = nisis.ui.mapview.util.unionGeometries(geoms);
							union.then(
							function(geom){
								//console.log(geom);
								var opt = {
									geom: geom,
									outFields: outFields
								};
								if(options.objectIds){
									opt.objectIds = options.objectIds;
								}
								buildTable(nisis.ui.currentLayer,opt);
							},
							function(e){
								console.log(e);
								tblMsg.html(e.message).css('color','red');
								$('#tableview-div').spin(false);
							},
							function(p){
								console.log(p);
								tblMsg.html(p).css('color','green');
							});
						} else {
							var intersect = nisis.ui.mapview.util.intersectGeometries(geoms, geom);
							intersect.then(
							function(geoms){
								//console.log(geoms);
								var geom;
								if(geoms && geoms.length & geoms.length > 0){
									geom = geoms[0];
								}else if(geoms && !geoms.length){
									geom = geoms;
								}
								var opt = {
									geom: geom,
									outFields: outFields
								};
								if(options.objectIds){
									opt.objectIds = options.objectIds;
								}
								buildTable(nisis.ui.currentLayer,opt);
							},
							function(e){
								console.log(e);
								tblMsg.html(e.message).css('color','red');
								$('#tableview-div').spin(false);
							},
							function(p){
								console.log(p);
								tblMsg.html(p).css('color','green');
							});
						}
					}
				},function(e){
					console.log(e);
					$('#tableview-div').spin(false);
				},function(p){
					console.log(p);
				}
				);
			} else {
				buildTable(nisis.ui.currentLayer,{
					geom: options.geom,
					outFields: outFields
				});
			}
		};
		getFilterShapes(userOptions);

		$("a#dataDownload").attr("href", "#");
	};
	//create contextual menu for features
	uiEls.createMapSubMenu = function(evt) {
		var host = window.location.host,
			navTool = mapConfig.navActiveTool,
			map = evt.target.id !== "" ? true : false;

		/**
		 * Preventing evt default or stopping propagation introduce a bug.
		 * Check event target in order to determine if it's map or feature event
		 */
		if (!map) {
			return;
		}
		
		//no sub menu with active zoom in/out box
		if (navTool !== 'pan') {
			var msg = navTool.toUpperCase();
			msg += " is active. Sub menus are displayed only with active PAN tool.";
			nisis.ui.displayMessage(msg, 'error');
			return;
		}

		var mapSubMenu = $("<ul></ul>");
		var cluster = $('<li>Show Cluster</li>');
		var copy = $('<li>Copy Features</li>');
		var pt = $('<li>Add Point</li>');
		var buffer = $('<li>Add Buffer</li>');
		var sa = $('<li>Add Service Area</li>');
		var gInfo = $('<li/>', {
			text: "Copy Coordinates"
		});

		var tab,
			actGeomTab = $('#draw-options-div').data('kendoTabStrip').select();

		if (actGeomTab) {
			tab = $(actGeomTab[0]).attr('id');
		} else {
			tab = null;
		}
		
		// Show Cluster submenu only appears if either ANS Systems checkboxes are checked
		var	ANSchecked = nisis.ui.mapview.layers.ans.visible, 
			ANS1checked = nisis.ui.mapview.layers.ans1.visible,
			ANS2checked = nisis.ui.mapview.layers.ans2.visible;

		if(ANSchecked || ANS1checked || ANS2checked){			
			mapSubMenu.append(cluster);
		}	

		if ( host && host === 'localhost' ) {
			//if ( $('#drawing-div').is(':visible') && tab && tab === 'enter-vertices' ) {
			if ( $('#drawing-div').is(':visible') ) {
				mapSubMenu.append(gInfo);
			}
		}

		mapSubMenu.kendoMenu({
			orientation: 'vertical'
		});
		//map point
		var mPt = evt.mapPoint,	
			off = null;
		
		var tooltip = $('#map')
		.kendoTooltip({
            width: 'auto',
            height: 'auto',
            position: "bottom",
            content: $(mapSubMenu),
            show: function(e){
            	//checking content in $(this).content
            	//tooltip's innerHTML
            	var tthtml = $('.k-tooltip-content').find('ul[data-role="menu"]').html();



            	off = $(this.popup.wrapper).width() / 2;
            	var $this = this;
            	if(tthtml !== "")
	            	$(this.popup.wrapper).css({
	            		top: evt.clientY,
	            		left: evt.clientX - off
	            	});
            },
            hide: function(e){
            	this.destroy();
            }
        }).data("kendoTooltip");
		//handle user action
        $(cluster).click(function(e){
        	tooltip.destroy();
			//console.log('Show Clustered objects @ ', mPt.y + ' / ' + mPt.x);
			nisis.ui.mapview.util.queryCluster(mPt, [evt.clientY, evt.clientX]);
		});

		$(copy).click(function(e){
        	tooltip.destroy();
			var msg = "This feature is still in development";
			nisis.ui.displayMessage(msg, '','error');
		});

		$(gInfo).click(function(e){
			tooltip.destroy();

			require(['app/windows/shapetools',
				"esri/geometry/webMercatorUtils"]

			, function(shapetools, webMercatorUtils){

				var geoPt = webMercatorUtils.webMercatorToGeographic(mPt),
					ll = geoPt.y + "," + geoPt.x;

				shapetools.set('copiedCoordinates', ll);
				//fire copy event
				$('#dt-copy-coords').select();
				$('#dt-copy-coords').trigger('copy');
			});
		});

        tooltip.show();
	};
	//create contextual menu for map
	uiEls.createFeatureSubMenu = function(evt) {
		var $evt = evt,
			navTool = mapConfig.navActiveTool;

		if(!evt.which || evt.which !== 3) {
			return;
		}
		
		//no sub menu with active zoom in/out box
		if (navTool !== 'pan') {
			var msg = navTool.toUpperCase() + " is active. Sub menus are displayed only with active PAN tool.";
			nisis.ui.displayMessage(msg, 'error');
			return;
		}

		//avoid displaying map and feature submenu a the same time
		evt.preventDefault();
		evt.stopPropagation();
		
		//start creating feature sub menu
		var target = $(evt.graphic.getDojoShape().getNode()),
			gid = evt.currentTarget.id,
			id = evt.currentTarget.id.split('_layer')[0],
			graphic = evt.graphic,
			gType = graphic.geometry.type,
			layer = graphic.getLayer(),
			group = layer.group;

		if( !layer ) {
			return;
		}
		//clear selection before show sub menu
		var node = graphic.getNode(),
			editable = false,
			opt = null,
			msg = 'Sub menu can not be displayed on editable features.';
			msg += ' Click out and right click to get the sub menu.';

		if ( layer.isEditable ) {
			editable = layer.isEditable();
		}

		if ( !editable ) {
			if (layer.id.toLowerCase() === 'user_graphics') {
				opt = nisis.ui.mapview.edit.tools.getCurrentState();

				if (opt.tool !== 0) {
					uiEls.displayMessage(msg, '', 'error');
					return;
				}

			} else {
				nisis.ui.mapview.featureEditor.drawingToolbar.deactivate();
				nisis.ui.mapview.featureEditor._clearSelection();
				layer.refresh();
			}
			
		} else {
			opt = nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.getCurrentState();
			var selFeats = layer.getSelectedFeatures();
			//console.log(selFeats);

			if ( opt.tool !== 0 ) {
				/*nisis.ui.mapview.featureEditor.drawingToolbar.deactivate();
				nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.deactivate();
				nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar._disableMove();
				nisis.ui.mapview.featureEditor._editToolbar._clear();
				nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar._endOperation();
				nisis.ui.mapview.featureEditor._clearSelection();
				layer.refresh();

				$(node).trigger('click');
				$(nisis.ui.mapview.map).trigger('click')*/
				//nisis.ui.mapview.map.emit('click', {target: $('#map_gc')});
				
				//clear feature selection
				$('#btnClearSelection').trigger('click');
				layer.refresh();
				uiEls.displayMessage(msg, '', 'error');
				return;
			}
		}

		//if groups layers are 'nisisto' or 'dot' return nothing when right clicking on the layers
		if(layer.group === 'nisisto' || layer.group === 'dot') {
			//display clusters instead 
			uiEls.createMapSubMenu(evt);	
			return;
		}
		//console.log(evt,graphic,target);
		var featureSubMenu = $("<ul id='featuresSubMenu'></ul>");
		var	create = $("<li id='create'>Create Feature</li>"),
		createOptions = $("<ul id='createShapeOptions'></ul>"),
		buffer = $("<li id='buffer'>Create Buffer</li>");
		$(buffer).on('click', function(){
			nisis.ui.displayMessage('Creating Buffers is not implemented.','','info');
			//nisis.ui.mapview.util.createBuffer(graphic.geometry);
		});

		var	sa = $("<li>Service Area</li>");
		$(sa).on('click', function(){
			nisis.ui.displayMessage('Creating SAs is not implemented.','','info');
			//nisis.ui.mapview.util.createServiceArea(graphic.geometry);
		});
		createOptions.append(buffer,sa);
		create.append(createOptions);
			
		var	edit = $("<li id='edit'>Edit Feature</li>"),
		editOptions = $("<ul id='editOptions'></ul>"),
		other = $("<li id='other'>Other Operations</li>"),
		otherOptions = $("<ul id='otherOptions'></ul>"),
		order = $("<li id='order'>Arrange Order</li>"),
		orderOptions = $("<ul id='orderOptions'></ul>"),
		shape = $("<li>Edit Shape</li>");
		shape.on('click', function(){
			tooltip.destroy();
			if(layer.group !== 'nisisto'){
				if (layer.selectFeatures) {
					layer.selectFeatures(graphic);
				}
				var tools = esri.toolbars.Edit.MOVE | esri.toolbars.Edit.EDIT_VERTICES 
						| esri.toolbars.Edit.ROTATE | esri.toolbars.Edit.SCALE;

				var options = {
					allowAddVertices: true,
					allowDeleteVertices: true,
					allowScaling: true
				};

				if (layer.id.toLowerCase() === 'user_graphics') {
					nisis.ui.mapview.edit.tools.activate(tools, graphic, options);
				} else {
					nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.activate(tools,graphic);
				}
			} else {
				nisis.ui.displayMessage('Editing shapes is not allowed for ' + layer.name, '', 'error');
			}
		});

		var	attr = $("<li>Attributes</li>");
		attr.on('click',function(){
			tooltip.destroy();
			nisis.ui.displayMessage('Left click on the feature to display attributes.','','info');
		});

		var style = $("<li>Format Feature</li>");
		style.on('click',function(){
			tooltip.destroy();
			if(layer.group !== 'nisisto'){
				//select the layer and the selection handler will take care of the rest
				//layer.selectFeatures(graphic);
				require(['app/windows/shapetools'],function(shapetools){
					shapetools.set('styleFeature', graphic);
					shapetools.setStylesForm();
				});

				//open the form window for the user
				var shapeToolsWindow = $('#drawing-div').data('kendoWindow');
				shapeToolsWindow.title("Format Feature");
				shapeToolsWindow.open();

				if (!appConfig.checkIfHandlerExists(shapeToolsWindow, "close", "onShapeToolsClosed")){
					shapeToolsWindow.bind("close", appConfig.onShapeToolsClosed);
				}

				$('#draw-options-div').data('kendoTabStrip').activateTab($('#styles'));

				//NISIS-2049: CC - Showing the TabStrip ul elements because they may be hidden when called from Edit Feature (Feature Manager) and hiding the "styles" tab in drawing window because here is no editing features.
				appConfig.showHideStylesFeatureTab("show");
				
			} else {
				nisis.ui.displayMessage('Editing styles is not allowed for ' + layer.name,'Styles Failed!','error');
			}
		});
		
		editOptions.append(shape,attr,style);
		edit.append(editOptions);

		var saveAs = $("<li id='copy-feature'>Copy Feature</li>");
		$(saveAs).on('click', function(evt){
			tooltip.destroy();
			//clone the target graphic
			var copyFeat = $.extend({},graphic);
			//console.log('Original graphic to user features: ',graphic);
			//console.log('Copying graphic to user features: ',copyFeat);
			//nisis.ui.mapview.map.setExtent(nisis.ui.mapview.util.getGraphicsExtent([graphic]), true);
			nisis.ui.mapview.util.addFeatureToUserLayers(copyFeat)
			.then(function(newFeats){
				if( newFeats.length > 0 && newFeats[0].success ) {
					var msg = 'Your feature was copied successfully. Please Update attributes.';
					nisis.ui.displayMessage(msg,'info');

				} else {
					var msg2 = 'Your feature was not copied. Please try again.';
					nisis.ui.displayMessage(msg2, 'error');
				}
			},function(e){
				
				var error = e.message ? e.message : e;
				nisis.ui.displayMessage(error, 'error');
				$('.dijitProgressBar.dijitProgressBarEmpty.dijitProgressBarIndeterminate.progressBar').hide();
			});
		});

		//cut out features
		var cutFeats = $("<li id='cutFeats'>Cut Out Features</li>");
		cutFeats.on('click', function(evt) {
			tooltip.destroy();
			//use cutGeom from geomUtils module
			require(["app/mapview/geomUtils"], function(geomUtils) {
				geomUtils.cutGeometries(graphic);
			});
		});

		//export features as zip file
		var expFeat = $("<li id='remove'>Export Feature</li>");
		expFeat.on('click', function(){
			tooltip.destroy();
			//clone the target graphic
			var feat = $.extend({},graphic);
			//display extraction options
			var dataSource = [];
			require(["app/appUtils"], function(appUtils) {
				dataSource = appUtils.getExtractDataFormats();
			});

			var options = {
				title: 'Output Formats',
				label: '',
				message: 'Select the output file format.',
				filter: false,
				ftext: 'name',
				fvalue: 'value',
				dataSource: dataSource
			};

			nisis.ui.displaySelections(options)
			.then(
				function(resp) {
					if ( resp !== 'close' || resp !== 'cancel' ) {
						var format = resp,
							lyrs = [layer],
							feats = [graphic];
						//notify user
						nisis.ui.displayMessage("Prcessing data extraction. Output: " + resp, 'info');

						//process the extraction
						require(["app/mapview/gpUtils"], function(gpUtils) {
							gpUtils.extractData(feats, format);
						});
					}

				},function(error) {
					var msg = error ? error : 'App could not open selection widget.';
					nisis.ui.displayMessage(msg, 'error');
				}
			);
		});
		
		//remove features
		var remove = $("<li id='remove'>Remove Feature</li>");
		$(remove).on('click', function(evt){
			tooltip.destroy();
			//layer.remove(graphic)
			var rmFeat = nisis.ui.mapview.util.removeFeatureFromUserLayers(graphic);

			if(!rmFeat) {
				return;
			}

			rmFeat.then(function(d){
				var id = null, 
					success,
					msg;

				if(graphic.attributes){
				    id = graphic.attributes.OBJECTID ? graphic.attributes.OBJECTID : graphic.attributes.FID;
				}

				success = id ? 'Feature #' + id + ' is removed' : 'Feature is removed';

				nisis.ui.displayMessage(success, 'info');

			},function(e){
				var error = e.message ? e.message : e;
				nisis.ui.displayMessage(error,'Remove Failed!','error');
			});
		});
		
		//cancel operation
		var cancel = $("<li id='cancel'>Cancel</li>");
		$(cancel).click(function(e){
			tooltip.destroy();
		});

		//bring Forward
		var bringForward = $("<li >Bring Forward</li>");
		bringForward.on('click', function(){
			tooltip.destroy();
			//clone the target graphic
			var feat = $.extend({},graphic);
			shapesManager.moveShape([feat, 'bringForward']);			
		});
		//bring Backward
		var sendBackward = $("<li >Send Backward</li>");
		sendBackward.on('click', function(){
			tooltip.destroy();
			//clone the target graphic
			var feat = $.extend({},graphic);
			shapesManager.moveShape([feat, 'sendBackward']);		
		});
		//Send To Back
		var sendToBack = $("<li >Send To Back</li>");
		sendToBack.on('click', function(){
			tooltip.destroy();
			//clone the target graphic
			var feat = $.extend({},graphic);
			shapesManager.moveShape([feat, 'sendToBack']);		
		});
		//bring to Front
		var bringToFront = $("<li >Bring to Front</li>");
		bringToFront.on('click', function(){
			tooltip.destroy();
			//clone the target graphic
			var feat = $.extend({},graphic);
			shapesManager.moveShape([feat, 'bringToFront']);	
		});

		//check for geom type
		if (gType && gType === 'polygon') {
			//TODO: add buffer option
			otherOptions.append(style,remove,cutFeats,expFeat);
		} else if (gType && gType === 'point') {
			otherOptions.append(remove,expFeat);
		}else {
			otherOptions.append(style,remove,expFeat);
		}
		
		other.append(otherOptions);

        orderOptions.append(sendToBack, sendBackward, bringToFront, bringForward);
		order.append(orderOptions);
			
        var isReadOnlyFeature=nisis.ui.isReadOnlyFeature(graphic.attributes.OBJECTID, graphic._layer.id);
		if (isReadOnlyFeature) {
			$(featureSubMenu)			
			.append(saveAs,order, expFeat, cancel)
			.kendoMenu({
				orientation: 'vertical'
			});
		}
		else {
			$(featureSubMenu)
			//.append(create,saveAs,edit,remove,cancel)
			//.append(saveAs,edit,remove,cancel)
			//.append(saveAs,shape,remove,style,expFeat,cutFeats,cancel)
			.append(saveAs,shape,other,order, cancel)
			.kendoMenu({
				orientation: 'vertical'
			});
		}

		var tooltip = $('#map')
		.kendoTooltip({
            //autoHide: false,
            showOn: 'mouseenter',
            width: 'auto',
            height: 'auto',
            position: "top",
            content: $(featureSubMenu),
            show: function(e){
            	//console.log('Tooltip opened for ' + layer.name, $(target).width());
            	$(this.popup.wrapper).css({
            		top: evt.clientY,
            		left: evt.clientX - ($(this.popup.wrapper).width() / 2)
            	});
            },
            hide: function(e){
            	this.destroy();
            }
        }).data("kendoTooltip");
        tooltip.show();
	};

    uiEls.isReadOnlyFeature = function(objectId, layerId) {    	
    	var result = false;    	
    	if (!(layerId == 'Polygon_Features' || layerId == 'Point_Features' || layerId == 'Line_Features' || layerId == 'text_features')) {
    		return false;
    	}
    	    	
		var itemNode = features.getItemByObjectId(objectId, features.getItemTypeByLayerId(layerId));

		//Disable the editing/moving if the shape is shared with the user, since the user does not own it
        if ( (itemNode.USERORGROUPITEM === "user" && itemNode.OWNERID !== nisis.user.userid) || (itemNode.USERORGROUPITEM === "group" && features.userGroupPriv === 1 ) ) {
        	return true;
        }
        return result;
    };
	//Display data blocks
	uiEls.displayDataBlocks = function(layer, attr, evt, count) {
		if(!layer || !attr){
			return;
		}


		// console.log("UI.JS =====================\n")
		// console.log("layer:", layer, "attr:", attr);

	    var	content = "",
	    	//field used for status
	    	typeIdField = layer.typeIdField,
	    	//unique values
			fTypes = layer.types;

	    $.each(layer._outFields,function(i,field){

	    	if(field === 'SYMBOLOGY' || field === 'ARP_TYPE'){
	    		return;
	    	}
	    	$.each(layer.fields,function(j,f){
	    		if(field === f.name){
	    			if (f.type === "esriFieldTypeOID") {
	    				return;
	    			}
	    			else if (f.type === "esriFieldTypeDate") {
	    				content += "<span>" + f.alias.replace(/_/g, ' ') + ": <b>";
	    				if(attr[field]){
		    				var d = new Date(attr[field]);
		    				//console.log(d);
		    				var date = nisis.ui.formatDateTime(d);
		    				//console.log(date);
		    				content += date + "</b></span><br>";
		    			} else {
		    				content += "</b></span><br>";
		    			}
	    			} else {
	    				content += "<span>" + f.alias.replace(/_/g, ' ') + ": <b>";
	    				if(field === typeIdField){
	    					$.each(fTypes,function(t,ft){
	    						if(attr[field] === ft.id){
	    							content += ft.name ? ft.name : "";
	    						}
	    					});
	    				} else {
	    					if (attr[field] !== null){
								var tmp = templates[layer.id];

								if (tmp && tmp.picklists && tmp.picklists[0][field.toLowerCase()]){
		    						var values = tmp.picklists[0][field.toLowerCase()];
		    						$.each(values, function(i,v){
	    								if(v.value == attr[field]){
		    								content += v.label;
		    							} else {
			    							content += "";
			    						}

	    							});		    							
								} else {									
			    					if(f.domain){
			    						var values = f.domain.codedValues;
			    						//console.log(values);
			    						$.each(values, function(i,v){
			    							if(v.code == attr[field]){
			    								content += v.name;
			    							}
			    						});
			    					} else {
			    						// NISIS-2879 escape <>. They interfere with datablock html.
			    						// NISIS-2945 make sure typeof attr[field] === "String"
			    						content += attr[field].toString().replace("<","&lt;").replace(">","&gt;");
			    					}
								}
							} else {
								// console.log("I GOT NOTHIN")
    							content += "";
    						}	
	    				}
	    				content += "</b></span><br>";
	    			}
	    			return;
	    		}
	    	});
	    });
		//add count of features at location
		if (count && count > 1) {
			content += "<span><b>**";
			content += count -1;
			content += " MORE FEATURE(s)</b></span><br>";
		}
		//map size
	    var W = $('#map').width(),
	    	H = $('#map').height();
	    //data block position
		$('#data-blocks').html(content)
		.css({top: evt.pageY + "px", left: evt.pageX + 10 + "px"});
		//adjust data block
		var w = $('#data-blocks').width(),
	    	h = $('#data-blocks').height();

		if(W - evt.pageX < w) {
			$('#data-blocks').css({left: (evt.pageX - (w + 20)) + "px"});
		}
		if(H - evt.pageY < h - 20){
			$('#data-blocks').css({top: (evt.pageY - (h + 20)) + "px"});
		}
		//display data blocks
		$('#data-blocks').show();
	};
	//hide data blocks
	uiEls.hideDataBlocks = function() {
		$('#data-blocks').html("").hide();
	}

	//Display DOT data blocks
	uiEls.displayDOTDataBlocks = function(layerid, pkid, evt) {
		// console.log('evt: ', evt, layer);
		$.ajax({
            url: "api/",
            type: "POST",
            data: {                 
                action: "get_datablocks",
                layerid: layerid,
                pkid: pkid
            },                            
            success: function(data, status, req) {  
                var resp = JSON.parse(data);
                //build content based on the dot object type
                // var content = dotObjectHeaders.getHeader(layer.layer.id,resp);               
                // var infobox = new DOTObjectProfile(content, resp, evt);
                var datablockObject = resp.datablock;
				var content='';

				for(var key in datablockObject){
					if (datablockObject.hasOwnProperty(key)) {
						if (key !== 'OBJECTID'){
						    var val = datablockObject[key];
						    content += "<span>" + key.replace(/_/g, ' ') + ": <b>";
						    content += val;
						    content += "</b></span><br>";
						}
					}
				}

                //map size
			    var W = $('#map').width(),
			    	H = $('#map').height();
			    //data block position
				$('#data-blocks').html(content)
				.css({top: evt.pageY + "px", left: evt.pageX + 10 + "px"});
				//adjust data block
				var w = $('#data-blocks').width(),
			    	h = $('#data-blocks').height();

				if(W - evt.pageX < w) {
					$('#data-blocks').css({left: (evt.pageX - (w + 20)) + "px"});
				}
				if(H - evt.pageY < h - 20){
					$('#data-blocks').css({top: (evt.pageY - (h + 20)) + "px"});
				}
				//display data blocks
				$('#data-blocks').show();
		    },
		    error: function(req, status, err) {
	                console.error("ERROR in ajax call to get the datablocks info ui.displayBlocks() method");
	        }
	    });
	}

	//display ago layers attr
	uiEls.displayAgoAttributes = function(layer, evt, symbol) {
		if (!layer || !evt){
			return;
		}

		var fields = layer.fields,
			graphic = evt.graphic,
			attr = graphic.attributes;

		var agoInfo = $("<div id='ago-attr-div' class='dialog'></div>");
		var content = ""; //$("<div class='ago-attr'></div>");

		for (var key in evt.graphic.attributes) {

			var field = "", 
				value = evt.graphic.attributes[key];

			$.each(fields, function(i, f) {
				if (f.name === key) {
					field = f.alias ? f.alias.toUpperCase().replace(/_/g, ' ') : key.toUpperCase().replace(/_/g, ' ');
					if(key.toUpperCase() != 'OBJECTID'){
						content += "<br/><b>";
						content += field;
						content += "</b>: ";

						if (f.type === "esriFieldTypeDate") {
		    				
		    				var d = new Date(value);
		    				//console.log(d);
		    				var date = nisis.ui.formatDateTime(d);
		    				//console.log(date);
		    				content += date
		    			} else {
		    				if(key === layer.typeIdField){
		    					$.each(layer.types,function(t,ft){
		    						if(value === ft.id){
		    							content += ft.name ? ft.name : "";
		    						}
		    					});
		    				} else {
		    					if(f.domain){
		    						var values = f.domain.codedValues;
		    						//console.log(values);
		    						$.each(values, function(i,v){
		    							if(v.code == value){
		    								content += v.name;
		    							}
		    						});
		    					} else {
		    						//console.log(field);
		    						if (value){
		    							console.log('Value: ', value);
		    							if (typeof value == 'string' && value.indexOf('http') != -1) {
			    							value = "<a href='" + value + "' target='_blank'>link</a>";
			    							content += value;
			    						} else {
			    							content += value ? value : "";
			    						}
			    					} else {
			    						content += "";
			    					}
		    					}
		    				}
		    			}
					}
					return;
				}
			});
		}

		$('body').append(agoInfo);
		agoInfo.append(content);
		//wrap.append(content);
		
		agoInfo.kendoWindow({
			title: layer.name,
			visible: false,
			actions: ['Minimize','Close'],
			width: 400,
			height: 'auto',
			maxHeight: 500,
			position: {
				top: 50,
				left: 50
			}
			,open: function(e){
				//reposition the window
				this.setOptions({
					position: {
						top: evt.y,
						left: evt.x
					}
				});
			},
			close: function(e){
				this.destroy();
				evt.graphic.setSymbol(symbol);
				evt.graphic.redraw();
			}

		}).data("kendoWindow").open();
	};

	//collapse all windows
	uiEls.collapseWindows = function(){
		$('.dialog').each(function(){
    		var kw = $(this).data('kendoWindow');
    		if(kw && $(this).is(':visible')){
    			kw.minimize();
    		}
    	});
	};
	//close all windows
	uiEls.closeWindows = function(){
		$('.dialog').each(function(){
    		var kw = $(this).data('kendoWindow');
    		if(kw && $(this).is(':visible')){
    			kw.close();
    		}
    	});
	};

	//format dateTime number value from database into zulu-formatted string
	uiEls.formatDateTime = function(dateTime){
		var zuluDate = new Date(dateTime);
        var placeHolder;
        //console.log('Last Update Date=' + zuluDate);
        var toReturn = zuluDate.getUTCFullYear() + '/';

        placeHolder = (zuluDate.getUTCMonth()+1);
        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

        toReturn += '/';
        placeHolder = zuluDate.getUTCDate();
        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

        toReturn += ' ';	
        placeHolder = zuluDate.getUTCHours();
        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

        //toReturn += ':';
        placeHolder = zuluDate.getUTCMinutes();
        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;
        toReturn += 'Z';

        //console.log('datetime format=' + toReturn);
        return toReturn;
	}
	//build feature details table
	uiEls.buildFeatureDetailsTable = function(feature){
		var selFeat = feature,
			lyr = selFeat.getLayer(),
			oid = selFeat.attributes.OBJECTID,
			title = lyr.name + ' - FID: ' + oid;
		//create a dialog box
		var destroy = function(evt){
			$(evt.target).remove();
		};
		var dialog = nisis.ui.widgets.createDialogBox('featDetails',title,{
			width: 500,
			visible:false,
			resizable:true,
			afterHide:destroy
		});
		var table = $("<div id='featDetTbl'></div>").css({
			width:'auto', height:'auto'
		}).appendTo(dialog);
		dialog.puidialog('show');
		table.spin(true);
		var displayFeatDetails = function(results){
			if(results && typeof results == 'string'){
				table.html('Error: '+results);
			}else{
				if(results.length>0){
					var  fields=[], attr = results[0].feature.attributes,
					columns = [
						{field:'FieldName', headerText: 'Field Name', sortable:true},
						{field:'FieldValue', headerText: 'Field Value', sortable:true}
					];
					for (var field in attr){
						fields.push({
							'FieldName': field,
							'FieldValue': attr[field]
						});
					}
					table.puidatatable({
						paginator: {
							rows: 5
						},
						columns: columns,
						datasource: fields,
						selectionMode: 'single',
						rowSelect: function(evt){
							
						},
						rowUnselect: function(evt){
							
						}
					});
				} else {
					table.html('Error: counld not find your search.');
				}
			}
			table.spin(false);
		};
		nisis.ui.mapview.util.findFeatures(lyr.url,['OBJECTID'],oid,displayFeatDetails);
	};
	
    //public object
    return uiEls;
})();
});