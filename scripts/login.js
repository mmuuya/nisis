//welcome page
$('#content').ready(function(){
	var oldURL = document.referrer;
	if (oldURL.indexOf('reset.php') !== -1) { 
		// TODO this url check probably won't work with the new reset code
		// show a success message if we just came from a reset
		nisis.ui.displayMessage("Your password has been successfully reset.", "info");
	}

	var checkLogin = function() {
		var username = $.trim($('#username').val()),
			password = $.trim($('#password').val());
				
		if(username === '' && password === '') {
			nisis.ui.displayMessage("Please enter your username and password", "error");
		} else if(username === '') {
			nisis.ui.displayMessage("Please enter your username", "error");
		} else if (password === ''){
			nisis.ui.displayMessage("Please enter your password", "error");
		} else {
			nisis.user.login(username,password);
			// console.log('you are good...');
		}
	};

	//show warning on page
	var showWarning = function(){
		$('.lgn-ol').fadeIn("slow");
		$('.lgn-mdl').fadeIn("slow");

		$('#continue').click(function(){
			$('.lgn-ol').fadeOut("slow");
			$('.lgn-mdl').fadeOut("slow");

			$('#username').prop('disabled', false);
			$('#password').prop('disabled', false);
			$('#username').focus();
		});

		$('#cancel').click(function(){
			var host = window.location.hostname;

			if(host == 'dotmap.dot.gov'){
				window.location.href = "https://www.dot.gov";
			}else{
				window.location.href = "https://www.faa.gov";
			}
			
		});
	};

	//Modal warning
	showWarning();

	$('#newpass').on('click', function(e){
		var $el = $('#resetForms');
		var $parent = $el.parent();
		$parent.addClass('reset-modal')
	})

	//handle the reset login for request
	$('#reset').click(function() {
		$('#username').val('');
		$('#password').val('');
	});
	// handle an enter keypress event when Password field in focus
	$('#username').keyup(function(event) {
		if (event.keyCode == 13) {
			checkLogin();
		}
	});
	// handle an enter keypress event when Password field in focus
	$('#password').keyup(function(event) {
		if (event.keyCode == 13) {
			checkLogin();
		}
	});
	//submit login
	$('#submit').click(function(){
		checkLogin();
	});
	//focus on username field
	$('#username').focus();
});