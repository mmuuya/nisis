/*
 * Map Layers and layers tree
 */
require(["esri/virtualearth/VETiledLayer","esri/layers/ArcGISDynamicMapServiceLayer",
	"esri/layers/ArcGISTiledMapServiceLayer","esri/layers/FeatureLayer","esri/layers/LabelLayer",
	"esri/graphic","esri/layers/GraphicsLayer","esri/layers/TableDataSource","esri/layers/LayerDataSource",
	"esri/layers/wms","esri/layers/WMSLayer","esri/layers/WMSLayerInfo","esri/layers/FeatureTemplate"
	,"esri/dijit/Basemap","esri/geometry/Extent","esri/SpatialReference"
	,"esri/layers/WebTiledLayer","esri/InfoTemplate"
	,"esri/symbols/TextSymbol","esri/renderers/SimpleRenderer"
	,"esri/kernel"
	,"app/appConfig"
	,"app/layersConfig"
	,"app/iconsConfig"
	,"app/renderers"
	,"app/fieldsInfos"
	,"esri/dijit/PopupTemplate"
	],
function(VETiledLayer,ArcGISDynamicMapServiceLayer,
	ArcGISTiledMapServiceLayer,FeatureLayer,LabelLayer,
	Graphic,GraphicsLayer,TableDataSource,LayerDataSource,
	Wms,WMSLayer,WMSLayerInfo,FeatureTemplate
	,Basemap,Extent,SpatialReference
	,WebTiledLayer,InfoTemplate
	,TextSymbol,SimpleRenderer
	,kernel
	,appConfig
	,layersConfig
	,icons
	,renderers
	,fieldsInfos
	,PopupTemplate
	){

nisis.ui.mapview.layers = (function(){
	var mapview = nisis.ui.mapview,
	layers = {},
	layerDefaultQueries = {},
	src = layersConfig.slice(); //make a copy

	//define overlay Layers
	mapview.buildMapLayers = function() {
	    //tooltips
        mapview.tooltips = {};
		//load layers in the reverse order
		var source = src.reverse(),
			len = 0,
			userLayers = [],
			tfrLayers = [],
			nisisGroups = ['user', 'nisisto'],
			auth = nisis.user.checkCredentialsValidity();

		$.each(source, function(index, layer){
			if (layer.add) {
				len++;
				var options = {};

				//check auth
				if ( layer.secured && !auth ) {
					return;
				}

				//nisis secured services
				if ( layer.secured && !nisis.user.loadTOLayers
					&& layer.url.indexOf(nisis.url.arcgis.server) !== -1) {
					return;
				}

				//weather: when to load secured layers ==> uncomment when ready for weather services
				if ( layer.secured && !nisis.user.loadWSLayers
					&& layer.url.indexOf(nisis.url.arcgis.wsServer) !== -1) {
					return;
				}

				//CC - Checking if the layer.id is "airports" because the airports layer should be available for all TFR users (no matter if they don't have access to NISIS) in order to run the query to retrieve all the airports inside of all the TFR shapes.
				//See the third tab when you open a TFR (Airports). TABS :
				// || Quick Entry || Notam Text || Airports ||
				if (layer.id !== "airports"){
					//do not add layers of a group the user is not part of
					if ( layer.group
						&& layer.group === 'tfr'
						&& $.inArray(layer.group.toUpperCase(), nisis.user.apps) === -1 ) {
						return;
					}
					else if ( layer.group
						&& $.inArray(layer.group, nisisGroups) !== -1
						&& $.inArray('NISIS', nisis.user.apps) === -1 ) {
						return;
					}
				}

				//build layer base on type
				if (layer.type == "WMS") {
					options = {
						id: layer.id,
						opacity: 1,
						visible: layer.visible
					};
					mapview.layers[layer.id] = new ArcGISDynamicMapServiceLayer(layer.url, options);
					//set visible layers (used to restrict layerIds to be included in WMS layer)
					if ( layer.vLayers && layer.vLayers.length > 0 ) {
						mapview.layers[layer.id].setVisibleLayers(layer.vLayers);
					}
				}
				else if (layer.type == "WFS") {					
					//define layer options
					options = {
						id: layer.id,
						opacity: 1,
						name: layer.name,
						mode: FeatureLayer.MODE_ONDEMAND,
						outFields: layer.attributes && layer.attributes.fields ? layer.attributes.fields : ["*"],
						visible: layer.visible
						,refreshInterval: 1						
					};

					//irena - special template for All Feature layers.		
					// if (layer.id=="Polygon_Features")			
					// console.log("layer.type....", layer.type);
					if (layer && layer.type && layer.type == 'WFS') {	
					    // console.log("layer.type....", layer.id);		
					    // console.log("layer.id....", layer.type);	
					    var fieldInfosPopup = [];	
					    var fields = null;
					    var fieldInfo = null;
					    if( layer.renderer) {
					     	fieldInfo = layer.renderer ? layer.renderer : null;
					     	if (fieldInfo!=null) {
					     		//console.log("layer fieldInfo...", fieldInfo);	
						    	fields = fieldsInfos[fieldInfo];
						    	if (fields != undefined) {
							        //console.log("fields...", fields);	
							        for (var ndx=0; ndx<fields.length; ndx++) {
							        	var field = fields[ndx];
							        	if (field.type !== 'esriFieldTypeOID' && field.type !== 'esriFieldTypeGeometry' && field.type !== 'esriFieldTypeBlob' && field.type !== 'esriFieldTypeRaster' && field.type !== 'esriFieldTypeGUID' && field.type !== 'esriFieldTypeGlobalID' && field.type !== 'esriFieldTypeXML') {  
								        fieldInfosPopup.push({  
								          fieldName: field.fieldName,  
								          visible: true,  
								          isEditable: field.isEditable,
								          //customField: new dijit.form.TextBox(),
								          customField: new  dijit.form.ValidationTextBox({
          regExp : /~|`|!|@|#|\$|%|\^|&|\*|\(|\)|_|\+|\[|\]|\|;|'|,|\.|,|\/|<|>|\?|:|"|{|}|\|/,
          required : false,
          promptMessage: "Enter a shape name with valid characters",
          invalidMessage : "Your input contains illegal characters"
	  }),
								          label: field.label + ': '  
								        });  
								      }  
							        }						    
								    //console.log("layer fieldInfosPopup...", fieldInfosPopup);	
								}
					     	}					     		
					    }											
						
						if (fieldInfosPopup!=[]) {											
							var template = new PopupTemplate({								
								         title: fieldInfo ? fieldInfo: ""
								         ,fieldInfos: fieldInfosPopup										
							});				

							options.infoTemplate=template; //irena - this is popup template for info Template
						}
						
				
					}

					//refresh interval should be 1 or 5
					if(layer.group && layer.group === 'nisisto'){
						options.refreshInterval = 1;
					} else {
						options.refreshInterval = 5;
					}
					//define feature layer
					layers[layer.id] = new FeatureLayer(layer.url, options);				
					//specify layer type
					layers[layer.id].type = 'Feature Layer';
					//user layers
                    if(layer.group === 'user'){
						userLayers.push(layer);
						var query = " objectid IN (" + nisis.user.userVisibleFeatures[features.getItemTypeByLayerId(layer.id)] + ")";
						layers[layer.id].setDefinitionExpression(query);
                    }

                    //tfr layers
                    //if(layer.group === 'tfr' && appConfig.get('tfrMode')) {
                    if (layer.group === "tfr") {
                    	if (appConfig.get('tfrMode')) {
                    		layers[layer.id].setVisibility(true);
                    	}
                    	//BK: Use the refresh event to apply default symbol to shapes
                    	//layers[layer.id].on('update-end', renderers.applyTfrDefaultSymbols);
						//layers[layer.id].on('click', renderers.getSelectedFeatures);
						//layers[layer.id].on('selection-complete', renderers.getSelectedFeatures);

						layers[layer.id].on('selection-clear', function(evt){
							//force a refresh of the layer when the selection is clear
							var l = evt.target,
								n = l && l.graphics ? l.graphics.length : 0;

							if (n && !l.updating) {
								l.refresh();
							}
						});
                    }

                    //specify renderers and custom styles
					if( layer.renderer) {
						layers[layer.id].apprenderer = layer.renderer;
					}

					//apply different rendering to the feature layer
					renderers.applyFeatureLayersRenderer(layers[layer.id]);
					//handle text labeling
					if( layer.id === 'text_features' ) {
						layers[layer.id].on('update-end', renderers.text.applyTextSymbols);
						layers[layer.id].on('selection-clear', function(evt){
							//force a refresh of the layer when the selection is clear
							var l = evt.target,
								n = l && l.graphics ? l.graphics.length : 0;

							if (n && !l.updating) {
								l.refresh();
							}
						});
					} else {
						//for custom styles
						layers[layer.id].on('update-end', renderers.applyCustomStyles);
					}
					//for the custom editor we need to add username ourselves
					//layers[layer.id].on('edits-complete', mapview.util.updateTrackingInfo);

					//for Feature Manager features, enable/disable move and editing bases on the user privs and graphic they clicked on
					if (layer.id == 'Polygon_Features' || layer.id == 'Point_Features' || layer.id == 'Line_Features' || layer.id == 'text_features') {
						layers[layer.id].on('click', nisis.ui.mapview.util.toggleEditing);
					}

				}
				else if (layer.type == "graphics"){
					layers[layer.id] = new esri.layers.GraphicsLayer({
						id: layer.id,
						name: layer.name,
						visible: layer.visible
					});

					layers[layer.id].on('click', nisis.ui.mapview.util.activateEditTools);
					layers[layer.id].on('dbl-click', nisis.ui.mapview.util.activateEditTools);
					layers[layer.id].on('mouse-up', nisis.ui.createFeatureSubMenu);
				}
				else if(layer.type == 'TABLE'){
					var tableSource = new TableDataSource();
					tableSource.dataSourceName = layer.table;
					var layerSource = new LayerDataSource();
					options = {
						id: layer.id,
						opacity: 1,
						name: layer.name,
						mode: FeatureLayer.MODE_ONDEMAND,
						outFields: layer.attributes.fields,
						visible: layer.visible,
						source: layerSource
					};
					layers[layer.id] = new FeatureLayer(layer.url,options);
				}
				else if (layer.type === 'TILES'){
					options = {
						id: layer.id,
						opacity: 1,
						name: layer.name,
						visible: layer.visible
					};
					layers[layer.id] = new ArcGISTiledMapServiceLayer(layer.url, options);
				}
				else if (layer.type == 'OGC'){
				    /*mapview.layers[layer.id] = new WMSLayer(layer.url);
				    mapview.layers[layer.id].setVisibleLayers([0]);
                    mapview.layers[layer.id].setImageFormat("png");*/

                    //var srs = new SpatialReference({wkid:4326}),
                    //extent = new Extent(-180, -90, 180, 90, srs);

                    /*layers[layer.id] = new WMSLayer(layer.url, {
                    	resourceInfo: {
				            extent: extent,
				            layerInfos: [new WMSLayerInfo({name:layer.id, title:layer.name})]
				        },
				        visibleLayers: layer.layers
                    });*/

                    /*var basemap = new Basemap({
                    	id: layer.id,
                    	basemapLayers: [mapview.layers[layer.id]]
                    });*/

                    //mapview.basemaps.add(mapview.layers[layer.id]);
				}
				else if (layer.type == "WTL"){
					layers[layer.id] = new WebTiledLayer(layer.url, {
						id: layer.id,
						subDomains: layer.subDomains,
						copyright: layer.copyright,
						visible: layer.visible
					});
				}
				//used for layer tree grouping
				mapview.layers[layer.id].group = layer.group;
				//apply default query
				if(layer.hasOwnProperty('query') && layer.query !== null){
					mapview.layers[layer.id].setDefinitionExpression(layer.query);
					mapview.layers[layer.id].query = layer.query;
				}
				//add default labels
				if(layer.type === 'WFS' && layer.label) {
					var id = layer.id + "_labels";
					var name = layer.name + " Labels";
					var symbol = new TextSymbol();
  					var renderer = new SimpleRenderer(symbol);
  					var label = "{" + layer.label + "}";
					var defaultLabel = new LabelLayer({
						id: id,
						mode: 'DYNAMIC'
					});
					defaultLabel.type = "Label Layer";
					defaultLabel.name = name;
					defaultLabel.group = layer.group;
				    defaultLabel.addFeatureLayer(mapview.layers[layer.id], renderer, label);
					//add feature layer and it's default labels, if visible					
					if (layer.visible) {
				        nisis.ui.mapview.overlays.push(layers[layer.id]);
				        nisis.ui.mapview.overlays.push(defaultLabel);
			    	}
			    	//add default label layers to layers
			    	mapview.layers[id]=defaultLabel;
				} else {
					//add all layers into overlays array
					//nisis.ui.mapview.overlays.push(layers[layer.id]);

					//add only visible layers into overlays array
					if (layer.visible) {
						nisis.ui.mapview.overlays.push(layers[layer.id]);
					}
				}
			}
		});

		//check user groups
		var layerTree = null,
			userGpsID = 'groups',
			gNode = null,
			groupNodes = [];
			//TreeView object
			layerTree = $("#overlays-toggle").data("kendoTreeView");

		//get group node by it
		gNode = layerTree.dataSource.get(userGpsID);

		//appConfig.updateMainMenu();

		//console.log("-------------->>> Number of layers adding to map:", nisis.ui.mapview.overlays.length);
		//console.log("-------------->>> layers adding to map:", nisis.ui.mapview.overlays);
		/*if (nisis.ui.mapview.overlays.length > 0){
			mapview.map.addLayers(nisis.ui.mapview.overlays);
		}*/
		//get user groups and extend layers tree
		nisis.user.getUserGroups(nisis.user.userid)
		.then(function(gps){
			var groups = gps;
			//var n = userLayers.length;
			//nisis.ui.sortArrayOfObjects(groups,'GNAME');
			var admin = false;

			$.each(groups, function(i,group) {
				if(group.GNAME === "Administrators"){
					admin = true;
				}
			});

			nisis.user.isAdmin = admin;
			appConfig.updateMainMenu();
			//add all layers
			//mapview.map.addLayers(nisis.ui.mapview.overlays);
		},function(error){
			console.log(error);
			gNode.items.push({
				"text": "Error loading groups",
				"id": "error",
				"type": "image",
				"parent": userGpsID,
				"hasChildren": false
				//,"checked": true
			});
			//add all layers
			//mapview.map.addLayers(nisis.ui.mapview.overlays);
		})
		.then(mapview.map.addLayers(nisis.ui.mapview.overlays.reverse()), function (error) { console.log(error); });//irena - try to reverse the overlays
	};

	//public objects
	return layers;
})();
});
