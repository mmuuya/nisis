//nisis public object
var nisis = null;
//
require(["esri/kernel", "esri/config"]
	,function (kernel, config) {

  config.defaults.io.corsEnabledServers.push(agsHost);

nisis = {
	url: {
		web: location.href,
		arcgis: {
			server: agsServer
            ,wsServer: wsServer
			,portal: document.location.protocol + '//www.arcgis.com/'
			,route: document.location.protocol + "//sampleserver3.arcgisonline.com/ArcGIS/rest/services/Network/USA/NAServer/Route"
			,serivce_area: document.location.protocol + "//sampleserver3.arcgisonline.com/ArcGIS/rest/services/Network/USA/NAServer/Service%20Area"
			,closest_facility: document.location.protocol + "//sampleserver3.arcgisonline.com/ArcGIS/rest/services/Network/USA/NAServer/Closest%20Facility"
			,tokens: agsServer + "tokens/generateToken"
            ,wsTokens: wsServer + "tokens/generateToken"
			,services: agsServer + "rest/services/"
            ,threatLayers: wsServer + "rest/services/"
			,gpExtractData: function() {
				//return this.services + "http://192.168.19.211:6080/arcgis/rest/services/geoprocessing/UsersExtractDataTask/GPServer/Extract%20Data%20Task";
				return this.services + "geoprocessing/ExtractDataTask/GPServer/Extract%20Data%20Task";
			}
			,securedSVC: function(){
			    var services = this.services,
			    	sslServices = [];

			    require(["app/layersConfig"], function(layersConfig) {
			    	$.each(layersConfig, function(i, layer) {
				    	if (layer.add && layer.secured && layer.url.indexOf(nisis.url.arcgis.server) !== -1) {
				    		if ( $.inArray(layer.url, sslServices) === -1) {
					    		sslServices.push(layer.url);
					    	}
				    	}
				    });
			    });
			    //feature extraction service
			    sslServices.push( this.gpExtractData() );

			    return sslServices;
			}
            ,weatherSVC: function() {
                var sslWsServices = [];

                require(["app/layersConfig"], function(layersConfig) {
                    $.each(layersConfig, function(i, layer) {
                        if (layer.add && layer.secured && layer.url.indexOf(nisis.url.arcgis.wsServer) !== -1) {
                            if ( $.inArray(layer.url, sslWsServices) === -1) {
                                sslWsServices.push(layer.url);
                            }
                        }
                    });
                });

                return sslWsServices;
            }
		},
		geoserver: {
			services: geoServer + "geoserver/adapt/wms?",
			services2: geoServer + "geoserver/gwc/service/wms?"
		}
	},
	ui : {},
	historyObject: {},
	alerts : {
		oid : '',
		facId : '',
		facType : '',
		statusType : '',
		displayedAlerts : []
	},
	displayedNotifications : [],
	picklists : {},
	groupLayers: ['Point_Features','Line_Features','Polygon_Features'],
	user : {
		// nisis: this, //CC - Looks like the global object nisis is being set in here recursively ('nisis' inside nisis.users), I removed it. Feel free to add it back if this is breaking something
		userid: null,
		username: null,
		name: null,
		credentials: "nisis-services-credentials",
		groups : [],
        grpPrivileges: {},
		apps: [],
		tfrgroups : [],
		features: [],
		query: null,
		isAdmin: false,
        isGroupAdmin: false,
		tfrAccess: false,
        loadTOLayers: false,
        loadWSlayers: false,
        userVisibleFeatures: {"polygon":"","point":"","line":"","text":""},
		login : function(username,password) {
			$('#welcome').spin(true);
			var $this = this,
    			user = username,
    			pw = password;

			var data = {
				action : 'login',
				username : user,
				password : pw
			};

			$.ajax({
				url : 'api/',
				type : 'POST',
				data : data,
				success : function(data, status, xhr) {
					var resp;
					try {
						resp = JSON.parse(data);
					} catch (err) {
						$('#welcome').spin(false);
						var msg = "App could not log you in. Contact Support or try again later.";
						nisis.ui.displayMessage(msg, 'error');
						return;
					}
					//check response
					if (resp && resp['return'] === 'Success') {
					    $this.username = resp.username;
						//check for token

                        // NISIS-3021 KOFI HONU - 
                        // Use actual username spelling in DB (upper/lowercase) for use in the rest of the app.
                        // If we use the one supplied at login, it might not be same case as stored in ArcGIS / browser credentials / etc.
                        //$this.getCredentials(user, pw)
                        $this.getCredentials($this.username, pw)
						.then(function(token){

                            var agsToken = token[0],
                                wsToken = token[1] ? token[1] : null;

						    if (!agsToken || agsToken['error']) {
						    	var msg = 'App could not get map services token. Try logging again.';
    					    	nisis.ui.displayMessage(msg, 'error');
    					    	$('#welcome').spin(false);

						    } else {

                                //flag Track Objects Layers
                                nisis.user.loadTOLayers = true;

							    resp.token = agsToken;

	                            var res = nisis.url.arcgis.securedSVC(),
                                    wsRes = nisis.url.arcgis.weatherSVC();

	                            var token = {
	                                serverInfos: [{
	                                    server: nisis.url.arcgis.server
	                                    ,hasPortal: true
	                                }],
	                                credentials: [{
	                                    userId: resp.username,
	                                    server: nisis.url.arcgis.server,
	                                    scope: 'server',
	                                    token: resp.token.token,
	                                    expires: resp.token.expires,
	                                    ssl: true,
	                                    validity: 720,
	                                    resources: res
	                                }]
	                            };

                                //add weather services token
                                if (wsToken) {
                                    //flag WS Layers
                                    nisis.user.loadWSLayers = true;

                                    token.serverInfos.push({
                                        server: nisis.url.arcgis.wsServer
                                        ,hasPortal: true
                                    });

                                    token.credentials.push({
                                        userId: 'faaajr2',
                                        server: nisis.url.arcgis.wsServer,
                                        scope: 'server',
                                        token: wsToken.token,
                                        expires: wsToken.expires,
                                        ssl: true,
                                        validity: 720,
                                        resources: wsRes
                                    });

                                } else {
                                    var msg = 'App could not get weather map services token.';
                                    nisis.ui.displayMessage(msg, 'error');
                                }

	                            //init token
	                            kernel.id.initialize(token);
	                            //check initialization
	                            if(kernel.id.credentials.length > 0){
	                                nisis.user.storeCredentials();
	                            }
	                        }

                            //unload the login form and load app elements
                            $('#welcome').spin(false);
                            $('#welcome').fadeOut(1000, function(){
                                $('body').spin(true);
                                window.location.reload();
                            });

    					},function(err){
    					    console.log('Could not get service token: ', err);
    					    var msg = 'Could not get service token. Try logging again.';
    					    nisis.ui.displayMessage(msg, 'error');
    					    //unload the login form and load app elements
                            $('#welcome').spin(false);
                            $('#welcome').fadeOut(200, function() {
                                $('body').spin(true);
                                window.location.reload();
                                $('body').spin(false);
                            });
    					});

					} else {
						nisis.ui.displayMessage('Wrong Username/Password combination', 'error');
						$('#welcome').spin(false);
					}
				},
				error : function(xhr, status, err) {
					$('#welcome').spin(false);
					nisis.ui.displayMessage(status, 'error');
				}
			});
		},
		logout : function() {
		    var msg = 'Logout failed. Please try again.';
			$.ajax({
                url: 'api/',
                data: {'action' : 'logout'},
                type: 'GET',
                success: function(data, status, xhr) {
                    var resp = null;

                    try {
                    	resp = JSON.parse(data);
                    } catch (err) {
                    	nisis.ui.displayMessage(msg, 'error');
                    	return;
                    }

                    if (resp && resp['return'] == 'Success') {
                    	//close all windows before loading logging page
                    	nisis.ui.closeWindows();
                        $('#map').fadeOut(2000, function(){
                            //clear credentials
                            nisis.user.clearCredentials();
                            $('.colpick').empty();
                            $('#user-controls').empty();
                            $('#content').empty();
                            $('#content').html(resp.newhtml);
                            //all dijit widgets have to be deregistred before logging back in
                            dijit.registry.forEach(function(widget){
                               widget.destroy();
                            });
                            //force the page to relaod
                            window.location.reload();
                        });
                    } else {
                        nisis.ui.displayMessage(msg, 'error');
                    }
                },
                error: function(xhr, status, err){
                    nisis.ui.displayMessage(msg, 'error');
                }
            });
		},
		getCredentials: function(user,pw){
		    var $this = this,
                def = new $.Deferred();
		    //arcgis server tokens
		    var tokenUrl = nisis.url.arcgis.tokens;
		    //validate inputs
		    if(!user || !pw) {
		        def.reject('Missing arguments for getCredentials');
		    }
		    //request arcgis token
		    $.ajax({
		        url: tokenUrl,
		        type: 'POST',
		        data: {
		            username: user,
		            password: pw,
		            client: 'requestip',
		            expiration: 720,
		            f: 'json'
		        },
                dataType: "json",
		        success: function(data, status, xhr) {
		            if(status === 'success'){
                        //Weather cred ==> uncomment when ready to push Weather
                        var wCred = $this.getWeatherCred();
                        wCred.then(function(wdata){
                            def.resolve([data, wdata]);
                        }, function() {
                            def.reject('No Weather credentials.');
                            def.resolve([data]);
                        });
		                //def.resolve(data);
		            } else {
		                def.reject(status);
		            }
		        },
		        error: function(xhr, status, err){
		        	console.log('ArcGIS Creds error:', arguments);
		            def.reject(err);
		        }
		    });
		    //return promise
		    return def.promise();
		},
        getWeatherCred: function(){
            var def = new $.Deferred();

            //request arcgis server user info
            $.ajax({
                url: "api/",
                type: 'POST',
                data: {action: 'weather_services'},
                dataType: "json",
                success: function(data, status, xhr) {
                    /*if(status === 'success'){
                        def.resolve(data);
                    } else {
                        def.reject(status);
                    }*/
                    //request arcgis server token
                    $.ajax({
                        url: nisis.url.arcgis.wsTokens,
                        type: 'POST',
                        data: {
                            username: data.user,
                            password: data.pass,
                            client: 'referer',
                            referer: document.location.protocol + '//' + window.location.hostname, //like 'http://nisis.ndpreston.net' for test
                            expiration: 720,
                            f: 'json'
                        },
                        dataType: "json",
                        success: function(data, status, xhr) {
                            if(status === 'success') {
                                def.resolve(data);
                            } else {
                                def.reject(status);
                            }
                        },
                        error: function(xhr, status, err){
                            def.reject(err);
                        }
                    });
                },
                error: function(xhr, status, err){
                    def.reject(err);
                }
            });
            //return promise
            return def.promise();
        },
		storeCredentials: function(){
		    //console.log("Storing layers credentials ...");
            if(kernel.id.credentials.length === 0){
                return;
            }
            var id = JSON.stringify(kernel.id.toJson());
            if (window.localStorage && window.localStorage !== null) {
                window.localStorage.setItem(this.credentials,id);
            } else {
                nisis.ui.displayMessage('Your browser does not support local storage.', 'error');
            }
        },
        loadCredentials: function(){
            //console.log("Loading layers credentials ...");
            var id, c;

            if(window.localStorage && window.localStorage !== null){
                id = window.localStorage.getItem(this.credentials);
            } else {
                id = cookie(this.credentials);
            }

            if(id && id !== 'null' && id.length > 4){
                c  = JSON.parse(id);
                kernel.id.initialize(c);
            } else {
                console.log("Credentials have not been saved");
            }
        },
        /**
         * check time left from
         * experiation datetime
         * <exp> in milliseconds and utc
         */
        getTimeLeft: function(exp) {
            var eDate = new Date(exp),
                cDate = new Date(),
                utcEDate,
                utcCDate,
                diff;

            utcEDate = new Date(
                eDate.getUTCFullYear(),
                eDate.getUTCMonth(),
                eDate.getUTCDate(),
                eDate.getUTCHours(),
                eDate.getUTCMinutes(),
                eDate.getUTCSeconds()
            );

            utcCDate = new Date(
                cDate.getUTCFullYear(),
                cDate.getUTCMonth(),
                cDate.getUTCDate(),
                cDate.getUTCHours(),
                cDate.getUTCMinutes(),
                cDate.getUTCSeconds()
            );

            diff = utcEDate - utcCDate;

            return diff;
        },
        checkCredentialsValidity: function() {
        	//console.log("Checking layers credentials validaty:", kernel.id);
            var id = kernel.id,
            	valid = false,
            	msg = "The map layers credentials are expired. Please try logging out and back in again.";

            if ( id && id.credentials && id.credentials.length ) {
            	var cred = id.credentials[0],
                    wsCred = id.credentials[1],
            		exp = cred.expires,
                    exp2,
                    diff,
                    diff2;

                diff = nisis.user.getTimeLeft(exp);

    			if ( $.isNumeric(diff) && diff > 0 ) {
    				valid = true;
                    nisis.user.loadTOLayers = true;
    			} else {
                    nisis.user.loadTOLayers = false;
    				nisis.ui.displayMessage(msg, 'error');
    			}
                //check weather services
                if ( !wsCred ) {
                    nisis.user.loadWSLayers = true;

                } else {
                    exp2 = wsCred.expires ? wsCred.expires : 0,
                    diff2 = nisis.user.getTimeLeft(exp2);
                }

                if ( $.isNumeric(diff2) && diff2 > 0 ) {
                    nisis.user.loadWSLayers = true;
                } else {
                    nisis.user.loadWSLayers = false;
                    //nisis.ui.displayMessage(msg.replace('map layers', 'weather layers'), 'error');
                }
            }

            if ( !valid ) {
                nisis.ui.displayMessage(msg, 'error');
            }

            return valid;
        },
        checkCredentials: function() {

        	var $this = this,
        		id, c;

        	if(window.localStorage && window.localStorage !== null){
                id = window.localStorage.getItem(this.credentials);
            } else {
                id = cookie(this.credentials);
            }

            if(id && id !== 'null' && id.length > 4){
                c  = JSON.parse(id);
            } else {
                console.log("Credentials have not been saved");
                return;
            }

            if (c && c.credentials.length) {

            	var start = c.credentials[0].creationTime,
            		sDate = new Date(start),
            		exp = c.credentials[0].expires,
            		eDate = new Date(exp),
            		cDate = new Date(),
            		MS_PER_DAY = 1000 * 60 * 60 * 24,
            		MS_PER_HR = 1000 * 60 * 60;

            	var utcSDate = new Date(
    					sDate.getUTCFullYear(),
    					sDate.getUTCMonth(),
    					sDate.getUTCDate(),
    					sDate.getUTCHours(),
    					sDate.getUTCMinutes(),
    					sDate.getUTCSeconds()
    				),
            		utcEDate = new Date(
    					eDate.getUTCFullYear(),
    					eDate.getUTCMonth(),
    					eDate.getUTCDate(),
    					eDate.getUTCHours(),
    					eDate.getUTCMinutes(),
    					eDate.getUTCSeconds()
    				),
            		utcCDate = new Date(
    					cDate.getUTCFullYear(),
    					cDate.getUTCMonth(),
    					cDate.getUTCDate(),
    					cDate.getUTCHours(),
    					cDate.getUTCMinutes(),
    					cDate.getUTCSeconds()
    				);

            	//var diff = (utcEDate - utcCDate) / MS_PER_HR;
                var diff = nisis.user.getTimeLeft(exp) / MS_PER_HR;

            	if (diff < 1) {
            		var count = 0,
            		max = 10, //1min
            		response = false,
            		options = {
            			title: 'Session timeout!',
            			message: "Your session will timeout very soon. Would you like to extend it?",
            			buttons: ['YES', 'NO']
            		};

            		nisis.ui.displayConfirmation(options)
            		.then(
        			function(resp) {
        				response = true;

        				if (resp.toUpperCase() !== options.buttons[0]) {
        					$this.logout();
        				} else {
                            $this.logout();
        				}
        			},
        			function(error) {
        				response = true;
        				nisis.ui.displayMessage('App could not capture your input.', 'error');
        			});

        			var counter = null;
        			var checkResponse = function() {
        				count++;

        				if (count > max && !response) {
        					console.log("App is terminating user session.");
        					$this.logout();
        				} else if (response) {
        					console.log("App is keeping user session alive.");
        					clearTimeout(counter);
        				} else {
        					console.log("App checked user session: ", new Date());
        					console.log("User session will timeout in: ", diff);
        				}
        			}

        			counter = setInterval(function() {
        				checkResponse();
        			}, 1000);
            	}
            }
        },
        clearCredentials: function(){
            //console.log("Clearing layers credentials ...");
            if(window.localStorage && window.localStorage !== null){
                window.localStorage.clear();
                window.localStorage.removeItem(this.credentials);
                kernel.id.credentials = [];
                kernel.id.serverInfos = [];
            } else {
                cookie(this.credentials);
            }
        },
        //NISIS-2837 - Kofi Honu - Added this function to get just layers user has access to.
        // Useful in tableview.js
        getUserAllowedLayers: function(){
            nisis.user.userAllowedLayers = [];
            var myGroups = nisis.user.groupObjects;
            var showPipelines = false;

            $.each(myGroups, function(i, group) {
                var name = group.GNAME.toLowerCase(); //Converting to lower-case to be case insensitive
                if (name === "administrators"){
                    nisis.user.isAdmin = true;
                }
                if((name.indexOf("pipeline") !== -1)){
                    //If Pipelines group is found even once, then don't hide pipelines layer
                    showPipelines = true;
                }
            });

            require(["app/layersConfig"], function(layersConfig) {
                $.each(layersConfig, function(i, layer) {
                    if (layer.add 
                        && (layer.group === 'nisisto' 
                            || layer.group === 'dot' 
                            || (layer.group === 'pipelines' && showPipelines))) {
                        nisis.user.userAllowedLayers.push(layer);
                    }
                });
            });
        },
        getUserGroups: function(userid){
        	var $this = this;
        	if(!userid){
        		console.error('User id is required');
        		return;
        	}
			var def = new $.Deferred(),
				gps = [];
			//get user groups
	    	$.ajax({
	    		type: "POST",
	    		url: "admin/api/",
	    		data: {
	    			'action': "acl",
	    			'function': 'getusergroups',
	    			id: userid
	    		},
	    		success: function(data, status, xhr) {
	    			var data = JSON.parse(data);
	    			// console.log('-------------App Groups: ', data);
	    			if(data.result.toLowerCase() == 'success'){
	    				var gps = data.items;
                        nisis.user.groups = [];
                        //KOFI: NISIS-2327-AGAIN: reducing calls to getUserGroups function
                        nisis.user.groupObjects = data.items;
	    				//nisis.user.features = [];
                        nisis.user.grpPrivileges = {};
	    				// if(!gps || (gps.length && gps.length === 0)){
							// nisis.user.features.push({
							// 	"text": 'No groups found',
							// 	"id": 'empty',
							// 	"type": "image",
							// 	"hasChildren": false
							// 	,"checked": true
							// });
						// } else {
                        if (gps && gps.length > 0){
                            nisis.ui.sortArrayOfObjects(data.items,'GNAME');
							$.each(data.items, function(i,g){
			    				nisis.user.groups.push(parseInt(g.GROUPID));
                                nisis.user.grpPrivileges[g.GROUPID] = g.PRIV;
                                // KOFI: IS THE FOLLOWING STILL NEEDED?
			    	// 			nisis.user.features.push({
								// 	"text": g.GNAME,
								// 	"id": "group_" + g.GROUPID,
								// 	"type": "folder",
								// 	"parent": "groups",
								// 	"hasChildren": true
								// 	,"checked": true
								// });
			    			});

			    			//Set the isGroupAdmin flag
                            nisis.user.setUserGroupAdminFlag();
						}
	    			}

	    			def.resolve(data.items);
	    		},
	    		error: function(xhr, status, err) {
	    			console.log('User Groups errors: ',arguments);
	    			nisis.user.groups = [];
	    // 			nisis.user.features = [];
	    // 			nisis.user.features.push({
					// 	"text": "Error loading groups",
					// 	"id": "error",
					// 	"type": "image",
					// 	"hasChildren": false
					// 	,"checked": true
					// });
	    			def.reject(err.message);
	    		}
	    	});
	    	return def.promise();
		},
        loadUserDefaultVisibleShapes_Recursive: function (jsonObj) {
            var $this = this;
            if( typeof jsonObj == "object" ) {
                $.each(jsonObj, function(k,v) {
                    if (k === 'ITEMTYPE' &&  v !== 'folder'){
                        nisis.user.userVisibleFeatures[v] += "," + jsonObj.SHAPEID;
                    }
                    else if (k === "STYLE" && v.hasOwnProperty("CREATED_USER")){
                        if ( nisis.user.styles ) {
                            nisis.user.styles = nisis.user.styles.concat(v);
                        } else {
                            nisis.user.styles = [];
                            nisis.user.styles = nisis.user.styles.concat(v);
                        }
                    }
                    $this.loadUserDefaultVisibleShapes_Recursive(v);
                });
            }
        },
        getUserDefaultVisibleShapes: function(){
            var $this = this;
            nisis.user.userVisibleFeatures['polygon'] = "-1";
            nisis.user.userVisibleFeatures['point'] = "-1";
            nisis.user.userVisibleFeatures['line'] = "-1";
            nisis.user.userVisibleFeatures['text'] = "-1";

            var def = new $.Deferred(),
                shapeLayers = [];
            //get user default visible shapes
            $.ajax({
                type: "POST",
                url: "api/",
                data: {
                    action: "get_my_features_tree"
                },
                success: function(data, status, xhr) {
                    var response = JSON.parse(data);
                    if (response.result.toLowerCase() == 'success'){
                        $this.loadUserDefaultVisibleShapes_Recursive(response);

                        require(["app/renderers"], function(renderers){
                            renderers.buildCustomStyles(nisis.user.styles);
                        });
                    }
                    def.resolve(response);
                },
                error: function(xhr, status, err) {
                    console.log('User Def Visible Shapes error: ',arguments);
                    // KOFI.  REMEMBER TO CODE THIS ERROR HANDLER TOO
                    // nisis.user.groups = [];
                    // nisis.user.features = [];

                    def.reject(err.message);
                }
            });
            return def.promise();
        },
		setUserGroupAdminFlag : function(){
			var privs = nisis.user.grpPrivileges;
			for (var key in privs) {
				if (privs.hasOwnProperty(key)) {
					if (key !== "0"){
						if (!nisis.user.isGroupAdmin){
							nisis.user.isGroupAdmin = (privs[key] === "3") ? true : false;
						}else{
							break;
						}
					}
				}
			}
		},
		getProfile : function(){
		    var $this = this;
			nisis.user.userid = parseInt($('#user-userid').val(), 10);
			nisis.user.username = $('#user-username').val();
			nisis.user.name = $('#user-name').val();
			//get user groups
			$.ajax({
				url : 'api/',
				type : 'GET',
				data : {
					'action' : 'query',
					'tables' : 'NISIS_ADMIN.ACCESSGROUP',
					'values' : 'USERGROUP',
					'constraints' : '"USERID" = ' + nisis.user.userid
				},
				success : function(data, status, xhr) {
					var resp;
					try{
						resp = JSON.parse(data);
					}catch(e){
						console.log(data,e);
						nisis.ui.displayMessage(e.message, 'Parsing Error!', 'error');
						return;
					}
					if (resp['return'] && resp['return'] == 'Success') {
						if(resp.results.length > 0){
							var groups = $.map(resp.results,function(group,i){
								if(isNaN(parseInt(group.USERGROUP,10))){
									return 0;
								}else{
									return group.USERGROUP;
								}
							});
							//console.log('User groups: ' + groups);
							$this.getCredentials(groups);
							//nisis.ui.mapview.buildMapLayers(groups);
						}else{
							nisis.ui.displayMessage('This user is not part of any group.', 'Groups!', 'warn');
						}
					} else {
						nisis.ui.displayMessage('The APP could not get your profile. Please try again.', 'Failure!', 'warn');
					}
				},
				error : function(xhr, status, err) {
					//console.log(err);
					nisis.ui.displayMessage(status, 'Error!', 'error');
				}
			});
		},
// KOFI: Obsolete function.
// 		getStyles: function(){
// 			// console.log('App is retreiving user styles ...');
// 			$.ajax({
// 		        url: "api/",
// 		        type: "POST",
// 		        data: {
// 		            action: "get_styles",
// 		        },
// 		        success: function(data, status, req){
// 		            var s = JSON.parse(data),
// 		            	l = s.results.length,
// 		            	n = s.errors.length;

// 		            if ( l > 0 ) {
// 		            	var styles = [];
// 		            	$.each(s.results, function(i, style){
// 		            		styles = styles.concat(style);
// 		            	});
// console.log("---------------------------adding style:",styles);
// 		            	if ( styles.length > 0 ) {
// 		            		if ( nisis.user.styles ) {
// 		            			nisis.user.styles = nisis.user.styles.concat(styles);
// 		            		} else {
// 		            			nisis.user.styles = [];
// 		            			nisis.user.styles = nisis.user.styles.concat(styles);
// 		            		}
// 		            	}

// 		            	require(["app/renderers"], function(renderers){
// 		            		renderers.buildCustomStyles(styles);
// 		            	});
// 		            }

// 		            if ( n > 0 ) {
// 		            	var e = "";
// 		            	$.each(s.errors, function(i, err){
// 		            		if ( i === 0 ) {
// 		            			e += err;
// 		            		} else if ( i < n-1 ) {
// 		            			e += ', ' + err;
// 		            		} else {
// 		            			e += err + '.';
// 		            		}
// 		            	});

// 		            	nisis.ui.displayMessage(e, '', 'error');
// 		            }
// 		        },
// 		        error: function(req, status, err){
// 		        	console.log("Done retreiving user styles");
// 		            console.log("User styles error: ", arguments);
// 		        }
// 		    });
// 		},
		getAppsAccess: function(){
			var $this = this,
				def = new $.Deferred(),
				msg = "Error trying to get the Apps Access for the user";

			$.ajax({
		        url: "api/",
		        type: "POST",
		        data: {
		            action: "get_apps_access",
		        },
		        success: function(data, status, req){
		            var response = null;

		            try {
		            	response = JSON.parse(data);
		            } catch (err) {
		            	def.reject(err.message);
		            }

		            if (response && response.userapps.length) {
			            nisis.user.apps = response.userapps;

			            if ( $.inArray('TFR', response.userapps) !== -1 ) {
			            	$this.tfrAccess = true;
			            }

			            def.resolve(response);
			        } else {
			        	def.reject(msg);
			        }
		        },
		        error: function(req, status, err){
		        	console.error(msg);
		            nisis.ui.displayMessage(msg, 'error');
		            def.reject(msg);
		        }
		    });

		    return def.promise();
		},
		getTFRGroupsAccess: function(){
			$.ajax({
		        url: "api/",
		        type: "POST",
		        data: {
		            action: "get_tfr_groups_access",
		        },
		        success: function(data, status, req){
		            var response = JSON.parse(data);
		            nisis.user.tfrgroups = response.tfrgroups;
		        },
		        error: function(req, status, err){
		        	console.error("Error trying to get the TFR Groups Access");
		            nisis.ui.displayMessage('Error trying to get the TFR Groups Access', 'Error', 'error');
		        }
		    });
		},
        getLayersPks: function(){
            //Getting the Primary Keys and put them in an object (nisis.layersPks) for easy access when it's needed
            $.ajax({
                url: "api/",
                type: "POST",
                data: {
                    action: "get_layers_pks",
                },
                success: function(data, status, req) {
                    var resp = JSON.parse(data);
                    var layersPks = resp.layersPks;
                    nisis.layersPks = layersPks;
                },
                error: function(req, status, err){
                    console.error("ERROR in ajax call to get all the layers primary keys in getLayersPks function - base.js");
                    console.error("req: ", req);
                    console.error("status: ", status);
                    console.error("err: ", err);
                }
            });
        },
		name: null,
		token: {
			token: null,
			expires: null
		}
	},
	util : {
		reporter : {
			report : {},
			getReport : function() {
				nisis.util.reporter.report = $.get("api/", {
					action : "report"
				}, function(data) {
					if (data.type != "none") {
						nisis.ui.displayMessage(data.message, data.type, 0);
					}
				}, "json").done(nisis.util.reporter.getReport);
			}
		}
	},
	log: function() {
		var host = window.location.hostname,
			debug = false;

		if (host === 'localhost') {
			debug = true;
		}

		if (debug && arguments.length) {
			for (var i=0; i<arguments.length; i++) {
				console.log(arguments[i]);
			}
		}
	}
};

});

$(document).ready(function() {
	nisis.ui.content = $("#content");
	nisis.ui.msg = $('#messages').kendoNotification({
		position: {
			left: 10
		},
		button: true
		//,autoHideAfter: 10000
	}).data("kendoNotification");

	//Getting the Picklists and put them in an object (nisis.picklists) for easy access when it's needed
	$.ajax({
        url: "api/",
        type: "POST",
        data: {
            action: "get_all_picklists",
        },
        success: function(data, status, req) {
            var resp = JSON.parse(data);
            var picklists = resp.picklists;
            nisis.picklists = picklists;
        },
        error: function(req, status, err){
            console.error("ERROR in ajax call to get all the picklists fields in base.js");
            console.error("req: ", req);
            console.error("status: ", status);
            console.error("err: ", err);
        }
    });

});
