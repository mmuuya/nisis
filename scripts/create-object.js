function DefaultLoad() {
    $("#airport-stuff").hide();
    $('#atm-stuff').hide();
}

function ChangeForm(nisisO) {

var selection= nisisO;
var value = nisisO.value;
console.log(value);

if (value == 0) {
    $("#airport-stuff").hide();
    $('#atm-stuff').hide();
}

if (value == 1) {
    $("#airport-stuff").show();
    $('#atm-stuff').hide();
}

if (value == 2) {
    $("#atm-stuff").show();
    $('#airport-stuff').hide();
}
};


function HideSearch(){
$("#search").hide();
};

function ShowSearch() {
$("#search").show("slide");
};

// This function returns the fields for the appropriate airport object.
function FindObject() {

    console.log("smart people do stuff to retrieve object")
}

// This function is used to populate the ATM Fields from the ATM table into the Airports object
function FindATMObject() {


    console.log("smart people do stuff to retrieve object")
}

function FindANSObject() {


    console.log("smart people do stuff to retrieve object")
}

function DropdownResize () {

var resizeDropDown = function (e) {
    var $dropDown = $(e.sender.element),
        listWidth = 153,
        containerWidth = 153;

    // Set widths to the new values
    $dropDown.data("kendoDropDownList").list.width(listWidth);
    $dropDown.closest(".k-widget").width(containerWidth);

}

// Default implementation with Kendo CSS sizing
$("#defaultSize").kendoDropDownList();

// Resized on data bound to the width specified above
$("#cmbState").kendoDropDownList({
   dataBound: resizeDropDown
});
}

function DropDownPopulate(){

//Populate the States Dropdown
var ddstates    = document.getElementById("ddState");
var ddATMState  = document.getElementById("ddATMState");

var us_states   = ['AL','AK','AS','AZ','AR','CA','CO','CT','DE','DC','FM','FL','GA','GU','HI','ID','IL','IN','IA','KS','KY','LA','ME','MH','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','MP','OH','OK','OR','PW','PA','PR','RI','SC','SD','TN','TX','UT','VT','VI','VA','WA','WV','WI','WY'];

    for(var i = 0; i < us_states.length; i++) {
        var opt = us_states[i];
        var el = document.createElement("option");
        var el2 = document.createElement("option");
        
        el.textContent = opt;        
        el.value = opt;

        el2.textContent = opt;        
        el2.value = opt;
        
        ddstates.appendChild(el);
        ddATMState.appendChild(el2);
    }

//Time Zones

var ddTimeZone = document.getElementById("ddTimeZone");
var ddATMTimeZone = document.getElementById("ddATMTimeZone");

var time_zones = ['EST','CST','MST','PST','AKDT','HST'];

    for(var i = 0; i < time_zones.length; i++) {
        var opt = time_zones[i];

        var el = document.createElement("option");
        var el2 = document.createElement("option");

        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;

        ddTimeZone.appendChild(el);
        ddATMTimeZone.appendChild(el2);
    }

//Populate the FAA Regions
var regions     = document.getElementById("ddFAARegion");
var atm_regions = document.getElementById("ddATMFAARegion");

var faa_regions = ['AAL','ACE','AEA','AGL','ANM','ASO','ASW'];

    for(var i = 0; i < faa_regions.length; i++) {
        var opt = faa_regions[i];
        var el = document.createElement("option");
        var el2 = document.createElement("option");

        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;

        regions.appendChild(el);
        atm_regions.appendChild(el2);
    }

//Populate ATO Service Area
var ato = document.getElementById("ddATO");
var atm_ato = document.getElementById("ddATMATO");

var ato_types = ['Central','Western','Eastern'];

    for(var i = 0; i < ato_types.length; i++) {
        var opt = ato_types[i];

        var el = document.createElement("option");
        var el2 = document.createElement("option");

        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;

        ato.appendChild(el);
        atm_ato.appendChild(el2);
    }

//Populate FEMA
var fema = document.getElementById("ddFEMA");
var ddATMFEMA = document.getElementById("ddATMFEMA");

var fema_regions = ['1','2','3','4','5','6','7','8','9','10'];

    for(var i = 0; i < fema_regions.length; i++) {
        var opt = fema_regions[i];
        var el = document.createElement("option");
        var el2 = document.createElement("option");

        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;

        fema.appendChild(el);
        ddATMFEMA.appendChild(el2);
    }

//Populate Airport Use
var use = document.getElementById("ddUse");
var arp_use = ['PU','PR','MG'];

    for(var i = 0; i < arp_use.length; i++) {
        var opt = arp_use[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        use.appendChild(el);
    }

//Populate Classification
var classification = document.getElementById("ddClass");
var arp_class = ['N/A','CS','GA','P'];

    for(var i = 0; i < arp_class.length; i++) {
        var opt = arp_class[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        classification.appendChild(el);
    }

//Populate Hub
var hub = document.getElementById("ddHub");
var hub_types = ['N','S','M','L'];

    for(var i = 0; i < hub_types.length; i++) {
        var opt = hub_types[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        hub.appendChild(el);
    }

//Populate Tower Type
var ddTowerType = document.getElementById("ddTowerType");
var ddATMTowerType = document.getElementById("ddATMTowerType");

var tower_type = ['ATCT (without Radar)','ATCT (with Radar)','Combined TRACON/Tower','Combined Non-Radar Approach/Tower','Combined Control Facility','FCT','Municipal Tower','None','Other','Unknown'];

    for(var i = 0; i < tower_type.length; i++) {
        var opt = tower_type[i];
        var el = document.createElement("option");
        var el2 = document.createElement("option");

        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;

        ddTowerType.appendChild(el);
        ddATMTowerType.appendChild(el2);
    }
/*
//Populate Operational Status
var ddOpStatAC  = document.getElementById("ddOpStatAC");
var ddOpStatATC = document.getElementById("ddOpStatATC");
var ddOpStat    = document.getElementById("ddOpStat");
var ddOvrStat   = document.getElementById("ddOvrStat");

var op_status = ['0 - Operational','1','2','3','4'];

    for(var i = 0; i < op_status.length; i++) {
        var opt = op_status[i];
        var el = document.createElement("option");
        var el2 = document.createElement("option");
        var el3 = document.createElement("option");
        var el4 = document.createElement("option");

        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;

        el3.textContent = opt;
        el3.value = opt;

        el4.textContent = opt;
        el4.value = opt;

        //ddOpStatAC.appendChild(el);
        //ddOpStatATC.appendChild(el2);
        //ddOpStat.appendChild(el3);
        //ddOvrStat.appendChild(el4);       
    }
*/

//Populate Fuel Type and Supplies
var ddFuel = document.getElementById("ddFuel");
var fuel_type = ['100  A','100LLA1+  B','100  A    A1','A    MOGAS','100LL80   MOGAS','100LLA    B','100LLA    B+','100  A    A1+','100  100LLA+','100LL','B+','100  80','100','100  100LLA','A1','100LL80   A1+','100LL115  B+','100  115','100LLB','115  A1','100LLA','100LLA1+','100LLA    A1+','100  100LLB+','100  80   B+','A1+  B+','100  A1+','100  B+','100  80   MOGAS','100LLA    MOGAS','100LL80   A','100LL115  MOGAS','100LLA1   A1+','100LLA    A1   A1+  B','100  B    MOGAS','100LLA    A1','100  100LLA    MOGAS','100  100LLA1+','100  100LL80   A','A','80','100LLMOGAS','100LLA+','MOGAS','115  B+','100  A    MOGAS','100  A1','100LLB+','100  MOGAS','100  115  A+','A+','100  100LL','B','80   MOGAS','100LLA    A+   A1+','100LLA1','100  100LLA    A1','A1+','100LLA+   MOGAS','100  80   A','100  80   A    A1+  B','100LLA1+  B+','115  A+   B+','100LLA    A1   B','100LL80   A    A1+','100LLA    A1   A1+','100LLA    A+','100LL80','100  A    A+','100LLA1+  MOGAS','100LLA1   B'];

    for(var i = 0; i < fuel_type.length; i++) {
        var opt = fuel_type[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddFuel.appendChild(el);
    }

//Populate Supplies
var ddFuelSup = document.getElementById("ddFuelSup");
var fuel_supp = ['Normal Supplies', 'Limited Supplies', 'No Supplies', 'Unknown', 'Other'];

    for(var i = 0; i < fuel_supp.length; i++) {
        var opt = fuel_supp[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddFuelSup.appendChild(el);
    }

//Populate Aviation Operations
var ddAviationOpStat = document.getElementById("ddAviationOpStat");
var aviation_ops = ['Normal', 'Constrained', 'Suspended', 'Other', 'Unknown'];

    for(var i = 0; i < aviation_ops.length; i++) {
        var opt = aviation_ops[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddAviationOpStat.appendChild(el);
    }

//Populate Airport Ownmer/Operator Operations Status
var ddAptOwnOperOpStat = document.getElementById("ddAptOwnOperOpStat");
var own_op_status = ['Normal Services', 'Limited Services', 'No Services', 'Unknown', 'Other'];

    for(var i = 0; i < own_op_status.length; i++) {
        var opt = own_op_status[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddAptOwnOperOpStat.appendChild(el);
    }

//Populate TSA Screening and CBP Screening
var ddTSAScreenStat = document.getElementById("ddTSAScreenStat");
var ddCBPAptOwnOperOpStat =document.getElementById("ddCBPAptOwnOperOpStat");
var tsa_cbp_status = ['Normal Throughput', 'Constrained Capacity', 'Screening Suspended', 'Unknown', 'Other'];

    for(var i = 0; i < tsa_cbp_status.length; i++) {
        var opt = tsa_cbp_status[i];
        var el = document.createElement("option");
        var el2 = document.createElement("option");
        
        el.textContent = opt;
        el.value = opt;

        el2.textContent = opt;
        el2.value = opt;        

        ddTSAScreenStat.appendChild(el);
        ddCBPAptOwnOperOpStat.appendChild(el2);
    }
/*************************************
*                                       
*
*   ATM PROFILE POPULATE
*
*
*
***************************************/
// ATM Type of ATM Facility
var ddAviationOpStat = document.getElementById("ddATMType");
var atm_type = ['ARTCC','ATC/TRACON', 'ATCT', 'ATCT/TRACON', 'FCT', 'FSS', 'TRACON'];

    for(var i = 0; i < atm_type.length; i++) {
        var opt = atm_type[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddATMType.appendChild(el);
    }

//Responsible Service Unit

var ddATMRSU = document.getElementById("ddATMRSU");
var atm_rsu = ['AJT (Terminal)','AJE (En Route and Oceanic)', 'AJR (Systems Operations)'];

    for(var i = 0; i < atm_rsu.length; i++) {
        var opt = atm_rsu[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddATMRSU.appendChild(el);
    }

// Detail Type of ATM Facility
var ddATMDetail     = document.getElementById("ddATMDetail");

var atm_det_route   = ['ARTCC', 'CERAP'];
var atm_det_trm     = ['ATCT', 'ATCT/TRACON', 'TRACON', 'Combined TRACON', 'FCT', 'Non-Fed Tower'];
var atm_det_cftm    = 'ATCSCC'
var atm_det_fis     = ['AFSS', 'FSS', 'LM AFSS'];

    for(var i = 0; i < atm_det_route.length; i++) {
            
        if (i == 0) {
            var grp = document.createElement("optgroup");
            grp.label = "Enroute";
            ddATMDetail.appendChild(grp);
        }

        var opt = atm_det_route[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddATMDetail.appendChild(el);
    }

    for(var i = 0; i < atm_det_trm.length; i++) {
        
        if (i == 0) {
            var grp2 = document.createElement("optgroup");
            grp2.label = "Terminal";
            ddATMDetail.appendChild(grp2);
        }

        var opt = atm_det_trm[i];
        var el2 = document.createElement("option");
        el2.textContent = opt;
        el2.value = opt;
        ddATMDetail.appendChild(el2);
    }

        var grp3 = document.createElement("optgroup");
        grp3.label = "CTFM";
        ddATMDetail.appendChild(grp3);

        var el3 = document.createElement("option");
        el3.textContent = atm_det_cftm;
        el3.value = opt;
        ddATMDetail.appendChild(el3);

        for(var i = 0; i < atm_det_fis.length; i++) {
        
            if (i == 0) {
                var grp4 = document.createElement("optgroup");
                grp4.label = "FIS";
                ddATMDetail.appendChild(grp4);
            }

            var opt = atm_det_fis[i];
            var el4 = document.createElement("option");
            el4.textContent = opt;
            el4.value = opt;
            ddATMDetail.appendChild(el4);

// ATM Operations Status

var ddATMOpStat = document.getElementById("ddATMOpStat");
var atm_stat = ['Normal Ops','ATC Alert', 'ATC Limited', 'ATC 0', 'Unknown', 'Other'];

    for(var i = 0; i < atm_stat.length; i++) {
        var opt = atm_stat[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        ddATMOpStat.appendChild(el);
    }

// ATM Facility EG Support
var ddATMEGSup = document.getElementById("ddATMEGSup");
var atm_eg = ['EG Equipped','No EG','Unknown'];

    for(var i = 0; i < atm_eg.length; i++) {
        var opt = atm_eg[i];

        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;

        ddATMEGSup.appendChild(el);
    }

// ATM Facility Power Status
var ddATMPS = document.getElementById("ddATMPS");
var atm_power = ['Main Power','Disrupted Power/No EG','Disrupted Power/Using EG', 'Unknown'];

    for(var i = 0; i < atm_power.length; i++) {
        var opt = atm_power[i];

        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;

        ddATMPS.appendChild(el);
    }



    }



















}
