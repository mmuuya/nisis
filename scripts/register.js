$(document).ready(function() {
	var content = $("#content");
	content.find(':button').button();

	$("#backToLogin").click(function() {
		content.fadeOut(400, function() {
			$.get('html/login.html', {}, function(data) {
				content.html(data);
			}).always(function() {
				content.fadeIn(400, function() {
					$("body").spin(false);
				});
			});
		});
	});

	//clear registration form
	$('#reset').click(function() {
		$('#fname').val('');
		$('#lname').val('');
		$('#email').val('');
		$('#username').val('');
		$('#password').val('');
		$('#cpassword').val('');
	});
});
