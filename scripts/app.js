// Style document on load
require(['app/appUtils','app/appConfig','app/mapConfig','app/windows/layers','app/windows/features','app/windows/shapetools',
  'app/windows/filter','app/windows/tableview', 'app/windows/iwl', 'app/windows/print',
  'app/dashboard','app/windows/tfrBuilder','app/windows/locations'],
  function (utils,appConfig,mapConfig,layers,features,shape,
  filter,table, iwl, print, dash, tfr, locations) {
    $('#app').ready(function () {
      nisis.log = utils.log;
    // keep a ref of all widgets
      if (nisis.ui) {
        nisis.ui.widgets = {};
      } else {
        nisis.ui = {};
        nisis.ui.widgets = {};
      }
      function openmenu(e){
          var $header = $('#header');          
          $(this).fadeOut();
          $header.delay(200).fadeIn();
        };
      $('#openBtn').on('click', openmenu);
      // get ride of the background image
      $('#content').css({
        'height':'100%',
        'overflow':'hidden',
        'background-image':'none',
      });
    // get a reference of mapview object
      var mapview = nisis.ui.mapview,
        user = nisis.user,
        widgets = nisis.ui.widgets;
      // retreive userinfo
      user.userid = $('#user-userid').val();
      user.username = $('#user-username').val();
      user.name = $('#user-full-name').val();
      // KOFI: WHAT IS THIS USED FOR?  FIND OUT LATER....
      // user.query = 'CREATED_USER = '" + user.username + "' OR USER_GROUP IN (" + user.groups.join(',') + ")";
    
      // start up polling for TFR builder / NES statuses every 10 minutes
      setInterval(function () {
        $.ajax({
          type: 'GET',
          url: 'api/tfr/NESStatus.php',
          success: function (data) {
            // if tfrGrid is open, refresh it
            if ($('#tfrGrid').length > 0) {
                 // refresh our grid, we just refreshed that database (pulled new statuses from NES)
                 // var tfrGrid = $('#tfrGrid').data('kendoGrid');
                 // one more ajax call to refresh the datasource and the grid
              $.ajax({
                url: 'html/windows/tfr/tfr-open.php',
                type: 'GET',
                success: function (data) {
                  var d;
                  try {
                    d = JSON.parse(data);
                  } catch (e) {
                    console.log(e);
                  }
                  if (d) {
                    $('#tfrGrid').data('kendoGrid').dataSource.data(d);
                    $('#tfrGrid').data('kendoGrid').refresh();
                  }
                },
                error: function () {
                  console.log(arguments);
                },
              });
            }
          },
          error: function (textStatus, errorThrown) {
            console.log('call to NESStatus.php failed');
          },
        });
      }, 600000);
      var buildApplication = function (apps) {
      // set the default app
        var msg = 'You dont seem to have access to any module of NISIS. Please contact NISIS Administrator.';
        if (!apps || (apps && !apps.length)) {
          nisis.ui.displayMessage(msg, 'error');
          return;
        }
        var app = 'nisis';
        if ( $.inArray(app.toUpperCase(), apps) !== -1 ) {
          appConfig.set('app', app);
        } else {
          app = apps[0].toLowerCase();
          appConfig.set('app', app);
        }
        // update shapetools app
        shape.set('app', app);
        if (!app) {
          nisis.ui.displayMessage(msg, 'error');
          return;
        } else {
          if (app.toLowerCase() === 'tfr') {
            appConfig.set('tfrMode', true);
            if (apps.length === 1) {
              // hide exit mode to tfr only user
              tfr.set('exitTfrMenu', false);
            }
          }
          // update main menu
          appConfig.updateMainMenu();
        }
    // delete features on delete press
        $(document).keyup(function (evt) {
          // console.log(evt);
          // keyCode: 13 ==> enter, 46 ==> delete
          // HH => added check to see if popup was visible before prompting feature delete.
          if (evt.keyCode === 46 && $('.esriPopup') && $('.esriPopup')[0].style.visibility === 'hidden') {
            // console.log('Delete key pressed');
            var selFeats = nisis.ui.mapview.featureEditor.attributeInspector._selection;
            if (selFeats!= null && selFeats.length > 0) {
              $.each(selFeats,function (idx,feat) {
                if (feat.getLayer().capabilities.indexOf('Delete') !== -1) {
                  var remove = nisis.ui.mapview.util.removeFeatureFromUserLayers(feat);
                  if (!remove) return;

                  remove.then(function (d) {
                    var id = feat.attributes.OBJECTID ? feat.attributes.OBJECTID : feat.attributes.FID,
                      success = 'Feature #' + id + ' is removed';
                    nisis.ui.displayMessage(success, 'Remove Successful!');
                  }, function (e) {
                    // console.log(e);
                    var error = e.message ? e.message : e;
                    nisis.ui.displayMessage(error, 'Remove Failed!', 'error');
                  });
                } else {
                  nisis.ui.displayMessage('This feature can not be removed', 'Remove Denied!', 'warn');
                }
              });
            }
          } else if (evt.keyCode === 122) { // F11 key
            if ($(window).height() === screen.height) {
              // console.log('in full screen');
              $('#full-screen').children().removeClass('fa-arrows-alt')
              .addClass('fa-times-circle-o');
            } else {
                  // console.log('out of full screen');
              $('#full-screen').children().removeClass('fa-times-circle-o')
              .addClass('fa-arrows-alt');
            }
          } else if (evt.keyCode === 27){ //Esc key
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
                $("#full-screen").children().removeClass('fa-times-circle-o').addClass('fa-arrows-alt');
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
                $("#full-screen").children().removeClass('fa-times-circle-o').addClass('fa-arrows-alt');
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
                $("#full-screen").children().removeClass('fa-times-circle-o').addClass('fa-arrows-alt');
            }
          }
      // short cuts ==> Alt + b, etc ...
          else if ( evt.altKey ) {
            switch ( evt.keyCode ) {
              case 66: // b => Basemaps
                $('#basemaps').trigger('click');
                break;
              case 76: // l => Map Layers
                $('#layers').trigger('click');
                break;
              case 75: // k => Map Legend (Keys)
                $('#legend').trigger('click');
                break;
              case 86: // v => View Options (Options)
                $('#view-options').trigger('click');
                break;
              case 83: // s = Shape Tools
                $('#drawing').trigger('click');
                break;
              case 77: // m => Measurements
                $('#measure').trigger('click');
                break;
              case 78: // n => Directions (Navigation)
                $('#directions').trigger('click');
                break;
              case 84: // t => Table View
                $('#tableview').trigger('click');
                break;
              case 73: // i => IWL
                $('#tbl-review-iwl').trigger('click');
                break;
              case 82: // r => Dashboard (Reports)
                $('#dashboard').trigger('click');
                break;
              case 65: // a => Alerts
                $('#alertsview').trigger('click');
                break;
              case 191: // => Help (Phone)
                $('#help > a').trigger('click');
                break;
              case 85: // u => admin (Users)
                $('#admin-menu-link').trigger('click');
                break;
            }
          }
        });


      // show user controls
        $('#search-and-admin').show();
        $('#search').removeClass('hide');
        // this is just annoying was, default to show and it looked like crap.
        $('#toggle-header').hide();
        // handle map ready
        $('#map').ready(function () {
            // prevent default contextual menu on map div
          document.getElementById('map').oncontextmenu = function () {
            return false;
          };
        // initialize map
          mapview.initMap();
          // bind app events
          $('#app').on('nav-change', mapConfig.updateNavTools);
          $('#app').on('layers-ready', function (evt) {
            // console.log('Layers Ready Handler: ', arguments);
            layers.updateLayersTree();
          });
          $('#app').on('module-change', function (evt) {
            appConfig.updateMainMenu();
            layers.updateLayersTree();
          });
        });
        // Initialize kendo widgets
        // kendo.bind($('#header'),appConfig);
        kendo.bind($('#title-box'), appConfig);
      // kendo.bind($('#menu-bar'),appConfig);
      // kendo.bind($('#toggle-header'), appConfig);
        kendo.bind($('#search'), appConfig);
      // Search box
        $('#search-box')
          .on('focus',function (evt) {
            appConfig.expandSearch(evt);
          })
          .on('blur',function (evt) {
            appConfig.restoreSearch(evt);
          })
          .on('keyup',function (evt) {
            if (!appConfig.get('autoCompleteItemSelected')) {
              if (evt.keyCode === 13) {
                appConfig.geocodeAddress(evt);
                // Clearing the quicksearch after it is done
                $('#search-box').data('kendoAutoComplete').value('');
              }
            } else {
              appConfig.set('autoCompleteItemSelected', false);
            }
          });
        // bind widget
        kendo.bind($('#map-header'), appConfig);
        kendo.bind($('#user-controls'), appConfig);
        kendo.bind($('#footer'), appConfig);
        // start app clock
        appConfig.updateClock();
        // map navigation
        kendo.bind($('#nav-div'), mapConfig);
        // layers
        kendo.bind($('#layers-content'), layers);
        // CC - We are binding 'eatures-content' div (features.php) with the JS kendo object features located in features.js file.
        kendo.bind($('#features-content'), features);
        $('#overlays-toggle').data('kendoTreeView')
        .dataSource.bind('change',function (e) {
          layers.applyLayersTreeChange(e);
        });
        $('#threat-layers').data('kendoTreeView')
        .dataSource.bind('change',function (e) {
          layers.applyLayersTreeChange(e);
        });   
        $('#arcgis-online').data('kendoTreeView')
        .dataSource.bind('change',function (e) {
          layers.applyLayersTreeChange(e);
        });             
        // update layers tree
        $('#app').trigger('layers-ready');

          // view opions
        kendo.bind($('#view-options-content'), appConfig);
        // drawing tools
        kendo.bind($('#drawing-content'), shape);
        // force label updates
        $('#labelText').keyup(function (e) {
          var text = e.target.value;
          shape.set('labelText',text);
          shape.labelCheck();
        });
    // make vertices sortables
    // $('.dt-vertices').sortable();
    /* $('.dt-vertices').kendoSortable({
      axis: "y",
      cursor: 'move',
      handler: '.dt-v-handler',
      ignore: 'input',
      change: function(e){
        var vertices = e.sender.element[0];
        shape.updateVerticesCount(vertices);
      }
    });*/
    // upload file event handler
        $('#ft-submitFile').on('click',function (evt) {
          nisis.ui.mapview.util.processInputFile(evt);
        });

        $('#ft-clearFile').click(function (evt) {
          $('#ft-uploadFile').val('');
          $('#ft-processMsg').html('');
        });

        $('#lt-featList').keyup(function (e) {
          var list = e.target.value;
          shape.set('fList', list);
          shape.checkListForm();
        });
        // copy lat/lon events
        $('#dt-copy-coords').on('copy', function (evt) {
          // console.log('Copy:', evt);
          evt.stopPropagation();
          evt.preventDefault();
          return;
          // var copy = evt.originalEvent.clipboardData;
          // copy.setData('text/plain', 'Hello World!!!');
          // console.log('Copy:', evt.originalEvent);
        });
        // paste lat/lon in input fields
        $('input.dt-ll').on('paste', function (evt) {
          evt.preventDefault();
          shape.populateCoordValues.call(shape, evt);
        });
        // geocoding
        kendo.bind($('#geocode-address'), locations);
        // filter binding
        // kendo.bind($('#filter-div'), filter);
        // tableview binding
        kendo.bind($('#tableview-div'), kendo.observable(table));
        // filter binding
        kendo.bind($('#iwl-div'), iwl);
        // print widget
        kendo.bind($('#print-div'), print);
        // dashboard widget bindings
        kendo.bind($('#dashboard-div'), kendo.observable(dash));
        // TFR Builder
        if (nisis.user.tfrAccess) {
          kendo.bind($('#tfr-div'), tfr);
        } else {
          $('#tfr-div').remove();
        }
        // kendo input type text does not have change event, so this need to me done after the binding
        $('#print-title').keyup(function (e) {
          var title = e.target.value;
          print.set('title',title);
          print.setPrint();
        });
        // data blocks
        $('#data-blocks').click(function () {
          $(this).hide();
          $(this).html('');
        });
        // update menu items on window
        $('.dialog').each(function () {
          // get a ref of the window
          var kWin = $(this).data('kendoWindow');
          if (!kWin) {
            return;
          }
          // ref of dialog boxes
          var widget = $(this).attr('id');
          if (widget) {
            widget = widget.split('-')[0];
            widgets[widget] = kWin;
          }
          // update menu items on window open
          kWin.bind('open',function (e) {
            var w = e.sender.element[0].id;

            $('.menu-item').each(function () {
              var target = $(this).data('target');
              if (target === w) {
                $(this).addClass('menu-item-active');
                return;
              }
            });
          });
      // update menu items on window close
          kWin.bind('close',function (e) {
            var w = e.sender.element[0].id;

            $('.menu-item').each(function () {
              var target = $(this).data('target');
              if (target === w) {
                $(this).removeClass('menu-item-active');
                return;
              }
            });
        // clear measurement results
            if (w === 'measure-div') {
              nisis.ui.mapview.measure.clearResult();
            }
          });
        });
    // hide the spinner for the content
        $('#content').spin(false);
        $('body').spin(false);
      };

  /*
   * Getting the apps which the user has access
   * Build applications based on access
   */
      nisis.user.getAppsAccess()
      .then(
      function (response) {
        if (!response || !response.userapps || !response.userapps.length) {
          return;
        }

        if ($.inArray('NISIS', nisis.user.apps) !== -1 ) {

          //NISIS-2327-AGAIN: KOFI - 
          nisis.user.getUserGroups(nisis.user.userid)
          .then(  
          function(uresp){
            nisis.user.groupObjects = uresp;
            //NISIS-2837 - Kofi Honu - Added this function to get just layers user has access to.
            // Useful in tableview.js
            nisis.user.getUserAllowedLayers();
            nisis.user.getUserDefaultVisibleShapes()
            .then(
            function (resp) {
              if (!resp ) {
                return;
              }
              // initialize all kendoui widgets
              kendo.init('#app');
              // build app menu
              nisis.ui.buildAppMenu();
              // Getting the tfrgroups which the user has access
              nisis.user.getTFRGroupsAccess();
              // get user styles
              // user.getStyles();
              // build application
              buildApplication(response.userapps);
              // display spinner
              nisis.ui.startProcess();
              // bind window resize event
              utils.addEvent($('#app')[0], 'resize', nisis.ui.collapseAppMenu);
              // utils.addEvent(window, 'resize', nisis.ui.collapseAppMenu);
            },
            function (err) {
              nisis.ui.displayMessage(err, 'error');
              // hide the spinner for the content
              $('#content').spin(false);
              $('body').spin(false);
            });
          });
        } else {
          // initialize all kendoui widgets
          kendo.init('#app');
          // build app menu
          nisis.ui.buildAppMenu();
          // Getting the tfrgroups which the user has access
          nisis.user.getTFRGroupsAccess();
          // get user styles
          // user.getStyles();
          // build application
          buildApplication(response.userapps);
          // display spinner
          nisis.ui.startProcess();
          // bind window resize event
          utils.addEvent($('#app')[0], 'resize', nisis.ui.collapseAppMenu);
        }
      },
      function (errMsg) {
        nisis.ui.displayMessage(errMsg, 'error');
        // hide the spinner for the content
        $('#content').spin(false);
        $('body').spin(false);
      });
    });
  }
);
