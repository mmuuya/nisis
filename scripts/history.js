$(document).ready(function() {
        var historyDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "api/",
                    type: "POST",
                    dataType: "json",
                },
                parameterMap: function(data, type) {
                    if (type === 'read') {
                        data.action= "get_history";
                        if (!data.actionType){
                            data.actionType="ReadGrid";
                        }
                        if (nisis.historyObject.facId) data.facId = nisis.historyObject.facId;
                        if (nisis.historyObject.fieldType) data.fieldType = nisis.historyObject.fieldType;
                        if (nisis.historyObject.quicksearch) data.quicksearch = nisis.historyObject.quicksearch;
                        if (nisis.historyObject.pickedIWLs) data.pickedIWLs = nisis.historyObject.pickedIWLs;
                        if (nisis.historyObject.pickedShapeFilters) data.pickedShapeFilters = nisis.historyObject.pickedShapeFilters;
                        if (nisis.historyObject.columnfilt&&nisis.historyObject.columnfilt.length>0) data.columnfilt = {logic: "and", filters:nisis.historyObject.columnfilt};
                        if (nisis.historyObject.pickedShapeFilters) data.pickedShapeFilters = nisis.historyObject.pickedShapeFilters;
                        if (data.filter){
                            var filt = data.filter.filters;
                            for(var x = 0; x<filt.length;x++){
                                if(filt[x]["field"]==="STATUS_DATE"){
                                    var d = new Date(filt[x]["value"]);
                                    filt[x]["value"]=kendo.toString(d,"yyyyMMdd");
                                }
                            }
                            data.filter.filters=filt;
                        }
                        return data;
                    }else{
                        console.log("not read: "+type);
                    }
                }
            },
            schema: {
                model: {
                    id: "RNUM",
                    fields: {
                        RNUM: {type: "number"},
                        FACID: {type: "string"},
                        OBJECTID: {type: "number"},
                        SOURCE_TABLE: {type: "string"},
                        USERID: {type: "string"},
                        STATUS: {type: "number"},
                        STATUS_DISPLAY: {type: "string"},
                        STATUS_DATE: {type: "date"},
                        RTG_CODE: {type: "string"},
                        IM_ROLE: {type: "string"},
                        EXPLANATION: {type: "string"},
                        COL_NM : {type: "string"},
                        FIELD_DISPLAY_NM : {type: "string"},
                        TOTAL_CNT: {type: "number"}
                    }
                },
                data: "history",
                total: function (response) {
                    return response.total;
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            change: function(response) {
                console.log("HISTORY ===\n", response);
            }
        });

        var multiselectFilter = function(element, fieldName){
            element.removeAttr("data-bind");
            element.attr("id","history-filter-"+fieldName);
            element.kendoMultiSelect({
                change: function(e) {
                    var values = this.value();
                    if(values.length>0){
                        if(nisis.historyObject.columnfilt){
                            var set = false;
                            for(var x = 0; x<nisis.historyObject.columnfilt.length; x++){
                                if(nisis.historyObject.columnfilt[x]["field"]===fieldName){
                                    set=true;
                                    nisis.historyObject.columnfilt[x]={field: fieldName, operator: "eq", value: values};
                                    break;
                                }
                            }
                            if(!set){
                                nisis.historyObject.columnfilt.push({field: fieldName, operator: "eq", value: values});
                            }
                        }else{
                            nisis.historyObject.columnfilt=[{field: fieldName, operator: "eq", value: values}];
                        }
                    }else if(nisis.historyObject.columnfilt){
                        for(var x = 0; x<nisis.historyObject.columnfilt.length; x++){
                            if(nisis.historyObject.columnfilt[x]["field"]===fieldName){
                                nisis.historyObject.columnfilt.splice(x,1);
                                break;
                            }
                        }
                    }
                    historyDataSource.read();
                },
                valuePrimitive: true,
                dataTextField: fieldName,
                dataValueField: fieldName,
                placeholder:"--Select Value--",
                dataSource:{
                    transport: {
                        read: {
                            url: "api/",
                            type: "GET",
                            dataType: "json",
                        },
                        parameterMap: function(data, type) {
                            if (type === 'read') {
                                data.action = "get_history";
                                data.actionType = "get_unique_field";
                                data.filteredField=fieldName;
                                if (nisis.historyObject.facId) data.facId = nisis.historyObject.facId;
                                if (nisis.historyObject.fieldType) data.fieldType = nisis.historyObject.fieldType;
                                return data;
                            }
                        }
                    },
                    schema:{
                        data:function(response){
                            return response[fieldName];
                        }
                    }
                }
            });
        }

        var multiselectFilterFacID = function(element, fieldName){
            element.removeAttr("data-bind");
            element.attr("id","history-filter-"+fieldName);
            element.kendoMultiSelect({
                change: function(e) {
                    var values = this.value();
                    if(values.length>0){
                        if(nisis.historyObject.columnfilt){
                            var set = false;
                            for(var x = 0; x<nisis.historyObject.columnfilt.length; x++){
                                if(nisis.historyObject.columnfilt[x]["field"]===fieldName){
                                    set=true;
                                    nisis.historyObject.columnfilt[x]={field: fieldName, operator: "eq", value: values};
                                    break;
                                }
                            }
                            if(!set){
                                nisis.historyObject.columnfilt.push({field: fieldName, operator: "eq", value: values});
                            }
                        }else{
                            nisis.historyObject.columnfilt=[{field: fieldName, operator: "eq", value: values}];
                        }
                    }else if(nisis.historyObject.columnfilt){
                        for(var x = 0; x<nisis.historyObject.columnfilt.length; x++){
                            if(nisis.historyObject.columnfilt[x]["field"]===fieldName){
                                nisis.historyObject.columnfilt.splice(x,1);
                                break;
                            }
                        }
                    }
                    historyDataSource.read();
                },
                filter:"startswith",
                minLength:2,
                autobind:false,
                valuePrimitive: true,
                dataTextField: fieldName,
                dataValueField: fieldName,
                placeholder:"--Select Value--",
                dataSource:{
                    serverFiltering:true,
                    transport: {
                        read: {
                            url: "api/",
                            type: "GET",
                            dataType: "json",
                        },
                        parameterMap: function(data, type) {
                            if (type === 'read'&&data.filter&&data.filter.filters[0].value.length>1) {
                                data.action = "get_history";
                                data.actionType = "get_unique_facID";
                                data.filteredField=fieldName;
                                if (nisis.historyObject.facId) data.facId = nisis.historyObject.facId;
                                if (nisis.historyObject.fieldType) data.fieldType = nisis.historyObject.fieldType;
                                return data;
                            }
                        }
                    },
                    schema:{
                        data:function(response){
                            return response[fieldName]?response[fieldName]:[];
                        }
                    }
                }
            });
        }

        var columns = [
                {field: "FAC_ID", title:"Fac. ID", width: 100
                    //TODO: filterable needs to be updated to something that doesn't query all airports at once. Either adding a serverside filtering after several letters are typed in or perhaps a comma delimited search box
                    ,filterable:{
                        ui:function(element){
                            multiselectFilterFacID(element,"FAC_ID");
                        },
                        messages: {
                          info: ""
                        },
                        extra: false
                    }
                },
                {field: "FIELD_DISPLAY_NM", title:"Status Field", width: 125,
                    filterable:{
                        ui:function(element){
                            multiselectFilter(element,"FIELD_DISPLAY_NM");
                        },
                        messages: {
                          info: ""
                        },
                        extra: false
                    }
                },
                {field: "STATUS_DISPLAY", title:"Status Change", width: 175,
                    template:function(item){

                        var facTypes = {
                            'NISIS_AIRPORTS': 'APT',
                            'NISIS_AIRPORTS': 'APT',
                            'NISIS_AIRPORTS': 'APT',
                            'NISIS_ATM': 'ATM',
                            'NISIS_ANS': 'ANS',
                            'NISIS_ANS': 'ANS',
                            'NISIS_OSF': 'OSF',
                            'NISIS_TOF': 'TOF'
                        };

                        if(item.STATUS_DISPLAY){
                            return '<span class="history-status-display-svg ' +  facTypes[item.SOURCE_TABLE] +  ' status-' + item.STATUS_DISPLAY.replace(/ |\/|,/gi,'_') +'" ></span>'+'<span>'+item.STATUS_DISPLAY+'</span>';
                        }else{
                            return "Not Applicable";
                        }
                    },
                    filterable:{
                        ui:function(element){
                            multiselectFilter(element,"STATUS_DISPLAY");
                        },
                        messages: {
                          info: ""
                        },
                        extra: false
                    }
                },
                {field: "EXPLANATION", title:"Status Explanation/Supplemental Notes"},
                {field: "USERID", title:"User", width: 75,
                    filterable:{
                        ui:function(element){
                            multiselectFilter(element,"USERID");
                        },
                        messages: {
                          info: ""
                        },
                        extra: false
                    }
                },
                {
                    field: "STATUS_DATE",
                    title:"Date (Z)",
                    width: 150,
                    format: "{0:yyyy/MM/dd HH:mm:ss}",
                    filterable: {
                        extra: true,
                        ui:function(element){
                            element.kendoDatePicker({
                                format: "yyyy/MM/dd"
                            });
                        }
                    }
                }
            ];

        $("#historyTable").kendoGrid({
            toolbar: kendo.template($("#history-toolbar-template").html()),
            excel: {
                fileName: "FacilityStatusChanges.xlsx",
                allPages: true
            },
            dataSource: historyDataSource,
            filterable: true,
            columns: columns,
            scrollable: true,
            sortable: true,
            resizable: true,
            autoBind: false,
            //columnMenu: true,
            filterMenuInit: function(e){
                if(e.field == "FAC_ID"||e.field == "FIELD_DISPLAY_NM"||e.field == "STATUS_DISPLAY"||e.field == "USERID"){
                    var dropdown = e.container.find("select:eq(0)").data("kendoDropDownList");
                    setTimeout(function(){
                        dropdown.wrapper.hide();
                    });
                    $(e.container).width("300px");
                    e.container.on("click", "[type='reset']", function () {
                        for(var x = 0; x<nisis.historyObject.columnfilt.length; x++){
                            if(nisis.historyObject.columnfilt[x]["field"]===e.field){
                                nisis.historyObject.columnfilt.splice(x,1);
                                break;
                            }
                        }
                        historyDataSource.read();
                    });
                }
            },
            pageable: {
                refresh: true,
                buttonCount: 10
            }
        });

        $("#tbl-quicksearch-button").click(function(){
            var val=$('#searchbox').val();
            historyDataSource.page(1);
            if(val==''){
                delete nisis.historyObject.quicksearch;
            }else{
                nisis.historyObject.quicksearch=val;
            }
            historyDataSource.page(1);
            historyDataSource.read();
        });

        $("#export-button").click(function(){

            if(historyDataSource.total()<=10000){
                $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        action: "get_history",
                        actionType: "ExportToCsv",
                        facId : nisis.historyObject.facId ? nisis.historyObject.facId : undefined,
                        fieldType : nisis.historyObject.fieldType ? nisis.historyObject.fieldType : undefined,
                        quicksearch : nisis.historyObject.quicksearch ? nisis.historyObject.quicksearch : undefined,
                        pickedIWLs : nisis.historyObject.pickedIWLs ? nisis.historyObject.pickedIWLs : undefined,
                        pickedShapeFilters: nisis.historyObject.pickedShapeFilters ? nisis.historyObject.pickedShapeFilters : undefined,
                        columnfilt: (nisis.historyObject.columnfilt&&nisis.historyObject.columnfilt.length>0) ? {logic: "and", filters:nisis.historyObject.columnfilt} : undefined,
                        filter: (function(){
                            if(historyDataSource.filter()){
                                var filt = JSON.parse(JSON.stringify(historyDataSource.filter()));
                                for(var x = 0; x<filt.filters.length;x++){
                                    if(filt.filters[x]["field"]==="STATUS_DATE"){
                                        var d = new Date(filt.filters[x]["value"]);
                                        filt.filters[x]["value"]=kendo.toString(d,"yyyyMMdd");
                                    }
                                }
                            }
                            return filt?filt:undefined;
                        })()
                    },
                    success: function(data, status, req) {
                        var resp = JSON.parse(data);

                        if (resp.csvData !== ""){
                            //Replacing all the break lines with the appropiate string encoding
                            var csv = resp.csvData.replace(/\r\n/gi, "%0A");
                            var a = document.createElement('a');
                            a.href ='data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
                            a.target = '_blank';
                            a.download = 'HistoryExportedData.csv';
                            document.body.appendChild(a)
                            a.click();
                        }else{
                            nisis.ui.displayMessage("There is no data to export to CSV.", 'error');
                        }
                    },
                    error: function(req, status, err) {
                        console.error("ERROR in history.php file in ajax call trying to get the exported CSV data.");
                    },
                });
            }else{
                nisis.ui.displayMessage("CSV Exporting is limited to 10000 records. Please apply a filter.", 'error');
            }
        });

        $("#iwl-multiselect").kendoMultiSelect({
            dataTextField: "NAME",
            dataValueField: "ID",
            placeholder:'Filter by IWLs',
            dataSource:{
                transport: {
                    read: {
                        url: "api/",
                        type: "GET",
                        dataType: "json",
                    },
                    parameterMap: function(data, type) {
                        if (type === 'read') {
                            data.action= "get_iwls";
                            data.username=nisis.user.username;
                            data.usergroups=nisis.user.groups;
                            return data;
                        }
                    }
                },
                schema:{
                    data:function(response){
                        return response.results;
                    }
                }
            }
        });

        $("#shape-filter-multiselect").kendoMultiSelect({
            placeholder:'Filter by Shape Filters',
            dataTextField: "NAME",
            dataValueField: "OBJECTID",
            dataSource:{
                transport: {
                    read:function(options){
                        if(!nisis.ui.mapview.map){
                            options.error([]);
                            return;
                        }
                        var fsLayer = nisis.ui.mapview.map.getLayer('Polygon_Features');

                        var opts = {
                            layer: fsLayer,
                            returnGeom: false,
                            outFields: ['OBJECTID','NAME'],
                            where: "FS_DESIGN = '1' AND CREATED_USER = '"+nisis.user.username+"'"
                        };

                        // NISIS-2222: Kofi - commented out.
                        // var l = nisis.user.groups.length;
                        // if(l > 0){
                        //     opts.where += " OR USER_GROUP IN (";
                        //     $.each(nisis.user.groups,function(i,group){
                        //         opts.where += group;
                        //         if(i < l-1){
                        //             opts.where += ",";
                        //         }
                        //     });
                        //     opts.where += ")";
                        // }
                        //query request

                        var getFSs = nisis.ui.mapview.util.queryFeatures(opts);
                        getFSs.then(
                        function(d){
                            var resp=d.features;
                            var attributes = [];
                            for(var x=0; x<resp.length; x++){
                                attributes.push(resp[x]["attributes"]);
                            }
                            options.success(attributes);
                            return;
                        },
                        function(e){
                            nisis.ui.displayMessage(e.message,'','error');
                            options.error(d.features);
                            return;
                        });
                        return;
                    }
                },
                schema:{
                    data:function(response){
                        return response;
                    }
                }
            }
        });

        $("#iwl-refresh").click(function(){
            $("#iwl-multiselect").data("kendoMultiSelect").dataSource.read();
        });

        $("#shape-filter-refresh").click(function(){
            $("#shape-filter-multiselect").data("kendoMultiSelect").dataSource.read();
        });

        $("#iwl-multiselect").change(function(){
            var val=$("#iwl-multiselect").data("kendoMultiSelect").value();
            if(val===[]){
                delete nisis.historyObject.pickedIWLs;
            }else{
                nisis.historyObject.pickedIWLs=val;
            }
            historyDataSource.page(1);
            historyDataSource.read();
        });

        $("#shape-filter-multiselect").change(function(){
            var val = $("#shape-filter-multiselect").data("kendoMultiSelect").value();
            if(val===[]){
                delete nisis.historyObject.pickedShapeFilters;
            }else{
                nisis.historyObject.pickedShapeFilters=val;
            }
            historyDataSource.page(1);
            historyDataSource.read();
        });

});
