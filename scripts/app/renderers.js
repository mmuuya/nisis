/*
 * Renderers
*/

define(["dojo/_base/Color",
    "esri/symbols/SimpleMarkerSymbol","esri/symbols/PictureMarkerSymbol","esri/symbols/MarkerSymbol",
    "esri/symbols/SimpleLineSymbol","esri/symbols/LineSymbol","esri/symbols/CartographicLineSymbol",
    "esri/symbols/SimpleFillSymbol","esri/symbols/PictureFillSymbol","esri/symbols/FillSymbol",
    "esri/symbols/TextSymbol","esri/symbols/Font","esri/renderers/Renderer",
    "esri/renderers/SimpleRenderer","esri/renderers/UniqueValueRenderer",
    "esri/renderers/ClassBreaksRenderer"
    ,"app/symbols/symbolsPaths"
    ,"app/mapConfig"
    ,"app/iconsConfig"
    ,"app/symbols/symbolData"
    ,"app/windows/shapesManager"
    ,"app/windows/features"
    ], function(Color,
    SimpleMarkerSymbol,PictureMarkerSymbol,MarkerSymbol,
    SimpleLineSymbol,LineSymbol,CartographicLineSymbol,
    SimpleFillSymbol,PictureFillSymbol,FillSymbol,
    TextSymbol,Font,Renderer,
    SimpleRenderer,UniqueValueRenderer,
    ClassBreaksRenderer
    ,paths
    ,mapConfig
    ,icons
    ,shapesData
    ,shapesManager
    ,features
    ){

    var renderers = {},
    //default colors colors
    color = {
        white0: [255,255,255],
        white1: [255,255,255,0.25],
        white2: [255,255,255,0.5],
        white3: [255,255,255,0.75],
        gray0: [102,102,102],

        nisis_blue: [47,119,178],
        nisis_blue1: [47,119,178,0.25],
        nisis_blue2: [47,119,178,0.50],
        nisis_blue3: [47,119,178,0.75],

        nisis_white: [254,254,254],
        nisis_white1: [254,254,254,0.25],
        nisis_white2: [254,254,254,0.50],
        nisis_white3: [254,254,254,0.75],

        nisis_gray: [191,191,191],
        nisis_gray1: [191,191,191,0.25],
        nisis_gray2: [191,191,191,0.50],
        nisis_gray3: [191,191,191,0.75],

        nisis_green: [51,204,51],
        nisis_green1: [51,204,51,0.25],
        nisis_green2: [51,204,51,0.50],
        nisis_green3: [51,204,51,0.75],

        nisis_yellow: [252,253,1],
        nisis_yellow1: [252,253,1,0.25],
        nisis_yellow2: [252,253,1,0.50],
        nisis_yellow3: [252,253,1,0.75],

        nisis_red: [255,0,0],
        nisis_red1: [255,0,0,0.25],
        nisis_red2: [255,0,0,0.50],
        nisis_red3: [255,0,0,0.75]
    };

    //default symbology
    renderers.symbols = {
        point: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([0,0,0]), 2),
            new Color(color.white2)),

        blueStar: new PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS",
        "url":"http://static.arcgis.com/images/Symbols/Shapes/BlueStarLargeB.png",
        "imageData":"iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2MzI2NjFBRkQzMzkxMUUwQUU5NUVFMEYwMTY0NzUwNSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2MzI2NjFCMEQzMzkxMUUwQUU5NUVFMEYwMTY0NzUwNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjYzMjY2MUFERDMzOTExRTBBRTk1RUUwRjAxNjQ3NTA1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjYzMjY2MUFFRDMzOTExRTBBRTk1RUUwRjAxNjQ3NTA1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+bO0+/QAAC/5JREFUeF7tW3lQVdcdPiBq3UFNXLDuRaPWqnVJ2+k0GRObGacmtk3S2tRlrJnGmTR/tE2srWIYNzSGIEZF0RhtXAgiBndiRcWlKgQFlQTF4IYCRapGlO3X7zvvntfLVeQlfQ+p8ma+uXC599zzfb/v9zvLfSgRUY8yHmnyDHy9AI+y/esdUJ8C9TWgvgjWjwL1o0ANM0H1kHyqC3SNKfCQ8K+W5/8sgJ+fnyL8/f01AgICVMOGDVWjRo1UkyZNVPPmzVVgYKBq3bq1atOmjWrbtq0Gfy4rK3saAvvZwftNW2zXWx+fOcBOvkGDBloAkm/atKlq0aKFCgoK0oTbtWun2rdvrzp06KBRUlLSDZ2S/Pz8HiDZEAgA/CkG26EI/xcCsKOGuIk8ybds2VJHncRJODg4WHXq1EmDJMvLyz+kABDi7/i9FdAc+JYlhJ8RwCmCcZw5euoQnznASb5Zs2aaPC3OiJN4586dVdeuXVW3bt00iouLu5P8tMwKHmTOnDk/BhEq0wZoSifY08BJ2vyNR08/PhPARL1x48bufKflGXVGm8R79OihevbsqWGiv7+gQlTsbUnOr5Dc3NytOP99oDvQ2koJ7YKa8MAFIHETdWP5jh07qi5duqju3burkJAQ1atXL9W7d28NE/1WyaVaALW7VLtg9OjRvwOZYcC3nS4wItBtTjxwAVjoWOWN5Rl12pzRJuE+ffqovn37qn79+rmjn8Lox91xCbD+tuy5WiGpqamHcMHPAF7IVGBh1AXRpBkLrBMPXAB71JnrJuokTtL9+/d3w0S/8z5E3yaA+tTlguHDh78FQqwHnY0LDGGmmhP8m6cfn9UAU+hMrtPujDiJDxw4UA0aNEjD5P6hwkpR8Yi+XYC1t+UfcMHhw4eP4boXnC4wdYbDqx087+nHZwI4Lc+oDxgwQJMePHiwGxcvXmxdWVlZ3Hc/or/xbgFUUqncunXrNoT8g9MFJM1aY2BEqBMC2PPdSX7IkCGKYPQx6wvT0d8E8hTgY2ADasA6AA5Qa1wuSEhI+BTXPw/0tUYE+tyP5Dmz5LFOCcAhzml7Rp7Ehw4dqo/I/SBX9Muq2t8I8JFLALXL7YLXQfpHAOcGnBdoAZzk60QNMNXeFDxj/ftG35H/ygiw+rbsvlIhmzdvTrJGhD44BgHaBYw8bW8KY52YCNkdgEITyAUOprkTKioq3kbUE/DzXlZ4nfvO4kf728irD/H7zjt6RDh//nxWXl7ePtSO97BeeK2oqOhZDJVtIUQDikF8nbWCV4tgdSR1z/E5WVwpzPfwrHL5U0a5hDgLnyP3FSKvyRvsuCMvHSmTN9LLJQmOSC2qNE3rY2lp6X6sIRKvX78+C8JMKigoeMZyiVsc5+jgDQH80EiE6cnxa5WyD9PY6Scr5HV0VM/stltRZqQNWPDsVd9OntG3k19VIor4wMJKHIkVQAywAUgskedSbsnEw1/JJ+duyv6LN+XmTRfglMUgzkVVY5M2llu8sh9A2wVA/dUUQW0DWVoYMzldzTmrY2VnfjvB89asz1317eTtxJ2kl98SteymqOjropYWi1pSJOr9QlGLCkRF5SOF8uXatWuSlZW1Gf0LsQon1xNNbCJ4TYBGaLQVLBdTfAdD2laIwCHMCGHEMILwSPA8wWutIc8deRPxu4h/BeI3QPrf/yVNwguviIrME/XeZaTMZcm+lC/bt2/fgX79HBgODAA4k+QSm/3VuypeSQHLWixETyQmJs5zi8AhjBE1BI0oZow3550Fz07e2Hw5iSPidxEn6UuiIi6IWpCL1MiVU7mXZMqUKSQfBvwR+K0lwhM4sp9MBa8LwIVKb+CZmJiYlUyHYQfgBBYw5jPFMDCEze/OfGeu23Oc5I3VF//LZfOFFvF3z4t655yo+WclOC6Ho4RMnjz5KPqxBlgEhAIT2S+rf+yn1wXgeNwS6AI8CbwcGRkZSxEGpkAERtReze/1870sb8/zJddcOU670+o64l+KmndGVPgX0ik2W3JycmTixIkn8fwtwDpgIfAm+2P1i/1jP/X8AfBaCnALhttWVJcF5ylgbERExEb3WG8quClsNVV2Q76K5a+6ctxEfV62qLlZ8vj6LMnOzpYJEyacwnO3Ax8DS4C/sR9Wf9gv9o/91HuM3hKA7bAxNkpr8SHfAbh8fcWIoMd8U9CcR+a5O9dR3XW+s9DZqnuUnXyOjrqac1o6rjvNSi/jx4+/F/lXrH6wP8b6bvLeFMApAqep3NWlCL+pIoIhe0/SKHLuCg/Lu/MdFd4ZeZBvt/aUJj9u3Dgn+b/yudbz2Q/2h8GpQt7bArA9IwQfFghwL6+KCCoJYz8nL7S4qewk7R7PLeIm33Wxc9ge5NUqj8jz+eyHu+iZTpqjN4ZBZ5vVinDmzJmcV9PKqxJnjhurM+Ju4ta4HnHRNby9A9tbOa9mn5QXN52SuLi4IjyMw10cYHLeRL5G8r5yQLVO0DPFrRgOq53BIc9NxEmcxY6VHkOcznkUPDX7lKiZGZgEHZfMzEx8k0ElACuAGQALHh3nEXmfCmCtyFgcab+g0NDQp7EKxMzPGtPNsKbHdDOLs01orLFdRz38c13wGHlNPiwdjkiTjIwMwcZLItp/F3gNeBZgtTc5X+M7NF+lgHvfHp3RIhw5cmR0YQnsz1y3j+n2CQ2jra3uGtt1xDVxRH0Woj3zhIv822lAqhxIPS4jRozYifY54/sVwHcI7e6X87VVA5wvMhtgYTJ32wUUQOa7znNrWDMTGtq8SrRB2kTcEA/7zEV+xjFRoUdl6e50CQsL+wykON6/AHC7jNHn8tejj88dYKWC/40bNz6amgb7s9A5x3R3cbsPaUTcECd5FXpEJsWnSXR0dDaYTrMJEIifPX435nMBrDAEQICUn+6F/c1UlgXOFDfa3LJ48OoTsjw5Ux5bxWiDtCbrIqwx/Z9uPLkmVeLj469YKfAijv0BTng8fjHgUwFsW1MNsVODzRCs2ZnztD1znXluq+rL9mTwHUAZ1hF5Bw8eLHs/CXZfCPLTD98DECLqmJw4gbqg1FyAI8BgqwZ4/GLAZwLY9+XQyRDuzKilyHsOb6zw1lRWRWfKsr2ZuqJjCXsOW9x7QGIHXqXvmjp16hdpaWkStRM5HwnC0w65YASZeVQLgN3nGNwzCfghEGwVwQdbA2xP97tw4cLL5wsx4eGMjpMaVvmoLFlxIEtOnz4tUVFReXiRkoJ7uJDZBHAltwpYiWEuDn/PwcanS4i5xg0QBCmRknZCxo4duw3Xcsv8KYArPr1l7okCPnOAXQDsyc2O/xzFj9Ffek5Ck3Pk7Nmzsnjx4st4b0jinM0lAGuBaGA+MANgdedxPt4trl20aNFZOuLNT1AbwlkTjgrTBt8j4KuzPwPPAXzX3qIuCeCPrbItS9MLZNahy4LtbNm1a9dlvCRJtiLOPTtGfBkQDnD9zlfiLwGjrSN/5/nwYcOGbcQU+Cqt/9bW4xKelCmxsbFfWmLxzRF3fQIBj0aC2nBAAATIKCwslPT09JNjxoxZhc4xZzcA64HljDDwF4s4SXA6OwDguM4jf+d5CsHr5o8cOXILXpRc5WowOTm5EOdmWGJ9D0ePR4LaEKARUiAD9n0VHfu1FalIHLl4WQBMBfg3RptESZqbl48B3MXlkb/zPP/O63g971swatSozSkpKflWu2zfzAa58VnjpzYE4FqgPTAQ+CXwBsD1OiP5e+AXwE+A71pEzfeBzDfEeGRR43kKwet4Pe/j/WyH7bFdts/n8Hl8bo2f2hLgcfSEERwBcH+OS1ZuV/P7gLQsKzcJNgNI2L5xYXabeJ5/53W8nvfxfrbD9tgu2+dz+Lw6IwCtGGh1mtH5AcDv/JBAN4AW51sbXnfXjg3OmY8Rgtfxet7H+9kO22O7bJ/i8Hl1JgU4LaWFmc98rU0b80gCHK7YUfPuzsa32h8pBK/nfbyf7djb5XP4PI+mw7WRAuwwO8PdWEaOYAdp0a9D3KmIEYLtsD3TtvtLlZ6oWVsCGPuSsLG5RzM1D0iwnW/cfm0I4AGHB3fJNxaguhsflvM1fl3+YSFa74Bq/jOm3gEPu8Vr4vfIO+A/t1VDJpiRYaoAAAAASUVORK5CYII=",
        "contentType":"image/png","width":24,"height":24}),

        redStar: new PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS",
        "url":"http://static.arcgis.com/images/Symbols/Shapes/RedStarLargeB.png",
        "imageData":"iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo0QURGQTcwQ0QyN0QxMUUwQUU5NUVFMEYwMTY0NzUwNSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo0QURGQTcwREQyN0QxMUUwQUU5NUVFMEYwMTY0NzUwNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjRBREZBNzBBRDI3RDExRTBBRTk1RUUwRjAxNjQ3NTA1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjRBREZBNzBCRDI3RDExRTBBRTk1RUUwRjAxNjQ3NTA1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+RVLyfQAADBxJREFUeF7tWwlQVdcZviBqXQNu4BIXNICACAoSjcYFxRobI+6aRGWIiTKpTWprjLbVYBO1jrEuo2OlGqtxCxocbE1tZmpSRx2NKy4kKIgaqUqAqtG4/v2+8+55uV4FXtL3kCp35pv7uO/cc8/3/ds55z4METEeZzzW5Gn4SgEeZ/ev9IDKEKjMAZVJsLIKVFaBMmaCxiNylGToMkPgEeFfIs//WQAvLy+D8Pb2VvDx8TGqVq1qVKtWzahRo4ZRu3Ztw9fX16hXr55Rv359o0GDBgr8fOvWrZ4Q2MsK3q/7Yr/uOjzmAVbyVapUUQKQfM2aNY06deoYfn5+irC/v78REBBgNG7cWOH69eutMCi5ePFia5CsCvgA3hSD/VCE/wsBOFBNXFue5OvWrausTuIk3LRpU6NZs2YKJHn79u1VFABCrMHfTwC1gZ+YQnhpAewiaI/TZ1c9xGMeYCdfq1YtRZ4uTouTePPmzY2WLVsarVq1UiguLg4keVm3Up1mzZrVDUSoTH2gJj3BGgZ20vo7nl09PCaAtnr16tWd8U6Xp9VpbRJv3bq10aZNGwWn9Y8dFnm+q0jmQcnLy/srrncEAoF6ZkgoLygLD10AEtdW1y7fpEkTo0WLFkZgYKARFBRkBAcHGyEhIQpO68+YJDIAAkx9XXlBQkLCKyATCzxp9wItAr3NjocuABMds7x2eVqdbk5rk3BoaKgRFhZmhIeHf2/940dEBvV0CNC/M7zggOzfv383GjwPsCFDgYlRJUQdZkywdjx0AaxWZ6xrq5M4SUdERDjhtH7KZJHBvRwCPBcr8tYE5QVxcXFvgRDzQXPtBZowQ80Ofufq4bEcoBOdjnW6Oy1O4lFRUUaHDh0UnLF/IlNkWPz3HkAB+nQUObJf9uzZ8wXaDbR7gc4zLK9W8Lqrh8cEsLs8rR4ZGalIR0dHO3Hu3Ll6d+/eLZZ3p4gM7eMQgEmwXyeHAL96Va5du/YdhJxo9wKSZq7R0CJUCAGs8W4nHxMTYxC0PmZ9KZJ1VGRkP5Ehcch63SHAMxAgRqR3B5Fe7UUOfyHp6emfov0LQJhZEejnXiTPmSXPFUoAlji729PyJN6pUyd1Ruz7Oaz/tsjwvo74f6GbyM+6iPSlAFEiPSHAL5O0F/wcpJ8BODfgvEAJYCdfIXKAzvY64WnXL936PRzur+M/LlKkR4RI9zDlBVu2bPmHWRFCcfYDlBfQ8nR7nRgrxETI6gFINL5c4GCam3jnzp13YPV0fP5MpXjGvk5+tD7Ln9X6PcJFng0VeTNRNT9z5kxWfn7+58gdf8R6YUJhYWEflMoGEKIKxSB+yFrBrUmwRJJq6DjyckXF+8frRFYvF/m9mfisrm+NfU2+W1uRbiEib4wVWTRHZOVSkUP7RLKzdM/qfPPmzX9hDZFx+fLldyHMuEuXLvU2vcQpjr06uEMAL3Qy3zmSnJMiRw+JbMCaZuUSEc7skl92WJlxrsGMz6THrE/L68THzK9c37Q8iXcJFukcJPJ0G6C1SCyWDJ2waIxuKdKxuSNsXhshMvcdkaXvi+z+XO4ePihXr15VgKcsAXEuqqrrsDG9xS37AXQ7H6j/FyVC8ouODE5XJilOagY+i+yO+CZZDf7N6xw82/IeO/mumrhJOgaENekOzUSingSaOhBJNBGJMDGkjxQVFUlWVtYWjC/ITJxcT9SwiOA2Aaqh0yfgcqly5bLI+JEOMoxl1nMSZGanICSszoAiju/jo78vebQ8XZ7kaXFaWxFvIULSingJpNuBfLvGqCa9pTA3R7Zt2/YJxjUIiAMiAc4kucTmeNWuiltCwHQtJqK2GRkZf1AijBvmqOF0Z9ZzCkKiFIXgZ17jd9ZsbyVvdXOrxa3WBum7IH07vLHcAm4kxMmFr76UKVOmkHwKMAl42RShLc4cJ0PB7QJwoRIC9E5NTV2hwmH27xwZnFZlOaMgWhT9WZc5ttPxzjjX5B9kdbq5hfh34QFyLSxArkwazyohycnJ+zCO1cBiYDqQxHGZ4+M43S4A63FdoAXwNDB8wYIFG5UI701zuDPJ0bokaoXO8Ex0D3J5Hec2q9PiN4BvSRwoevM1ycnJkaSkpGN4/lZgHbAQmMzxmOPi+DhONX8A3BYC3ILhthXVZcLpAYyeP3/+JiUCZ3q0KgmSqEZpmb1El3e4O61+FcSLQ/3lmzfGSXZ2tiQmJh7Hc7cBHwFLgd9wHOZ4OC6Oj+NUe4zuEoD9sDN2StfiQ54CuHx9ySkCaz7dmklNg3/rzM4kx5JWWqIzY/26tjrIF0wcx0wvY8eOfRD5l8xxcDza9Z3k3SmAXQROU7mrSxFedIowEyKwjCmA8H2k7WUN2V6XNZO8tnwRyF+a+IoiP2bMGDv5aXyu+XyOg+Ohce4h724B2J8Wgg/zBbiXd68Ik7HJcY+lLaSd9dxS083SZnV7kv8maYQr5Pl8jsOZ9PQg9dkdZdDeZ4kinDx5MkeWLzRreRnEzUx/B5ZneaPb65gvaOsv+TOnSVpaWiEexnKXBuiY15Yvk7ynPKBET1AJkVPW0mZwpsVJXGd6VeJ0wqPrU4Bh/eTo0aP4JYORDvwZmAEw4dHjXCLvUQHMFRmTI93Pb/r06T2xCsSMEDPAB01dbRMaXdtp9cuhKHNMeCCuyIc0kvPdIyUzM1Ow8ZKB/t8HJgB9AGZ7HfNlvkPzVAg49+0xGCXC3r17E6S46H7yFuKs63R1XdtZ4lS8m+QvmuS/hgDngGO7d0t8fPzf0T9nfCMAvkPwLy3myysH2F9kVsHCZLbs22UKYC5YbG5utbYmTYtr4udBmuTPBjeSM0D2hjWSkpJyEKRY7wcC3C6j9bn8denwuAeYoeB95cqVD2V1qmPVphKcI8atya000rS4Jk7yecENJXfOTFm2bFk2mP7WIoAvPrv8bszjAphm8IEAO2XOdJN8E0VeJ7dCq4u/niS5aevkbHKicnOS1YQVaQtOT0qWzZs3/9sMgaE4RwCc8Lj8YsCjAli2pqpip0YkaahaxGjLM7PT6jqr56St5TuAW1hH5O/atevWl+tXS+7gvpID0nZQiDx8d+TIEVaC2QArQLSZA1x+MeAxAaz7chhkEHdmpGtbYXljhid5Wv7S8Ofk9Kb1KqNjCZuLLe5/gsQneJW+ferUqV8dOHBATny4SnIGxcuppxoqaDHyOgYqAbD7nIp7xgFdgKZmEny4OcDydK+zZ88O//b81yruOalhlv9Pr45yLv0jOXHihCxatCgfL1J24h4uZD4GuJL7AFiBMpeG73Ow8amEyO3WXgmgPAA4vme3jB49+m9oyy3zHgBXfGrL3BUFPOYBVgGwJ/fezc8+Vda/0TtGilYsk1OnTsmSJUvO470hiXM2lw6sBZYBc4EZALM7z3PxbnHt4sWLT9EjTi6cJ2cgBHMDwwa/I+Crs18DPwX4rr1ORRLAG1tlW29kbJLra1YItrNl+/bt5/GSZIdpce7Z0eJ/AuYAXL/zlfgwIME8829enxMbG7sJU+ALdP3Ti+dL3qpU2bhx42lTLL454q6PL+BSJSgPD/CBAJkFBQVy6NChY6NGjfoAg2PMbgDWA8tpYeBtkzhJcDobCbCu88y/eZ1CsN3c/v37b8WLkgtcDe7YsaMA12aYYrXH2eVKUB4CVEMIZMJ9X8XARpqWWoAzFy/zgKkAv6O1SZSkuXnZEOAuLs/8m9f5PduxPe+bN2DAgC07d+68aPbL/vVskBufZR7lIQDXAgFAFDAE+AXA9TotOR4YDHQH2plE9e+B9C/EeGZS43UKwXZsz/t4P/thf+yX/fM5fB6fW+ZRXgI0wkhowXiA+3NcsnK7mr8HpMsyc5NgLYCErRsXereJ1/k927E97+P97If9sV/2z+fweRVGALqirzloWqczwN/8kEArgC7OtzZsd9+ODa7pQwvBdmzP+3g/+2F/7Jf9Uxw+r8KEAKeldGHGM19r0415JgGWKw5Uv7uz8C3xI4Vge97H+9mPtV8+h89zaTpcHiHAAXMw3I2l5QgOkC76Q4jbFdFCsB/2p/t2/qjSFTXLSwDtviSs3dylmZoLJNjPj+6/PARwgcPDa/KjBSjpxkflepk/l39UiFZ6QAn/GVPpAY+6i5fF77H3gP8CPrDyH104F4wAAAAASUVORK5CYII=",
        "contentType":"image/png","width":24,"height":24}),

        greenStar: new PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS",
        "url":"http://static.arcgis.com/images/Symbols/Shapes/GreenStarLargeB.png",
        "imageData":"iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowNzc0N0NCNEQzMjExMUUwQUU5NUVFMEYwMTY0NzUwNSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowNzc0N0NCNUQzMjExMUUwQUU5NUVFMEYwMTY0NzUwNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjA3NzQ3Q0IyRDMyMTExRTBBRTk1RUUwRjAxNjQ3NTA1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjA3NzQ3Q0IzRDMyMTExRTBBRTk1RUUwRjAxNjQ3NTA1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+TMb00wAAC5FJREFUeF7tWw10jtcdv4lgPptg8/1NKGqoj61bz+qEdKtTZTpas+BoHZyz9Ww7M7V1MmelapqpHK1WzVoztbCk0YYwDSIh8RFCxfKhCeojiEPQ4+vu97vP/cfjkTSv7n0jI885v/O875P73Of+fv+ve+/zRmmt1YOMB5o8DV8jwIPs/jUeUBMCNTmgJgnWVIGaKlDJTFDdJ0dFhq40BO4T/hXy/J8FCAoKUkRwcLBBSEiIql27tqpTp46qV6+eatiwoQoNDVVNmjRRTZs2Vc2aNTPg52vXrg2GwEFu8H7pi/366wiYB7jJ16pVywhA8vXr11eNGjVSYWFhhnDz5s1VixYtVMuWLQ2uXLnSEYPSp0+f7gyStYEQIJhisB+K8H8hAAcqxMXyJN+4cWNjdRIn4datW6s2bdoYkOT169f/RgEgxAp8fwhoCHzDChEkAnhFEI+Ts68eEjAP8JJv0KCBIU8Xp8VJvF27dqpDhw6qY8eOBufPn+9E8guL5/Ok586d+ziIUJmmQH16gjsMvKTlbzz7egRMALF63bp1y+KdLk+r09ok3rlzZ9WlSxcDsf6uS+laHVQ6s3S7Liws/BjXHwU6AU1sSBgvqAz3XAASF6uLy7dq1Uq1b99ederUSYWHh6tu3bqp7t27G4j1nyp8xgjw+JFI4wUjR458AWQGAW29XiAi0Nu8uOcCMNExy4vL0+p0c1qbhHv06KF69uypevXqVWb9PZd2avWZMgIQGaWpevfu3elo8DTAhgwFJkaTECXMmGC9uOcCuK3OWBerkzhJ9+7duwxi/VGFz94S4IDS/QoGGy+IiIj4LQgxH7QTLxDCDDUv+Ddfj4DlAEl0Eut0d1qcxPv27av69etnILG/91KGVodgefEACKCyld5Ruk3v2LFjF9qN8HqB5BmWVzd43dcjYAJ4XZ5W79OnjyHdv3//Mhw7dqzJzZs3z48peq5cAboXfF9fvnz5Swj5C68XkDRzjUBEqBYCuOPdS37AgAGKoPUx65u971KmVjmweDke4HjBVh0fH78J7Z8BetqKQD8PInnOLHmuVgKwxHndnpYn8YEDB5ozYj+M1h9b9Pwt8p4QUPuVDs9/TLzg5yD9PYBzA84LjABe8tUiB0i2l4Qnrn+31qcARDq8ICEhYaOtCD1wDgOMF9DydHtJjNViIuT2ACSaUC5wMM2deOPGjT/C6vH4vIUZ/rbYL8f6IkDz/EGmIhQVFeWcOHFiK3LHX7BemHru3LmhKJXNIEQtikHczVrBr0mwIpJm5DjyvszR+5Dt3zkbqxecmgPyYyp2fcS+kFf78BlomP+o/tXxl/Srp6J1+sUt+uDlLOnanK9evboNa4jECxcuvAphXiwuLh5ivaRMHG918IcAQegkRkZy+Eo2prFp+s3iBXrOyWj9dOEIrfIecYiWB1rda/lyyIsIKgvt3diL7yyZOV31lM8n65eLpuvNxUk682y6Li0tNYCnLAZxLqrqSthYb/HLfgDdLgTqv08RVF5PZ0B2NmfOQrK8s7SzdZ9Z32v5cknvQbvdQCawE9gBpAGpFrheUlKic3JyEjC+cJs4uZ6o5xLBbwLUQacPweWWXrx+XqvccDOJMUJ4xXALw8/Shu2FvHV5Y3WxNi1NkPQuIMNDeiu+p1ikK11wMlcnJSWtx7h+DEQAfQDOJLnE5njNropfQsC6FhPRw4mJia87InRwCLkhZL2kK3J5N3kSJ7zEt+Dap8C/gU3ANqUPH83RM2bMIPnZwK+Bn1kRHsaZ42Qo+F0ALlS6A0OWLl26jOEQdTTqljuznHkFcRPn38Xy3hh3uzqsa9xciJN0MrBe6SHpT7FK6GnTpmViHB8AscAsYBLHZcfHcfpdANbjxkB74DvAmIULF66mCGOLxt0iRoK2rt8R517ydHdvnEuM09Vp8Y0OcfWJ0pGpP9IFBQV60qRJB/H8dcA/gDeB6RyPHRfHx3Ga+QPgtxDgFgy3raguE84TQFRMTMwap9ZjpkerumO7vDh3u7yQF5ff7ri3iXOxehI+r4Pltzypc3Nz9cSJEz/Dc5OAfwJvAb/nOOx4OC6Oj+M0e4z+EoD9sDN2StfiQ7oCXL6OExFGF6Lme0uY19XdVmeik+zuJb/BsTrJR6Y8yUyvJ0yYUB75cXYcHI+4fhl5fwrgFYHTVO7qUoSfigij3CJIVneTdmd4kpd4lwzvsXyEJT9+/Hgv+d/xufb5HAfHQ+PcRt7fArA/EYIPCwW4l3ebCL2xyWHKmbi4kJZ6LsRpdUl25bh91839jeUrIc/ncxxlSU8GKWd/lEFvnxWKkJeXV/CHkzOdcua1NicytLgQF6tvtjFPt7cxrz5SemrqZB0XF3cOD2O5iwMk5sXylZIPlAdU6Almpni4a8UzOCY5ljdaXIi7Mj1jXiUC8RSjrT5w4AB+yYBvSr0HRANMePQ4n8gHVAC7ImNypPuFzZo1azBWgc5cQNxdyppY2z2hsbXdWP1jJ+HR8ob8WiBB6ezsbI2Nl0T0/wYwFRgKMNtLzFf6Di1QIVC2b4/BGBEyMjJGllw9eyd594SG1qar29puSIvVQbiMfBw+A2lZ23VkZOQG9M8Z33MA3yE0/6qYr6oc4H2RWQsLk9e2lmx0ShstLzXdPaFxW5uuLhYXq69xiKPKa7Va6eXp7+rZs2fvBSnW+xEAt8tofS5/fToC7gE2FIIvXrz49z8fn+skOveExp3cvoq0izjJqw+Vnp7yS71kyZJcMH3FJUAoPvv8bizgAlgzhECA1Cl5LzilTcqauDqJWxeP2DRUr8hcpgdvGOJYm2QtYZJWq25h9IZReu3atSdtCPwE594AJzw+vxgIqACurana2KnBVLiLk+Xp9jKbc2X1DzLe4zuAa1hHnEhLS7u2fPsShEHb20i7BVCJbfX+/ftZCV4DWAH62xzg84uBgAng3pfDIMO5M2NqPMsbMzynsiSf3E6v2PVXk9GxhD2CLe5PQWI9XqUnz5w58z979uzRy7a9jQTYRmN540C8AB5CAbD7vBT3vAg8BrS2SfDe5gDX04OOHj065nhJ0e2uDxFWZb2vDx06pBctWnQCL1JScQ8XMv8CuJJbDixDmYvD3wuw8ekIwbAQARAS6fvSdFRU1Cdoyy3zJwCu+MyWuS8KBMwD3AJgT27O+uMfOdZHrY/ZN0/n5+frxYsXf4H3hiTO2Vw8sBJYAswHogFmd57n493iytjY2Hx6xJ9SXnHmAsgNDBv8joCvzn4D/BDgu/ZG1UmAYGyVrVt5ZLmOPfyGxna2Tk5O/gIvSVKsxblnR4u/A8wDuH7nK/HRwEh75ndenzdo0KA1mAKfouvPTY3Wi3fG6NWrV39uxeKbI+76hAI+VYKq8IAQCJB95swZnZWVdXDs2LHLMTjG7IfAKuBdWhh42RInCU5n+wCs6zzzO69TCLabP2zYsHV4UXKKC6KUlJQzuBZtxfo2zj5XgqoQoA5CIBvuOxkDe95aaiHOXLwsAGYC/ButTaIkzc3LbwLcxeWZ33mdf2c7tud9C4YPH56Qmpp62vbL/mU2yI3PSo+qEIBrgRZAX+BZ4CWA63VacgowCvgB8IglKr8Hkl+I8cykxusUgu3YnvfxfvbD/tgv++dz+Dw+t9KjqgT4FkZCC0YC3J/jkpXb1fw9IF2WmZsEGwAk7N64kN0mXuff2Y7teR/vZz/sj/2yfz6Hz6s2AtAVQ+2gaZ3vAvzNDwl0BOjifGvDdnfs2OCaHCIE27E97+P97If9sV/2T3H4vGoTApyW0oUZz3ytTTfmmQRYrjhQeXfn4lvhRwrB9ryP97Mfd798Dp/n03S4KkKAA+ZguBtLyxEcIF30boh7FREh2A/7k77LflTpi5pVJYC4LwmLm/s0U/OBBPv52v1XhQA+cLh3Tb62ABXdeL9cr/Tn8vcL0RoPqOA/Y2o84H538cr4PfAe8F9BNMsRpUBQNgAAAABJRU5ErkJggg==",
        "contentType":"image/png","width":24,"height":24}),

        blackStar: new PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS",
        "url":"http://static.arcgis.com/images/Symbols/Shapes/BlackStarLargeB.png",
        "imageData":"iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5NzUwQUU5Q0QyNzIxMUUwQUU5NUVFMEYwMTY0NzUwNSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo5NzUwQUU5REQyNzIxMUUwQUU5NUVFMEYwMTY0NzUwNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjU2QzIyRjIxRDI3MjExRTBBRTk1RUUwRjAxNjQ3NTA1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjU2QzIyRjIyRDI3MjExRTBBRTk1RUUwRjAxNjQ3NTA1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+8i1BegAACrpJREFUeF7tm3lsVVkdx29LAVmnLcguu0ABkR2VgAPMEAlhqaCj7NsQhiD+YQRECUsQZgRkDYZtKCKjIoMlrTKgRJR9lbAIyKIsGRwgQICAocDx+zm55+Vyx857TN8rndKbfHP77jv33PP9/pZzzu++esYY72XGS00ew5cI8DK7f4kHlIRASQ4oSYIls0DJLBBlJegVkyM/Q0cNgWLCP1+eBRYgKSnJA8nJyRYpKSle6dKlvTJlynjlypXzKlas6KWmpnrp6elelSpVvKpVq1rwd15eXjcJnBQE97u+6DdeR8I8IEi+VKlSVgDIly9f3qtUqZKXlpZmCVevXt2rUaOGV7NmTYuHDx820KDM9evXG4lkaSFFSEYM+kGEz4QADNQRd5aHfOXKla3VIQ7h2rVre3Xq1LGA5OPHj9chgIT4lT6/IlQUPucLkeQECIvgPM6dY/WQhHlAmHyFChUseVwci0O8bt26Xv369b0GDRpY3LlzpyHkFy1axMnMnTu3i4igTBWhPJ4QDIMwafcd51iPhAngrF62bNlIvOPyWB1rQ7xRo0Ze48aNLZz1Dxw4YGrVqmX27dtnLl269Addbyc0FNL9kLBeEA0vXACIO6s7lxcxr169el7Dhg29Jk2aeE2bNvWaNWtm4aw/ZMgQI+8wAwcOtF6QmZk5RmQ6CV8Ie4ETAW8L44ULQKIjyzuXx+q4OdaGcPPmzb0WLVp4LVu2jFj/0KFDRm2sAMoRZu/evebIkSP71KCPQENCgcRoE6ILMxJsGC9cgKDViXVndYhDulWrVhE4648YMcKonRWgWrVqpn///tYLevToMVmEyAd1nRc4woRaGHwX65GwHOASnYt13B2LQ7xNmzZe27ZtLVzsHz582KhNxAMQQB5k9uzZY/bv339Y7fqHvcDlGabXILge65EwAcIuj9Vbt25tSbdv3z6Cq1evpj99+vTO6NGjjfKCFYAkqIRpBejTp4958ODBfyXkxLAXQJpc4+BEKBICBOM9TL5Dhw4ewPpa9c1SnBuFhlF+MEqSRjOFUe4wmjaNEqnZvXu3yc7O/rPa9xNa+DMCfp4EeVaWnIuUAExxYbfH8hDv2LGjPSv207D+mDFjjBKjjX95jlH4GOUQo0RqtHgyvXv3dl7wPZHuLLA2YF1gBQiTLxI5wGV7l/Cc63+S9eXm1v1d/Gu/YGRdI8taL9iyZcuf/Bmhuc5pgvUCLI/bu8RYJBZCQQ9Qokllg6Nl7sgnT57MlNWz9fdfyfDEvkt+WJ/pL2h9WdcK0KtXLzsjXL58+cy1a9f+ptyxSPuFt27duvW6QqiqhCiFGOB59gpxTYL5kbQj13H27FlDtl+xYoWZP3++GTVqlE18QdcPxr4jL+sa0LNnTzN58mQzZ84cs2vXLnP8+HHXtT0/evRol/YQOXfv3v2phHnzxo0br/leEhEnPDvEQ4AkdbLQjeTUqVNMW2bJkiV2oEOHDjXdu3e3VibOHSAuL7FZH8u7xEfmx/WD5OXaBmjhYyEXt5ClVb71bNh06dLFTJw40UybNs1s27bNjuH+/fsW8pTlasemqqwLG99b4lIPwO1SpP4vEaFbt242g+PKkGJRo4WQIb4h68BnrjN42nJPmHyQeJg0xD8JGRkZ5vbt2+bMmTNb1K6JnzjZT5QLiBA3Acqo01fkcquV2U3Xrl0tGWKZ+RyCZHYEgTBnwHW+V20gMuVhedzdWfx5iSMKXnb+/HmzdevWD/T5m0IPobXASpItNuO1VZW4hIDvWiSijJycnJ8hQufOne0cjjsznyMIRBEF8DfX+C6Y7cPknZtHs7j7nlCT1c2UKVMgP0v4gTDUFyFDZ8ZJKMRdADYqzYTXVq9e/S7hMG7cOJvBsSrTGYI4UdzfbpqjXUHJs4PULGHGjx9/SONYLywTpgujGZc/PsYZdwGYjysL9YSvCG8sXrx4IyKMHTvWurPL5BANwl0viMtj/QEDBpiLFy8yrZ7S51zh18ISYRLj8cfF+BinXT8IcQsBSjCUrVCXhPOqMGzhwoXvIwIrPZfBXWKLltkZW6xQzcCcO3fOjBw58h+6Z6vwO+EXwk8Yhz8exsX4GKetMcZLAPqhMzrFtXjIFwW2r0OcCMz5LqGFz8T588a6EwfyxLy20v+P/BB/HIzHuX6EfDwFCIvAMpWqLiIMDorgyBaEdJj88OHDw+R/zHP95zMOxoNxniEfbwHozwnBw1IFannPiECRwxEoyJnFFZaPQp7nM45I0nODdOd4TIPhPvMVQXPzxZkzZ8ZFgAkTJphNmzbd0sOY7jYJLuad5aOST5QH5OsJJESWrAWxvLtXu0tz8uRJ+soW1ggzBBIeHhcT+YQK4O/ISI64X9r06dO7aRdoV4LxEICl84kTJ1ha56i/nwtvCa8LZHsX81HfoSUqBCJ1ew3GinDw4MFM7dDiQt4JSL1QO8Rt+syK7zsC7xCq+6JHJZ9QDwi9yCyljcnbO3bsiKsAa9asMbNmzfq7iDDf9xcol2F9tr8xHQn3AD8Uku/du7eBGkA83N/1MWnSJGoL5/R5WkCAVP0d87uxhAvgmyFFAuwmc0cToF+/fiYrK8v07ds3atvBgwebzZs3/8cPgW/p3EpgwRPzi4GEChAoTZVWpcbuEPMTgKy+du1aChl52kdc01uhvFWrVhm9Q8j3Hr6jKqQ+3xaYAdr7OSDmFwMJEyBYl9Mgm1CZYRMUFqBdu3Zm3bp1NqNrC/sv7Q7/ojYfqBq8ferUqf88evSoWblypdE7hY/dy0YKAbQFXq173hS+JtQWmHliOhImQODpSVeuXHlDeIYAld8NGzaY06dPm6VLl15TWWy37mEj83uBnVyW8K6muU36/iLvDhCC6S8oIu8Phw0b9kddo2T+qsCOz5bMY1GgUARQTW5Obm6uHThVoAULFpgLFy6Y5cuXf6iyGMRZzWUL7wkrhHnCDIHsznmeCqfvLVu27AIewWrSCUHY6HcEvDr7ofANgXftlYqSAMkqleWuX7/eFkpVzjbbt2//UC9JdmqQWJyaHRZfKbwjsH/nlfi3hUz/zGeuv9OpU6f3tQT+CNefPXu27XPjxo3/9sXizRFVn1QhppmgMDwgRQKcuHnzpjl27NipQYMGZWlwxOxvhd8Iq7Cw8COfOCRYzrYWmNc585nrCEG7eXpblKsXJR+xIdq5c+dNXZvhi/VlnWOeCQpDgDIKgRNy37Ea2Hd9Sy3Wmc3LAmGqwHdYG6KQpnj5eYEqLmc+c53vaUd77lug6XKL3hpd9/ulf7capPAZ9SgMAcjINYQ2wkDh+wL7dSw5ThggfF34kk/U/R7I/UKMM0mN6whBO9pzH/fTD/3RL/3zHJ4X00xQWAJU04CwYE+B+hxbVsrV/B4QlyVzQ7CCAOFg4cJVm7jO97SjPfdxP/3QH/3SP8/heUVGAFwx1R801vmqwG9+INBAwMV5a0O7j1VsdM0dTgja0Z77uJ9+6I9+6R9xeF6RCQGWpbgw8cxrbdyYMwSYrhioe3cX4JvvnwhBe+7jfvoJ9stzeF5My+HCCAEGzGCoxmI5wABx0echHlbECUE/9Of6jvyoMhY1C0sA574Qdm4e00otBhL086n7LwwBYuDw4pp8agHyu7G4XI/6c/niQrTEA/L5z5gSDyjuLh6N30vvAf8DpyBpSh6d204AAAAASUVORK5CYII=",
        "contentType":"image/png","width":24,"height":24}),

        selectedPoint: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,
            new Color([0,0,255]), 2),
            new Color([255,255,255,0.5])),

        flashPoint: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 50,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,
            new Color([255,0,0]), 3),
            new Color([255,255,255,0])),

        grayOut: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([100,100,100]), 1),
                new Color([150,150,150])),

        polyline: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
        new Color([102,102,102,0.75]), 2),

        freehand_polyline: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
        new Color([102,102,102,0.75]), 2),

        polygon: new esri.symbol.SimpleFillSymbol()
            .setColor(new Color(color.white2))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([102,102,102]), 1)),

        freehand_polygon: new esri.symbol.SimpleFillSymbol()
            .setColor(new Color(color.whiteTrans))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([102,102,102]), 1)),

        circle: new esri.symbol.SimpleFillSymbol()
            .setColor(new Color([0,255,0,0.75]))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([102,102,102]), 1)),

        cluster: new esri.symbol.SimpleFillSymbol()
            .setColor(new Color([0,255,0,0]))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,
            new Color([255,0,0]), 3)),

        text: new esri.symbol.TextSymbol(),
            //.setColor('#000')
            //.font.setSize('15px'),
            //.font.setFamily('Arial')
            //.font.setWeight('bold')
            //.font.setStyle('normal')
            //.font.setDecoration('none'),

        cut: new esri.symbol.SimpleFillSymbol()
            .setColor(new Color(color.nisis_blue2))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,
            new Color(color.nisis_red), 2)),

        extract: new esri.symbol.SimpleFillSymbol()
            .setColor(new Color(color.nisis_green2))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,
            new Color(color.nisis_red), 2)),
        nisis: {
            // NISIS-2400 - KOFI: in fixing missing pushpins, identified wrong offsets for pushpin1, pushpin3
            // NISIS-3099 - KOFI HONU - Have to do eval to instantiate brand 
            // new symbol instead of reusing same instance... 
            // !!! E.g. circles were all getting same color!!!
            point_pushpin1: "new SimpleMarkerSymbol().setPath(paths.pushpin1.path).setColor(new Color([0,0,0])).setOffset(10,10)"
            ,point_pushpin2: "new SimpleMarkerSymbol().setPath(paths.pushpin2.path).setColor(new Color([0,0,0])).setOffset(0,10)"
            ,point_pushpin3: "new SimpleMarkerSymbol().setPath(paths.pushpin3.path).setColor(new Color([0,0,0])).setOffset(0,10)"
            ,point_circle: "new SimpleMarkerSymbol().setPath(paths.circle.path).setColor(new Color([0,0,0]))"
            ,point_square: "new SimpleMarkerSymbol().setPath(paths.square.path).setColor(new Color([0,0,0]))"
            ,point_diamond: "new SimpleMarkerSymbol().setPath(paths.diamond.path).setColor(new Color([0,0,0]))"
            ,point_pentagon: "new SimpleMarkerSymbol().setPath(paths.pentagon.path).setColor(new Color([0,0,0]))"
            ,point_hexagon: "new SimpleMarkerSymbol().setPath(paths.hexagon.path).setColor(new Color([0,0,0]))"
            ,point_triangle: "new SimpleMarkerSymbol().setPath(paths.triangle.path).setColor(new Color([0,0,0]))"
            ,point_star: "new SimpleMarkerSymbol().setPath(paths.star.path).setColor(new Color([0,0,0]))"
            ,point_plus: "new SimpleMarkerSymbol().setPath(paths.plus.path).setColor(new Color([0,0,0]))"
            ,point_cross: "new SimpleMarkerSymbol().setPath(paths.cross.path).setColor(new Color([0,0,0]))"
            ,point_natA: "new SimpleMarkerSymbol().setPath(paths.natA.path).setColor(new Color([0,0,0]))"
            ,point_natB: "new SimpleMarkerSymbol().setPath(paths.natB.path).setColor(new Color([0,0,0]))"
            ,point_natC: "new SimpleMarkerSymbol().setPath(paths.natC.path).setColor(new Color([0,0,0]))"
            // NISIS-2400 - KOFI: in fixing missing pushpins, added values below for all the other point types.


            ,point_natD: "new SimpleMarkerSymbol().setPath(paths.natD.path).setColor(new Color([0,0,0]))"
            ,point_natE: "new SimpleMarkerSymbol().setPath(paths.natE.path).setColor(new Color([0,0,0]))"
            ,point_natF: "new SimpleMarkerSymbol().setPath(paths.natF.path).setColor(new Color([0,0,0]))"
            ,point_natG: "new SimpleMarkerSymbol().setPath(paths.natG.path).setColor(new Color([0,0,0]))"
            ,point_natH: "new SimpleMarkerSymbol().setPath(paths.natH.path).setColor(new Color([0,0,0]))"
            ,point_natI: "new SimpleMarkerSymbol().setPath(paths.natI.path).setColor(new Color([0,0,0]))"
            ,point_natJ: "new SimpleMarkerSymbol().setPath(paths.natJ.path).setColor(new Color([0,0,0]))"
            ,point_natK: "new SimpleMarkerSymbol().setPath(paths.natK.path).setColor(new Color([0,0,0]))"
            ,point_natL: "new SimpleMarkerSymbol().setPath(paths.natL.path).setColor(new Color([0,0,0]))"
            ,point_natM: "new SimpleMarkerSymbol().setPath(paths.natM.path).setColor(new Color([0,0,0]))"
            ,point_natN: "new SimpleMarkerSymbol().setPath(paths.natN.path).setColor(new Color([0,0,0]))"
            ,point_natO: "new SimpleMarkerSymbol().setPath(paths.natO.path).setColor(new Color([0,0,0]))"
            ,point_natP: "new SimpleMarkerSymbol().setPath(paths.natP.path).setColor(new Color([0,0,0]))"
            ,point_natQ: "new SimpleMarkerSymbol().setPath(paths.natQ.path).setColor(new Color([0,0,0]))"
            ,point_natR: "new SimpleMarkerSymbol().setPath(paths.natR.path).setColor(new Color([0,0,0]))"
            ,point_natS: "new SimpleMarkerSymbol().setPath(paths.natS.path).setColor(new Color([0,0,0]))"
            ,point_natT: "new SimpleMarkerSymbol().setPath(paths.natT.path).setColor(new Color([0,0,0]))"
            ,point_natU: "new SimpleMarkerSymbol().setPath(paths.natU.path).setColor(new Color([0,0,0]))"
            ,point_natV: "new SimpleMarkerSymbol().setPath(paths.natV.path).setColor(new Color([0,0,0]))"
            ,point_natW: "new SimpleMarkerSymbol().setPath(paths.natW.path).setColor(new Color([0,0,0]))"
            ,point_natX: "new SimpleMarkerSymbol().setPath(paths.natX.path).setColor(new Color([0,0,0]))"
            ,point_natY: "new SimpleMarkerSymbol().setPath(paths.natY.path).setColor(new Color([0,0,0]))"
            ,point_incA: "new SimpleMarkerSymbol().setPath(paths.incA.path).setColor(new Color([0,0,0]))"
            ,point_incB: "new SimpleMarkerSymbol().setPath(paths.incB.path).setColor(new Color([0,0,0]))"
            ,point_incC: "new SimpleMarkerSymbol().setPath(paths.incC.path).setColor(new Color([0,0,0]))"
            ,point_incD: "new SimpleMarkerSymbol().setPath(paths.incD.path).setColor(new Color([0,0,0]))"
            ,point_incE: "new SimpleMarkerSymbol().setPath(paths.incE.path).setColor(new Color([0,0,0]))"
            ,point_incF: "new SimpleMarkerSymbol().setPath(paths.incF.path).setColor(new Color([0,0,0]))"
            ,point_incG: "new SimpleMarkerSymbol().setPath(paths.incG.path).setColor(new Color([0,0,0]))"
            ,point_incH: "new SimpleMarkerSymbol().setPath(paths.incH.path).setColor(new Color([0,0,0]))"
            ,point_incI: "new SimpleMarkerSymbol().setPath(paths.incI.path).setColor(new Color([0,0,0]))"
            ,point_incJ: "new SimpleMarkerSymbol().setPath(paths.incJ.path).setColor(new Color([0,0,0]))"
            ,point_incK: "new SimpleMarkerSymbol().setPath(paths.incK.path).setColor(new Color([0,0,0]))"
            ,point_incL: "new SimpleMarkerSymbol().setPath(paths.incL.path).setColor(new Color([0,0,0]))"
            ,point_incM: "new SimpleMarkerSymbol().setPath(paths.incM.path).setColor(new Color([0,0,0]))"
            ,point_incN: "new SimpleMarkerSymbol().setPath(paths.incN.path).setColor(new Color([0,0,0]))"
            ,point_incO: "new SimpleMarkerSymbol().setPath(paths.incO.path).setColor(new Color([0,0,0]))"
            ,point_incP: "new SimpleMarkerSymbol().setPath(paths.incP.path).setColor(new Color([0,0,0]))"
            ,point_incQ: "new SimpleMarkerSymbol().setPath(paths.incQ.path).setColor(new Color([0,0,0]))"
            ,point_incR: "new SimpleMarkerSymbol().setPath(paths.incR.path).setColor(new Color([0,0,0]))"
            ,point_incS: "new SimpleMarkerSymbol().setPath(paths.incS.path).setColor(new Color([0,0,0]))"
            ,point_incT: "new SimpleMarkerSymbol().setPath(paths.incT.path).setColor(new Color([0,0,0]))"
            ,point_incU: "new SimpleMarkerSymbol().setPath(paths.incU.path).setColor(new Color([0,0,0]))"
            ,point_incV: "new SimpleMarkerSymbol().setPath(paths.incV.path).setColor(new Color([0,0,0]))"
            ,point_incW: "new SimpleMarkerSymbol().setPath(paths.incW.path).setColor(new Color([0,0,0]))"
            ,point_incX: "new SimpleMarkerSymbol().setPath(paths.incX.path).setColor(new Color([0,0,0]))"
            ,point_incY: "new SimpleMarkerSymbol().setPath(paths.incY.path).setColor(new Color([0,0,0]))"
            ,point_incZ: "new SimpleMarkerSymbol().setPath(paths.incZ.path).setColor(new Color([0,0,0]))"
            ,point_inca: "new SimpleMarkerSymbol().setPath(paths.inca.path).setColor(new Color([0,0,0]))"
            ,point_incb: "new SimpleMarkerSymbol().setPath(paths.incb.path).setColor(new Color([0,0,0]))"
            ,point_incc: "new SimpleMarkerSymbol().setPath(paths.incc.path).setColor(new Color([0,0,0]))"
            ,point_incd: "new SimpleMarkerSymbol().setPath(paths.incd.path).setColor(new Color([0,0,0]))"
            ,point_ince: "new SimpleMarkerSymbol().setPath(paths.ince.path).setColor(new Color([0,0,0]))"
            ,point_incf: "new SimpleMarkerSymbol().setPath(paths.incf.path).setColor(new Color([0,0,0]))"
            ,point_incg: "new SimpleMarkerSymbol().setPath(paths.incg.path).setColor(new Color([0,0,0]))"
            ,point_inch: "new SimpleMarkerSymbol().setPath(paths.inch.path).setColor(new Color([0,0,0]))"
            ,point_inci: "new SimpleMarkerSymbol().setPath(paths.inci.path).setColor(new Color([0,0,0]))"
            ,point_incj: "new SimpleMarkerSymbol().setPath(paths.incj.path).setColor(new Color([0,0,0]))"
            ,point_inck: "new SimpleMarkerSymbol().setPath(paths.inck.path).setColor(new Color([0,0,0]))"
            ,point_incl: "new SimpleMarkerSymbol().setPath(paths.incl.path).setColor(new Color([0,0,0]))"
            ,point_incm: "new SimpleMarkerSymbol().setPath(paths.incm.path).setColor(new Color([0,0,0]))"
            ,point_incn: "new SimpleMarkerSymbol().setPath(paths.incn.path).setColor(new Color([0,0,0]))"
            ,point_inco: "new SimpleMarkerSymbol().setPath(paths.inco.path).setColor(new Color([0,0,0]))"
            ,point_incp: "new SimpleMarkerSymbol().setPath(paths.incp.path).setColor(new Color([0,0,0]))"
            ,point_incq: "new SimpleMarkerSymbol().setPath(paths.incq.path).setColor(new Color([0,0,0]))"
            ,point_incr: "new SimpleMarkerSymbol().setPath(paths.incr.path).setColor(new Color([0,0,0]))"
            ,point_incs: "new SimpleMarkerSymbol().setPath(paths.incs.path).setColor(new Color([0,0,0]))"
            ,point_inct: "new SimpleMarkerSymbol().setPath(paths.inct.path).setColor(new Color([0,0,0]))"
            ,point_incu: "new SimpleMarkerSymbol().setPath(paths.incu.path).setColor(new Color([0,0,0]))"
            ,point_incv: "new SimpleMarkerSymbol().setPath(paths.incv.path).setColor(new Color([0,0,0]))"

            ,tracks: new SimpleMarkerSymbol()

            ,airports_white: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3),
                new Color(color.nisis_white))

            ,airports_gray: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_gray))

            ,airports_green: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_green))

            ,airports_yellow: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_yellow))

            ,airports_red: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_red))

            ,atm_white: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3),
                new Color(color.nisis_white))

            ,atm_gray: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_gray))

            ,atm_green: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_green))

            ,atm_yellow: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_yellow))

            ,atm_red: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_red))

            ,ans: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_green))

            ,ans_white: new SimpleMarkerSymbol()
                .setPath(paths.ans_white)
                /*.setColor(color.nisis_white)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color(color.nisis_gray), 3))*/

            ,ans_gray: new SimpleMarkerSymbol()
                .setPath(paths.ans_gray)
                /*.setColor(color.nisis_gray)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color(color.nisis_gray), 2))*/

            ,ans_green: new SimpleMarkerSymbol()
                .setPath(paths.ans_green)
                /*.setColor(color.nisis_green)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color(color.nisis_gray), 3))*/

            ,ans_yellow: new SimpleMarkerSymbol()
                .setPath(paths.ans_yellow)
                /*.setColor(color.nisis_yellow)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color(color.nisis_gray), 3))*/

            ,ans_red: new SimpleMarkerSymbol()
                .setPath(paths.ans_red)
                /*.setColor(color.nisis_red)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color(color.nisis_gray), 3))*/

            ,osf_white: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3),
                new Color(color.nisis_white))

            ,osf_gray: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_gray))

            ,osf_green: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_green))

            ,osf_yellow: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_yellow))

            ,osf_red: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_red))

            ,tof_white: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3),
                new Color(color.nisis_white))

            ,tof_gray: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_gray))

            ,tof_green: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_green))

            ,tof_yellow: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_yellow))

            ,tof_red: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_red))

            ,resp: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_blue), 3),
                new Color(color.nisis_green))

            ,resp_white: new SimpleMarkerSymbol()
                .setPath(paths.resp_white)
                /*.setColor(color.nisis_white)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3))*/

            ,resp_gray: new SimpleMarkerSymbol()
                .setPath(paths.resp_gray)
                /*.setColor(color.nisis_gray)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3))*/

            ,resp_green: new SimpleMarkerSymbol()
                .setPath(paths.resp_green)
                /*.setColor(color.nisis_green)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3))*/

            ,resp_yellow: new SimpleMarkerSymbol()
                .setPath(paths.resp_yellow)
                /*.setColor(color.nisis_yellow)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3))*/

            ,resp_red: new SimpleMarkerSymbol()
                .setPath(paths.resp_red)
                /*.setColor(color.nisis_red)
                .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color(color.nisis_gray), 3))*/
        }
    };

    //user text labels
    renderers.text = {};
    renderers.text.applyTextSymbols = function(evt) {
        if(!evt){
            return;
        }

        if(evt && (evt.error || !evt.target)){
            console.log(evt);
            return;
        }

        var layer = null;
        if(evt && evt.target){
            layer = evt.target;
        }

        if(!layer) {
            layer = nisis.ui.mapview.map.getLayer('text_features');
        }

        var graphics = null;
        var zoom = nisis.ui.mapview.map.getZoom();
        if(!zoom){
            zoom = mapConfig.zoom;
        }
        zoom += 5;
        /*var size = zoom + 'px';
        var font = new Font(size,Font.STYLE_NORMAL,Font.VARIANT_NORMAL,Font.WEIGHT_BOLD,"Arial");
        var defaultText = new TextSymbol('A', font, new Color("#000000"));*/

        if(layer){
            graphics = layer.graphics;
        }

        if(graphics && graphics.length > 0){
            // console.log('Layer has graphics:', graphics);

            $.each(graphics, function(idx, graphic){
                //var symbol = $.extend({}, defaultText);
                var size = zoom + 'px';
                var font = new Font(size,Font.STYLE_NORMAL,Font.VARIANT_NORMAL,Font.WEIGHT_BOLD,"Arial");
                var defaultText = new TextSymbol('A', font, new Color("#000000"));
                var symbol = defaultText;
                if(graphic.attributes && graphic.attributes.LABEL){
                    if ( symbol ) {
                        symbol.text = graphic.attributes.LABEL;
                        graphic.setSymbol(symbol);
                    }
                }
            });
        } else {
            //console.log('Text Layer has no graphics', graphics);
        }
    };
    renderers.text.applyTextSymbol = function(evt){
        //console.log('Text symbol: ',arguments);
        if(!evt){
            return;
        }

        var text = evt.graphic.attributes.LABEL;
        //console.log(text);
        if(text){
            var symbol = evt.graphic.symbol;
            if(symbol.type === "textsymbol"){
                symbol.setText(text);
                evt.graphic.setSymbol(symbol);
            }
        }
    };
    renderers.text.getBroaderIcons = function(size){
        var size = size ? size : 15;
        var font = new Font(size + "px",Font.STYLE_NORMAL,Font.VARIANT_NORMAL,Font.WEIGHT_BOLD,"Arial");
        //var defaultText = new TextSymbol('BABOYMA KAGNINIWA', font, new Color("#000000"));
        var defaultText = new TextSymbol('Labeling ...', font, new Color("#000000"));
        //var defaultText = new TextSymbol('{LABEL}', font, new Color("#000000"));

        renderers.text.broader = new UniqueValueRenderer(defaultText, 'SYMBOLS');
        /*renderers.text.broader.addValue({
            value: 0,
            label: 'Labels',
            symbol: defaultText
        });*/
        return renderers.text.broader;
        //return defaultText;
    };
    //user point graphics
    renderers.points = {};
    renderers.points.styles = [];
    renderers.points.getBroaderIcons = function(size){
        //console.log('Point Features Style Size: ', size);
        var size = size ? size : 15;
        var defaultPoint = new SimpleMarkerSymbol()
                .setSize(size).setColor(new Color([0,0,0]))
                .setPath(paths.circle);

        renderers.points.broader = new UniqueValueRenderer(defaultPoint, 'SYMBOL');
         for(var x=0;x<shapesData.length;x++){
            //console.log(elem.label);
            if(shapesData[x].value<2000&&shapesData[x].value!=4){
                renderers.points.broader.addValue({
                value: shapesData[x].value,
                label: shapesData[x].label,
                symbol: new SimpleMarkerSymbol()
                    .setSize(size).setColor(new Color([0,0,0]))
                    .setPath(shapesData[x].path)
            });
            }
            else if(shapesData[x].value==4){
                renderers.points.broader.addValue({
                value: shapesData[x].value,
                label: shapesData[x].label,
                symbol: new SimpleMarkerSymbol()
                    .setSize(size).setColor(new Color([0,0,0]))
                    .setPath("M16,1.466C7.973,1.466,1.466,7.973,1.466,16c0,8.027,6.507,14.534,14.534,14.534c8.027,0,14.534-6.507,14.534-14.534C30.534,7.973,24.027,1.466,16,1.466z")
            });
            }
            else{
                renderers.points.broader.addValue({
                value: shapesData[x].value,
                label: shapesData[x].label,
                symbol: new SimpleMarkerSymbol()
                    .setSize(size*1.5).setColor(new Color([0,0,0]))
                    .setPath(shapesData[x].path)
            });
            }
        }
        return renderers.points.broader;
    };

    /*
    RENDERRING amtrak, trans ln, rail ln, and pipelines to different styles using esri's
    simpleLineSymbol constructor, because the symbols in ArcMap are not being brought into
    the application.
    */

    //AMTRAK
    renderers.amtrak = {};
    renderers.amtrak.styles = [];
    renderers.amtrak.getBroaderIcons = function(){
        renderers.amtrak.broader = new UniqueValueRenderer(null, 'RR_STATUS');
        renderers.amtrak.broader.addValue({
            value: 3,
            label: 'Unknown',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([104,104,104]), 4)
        });
        renderers.amtrak.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255,0,0]), 4)
        });
        renderers.amtrak.broader.addValue({
            value: 1,
            label: 'Open with limitations',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255,170,0]), 4)
        });
        renderers.amtrak.broader.addValue({
            value: 0,
            label: 'Open',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([56,168,0]), 4)
        });
        //return the renderer
        return renderers.amtrak.broader;
    };

    //ntad_trans_ln
    renderers.ntad_trans_ln = {};
    renderers.ntad_trans_ln.styles = [];
    renderers.ntad_trans_ln.getBroaderIcons = function(){
        renderers.ntad_trans_ln.broader = new UniqueValueRenderer(null, 'TRL_STATUS');
        renderers.ntad_trans_ln.broader.addValue({
            value: 3,
            label: 'Unknown',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOTDOT, new Color([104,104,104]), 4)
        });
        renderers.ntad_trans_ln.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOTDOT, new Color([255,0,0]), 4)
        });
        renderers.ntad_trans_ln.broader.addValue({
            value: 1,
            label: 'Open with limitations',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOTDOT, new Color([255,170,0]), 4)
        });
        renderers.ntad_trans_ln.broader.addValue({
            value: 0,
            label: 'Open',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOTDOT, new Color([56,168,0]), 4)
        });
        //return the renderer
        return renderers.ntad_trans_ln.broader;
    };

    //ntad_rail_ln
    renderers.ntad_rail_ln = {};
    renderers.ntad_rail_ln.styles = [];
    renderers.ntad_rail_ln.getBroaderIcons = function(){
        renderers.ntad_rail_ln.broader = new UniqueValueRenderer(null, 'RLL_STATUS');
        renderers.ntad_rail_ln.broader.addValue({
            value: 3,
            label: 'Unknown',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_LONGDASHDOT, new Color([104,104,104]), 4)
        });
        renderers.ntad_rail_ln.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_LONGDASHDOT, new Color([255,0,0]), 4)
        });
        renderers.ntad_rail_ln.broader.addValue({
            value: 1,
            label: 'Open with limitations',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_LONGDASHDOT, new Color([255,170,0]), 4)
        });
        renderers.ntad_rail_ln.broader.addValue({
            value: 0,
            label: 'Open',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_LONGDASHDOT, new Color([56,168,0]), 4)
        });
        //return the renderer
        return renderers.ntad_rail_ln.broader;
    };

    //pipelines
    renderers.pipelines = {};
    renderers.pipelines.styles = [];
    renderers.pipelines.getBroaderIcons = function(){
        renderers.pipelines.broader = new UniqueValueRenderer(null, 'PPL_STATUS');
        renderers.pipelines.broader.addValue({
            value: 3,
            label: 'Unknown',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([104,104,104]), 4)
        });
        renderers.pipelines.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([255,0,0]), 4)
        });
        renderers.pipelines.broader.addValue({
            value: 1,
            label: 'Open with limitations',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([255,170,0]), 4)
        });
        renderers.pipelines.broader.addValue({
            value: 0,
            label: 'Open',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([56,168,0]), 4)
        });
        //return the renderer
        return renderers.pipelines.broader;
    };

    //liquid_pipelines
    renderers.liquid_pipelines = {};
    renderers.liquid_pipelines.styles = [];
    renderers.liquid_pipelines.getBroaderIcons = function(){
        renderers.liquid_pipelines.broader = new UniqueValueRenderer(null, 'LN_STATUS');
        renderers.liquid_pipelines.broader.addValue({
            value: 3,
            label: 'Unknown',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([104,104,104]), 4)
        });
        renderers.liquid_pipelines.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([255,0,0]), 4)
        });
        renderers.liquid_pipelines.broader.addValue({
            value: 1,
            label: 'Open with limitations',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([255,170,0]), 4)
        });
        renderers.liquid_pipelines.broader.addValue({
            value: 0,
            label: 'Open',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDOT, new Color([56,168,0]), 4)
        });
        //return the renderer
        return renderers.liquid_pipelines.broader;
    };

    //user polyline features
    renderers.polylines = {};
    renderers.polylines.styles = [];
    renderers.polylines.getBroaderIcons = function(){
        renderers.polylines.broader = new UniqueValueRenderer(null, 'SYMBOL');
        renderers.polylines.broader.addValue({
            value: 0,
            label: 'Solid Color Line',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2)
        });
        renderers.polylines.broader.addValue({
            value: 1,
            label: 'Dashed Line',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2)
        });
        renderers.polylines.broader.addValue({
            value: 2,
            label: 'Dotted Line',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DOT, new Color([0,0,0]), 2)
        });
        renderers.polylines.broader.addValue({
            value: 3,
            label: 'Dashed-Dotted Line',
            symbol: new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT, new Color([0,0,0]), 2)
        });
        //return the renderer
        return renderers.polylines.broader;
    };

    //user polygon features
    renderers.polygons = {};
    renderers.polygons.styles = [];
    renderers.polygons.getBroaderIcons = function(){
        renderers.polygons.broader = new UniqueValueRenderer(null, 'SYMBOL');
        renderers.polygons.broader.addValue({
            value: 0,
            label: 'Solid Color Fill Polygon',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2),
                new Color([120,120,120,0.7]))
        });
        renderers.polygons.broader.addValue({
            value: 1,
            label: 'Horizontal Line Fill Polygon',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_HORIZONTAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2))
        });
        renderers.polygons.broader.addValue({
            value: 2,
            label: 'Vertical Line Fill Polygon',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_VERTICAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2))
        });
        renderers.polygons.broader.addValue({
            value: 3,
            label: 'Forward Diagonal Line Fill Polygon',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_FORWARD_DIAGONAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2))
        });
        renderers.polygons.broader.addValue({
            value: 4,
            label: 'Backward Diagonal Line Fill Polygon',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_BACKWARD_DIAGONAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2))
        });
        renderers.polygons.broader.addValue({
            value: 5,
            label: 'Crossed Line Fill Polygon',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_DIAGONAL_CROSS,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2))
        });

        if(renderers.polygons.styles.length){
            $.each(renderers.polygons.styles,function(i, style){
                console.log('Custum Style: ', style);
                renderers.polygons.broader.addValue(style);
            });
        }
        //return the renderer
        return renderers.polygons.broader;
    };

    //tracks
    renderers.tracks = {};
    renderers.tracks.getBroaderIcons = function(size){
        var defaultSymbol = new SimpleMarkerSymbol()
            .setSize(size - 5)
            .setColor(new Color([0,0,0,0]))
            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color("#00ffff"), 4))
            .setPath(paths.tracks);

        renderers.tracks.broader = new SimpleRenderer(defaultSymbol);
        return renderers.tracks.broader;
    };
    //airports rendering
    renderers.airports = {};
    //renderers.airports.broader
    renderers.airports.getBroaderIcons = function(size){
        var defaultSymbol = renderers.symbols.nisis.airports_gray.setSize(size - 2);
        renderers.airports.broader = new UniqueValueRenderer(defaultSymbol, 'OVR_STATUS');
        renderers.airports.broader.addValue({
            value: 0,
            label: 'Unknown',
            symbol: renderers.symbols.nisis.airports_gray
                .setSize(size - 2)//new PictureMarkerSymbol(icons.apts.all['new'], size, size)
        });
        renderers.airports.broader.addValue({
            value: 1,
            label: 'Open with normal operations',
            symbol: renderers.symbols.nisis.airports_green
                .setSize(size - 2)//new PictureMarkerSymbol(icons.apts.all.unknown, size, size)
        });
        renderers.airports.broader.addValue({
            value: 2,
            label: 'Open with limitations',
            symbol: renderers.symbols.nisis.airports_yellow
                .setSize(size - 2)//new PictureMarkerSymbol(icons.apts.all.good, size, size)
        });
        renderers.airports.broader.addValue({
            value: 3,
            label: 'Closed for normal operations',
            symbol: renderers.symbols.nisis.airports_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.apts.all.warn, size, size)
        });
        renderers.airports.broader.addValue({
            value: 4,
            label: 'Closed for all operations',
            symbol: renderers.symbols.nisis.airports_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.apts.all.down, size, size)
        });
        renderers.airports.broader.addValue({
            value: 5,
            label: 'Other',
            symbol: renderers.symbols.nisis.airports_gray
                .setSize(size - 2)//new PictureMarkerSymbol(icons.apts.all.down, size, size)
        });

        return renderers.airports.broader;
    };

    //renderers.airports.details
    renderers.airports.getDetailsIcons = function(size){
        // var defaultSymbol = renderers.symbols.nisis.airports_gray.setSize(size);
        var defaultSymbol = renderers.symbols.nisis.airports_gray.setSize(size - 5);
        renderers.airports.details = new UniqueValueRenderer(defaultSymbol, 'SYMBOLOGY', 'OVR_STATUS', null, ':');
        //Heliports
        renderers.airports.details.addValue({
            value: 'HXXXXX:0',
            label: 'Unknown Heliport',
            symbol: new PictureMarkerSymbol(icons.apts.heliport.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'HXXXXX:1',
            label: 'Open Heliport with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.heliport.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'HXXXXX:2',
            label: 'Open Heliport with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.heliport.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'HXXXXX:3',
            label: 'Closed Heliport for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.heliport.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'HXXXXX:4',
            label: 'Closed Heliport for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.heliport.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'HXXXXX:5',
            label: 'Other Heliport',
            symbol: new PictureMarkerSymbol(icons.apts.heliport.unknown, size, size)
        });
        //seaplane base
        renderers.airports.details.addValue({
            value: 'SXXXXX:0',
            label: 'Unknown Seaplane Base',
            symbol: new PictureMarkerSymbol(icons.apts.seaplane.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'SXXXXX:1',
            label: 'Open Seaplane Base with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.seaplane.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'SXXXXX:2',
            label: 'Open Seaplane Base with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.seaplane.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'SXXXXX:3',
            label: 'Closed Seaplane Base for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.seaplane.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'SXXXXX:4',
            label: 'Closed Seaplane Base for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.seaplane.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'SXXXXX:5',
            label: 'Other Seaplane Base',
            symbol: new PictureMarkerSymbol(icons.apts.seaplane.unknown, size, size)
        });
        //Balloonports
        renderers.airports.details.addValue({
            value: 'BXXXXX:0',
            label: 'Unknown Balloonport',
            symbol: new PictureMarkerSymbol(icons.apts.balloonport.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'BXXXXX:1',
            label: 'Open Balloonport with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.balloonport.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'BXXXXX:2',
            label: 'Open Balloonport with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.balloonport.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'BXXXXX:3',
            label: 'Closed Balloonport for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.balloonport.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'BXXXXX:4',
            label: 'Closed Balloonport for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.balloonport.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'BXXXXX:5',
            label: 'Other Balloonports',
            symbol: new PictureMarkerSymbol(icons.apts.balloonport.unknown, size, size)
        });
        //ultralights
        renderers.airports.details.addValue({
            value: 'UXXXXX:0',
            label: 'Unknown Ultralight',
            symbol: new PictureMarkerSymbol(icons.apts.ultralight.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'UXXXXX:1',
            label: 'Open Ultralight with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.ultralight.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'UXXXXX:2',
            label: 'Open Ultralight with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.ultralight.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'UXXXXX:3',
            label: 'Closed Ultralight for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.ultralight.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'UXXXXX:4',
            label: 'Closed Ultralight for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.ultralight.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'UXXXXX:5',
            label: 'Other Ultralights',
            symbol: new PictureMarkerSymbol(icons.apts.ultralight.unknown, size, size)
        });
        //Gliderports
        renderers.airports.details.addValue({
            value: 'GXXXXX:0',
            label: 'Unknown Gliderport',
            symbol: new PictureMarkerSymbol(icons.apts.gliderport.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'GXXXXX:1',
            label: 'Open Gliderport with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.gliderport.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'GXXXXX:2',
            label: 'Open Gliderport with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.gliderport.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'GXXXXX:3',
            label: 'Closed Gliderport for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.gliderport.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'GXXXXX:4',
            label: 'Closed Gliderport for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.gliderport.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'GXXXXX:5',
            label: 'Other Gliderports',
            symbol: new PictureMarkerSymbol(icons.apts.gliderport.unknown, size, size)
        });
        //Mil & Gov Airports without ATC Tower
        renderers.airports.details.addValue({
            value: 'AMGNXX:0',
            label: 'Unknown Mil or Gov Airport without ATC Tower',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.natct.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGNXX:1',
            label: 'Open Mil or Gov Airport without ATC Tower with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.natct.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGNXX:2',
            label: 'Open Mil or Gov Airport without ATC Tower with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.natct.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGNXX:3',
            label: 'Closed Mil or Gov Airport without ATC Tower for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.natct.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGNXX:4',
            label: 'Closed Mil or Gov Airport without ATC Tower for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.natct.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGNXX:5',
            label: 'Other Mil or Gov Airport without ATC Tower',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.natct.unknown, size, size)
        });
        //Mil & Gov Airports with ATC Tower
        renderers.airports.details.addValue({
            value: 'AMGYXX:0',
            label: 'Unknown Mil or Gov Airport without ATC Tower',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.atct.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGYXX:1',
            label: 'Open Mil or Gov Airport without ATC Tower with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.atct.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGYXX:2',
            label: 'Open Mil or Gov Airport with ATC Tower with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.atct.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGYXX:3',
            label: 'Closed Mil or Gov Airport with ATC Tower for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.atct.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGYXX:4',
            label: 'Closed Mil or Gov Airport with ATC Tower for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.atct.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'AMGYXX:5',
            label: 'Other Mil or Gov Airport with ATC Tower',
            symbol: new PictureMarkerSymbol(icons.apts.airport.milgov.atct.unknown, size, size)
        });
        //non public airports
        renderers.airports.details.addValue({
            value: 'APRXXX:0',
            label: 'Unknown Non-Public Airport',
            symbol: new PictureMarkerSymbol(icons.apts.airport.nonpublic.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APRXXX:1',
            label: 'Open Non-Public Airport with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.nonpublic.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APRXXX:2',
            label: 'Open Non-Public Airport with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.nonpublic.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APRXXX:3',
            label: 'Closed Non-Public Airport for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.nonpublic.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APRXXX:4',
            label: 'Closed Non-Public Airport for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport.nonpublic.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APRXXX:5',
            label: 'Other Non-Public Airport',
            symbol: new PictureMarkerSymbol(icons.apts.airport.nonpublic.unknown, size, size)
        });
        //public airports with ATC, CSP and LH
        renderers.airports.details.addValue({
            value: 'APUYPL:0',
            label: 'Unknown PU Airport with ATC, CS and LH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_lh.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPL:1',
            label: 'Open PU Airport with ATCT, CSP and LH with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_lh.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPL:2',
            label: 'Open PU Airport with ATCT, CS and LH with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_lh.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPL:3',
            label: 'Closed PU Airport with ATCT, CSP and LH for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_lh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPL:4',
            label: 'Closed PU Airport with ATCT, CS and LH for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_lh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPL:5',
            label: 'Other PU Airport with ATCT, CS and LH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_lh.unknown, size, size)
        });
        //public airports with ATC, CSP and MH
        renderers.airports.details.addValue({
            value: 'APUYPM:0',
            label: 'Unknown PU Airport with ATCT, CSP and MH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_mh.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPM:1',
            label: 'Open PU Airport with ATCT, CSP and MH with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_mh.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPM:2',
            label: 'Open PU Airport with ATCT, CSP and MH with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_mh.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPM:3',
            label: 'Closed PU Airport with ATCT, CSP and MH for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_mh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPM:4',
            label: 'Closed PU Airport with ATCT, CSP and MH for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_mh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPM:5',
            label: 'Other PU Airport with ATCT, CSP and MH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_mh.unknown, size, size)
        });
        //public airports with ATC, CSP and SH
        renderers.airports.details.addValue({
            value: 'APUYPS:0',
            label: 'Unknown PU Airport with ATCT, CSP and SH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_sh.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPS:1',
            label: 'Open PU Airport with ATCT, CSP and SH with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_sh.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPS:2',
            label: 'Open PU Airport with ATCT, CSP and SH with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_sh.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPS:3',
            label: 'Closed PU Airport with ATCT, CSP and SH for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_sh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPS:4',
            label: 'Closed PU Airport with ATCT, CSP and SH for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_sh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPS:5',
            label: 'Other PU Airport with ATCT, CSP and SH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_sh.unknown, size, size)
        });
        //public airports with ATC, CSP and NH
        renderers.airports.details.addValue({
            value: 'APUYPN:0',
            label: 'Unknown PU Airport with ATCT, CSP and NH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPN:1',
            label: 'Open PU Airport with ATCT, CSP and NH with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPN:2',
            label: 'Open PU Airport with ATCT, CSP and NH with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPN:3',
            label: 'Closed PU Airport with ATCT, CSP and NH for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPN:4',
            label: 'Closed PU Airport with ATCT, CSP and NH for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYPN:5',
            label: 'Other PU Airport with ATCT, CSP and NH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.unknown, size, size)
        });
        //public airports with ATC, GA and NH
        renderers.airports.details.addValue({
            value: 'APUYGX:0',
            label: 'Unknown PU Airport with ATCT, GA and NH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYGX:1',
            label: 'Open PU Airport with ATCT, GA and NH with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYGX:2',
            label: 'Open PU Airport with ATCT, CSP and NH with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYGX:3',
            label: 'Closed PU Airport with ATCT, GA and NH for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYGX:4',
            label: 'Closed PU Airport with ATCT, GA and NH for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYGX:5',
            label: 'Other PU Airport with ATCT, GA and NH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csp_nh.unknown, size, size)
        });
        //public airports with ATCT, CSNP
        renderers.airports.details.addValue({
            value: 'APUYXX:0',
            label: 'Unknown PU Airport with ATCT, CSP and NH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csnp.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYXX:1',
            label: 'Open PU Airport with ATCT, CSP and NH with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csnp.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYXX:2',
            label: 'Open PU Airport with ATCT, CSP and NH with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csnp.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYXX:3',
            label: 'Closed PU Airport with ATCT, CSP and NH for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csnp.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYXX:4',
            label: 'Closed PU Airport with ATCT, CSP and NH for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csnp.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUYXX:5',
            label: 'Other PU Airport with ATCT, CSP and NH',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].atct_csnp.unknown, size, size)
        });
        //public airports without ATCT
        renderers.airports.details.addValue({
            value: 'APUNXX:0',
            label: 'Unknown PU Airport without ATCT',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].non_atct.unknown, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUNXX:1',
            label: 'Open PU Airport without ATCT with normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].non_atct.good, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUNXX:2',
            label: 'Open PU Airport without ATCT with limitations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].non_atct.warn, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUNXX:3',
            label: 'Closed PU Airport without ATCT for normal operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].non_atct.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUNXX:4',
            label: 'Closed PU Airport without ATCT for all operations',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].non_atct.down, size, size)
        });
        renderers.airports.details.addValue({
            value: 'APUNXX:5',
            label: 'Other PU Airport without ATCT',
            symbol: new PictureMarkerSymbol(icons.apts.airport['public'].non_atct.unknown, size, size)
        });
        //
        renderers.airports.details.addValue({
            value: 'XXXXXX:0',
            label: 'Unidentified Airport',
            symbol: new PictureMarkerSymbol(icons.apts.all.unidentified, size, size)
        });
        return renderers.airports.details;
    };
    //ATM Facilities styles
    renderers.atm = {};
    //renderers.atm.broader
    renderers.atm.getBroaderIcons = function(size){
        var defaultSymbol = renderers.symbols.nisis.atm_gray.setSize(size - 5);
        // var defaultSymbol = "";
        renderers.atm.broader = new UniqueValueRenderer(defaultSymbol, 'OPS_STATUS');
        renderers.atm.broader.addValue({
            value: 0,
            //label: 'New ATM Facility',
            label: 'Normal Operations',
            symbol: renderers.symbols.nisis.atm_green
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all['new'], size, size)
        });
        renderers.atm.broader.addValue({
            value: 1,
            //label: 'Unknown status',
            label: 'ATC Alertz',
            symbol: renderers.symbols.nisis.atm_yellow
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.unknown, size, size)
        });
        renderers.atm.broader.addValue({
            value: 2,
            //label: 'Normal Operations',
            label: 'ATC Limited',
            symbol: renderers.symbols.nisis.atm_yellow
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.good, size, size)
        });
        renderers.atm.broader.addValue({
            value: 3,
            //label: 'Limited Service due to staffing availability',
            label: 'ATC Zero',
            symbol: renderers.symbols.nisis.atm_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.warn, size, size)
        });
        renderers.atm.broader.addValue({
            value: 4,
            //label: 'Suspended Service due to Non-ATO Factors',
            label: 'Other',
            symbol: renderers.symbols.nisis.atm_gray
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.down, size, size)
        });
        // renderers.atm.broader.addValue({
        //     value: 5,
        //     label: 'Suspended Service due to prepardness measure',
        //     symbol: renderers.symbols.nisis.atm_red
        //         .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.down, size, size)
        // });

        // console.log("renderers obj:", renderers.atm.broader);
        // debugger;
        return renderers.atm.broader;
    };

    //renderers.atm.details
    /*
FSS-SATELLITE
FSS-CONTRACT
ARTCC
CERAP
ATCT-NON-RAD APP
ATCT-TRACON
ATCT
FCT
FSS-AUTOMATED
MIL-TWR
MIL-TWR/APP
NFCT
TRACON
MATCT
INTERNATIONAL

fss-satellite
fss-contract
artcc
cerap
ATCT-NON-RAD APP
atct-tracon
atct
fct
fss-automated
mil-twr
mil-twr/app
nfct
tracon
matct
international
*/
    renderers.atm.getDetailsIcons = function(size){
        //we can also get these from the database, index corresponds to the status
        var atmStatusStr = [
            "Normal Operations",
            "ATC Alert",
            "ATC Limited",
            "ATC 0",
            "Other Status"
            ];

        var defaultSymbol = renderers.symbols.nisis.atm_gray.setSize(size - 5);
        renderers.atm.details = new UniqueValueRenderer(defaultSymbol, 'BASIC_TP', 'OPS_STATUS', null, ':');
       
        //ARTCC Radar
        renderers.atm.details.addValue({
            value: 'ARTCC:0',
            label: 'ARTCC with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.artcc.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ARTCC:1',
            label: 'ARTCC with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.artcc.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ARTCC:2',
            label: 'ARTCC with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.artcc.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ARTCC:3',
            label: 'ARTCC with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.artcc.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ARTCC:4',
            label: 'ARTCC with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.artcc.unknown, size, size)
        });
        // renderers.atm.details.addValue({
        //     value: 'ARTCC:5',
        //     label: 'ARTCC with ' + atmStatusStr[],
        //     symbol: new PictureMarkerSymbol(icons.atm.artcc.down, size, size)
        // });
        //tracon Radar
        renderers.atm.details.addValue({
            value: 'TRACON:0',
            label: 'TRACON with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.tracon.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'TRACON:1',
            label: 'TRACON with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.tracon.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'TRACON:2',
            label: 'TRACON with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.tracon.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'TRACON:3',
            label: 'TRACON with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.tracon.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'TRACON:4',
            label: 'TRACON with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.tracon.unknown, size, size)
        });

        renderers.atm.details.addValue({
            value: 'ATCT-TRACON:0',
            label: 'ATCT-TRACON with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.atct_tracon.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-TRACON:1',
            label: 'ATCT-TRACON with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.atct_tracon.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-TRACON:2',
            label: 'ATCT-TRACON with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.atct_tracon.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-TRACON:3',
            label: 'ATCT-TRACON with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.atct_tracon.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-TRACON:4',
            label: 'ATCT-TRACON with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.atct_tracon.unknown, size, size)
        });

        // renderers.atm.details.addValue({
        //     value: 'ATCT/TRACON:5',
        //     label: 'ATCT/TRACON with ' + atmStatusStr[],
        //     symbol: new PictureMarkerSymbol(icons.atm.atct_tracon.down, size, size)
        // });
        //atct no radar - ATCT-NON-RAD APP
        //
        // Matt insists on the space between rad and app!!!!
        renderers.atm.details.addValue({
            value: 'ATCT-NON-RAD APP:0',
            label: 'ATCT-NON-RAD APP with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.atct_non_rad_app.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-NON-RAD APP:1',
            label: 'ATCT-NON-RAD APP with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.atct_non_rad_app.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-NON-RAD APP:2',
            label: 'ATCT-NON-RAD APP with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.atct_non_rad_app.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-NON-RAD APP:3',
            label: 'ATCT-NON-RAD APP with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.atct_non_rad_app.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT-NON-RAD APP:4',
            label: 'ATCT-NON-RAD APP with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.atct_non_rad_app.unknown, size, size)
        });

        //atct Radar
        renderers.atm.details.addValue({
            value: 'ATCT:0',
            label: 'ATCT with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.atct.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT:1',
            label: 'ATCT with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.atct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT:2',
            label: 'ATCT with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.atct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT:3',
            label: 'ATCT with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.atct.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'ATCT:4',
            label: 'ATCT with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.atct.unknown, size, size)
        });
        // renderers.atm.details.addValue({
        //     value: 'ATCT:5',
        //     label: 'ATCT with ' + atmStatusStr[],
        //     symbol: new PictureMarkerSymbol(icons.atm.atct.down, size, size)
        // });
        //fct Radar
        renderers.atm.details.addValue({
            value: 'FCT:0',
            label: 'FCT with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.fct.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FCT:1',
            label: 'FCT with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.fct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FCT:2',
            label: 'FCT with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.fct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FCT:3',
            label: 'FCT with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.fct.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FCT:4',
            label: 'FCT with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.fct.unknown, size, size)
        });

         //MATCT - MOBILE TOWERS
        renderers.atm.details.addValue({
            value: 'MATCT:0',
            label: 'MATCT with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.matct.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MATCT:1',
            label: 'MATCT with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.matct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MATCT:2',
            label: 'MATCT with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.matct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MATCT:3',
            label: 'MATCT with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.matct.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MATCT:4',
            label: 'MATCT with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.matct.unknown, size, size)
        });


         //INTERNATIONAL 
        renderers.atm.details.addValue({
            value: 'INTERNATIONAL:0',
            label: 'INTERNATIONAL with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.international.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'INTERNATIONAL:1',
            label: 'INTERNATIONAL with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.international.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'INTERNATIONAL:2',
            label: 'INTERNATIONAL with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.international.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'INTERNATIONAL:3',
            label: 'INTERNATIONAL with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.international.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'INTERNATIONAL:4',
            label: 'INTERNATIONAL with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.international.unknown, size, size)
        });

        // renderers.atm.details.addValue({
        //     value: 'FCT:5',
        //     label: 'FCT with ' + atmStatusStr[],
        //     symbol: new PictureMarkerSymbol(icons.atm.fct.down, size, size)
        // });
        //fss Radar
        //fss-automated
        renderers.atm.details.addValue({
            value: 'FSS-AUTOMATED:0',
            label: 'FSS-AUTOMATED with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.fss_automated.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-AUTOMATED:1',
            label: 'FSS-AUTOMATED with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.fss_automated.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-AUTOMATED:2',
            label: 'FSS-AUTOMATED with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.fss_automated.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-AUTOMATED:3',
            label: 'FSS-AUTOMATED with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.fss_automated.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-AUTOMATED:4',
            label: 'FSS-AUTOMATED with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.fss_automated.unknown, size, size)
        });
        //fss-contract
         renderers.atm.details.addValue({
            value: 'FSS-CONTRACT:0',
            label: 'FSS-CONTRACT with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.fss_contract.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-CONTRACT:1',
            label: 'FSS-CONTRACT with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.fss_contract.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-CONTRACT:2',
            label: 'FSS-CONTRACT with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.fss_contract.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-CONTRACT:3',
            label: 'FSS-CONTRACT with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.fss_contract.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-CONTRACT:4',
            label: 'FSS-CONTRACT with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.fss_contract.unknown, size, size)
        });
        //fss-satellite
         renderers.atm.details.addValue({
            value: 'FSS-SATELLITE:0',
            label: 'FSS-SATELLITE with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.fss_satellite.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-SATELLITE:1',
            label: 'FSS-SATELLITE with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.fss_satellite.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-SATELLITE:2',
            label: 'FSS-SATELLITE with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.fss_satellite.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-SATELLITE:3',
            label: 'FSS-SATELLITE with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.fss_satellite.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'FSS-SATELLITE:4',
            label: 'FSS-SATELLITE with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.fss_satellite.unknown, size, size)
        });

        //CERAP
        renderers.atm.details.addValue({
            value: 'CERAP:0',
            label: 'CERAP with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.cerap.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'CERAP:1',
            label: 'CERAP with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.cerap.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'CERAP:2',
            label: 'CERAP with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.cerap.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'CERAP:3',
            label: 'CERAP with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.cerap.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'CERAP:4',
            label: 'CERAP with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.cerap.unknown, size, size)
        });
       
        //MIL-TWR
        renderers.atm.details.addValue({
            value: 'MIL-TWR:0',
            label: 'MIL-TWR with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR:1',
            label: 'MIL-TWR with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR:2',
            label: 'MIL-TWR with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR:3',
            label: 'MIL-TWR with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR:4',
            label: 'MIL-TWR with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr.unknown, size, size)
        });
        // renderers.atm.details.addValue({
        //     value: 'MILT:5',
        //     label: 'MILT with ' + atmStatusStr[5],
        //     symbol: new PictureMarkerSymbol(icons.atm.milt.down, size, size)
        // });
        //MIL-TWR/APP
        renderers.atm.details.addValue({
            value: 'MIL-TWR/APP:0',
            label: 'MIL-TWR/APP with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr_app.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR/APP:1',
            label: 'MIL-TWR/APP with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr_app.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR/APP:2',
            label: 'MIL-TWR/APP with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr_app.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR/APP:3',
            label: 'MIL-TWR/APP with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr_app.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'MIL-TWR/APP:4',
            label: 'MIL-TWR/APP with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.mil_twr_app.unknown, size, size)
        });
        // renderers.atm.details.addValue({
        //     value: 'MIL-TWR/APP:5',
        //     label: 'MIL-TWR/APP with ' + atmStatusStr[5],
        //     symbol: new PictureMarkerSymbol(icons.atm.mil_twr_app.down, size, size)
        // });
        //NFCT
        renderers.atm.details.addValue({
            value: 'NFCT:0',
            label: 'NFCT with ' + atmStatusStr[0],
            symbol: new PictureMarkerSymbol(icons.atm.nfct.good, size, size)
        });
        renderers.atm.details.addValue({
            value: 'NFCT:1',
            label: 'NFCT with ' + atmStatusStr[1],
            symbol: new PictureMarkerSymbol(icons.atm.nfct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'NFCT:2',
            label: 'NFCT with ' + atmStatusStr[2],
            symbol: new PictureMarkerSymbol(icons.atm.nfct.warn, size, size)
        });
        renderers.atm.details.addValue({
            value: 'NFCT:3',
            label: 'NFCT with ' + atmStatusStr[3],
            symbol: new PictureMarkerSymbol(icons.atm.nfct.down, size, size)
        });
        renderers.atm.details.addValue({
            value: 'NFCT:4',
            label: 'NFCT with ' + atmStatusStr[4],
            symbol: new PictureMarkerSymbol(icons.atm.nfct.unknown, size, size)
        });
        // renderers.atm.details.addValue({
        //     value: 'NFCT:5',
        //     label: 'NFCT with ' + atmStatusStr[5],
        //     symbol: new PictureMarkerSymbol(icons.atm.nfct.down, size, size)
        // });

        return renderers.atm.details;
    };

    //ATM Facilities styles
    renderers.ans = {};
    //renderers.ans.broader
    renderers.ans.getBroaderIcons = function(size){
        //var defaultSymbol = renderers.symbols.nisis.ans_gray.setSize(size);
        var defaultSymbol = new PictureMarkerSymbol(icons.ans.all.unknown, size, size);

        renderers.ans.broader = new UniqueValueRenderer(defaultSymbol, 'ANS_STATUS');
        renderers.ans.broader.addValue({
            value: 0,
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.all.good, size, size)
        });
        renderers.ans.broader.addValue({
            value: 1,
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.all.warn, size, size)
        });
        renderers.ans.broader.addValue({
            value: 2,
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.all.warn, size, size)
        });
        renderers.ans.broader.addValue({
            value: 3,
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.all.warn, size, size)
        });
        renderers.ans.broader.addValue({
            value: 4,
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.all.down, size, size)
        });

        return renderers.ans.broader;
    };

    //renderers.atm.broader
    renderers.ans.getDetailsIcons = function(size){
        //var defaultSymbol = renderers.symbols.nisis.ans_gray.setSize(size);
        var defaultSymbol = new PictureMarkerSymbol(icons.ans.all.unknown, size, size);

        renderers.ans.details = new UniqueValueRenderer(defaultSymbol, 'BASIC_TP', 'ANS_STATUS', null, ':');
        //ANS ==> COMMUNICATION
        renderers.ans.details.addValue({
            value: 'COMMUNICATION:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.com.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.com.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.com.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.com.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.com.down, size, size)
        });
        //ANS ==> COMMUNICATION SERVICE
        renderers.ans.details.addValue({
            value: 'COMMUNICATION SERVICE:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.com.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION SERVICE:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.com.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION SERVICE:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.com.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION SERVICE:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.com.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COMMUNICATION SERVICE:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.com.down, size, size)
        });
        //NAV
        renderers.ans.details.addValue({
            value: 'NAVIGATION:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.nav.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAVIGATION:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.nav.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAVIGATION:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.nav.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAVIGATION:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.nav.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAVIGATION:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.nav.down, size, size)
        });
        //SUR
        renderers.ans.details.addValue({
            value: 'SURVEILLANCE:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.sur.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'SURVEILLANCE:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.sur.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'SURVEILLANCE:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.sur.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'SURVEILLANCE:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.sur.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'SURVEILLANCE:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.sur.down, size, size)
        });
        //ANS ==> LIGHTING
        renderers.ans.details.addValue({
            value: 'LIGHTING:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.other.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'LIGHTING:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'LIGHTING:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'LIGHTING:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'LIGHTING:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.down, size, size)
        });
        //OTHER ANS ==> POWER
        renderers.ans.details.addValue({
            value: 'POWER:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.other.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.down, size, size)
        });
        //OTHER ANS ==> AERMET
        renderers.ans.details.addValue({
            value: 'AERMET:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.other.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AERMET:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AERMET:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AERMET:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AERMET:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.down, size, size)
        });
        //auto ANS ==> automation
        renderers.ans.details.addValue({
            value: 'AUTOMATION:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.auto.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.auto.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.auto.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.auto.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.auto.down, size, size)
        });
        //auto ANS ==> automation service
        renderers.ans.details.addValue({
            value: 'AUTOMATION SERVICE:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.auto.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION SERVICE:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.auto.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION SERVICE:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.auto.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION SERVICE:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.auto.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'AUTOMATION SERVICE:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.auto.down, size, size)
        });
        //infra ANS ==> infrastructure
        renderers.ans.details.addValue({
            value: 'INFRASTRUCTURE:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.infra.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'INFRASTRUCTURE:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.infra.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'INFRASTRUCTURE:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.infra.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'INFRASTRUCTURE:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.infra.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'INFRASTRUCTURE:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.infra.down, size, size)
        });
        //POWER ANS ==> POWER
        renderers.ans.details.addValue({
            value: 'POWER:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.power.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.power.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.power.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.power.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'POWER:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.power.down, size, size)
        });
        //WEATHER ANS ==> WEATHER
        renderers.ans.details.addValue({
            value: 'WEATHER:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.weather.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.weather.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.weather.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.weather.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.weather.down, size, size)
        });
        //WEATHER ANS ==> WEATHER SERVICE
        renderers.ans.details.addValue({
            value: 'WEATHER SERVICE:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.weather.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER SERVICE:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.weather.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER SERVICE:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.weather.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER SERVICE:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.weather.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'WEATHER SERVICE:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.weather.down, size, size)
        });
        //ANS ==> OTHER
        renderers.ans.details.addValue({
            value: 'OTHER:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.other.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'OTHER:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'OTHER:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'OTHER:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'OTHER:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.down, size, size)
        });
        //ANS ==> COORDINATION
        renderers.ans.details.addValue({
            value: 'COORDINATION:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.other.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COORDINATION:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COORDINATION:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COORDINATION:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'COORDINATION:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.down, size, size)
        });
        //ANS ==> NAS COORDINATION
        renderers.ans.details.addValue({
            value: 'NAS COORDINATION:0',
            label: 'Normal Operation',
            symbol: new PictureMarkerSymbol(icons.ans.other.good, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAS COORDINATION:1',
            label: 'Line Only Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAS COORDINATION:2',
            label: 'Reduced System/Equipment',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAS COORDINATION:3',
            label: 'Reduced Facility/Service Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.warn, size, size)
        });
        renderers.ans.details.addValue({
            value: 'NAS COORDINATION:4',
            label: 'Full Interruption',
            symbol: new PictureMarkerSymbol(icons.ans.other.down, size, size)
        });

        //console.log("renderers.ans.details", renderers.ans.details);

        return renderers.ans.details;
    };

    //OSF Facilities styles
    renderers.osf = {};
    //renderers.osf.broader
    renderers.osf.getBroaderIcons = function(size){
        var defaultSymbol = renderers.symbols.nisis.osf_gray.setSize(size - 2);
        renderers.osf.broader = new UniqueValueRenderer(defaultSymbol, 'BOPS_STS');
        renderers.osf.broader.addValue({
            value: 0,
            label: 'Normal operations',
            symbol: renderers.symbols.nisis.osf_green
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all['new'], size, size)
        });
        renderers.osf.broader.addValue({
            value: 1,
            label: 'Limited/Degraded Operations',
            symbol: renderers.symbols.nisis.osf_yellow
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.unknown, size, size)
        });
        renderers.osf.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: renderers.symbols.nisis.osf_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.good, size, size)
        });
        renderers.osf.broader.addValue({
            value: 3,
            label: 'Other',
            symbol: renderers.symbols.nisis.osf_gray
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.warn, size, size)
        });
        /*renderers.osf.broader.addValue({
            value: 4,
            label: 'Closed OSF',
            symbol: renderers.symbols.nisis.osf_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.down, size, size)
        });*/

        return renderers.osf.broader;
    };

    //renderers.atm.broader
    renderers.osf.getDetailsIcons = function(size){
        var defaultSymbol = renderers.symbols.nisis.osf_gray.setSize(size);
        renderers.osf.details = new UniqueValueRenderer(defaultSymbol, 'BOPS_STS');
        //osf
        renderers.osf.details.addValue({
            value: 0,
            // label: 'OSF with Normal Operations', FB: probably to distiguish from other similar icon
            label: 'Normal Operations',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.good, size, size)
        });
        renderers.osf.details.addValue({
            value: 1,
            // label: 'OSF with Limited / Degraded Services', FB: probably to distiguish from other similar icon
            label: 'OSF with Limited / Degraded Services',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.warn, size, size)
        });
        renderers.osf.details.addValue({
            value: 2,
            // label: 'Closed OSF', FB: probably to distiguish from other similar icon
            label: 'Closed',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.down, size, size)
        });
        renderers.osf.details.addValue({
            value: 3,
            // label: 'Other OSF', FB: probably to distiguish from other similar icon
            label: 'Other',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.unknown, size, size)
        });
        /*renderers.osf.details.addValue({
            value: 4,
            label: 'Closed OSF',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.down, size, size)
        });*/
        return renderers.osf.details;
    };


    //tof Facilities styles
    renderers.tof = {};
    //renderers.tof.broader
    renderers.tof.getBroaderIcons = function(size){
        var defaultSymbol = renderers.symbols.nisis.tof_gray.setSize(size - 2);
        // NISIS-2733 KOFI HONU - remove obsolete columns
        renderers.tof.broader = new UniqueValueRenderer(defaultSymbol, 'OPS_STATUS');
        renderers.tof.broader.addValue({
            value: 0,
            label: 'Normal operations',
            symbol: renderers.symbols.nisis.tof_green
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all['new'], size, size)
        });
        renderers.tof.broader.addValue({
            value: 1,
            label: 'Limited/Degraded Operations',
            symbol: renderers.symbols.nisis.tof_yellow
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.unknown, size, size)
        });
        renderers.tof.broader.addValue({
            value: 2,
            label: 'Closed',
            symbol: renderers.symbols.nisis.tof_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.good, size, size)
        });
        renderers.tof.broader.addValue({
            value: 3,
            label: 'Other',
            symbol: renderers.symbols.nisis.tof_gray
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.warn, size, size)
        });
        /*renderers.tof.broader.addValue({
            value: 4,
            label: 'Closed OSF',
            symbol: renderers.symbols.nisis.tof_red
                .setSize(size - 2)//new PictureMarkerSymbol(icons.atm.all.down, size, size)
        });*/

        return renderers.tof.broader;
    };

    //renderers.tof.broader
    renderers.tof.getDetailsIcons = function(size){
        var defaultSymbol = renderers.symbols.nisis.tof_gray.setSize(size);
        // NISIS-2733 KOFI HONU - remove obsolete columns
        renderers.tof.details = new UniqueValueRenderer(defaultSymbol, 'OPS_STATUS');
        //osf
        renderers.tof.details.addValue({
            value: 0,
            label: 'Normal Operations',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.good, size, size)
        });
        renderers.tof.details.addValue({
            value: 1,
            label: 'Limited / Degraded Services',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.warn, size, size)
        });
        renderers.tof.details.addValue({
            value: 2,
            label: 'Closed',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.down, size, size)
        });
        renderers.tof.details.addValue({
            value: 3,
            label: 'Other',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.unknown, size, size)
        });
        /*renderers.tof.details.addValue({
            value: 4,
            label: 'Closed OSF',
            symbol: new PictureMarkerSymbol(icons.atm.faa_staffed.down, size, size)
        });*/
        return renderers.tof.details;
    };




    //NISIS-2754 - KOFI HONU
    //RESP TEAMS
    // renderers.teams = {};
    // //renderers.resp.broader
    // renderers.teams.getBroaderIcons = function(size){
    //     var defaultSymbol = new PictureMarkerSymbol(icons.teams.all.unknown, size, size);
    //     renderers.teams.broader = new UniqueValueRenderer(defaultSymbol, 'RES_STS');
    //     renderers.teams.broader.addValue({
    //         value: 0,
    //         label: 'New Response Team',
    //         symbol: /*renderers.symbols.nisis.resp_white
    //             .setSize(size - 2)//*/ new PictureMarkerSymbol(icons.teams.all['new'], size, size)
    //     });
    //     renderers.teams.broader.addValue({
    //         value: 1,
    //         label: 'Response Team with Unknown Mission',
    //         symbol: /*renderers.symbols.nisis.resp_gray
    //             .setSize(size - 2)//*/ new PictureMarkerSymbol(icons.teams.all.unknown, size, size)
    //     });
    //     renderers.teams.broader.addValue({
    //         value: 2,
    //         label: 'Response Team Fully Mission Capable',
    //         symbol: /*renderers.symbols.nisis.resp_green
    //             .setSize(size - 2)//*/ new PictureMarkerSymbol(icons.teams.all.good, size, size)
    //     });
    //     renderers.teams.broader.addValue({
    //         value: 3,
    //         label: 'Response Team Partially Mission Capable',
    //         symbol: /*renderers.symbols.nisis.resp_yellow
    //             .setSize(size - 2)//*/ new PictureMarkerSymbol(icons.teams.all.warn, size, size)
    //     });
    //     renderers.teams.broader.addValue({
    //         value: 4,
    //         label: 'Response Team Not Mission Capable',
    //         symbol: renderers.symbols.nisis.resp_red
    //             .setSize(size - 2)// new PictureMarkerSymbol(icons.teams.all.down, size, size)
    //     });

    //     return renderers.teams.broader;
    // };

    //renderers.teams.broader
    // renderers.teams.getDetailsIcons = function(size){
    //     var defaultSymbol = new PictureMarkerSymbol(icons.teams.all.unknown, size, size);
    //     renderers.teams.details = new UniqueValueRenderer(defaultSymbol, 'RES_STS');
    //     //osf
    //     renderers.teams.details.addValue({
    //         value: 0,
    //         label: 'New Response Team',
    //         symbol: new PictureMarkerSymbol(icons.teams.response['new'], size, size)
    //     });
    //     renderers.teams.details.addValue({
    //         value: 1,
    //         label: 'Response Team with Unknown Mission',
    //         symbol: new PictureMarkerSymbol(icons.teams.response.unknown, size, size)
    //     });
    //     renderers.teams.details.addValue({
    //         value: 2,
    //         label: 'Response Team Fully Mission Capable',
    //         symbol: new PictureMarkerSymbol(icons.teams.response.good, size, size)
    //     });
    //     renderers.teams.details.addValue({
    //         value: 3,
    //         label: 'Response Team Partially Mission Capable',
    //         symbol: new PictureMarkerSymbol(icons.teams.response.warn, size, size)
    //     });
    //     renderers.teams.details.addValue({
    //         value: 4,
    //         label: 'Response Team Not Mission Capable',
    //         symbol: new PictureMarkerSymbol(icons.teams.response.down, size, size)
    //     });
    //     return renderers.teams.details;
    // };

    //gqps features
    renderers.gqps = {};
    renderers.gqps.getBroaderSymbol = function(){
        renderers.gqps.broader = new UniqueValueRenderer(null, 'TYPE');
        renderers.gqps.broader.addValue({
            value: 'dash-empty',
            label: 'GQP A',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2),
                new Color([120,120,120,0]))
        });
        renderers.gqps.broader.addValue({
            value: 'dash-vline',
            label: 'GQP B',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_VERTICAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2))
        });
        renderers.gqps.broader.addValue({
            value: 'dash-hline',
            label: 'GQP C',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_HORIZONTAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2))
        });
        renderers.gqps.broader.addValue({
            value: 'dash-cline',
            label: 'GQP D',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_DIAGONAL_CROSS,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2))
        });
        renderers.gqps.broader.addValue({
            value: 'dash-fdline',
            label: 'GQP E',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_FORWARD_DIAGONAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2))
        });
        renderers.gqps.broader.addValue({
            value: 'dash-bdline',
            label: 'GQP F',
            symbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_BACKWARD_DIAGONAL,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0,0,0]), 2))
        });
        //return the renderer
        return renderers.gqps.broader;
    };
    //get icons size based on zoom level
    renderers.getFeatureRenderer = function(iconSize,layer,zoom){
        if(!renderers[layer]){
            return;
        }
        //NISIS-2222: KOFI - added 'text'
        var renderer,
            userLayers = ['points','polylines','polygons','text'];
            //userLayers = ['points','polylines','polygons'];

        // KOFI: NISIS-2245 & 2339: get out of this renderer function if it's a 'User' layer.
        // Bcos those layers don't have the 'SYMBOL' field anymore!
        if($.inArray(layer, userLayers) !== -1){
            return null;
        }

        //NISIS-3049 kofi honu
        //zoom = zoom ? zoom : mapConfig.zoom();
        zoom = zoom ? zoom : nisis.ui.mapview.map.getZoom();

        //check map zoom level
        if (zoom < 7) {
            iconSize -= 5;
            renderer = renderers[layer].getBroaderIcons(iconSize);
        }
        else if (zoom >= 7 && zoom < 11) {
            iconSize += 0;
            renderer = renderers[layer].getBroaderIcons(iconSize);
        }
        else if (zoom >= 11 && zoom < 14) {
            iconSize += 10;

            // KOFI: NISIS-2245 & 2339:
            // if($.inArray(layer, userLayers) !== -1){
            //     renderer = renderers[layer].getBroaderIcons(iconSize);
            // } else {
                if(renderers[layer].getDetailsIcons){
                    renderer = renderers[layer].getDetailsIcons(iconSize);
                }
            //}
        }
        else {
            iconSize += 15;
            // KOFI: NISIS-2245 & 2339:
            // if($.inArray(layer, userLayers) !== -1){
            //     renderer = renderers[layer].getBroaderIcons(iconSize);
            // } else {
                if(renderers[layer].getDetailsIcons){
                    renderer = renderers[layer].getDetailsIcons(iconSize);
                }
            //}
        }
        //check of the renderer is not null
        if(renderer){
            return renderer;
        }
    };
    //apply feature layers renderers
    renderers.applyFeatureLayersRenderer = function(layer){
        var id = layer.id;
        //check for common renderers
        if(layer.apprenderer && (layer.apprenderer != null || layer.apprenderer != "")){
            id = layer.apprenderer;
        }
        // NISIS-2222: KOFI - commented out unnecessary "else statement"
        //else {
            //console.log('Renderer: ', layer.id, id, layer.apprenderer);
        //}
        //
        switch (id){
            case "text":
                try {
                    layer.setRenderer(renderers.text.getBroaderIcons(15));
                } catch (err) {
                    console.log('Renderer error: ', arguments);
                }
                //renderers.text.applyTextSymbol(layer);
                break;

            case "points":
                //layer.setRenderer(renderers.points.getBroaderIcons(15));
                break;

            case "polylines":
                var defaultSymbol =  new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2);
                layer.setRenderer(new SimpleRenderer(defaultSymbol));

                //layer.setRenderer(renderers.polylines.getBroaderIcons());
                break;
            case "polygons":
                //layer.setRenderer(renderers.polygons.getBroaderIcons());
                break;
            case "tracks":
                layer.setRenderer(renderers.getFeatureRenderer(15,'tracks'));
                break;
            case "airports":
                layer.setRenderer(renderers.getFeatureRenderer(15,'airports'));
                break;
            case "atm":
                layer.setRenderer(renderers.getFeatureRenderer(15,'atm'));
                break;
            case "ans":
                layer.setRenderer(renderers.getFeatureRenderer(15,'ans'));
                break;
            case "ans1":
                layer.setRenderer(renderers.getFeatureRenderer(15,'ans'));
                break;
            case "ans2":
                layer.setRenderer(renderers.getFeatureRenderer(15,'ans'));
                break;
            case "osf":
                layer.setRenderer(renderers.getFeatureRenderer(15,'osf'));
                break;
            case "tof":
                layer.setRenderer(renderers.getFeatureRenderer(15,'tof'));
                break;
            // NISIS-2754 - KOFI HONU
            // case "teams":
            //     layer.setRenderer(renderers.getFeatureRenderer(15,'teams'));
            //     break;
            case "amtrak":
                layer.setRenderer(renderers.amtrak.getBroaderIcons());
                break;
            case "ntad_trans_ln":
                layer.setRenderer(renderers.ntad_trans_ln.getBroaderIcons());
                break;
            case "ntad_rail_ln":
                layer.setRenderer(renderers.ntad_rail_ln.getBroaderIcons());
                break;
            case "pipelines":
                layer.setRenderer(renderers.pipelines.getBroaderIcons());
                break;
            case "liquid_pipelines":
                layer.setRenderer(renderers.liquid_pipelines.getBroaderIcons());
                break;
        }
    };
    //change symbol size
    renderers.updateFeatureSymbols = function(evt) {
        console.log('Map zoom level has changed to ' + evt.target.getZoom());
        //debugger;
        var zoom = evt.level ? evt.level : evt.lod.level,
        iconSize = (zoom ? zoom : evt.target.getZoom()) + 6,
        visLayers = evt.target.getLayersVisibleAtScale();
        $.each(visLayers,function(idx,layer){
            var id = layer.id;
            //check for common renderers
            if(layer.apprenderer && (layer.apprenderer != null || layer.apprenderer != "")){
                id = layer.apprenderer;
            }
            switch (id){
                case "text":
                    //layer.setRenderer(renderers.text.getBroaderIcons(15));
                    //renderers.text.applyTextSymbol(layer);
                    break;

                case "points":
                    //layer.setRenderer(renderers.getFeatureRenderer(iconSize,'points',zoom));

                    break;
                case "polylines":
                    //layer.setRenderer(renderers.getFeatureRenderer(iconSize,'polylines',zoom));
                    break;
                case "polygons":
                    //layer.setRenderer(renderers.getFeatureRenderer(iconSize,'polygons',zoom));
                    break;
                case "Tracks":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'tracks',zoom));
                    break;
                case "airports":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'airports',zoom));
                    break;
                case "atm":
                 console.log('before layer.setRenderer ');
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'atm',zoom));
                     console.log('after layer.setRenderer ');
                    break;
                case "ans":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'ans',zoom));
                    break;
                case "ans1":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'ans',zoom));
                    break;
                case "ans2":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'ans',zoom));
                    break;
                case "osf":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'osf',zoom));
                    break;
                case "tof":
                    layer.setRenderer(renderers.getFeatureRenderer(iconSize,'tof',zoom));
                    break;
                // NISIS-2754 - KOFI HONU
                // case "teams":
                //     layer.setRenderer(renderers.getFeatureRenderer(iconSize,'teams',zoom));
                //     break;
                case "amtrak":
                    layer.setRenderer(renderers.amtrak.getBroaderIcons());
                    break;
                case "ntad_trans_ln":
                    layer.setRenderer(renderers.ntad_trans_ln.getBroaderIcons());
                    break;
                case "ntad_rail_ln":
                    layer.setRenderer(renderers.ntad_rail_ln.getBroaderIcons());
                    break;
                case "pipelines":
                    layer.setRenderer(renderers.pipelines.getBroaderIcons());
                    break;
                case "liquid_pipelines":
                    layer.setRenderer(renderers.liquid_pipelines.getBroaderIcons());
                    break;
            }
        });
        // NISIS-3049 KOFI HONU
        // Legend needs to update when feature symbols update. 
        nisis.ui.mapview.util.refreshMapLegend(); 
    };
    //store updates
    renderers.layerUpdates = {};
    //grayout layers from main menu or tableview
    renderers.deemphasizeNonIWLFeatures = function(evt,ids){
        //console.log('Emphasizing IWL features ...');
        var zoom = evt ? evt.target.getMap().getZoom() : nisis.ui.mapview.map.getZoom();

        //console.log('De-emphasize is active for ' + evt.target.id);

        $.each(evt.target.graphics,function(i,g){
            if($.inArray(g.attributes.OBJECTID, ids) == -1){
                //console.log(g.attributes.OBJECTID, zoom, g.symbol);
                if(g.symbol){
                    renderers.symbol.grayOut.setSize(g.symbol.size);
                } else {
                    renderers.symbol.grayOut.setSize(zoom);
                }

                g.setSymbol(renderers.symbol.grayOut);
            }
        });
    };
    //clear non IWL features updates
    renderers.clearIWLUpdates = function(lyrId){
        //console.log('Clearing IWL Features updates: ',lyrId);
        var layer = nisis.ui.mapview.map.getLayer(lyrId);
        if(layer.getDefinitionExpression() !== "" || !layer.getDefinitionExpression()){
            layer.setDefinitionExpression('');
        }
        if(renderers.layerUpdates[lyrId]){
            dojo.disconnect(renderers.layerUpdates[lyrId]);
        }
        //reset the layer
        layer.redraw();
    };
    //build custom feaure styles
    renderers.buildCustomStyles = function(userStyles) {
        var styles = userStyles;

        if ( !userStyles ) {
            styles = nisis.user.styles;
        }

        if ( !styles || styles.length < 1 ) {
            return;
        }

        if ( renderers['Polygon_Features'] ) {
            renderers['Polygon_Features'] = {};
            renderers['Polygon_Features'].styles = [];
        }

        if ( renderers['Polyline_Features'] ) {
            renderers['Polyline_Features'] = {};
            renderers['Polyline_Features'].styles = [];
        }

        if ( renderers['Point_Features'] ) {
            renderers['Point_Features'] = {};
            renderers['Point_Features'].styles = [];
        }

        $.each(styles, function(i, style) {
            var lyr = null;
            var oid = null;
            var desc = null;
            var symType = style.SYMBOL;
            var symbol = null;

            if ( symType === 'point' ) {
                //NISIS-2400 KOFI - fix missing pushpins: use style settings from db.
                lyr = style.PT_LAYER;
                oid = style.PT_OID;
                desc = style.PT_DESCRIPTION || 'Custom Style';
   
                var color = style.PT_COLOR || '#fff',
                    outColor = style.PT_OUT_COLOR || '#000',
                    outType = style.PT_OUT_TYPE || 'solid',
                    outWidth = style.PT_OUT_WIDTH || 2,
                    size = style.PT_SIZE || 15,
                    trans = style.PT_TRANS || 0,
                    //FB: 5/16 test
                    type = style.PT_TYPE || 'circle';

                if (lyr === 'Point_Features'){
                    // NISIS-3099 - KOFI HONU - Have to do eval to instantiate brand 
                    // new symbol instead of reusing same instance... 
                    // !!! E.g. circles were all getting same color!!!
                    symbol = eval(renderers.symbols.nisis['point_' + style.PT_TYPE]);
                    symbol.setSize(parseInt(size));

                    var c = new Color(color);
                    if(trans){
                        c = c.toRgba();
                        c[3] = 1 - trans;
                        c = new Color(c);
                    }
                    symbol.setColor(c);
                    var outline = new SimpleLineSymbol();
                    outline.setStyle(outType);
                    outline.setWidth(parseInt(outWidth));
                    outline.setColor(new Color(outColor));
                    symbol.setOutline(outline);
                    //FB: end add
                }
                else {
                    symbol = new SimpleMarkerSymbol();
                    symbol.setColor(new Color(color));
                    symbol.setSize(parseInt(size));
                    //symbol.setOpacity(1 - trans);
                    var outline = new SimpleLineSymbol();
                    outline.setStyle(outType);
                    outline.setWidth(parseInt(outWidth));
                    outline.setColor(new Color(outColor));
                    symbol.setOutline(outline);
                }

            } else if ( symType === 'polyline' ) {
                lyr = style.PL_LAYER;
                oid = style.PL_OID;
                desc = style.PL_DESCRIPTION || 'Custom Style';

                var color = style.PL_COLOR,
                    type = style.PL_TYPE,
                    width = style.PL_WIDTH,
                    trans = style.PL_TRANS;

                symbol = new SimpleLineSymbol();
                symbol.setStyle(type);
                symbol.setWidth(width);

                var c = new Color(color);
                if(trans){
                    c = c.toRgba();
                    c[3] = 1 - trans;
                    c = new Color(c);
                }
                symbol.setColor(c);

            } else if ( symType === 'polygon' ) {
                lyr = style.PG_LAYER;
                oid = style.PG_OID;
                desc = style.PG_DESCRIPTION || 'Custom Style';

                var color = style.PG_FILL_COLOR,
                    type = style.PG_FILL_TYPE,
                    trans = style.PG_FILL_TRANS,
                    outColor = style.PG_OUT_COLOR,
                    outType = style.PG_OUT_TYPE,
                    outWidth = style.PG_OUT_WIDTH;

                symbol = new SimpleFillSymbol();
                symbol.setStyle(type);
                if (type == 'solid') {
                    var c = new Color(color);
                    if(trans){
                        c = c.toRgba();
                        c[3] = 1 - trans;
                        c = new Color(c);
                    }
                    symbol.setColor(c);
                }

                var outline = new SimpleLineSymbol();
                outline.setColor(new Color(outColor));
                outline.setStyle(outType);
                outline.setWidth(outWidth);

                symbol.setOutline(outline);

            } else {
                var msg = 'App could not identify the symbol type.';
                console.log(msg);
                nisis.ui.displayMessage(msg, '', 'error');
            }

            if ( symbol ) {
                if ( !renderers[lyr] ) {
                    renderers[lyr] = {};
                    renderers[lyr].styles = [];
                }

                if ( !renderers[lyr].styles ) {
                    renderers[lyr].styles = [];
                }
                var customStyle = {
                    layer: lyr,
                    oid: parseInt(oid),
                    value: oid,
                    label: desc,
                    symbol: symbol
                };
                renderers[lyr].styles.push(customStyle);
            } 
        });
    };
    //apply custom feature styles
    renderers.applyCustomStyles = function(evt){
        if(!evt){
            return;
        }

        //get the layer
        var layer = evt.target,
            graphics = layer.graphics,
            id = layer.id;

        if (layer.group && layer.group == "tfr") {
            renderers.applyTfrDefaultSymbols(evt);
            return;
        }

        //nisis tracked objects and tracks should not have custom styles
        // NISIS-3049 KOFI HONU 
        // redundant refresh of map legend
        // if ( layer.group === 'nisisto' || id.toLowerCase() === 'tracks' ) {
        //     //BK: JIRA #1937 ==> force Legend refresh
        //     if (nisis.ui.mapview && nisis.ui.mapview.legend && nisis.ui.mapview.legend.refresh) {                
        //         nisis.ui.mapview.util.refreshMapLegend();                
        //     }
        //     return;
        // }
        if( graphics && renderers[id]) {
            if( renderers[id].styles ){
                $.each(graphics,function(i,g){
                    var oid = g.attributes.OBJECTID;
                    var found = false;

                    $.each(renderers[id].styles, function(j, style){
                        // NISIS-3099 - KOFI HONU - Added check for layer.  OID is not unique across layers.
                        if( (oid == style.oid) && (id == style.layer)) {
                            found = true;
                            g.setSymbol(style.symbol);
                            return false;
                        }
                    });

                    // if ( !found ) {
                    // }
                });
            }
        } 
        // else {
        //     //console.log('Undefined styles: ', id);
        // }

        //Arrange order functionality is for polygons only.
        if (id == 'Polygon_Features' ) {
            shapesManager.renderShapesInOrder();
            if (features && features.userOrGroupTab == "manager") {
                shapesManager.refreshShapesManagementWindow();
            }
        }
    };

    //keep default symbols for active tfr shapes
    renderers.tfrActiveShapes = [];
    renderers.tfrSelectedFeatures = [];

    renderers.getSelectedFeatures = function(evt) {
        //console.log("TFR graphics selection complete:", evt);
        var layer = evt.target,
            selGraphics = layer.getSelectedFeatures();

        //console.log("TFR Selected graphics: ", selGraphics);
        selGraphics = $.map(selGraphics, function(graphic) {
            return graphic.attributes.OBJECTID;
        });

        renderers.tfrSelectedFeatures = selGraphics;
    };

    renderers.applyTfrDefaultSymbols = function(evt) {
        //console.log("ApplyTfrDefaultSymbols", evt, renderers.tfrActiveShapes);
        var layer = evt.target,
            id = layer.id,
            len = renderers.tfrActiveShapes.length,
            graphics = null;

        if (id != 'tfr_polygon_features' || !len) return;
        //BK: Get selected features' ids
        var selGraphics = renderers.tfrSelectedFeatures;

        var dSymbol = new SimpleFillSymbol(),
            style = SimpleFillSymbol.STYLE_SOLID,
            outSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([211,211,211,1]), 2),
            fillColor = new Color([255,255,255,0]),
            rend = null;

        dSymbol.setStyle(style);
        dSymbol.setOutline(outSymbol);
        dSymbol.setColor(fillColor);

        rend = new UniqueValueRenderer(dSymbol, 'OBJECTID');

        $.each(layer.graphics, function(i, graphic) {
            var oid = graphic.attributes.OBJECTID,
                tfrShape = graphic.attributes.TFR_SHAPE,
                color = null,
                symbol = null;
            //if the current shape is part of the tfr
            if ( $.inArray(oid, renderers.tfrActiveShapes) != -1) {

                if (tfrShape == 'base') {
                    color = new Color([0,112,255,1]);
                }
                else if(tfrShape == 'add') {
                    color = new Color([76,230,0,1]);
                }
                else if(tfrShape == 'subtract') {
                    color = new Color([255,0,0,1]);
                }
                else {
                    color = new Color([0,0,0,1]);
                }

                //BK: Apply default to non selected shapes only
                symbol = new SimpleFillSymbol(
                    SimpleFillSymbol.STYLE_SOLID,
                    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, color, 2),
                    fillColor);
                //Add renderer value base on tfr shapes
                rend.addValue({
                    value: oid,
                    symbol: symbol
                });
            }
        });
        //reset tfr layer renderer each time the refreshes
        layer.setRenderer(rend);
    };
    //returned object
    return renderers;
});
