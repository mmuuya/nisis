/*
 * Map layers
 */

define(['/nisis/settings/settings.js'], function(settings){
    return [{
        type: 'WFS',
        id: 'tfr_point_features',
        name: 'Point Features',
        group: 'tfr',
        add: true,
        visible: false,
        tooltip: 'NAME',
        renderer: 'tfr_points',
        url: nisis.url.arcgis.services + 'tfr/tfr_points/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'tfr_line_features',
        name: 'Line Features',
        group: 'tfr',
        add: true,
        visible: false,
        tooltip: 'NAME',
        renderer: 'tfr_polylines',
        url: nisis.url.arcgis.services + 'tfr/tfr_lines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'tfr_polygon_features',
        name: 'Polygon Features',
        group: 'tfr',
        add: true,
        visible: false,
        tooltip: 'NAME',
        renderer: 'tfr_polygons',
        url: nisis.url.arcgis.services + 'tfr/tfr_polygons/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'text_features',
        name: 'Text Labels',
        group: 'user',
        add: true,
        visible: true,
        tooltip: 'NAME',
        renderer: 'text',
        url: nisis.url.arcgis.services + 'users/text_features/FeatureServer/0',
        secured: true,
        attributes: {
            //NISIS-2222: KOFI - commented out
            // fields: ['OBJECTID','LABEL','USER_GROUP','SYMBOLS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
            fields: ['OBJECTID','LABEL','SYMBOLS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'Point_Features',
        name: 'Point Features',
        group: 'user',
        add: true,
        visible: true,
        tooltip: 'NAME',
        renderer: 'points',
        url: nisis.url.arcgis.services + 'users/point_features/FeatureServer/0',
        secured: true,
        attributes: {
            //NISIS-2222: KOFI - commented out
            // fields: ['OBJECTID','NAME','DESCRIPTION','REF_NUMBER','SUP_NOTES','MIN_A_AGL','MIN_A_MSL','MAX_A_AGL','MAX_A_MSL','VERTICES','USER_GROUP','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
            fields: ['OBJECTID','NAME','DESCRIPTION','REF_NUMBER','SUP_NOTES','MIN_A_AGL','MIN_A_MSL','MAX_A_AGL','MAX_A_MSL','VERTICES','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'Line_Features',
        name: 'Line Features',
        group: 'user',
        add: true,
        visible: true,
        tooltip: 'NAME',
        renderer: 'polylines',
        url: nisis.url.arcgis.services + 'users/line_features/FeatureServer/0',
        secured: true,
        attributes: {
            //NISIS-2222: KOFI - commented out
            // fields: ['NAME','REF_NUMBER','DESCRIPTION','SUP_NOTES','MIN_A_AGL','MIN_A_MSL','MAX_A_AGL','MAX_A_MSL','VERTICES','USER_GROUP','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
            fields: ['NAME','REF_NUMBER','DESCRIPTION','SUP_NOTES','MIN_A_AGL','MIN_A_MSL','MAX_A_AGL','MAX_A_MSL','VERTICES','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'Polygon_Features',
        name: 'Polygon Features',
        group: 'user',
        add: true,
        visible: true,
        tooltip: 'NAME',
        renderer: 'polygons',
        url: nisis.url.arcgis.services + 'users/polygon_features/FeatureServer/0',
        secured: true,
        attributes: {
            //NISIS-2222: KOFI - commented out
            // fields: ['NAME','REF_NUMBER', 'DESCRIPTION','SUP_NOTES','FS_DESIGN','IWA_DESIGN','MIN_A_AGL','MIN_A_MSL','MAX_A_AGL','MAX_A_MSL','VERTICES','USER_GROUP','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
            fields: ['NAME','REF_NUMBER', 'DESCRIPTION','SUP_NOTES','FS_DESIGN','IWA_DESIGN','MIN_A_AGL','MIN_A_MSL','MAX_A_AGL','MAX_A_MSL','VERTICES','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'graphics',
        id: 'User_Graphics',
        name: 'User Graphics',
        group: 'user',
        add: true,
        visible: true,
        tooltip: 'NAME',
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'Tracks',
        name: 'ADAPT Aircraft Tracks',
        group: 'faa',
        add: true,
        visible: false,
        renderer: 'tracks',
        url: nisis.url.arcgis.services + 'faa/aads_tracks/MapServer/0',
        secured: true,
        attributes: {
            fields: ['CALLSIGN','ALTITUDE','DEPT','DEST','SPEED','BEACON']
        }
    },{
        type: 'WFS',
        id: 'airports',
        name: 'Airports (All)',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'FAA_ID',
        // NISIS-2744 - KOFI HONU - cleanup readiness CRT_RL
        readiness: 'OVR_STATUS',
        query: null,
        renderer: 'airports',
        maplayers: false,
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_airports/featureserver/0',
        secured: true,
        
        // NISIS-2733 KOFI HONU - remove obsolete columns
        attributes: {
            fields: ['OBJECTID','FAA_ID','ICAO_ID','LG_NAME','BASIC_USE','ARP_TYPE', 'ARP_CLASS','OVR_STATUS','SYMBOLOGY','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'airports_pu',
        name: 'Airports (Public)',
        group: 'nisisto',
        add: true,
        visible: true,
        tooltip: 'FAA_ID',
        // NISIS-3088 - KOFI HONU - 2017-07-13 - Modified where clause for type.
        query: "BASIC_USE = 'PU'",
        renderer: 'airports',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_airports/featureserver/0',
        secured: true,
        // NISIS-2733 KOFI HONU - remove obsolete columns
        attributes: {
            fields: ['OBJECTID','FAA_ID','ICAO_ID','LG_NAME','BASIC_USE','ARP_TYPE', 'ARP_CLASS','OVR_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'airports_pr',
        name: 'Airports (Private)',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'FAA_ID',
        // NISIS-3088 - KOFI HONU - 2017-07-13 - Modified where clause for type.
        query: "BASIC_USE = 'PR'",
        renderer: 'airports',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_airports/featureserver/0',
        secured: true,
        // NISIS-2733 KOFI HONU - remove obsolete columns
        attributes: {
            fields: ['OBJECTID','FAA_ID','ICAO_ID','LG_NAME','BASIC_USE','ARP_TYPE','ARP_CLASS','OVR_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'airports_others',
        name: 'Airports (Others)',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'FAA_ID',
        //query: "ARP_TYPE <> 'AIRPORT'",
        // NISIS-3088 - KOFI HONU - 2017-07-13 - Modified where clause for type.
        query: "BASIC_USE = 'OT'",
        renderer: 'airports',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_airports/featureserver/0',
        secured: true,
        // NISIS-2733 KOFI HONU - remove obsolete columns
        attributes: {
            fields: ['OBJECTID','FAA_ID','ICAO_ID','LG_NAME','BASIC_USE','ARP_TYPE','ARP_CLASS','OVR_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        // NISIS-3088 - KOFI HONU - 2017-07-13 - Added Military Airport layer
        type: 'WFS',
        id: 'airports_military',
        name: 'Airports (Military)',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'FAA_ID',
        query: "BASIC_USE = 'MG'",
        renderer: 'airports',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_airports/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','FAA_ID','ICAO_ID','LG_NAME','BASIC_USE','ARP_TYPE','ARP_CLASS','OVR_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'atm',
        name: 'ATM Facilities',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'ATM_ID',
        readiness: 'CRT_RL',
        renderer: 'atm',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_atm/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ATM_ID','ARP_ID','LG_NAME','BASIC_TP','OPS_STATUS','CRT_RL','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ans',
        name: 'ANS Systems (All)',
        group: 'nisisto',
        add: true,
        visible: false,
        query: null,
        renderer: 'ans',
        maplayers: false,
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','NAPRS_REPORTABLE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ans1',
        name: 'ANS Systems (NAPRS)',
        group: 'nisisto',
        add: true,
        visible: false,
        query: "NAPRS_REPORTABLE = 'Y'",
        renderer: 'ans',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','NAPRS_REPORTABLE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ans2',
        name: 'ANS Systems (Non NAPRS)',
        group: 'nisisto',
        add: true,
        visible: false,
        query: "NAPRS_REPORTABLE = 'N'",
        renderer: 'ans',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','NAPRS_REPORTABLE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'navans',
        name: 'Navigation Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'NAV'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'comans',
        name: 'Communication Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'COM'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'surans',
        name: 'Survillance Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'SUR'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'autoans',
        name: 'Automation Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'AUTOMATION'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'aermetans',
        name: 'Whether Reporting Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'AERMET'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'powerans',
        name: 'Power Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'POWER'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'lightans',
        name: 'Lighting Systems',
        group: 'ans',
        add: false,
        visible: true,
        tooltip: 'ANS_ID',
        query: "BASIC_TP = 'LIGHTING'",
        url: nisis.url.arcgis.services + 'nisisv2/nisis_ans/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','ANS_ID','BASIC_TP','BASIC_CAT','DESCRIPT','ANS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'osf',
        name: 'Other Staffed Facilities',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'OSF_ID',
        readiness: 'CRT_RL',
        renderer: 'osf',
        url: nisis.url.arcgis.services + 'nisisv2/nisis_osf/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','OSF_ID','LG_NAME','CRT_RL','BOPS_STS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },
/*
    {
        type: 'WFS',
        id: 'teams',
        name: 'Response Teams',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'RESP_ID',
        renderer: 'teams',
        url: nisis.url.arcgis.services + 'nisisv2/nisis_resp/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','RES_ID','LG_NAME','RES_STS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },*/
    {
        type: 'WFS',
        id: 'all_objects',
        name: 'NISIS Tracked Objects',
        group: 'nisisto',
        add: false,
        visible: false,
        maplayers: false,
        tableview: true,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        secured: true,
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'tof',
        name: 'Tech Ops Facilities',
        group: 'nisisto',
        add: true,
        visible: false,
        tooltip: 'TOF_ID',
        readiness: 'CRT_RL',
        renderer: 'tof',
        tableview: true,
        url: nisis.url.arcgis.services + 'nisisv2/nisis_tof/featureserver/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','TOF_ID','ATOW_CODE','LONG_NAME','BASIC_TYPE','OPS_STATUS','CRT_RL','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'artcc_sa',
        name: 'ARTCC Airspace',
        group: 'faa',
        add: true,
        visible: false,
        label: 'LABEL',
        url: nisis.url.arcgis.services + 'faa/faa_artcc/MapServer/0',
        attributes: {
            fields: ['ARTCC_NM','ICAO','ID','LABEL']
        }
    },{
        type: 'WFS',
        id: 'faa_tracons',
        name: 'TRACON Airspace',
        group: 'faa',
        add: true,
        visible: false,
        label: 'NAME',
        url: nisis.url.arcgis.services + 'faa/faa_tracons/MapServer/0',
        attributes: {
            fields: ['OBJECTID','TRACON','ARTCC','FACILITY','SECTOR','NAME']
        }
    },{
        type: 'WFS',
        id: 'Airspace_Class_B',
        name: 'Airspace Class B',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_features/MapServer/0',
        attributes: {
            fields: ['OBJECTID','AIRSPACE','NAME','LOWALT','HIGHALT']
        }
    },{
        type: 'WFS',
        id: 'Airspace_Class_C',
        name: 'Airspace Class C',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_features/MapServer/1',
        attributes: {
            fields: ['OBJECTID','AIRSPACE','NAME','LOWALT','HIGHALT']
        }
    },{
        type: 'WFS',
        id: 'Airspace_Class_D',
        name: 'Airspace Class D',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_features/MapServer/2',
        attributes: {
            fields: ['OBJECTID','AIRSPACE','NAME','LOWALT','HIGHALT']
        }
    },{
        type: 'WMS',
        id: 'faa_sect_charts',
        name: 'FAA Sectional Charts',
        group: 'faa',
        add: true,
        visible: false,
        url: nisisProtocol + '://maps7.arcgisonline.com/arcgis/rest/services/FAA_Sectional_Charts/MapServer'
    },{
        type: 'WMS',
        id: 'dafif_atsh_us',
        name: 'High Altitude Air Routes',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_us/MapServer/',
        vLayers: [3],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_atsl_us',
        name: 'Low Altitude Air Routes',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_us/MapServer/',
        vLayers: [4],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_wpth_us',
        name: 'High Altitude Waypoints',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_us/MapServer/',
        vLayers: [1],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_wptl_us',
        name: 'Low Altitude Waypoints',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_us/MapServer/',
        vLayers: [2],
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'Airport_Boundaries',
        name: 'Airport Boundaries',
        group: 'faa',
        add: true,
        visible: false,
        template: true,
        url: nisis.url.arcgis.services + 'nisis/nisis_features/MapServer/4',
        secured: true,
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_runways',
        name: 'Airport Runways',
        group: 'faa',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_us/MapServer/',
        vLayers: [5],
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'Aircraft_Taxiways',
        name: 'Aircraft Taxiways',
        group: 'faa',
        add: true,
        visible: false,
        template: true,
        url: nisis.url.arcgis.services + 'nisis/nisis_features/MapServer/3',
        secured: true,
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'faa_regions',
        name: 'FAA Regions',
        group: 'faa',
        add: true,
        visible: false,
        label: 'NAME',
        url: nisis.url.arcgis.services + 'faa/faa_regions/MapServer/0',
        attributes: {
            fields: ['OBJECTID','FAA_REG','NAME']
        }
    },{
        type: 'WFS',
        id: 'ato_sa',
        name: 'ATO Service Areas',
        group: 'faa',
        add: true,
        visible: false,
        label: 'NAME',
        url: nisis.url.arcgis.services + 'faa/faa_ato/MapServer/0',
        attributes: {
            fields: ['OBJECTID','ATO_REG','NAME']
        }
    },{
        type: 'WMS',
        id: 'ortca',
        name: 'Off-Route Terrain Clearance Altitude',
        group: 'faa',
        add: true,
        visible: false,
        url: nisis.url.arcgis.services + 'faa/faa_ortca/MapServer/'
    },{
        type: 'WFS',
        id: 'States_Emergency_Operations_Centers',
        name: 'States Emergency Operations Centers',
        group: 'fema',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'fema/fema_features/MapServer/0',
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'FEMA_Recovery_Offices',
        name: 'FEMA Recovery Offices',
        group: 'fema',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'fema/fema_features/MapServer/1',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'FEMA_Regions_Headquaters',
        name: 'FEMA Regions Headquaters',
        group: 'fema',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'fema/fema_features/MapServer/2',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'FEMA_Regions',
        name: 'FEMA Regions',
        group: 'fema',
        add: true,
        visible: false,
        label: 'NAME',
        url: nisis.url.arcgis.services + 'fema/fema_regions/MapServer/0',
        layers: [0],
        mode: 'MODE_SELECTION',
        attributes: {
            fields: ['FEMA_REG','NAME'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'DOT_NRP_Regions',
        name: 'USDOT NRP Regions',
        group: 'ntad',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'ntad/dot_nrp_regions/MapServer',
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WFS',
        id: 'ntad_amtrak',
        name: 'Amtrak Lines',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'FRAARCID',
        renderer: 'amtrak',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_amtrak/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','FRAARCID','RROWNER1','RROWNER2','RROWNER3','POC_NAME','POC_PHONE','STATEAB','COUNTY','SWITCHES','RR_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ntad_amtrk_sta',
        name: 'Amtrak Stations',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'STNCODE',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_amtrk_sta/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','STNNAME','STNCODE','POC_PHONE','ADDRESS1','ADDRESS2','CITY','STATE','ZIP','COUNTY','STN_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ntad_ports',
        name: 'Ports',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'PORT',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_ports/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','PORT','PORT_NAME','LATITUDE1','LONGITUDE1','OWNER','POC_PHONE','STPOSTAL','TOTAL','MAX_DRAFT','NUM_BERTHS','PRT_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ntad_nhpn_ishwy',
        name: 'Interstate Highways',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'RECID',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_nhpn_ishwy/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','RECID','POC_NAME','POC_PHONE','SIGN1','SIGN2','SIGN3','LNAME','HWY_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ntad_nhpn_ushwy',
        name: 'US Highways',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'RECID',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_nhpn_ushwy/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','RECID','POC_NAME','POC_PHONE','SIGN1','SIGN2','SIGN3','LNAME','HWY_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },/*{
        type: 'WFS',
        id: 'ntad_nhpn_sthwy',
        name: 'State Highways',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'RECID',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_nhpn_sthwy/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','RECID','LNAME','HWY_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },*/{
        type: 'WFS',
        id: 'ntad_rail_ln',
        name: 'Rail Lines',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'FRAARCID',
        renderer: 'ntad_rail_ln',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_rail_ln/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','FRAARCID','RROWNER1','POC_PHONE','CLASS','STATEAB','COUNTY','FREIGHT_STATIONS','RLL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ntad_rail_st',
        name: 'Rail Line Stations',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'FRANODEID',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_rail_st/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','FRANODEID','POC_NAME','POC_PHONE','STNNAME','CITY','STATEAB','RLN_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']

        }
    },{
        type: 'WFS',
        id: 'ntad_trans_ln',
        name: 'Fixed Guideway Transit Lines',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'RECID',
        renderer: 'ntad_trans_ln',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_trans_ln/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','RECID','POC_NAME','POC_PHONE','SYSTEM','SYSTEM2','UZA_STATE','COUNTY','UZA_CITY','TRL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'ntad_trans_st',
        name: 'Fixed Guideway Transit Stations',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'RECID',
        //renderer: 'polylines',
        url: nisis.url.arcgis.services + 'dot/ntad_trans_st/FeatureServer/0',
        secured: true,
        attributes:
        {
            fields: ['OBJECTID','RECID','STATION','POC_NAME','POC_PHONE','STR_ADD','UZA_STATE','UZA_CITY','TRS_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'hsip_nat_gas_comp_stations',
        name: 'HSIP Natural Gas Compressor Stations',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        url: nisis.url.arcgis.services + 'dot/hsip_nat_gas_comp_stations/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','OGR_FID','GCOMPID','STATION','PIPECO','COMPID','STATE','ZIP','POC_NAME','POC_PHONE','STA_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'hsip_nat_gas_liquid_pipelines',
        name: 'HSIP Natural Gas Liquid Pipelines',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'liquid_pipelines',
        url: nisis.url.arcgis.services + 'dot/hsip_nat_gas_liquid_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','OGR_FID','OPERATORNA','POC_NAME','POC_PHONE','LN_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'hsip_oil_refinery',
        name: 'HSIP Oil Refineries',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        url: nisis.url.arcgis.services + 'dot/hsip_oil_refinery/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','OGR_FID','PLATTSID','NAME','CITY','STATE','ZIP','POC_NAME','POC_PHONE','STA_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },/*{
        type: 'WFS',
        id: 'hsip_petrol_pump_stations',
        name: 'HSIP Petrol Pumping Stations',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        url: nisis.url.arcgis.services + 'dot/hsip_petrol_pump_stations/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','OGR_FID','PUMPID','NAME','OWNER','STATE','ZIP','STA_STATUS','POC_NAME','POC_PHONE','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },*/{
        type: 'WFS',
        id: 'hsip_terms_store_fac',
        name: 'HSIP POL Terminals / Storage Facilities / Tank Farms',
        group: 'dot',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        url: nisis.url.arcgis.services + 'dot/hsip_terms_store_fac/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','OGR_FID','TERMID','NAME','OWNER','ADDRESS','CITY','STATE','ZIP','STA_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_aa',
        name: 'Pipelines Anhydrous Ammonia',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'AA'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_co2',
        name: 'Pipelines Carbon Dioxide',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'CO2'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_crd',
        name: 'Pipelines Crude Oil',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'CRD'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','LINE_DESIG','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_epg',
        name: 'Pipelines Empty Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'EPG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_epl',
        name: 'Pipelines Empty Liquid',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'EPL'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_eth',
        name: 'Pipelines Fuel Grade Ethanol',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'ETH'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_hg',
        name: 'Pipelines Hydrogen Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'HG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_lpg',
        name: 'Pipelines Liquefied Petroleum Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'LPG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_ng',
        name: 'Pipelines Natural Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'NG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_ngl',
        name: 'Pipelines Natural Gas Liquids',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'NGL'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_ohv',
        name: 'Pipelines Other HVLs',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'OHV'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_otg',
        name: 'Pipelines Other Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'OTG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_pg',
        name: 'Pipelines Propane Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'PG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_prd',
        name: 'Pipelines Non-HVL Product',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'PRD'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WFS',
        id: 'pipelines_sg',
        name: 'Pipelines Synthetic Gas',
        group: 'pipelines',
        add: true,
        visible: false,
        tooltip: 'OBJECTID',
        renderer: 'pipelines',
        query: "COMMODITY = 'SG'",
        url: nisis.url.arcgis.services + 'dot/npms_pipelines/FeatureServer/0',
        secured: true,
        attributes: {
            fields: ['OBJECTID','POC_NAME','POC_PHONE','CMDTY_DESC','CMDTY_GEN','ST_ABBR','CNTY_NAME','PPL_STATUS','LAST_EDITED_USER','LAST_EDITED_DATE']
        }
    },{
        type: 'WMS',
        id: 'RIDGE_radar',
        name: 'RIDGE Radar',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/RIDGERadar/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Current_and_Watches_Warnings',
        name: 'Current and Watches Warnings',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/watchwarn/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Current_Warnings',
        name: 'Current Warnings',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/watchwarn/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Watches_Warnings',
        name: 'Watches Warnings',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/watchwarn/MapServer/1',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Radar_and_Warnings',
        name: 'Radar and Warnings',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/Radar_warnings/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Radar',
        name: 'Radar',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/Radar_warnings/MapServer/2',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Current_Warnings',
        name: 'Current Warnings',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/Radar_warnings/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Watches_Warnings',
        name: 'Watches Warnings',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/Radar_warnings/MapServer/1',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Atlantic_Hurricane_Forecast',
        name: 'Atlantic Hurricane Forecast',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/AtStormViewer/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'East_Pacific_Hurricane_Forecast',
        name: 'noaa East Pacific Hurricane Forecast',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/EpStormViewer/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'AHPS_River_Gauge_Observations_Forecasts',
        name: 'AHPS River Gauge Observations Forecasts',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/ahps_gauges/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'National_Significant_River_Flood_Outlook',
        name: 'National Significant River Flood Outlook',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/FOP/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Hazards_Drought_Wildlife',
        name: 'U.S. Hazards / Drought / Wildlife',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/cpc_weather_hazards/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Quantitative_Precipication_Forecast',
        name: 'Quantitative Precipication Forecast',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/QPF/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Weather_Features_Precipitation_Forecast',
        name: 'Weather Features / Precipitation Forecast',
        group: 'noaa',
        add: false,
        visible: false,
        template: false,
        url: nisisProtocol + '://gis.srh.noaa.gov/arcgis/rest/services/wxmap/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'US_Zip_Codes',
        name: 'U.S. Zip Codes',
        group: 'census',
        add: true,
        visible: false,
        template: false,
        //url: nisis.url.arcgis.services + 'census/census_features/MapServer/0',
        url: nisis.url.arcgis.services + 'census/census_zip_codes/MapServer/0',
        attributes: {
            fields: ['ZIPCODE']
        }
    },{
        type: 'WFS',
        id: 'US_Counties',
        name: 'U.S. Counties',
        group: 'census',
        add: true,
        visible: false,
        template: false,
        //url: nisis.url.arcgis.services + 'census/census_features/MapServer/1',
        url: nisis.url.arcgis.services + 'census/census_counties/MapServer/0',
        attributes: {
            fields: ['NAME', 'STATE_NAME', 'STATE_FIPS', 'CNTY_FIPS']
        }
    },{
        type: 'WFS',
        id: 'US_States',
        name: 'U.S. States',
        group: 'census',
        add: true,
        visible: false,
        template: false,
        //url: nisis.url.arcgis.services + 'census/census_features/MapServer/2',
        url: nisis.url.arcgis.services + 'census/census_states/MapServer/0',
        attributes: {
            fields: ['STATE_NAME', 'STATE_ABBR', 'STATE_FIPS', 'SUB_REGION']
        }
    },{
        type: 'WFS',
        id: 'Bathymetry Linear',
        name: 'Bathymetry Linear',
        group: 'hsip',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Political_Boundaries_Linear',
        name: 'Political Boundaries Linear',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/1',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'City_Limits',
        name: 'City Limits',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/2',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'City_Area_of_Interest',
        name: 'City Area of Interest',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/3',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Counties',
        name: 'Counties',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/4',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Congressional_Districts',
        name: 'Congressional Districts',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/5',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'EPA_Regions',
        name: 'EPA Regions',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/6',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Federal_Lands',
        name: 'Federal Lands',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/7',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Indian_Reservations',
        name: 'Native American Reservations',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/8',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Islands',
        name: 'Islands',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/9',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Land_Use',
        name: 'Land Use',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/10',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'National_Forest',
        name: 'National Forest',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/11',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'National_Marine_Sanctuaries',
        name: 'National Marine Sanctuaries',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/12',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Political_Boundaries',
        name: 'Political Boundaries',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/13',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Urbanized_Area',
        name: 'Urbanized Area',
        group: 'hsip',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/14',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Counties_Boundaries',
        name: 'Counties Boundaries',
        group: 'hsip',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/15',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'States_Boundaries',
        name: 'States Boundaries',
        group: 'hsip',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/16',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'Bathymetry_Polygons',
        name: 'Bathymetry Polygons',
        group: 'hsip',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hsip/hsip_boundaries/MapServer/17',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_iner',
        name: 'DAFIF Inter Layers',
        group: 'inter',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_arpt_inter',
        name: 'Airports',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [0],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_wpth_inter',
        name: 'High Altitude Waypoints',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [1],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_wptl_inter',
        name: 'Low Altitude Waypoints',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [2],
        attributes: {
            fields: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_atsh_inter',
        name: 'High Altitude Air Routes',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [3],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_atsl_inter',
        name: 'Low Altitude Air Routes',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [4],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_runways_inter',
        name: 'Runways',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [5],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_firh_inter',
        name: 'FIR High Boundaries',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [6],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'dafif_firl_inter',
        name: 'FIR Low Boundaries',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'faa/faa_dafif_inter/MapServer/',
        vLayers: [7],
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'timezones',
        name: 'World Time Zones',
        group: 'inter',
        add: true,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'other/timezones/MapServer/',
        vLayers: [0]
    },{
        type: 'WMS',
        id: 'Sonia_2013',
        name: 'Sonia 2013',
        group: 'hurrep',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hurricanes/hurr_sonia_2013/MapServer/',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WMS',
        id: 'Basemap',
        name: 'Basemap',
        group: 'hurrbm',
        add: false,
        visible: false,
        template: false,
        url: nisis.url.arcgis.services + 'hurricanes/hurr_basemap/MapServer/'
    },
    //Threat Layers
    //Weather ==> Models
    //Weather ==> Models => HPA
    {
        type: 'WFS',
        id: 'wmhtl',
        name: 'High Threat Latest',
        group: 'wmhpa',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/HPA/MapServer/0',
        secured: true
    },{
        type: 'WFS',
        id: 'wmmtl',
        name: 'Moderate Threat Latest',
        group: 'wmhpa',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/HPA/MapServer/1'
        ,secured: true
    },
    //Weather ==> Models => LPA
    {
        type: 'WFS',
        id: 'wmhlt',
        name: 'High Lighting Threat',
        group: 'wmlpa',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/lpa_conus/MapServer/0'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wmmlt',
        name: 'Moderate Lighting Threat',
        group: 'wmlpa',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/lpa_conus/MapServer/1'
        ,secured: true
    },
    //Weather ==> Models => LAPS
    {
        type: 'WMS',
        id: 'wmwsnd',
        name: 'Wind Speed & Direction',
        group: 'wmlaps',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/LAPS/MapServer/'
        ,vLayers: [0]
        ,secured: true
    },{
        type: 'WMS',
        id: 'wmdtif',
        name: 'Dewpoint Temperature in F',
        group: 'wmlaps',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/LAPS/MapServer/'
        ,vLayers: [1]
        ,secured: true
    },{
        type: 'WMS',
        id: 'wmrhip',
        name: 'Relative Humidity in %',
        group: 'wmlaps',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/LAPS/MapServer/'
        ,vLayers: [2]
        ,secured: true
    },{
        type: 'WMS',
        id: 'wmstif',
        name: 'Surface Temperature in F',
        group: 'wmlaps',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/LAPS/MapServer/'
        ,vLayers: [3]
        ,secured: true
    },{
        type: 'WMS',
        id: 'wmvim',
        name: 'Visibility in Miles',
        group: 'wmlaps',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/LAPS/MapServer/'
        ,vLayers: [4]
        ,secured: true
    },{
        type: 'WMS',
        id: 'wmws',
        name: 'Wind Speed',
        group: 'wmlaps',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/LAPS/MapServer/'
        ,vLayers: [5]
        ,secured: true
    },
    //Weather ==> Models => QPE
    {
        type: 'WFS',
        id: 'wmhp24',
        name: '24 Hour Precipitation (in)',
        group: 'wmqpe',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/QPE_Current/MapServer/0'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wmhp6',
        name: '6 Hour Precipitation (in)',
        group: 'wmqpe',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/QPE_Current/MapServer/1'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wmhp1',
        name: '1 Hour Precipitation (in)',
        group: 'wmqpe',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Model/QPE_Current/MapServer/2'
        ,secured: true
    },
    //Weather ==> Observations
    //Weather ==> Obs ==> CL
    {
        type: 'WFS',
        id: 'woccl',
        name: 'US CG Lightning',
        group: 'wocl',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/conus_lightning/MapServer/0'
        ,secured: true
    },
    //Weather ==> Obs ==> SO
    {
        type: 'WFS',
        id: 'worh',
        name: 'Relative Humidity',
        group: 'woso',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/CONUS_sfc_obs_production/MapServer/0'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wow',
        name: 'Winds',
        group: 'woso',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/CONUS_sfc_obs_production/MapServer/1'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wot',
        name: 'Temperature',
        group: 'woso',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/CONUS_sfc_obs_production/MapServer/2'
        ,secured: true
    },
    //Weather ==> Obs ==> CWW
    {
        type: 'WFS',
        id: 'wonww',
        name: 'NWS Watches and Warnings',
        group: 'wocww',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/CONUS_Watch_Warning/MapServer/0'
        ,secured: true
    },
    //Weather ==> Obs ==> FPC
    {
        type: 'WMS',
        id: 'wopc',
        name: 'Pressure Centers',
        group: 'wofpc',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/Fronts_and_Pressure_Centers/MapServer/'
        ,vLayers: [0]
        ,secured: true
    },{
        type: 'WMS',
        id: 'wof',
        name: 'Fronts',
        group: 'wofpc',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/Fronts_and_Pressure_Centers/MapServer/'
        ,vLayers: [1]
        ,secured: true
    },
    //Weather ==> Obs ==> CO
    {
        type: 'WFS',
        id: 'wod1c',
        name: 'Day 1 Categorical',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/0'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod1h',
        name: 'Day 1 Hail',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/1'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod1t',
        name: 'Day 1 Tornado',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/2'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod1w',
        name: 'Day 1 Wind',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/3'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod2as',
        name: 'Day 2 Any Severe',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/4'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod2c',
        name: 'Day 2 Categorical',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/5'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod3as',
        name: 'Day 3 Any Severe',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/6'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wod3c',
        name: 'Day 3 Categorical',
        group: 'woco',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Observations/SPC_Outlook/MapServer/7'
        ,secured: true
    },
    //Weather ==> Radar and Satellite
    {
        type: 'WFS',
        id: 'wrsnascr',
        name: 'North America Radar Single Scan Radar',
        group: 'wrsat',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Radar/NA_Radar_SingleScan/MapServer/0'
        ,secured: true
    },{
        type: 'WMS',
        id: 'wrsgis',
        name: 'Global Infrared Satellite',
        group: 'wrsat',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Satellite/Global_IR_Satellite/MapServer/'
        ,vLayers: [0]
        ,secured: true
    },
    //Weather ==> Current Tropical
    {
        type: 'WFS',
        id: 'wctocl',
        name: 'Observed Current Location',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/0'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctfl',
        name: 'Forecast Locations',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/1'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctol',
        name: 'Observed Locations',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/2'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctft',
        name: 'Forecast Track',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/3'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctot',
        name: 'Observed Track',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/4'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctfec',
        name: 'Forecast Error Cone',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/5'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctf34wr',
        name: 'Forecast 34kt Wind Radius',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/8'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctf50wr',
        name: 'Forecast 50kt Wind Radius',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/7'
        ,secured: true
    },{
        type: 'WFS',
        id: 'wctf64wr',
        name: 'Forecast 64kt Wind Radius',
        group: 'wctrop',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_current/MapServer/6'
        ,secured: true
    },  
    // Weather ==> Forecasted
        {
        type: 'WFS',
        id: 'wctfm',
        name: 'Tropical Forecast Model Points',
        group: 'wcforc',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_Forecast_Models/MapServer/0',
        secured: true,
    },{
        type: 'WFS',
        id: 'wctfm1',
        name: 'Tropical Forecast Model Lines',
        group: 'wcforc',
        add: true,
        visible: false,
        url: nisis.url.arcgis.threatLayers + 'Tropical/Tropical_Forecast_Models/MapServer/1',
        secured: true,
    },
    //Weather ==> Fires
    {
        type: 'WFS',
        id: 'wflfp',
        name: 'Large Fire Points',
        group: 'wfires',
        add: true,
        visible: false,
        url: nisisProtocol + '://wildfire.cr.usgs.gov/arcgis/rest/services/geomac_fires/MapServer/1'
    },{
        type: 'WFS',
        id: 'wffp',
        name: 'Fire Perimeters',
        group: 'wfires',
        add: true,
        visible: false,
        url: nisisProtocol + '://wildfire.cr.usgs.gov/arcgis/rest/services/geomac_fires/MapServer/2'
    },{
        type: 'WFS',
        id: 'wfmts',
        name: 'MODIS Thermal Satellite',
        group: 'wfires',
        add: true,
        visible: false,
        url: nisisProtocol + '://wildfire.cr.usgs.gov/arcgis/rest/services/geomac_fires/MapServer/3'
    },{
        type: 'WFS',
        id: 'wfifp',
        name: 'Inactive Fire Perimeters',
        group: 'wfires',
        add: true,
        visible: false,
        url: nisisProtocol + '://wildfire.cr.usgs.gov/arcgis/rest/services/geomac_fires/MapServer/4'
    },{
    //Weather ==> Advanced Hydrologic Prediction Service -irena
        type: 'WFS',
        id: 'observerd_river_stages',
        name: 'Observed River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/0'
    },  
    {
        type: 'WFS',
        id: '48_hour_forecast_river_stages',
        name: '48 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/1'
    },
    {
        type: 'WFS',
        id: '72_hour_forecast_river_stages',
        name: '72 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/2'
    },
    {
        type: 'WFS',
        id: '96_hour_forecast_river_stages',
        name: '96 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/3'
    },
    {
        type: 'WFS',
        id: '120_hour_forecast_river_stages',
        name: '120 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/4'
    },
    {
        type: 'WFS',
        id: '144_hour_forecast_river_stages',
        name: '144 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/5'
    },
    {
        type: 'WFS',
        id: '192_hour_forecast_river_stages',
        name: '192 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/6'
    },
    {
        type: 'WFS',
        id: '216_hour_forecast_river_stages',
        name: '216 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/7'
    },
    {
        type: 'WFS',
        id: '240_hour_forecast_river_stages',
        name: '240 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/8'
    },
    {
        type: 'WFS',
        id: '264_hour_forecast_river_stages',
        name: '264 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/9'
    },
    {
        type: 'WFS',
        id: '288_hour_forecast_river_stages',
        name: '288 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/10'
    },
    {
        type: 'WFS',
        id: '312_hour_forecast_river_stages',
        name: '312 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/11'
    },
    {
        type: 'WFS',
        id: '336_hour_forecast_river_stages',
        name: '336 Hour Forecast River Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/12'
    },
    {
        type: 'WFS',
        id: 'full_forecast_period_stages',
        name: 'Full Forecast Period Stages',
        group: 'rivers',
        add: true,
        visible: false,
        template: false,
        secured: true,
        url: nisisProtocol + '://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Observations/ahps_riv_gauges/MapServer/13'
    },
    //ArcGIS Online Layers
    {
        type: 'TILES',
        id: 'nav_chart',
        name: 'World Navigation Charts',
        group: 'basemap',
        add: true,
        visible: false,
        url: nisisProtocol + '://services.arcgisonline.com/arcgis/rest/services/Specialty/World_Navigation_Charts/MapServer'
    },{
        type: 'OGC',
        id: 'base_light',
        name: 'Dard Convas',
        group: 'basemap',
        layers: ["base_light"],
        add: false,
        visible: false,
        url: nisis.url.geoserver.services,
        basemap: true
    },{
        type: 'WTL',
        id: 'base_light',
        name: 'Dard Convas',
        group: 'basemap',
        subDomains: ['a','b','c'],
        layers: ["base_light"],
        add: false,
        visible: false,
        url: nisisProtocol + "://${subDomain}.tile.opencyclemap.org/cycle/${level}/${col}/${row}.png",
        copyright: 'Map Box'
    },{
        type: 'WFS',
        id: 'dash_airports',
        name: 'All Airports',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'dash_airports_pub',
        name: 'Public Use Airports',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'dash_atm',
        name: 'ATM',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'dash_ans',
        name: 'NAPRS ANS',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },{
        type: 'WFS',
        id: 'dash_osf',
        name: 'OSF',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    },
    // NISIS-2754 - KOFI HONU
    {
        type: 'WFS',
        id: 'dash_tof',
        name: 'TOF',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    }
    /*,{
        type: 'WFS',
        id: 'dash_resp',
        name: 'Resp. Resources',
        group: 'dashdataset',
        add: false,
        visible: false,
        table: 'nisis.master_objects_vw',
        template: false,
        url: nisis.url.arcgis.services + 'nisis/nisis_objects/MapServer/0',
        attributes: {
            fields: ['*'],
            labels: ['*']
        }
    }*/];
});
