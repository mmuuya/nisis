define(["dojo/_base/declare", "esri/layers/DynamicMapServiceLayer", "/nisis/settings/settings.js"]
	,function(declare, DynamicMapServiceLayer, settings){

		//'nisis.ShaddedReliefLight',
	return declare(null,["esri/layers/DynamicMapServiceLayer"], {
		constructor: function(){
			this.initialExtent = this.fullExtent = new esri.geometry.Extent({"xmin":-16476154.32,"ymin":2504688.54,"xmax":-6457400.14,"ymax":7514065.62,"spatialReference":{"wkid":102100}});
		    this.spatialReference = new esri.SpatialReference({wkid:102100});
		    this.loaded = true;
		    this.onLoad(this);
		},

		getImageUrl: function(extent, width, height, callback) {
			var params = {
		        request:"GetMap",
		        transparent:true,
		        format:"image/png",
		        bgcolor:"ffffff",
		        version:"1.1.1",
		        layers:"0,1",
		        styles: "default,default",
		        exceptions: "application/vnd.ogc.se_xml",

		        bbox:extent.xmin + "," + extent.ymin + "," + extent.xmax + "," + extent.ymax,
		        srs: "EPSG:" + extent.spatialReference.wkid,
		        width: width,
		        height: height
		    };

		    //callback(nisis.url.geoserver.services + "?" + dojo.objectToQuery(params));
		    //callback("https://192.168.19.211:6443/arcgis/rest/services/hurricanes/hurr_basemap/MapServer/WMSServer?" + dojo.objectToQuery(params));
		    callback(nisisProtocol + '://sampleserver1.arcgisonline.com/arcgis/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer?settings.protocol + ' + dojo.objectToQuery(params));
		}
	});

});