define(["dojo/_base/Color",
    "esri/symbols/SimpleMarkerSymbol","esri/symbols/PictureMarkerSymbol","esri/symbols/MarkerSymbol",
    "esri/symbols/SimpleLineSymbol","esri/symbols/LineSymbol","esri/symbols/CartographicLineSymbol",
    "esri/symbols/SimpleFillSymbol","esri/symbols/PictureFillSymbol","esri/symbols/FillSymbol",
    "esri/symbols/TextSymbol","esri/symbols/Font","esri/renderers/Renderer",
    "esri/renderers/SimpleRenderer","esri/renderers/UniqueValueRenderer",
    "esri/renderers/ClassBreaksRenderer"
    ,"app/symbols/symbolsPaths"
    ,"app/mapConfig"
    ,"app/iconsConfig"
    ,"app/symbols/symbolData"
    ], function(Color,
    SimpleMarkerSymbol,PictureMarkerSymbol,MarkerSymbol,
    SimpleLineSymbol,LineSymbol,CartographicLineSymbol,
    SimpleFillSymbol,PictureFillSymbol,FillSymbol,
    TextSymbol,Font,Renderer,
    SimpleRenderer,UniqueValueRenderer,
    ClassBreaksRenderer
    ,paths
    ,mapConfig
    ,icons
    ,shapesData
    ){
    
    var renderers = {};
});