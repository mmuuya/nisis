/*
 * App utilities
 */

define([], function() {

	return {
		log: function(){
			var host = window.location.hostname,
				debug = false;

			if (host === 'localhost') {
				debug = true;
			}

			if (debug && arguments.length) {
				console.log(arguments);
			}
		},
		addEvent: function(el, type, handler) {
			if ( el == null || typeof(el) == 'undefined' ) return;

			if ( el.addEventListener ) {
				el.addEventListener( type, handler, false );

			} else if ( el.attachEvent ) {
				el.attachEvent( 'on' + type, handler);

			} else {
				el['on'+type] = handler;
			}
		},
		getRandomChar: function(numChars, alphaNum) {
			var text = "",
				len = 3,
				chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
				number = "0123456789",
				choice = chars + chars.toLowerCase();

			if (alphaNum) {
				choice += number;
			}

			if (numChars && $.isNumeric(numChars)) {
				len = numChars
			}

			for (var i=0; i < len; i++) {
				text += choice.charAt(Math.floor(Math.random() * choice.length));
			}

			return text;
		},
		getCharAtPos: function(position) {
			if (position < 1) {
				return " ";
			}

			var chars = {
				1: 'A',
				2: 'B',
				3: 'C',
				4: 'D',
				5: 'E',
				6: 'F',
				7: 'G',
				8: 'H',
				9: 'I',
				10: 'J',
				11: 'K',
				12: 'L',
				13: 'M',
				14: 'N',
				15: 'O',
				16: 'P',
				17: 'Q',
				18: 'R',
				19: 'S',
				20: 'T',
				21: 'U',
				22: 'V',
				23: 'W',
				24: 'X',
				25: 'Y',
				26: 'Z'
			}

			if (position <= 26) {
				return chars[position];

			} else if (position > 26) {
				var pos = position,
					n = 1, c = "A";
					pos -= 26;

				while (pos > 0) {
					if (pos > 26) {
						n++;
						c += chars[n];
						pos -= 26;
						if (n == 26){
							n = 1;
						}

					} else {
						c += chars[pos];
						break;
					}
				}

				return c;

			} else {
				return null;
			}
		},
		getExtractDataFormats: function() {
            return [
	            {
	                "name": "ESRI Shapefile",
	                "value": "shapefile" //"Shapefile - SHP - .shp"
	            },
	            {
	                "name": "Google KML",
	                "value": "kml"
	            }/*,
	            {
	                "name": "OGC GeoJSON",
	                "value": "geojson"
	            }*/
            ];
        },
        months: ['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY',
			'AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'],
        formatDateTime: function(dateTime, sep, utc) {
			var zuluDate = new Date(dateTime);
	        var placeHolder;
	        //console.log('Last Update Date=' + zuluDate);
	        var toReturn = zuluDate.getUTCFullYear() + '/';

	        placeHolder = (zuluDate.getUTCMonth()+1);
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += '/';
	        placeHolder = zuluDate.getUTCDate();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += ' ';
	        placeHolder = zuluDate.getUTCHours();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        //toReturn += ':';
	        placeHolder = zuluDate.getUTCMinutes();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;
	        toReturn += 'Z';

	        //console.log('datetime format=' + toReturn);
	        return toReturn;
		},
		formatDateTimeToUtcDTG: function(dateTime, sep, sec) {
			var zuluDate = new Date(dateTime),
	        	placeHolder = null,
	        	sep = sep ? sep : "/",
	        	sec = sec ? sec : false;
	        //console.log('Last Update Date=' + zuluDate);
	        var toReturn = zuluDate.getUTCFullYear() + sep;

	        placeHolder = (zuluDate.getUTCMonth()+1);
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += sep;
	        placeHolder = zuluDate.getUTCDate();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += ' ';
	        placeHolder = zuluDate.getUTCHours();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += sep !== '' ? ':' : '';
	        placeHolder = zuluDate.getUTCMinutes();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        if (sec) {
	        	toReturn += sep !== '' ? ':' : '';
	        	placeHolder = zuluDate.getUTCSeconds();
	        	toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;
	        }

	        toReturn += 'Z';

	        //console.log('datetime format=' + toReturn);
	        return toReturn;
		},
		formatDateTimeToDTG: function(dateTime, sep, sec) {
			var zuluDate = new Date(dateTime),
	        	placeHolder = null,
	        	sep = sep ? sep : "/",
	        	sec = sec ? sec : false;

	        var toReturn = zuluDate.getFullYear() + sep;

	        placeHolder = (zuluDate.getMonth()+1);
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += sep;
	        placeHolder = zuluDate.getDate();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += ' ';
	        placeHolder = zuluDate.getHours();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        toReturn += sep !== '' ? ':' : '';
	        placeHolder = zuluDate.getMinutes();
	        toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;

	        if (sec) {
	        	toReturn += sep !== '' ? ':' : '';
	        	placeHolder = zuluDate.getSeconds();
	        	toReturn += placeHolder < 10 ? '0'+ placeHolder : placeHolder;
	        }

	        //toReturn += 'Z';

	        //
	        return toReturn;
		},
		formatDateToDTG: function(datetime, fullYear, tz) {
			var dt = new Date(datetime),
				msHr = 60 * 60 * 1000,
				date = null;

			if ( tz && (tz < 0 || tz > 0) ) {
				var newDate = Date.parse(dt);
				newDate += (-1 * (tz * msHr));
				dt = new Date(newDate);
			}

			var yy = dt.getFullYear();

			if ( !fullYear ) {
				yy = yy.toString().substr(2);
			}

			date = yy;

			var mm = dt.getMonth() + 1;

			date += mm < 10 ? "0" + mm : mm;

			var dd = dt.getDate();

			date += dd < 10 ? "0" + dd : dd;

			var hh = dt.getHours();

			date += hh < 10 ? "0" + hh : hh;

			var m = dt.getMinutes();

			date += m < 10 ? "0" + m : m;

			//"1502271534" or "201502271534"

			return date;
		},
		formatDateToText: function(datetime, tz) {
			if (!datetime) {
				return null;
			}

			var $this = this,
				dt = null,
				dd = null,
				mm = null,
				yy = null,
				msHr = 60 * 60 * 1000,
				date = null;
			//convert to local time
			if (tz && tz !== 0) {
				var d = Date.parse(new Date(datetime)) + (tz * msHr);
				dt = new Date(d);

			} else {
				dt = new Date(datetime);
			}

			if (!dt) return null;

			yy = dt.getFullYear();
			mm = $this.months[dt.getMonth()];
			dd = dt.getDate();

			dd = dd < 10 ? '0' + dd : dd;

			date = mm + ' ' + dd + ', ' + yy;

			return date;
		},
		formatDateToText2: function(datetime, tz) {
			if (!datetime) {
				return null;
			}

			var $this = this,
				tz = Number(tz),
				dt = null,
				dd = null,
				mm = null,
				yy = null,
				hh = null,
				min = null,
				msHr = 60 * 60 * 1000,
				date = null,
				time = null;
			//convert to local time
			if (tz && tz !== 0) {
				var d = Date.parse(new Date(datetime)) + (tz * msHr);
				dt = new Date(d);

			} else {
				dt = new Date(datetime);
			}

			if (!dt) return null;

			yy = dt.getFullYear().toString().substr(2);
			mm = dt.getMonth() + 1;
			dd = dt.getDate();
			hh = dt.getHours();
			min = dt.getMinutes();

			mm = mm < 10 ? '0' + mm : mm;
			dd = dd < 10 ? '0' + dd : dd;
			hh = hh < 10 ? '0' + hh : hh;
			min = min < 10 ? '0' + min : min;

			date = mm + '/' + dd + '/' + yy;
			time = hh + "" + min;
			//["1447", "04/09/15"]
			return [time, date];
		},
		arrayToString: function(input, floatPt) {
			var arr = input,
				output = "",
				n = 0;

			if (!input) {
				arr = [];
				return "";
			}

			function extractItems(arrList) {
				if ( output == "" ) {
					output += "(";

				} else if (output !== "" && output.lastIndexOf(")") == output.length -1) {
					output += ", (";
				}

				$.each(arrList, function(i, item) {

					if( typeof item === 'object' && item.length ) {
						extractItems(item);
						return;
					}

					n++;

					if (floatPt && typeof(item) == "number") {
						output += item.toFixed(floatPt);
					} else {
						output += item;
					}

					if ( n % 2 == 1 ) {
						output += ", "
					} else {
						output += ")"
					}
				});
			};

			extractItems(arr);

			return output;
		},
		getDistinctItems: function(arr, key) {
			if (!arr) {
				return null;
			}

			if ( !arr.length ) {
				//return null;
			}

			var arrayData,
				distincts = [];

			if ( key ) {

				arrData = $.map(arr, function(d) {
					return d[key];
				});

			} else {
				arrData = arr;
			}

			$.each(arrData, function(i, a) {
				if ( $.inArray(a, distincts) == -1 ) {
					distincts.push(a);
				}
			});

			return distincts;
		},
		/* compare 2 objects
		 * similar to dirty flag
		 */
		isEqual: function(o1, o2, cfg, reverse) {
		    cfg = cfg || {};
		    //first we check the reference. we don't care if null == undefined
		    cfg.exclude = cfg.exclude || {};

		    if (cfg.strictMode) {
		        if (o1 === o2) return true;
		    } else {
		        if (o1 == o2) return true;
		    }

		    if (typeof o1 == "number" || typeof o1 == "string" || typeof o1 == "boolean" || !o1 || typeof o2 == "number" || typeof o2 == "string" || typeof o2 == "boolean" || !o2) {
		        return false;
		    }

		    if ( ( (o1 instanceof Array) && !(o2 instanceof Array) ) || ( (o2 instanceof Array) && !(o1 instanceof Array) ) ) return false;

		    for (var p in o1) {
		        if (cfg.exclude[p] || !o1.hasOwnProperty(p)) continue;

		        if ( !isEqual(o1[p], o2[p], cfg) ) return false;
		    }

		    if (!reverse && !cfg.noReverse) {
		        reverse = true;
		        return isEqual(o2, o1, cfg, reverse);
		    }

		    return true;
		},
		formatTextWithSignature: function(statusExplanation, username, dtg) {
			//There is no need to put the Z here at the end because the dtg value already contains it. The dtg was built with appUtils.formatDateTimeToUtcDTG() and just passed here formatted
        	return (statusExplanation + '<br />'+ '<div class="signature"> Entered by ' + '<b>' + username + '</b>' + ' on ' + '<i>' + dtg + '</i></div>');
    	}
	};
});
