/*
 * Setting default attributes for user features
*/
define(["app/layersConfig", "app/renderers"],function(layers,renderers){
	return {
		user: function(){
			var user = {};
			$.each(layers,function(i,lyr){
				if(lyr.add){
					var layer = nisis.ui.mapview.map.getLayer(lyr.id);
					if(lyr.group === 'user' && layer.geometryType){
						var geom = null;
						if(layer.geometryType === 'esriGeometryPoint'){
							geom = 'point';
						} else if (layer.geometryType === 'esriGeometryPolyline'){
							geom = 'polyline';
						} else if (layer.geometryType === 'esriGeometryPolygon'){
							geom = 'polygon'
						}

						user[geom] = {};
						user[geom].id = lyr.id,
						user[geom].name = lyr.name,
						user[geom].fields = {};

						$.each(layer.fields,function(i,f){
							if(f.editable && !f.nullable) {
								if(f.name === "NAME"){
									user[geom].fields[f.name] = geom;
								} else if(f.name === "USER_GROUP"){
									user[geom].fields[f.name] = 0;
								} else if (f.name === "SYMBOL") {
									user[geom].fields[f.name] = 0;
								}
							} else if (f.editable && f.nullable){
								if (f.name === "DESCRIPTION") {
									user[geom].fields[f.name] = geom + ' feature';
								} else if (f.name === "SUP_NOTES") {
									user[geom].fields[f.name] = geom + ' feature';
								} 
							}
						});
					}
				}
			});
			return user;
		},
		nisisto: function(){
			var nisisto = {};
		}
	}
});