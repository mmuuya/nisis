/*
 * HTML FONTS
 */
define([], function(){
	return {
		'sans-sherif': ['Arial','Arial Black','Arial Narrow','Arial Rounded MT Bold','Avant Garde','Calibri',
			'Candara','Century Gothic','Franklin Gothic Medium','Futura','Geneva','Gill Sans','Helvetica','Impact',
			'Lucida Grande','Optima','Segoe UI','Tahoma','Trebuchet MS','Verdana'],
		'sherif': []
	}
});
