/*
 * geometry utils
 */

define([], function(){
	
	return {
		getDestinationPoint: function(lat1, lon1, d, angle) {
			var R = 6378.3,
				latitude1 = lat1 * (Math.PI / 180),
				longitude1 = lon1 * (Math.PI / 180),
				brng = angle * (Math.PI / 180);

			var latitude2 = Math.asin(Math.sin(latitude1) * Math.cos(d/R) + 
							Math.cos(latitude1) * Math.sin(d/R) * Math.cos(brng));

			var longitude2 = longitude1 + Math.atan2(Math.sin(brng) * Math.sin(d/R) * Math.cos(latitude1), 
							Math.cos(d/R) - Math.sin(latitude1) * Math.sin(latitude1));

			var lat2 = latitude2 * (180 / Math.PI),
				lon2 = longitude2 * (180 / Math.PI);

			return [lon2, lat2];
		}
	};
});