define(["app/layersConfig"]
	,function(layers){
    return kendo.observable({
    	//Dashboard Widget - Patrick Stalcup & Haeden Howland

    	//Global Variables:
    	$ovr_status:"",
		$ovr_sts_ad:"",

		ovr_status_categories:"",
		ovr_status_data:"",
		ovr_status_color:"",
		allData: [],
		layer: "NONE",
		enableDashFilter: false,
		enableDashAnalysis: false,
		filterDataSource: new kendo.data.DataSource({
			data: [{name:'None', value:""}]
		}),
		analysisDataSource: new kendo.data.DataSource({
			data: [{name:'Basic Status', value:"basic"}]
		}),

    	targetData: function(){
    		/*
				Generate applicable selections for the target data dropdown list.
    		*/
    		var $this = this;
			var dataSource = new kendo.data.DataSource({
				data: []
			});
			dataSource.add({name:'Select a layer', value:""});
			$.each(layers, function(i,lyr){      
	            if(lyr.group === 'dashdataset'){
	                dataSource.add({name:lyr.name, value:lyr.id});

	            }
	        });

	        return dataSource;
    	},

    	updateFilterDataSource: function(){
    		/*
				Get the applicable selections for the target filter dropdown list.
    		*/
    		var $this = this;
    		var dataSource = new kendo.data.DataSource({
				data: []
			});
			
			dataSource.add({name:'None', value:""});
			
			$.ajax({
				url: "api/",
				type: "POST",
				data: {
					action: "get_iwls",
					filter: $this.layer
				},
				async: false,
				success: function(d,t,j){
					for(var i = 0; i < d.results.length; i++){
						dataSource.add({name:d.results[i].NAME, value:d.results[i].ID});
					}

					$this.set('filterDataSource', dataSource);
				},
	 			dataType:"json"
	 		});
    	},

    	updateAnalysisDataSource: function(){
    		var $this = this;
    		
    		var dataSource = new kendo.data.DataSource({
    			data: []
    		});
    		dataSource.add({name:'Basic Status', value:"basic"});
    		// NISIS-2744 KOFI HONU - clean up readiness CRT_RL
    		// CRT_RL removed from nisis_airports table so avoid that column here.
    		if ($this.layer !== 'ans' && $this.layer !== 'airports'){
    			dataSource.add({name:'Readiness Level', value:"readiness"});
    		}
    		$this.set('analysisDataSource', dataSource);
    	},

    	filterData: function(e){
    		var $this = this;
    		var iwlOption = $('#dash-filter').data('kendoDropDownList').value();
    		var targetOption = $('#dash-targets').data('kendoDropDownList').value();
			var analysisOption = $('#dash-analysis').data('kendoDropDownList').value();

			if (iwlOption === ""){
				$this.queryData();
			}else{
				iwlOption = iwlOption.indexOf('None') > -1 ? undefined : iwlOption; //If default value, set to undefined
				analysisOption = analysisOption.indexOf('basic') > -1 ? undefined : analysisOption;

				$.ajax({
					url: "api/ovr_status.php",
					data: {queryType : targetOption,
							filterIWL : iwlOption,
							analysis : analysisOption},
					type: "POST",
					async:false,
					success: function(d,t,j){
						$this.fetchData(d); 
					},
		 			dataType:"json"
	 			});
			}
    		
    	},
		
    	changeLayer: function(e){
    		var $this = this;
    		var target = $('#dash-targets').data('kendoDropDownList').value();

			if (target === ''){
				$this.set('enableDashFilter', false);
    			$this.set('enableDashAnalysis', false);
    			$('#dash-ovr-status-chart').empty();
				$('#dash-ovr-sts-ad-chart').empty();
				$('#dash-filter').data('kendoDropDownList').value('');
				$('#dash-analysis').data('kendoDropDownList').value('basic');
			}else{
				$this.set('enableDashFilter', true);
    			$this.set('enableDashAnalysis', true);

    			var array = target.split("_");
    			$this.set('layer', array[1]);
				$this.updateFilterDataSource();
				$this.updateAnalysisDataSource();		
		 		$this.queryData();
			}
    	},

    	queryData: function(){
    		/*
				Query the data based off of the selection made by the user.
    		*/
			var $this = this;
			var option = $('#dash-targets').data('kendoDropDownList').value();
			var analysisOption = $('#dash-analysis').data('kendoDropDownList').value();

			analysisOption = analysisOption.indexOf('basic') > -1 ? undefined : analysisOption;
			$.ajax({
				url: "api/ovr_status.php",
				data: {queryType : option,
						analysis : analysisOption},
				type: "POST",
				async:false,
	 			dataType:"json",
				success: function(d,t,j){
					$this.fetchData(d); 
					// $('#dash-filter').data('kendoDropDownList').value('');
					// if(option != 'dash_airports' && option != 'dash_atm' && option != 'dash_osf' && option != 'dash_airports_pub'){						
					// 	$('#dash-analysis').data('kendoDropDownList').value('basic');
					// 	$('#dash-analysis').data('kendoDropDownList').enable(false);
					// 	$('#dash-filter').data('kendoDropDownList').enable(false);
					// 	// alert('false!');
					// } else {						
						// $('#dash-analysis').data('kendoDropDownList').enable(true);
						// $('#dash-filter').data('kendoDropDownList').enable(true);
						// alert('true!');
					// }
				}
	 		});
		},

		resizeSplitter: function() {
			var newSize = $('#dashboard-div').height()-15;
			$('#dashboard-content').css({"height": newSize+"px"});
			$('#dash-panel-left').css({"height": newSize+"px"});
			$('#dashwrapper').css({"height": newSize+"px"});
		},

		createDashboardWidget: function() {

			/*
				Initialize charts and global array data
			*/

            if($("#dashboard-content").data('reset') === true) {
    			$("#dashboard-content").data('kendoSplitter').toggle('#dash-panel-left');
				$("#dashboard-content").data('reset',false);
    		}

			ovr_status_categories = ["0","1","2","3","4"]; 
			ovr_status_data = [0,0,0,0,0]; 
			ovr_status_color = ["#000000","#000000","#000000","#000000","#000000"];
		},

		determineReadinessColor: function(in_status_data){
			var toReturn;
			switch(in_status_data) {
					case "0": //RL-Alpha
						toReturn = "#339900";
						break;
					case "1": //RL-Bravo
						toReturn = "#FFFF33";
						break;
					case "2": //RL-Charlie
						toReturn = "#FF8E33";
						break;
					case "3": //RL-Delta
						toReturn = "#FF0000";
						break;
					case "4":  //Unknown
						toReturn = "#999999";
						break;
					default: //N/A
						toReturn = "#CCCCCC"; 
				}
			return toReturn;
		},
		
		determineDisplayColor: function(in_status_data, in_table_data){
			/*
				Given the input, determine the correct color to correspond to
				the section of the pie chart represented. Save data to global array
				for access while building charts.
			*/
			var toReturn = "";
			var analysisOption = $('#dash-analysis').data('kendoDropDownList').value();
			analysisOption = analysisOption.indexOf('basic') > -1 ? undefined : analysisOption;		

			if (in_table_data == 'nisis_airports'){	
				//Airport object colors			
				switch(in_status_data) {
					case "1": //Open with Normal Operations
						toReturn = "#339900";
						break;
					case "2": //Open with Limitations
						toReturn = "#FFFF33";
						break;
					case "3": //Closed for Normal Operations
						toReturn = "#B00000";
						break;
					case "4": //Closed for All Operations
						toReturn = "#FF0000";
						break;
					case "5":  //Other
						toReturn = "#999999";
						break;
					default: //Unknown
						toReturn = "#CCCCCC"; 
				}
				//Airport Readiness Level
				if (analysisOption){
					toReturn = this.determineReadinessColor(in_status_data);
				}

			} else if (in_table_data == 'nisis_atm'){
				//ATM object colors
				switch(in_status_data) {
					case "0": //Normal Operations
						toReturn = "#339900";
						break;
					case "1": //ATC Alert
						toReturn = "#FFFF33";
						break;
					case "2": //ATC Limited
						toReturn = "#FFCA33";
						break;
					case "3": //ATC 0
						toReturn = "#FF0000";
						break;
					default: //Other
						toReturn = "#999999"; 
				}
				//ATM Readiness Level
				if (analysisOption){
					toReturn = this.determineReadinessColor(in_status_data);
				}
			} else if (in_table_data == 'nisis_ans'){
				//ANS object colors
				switch(in_status_data) {
					case "0": //Normal Operation
						toReturn = "#339900";
						break;
					case "1": //Line Only Interruption
						toReturn = "#FFFF33";
						break;
					case "2": //Reduced System Equipment
						toReturn = "#FFCA33";
						break;
					case "3": //Reduced Facility/Service Interruption
						toReturn = "#FF8E33";
						break;
					default:  //Full Interuption
						toReturn = "#FF0000";
				}
			} else if (in_table_data == 'nisis_osf'){
				//OSF object colors
				switch(in_status_data) {
					case "0": //Normal Operations
						toReturn = "#339900";
						break;
					case "1": //Limited/Degraded Operations
						toReturn = "#FFFF33";
						break;
					case "2": //Closed
						toReturn = "#FF0000";
					default: //Other
						toReturn = "#999999"; 
				}
				//OSF Readiness Level
				if (analysisOption){
					toReturn = this.determineReadinessColor(in_status_data);
				}
			} 
			// NISIS-2767 KOFI HONU
			else if (in_table_data == 'nisis_tof'){
				switch(in_status_data) {
					case "0": //Open with Normal Operations
						toReturn = "#00ff00";
						break;
					case "1": //Open with Normal Operations
						toReturn = "#339900";
						break;
					case "2": //Open with Limitations
						toReturn = "#FFFF33";
						break;
					case "3": //Closed for Normal Operations
						toReturn = "#B00000";
						break;
					case "4": //Closed for All Operations
						toReturn = "#FF0000";
						break;
					case "5":  //Other
						toReturn = "#999999";
						break;
					default: //Other
						toReturn = "#999999"; 
				}
				//OSF Readiness Level
				if (analysisOption){
					toReturn = this.determineReadinessColor(in_status_data);
				}
			} 
			// NISIS-2742- KOFI HONU
			// else if (in_table_data == 'nisis_resp'){
			// 	//Responce Resource object colors
			// 	switch(in_status_data) {
			// 		case "1": //Inside IWL or FS
			// 			toReturn = "#339900";
			// 			break;
			// 		default: //Outside IWL or FS
			// 			toReturn = "#FF0000"; 
			// 	}
			// }

			return toReturn;
		},

		createCharts: function (chart_type) {
			/*
				Function to build the charts themselves given global array data.
			*/

			$ovr_status = $("#dash-ovr-status-chart"); 
			$ovr_sts_ad = $("#dash-ovr-sts-ad-chart"); 

			$ovr_status.css({"height":"300px"}); 
			$ovr_sts_ad.css({"height":"300px"}); 

			var table_name, display_name;

			if (chart_type == null){
				table_name = "Invalid Selection";
			} else {
				table_name = chart_type;
			}
			
			display_name = table_name;
			$ovr_status.prepend('<h1>' + display_name + '</h1>');
			$ovr_status.kendoChart({
	 			// title : {
	 			// 	text : display_name
	 			// },
	 			legend : {
	 				visible : false 
	 			},
	 			seriesDefaults: {
	 				type : "bar",
	 				labels: {
                        template: "#= value #",
                        position: "insideEnd", //center insideEnd insideBase outsideEnd
                        visible: true,
                        background: "transparent"
	 				}
	 			},
	 			series : [{
	 				// name : "Bar_Graph",
	 				// data : ovr_status_data
	 				field: 'value',
	 				categoryField: 'category',
	 				colorField: "color",
	 				data : allData 	
	 			}],
	 			categoryAxis : [{
	 				categories: ovr_status_categories
	 			}],  
	 			tooltip: {
					visible: true,
					template: "#= category #: #= value #"
                }
	 		}).prepend('<h1>' + display_name + '</h1>'); 

			display_name = table_name;
			$ovr_sts_ad.kendoChart({
	 			// title : {
	 			// 	text : display_name
	 			// },
	 			legend : {
	 				//visible : true,
	 				position: "bottom"
	 			},
	 			seriesDefaults: {
	 				//type : "pie",
	 				//startAngel: 180
	 				labels: {
                        template: "#= value #\n#= kendo.format('{0:P}', percentage)#",
                        position: "outsideEnd", //center insideEnd insideBase outsideEnd
                        visible: true,
                        background: "transparent"
	 				}
	 			},
	 			series : [{
	 				//name : "Pie_Chart",
	 				type: 'pie',
	 				field: 'value',
	 				categoryField: 'category',
	 				colorField: "color",
	 				data : allData 				
	 			}],	
	 			// seriesCategories: ovr_status_categories,	
	 			// seriesColors: ovr_status_color, 
	 			tooltip: {
					visible: true,
					template: "#= category #: #= value # (#= kendo.format('{0:P}', percentage)#)"
                }
	 		}).prepend('<h1>' + display_name + '</h1>'); 
		},

	    fetchData: function (in_data) {
	    	/*
				Function received a JSON-type data array from the database query, sets up global
				arrays with new data and calls the function to rebuild the charts with the newest data.
	    	*/
	    	//NISIS-2744 KOFI HONU - Cleanup Readiness CRT_RL
	    	// added this if statement to avoid console errors when a dropdown choice in GUI returned zero query results.	    	
	    	if (in_data.length == 0 ){
	    		$('#dash-ovr-status-chart').empty();
				$('#dash-ovr-sts-ad-chart').empty();
	    		return;
	    	}

	    	var $this = this;
	    	var display_name = null; 

	    	allData = [];

	    	while (ovr_status_categories.length > in_data.length) {
	    		ovr_status_categories.pop();
	    		ovr_status_data.pop();
	    		ovr_status_color.pop();
	    	}

	    	for(var i in in_data) { 
	    		ovr_status_categories[i] = in_data[i]["OVR_STATUS_TEXT"];	    		
	    		ovr_status_data[i] = in_data[i]["COUNT"]; 
	    		// NISIS-2767 KOFI HONU -- added this if statement in passing to avoid "undefined" on chart
	    		if (in_data[i]["OVR_STATUS"] != undefined && in_data[i]["SOURCETABLE"] != undefined){
		    		ovr_status_color[i] = this.determineDisplayColor(in_data[i]["OVR_STATUS"], in_data[i]["SOURCETABLE"]);
		    		allData.push({category: in_data[i]["OVR_STATUS_TEXT"], value: in_data[i]["COUNT"], color: ovr_status_color[i]});
	    		}
	    		// console.log(ovr_status_categories[i] + ", " + ovr_status_data[i] + ", " + 
	    		// 	in_data[i]["OVR_STATUS"] + ", " + ovr_status_color[i]);
	    	} 
	    	
	    	if(allData.length == 0){
	    		ovr_status_categories[0] = "Invalid IWL selection";
	    		ovr_status_data[0] = undefined;
	    		display_name = "Invalid IWL for object selection";
	    		$('#dash-ovr-status-chart').empty();
				$('#dash-ovr-sts-ad-chart').empty();

	    	} else {
				var analysisOption = $('#dash-analysis').data('kendoDropDownList').value();
				analysisOption = analysisOption.indexOf('basic') > -1 ? undefined : analysisOption;	    		
		    	switch(in_data[0]['SOURCETABLE'].toLowerCase()){
		    		case "nisis_airports":
		    			display_name = analysisOption ? "Airport Readiness Level" : "Airport Status";
		    			var public_use = $('#dash-targets').data('kendoDropDownList').value();
						public_use = public_use.indexOf('dash_airports_pub') > -1 ? "Public " : "";
						display_name = public_use + display_name;
		    			break;
		    		case "nisis_atm":
		    			display_name = analysisOption ? "ATM Readiness Level" : "ATM Status";
		    			break;
		    		case "nisis_ans":
		    			display_name = "ANS Status";
		    			break;
		    		case "nisis_osf":
		    			display_name = analysisOption ? "OSF Readiness Level" : "OSF Status";
		    			break;
		    		// NISIS-2767 KOFI HONU
		    		case "nisis_tof":
		    			display_name = analysisOption ? "TOF Readiness Level" : "TOF Status";
		    			break;
		    		// NISIS-2742 - KOFI HONU
		    		// case "nisis_resp":
		    		// 	display_name = "Response Resource Status";
		    		// 	break;
		    		default:
		    			display_name = "Invalid selection";
		    	}
		    	$this.createCharts(display_name);
    		}
 			
	 	}
    }) 
});