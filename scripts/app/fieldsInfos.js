/**
 * app shape tools
 */
define(['dijit/form/ValidationTextBox'], function(ValidationTextBox){
	return {
		//Validation
		validName: new dijit.form.ValidationTextBox({
          regExp : /~|`|!|@|#|\$|%|\^|&|\*|\(|\)|_|\+|\[|\]|\|;|'|,|\.|,|\/|<|>|\?|:|"|{|}|\|/,
          required : false,
          promptMessage: "Enter a shape name with valid characters",
          invalidMessage : "Your input contains illegal characters"
	  }),
		//Text layers
		text: [{
			'fieldName': "LABEL",
			'label': "LABEL",
			'isEditable': true

		}
		//NISIS-2222: KOFI - commented out
		// ,{
		// 	'fieldName': "USER_GROUP",
		// 	'label': "USER GROUP",
		// 	'isEditable': false,
		// }
		/*,{ //hidden field
			'fieldName': "SYMBOLS",
			'label': "SYMBOL",
			'isEditable': false,
		}*/],
		//user point features
		points: [{
			'fieldName': "NAME",
			'label': "NAME",
			'isEditable': true
		},{
			'fieldName': "DESCRIPTION",
			'label': "DESCRIPTION",
			'isEditable': true
		},{
			'fieldName': "REF_NUMBER",
			'label': "REFERENCE NUMBER",
			'isEditable': false
		},{
			'fieldName': "SUP_NOTES",
			'label': "SUPPLEMENTAL NOTES",
			'isEditable': true
		},{
			'fieldName': "MIN_A_AGL",
			'label': "MINIMUM ALTITUDE (AGL)",
			'isEditable': true
		},{
			'fieldName': "MIN_A_MSL",
			'label': "MINIMUM ALTITUDE (MSL)",
			'isEditable': true
		},{
			'fieldName': "MAX_A_AGL",
			'label': "MAXIMUM ALTITUDE (AGL)",
			'isEditable': true
		},{
			'fieldName': "MAX_A_MSL",
			'label': "MAXIMUM ALTITUDE (MSL)",
			'isEditable': true
		},{
			'fieldName': "VERTICES",
			'label': "VERTICES",
			'isEditable': false
		}/*,{
			'fieldName': "USER_GROUP",
			'label': "NAME",
			'isEditable': false
		},{ //hidden field
			'fieldName': "SYMBOL",
			'label': "SYMBOL",
			'isEditable': false
		}*/],
		//user line features
		polylines: [{
			'fieldName': "NAME",
			'label': "NAME",
			'isEditable': true
		},{
			'fieldName': "DESCRIPTION",
			'label': "DESCRIPTION",
			'isEditable': true
		},{
			'fieldName': "REF_NUMBER",
			'label': "REFERENCE NUMBER",
			'isEditable': false
		},{
			'fieldName': "SUP_NOTES",
			'label': "SUPPLEMENTAL NOTES",
			'isEditable': true
		},{
			'fieldName': "MIN_A_AGL",
			'label': "MINIMUM ALTITUDE (AGL)",
			'isEditable': true
		},{
			'fieldName': "MIN_A_MSL",
			'label': "MINIMUM ALTITUDE (MSL)",
			'isEditable': true
		},{
			'fieldName': "MAX_A_AGL",
			'label': "MAXIMUM ALTITUDE (AGL)",
			'isEditable': true
		},{
			'fieldName': "MAX_A_MSL",
			'label': "MAXIMUM ALTITUDE (MSL)",
			'isEditable': true
		},{
			'fieldName': "VERTICES",
			'label': "VERTICES",
			'isEditable': false
		}/*,{
			'fieldName': "USER_GROUP",
			'label': "USER GROUP",
			'isEditable': false
		},{
			'fieldName': "SYMBOL",
			'label': "SYMBOL",
			'isEditable': true
		}*/],
		//user polygon features
		polygons: [{
			'fieldName': "NAME",
			'label': "NAME",
			'isEditable': true,
			'customField': this.validName
		},{
			'fieldName': "DESCRIPTION",
			'label': "DESCRIPTION",
			'isEditable': true
		},{
			'fieldName': "REF_NUMBER",
			'label': "REFERENCE NUMBER",
			'isEditable': false
		},{
			'fieldName': "SUP_NOTES",
			'label': "SUPPLEMENTAL NOTES",
			'isEditable': true
		},{
			'fieldName': "FS_DESIGN",
			'label': "FILTER SHAPE DESIGNATION",
			'isEditable': true
		},{
			'fieldName': "IWA_DESIGN",
			'label': "IWA DESIGNATION",
			'isEditable': true
		},{
			'fieldName': "MIN_A_AGL",
			'label': "MINIMUM ALTITUDE (AGL)",
			'isEditable': true
		},{
			'fieldName': "MIN_A_MSL",
			'label': "MINIMUM ALTITUDE (MSL)",
			'isEditable': true
		},{
			'fieldName': "MAX_A_AGL",
			'label': "MAXIMUM ALTITUDE (AGL)",
			'isEditable': true
		},{
			'fieldName': "MAX_A_MSL",
			'label': "MAXIMUM ALTITUDE (MSL)",
			'isEditable': true
		},{
			'fieldName': "VERTICES",
			'label': "VERTICES",
			'isEditable': false
		}/*,{
			'fieldName': "USER_GROUP",
			'label': "USER GROUP",
			'isEditable': false
		}
		,{
			'fieldName': "SYMBOL",
			'label': "SYMBOL",
			'isEditable': true
		}
		*/],
		//tfr points
		tfr_points: [{
			'fieldName': "NAME",
			'label': "NAME",
			'isEditable': true
		},{
			'fieldName': "DESCRIPT",
			'label': "DESCRIPTION",
			'isEditable': true
		},/*{
			'fieldName': "NOTES",
			'label': "NOTES",
			'isEditable': true
		},{
			'fieldName': "TFR_ID",
			'label': "TFR ID",
			'isEditable': false
		},*/{
			'fieldName': "TFR_SHAPE",
			'label': "TFR SHAPE",
			'isEditable': true
		},{
			'fieldName': "FLOOR",
			'label': "FLOOR ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "IN_FLOOR",
			'label': "INCLUDE FLOOR ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "FL_TYPE",
			'label': "FLOOR ALTITUDE TYPE",
			'isEditable': true
		},{
			'fieldName': "CEILING",
			'label': "CEILING",
			'isEditable': true
		},{
			'fieldName': "IN_CEIL",
			'label': "INCLUDE CEILING ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "CEIL_TYPE",
			'label': "CEILING ALTITUDE TYPE",
			'isEditable': true
		}/*,{
			'fieldName': "GEOM_TYPE",
			'label': "GEOMETRY TYPE",
			'isEditable': false
		},{
			'fieldName': "VERTICES",
			'label': "VERTICES",
			'isEditable': false
		},{
			'fieldName': "USER_GROUP",
			'label': "ACCESS GROUP",
			'isEditable': false
		}*/],
		//tfr lines
		tfr_polylines: [{
			'fieldName': "NAME",
			'label': "NAME",
			'isEditable': true
		},{
			'fieldName': "DESCRIPT",
			'label': "DESCRIPTION",
			'isEditable': true
		},/*{
			'fieldName': "NOTES",
			'label': "NOTES",
			'isEditable': true
		},{
			'fieldName': "TFR_ID",
			'label': "TFR ID",
			'isEditable': false
		},*/{
			'fieldName': "TFR_SHAPE",
			'label': "TFR SHAPE",
			'isEditable': true
		},{
			'fieldName': "FLOOR",
			'label': "FLOOR ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "IN_FLOOR",
			'label': "INCLUDE FLOOR ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "FL_TYPE",
			'label': "FLOOR ALTITUDE TYPE",
			'isEditable': true
		},{
			'fieldName': "CEILING",
			'label': "CEILING",
			'isEditable': true
		},{
			'fieldName': "IN_CEIL",
			'label': "INCLUDE CEILING ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "CEIL_TYPE",
			'label': "CEILING ALTITUDE TYPE",
			'isEditable': true
		}/*,{
			'fieldName': "GEOM_TYPE",
			'label': "GEOMETRY TYPE",
			'isEditable': false
		},{
			'fieldName': "VERTICES",
			'label': "VERTICES",
			'isEditable': false
		},{
			'fieldName': "USER_GROUP",
			'label': "ACCESS GROUP",
			'isEditable': false
		}*/],
		//tfr polygons
		tfr_polygons: [{
			'fieldName': "NAME",
			'label': "NAME",
			'isEditable': true
		},{
			'fieldName': "DESCRIPT",
			'label': "DESCRIPTION",
			'isEditable': true
		},/*{
			'fieldName': "NOTES",
			'label': "NOTES",
			'isEditable': true
		},{
			'fieldName': "TFR_ID",
			'label': "TFR ID",
			'isEditable': false
		},*/{
			'fieldName': "TFR_SHAPE",
			'label': "TFR SHAPE",
			'isEditable': true
		},{
			'fieldName': "FLOOR",
			'label': "FLOOR ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "IN_FLOOR",
			'label': "INCLUDE FLOOR ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "FL_TYPE",
			'label': "FLOOR ALTITUDE TYPE",
			'isEditable': true
		},{
			'fieldName': "CEILING",
			'label': "CEILING",
			'isEditable': true
		},{
			'fieldName': "IN_CEIL",
			'label': "INCLUDE CEILING ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "CEIL_TYPE",
			'label': "CEILING ALTITUDE TYPE",
			'isEditable': true
		}/*,{
			'fieldName': "GEOM_TYPE",
			'label': "GEOMETRY TYPE",
			'isEditable': false
		},{
			'fieldName': "VERTICES",
			'label': "VERTICES",
			'isEditable': false
		},{
			'fieldName': "USER_GROUP",
			'label': "ACCESS GROUP",
			'isEditable': false
		}*/],
		//airport features
		airports: [{
			'fieldName': "FAA_ID",
			'label': "FAA ID",
			'isEditable': true
		},{
			'fieldName': "ICAO_ID",
			'label': "ICAO ID",
			'isEditable': true
		},{
			'fieldName': "LG_NAME",
			'label': "LONG NAME",
			'isEditable': true
		},{
			'fieldName': "BASIC_USE",
			'label': "BASIC USE",
			'isEditable': true
		},{
			'fieldName': "ARP_CLASS",
			'label': "AIRPORT CLASSIFICATION",
			'isEditable': true
		},{
			'fieldName': "OVR_STATUS",
			'label': "OVERALL STATUS",
			'isEditable': true
		}
		/* NISIS-2744 - KOFI HONU clean up READINESS ,{
			'fieldName': "CRT_RL",
			'label': "CURRENT READINESS LEVEL",
			'isEditable': true
		}/*,{ //hidden field
			'fieldName': "SYMBOLOGY",
			'label': "SYMBOL",
			'isEditable': true
		}*/],
		//atm features
		atm: [{
			'fieldName': "ATM_ID",
			'label': "ATM ID",
			'isEditable': true
		},{
			'fieldName': "ARP_ID",
			'label': "AIRPORT ID",
			'isEditable': true
		},{
			'fieldName': "LG_NAME",
			'label': "LONG NAME",
			'isEditable': true
		},{
			'fieldName': "BASIC_TP",
			'label': "BASIC TYPE",
			'isEditable': true
		},{
			'fieldName': "OPS_STATUS",
			'label': "OPERATIONS STATUS",
			'isEditable': true
		},{
			'fieldName': "CRT_RL",
			'label': "CURRENT READINESS LEVEL",
			'isEditable': true
		}],
		//ans features
		ans: [{
			'fieldName': "ANS_ID",
			'label': "ANS_ID",
			'isEditable': true
		},{
			'fieldName': "FSEP_ID",
			'label': "FSEP ID",
			'isEditable': true
		},{
			'fieldName': "BASIC_TP",
			'label': "BASIC TYPE",
			'isEditable': true
		},{
			'fieldName': "BASIC_CAT",
			'label': "BASIC CATEGORY",
			'isEditable': true
		},{
			'fieldName': "DESCRIPT",
			'label': "DESCRIPTION",
			'isEditable': true
		},{
			'fieldName': "ANS_STATUS",
			'label': "ANS SYSTEM STATUS",
			'isEditable': true
		},{
			'fieldName': "NAPRS_REPORTABLE",
			'label': "NAPRS REPORTABLE",
			'isEditable': true
		}],
		//osf features
		osf: [{
			'fieldName': "OSF_ID",
			'label': "OSF ID",
			'isEditable': true
		},{
			'fieldName': "LG_NAME",
			'label': "LONG NAME",
			'isEditable': true
		},{
			'fieldName': "CRT_RL",
			'label': "CURRENT READINESS LEVEL",
			'isEditable': true
		},{
			'fieldName': "BOPS_STS",
			'label': "BASIC OPERATIONS STATUS",
			'isEditable': true
		}],
		// NISIS-2754 KOFI HONU
		//tof features
		tof: [{
			'fieldName': "TOF_ID",
			'label': "TOF_ID",
			'isEditable': true
		},{
			'fieldName': "LONG_NAME",
			'label': "LONG NAME",
			'isEditable': true
		},{
			'fieldName': "CRT_RL",
			'label': "CURRENT READINESS LEVEL",
			'isEditable': true
		},{
			'fieldName': "OPS_STATUS",
			'label': "BASIC OPERATIONS STATUS",
			'isEditable': true
		}],
		//response teams features
		// teams: [{
		// 	'fieldName': "RES_ID",
		// 	'label': "RES ID",
		// 	'isEditable': true
		// },{
		// 	'fieldName': "LG_NAME",
		// 	'label': "LONG NAME",
		// 	'isEditable': true
		// },{
		// 	'fieldName': "RES_STS",
		// 	'label': "RESPONSE TEAM STATUS",
		// 	'isEditable': true
		// }],
		//adapt tracks
		tracks: [{
			'fieldName': "CALLSIGN",
			'label': "CALLSIGN",
			'isEditable': true
		},{
			'fieldName': "ALTITUDE",
			'label': "ALTITUDE",
			'isEditable': true
		},{
			'fieldName': "DEPT",
			'label': "DEPARTURE",
			'isEditable': true
		},{
			'fieldName': "DEST",
			'label': "DESTINATION",
			'isEditable': true
		},{
			'fieldName': "SPEED",
			'label': "SPEED (KNOTS)",
			'isEditable': true
		},{
			'fieldName': "BEACON",
			'label': "BEACON",
			'isEditable': true
		}]
	};
});
