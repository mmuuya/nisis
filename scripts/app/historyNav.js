define(function(){
    return {
        openFieldHistory: function(facId, fieldType, label, explanationType) {
            var historyGrid = $("#historyTable").data('kendoGrid');
                var dataSource = historyGrid.dataSource;
                var historyWindow = $('#history-div').data('kendoWindow');
                nisis.historyObject = {};
                nisis.historyObject.facId = facId;
                nisis.historyObject.fieldType = fieldType;
                dataSource.filter([]);
                $("#iwl-multiselect").data("kendoMultiSelect").value([]);
                $("#shape-filter-multiselect").data("kendoMultiSelect").value([]);
                $("#history-multiselect-filters").hide();
                dataSource.page(1);
                dataSource.read();
                historyGrid.hideColumn(0);
                historyGrid.hideColumn(1);
                if(explanationType==="supplemental"){
                    $("#history-div th[data-field=EXPLANATION]").html("Supplemental Notes");
                    historyGrid.hideColumn(2);
                }else if(explanationType==="explanation"){
                    $("#history-div th[data-field=EXPLANATION]").html("Status Explanation");
                    historyGrid.showColumn(2);
                }else{
                    //shouldn't hit this, but here for a fail safe
                    $("#history-div th[data-field=EXPLANATION]").html("Status Explanation/Supplemental Notes"); 
                    historyGrid.showColumn(2);
                }
                $("#searchbox").val('');
                if($("#history-filter-FAC_ID").data("kendoMultiSelect")){
                    $("#history-filter-FAC_ID").data("kendoMultiSelect").value({});
                    $("#history-filter-FAC_ID").data("kendoMultiSelect").dataSource.read();
                }
                if($("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect")){
                    $("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect").value({});
                    $("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect").dataSource.read();
                }
                if($("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect")){
                    $("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect").value({});
                    $("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect").dataSource.read();
                }
                if($("#history-filter-USERID").data("kendoMultiSelect")){
                    $("#history-filter-USERID").data("kendoMultiSelect").value({});
                    $("#history-filter-USERID").data("kendoMultiSelect").dataSource.read();
                }
                historyWindow.setOptions({
                    title: label + " History - " + facId,
                    width:800
                });
                historyWindow.restore();
                historyWindow.center().open();
        },

        openFacilityHistory: function(facId){
            var historyGrid = $("#historyTable").data('kendoGrid');
            var dataSource = historyGrid.dataSource;
            var historyWindow = $('#history-div').data('kendoWindow');
            nisis.historyObject = {};
            nisis.historyObject.facId = facId;
            dataSource.filter([]);
            $("#iwl-multiselect").data("kendoMultiSelect").value([]);
            $("#shape-filter-multiselect").data("kendoMultiSelect").value([]);
            $("#history-multiselect-filters").hide();
            dataSource.page(1);
            dataSource.read();
            historyGrid.hideColumn(0);
            historyGrid.showColumn(1);
            historyGrid.showColumn(2);
            $("#searchbox").val('');
            if($("#history-filter-FAC_ID").data("kendoMultiSelect")){
                $("#history-filter-FAC_ID").data("kendoMultiSelect").value({});
                $("#history-filter-FAC_ID").data("kendoMultiSelect").dataSource.read();
            }
            if($("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect")){
                $("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect").value({});
                $("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect").dataSource.read();
            }
            if($("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect")){
                $("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect").value({});
                $("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect").dataSource.read();
            }
            if($("#history-filter-USERID").data("kendoMultiSelect")){
                $("#history-filter-USERID").data("kendoMultiSelect").value({});
                $("#history-filter-USERID").data("kendoMultiSelect").dataSource.read();
            }
            $("#history-div th[data-field=EXPLANATION]").html("Status Explanation/Supplemental Notes");
            historyWindow.setOptions({
                title: "Facility History - " + facId,
                width:900
            });
            historyWindow.restore();
            historyWindow.center().open();
        },

        openMasterHistory: function(){
            var historyGrid = $("#historyTable").data('kendoGrid');
                var dataSource = historyGrid.dataSource;
                var historyWindow = $('#history-div').data('kendoWindow');
                $("#history-div th[data-field=EXPLANATION]").html("Status Explanation/Supplemental Notes");
                //Resetting the historyObject to be an empty object in order to delete facId and fieldType from it
                nisis.historyObject = {};
                historyGrid.showColumn(0);
                historyGrid.showColumn(1);
                historyGrid.showColumn(2);
                dataSource.filter([]);
                $("#iwl-multiselect").data("kendoMultiSelect").value([]);
                $("#iwl-multiselect").data("kendoMultiSelect").dataSource.read();
                $("#shape-filter-multiselect").data("kendoMultiSelect").value([]);
                $("#shape-filter-multiselect").data("kendoMultiSelect").dataSource.read();
                $("#history-multiselect-filters").show();
                dataSource.read();
                dataSource.page(1);
                //refresh grid filters
                if($("#history-filter-FAC_ID").data("kendoMultiSelect")){
                    $("#history-filter-FAC_ID").data("kendoMultiSelect").value({});
                    $("#history-filter-FAC_ID").data("kendoMultiSelect").dataSource.read();
                }
                if($("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect")){
                    $("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect").value({});
                    $("#history-filter-FIELD_DISPLAY_NM").data("kendoMultiSelect").dataSource.read();
                }
                if($("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect")){
                    $("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect").value({});
                    $("#history-filter-STATUS_DISPLAY").data("kendoMultiSelect").dataSource.read();
                }
                if($("#history-filter-USERID").data("kendoMultiSelect")){
                    $("#history-filter-USERID").data("kendoMultiSelect").value({});
                    $("#history-filter-USERID").data("kendoMultiSelect").dataSource.read();
                }
                $("#searchbox").val('');

                historyWindow.setOptions({
                    title: "Master History Log",
                    width:1000
                });
                historyWindow.restore();
                historyWindow.center().open();
        }
    };
});