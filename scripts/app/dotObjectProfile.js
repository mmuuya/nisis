//define([],function(){

//NISIS Facility Profile ViewModel
define(["app/appUtils", "esri/graphic",  "esri/geometry/Polyline", "esri/symbols/SimpleLineSymbol", "esri/geometry/Point", "esri/SpatialReference", "esri/geometry/webMercatorUtils", "esri/geometry/screenUtils", "esri/geometry/ScreenPoint", "app/windows/shapetools", "app/windows/tableview","app/historyNav"], 
    function(appUtils, Graphic, Polyline, SimpleLineSymbol, Point, SpatialReference, webMercatorUtils, screenUtils, ScreenPoint, shapetools, tableview,historyNav){

DOTObjectProfile = function(profileData, evt, objectId) {
    //this.statusesPicklist_ = resp.statusesPicklist;
    this.content=profileData;
    
    this.title_ = profileData["LAYERINFO"]["ROW"]["LAYER_LONG_NAME"] || "";
    this.screenPoint_ = evt.screenPoint || ""; 
    this.mapPoint_ = evt.mapPoint || "";
    this.graphicId_ = evt.graphic || "";    
    if (objectId) {
      this.objectId_ = objectId;
    }
    else {
      this.objectId_ = this.graphicId_.attributes.OBJECTID;
    }

    this.line_=null;    
  
    var observable = kendo.observable({
               tableName: profileData["LAYERINFO"]["ROW"]["TABLE_NAME"],
               objectId:this.objectId_, 
               fields:{},
               fieldsChanged:new Object(),               
               explanation: null, 
               note: null});


    //put data-bind fields into observable
    profileData["METADATA"].forEach(function(metaDataItem){
        observable.fields[metaDataItem["COL_NM"]]=profileData["DATA"]["ROW"][metaDataItem["COL_NM"]];
    });

    observable.bind("set", function(e) {  
        var observable = e.sender;

        console.log("observable: " + e.field);   
        var columnName=e.field;
        columnName = columnName.replace("fields.", "");  
        console.log("observable vaue " + value);

        observable.fieldsChanged[columnName]=e.value;

        //find our window and put up dirty form warning
        var dotObjectId = "dotObject_" + observable.tableName + observable.objectId;  
        var $kendoInfoWindow = $("#" + dotObjectId);       
        if ($kendoInfoWindow.length==1) {
            $kendoInfoWindow.find("div.dotProfileDirtyForm").show();            
        }         
    });
    
    this.observable_ = observable;
    this.draw();
};

DOTObjectProfile.prototype = {
    tip_ : 22,
    animationDuration_ : 100,
    div_ : null,   

    getContent: function() {
        return this.content_;
    },
  
    findProfile: function(dotObjectProfileWindowId) {
        var $kendoInfoWindow = $("#" + dotObjectProfileWindowId);    
        if ($kendoInfoWindow.length==1) {
            return $kendoInfoWindow.data('self');        
        } 
        return null;
    },   
    showDirtyFormIndicator: function() {       
        var dotObjectId = "dotObject_" + this.observable_.tableName + this.observable_.objectId;  
        var $kendoInfoWindow = $("#" + dotObjectId);   
        if ($kendoInfoWindow.length==1) {
              var $dirtyFormIndicator = $kendoInfoWindow.find("div.dotProfileDirtyForm");
              $dirtyFormIndicator.show();     
        } 
        
    },  

    history: function() {
           
    },

    mapExtendChangeHandler: function(evt) {
        var extent = evt.extent, zoomed = evt.levelChange;

        $(".dot-profile-wrapper").each(function(ndx) {
          var $dotWindow = $( this ).find("div.kendoInfoWindow");
          if ($dotWindow.length>0) {          
              var self = $dotWindow.data('self')
              if (self.line_  != null) {
                  //remove the self.line from the map
                  nisis.ui.mapview.map.graphics.remove(self.line_);
                  //delete the line for now, then redraw it.
                  self.drawLineBetweenPoints(self); 
              }
          }
        });        
    },  
    drawLine: function(evt) {
        //get the dot object window object    
        var self = $(evt.sender.element.context).data('self');     
        self.drawLineBetweenPoints(self);      
    },    
    drawLineBetweenPoints: function(self) {                
        //draw line between the DOT Object graphic and its corresponding profile window
        var graphicMapPoint = [], arrayOfCorners=[], path = [], $profileWindow, dotObjectId, 
            profileWindowOffset, profileWindowWidth, profileWindowHeight, screenPointTopLeft, screenPointTopRight, screenPointBottomLeft, screenPointBottomRight, 
            graphicsScreenPoint, profileWindowScreenPoint, profileWindowClosestMapPoint, mapProfileWindowPoint=[],
            line, polyline, polylineSymbol;

        graphicMapPoint.push(self.mapPoint_.x);
        graphicMapPoint.push(self.mapPoint_.y);
      
        //find our window
        dotObjectId = "dotObject_" + self.observable_.tableName + self.objectId_;       
        if ($("#" + dotObjectId).length==0 || $("#" + dotObjectId).parents(".dot-profile-wrapper").length==0) {
            //hm... not there?           
            console.log("drawLine - unable to find dot Object Profile Window" + dotObjectId);
            return;
        }
       
        $profileWindow = $("#" + dotObjectId).parents(".dot-profile-wrapper");
        profileWindowOffset =  $profileWindow.offset();
        profileWindowWidth = $profileWindow.outerWidth();         
        profileWindowHeight = $profileWindow.outerHeight(); 
       
        screenPointTopLeft = new Point(profileWindowOffset.left, profileWindowOffset.top, nisis.ui.mapview.map.spatialReference);
        screenPointTopRight = new Point(profileWindowOffset.left+profileWindowWidth, profileWindowOffset.top, nisis.ui.mapview.map.spatialReference);
        screenPointBottomLeft = new Point(profileWindowOffset.left, profileWindowOffset.top+profileWindowHeight, nisis.ui.mapview.map.spatialReference);
        screenPointBottomRight = new Point(profileWindowOffset.left+profileWindowWidth, profileWindowOffset.top+profileWindowHeight, nisis.ui.mapview.map.spatialReference);
       
        arrayOfCorners.push(screenPointTopLeft);
        arrayOfCorners.push(screenPointTopRight);
        arrayOfCorners.push(screenPointBottomLeft);
        arrayOfCorners.push(screenPointBottomRight);
        //convert to screenpoint 
        graphicsScreenPoint = esri.geometry.toScreenPoint(nisis.ui.mapview.map.extent, nisis.ui.mapview.map.width, nisis.ui.mapview.map.height, self.mapPoint_);

        //find closest Point
        profileWindowScreenPoint =  this.findClosest(arrayOfCorners, graphicsScreenPoint);
        //convert from screen point to map point 
        profileWindowClosestMapPoint = nisis.ui.mapview.map.toMap(profileWindowScreenPoint);     
       
        mapProfileWindowPoint.push(profileWindowClosestMapPoint.x);
        mapProfileWindowPoint.push(profileWindowClosestMapPoint.y);

        //got points calculated, now just draw                      
        path.push(graphicMapPoint);
        path.push(mapProfileWindowPoint);
        polyline = new Polyline(nisis.ui.mapview.map.spatialReference);
        polyline.addPath(path);

        // create new Polyline Symbol        
        polylineSymbol = new SimpleLineSymbol();         
        polylineSymbol.setStyle(SimpleLineSymbol.STYLE_SHORTDASH);
        polylineSymbol.setColor(new dojo.Color([13, 121, 190, .9]));
        polylineSymbol.setWidth(3);       

        line = new Graphic(polyline, polylineSymbol);
        if (self.line_  != null) {
            //remove the self.line from the map
            nisis.ui.mapview.map.graphics.remove(self.line_);
        }
          
        //add line to self and draw on the map       
        self.line_ = line;
        nisis.ui.mapview.map.graphics.add(self.line_);
    }, 

    findClosest: function(a, point) {
        var lowestDistance = Number.MAX_SAFE_INTEGER;
        var closestPoint=0;
        var distance=0;
        for (var i = 0; i < a.length; i++) {
          distance = this.distance(a[i], point) ;
          if (distance < lowestDistance) {
              closestPoint = a[i]; 
              lowestDistance = distance;
          } 
        }
        return closestPoint;
    },

    distance: function(point1, point2) {
        var xs = 0, ys = 0;         
        xs = point2.x - point1.x;
        xs = xs * xs;
         
        ys = point2.y - point1.y;
        ys = ys * ys;
                
        return Math.sqrt( xs + ys );     
    },
       
    draw: function() {       
        var self =this;
     
        //see if window already exists - set focus on it if it does
        var dotObjectId = "dotObject_" + this.observable_.tableName + this.objectId_;       
        if ($("#" + dotObjectId).length==1) {
            $("#" + dotObjectId).focus();
            return;
        }
        //convert tip to negative
        var kendoHeaderOffset=~this.tip_+1;  
       
        //then set up the window                      
        kendoInfoWindow = $("<div class='kendoInfoWindow ' id='"+ dotObjectId+"'></div>").kendoWindow({  
                              modal: false,
                              title: this.title_,
                              width: "540px",
                              maxHeight: 600,
                              resizable: false,
                              //draggable: false,
                              //pinned: true,
                              actions: ['Close'],                              
                              position: {
                                 left:  this.screenPoint_.x + "px",
                                 top:  this.screenPoint_.y + kendoHeaderOffset + "px"                                                               
                              },                 
                              animation: {
                                open: {
                                  duration: this.animationDuration_
                                }
                              },             
                              close: function(e) {                                
                                if (self.line_  != null) {
                                    //remove the self.line from the map
                                    nisis.ui.mapview.map.graphics.remove(self.line_);
                                }
                                this.destroy(); 
                                //refreshes layers and map using map.setExtent(map.extent) function
                                nisis.ui.mapview.map.setExtent(nisis.ui.mapview.map.extent); 
                              }                 
        }); 
        //remember it, save it in self data attribute
        $(kendoInfoWindow).data('self', this); 
      
        //handle drag
        kendoInfoWindow.data("kendoWindow").dragging._draggable.bind("dragend", this.drawLine);        
        var mapExtentChange = nisis.ui.mapview.map.on("extent-change", this.mapExtendChangeHandler);
       

        //add unsaved changes
        $("<div class='dotProfileDirtyForm' style='display:none'> You have unsaved changes. </div>").appendTo(kendoInfoWindow);  

        //dot specific profile function
        var dotForm = this.buildDOTForm();


        $("<br/><br/>").appendTo(dotForm);

        var centeredWrapper  =  $("<div class='centeredWrapper'/>");  
        var saveBtn = $("<span><i class='fa fa-floppy-o' title='Save'></i> Save</span>").kendoButton({click: this.onSave }).appendTo(centeredWrapper);  
        var content = this.content;
        var historyBtn = $("<span><i class='fa' title='Save'></i>History</span>").kendoButton({click: function(evt){historyNav.openFacilityHistory(content["DATA"]["ROW"][content["LAYERINFO"]["ROW"]["FACID_COL"]])},imageUrl: "images/icons/history.png" }).appendTo(centeredWrapper); 
        //only for DOT profile objects that are polygons: add on change status feature for adjacent objects
        if (self.graphicId_.geometry.type == "polyline") {
          var saveAdjucentObjectsBtnHTML = "<span data-tool='freehandpolygon' data-value='1' data-symbol='polygon' data-dotobjectid='" +dotObjectId+"' class='adjacentBtn'><i class='fa fa-sitemap' title='Select adjacent objects'></i> Include adjacent objects</span>";
          $(saveAdjucentObjectsBtnHTML).kendoButton({click: this.onSaveStatusToAdjacentObjects }).addClass("k-state-disabled").appendTo(centeredWrapper);  
        }
        var zoomBtn = $("<span class='dotProfileLabel'><i class='fa fa-eye' title='Zoom To'></i> Zoom To</span>").kendoButton({click: this.onZoom }).appendTo(centeredWrapper);
        centeredWrapper.appendTo(dotForm);

        var lastEditedMark = $("<div class='left'/>");
        lastEditedMark.html("Last edited by "+content["DATA"]["ROW"]["LAST_EDITED_USER"]+" on "+content["DATA"]["ROW"]["LAST_EDITED_DATE"]+"Z");
        lastEditedMark.appendTo(dotForm);

        dotForm.appendTo(kendoInfoWindow);


        //add dot-profile-wrapper class to the closest k-widget k-window        
        $(kendoInfoWindow.data('kendoWindow').wrapper).addClass("dot-profile-wrapper");

        //bind the observable
        kendo.bind(dotForm, this.observable_);
        //see if the kendo window will drop behind the map, if it will, center it and draw a line to it
        if (this.screenPoint_.y  + kendoInfoWindow.data('kendoWindow').wrapper.height() > nisis.ui.mapview.map.height) {           
            //open the window
            kendoInfoWindow = kendoInfoWindow.data('kendoWindow').center().open();            
            
            //draw the line
            //wait until kendo finishes window animation so the window coordinates are all set. Animation set to 100, give it 100 more
            setTimeout(function(){
              self.drawLineBetweenPoints(self);   
            }, self.animationDuration_+100);     
        }
        else {        
            //open the window next to graphic
            kendoInfoWindow.data('kendoWindow').open();            
        }                
        

    },

    buildDOTForm: function(){
        //start form       
        var dotForm = $("<div class='dot_profile_form'> </div>");
        var content=this.content;
        var self = this;
       
        content["METADATA"].forEach(function(metadata){
            if(metadata["COL_NM"]!=="LAST_EDITED_DATE"&&metadata["COL_NM"]!=="LAST_EDITED_USER"&&metadata["COL_NM"]!=="IM_ROLE"&&metadata["COL_NM"]!=="RTG_CODE"&&metadata["COL_NM"]!=="OBJECTID"){
                if(metadata["PERM"]==="r"){
                    //read only options
                    if(metadata["PICKLIST"]==="" || metadata["PICKLIST"]==null){
                        //fields that are not a picklist
                        if(metadata["ITEM_STATUS_TYPE"]===null){
                            //fields that do not require a signature
                            var readOnlySpan=self.buildReadOnlySpan(metadata);
                        }else{
                            //fields that do require a signature
                            var readOnlySpan=self.buildReadOnlySpanWithSignature(metadata);
                        }
                    }else{
                        //fields that are a picklist
                        var readOnlySpan=self.buildReadOnlyPicklist(metadata);
                    }
                    readOnlySpan.appendTo(dotForm);
                }else if(metadata["PERM"]==="w"){
                    //write permission options
                    if(metadata["PICKLIST"]==="" || metadata["PICKLIST"]==null){
                        //not a picklist
                        if(metadata["ITEM_STATUS_TYPE"]!==null&&metadata["ITEM_STATUS_TYPE"].split(".")[0]==="EX"){
                            //explanation
                            var writePermissionGroup = self.buildExplanation(metadata);
                        }else if(metadata["ITEM_STATUS_TYPE"]==="SN"){
                            //supplemental note
                            var writePermissionGroup = self.buildSupplementalNote(metadata);
                        }else{
                            //textbox
                            var writePermissionGroup = self.buildTextBox(metadata);
                        }
                    }else{
                        //picklist
                        if(metadata["ITEM_STATUS_TYPE"]!==null&&metadata["ITEM_STATUS_TYPE"].split(".")[0]==="ST"){
                            //status picklist
                            var writePermissionGroup = self.buildStatusDropdown(metadata);
                        }else{
                            //non-status picklist
                            var writePermissionGroup = self.buildPicklist(metadata);
                        }
                    }
                    writePermissionGroup.appendTo(dotForm);
                }
            }
        });
        return dotForm;
    },

    buildReadOnlySpanWithSignature: function(metadata){
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var readOnlySpan = $("<span type='text' class='k-textbox read-only'/>");
        var explanation = this.buildFieldTextWithSignature(metadata["COL_NM"]);
        readOnlySpan.html(explanation);
        readOnlySpan.appendTo(formGroup);
        return formGroup;
    },

    buildReadOnlySpan: function(metadata){
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var readOnlySpan = $("<span type='text' class='k-textbox read-only'/>");
        readOnlySpan.html(this.content["DATA"]["ROW"][metadata["COL_NM"]]);
        readOnlySpan.appendTo(formGroup);
        return formGroup;
    },

    buildReadOnlyPicklist: function(metadata){
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var dropdown = $("<select data-bind='value: fields."+metadata["COL_NM"]+"' class='non-status-dropdown'></select>");
        dropdown.appendTo(formGroup);
        dropdown.kendoDropDownList({
            dataTextField: "LABEL",
            dataValueField: "VALUE",
            valueTemplate: '<span class="selected-value statusImages" style="background-image: url(\'images/dropdowns/#:LABEL#.png\')"></span><span>#:LABEL#</span>',
            template: '<span class="k-state-default statusImages" style="background-image: url(\'images/dropdowns/#:LABEL#.png\')"></span><span class="k-state-default" >#: LABEL #</span>',
            dataSource: metadata["PICKLIST"]
        });
        dropdown.data("kendoDropDownList").readonly();
        return formGroup;
    },

    buildStatusDropdown: function(metadata){
        var content = this.content;
        var self=this;
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var dropdown = $("<select id='status-"+metadata["COL_NM"]+"' data-bind='value: fields."+metadata["COL_NM"]+"' class='status-dropdown'></select>");
        dropdown.appendTo(formGroup);
        dropdown.kendoDropDownList({
            dataTextField: "LABEL",
            dataValueField: "VALUE",
            select: this.statusExplanationChange, //send over explanation binding
            valueTemplate: '<span class="selected-value statusImages" style="background-image: url(\'images/dropdowns/#:LABEL#.png\')"></span><span>#:LABEL#</span>',
            template: '<span class="k-state-default statusImages" style="background-image: url(\'images/dropdowns/#:LABEL#.png\')"></span><span class="k-state-default" >#: LABEL #</span>',
            dataSource: metadata["PICKLIST"],
            select: function(e){
                e.preventDefault();
                self.statusExplanationChange(e,metadata["ITEM_STATUS_TYPE"].split(".")[1],metadata["COL_NM"],e.item);
            }
        });
        var historyButton = $("<span><i class='fa' title='View History'></i> </span>").kendoButton({click: function(evt){historyNav.openFieldHistory(content["DATA"]["ROW"][content["LAYERINFO"]["ROW"]["FACID_COL"]],metadata["COL_NM"],content["LAYERINFO"]["ROW"]["LAYER_LONG_NAME"],"explanation")}, imageUrl: "images/icons/history.png" });
        historyButton.find('img').css({margin:0});
        historyButton.appendTo(formGroup);
        return formGroup;
    },

    buildExplanation: function(metadata){
        var self=this;
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var explanationSpan = $("<span id='status-explanation-"+metadata["COL_NM"]+"' type='text' class='k-textbox status-explanation'/>");
        explanationSpan.addClass("status-explanation-"+metadata["COL_NM"]);
        var explanation = this.buildFieldTextWithSignature(metadata["COL_NM"]);
        explanationSpan.html(explanation);
        explanationSpan.appendTo(formGroup);

        var edit = $("<span><i class='fa fa-pencil' style='padding:1px' title='Enter or modify status explanation'></i> </span>").kendoButton({click: function(evt){self.statusExplanationChange(evt,metadata["COL_NM"],metadata["ITEM_STATUS_TYPE"].split(".")[1])} });
        edit.appendTo(formGroup);
        return formGroup;
    },

    buildSupplementalNote: function(metadata){
        var content = this.content;
        var self=this;
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var explanationSpan = $("<span type='text' id='supplemental-notes-"+metadata["COL_NM"]+"' class='k-textbox status-supplemental-note'/>");
        //explanationSpan.addClass("supplemental-notes-"+metadata["COL_NM"]);
        var explanation = this.buildFieldTextWithSignature(metadata["COL_NM"]);
        explanationSpan.html(explanation);
        explanationSpan.appendTo(formGroup);

        var edit = $("<span><i class='fa fa-pencil' style='padding:1px' title='Enter or modify status explanation'></i> </span>").kendoButton({click: function(evt){self.openSupplementalNotesWindow(evt,metadata["COL_NM"])} });
        edit.appendTo(formGroup);

        var history = $("<span><i class='fa' title='View History'></i> </span>").kendoButton({click: function(evt){historyNav.openFieldHistory(content["DATA"]["ROW"][content["LAYERINFO"]["ROW"]["FACID_COL"]],metadata["COL_NM"],content["LAYERINFO"]["ROW"]["LAYER_LONG_NAME"],"supplemental")}, imageUrl: "images/icons/history.png" });
        history.find('img').css({margin:0});
        history.appendTo(formGroup); 
        return formGroup;
    },

    buildTextBox: function(metadata){
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var textbox = $("<input class='k-textbox' maxlength="+metadata["DATA_LENGTH"]+" type='text' placeholder='"+metadata["FIELD_DISPLAY_NM"]+"' data-bind='value:fields."+(metadata["COL_NM"]!=null?metadata["COL_NM"]:"")+"'/></div>");
        textbox.appendTo(formGroup);
        return formGroup;
    },
    
    buildPicklist: function(metadata){
        var formGroup = $("<div class='dot_form_group'><label>"+metadata["FIELD_DISPLAY_NM"]+"</label>");
        var dropdown = $("<select data-bind='value: fields."+metadata["COL_NM"]+"' class='non-status-dropdown'></select>");
        var observable = this.observable_;
        dropdown.appendTo(formGroup);
        dropdown.kendoDropDownList({
            dataTextField: "LABEL",
            dataValueField: "VALUE",
            valueTemplate: '<span class="selected-value"></span><span>#:LABEL#</span>',
            template: '<span class="k-state-default"></span><span class="k-state-default" >#: LABEL #</span>',
            dataSource: metadata["PICKLIST"],
            change: function(e){
                observable.fieldsChanged[metadata["COL_NM"]]=this.value();
            }
        });
        return formGroup;
    },

    buildFieldTextWithSignature: function(colName){
        var content = this.content;
        if(content["STATUSDATA"]&&content["STATUSDATA"][colName]){
            var status = content["STATUSDATA"][colName];
            var explanation = status["EXPLANATION"]+'<br /><div class="signature"> Entered by <b>'+ status["USERID"] + '</b> on <i>' + status["STATUS_DATE"] + 'Z</i></div>'
        }else{
            var explanation = "";
        }
        return explanation;
    },



    onZoom: function(evt) {        
        var map = nisis.ui.mapview.map;
        var zoom = map.getZoom()+1;

        //get the dot object window object            
        var self = $(evt.sender.wrapper).parents("div.kendoInfoWindow").data('self');       
        var selFeat = self.graphicId_;
        //zoom in to a point or to polyline
        if(selFeat.geometry.type == 'point'){
          map.centerAndZoom(selFeat.geometry,zoom);
        } else {
          map.setExtent(selFeat.geometry.getExtent(),true);
        }
    },
    onSave: function(evt) {
        console.log("save!"); 


        //get to the window and retrieve self so access to observable
        $domElement = $(evt.sender.wrapper);
        var $kendoInfoWindow = $domElement.parents("div.kendoInfoWindow");

        //see if our dirty form indicator is visible. If not, they just clicked on it without changing anything    
        var $dirtyFormIndicator = $kendoInfoWindow.find("div.dotProfileDirtyForm");
        if ($dirtyFormIndicator.is(':visible') == false) {
            return;
        }

        $dirtyFormIndicator.hide();

        //disable poly drawing button        
        //clear out adjacent Btn - back to be disabled
        $kendoInfoWindow.find("span.adjacentBtn").addClass("k-state-disabled");

        var self = $kendoInfoWindow.data('self');
        console.log("class: "+JSON.stringify(self.observable_.fields["CLASS"]));
        if (JSON.stringify(self.observable_.fieldsChanged).length>0) {          
          var changes =  JSON.stringify(self.observable_.fieldsChanged);        
          //ajax call to save whats in observable
          $.ajax({
              url: "api/",
              type: "POST",
              data: { 
                  "action": "save_dot_profile",
                  "dataType": "json",                
                  "changes": JSON.stringify(self.observable_.fieldsChanged),                                               
                  "objectid": self.objectId_,
                  "tablenm": self.observable_.tableName
              },
              success: function(data, status, req) {
                          if (data!=null) {
                            var resp = JSON.parse(data);
                            if(resp.result === "Success") {
                                console.log("Updated successfully");
                                self.saveStatusForAdjacentObjects(self);
                                //refresh the graphic only if it was STATUS CHANGE                            
                                if (self.observable_.fieldsChanged.hasOwnProperty(self.content["LAYERINFO"]["ROW"]["MAIN_STATUS_COL_NM"])) {
                                  //refresh layer                  
                                  var layer = self.graphicId_._layer;            
                                  //******layer and map refresh function is called at profile window 'CLOSE' event in the draw function                      
                                  //refreshes layer and map with 'include adjacent objects' button  
                                  nisis.ui.mapview.map.setExtent(nisis.ui.mapview.map.extent); 

                                }                                               
                            } else {
                                console.error("ERROR updating DOT Object Profile");
                            }
                          }
                          //clear out fields To Save
                          self.observable_.fieldsChanged=new Object();                          
                      },                                           
              error: function(req, status, err){
                          console.error("ERROR in ajax call to save DOT profile");
                      }
          });
        }

       

    },  
    saveStatusForAdjacentObjects: function(self) {
        var content = this.content;
        var adjacentStatus =  self.observable_.fieldsChanged[content["LAYERINFO"]["ROW"]["MAIN_STATUS_COL_NM"]]; 
        var dotObjectId = "dotObject_" + self.observable_.tableName + self.objectId_; 
        //for query
        var ids = [];
        var logic = "AND";    
        var options = {};          
        options.outFields =  ['*'];
        options.layer = self.graphicId_._layer; 
        options.returnGeom = true;

        var polygons = nisis.ui.mapview.map.getLayer('Polygon_Features');   
        //can go through polygons and look for attr with REF_NUMBER that matches this DOT Profile Object     
        var graphics = polygons.graphics;
        $.each(graphics,function(i,graphic) {                           
            var graphsAttrRefNumber = graphic.attributes['REF_NUMBER'];
            //see if its a match!

            if (graphsAttrRefNumber === dotObjectId) {
              //do the spatial query, apply criteria and change status                 
              options.geom = graphic.geometry;   
              //remove the graphic from the User Polygons Layer                                         
              if(polygons.applyEdits){
                  polygons.applyEdits(null,null,[graphic])
                    .then(function(d){                                                                                                            
                    },function(e){                                                      
                    console.log("ERROR, unable to remove graphics from the Polygon Layer");        
                    });
              }else{
                  polygons.remove(graphic);                                                          
              }                       
              nisis.ui.mapview.util.queryFeatures(options)
                  .then(function(data){                                      
                          console.log("Done with geo spatial query");
                          //looping through all others
                               var feats = data.features,
                                    len = feats.length,
                                    l = null;
                                    console.log(feats);
                                if(data.fields){
                                    l = data.fields.length;

                                    //gather up the objectids from feats
                                    var adjacentObjects = [];
                                    $.each(feats, function(idx,feat){
                                      adjacentObjects.push(Number(feat.attributes.OBJECTID));
                                    });
                                    
                                   
                                    $.ajax({
                                      url: "api/",
                                      type: "POST",
                                      data: {
                                          "action": "save_dot_adjacent",
                                          "dataType": "json",                
                                          "changes": adjacentObjects,   
                                          "tablenm": self.observable_.tableName,                                            
                                          "objectid": Number(self.objectId_),                                                          
                                          "status": adjacentStatus,
                                          "statuscolumn": content["LAYERINFO"]["ROW"]["MAIN_STATUS_COL_NM"]                 
                                      },
                                      success: function(data, status, req) {
                                                if (data!= null) {
                                                  var resp = JSON.parse(data);
                                                    if (resp!=null && resp.result == "SUCCESS") {                                                      
                                                        console.log("Updated successfully"+data);  
                                                    }   
                                                    else  {
                                                        nisis.ui.displayMessage("Unable to change status for multiple adjacent objects", 'error');
                                                        console.log("Unable to change status for multiple adjacent objects");  
                                                    }  
                                                  }     
                                                else {
                                                      nisis.ui.displayMessage("Unable to change status for multiple adjacent objects", 'error');
                                                      console.log("Unable to change status for multiple adjacent objects");                                                       
                                                }                                    
                                              },
                                      error: function(req, status, err){
                                                  console.error("ERROR in ajax call to save DOT profile");                                                  
                                                  nisis.ui.displayMessage(" Unable to change status for multiple objects", 'error');                                                  
                                              }
                                    });
                                } else {                                 
                                    console.log('DOTObjectProfile: There is no features that matches your geo query.');
                                }                       
                  },function(err){
                      console.log('Query tableview data error: ',err);                    
                    
                  },function(note){
                      console.log('More: ', note);    

              }); //end of QueryFeatures
            }//there is a geometry associated with this DOT Profile Window              
        }); //for each graphic in User Polygon Layer  
    },    
    onSaveStatusToAdjacentObjects: function(evt) {
        console.log("onSaveStatusToAdjacentObjects!"); 

        //do the same as hand polygon does
        //add dataset to currentTarget
        var evt2 = evt.event;
        shapetools.activateDrawing(evt2);
    },
    openSupplementalNotesWindow: function(evt, colName) {
        console.log("openSupplementalNotesWindow...");         
        var self = this;
        var observable = self.observable_ ;
        var content = self.content;
        var supplementalNotesSpan = $('#supplemental-notes-' + colName);
        console.log("sup notes: "+JSON.stringify(supplementalNotesSpan));

        var domElementToAttachTo =  $(evt.sender.element);
        domElementToAttachTo.append($("<div id='supplementalNotes'><textarea  maxlength=250 /><br/><button class='k-button'  id='supplementalNotesOKButton'>OK</button><button class='k-button'  id='supplementalNotesCancelButton'>Cancel</button></div>"));
        
        $("#supplementalNotes textarea").val(content["DATA"][colName]);  
            
        /* bind the click    */
        $("#supplementalNotesOKButton").click(function() {       
                   
                    //conjure up the signature and copy the whole thing into the status explanation span and database
                    //user name and DTG
                    var username = nisis.user.username;
                    var dtg = appUtils.formatDateTimeToUtcDTG(new Date(), "/", true);
                    var suppNotesUsersText=$("#supplementalNotes textarea").val();
                    var supplementalNotesTextWithSignature = "";
                    if (suppNotesUsersText!=="") {
                        supplementalNotesTextWithSignature = appUtils.formatTextWithSignature(suppNotesUsersText, username, dtg);                    
                    }                    
                    //copy the text and appended signature to span
                    supplementalNotesSpan.html(supplementalNotesTextWithSignature);//supplementalNotesTextWithSignature);
                  
                    //tell kendoui that span has been changed so data-bind and save works
                    observable.fieldsChanged[colName]= $("#supplementalNotes textarea").val().toString();  

                    //tell kendo ui to save last edited user                    
                    observable.fieldsChanged["LAST_EDITED_USER"]=username; 
                    
                    //instead of doing observable set/ trigger, just show the unsaved changes 
                    var kendoInfoWindow = domElementToAttachTo.parents("div.kendoInfoWindow");
                    kendoInfoWindow.find("div.dotProfileDirtyForm").show();   

                    //destroy the window
                    supplementalNotesWindow.destroy();         
        });

        $("#supplementalNotesCancelButton").click(function(){                       
                    //destroy the window
                    supplementalNotesWindow.destroy();                     
        });    

        //then set up the window, form, datasource with kendo
       supplementalNotesWindow = $("#supplementalNotes").kendoWindow({
            modal: true,
            title: "Please provide supplemental notes for status change",
            width: "500px",
            actions: ['Close']        
        }).data("kendoWindow").open().center();   
        
         //set the focus on the textarea
        $("#supplementalNotes textarea").focus();   
    },  
    statusExplanationChange:function(evt,explanationColName,statusColName,statusChange) {  
        console.log("statusExplanationChange...");
        var self = this;
        var observable = self.observable_ ;
        var statusExplanationSpan = $('#status-explanation-' + explanationColName);
        var domElementToAttachTo =  $(evt.sender.element);
        domElementToAttachTo.append($("<div id='statusExplanation'><textarea  maxlength=250 /><div id='statusExplanationError' style='display:none;' > </div><br/><button class='k-button'  id='statusExplanationOKButton'>OK</button><button class='k-button'  id='statusExplanationCancelButton'>Cancel</button></div>"));
        //preset the textarea only if they edited through the pencil and not the dropdown
        if (!statusChange) {
            $("#statusExplanation textarea").val(observable.explanation);
        }       

        /* bind the click    */
        $("#statusExplanationOKButton").click(function(){
            if ($("#statusExplanation textarea").val().length<6) {
                    $("#statusExplanationError").html("Status explanation must be at least 6 characters."); 
                    $("#statusExplanationError").show();
            }
            else if ($.trim($("#statusExplanation textarea").val()).length==0) {
                    $("#statusExplanationError").html("Status explanation can not be empty."); 
                    $("#statusExplanationError").show(); 
            }
            else {
                    //conjure up the signature and copy the whole thing into the status explanation span and database
                    //user name and DTG
                    var username = nisis.user.username;
                    var dtg = appUtils.formatDateTimeToUtcDTG(new Date(), "/", true);
                    var changeExplanationTextWithSignature = appUtils.formatTextWithSignature($("#statusExplanation textarea").val(), username, dtg);                    
                    statusExplanationSpan.html(changeExplanationTextWithSignature);
                
                    //instead of doing observable set/ trigger, just show the unsaved changes 
                    var kendoInfoWindow = domElementToAttachTo.parents("div.kendoInfoWindow");
                    kendoInfoWindow.find("div.dotProfileDirtyForm").show();  
               
                    observable.fieldsChanged[explanationColName]= $("#statusExplanation textarea").val().toString();                  
                    
                    //tell kendo ui to save last edited user                    
                    observable.fieldsChanged["LAST_EDITED_USER"]=username;                   

                    if (statusChange) {    
                        //select the item
                        var dropdownlist = $("#status-"+statusColName).data('kendoDropDownList');
                        dropdownlist.select(statusChange);                        
                        //tell kendoui that span has been changed so data-bind and save works                         
                        var selectedOption = $(dropdownlist.element).find(":selected").val();  

                        //should trigger: you have unsaved changes                                
                        observable.fieldsChanged[statusColName]= selectedOption;                         
                    }


                    //enable the adjacent object save button                    
                    kendoInfoWindow.find("span.adjacentBtn").removeClass("k-state-disabled");

                    //destroy the explanation window
                    statusExplanationWindow.destroy();                                     
            };
        });

        $("#statusExplanationCancelButton").click(function(){                       
                    //destroy the window
                    statusExplanationWindow.destroy();                              
                    
                    //event propagation has been stopped, do not select an item; let the select stay where it was           
        })   

        //then set up the window, form, datasource with kendo
        statusExplanationWindow = $("#statusExplanation").kendoWindow({
            modal: true,
            title: "Please provide status change explanation",
            width: "400px",
            actions: []                 
        }).data("kendoWindow").open().center();         

        //set the focus on the textarea
        $("#statusExplanation textarea").focus();      
    }
    
   
};


  
    return DOTObjectProfile;
});
