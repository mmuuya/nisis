define([],function() {

    var testData=new Array();
    var result;
    var count=0;
    var v,l,p;
    $.ajax({
        url: 'html/phpSymbols.php',
        type: 'post',
        success: function(res){
            function sym(val,lab,pat){
                this.value=val;
                this.label=lab;
                this.path=pat;
            }
            //console.log('YAY!');
            result=res;
            //console.log(result);
            while(result.length>1){
                if(count%3==0) v=result.substring(0,result.indexOf('*'));
                if(count%3==1) l=result.substring(0,result.indexOf('*'));
                if(count%3==2) p=result.substring(0,result.indexOf('*'));
                result=result.substring(result.indexOf('*')+1);
                if(count%3==2) testData.push(new sym(v,l,p));
                count++;
                //console.log(count);
            }
            return testData;
        },
        error: function(){console.log('Boo')}
    });
    return testData;
});