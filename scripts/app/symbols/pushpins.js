/*
*	Pushpin Symbols
*/

define(["dojo/_base/Color",
    "esri/symbols/SimpleMarkerSymbol","esri/symbols/PictureMarkerSymbol","esri/symbols/MarkerSymbol",
    "esri/symbols/SimpleLineSymbol","esri/symbols/LineSymbol","esri/symbols/CartographicLineSymbol",
    "esri/symbols/SimpleFillSymbol","esri/symbols/PictureFillSymbol","esri/symbols/FillSymbol",
    "esri/symbols/TextSymbol"],
	function(Color,
    	SimpleMarkerSymbol,PictureMarkerSymbol,MarkerSymbol,
    	SimpleLineSymbol,LineSymbol,CartographicLineSymbol,
    	SimpleFillSymbol,PictureFillSymbol,FillSymbol,
        TextSymbol){

    return {
        pushpin1: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        pushpin2: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        pushpin3: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        circle: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        square: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        diamond: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        pentagon: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        hexagon: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        triangle: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        star: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        plus: {
            name: "",
            description: "",
            path: "M5 0 L15 0 L12.5 2.5 L12.5 7.5 L15 10 L11 10 L10 20 L9 10 L5 10 L7.5 7.5 L7.5 2.5 L5 0 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        cross: {
            name: "cross",
            description: "Cross",
            path: "M0 7.5 L7.5 7.5 L7.5 0 L12.5 0 L12.5 7.5 L20 7.5 L20 12.5 L12.5 12.5 L12.5 20 L7.5 20 L7.5 12.5 L0 12.5 Z",
            symbol: function(){
                return new SimpleMarkerSymbol()
                .setPath(this.path)
                .setOffset(10,20);
            }
        },
        text: {
            name: 'text',
            description: 'Free Text Label',
            symbol: function(){
                var text = new TextSymbol()
                .setColor('#000');
                //console.log(text);
                text.font.setSize(15)
                text.font.setFamily('Arial')
                text.font.setWeight('bold')
                text.font.setStyle('normal')
                text.font.setDecoration('none');
                //console.log(text);
                return text;
            }
        }
    };
});