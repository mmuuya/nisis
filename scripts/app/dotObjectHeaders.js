define({
	getHeader: function(layerID,data){

		var buildStyle = function(headerInfo){
			var header = "";
			for(var x=0;x<headerInfo.length;x++){
				header+="<div class='dot_form_group'><label>"+headerInfo[x].title + "</label><input class='k-textbox' type='text' placeholder='"+headerInfo[x].title+"' data-bind='value:fields."+(headerInfo[x].bind!=null?headerInfo[x].bind:"")+"'/></div>";
			}
			return header;
		};

        var headerInfo = [];
		var jsonDOT = JSON.parse(data.dot)[0];            

		switch(layerID){
			case "ntad_amtrak":		  
				headerInfo.push({title:"Rail Owner",data:jsonDOT.RROWNER1, bind:'RROWNER1'});
				headerInfo.push({title:"POC",data:jsonDOT.POC_NAME, bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE, bind:'POC_PHONE'});
				headerInfo.push({title:"Miles",data:jsonDOT.MILES, bind:'MILES'});
				headerInfo.push({title:"State",data:jsonDOT.STATEAB, bind:'STATEAB'});
				headerInfo.push({title:"County",data:jsonDOT.CNTYFIPS, bind:'CNTYFIPS'});
				headerInfo.push({title:"Switches",data:jsonDOT.SWITCHES, bind:'SWITCHES'});
				header = buildStyle(headerInfo);
				break;
			case "ntad_amtrk_sta":				
		        headerInfo.push({title:"Station Name",data:jsonDOT.STNNAME, bind:'STNNAME'});
				headerInfo.push({title:"Station Code",data:jsonDOT.STNCODE, bind:'STNCODE'});
				headerInfo.push({title:"POC",data:jsonDOT.POC_NAME, bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE, bind:'POC_PHONE'});
				headerInfo.push({title:"Station Address 1",data:jsonDOT.ADDRESS1, bind:'ADDRESS1'});
				headerInfo.push({title:"Station Address 2",data:jsonDOT.ADDRESS2, bind:'ADDRESS2'});
				headerInfo.push({title:"State",data:jsonDOT.STATE, bind:'STATE'});
				headerInfo.push({title:"County",data:jsonDOT.COUNTY, bind:'COUNTY'});
				header = buildStyle(headerInfo);
				break;
			case "ntad_nhpn_ishwy":
			case "ntad_nhpn_ushwy":
				//full header field requirements have not been determined yet
				var headerInfo = [];
				headerInfo.push({title:"Highway Name",data:jsonDOT.LNAME,bind:'LNAME'});
				headerInfo.push({title:"POC Name",data:jsonDOT.POC_NAME,bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE, bind:'POC_PHONE'});
				headerInfo.push({title:"Mile Marker",data:jsonDOT.MILE_MARKER, bind:'MILE_MARKER'});
				header = buildStyle(headerInfo);
				break;
				break;
			case "ntad_ports":
				var headerInfo = [];
				headerInfo.push({title:"Port Name",data:jsonDOT.PORT_NAME,bind:'PORT_NAME'});
				headerInfo.push({title:"Latitude",data:jsonDOT.LATITUDE1,bind:'LATITUDE1'});
				headerInfo.push({title:"Longitude",data:jsonDOT.LONGITUDE1,bind:'LONGITUDE1'});
				headerInfo.push({title:"Port Owner",data:jsonDOT.OWNER,bind:'OWNER'});
				headerInfo.push({title:"POC Name",data:jsonDOT.POC_NAME,bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE,bind:'POC_PHONE'});
				headerInfo.push({title:"State",data:jsonDOT.STPOSTAL,bind:'STPOSTAL'});
				headerInfo.push({title:"Total Tonnage",data:jsonDOT.TOTAL,bind:'TOTAL'});
				headerInfo.push({title:"Max Draft",data:jsonDOT.MAX_DRAFT,bind:'MAX_DRAFT'});
				headerInfo.push({title:"Number Of Berths",data:jsonDOT.NUM_BERTHS,bind:'NUM_BERTHS'});
				header = buildStyle(headerInfo);
				break;
			case "ntad_rail_ln":
				var headerInfo = [];
				headerInfo.push({title:"Rail Owner",data:jsonDOT.RROWNER1,bind:'RROWNER1'});
				headerInfo.push({title:"POC Name",data:jsonDOT.POC_NAME,bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE,bind:'POC_PHONE'});
				headerInfo.push({title:"Class of Rail",data:jsonDOT.CLASS,bind:'CLASS'});
				headerInfo.push({title:"State",data:jsonDOT.STATEAB,bind:'STATEAB'});
				headerInfo.push({title:"County",data:jsonDOT.CNTYFIPS,bind:'CNTYFIPS'});
				headerInfo.push({title:"Freight Stations",data:jsonDOT.FREIGHT_STATIONS,bind:'FREIGHT_STATIONS'});
				header = buildStyle(headerInfo);
				break;
			case "ntad_rail_st":
				var headerInfo = [];
				headerInfo.push({title:"Rail Owner",data:jsonDOT.OWNER,bind:'OWNER'});
				headerInfo.push({title:"POC Name",data:jsonDOT.POC_NAME,bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE,bind:'POC_PHONE'});
				headerInfo.push({title:"State",data:jsonDOT.STATEAB,bind:'STATEAB'});
				headerInfo.push({title:"County",data:jsonDOT.CNTYFIPS,bind:'CNTYFIPS'});
				header = buildStyle(headerInfo);
				break;
			case "ntad_trans_ln":
				var headerInfo = [];
				headerInfo.push({title:"POC Name",data:jsonDOT.POC_NAME,bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE,bind:'POC_PHONE'});
				headerInfo.push({title:"State",data:jsonDOT.UZA_STATE,bind:'UZA_STATE'});
				//headerInfo.push({title:"County",data:jsonDOT.CNTYFIPS,bind:'CNTYFIPS'});
				headerInfo.push({title:"City",data:jsonDOT.UZA_CITY,bind:'UZA_CITY'});
				header = buildStyle(headerInfo);
				break;
			case "ntad_trans_st":
				var headerInfo = [];
				headerInfo.push({title:"Station Name",data:jsonDOT.STATION,bind:'STATION'});
				headerInfo.push({title:"POC Name",data:jsonDOT.POC_NAME,bind:'POC_NAME'});
				headerInfo.push({title:"POC Phone Number",data:jsonDOT.POC_PHONE,bind:'POC_PHONE'});
				headerInfo.push({title:"Station Address",data:jsonDOT.STR_ADD,bind:'STR_ADD'});
				header = buildStyle(headerInfo);
				break;
			default:
				console.log("error: can't find header for type "+layerID);
		}
		return header?header:"";
	}
});