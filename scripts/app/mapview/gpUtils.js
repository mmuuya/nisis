/*
 * Geoprocessing utilities
 */

define(["esri/geometry/webMercatorUtils","esri/tasks/Geoprocessor","esri/tasks/FeatureSet"]
	, function(webMercatorUtils, Geoprocessor, FeatureSet) {

	return {
		agsUrls: nisis.url.arcgis,
		extractData: function(feats, format) {
			//console.log('Extract Data: ', format, feats);

			if (!feats || !feats.length || !format) {
				return;
			}
			//check file format
			if (format === 'kml') {
				this.exportToKml(feats);

			} else if (format === 'shapefile') {
				this.exportToZipShapefile(feats);

			} else {
				//console.log('Unknown format: ', format);
				nisis.ui.displayMessage('App could not identify the file format.', 'error');
			}
		},
		exportToKml: function(feats) {
			//console.log('Convert to KML file: ', feats);

			var features = $.map(feats, function(feat){
				var geom = webMercatorUtils.webMercatorToGeographic(feat.geometry),
					g = null;

				if (geom.type == 'polygon') {
					g = geom.rings;

				} else if (geom.type == 'polyline') {
					g = geom.paths;

				} else if (geom.type == 'point') {
					g = [geom.x, geom.y];
				} else {
					nisis.ui.displayMessage("Unknown geometry type.", 'error');
					return;
				}

				return {
					type: geom.type,
					geometry: g
					//,attributes: feat.attributes,
				};
			}); 

			$.ajax({
				url: "api/kml/",
		        type: "POST",
		        data: {
		            action: "generate-kml"
		            ,features: JSON.stringify(features)
		            //,features: features
		        },
		        //dataType: 'json',
		        success: function(data, status, req){
		        	//console.log('Kml success:', data);
		            var resp;

		            try {
		            	resp = JSON.parse(data);
		            	///console.log('Kml success json:', resp);
		            } catch (err) {
		            	//console.log('KML response parse error:', err);
		            	nisis.ui.displayMessage("App could not generate KML file.", 'error');
		            	return;
		            }

		            if (!resp) {
		            	nisis.ui.displayMessage("App could not generate KML file.", 'error');
		            	return;
		            }

		            if (resp['return'] && resp['return'].toLowerCase() === 'success') {
		            	nisis.ui.displayMessage(resp.message, 'info');

		            	var path = resp['path'];

		            	if ( path ) {
		            		nisis.ui.displayMessage("Downloading file now ...", 'info');
		            		var url = location.host + path;
		            		window.open(path);

		            	} else {
		            		nisis.ui.displayMessage("App could not download the file.", 'error');
		            	}

		            } else {
		            	nisis.ui.displayMessage(resp.message, 'error');
		            }
		            
		        },
		        error: function(req, status, err){
		            //console.log("Kml error:", err);
		            var msg = 'App could not generate KML file.';
		            msg += err;
		            nisis.ui.displayMessage(msg, 'error');
		        }
			});
		},
		exportToZipShapefile: function(feats, format) {
			//console.log('Convert to Zipped Shapefile: ', feats);
			
			var $this = this,
				def,
				gp, lyrs = [],
				featSet,
				params,
				gpUrl = $this.agsUrls ? $this.agsUrls.gpExtractData() : null;

			if (!gpUrl) {
				return;
			}

			if (!feats || !feats.length || !lyrs) {
				return;
			}

			gp = new Geoprocessor(gpUrl);
			featSet = new FeatureSet();
			featSet.features = feats;

			$.each(feats, function(i, f) {
				var l = f.getLayer(),
					id = l.id,
					group = l.group,
					name = l.name;

				if ( !name || !group) return;

				if ( group === 'tfr') {

					if ( name.toLowerCase().indexOf('Point') !== -1 ) lyrs.push('TFR Points');
					if ( name.toLowerCase().indexOf('Line') !== -1 ) lyrs.push('TFR Lines');
					if ( name.toLowerCase().indexOf('Polygon') !== -1 ) lyrs.push('TFR Polygons');

				} else if ( group === 'user' ) {

					if ( name.toLowerCase().indexOf('point') !== -1 ) lyrs.push('NISIS Points');
					if ( name.toLowerCase().indexOf('line') !== -1 ) lyrs.push('NISIS Lines');
					if ( name.toLowerCase().indexOf('polygon') !== -1 ) lyrs.push('NISIS Polygons');
				}
			});

			if ( !lyrs.length ) {
				var msg = "There is no valid layer for extraction task.";
				nisis.ui.displayMessage(msg, 'error');
				return;
			}
			//start process
			def = new $.Deferred();
			//setup process param
			params = {
				"Layers_to_Clip": lyrs,
				"Area_of_Interest": featSet,
				"Feature_Format": format ? format : "Shapefile - SHP - .shp"
			};
			//submit job
			gp.submitJob(params,
				//handle complete
				function(job) {
					//console.log('GP Job Completed:', job);

					if (job.jobStatus !== 'esriJobFailed') {
						def.notify('Downloading results');

						gp.getResultData(job.jobId, "Output_Zip_File"
							, function(result) {
								//console.log('GP Job Get Result:', result);
								def.resolve('Done downloading results');
								$this.downloadExtractedData(result);
							}, function(err) {
								console.log('GP Job Get Result Error:', err);
								var msg = err.message ? err.message : "failed to donwload result";
								def.reject(msg);
							}
						);
							
					} else {
						def.reject("Date extraction job failed.");
					}
				},
				//status update
				function(job) {
					//console.log('GP Job Update:', job);

					var msg = null;

					if (job && job.messages && job.messages.length) {
						msg = job.messages[job.messages.length-1].description;
					}

					msg = msg ? msg : "Processing request ...";
					def.notify(msg);
					//nisis.ui.displayMessage(msg, 'info');
				},
				//handle errors
				function(error) {
					console.log('GP Job error:', error);
					var msg = job.message ? job.message : "App failed to process your extraction request.";
					def.reject(msg);
					//nisis.ui.displayMessage(msg, 'error');
				}
			);

			return def.promise();
		},
		convertFeaturesToKML: function(feats) {
			if (!feats || !feats.length) {
				return;
			}

			var def = new $.Deferred();

			return def.promise();
		},
		convertFeaturesToGeoJSON: function(feats) {
			if (!feats || !feats.length) {
				return;
			}


			var def = new $.Deferred();

			return def.promise();
		},
		downloadExtractedData: function(result) {
			//console.log('GP Job result:', result);

			var url = result.value.url;
			window.location = url;
		},

	};
});