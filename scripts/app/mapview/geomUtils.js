/*
 * Geometry utilities
*/

define(["app/appConfig","esri/geometry/webMercatorUtils"]
	, function(appConfig, webMercatorUtils) {

	return {
		//get union of geometries
		unionGeometries: function() {

		},
		//get geometries intersections
		intersectGeometries: function() {

		},
		//cut geometries
		cutGeometries: function(cutter, geoms) {
			//console.log('Cut geometries: ', arguments);

			if(!cutter) {
				return;
			}

			var $this = this,
				geomService = nisis.ui.mapview.geometryService,
				cutGeom = cutter.geometry ? cutter.geometry : cutter,
				cutGeomExt = cutGeom.getExtent(),
				cutLayer = cutter.geometry ? cutter.getLayer() : null,
				activeLayers = [],
				geometries = [],
				map = nisis.ui.mapview.map;

			var geoms = geoms || null;

			if (!geoms) {

				var vLayers = map.getLayersVisibleAtScale();

				if (!vLayers || !vLayers.length) {
					return;
				}

				$.each(vLayers, function(i, layer){

					if ( !layer.visible || !layer.group ) {
						return;
					}

					if (!layer.type || !(layer.type && layer.type === "Feature Layer") ) {
						return;
					}

					if ( !layer.isEditable() ) {
						return;
					}

					graphics = layer.graphics;

					if (!graphics || !graphics.length) {
						return;
					}

					$.each(graphics, function(j, graphic) {

						if (graphic.geometry && graphic.geometry.type !== "point") {

							//var target = cutGeomExt.contains(graphic.geometry);
							var target = cutGeomExt.intersects(graphic.geometry);

							if ( target ) {
								//console.log('Found map graphic: ' + i + ' (' + layer.name + ') - ' + j);
								geometries.push(graphic.geometry);

								var diffOps = geomService.difference([graphic.geometry], cutGeom);
								//var intOps = geomService.intersect([graphic.geometry], cutGeom);
								//check for any errors
								//if (!diffOps || !intOps) {
								if (!diffOps) {
									console.log('Difference / intersect geometries: Service Error for :: ', layer.name);
									return;
								}
								//process operation
								diffOps
								.then(function(results) {
									$this.cutOpsSucceeded(results, layer, graphic);
								}, 
								$this.cutOpsFailed);

								/*intOps
								.then(function(results) {
									$this.cutOpsSucceeded(results, layer);
								}, 
								$this.cutOpsFailed);*/
							}
						}
					});
				});

			} else {
				geometries = geoms;
			}

			//remove only map graphics features
			if (cutLayer && cutLayer.id === 'map_graphics') {
				cutLayer.remove(cutter);
			}

			if (!geometries.length) {
				console.log('Cut geometries: None found.');
				return;
			}

			//console.log('Cut geometries: ', cutGeom, geometries);
		},
		cutOpsSucceeded: function(results, layer, graphic) {
			//console.log('Cut geometries success: ', results);

			if (!results) {
				return;
			}

			$.each(results, function(i, result) {
				if (layer) {
					var app = appConfig.get('app').toLowerCase();
					nisis.ui.mapview.util.addGraphicToUserLayers(result, app);
				} else {
					nisis.ui.mapview.util.addGraphicToMap(result);
				}
			});
		},
		cutOpsFailed: function(error) {
			console.log('Cut geometries error: ', error);

			if (!error) {
				return;
			}

			var msg = error.message ? error.message : "App could not perform cut operation.";

			if ( error.details.length ) {
				msg += " (";
				msg += error.details.join(", ");
				msg += ").";
			}

			nisis.ui.displayMessage(msg, 'error');
		},
		//extract data
		extractData: function(geom) {

		},
		transformGeom: function(geom, from, to) {
			var g = null;

			if ( from == to ) {
				g = geom;

			} else if ( (from == 3857 || from == 102100) && to == 4326 ) {
				g = webMercatorUtils.webMercatorToGeographic(geom);

			} else if ( from == 4326 && (to == 3857 || to == 102100) ) {
				g = webMercatorUtils.geographicToWebMercator(geom);
			}

			return geom;
		},
		getClosestVOR: function(lat, lon, num) {

			if(!lat || !$.isNumeric(lat)) {
				return;
			}

			if(!lon || !$.isNumeric(lon)) {
				return;
			}

			if(!num || !$.isNumeric(num)) {
				return;
			}

			var def = new $.Deferred();

			$.ajax({
				url: 'api/lookup.php',
				type: 'GET',
				data: {
					target: 'closest-vor',
					filter: 'vor',
					lat: lat,
					lon: lon,
					num: num
				},
				dataType: 'json',
				success: function(data, status, xhr) {

					if (data) {
						def.resolve(data);
					} else {
						def.reject('Query returned empty result');
					}
				},
				error: function(xhr, status, err) {
					console.log('VOR Search error: ', arguments);
					def.reject(err);
				}
			});

			return def.promise();
		},
		getVorByID: function(vorId) {

			if(!vorId) {
				return;
			}

			var def = new $.Deferred();

			$.ajax({
				url: 'api/lookup.php',
				type: 'POST',
				data: {
					target: 'vor-info',
					filter: vorId
				},
				dataType: 'json',
				success: function(data, status, xhr) {
					//console.log('VOR result: ', arguments);
					if (data) {
						def.resolve(data);
					} else {
						def.reject('Query returned empty result');
					}
				},
				error: function(xhr, status, err) {
					console.log('VOR error: ', arguments);
					def.reject('App could not retrieve the specified VOR info.');
				}
			});

			return def.promise();
		}
	};

});
