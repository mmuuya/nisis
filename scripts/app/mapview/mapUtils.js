/*
 * Map utilities
*/

define(["dojo/parser","dojo/dom","dojo/query","dojo/on",
	"dojo/keys","dojo/has","dojo/_base/connect",
	"dojo/dom-construct"]

, function (parser,dom,query,on,
	keys,has,connect,
    domConstruct) {

	return { 
		setMapInfoWindowCleanup: function(map) {

		},
		showInfoWindowOptions: function(evt) {

		},
		clearInfoWindowOptions: function(evt) {
			domConstruct.destroy(query('.shapeMeasure', mapElements.map.infoWindow.domNode)[0]);
			//hide all extra buttons
			$('.profileButton').hide();
			$('.copyButton').hide();
			$('.shareButton').hide();
			//hide feature nav buttons
			$('.titleButton.next').hide();
			$('.titleButton.prev').hide();
			//for layers to refresh after the info window closes
			var attrInsp = nisis.ui.mapview.featureEditor.attributeInspector,
				layer, 
				feat = attrInsp._currentFeature;

			if(feat){
				layer = attrInsp._currentFeature.getLayer();
				if(layer && layer.refresh){
					//layer.refresh();
				}
			}
		}
	};

});