//define([],function(){

//NISIS Facility Profile ViewModel
define(["app/iconsConfig", "app/appUtils", "esri/graphic",  "esri/geometry/Polyline", "esri/symbols/SimpleLineSymbol", "esri/geometry/Point", "esri/SpatialReference", "esri/geometry/webMercatorUtils", "esri/geometry/screenUtils", "esri/geometry/ScreenPoint", "app/windows/shapetools", "app/windows/tableview","app/historyNav"],
    function(iconsConfig, appUtils, Graphic, Polyline, SimpleLineSymbol, Point, SpatialReference, webMercatorUtils, screenUtils, ScreenPoint, shapetools, tableview,historyNav){

templates = {
    // NISIS-2744 KOFI HONU - renamed "airports_json_template" to "apt_json_template"
    'airports': apt_json_template,
    'airports_pr': apt_json_template,
    'airports_others': apt_json_template,
    'airports_pu': apt_json_template,
    'airports_military': apt_json_template,
    'atm': atm_json_template,
    'ans': ans_json_template,
    'ans1': ans_json_template,
    'ans2': ans_json_template,
    'osf': osf_json_template,
    'tof': tof_json_template
},

nisisProfileSkeletons = {
    'airports':  airportProfileConstants,
    'airports_pr': airportProfileConstants,
    'airports_others': airportProfileConstants,
    'airports_pu': airportProfileConstants,
    'airports_military': airportProfileConstants,
    'atm': atmProfileConstants,
    'ans': ansProfileConstants,
    'ans1': ansProfileConstants,
    'ans2': ansProfileConstants,
    'osf': osfProfileConstants,
    'tof': tofProfileConstants
},

buildAirportForm = function(objectId) {
    var form = '<div class="forms-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="form-contact_info-' + objectId+'" scrollable></div>'+
                '<div class="grid grid-pad showProfile" id="form-basic_info-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-status_update-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-assoc_atms-' + objectId +'" scrollable></div>'+
                //'<div class="grid grid-pad" id="form-associated_atm_facilities-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-airport_ops-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-current_notams-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-ans_ops-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-ans_inf-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-other_ops-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-incident_info-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-useful_links-' + objectId +'" scrollable></div>'+
            '</div>'+
            '<div class="datatables-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="datatable-runways-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="datatable-associated_ans-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="datatable-associated_atm-' + objectId +'" scrollable></div>'+
            '</div>';
            
        return form;
    },

buildATMForm = function(objectId) {
    var form = '<div class="forms-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="form-contact_info-' + objectId+'" scrollable></div>'+
                '<div class="grid grid-pad showProfile" id="form-basic_info-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-status_update-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-staffing-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-associated_atm_facilities-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-incident_info-' + objectId +'" scrollable></div>'+

            '</div>'+
            '<div class="datatables-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="datatable-supported-airports-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="datatable-tech-ops-staffed-facilities-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="datatable-ans-infrastructure-' + objectId +'" scrollable></div>'+

            '</div>';
        return form;
    },

buildANSForm = function(objectId) {
    var form = '<div class="forms-wrap push-right with-menu">'+
                '<div class="grid grid-pad showProfile" id="form-basic_info-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-status_update-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-incident_info-' + objectId +'" scrollable></div>'+

            '</div>'+
            '<div class="datatables-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="datatable-supported-airports-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="datatable-supported-airports-runways-procedures-' + objectId+'" scrollable></div>'+
                '<div class="grid grid-pad" id="datatable-supported-atms-' + objectId +'" scrollable></div>'+
            '</div>';
        return form;
    },

buildOSFForm = function(objectId) {
    var form = '<div class="forms-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="form-contact_info-' + objectId+'" scrollable></div>'+
                '<div class="grid grid-pad showProfile" id="form-basic_info-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-status_update-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-staffing-' + objectId +'" scrollable></div>'+

                '<div class="grid grid-pad" id="form-incident_info-' + objectId +'" scrollable></div>'+

            '</div>'+
            '<div class="datatables-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="datatable-supported-airports-' + objectId +'" scrollable></div>'+
            '</div>';
        return form;
    },

buildTOFForm = function(objectId) {
    var form = '<div class="forms-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="form-contact_info-' + objectId+'" scrollable></div>'+
                '<div class="grid grid-pad showProfile" id="form-basic_info-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-status_update-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-staffing-' + objectId +'" scrollable></div>'+
                '<div class="grid grid-pad" id="form-incident_info-' + objectId +'" scrollable></div>'+

            '</div>'+
            '<div class="datatables-wrap push-right with-menu">'+
                '<div class="grid grid-pad" id="datatable-associated_atm-' + objectId +'" scrollable></div>'+
            '</div>';
        return form;
    },

buildFormByType = {
    'airports': buildAirportForm,
    'airports_pr': buildAirportForm,
    'airports_others': buildAirportForm,
    'airports_pu': buildAirportForm,
    'airports_military': buildAirportForm,
    'atm': buildATMForm,
    'ans': buildANSForm,
    'ans1': buildANSForm,
    'ans2': buildANSForm,
    'osf': buildOSFForm,
    'tof': buildTOFForm
    },

// NISIS-2812 - KOFI HONU
findAirportProfileDataFieldValue = function(data, field) {
    var dbActualVal = null;
    //look for a value of dbCol in data
    var formData = data.formData;
    if (formData[0].hasOwnProperty(field)) {
        dbActualVal = formData[0][field];
    }
    return dbActualVal;
},



findAirportProfileDataDbColByName = function(template, in_name) {
    var item = "";

    //look for a value of name in data
     $.each(template,function(i,section){
        if (section.hasOwnProperty("formElements") && section.type=="form") {
            var sectionsFormElements = section.formElements;
            $.each(sectionsFormElements,function(j,formElement){
                if (formElement.hasOwnProperty("dbcol") && formElement.hasOwnProperty("name") && formElement.name == in_name) {
                    item = formElement;//.dbcol;
                    return false;
                }
            });
        }
    });

    return item;
},

findAirportProfileMultiOptions = function(template, in_dbcol) {
    var datasource = 0;
    $.each(template,function(i,section){
        if (section.hasOwnProperty("formElements") && section.type=="form") {
            var sectionsFormElements = section.formElements;
            $.each(sectionsFormElements,function(j,formElement){
                if (formElement.hasOwnProperty("dbcol") && formElement.dbcol == in_dbcol) {
                    datasource = formElement["selectOptions"];
                    return false;
                }

                if(datasource !== 0){
                    return false;
                }
            });
        }
    });
    return datasource;
},

facTypes = {
    'airports': 'APT',
    'airports_pr': 'APT',
    'airports_others': 'APT',
    'airports_pu': 'APT',
    'airports_military': 'APT',
    'atm': 'ATM',
    'ans': 'ANS',
    'ans1': 'ANS',
    'ans2': 'ANS',
    'osf': 'OSF',
    'tof': 'TOF'
},

facTypesIWL = {
    'airports': 'airports',
    'airports_pr': 'airports',
    'airports_others': 'airports',
    'airports_pu': 'airports',
    'airports_military': 'airports',
    'atm': 'atm',
    'ans': 'ans',
    'ans1': 'ans',
    'ans2': 'ans',
    'osf': 'osf',
    'tof': 'tof'
},

facTables = {
    'airports': 'NISIS_AIRPORTS',
    'airports_pr': 'NISIS_AIRPORTS',
    'airports_others': 'NISIS_AIRPORTS',
    'airports_pu': 'NISIS_AIRPORTS',
    'airports_military': 'NISIS_AIRPORTS',
    'atm': 'NISIS_ATM',
    'ans': 'NISIS_ANS',
    'ans1': 'NISIS_ANS',
    'ans2': 'NISIS_ANS',
    'osf': 'NISIS_OSF',
    'tof': 'NISIS_TOF'
},

titleFields = {
    'airports': [/*"icao_id","faa_id",*/"iata_id","lg_name","assoc_city","assoc_st"],
    'airports_pr': [/*"icao_id","faa_id",*/"iata_id","lg_name","assoc_city","assoc_st"],
    'airports_others': [/*"icao_id","faa_id",*/,"iata_id","lg_name","assoc_city","assoc_st"],
    'airports_pu': [/*"icao_id","faa_id",*/,"iata_id","lg_name","assoc_city","assoc_st"],
    'airports_military': [/*"icao_id","faa_id",*/,"iata_id","lg_name","assoc_city","assoc_st"],
    'atm': ["atm_id","lg_name"],
    'ans': ["ans_id","fsep_id","basic_ty"],
    'ans1': ["ans_id","fsep_id","basic_ty"],
    'ans2': ["ans_id","fsep_id","basic_ty"],
    'osf': ["osf_id","lg_name"],
    'tof': ["tof_id","long_name"]
},


ansIcons = {
                0: iconsConfig.ans.nav.good,
                1: iconsConfig.ans.nav.warn,
                2: iconsConfig.ans.nav.warn,
                3: iconsConfig.ans.nav.warn,
                4: iconsConfig.ans.nav.down
},
ansLabels = {
                0: 'Normal Operation',
                1: 'Line Only Interruption',
                2: 'Reduced System/Equipment',
                3: 'Reduced Facility/Service Interruption',
                4: 'Full Interruption'
},


personnelFields = {   //personnel total, personnel assigned
    'airports': [],
    'airports_pr': [],
    'airports_others': [],
    'airports_pu': [],
    'airports_military': [],
    'atm': ["ato_pa_ttl","ass_p_ttl"],
    'ans': [],
    'ans1': [],
    'ans2': [],
    'osf': ["ato_pa_ttl","ass_p_ttl"],
    'tof': ["pers_acct_for_total","assigned_pers_total"]
},

jump2profile = function(o){

    // console.log("object: ", o);

    var $forms = o.forms,
        $datatables = o.datatables,
        $target = o.target,
        $anchor = o.anchor,
        profileWindow = o.profileWindow;

    $forms.find('.showProfile').removeClass('showProfile');
    $datatables.find('.showProfile').removeClass('showProfile');
    $target.addClass('showProfile');

    profileWindow.find("menuitem").removeClass('current');
    $anchor.parent().addClass('current');

    //see if there are any datatable grids in this sections
    $target.find(".grid").addClass("showProfile");

    //scroll to the top of the section
    $target.animate({ scrollTop: (0) }, 'fast');
},


route2profile = function(e){
    var $anchor = $(e.currentTarget).find('a'),
        getHash = $anchor.prop('hash'),
        profileWindow = $anchor.closest('.nisis_profile'),
        isDataTable =  JSON.parse($anchor[0].attributes[1].value),
        $forms = profileWindow.find('.forms-wrap'),
        $datatables = profileWindow.find('.datatables-wrap'),
        $target = $(getHash), //irena - make sure hash containes the -objectId part to jump to correct section of the correct window
        //see if section has dataTables
        sectionHasGrid = ($target.has(".section-grid").length != 0),
        isDataTable = isDataTable || sectionHasGrid;

    //if snapshot was shown, hide it and show the sections
    var $filesWrap = profileWindow.find(".files-wrap");
    var $formsWrap = profileWindow.find(".forms-wrap");
    if (isDataTable) {
        $datatables.show();
    }
    else {
        $datatables.hide();
    }
    $formsWrap.show();
    $filesWrap.hide();

    $forms.find('.showProfile').removeClass('showProfile');
    $datatables.find('.showProfile').removeClass('showProfile');
    $target.addClass('showProfile');

    // profileWindow.find("menuitem").removeClass('current');
    // $anchor.addClass('current');

    //see if there are any datatable grids in this sections
    $target.find(".grid").addClass("showProfile");

    //scroll to the top of the section
    $target.animate({ scrollTop: (0) }, 'fast');    //Jon - before you would only click on the anchor I changed it so now the click event is on the menuitem and then it looks for the anchor, gets the hash and so on.

        e.preventDefault();

        toggleModuleWidth(e,isDataTable);

        // obj of stuff to jump2status
        var o = {
                profileWindow: profileWindow,
                forms: $forms,
                datatables: $datatables,
                target: $target,
                anchor: $anchor
        };

        jump2profile(o);

        // $forms.find('.showProfile').removeClass('showProfile');
        // $datatables.find('.showProfile').removeClass('showProfile');
        // $target.addClass('showProfile');
        //
        // profileWindow.find("menuitem").removeClass('current');
        // $anchor.addClass('current');
        //
        // //see if there are any datatable grids in this sections
        // $target.find(".grid").addClass("showProfile");
        //
        // //scroll to the top of the section
        // $target.animate({ scrollTop: (0) }, 'fast');
// });
},

//from vars.js
// TODO - add hilight behavior to qsr arrow icon
// FIXME - replace removeHilite's cleartimeout w/ animation end listener
jump2status = function(e){
    // console.log("jump2status");
    var $module = $(e.currentTarget).closest('.nisis_profile'),
        $moduleHash = $module.parent('.kendoInfoWindow').attr("id").replace("nisis-object_","");
    // adjustForm(e);

    var moduleOffset = $module.offset().top,
        $profileWindow = $(e.currentTarget).closest('.nisis_profile'),
        $forms = $profileWindow.find('.forms-wrap'),
        $menuItem = $(e.currentTarget).parent().parent(); //menuitem.qsr-container
        $qsrLabel=  $menuItem.find(".qsr-label");
        $targetField = $profileWindow.find("#" + $qsrLabel.text().replace(/ /gi,"_")),
        targetOffset = Number($menuItem.attr('data-target-offset')),
        $targetForm = $($menuItem.attr('data-target-form') + "-" + $moduleHash),
        offsetPadding = 85, //header plus 1.5em padding
        finalOffset = targetOffset - offsetPadding;

    // console.log("targetForm:", $($targetForm));
    $forms.find('.showProfile').removeClass('showProfile');
    $targetForm.addClass('showProfile');
    $targetForm.animate({scrollTop: finalOffset});
    $targetField.addClass('hiliteField');

    // TODO trigger class removal at animation end
    var removeHilite = setTimeout(function(){
        $targetField.removeClass('hiliteField');
    },6000);
},

//from handlers.js
closeModule = function(e){
    // console.log("closeModule");
    var $module = $(e.currentTarget).closest('.nisis_profile');
        $module.hide();
    },

showTooltip = function(e){
    var $el = $(e.currentTarget),
    $input = $el.find('input, select, textarea'),
    $span = $el.find('.sig'),
    $label = $el.find('label'),
    content,
    pos;

    e.preventDefault();

    //remove title attr don't need it
    $input.removeAttr('title');

    var tooltip = function(){
        if($el.data('kendoTooltip')){
            $(e.sender).show();
        }else{
            //custom tooltip
            $el.kendoTooltip({
                content: content,
                position: pos,
                autoHide:true
            }).data('kendoTooltip').show();
        }
    }

    //Labels
    if($label.length>0 && $label[0].hasAttribute("overwritten")) {
        pos = 'top';
        content = '';

        //look for data-bind
        $profileWindow = $(e.currentTarget).closest('.nisis_profile');
        var self = $profileWindow.closest('.kendoInfoWindow').data('self');
        var overwrites=self.original_data_.overwrites[0];

        var dbcol =  $el.find("*")
            .filter(function(){
                return this.hasAttribute("data-bind") ;
            })
        .attr('data-bind');

        if (dbcol && dbcol.length>0) {
            //get rid of value:fields. prefix
            dbcol = dbcol.replace(/value:fields./g, '');

            var updated_by =dbcol+'.updated_by', updated_date =dbcol+'.updated_date';

            if( $label[0].scrollWidth != undefined && $label[0].scrollWidth > $label.innerWidth()) {
                content = "<div class='overwrite_tooltip'>"+ $label.text().trim() + "</div>";
            }
            content += "<div class='overwrite_tooltip'>Entered by:"+overwrites[updated_by] +"</div><div class='overwrite_tooltip'>Last updated:"+overwrites[updated_date]+"</div>";
            tooltip();
        }
    } else if($label.length>0 && $label[0].scrollWidth != undefined) {
        if($label[0].scrollWidth > $label.innerWidth()){
        content = $label.text().trim();
                pos = 'top';
                tooltip();
            }
        }

    //Signature Spans
    if($span.text() != '' && $span[0].scrollWidth > $span[0].offsetWidth){
        $el = $span;
        content = $span.text().trim();
        pos = 'bottom';
        tooltip();
    }

},

    // TODO: rename to match QSR function name
toggleModuleCollapse = function(e) {
    // console.log("toggleModuleCollapse");
    var $module = $(e.currentTarget).closest('.nisis_profile');
        $module.toggleClass('moduleCollapsed');
        if($module.is('centered'))
             $module.removeClass('centered');
        // toggleQSR()
    },

toggleModuleWidth =    function(e,isDataTable){
    // console.log("toggleModuleWidth");
    var $module = $(e.currentTarget).closest('.nisis_profile');
        isDataTable ? $module.addClass('datatable') : $module.removeClass('datatable');
    },

toggleMenu = function(e){
    var profileWindow = $(e.currentTarget).closest('.nisis_profile'),
            $qsr_menu = profileWindow.find('.qsr-menu-container'),
            $profiles_menu = profileWindow.find('.profiles-menu-container'),
            $filesWrap = profileWindow.find('.files-wrap'),
            $link2status = profileWindow.find('menu.qsr > menuitem'),
            $menu = profileWindow.find('menu.qsr'),
            $select = profileWindow.find('select'),
            toggleActiveClass = 'dark-dark-blue',
            $snapshot = $('.btn-snapshots'),
            toggleList = {
                "btn-qsr": $qsr_menu,
                "btn-profiles": $profiles_menu,
                "btn-snapshots": $filesWrap
            };

        var btns = Object.keys(toggleList),
            btn_class = e.currentTarget.className.match(/btn-[a-z]*/)[0],
            //  DRYer than ...
            $btn_container = function(profileWindow, _className){
                return $(profileWindow).find("i." + _className).parents('li');
            };

            $btn_container(profileWindow, btn_class).toggleClass(toggleActiveClass);
            if(toggleList[btn_class]){
                toggleList[btn_class].toggleClass('menu-on');
            }


        btns.forEach(function(el){
            if(el !== btn_class){
                $btn_container(profileWindow, el).removeClass(toggleActiveClass);
                toggleList[el].removeClass('menu-on');
            }
        });
        adjustForm(e);
    },

showValidation = function(e){
    var profileWindow = $(e.currentTarget).closest('.nisis_profile'),
    $dirtyFormIndicator = profileWindow.find("div.nisisProfileDirtyForm");
    //show validation
    $dirtyFormIndicator.show();
},

paperCSSFocus = function(e){
    var $element = $(e.currentTarget).parents('span.k-numerictextbox'),
        state = 'has-focus';

    switch(e.type){
        case "focus":
            $element.addClass(state); break;
        case "blur":
            $element.removeClass(state); break;
    }
},


adjustForm = function(e){
    // console.log("adjustForm");
        var profileWindow = $(e.currentTarget).closest('.nisis_profile');
        var $forms = profileWindow.find('.forms-wrap');
        var $datatables = profileWindow.find('.datatables-wrap');
        var $navContainer = profileWindow.find('.nav-container');
        var openMenusCount = profileWindow.find('.nav-container').find('.menu-on').length;
        openMenusCount <= 0 ?
        ($forms.removeClass('with-menu'), $datatables.removeClass('with-menu'), $navContainer.css('width', 30)) :
        ($forms.addClass('with-menu'), $datatables.addClass('with-menu'), $navContainer.css('width', 292) );

    },

sigSpacer = "\r\n\r\n",
sigIntro = "Entered by",

capitalize = function(name){
    var nisisname = name,
        firstname = nisisname[0],
        lastname = nisisname[nisisname.indexOf(" ") + 1];

    return nisisname.replace(firstname, firstname.toUpperCase()).replace(lastname, lastname.toUpperCase());
},

stripSig = function(content){
    //FIXME: do me better
    content = content.replace(/(?:\r|\n)/gi,'');
    // console.log("content after space strip:", content);
    var start = content.indexOf('Entered by'),
        end = content.length;
    // console.log("content after sig strip:", content.replace(content.slice(start, end), ""));
    return content.replace(content.slice(start, end), "");
},


addSig = function(content, sig){
    // NISIS-2896 KOFI HONU - more fixes for newline
    //return content + sigSpacer + "\n" + sig;
    return content + sigSpacer  + sig;
},

//OMG
spaceSig = function(content){
    return content.replace(sigIntro, sigSpacer + sigIntro);
},


handleSig = function(e){

    var $profileWindow = $(e.currentTarget).closest('.nisis_profile'),
        $target = $(e.currentTarget)[0],
        signature = [sigIntro, capitalize(nisis.user.name),"\r\non", new Date().toGMTString()].join(" ").replace(/ GMT/,"Z");

    // console.log("event:", e.type);

    switch(e.type){
        case "focus":
            if($target.value.indexOf(sigIntro) !== -1){
                $target.value = stripSig($target.value);
            };
            return;
        case "blur":
            if($target.value.indexOf(sigIntro) === -1){
                $target.value = addSig($target.value, signature);
            };
            return;
    }

},

previousVal = "",
storedVal = "",


//FIXME: this should be part of handleSelect
captureCurrentVal = function(e){
    previousVal = e.sender._old;
},

storeCurrentVal = function(e){
    storedVal = previousVal;
},

//FIXME: rename to trigger modal or something?
handleSelect = function(e){

            var vals = {},
                $profileWindow = $(e.currentTarget).closest('.nisis_profile'),
                opts = $(e.currentTarget),
                select_ID = "#" + opts[0].id,
                $nextExplanationField = $(opts[0]).parents(".content").parent("div").next("div").find("textarea"),
                $newStatusClass = $(select_ID).find('option:selected').attr('class');

            // console.log("container:", $profileWindow);
            // console.log("next field:", $nextExplanationField);
            // console.log("select_ID:", opts[0].id);
            // console.log("next field's ID:", $nextExplanationField[0]);

            var odd_statuses_modal = ["#Current_Readiness_Level", "#Current_SECON_Level"];
            var exclude_statuses_modal = ["#Sat_Phone_Status"];
            //check if we need to put up modal
            if((select_ID.indexOf('Status') !== -1 || $.inArray(select_ID, odd_statuses_modal) > -1) && typeof $nextExplanationField[0] !== "undefined" && $.inArray(select_ID, exclude_statuses_modal)==-1 ){
                // remove previous status
                opts[0].className.split(" ").forEach(function( _el){
                    if(_el.indexOf('status-') !== -1){
                        $(select_ID).removeClass(_el);
                    }
                });

                // TODO: note element ID, current value

                // TODO: make modal
                vals = {
                    container: $profileWindow,
                    select: select_ID,
                    textarea: $nextExplanationField,
                    value: "",
                    top: $nextExplanationField.offset().top,
                    left: $nextExplanationField.offset().left,
                };
                makeModal(vals);

                // console.log("sent:", vals.textarea);
                $("#editField").focus();
            }

},



handleFileToggle = function(e){ 
    console.log("handleFileToggle");
    var profileWindow = $(e.currentTarget).closest('.nisis_profile');    
    var filesDiv = profileWindow.find(".k-upload");    
    var filesCheckedList = profileWindow.find('.files-wrap input[type=checkbox]:checked'); 
    var filesAreNotChecked = profileWindow.find('.files-wrap input[type=checkbox]:not(:checked)');

    var input =  $(e.currentTarget).find('input');
    var isChecked =  $(input).prop("checked");
    var fileId =  $(input).attr("fileid");
    var thumbnail =  $(input).parent().parent().find(".thumbnail");

    //get self and parms
    var self = $(e.currentTarget).parents("div.kendoInfoWindow").data('self');
    var feature = self.graphicId_;
    var layerId = self.layerId_;
    var facTable = facTables[layerId];
     
  
    var filesList = $(".files-wrap").find("span.k-filename");
    var fileIds = [];    
    $.each( filesCheckedList, function( key, value ) {         
        fileIds.push($(this).attr('fileid'));
    });    
    //make sure it stringified if empty
    if (fileIds.length==0) {
        fileIds = '[]';
    }
    
    $.ajax({
        url: "api/",
        type: "POST",
        data: {
              "action": "set_profile_pics",                    
              "objectid": feature.attributes.OBJECTID,
              "tableName": facTable,
              "pics": fileIds
        },
        success: function(data, status, req) {
            console.log("Success in ajax call to set_profile_pics");             
            if (data != null) {
                var resp = JSON.parse(data);
                console.log('response', resp);
                if(resp.result === "1") {
                    // JON, KOFI:
                    // HERE, WE CHECK FOR FUNCTION RETURN AND UPDATE BASIC_INFO SECTION WITH THE PICS THAT WERE CHECKED.
                    console.log(resp.result);
                    if (!isChecked) {
                        console.log("remove that images from the Basic Information Section");
                        //remove that images from the Basic Information Section
                        //look for thumbnails on basic information section with the attr of fileId                      
                        var thumbnails = profileWindow.find('.snapshot-container .thumbnail');
                        $.each( thumbnails, function( key, value ) {                             
                            if (fileId == $(this).attr("fileid")) {
                                //remove it from DOM
                                $(this).remove();
                                return;
                            }                            
                        }); 

                    }
                    else {                        
                        //they checked the box, find it, clone and copy
                        console.log("find it, clone and copy");
                        //add it from DOM
                        var el = profileWindow.find('.snapshot-container');
                        thumbnailClone = $(thumbnail).clone();
                        thumbnailClone.attr("fileid", fileId);   
                        thumbnailClone.appendTo( $(el) );                                                     
                    }
                    self.limitThumbnailsPromotionByDisablingCheckboxes(profileWindow);
                } else {
                    console.error("ERROR on set_profile_pics");
                }
            }
        },
        error: function(req, status, err){
                console.error("ERROR in set_profile_pics");
        }
    });

                                            
},
//clear input
clearInput = function(){
    return;
},

//edit input
edit = function (event) {
 console.log('edit input - NEED TO FIX - $(.paper-input) is incorrect ');
    var valState = function(_, val){
        return !val;
    };
    $('.paper-input').prop("disabled", valState);
},


dataBoundGBPA =function(e) {
    var event = e;
    e.sender.wrapper.find('img.gbpa-icon')
        .each(function() {
            $(this).on('click', function(evt) {
                console.log('Open GBPA:', evt);
                var id = evt.target.dataset.id;
                var lyr='ans1';  //EVANS , should we also query ans2 ? the original code queried only ans1, oversight?
                var layer = nisis.ui.mapview.layers[lyr];

                //query geometry
                var query = nisis.ui.mapview.util.queryFeatures({
                    layer: layer,
                    returnGeom: true,
                    outFields: ["*"],
                    where: "ANS_ID = '" + id + "'"
                });
                //success handler
                query.done(function(data){
                    //console.log(data);
                    var feature;
                    if(data.features.length > 0){
                        feature = data.features[0];
                        var loc = [feature.geometry.x, feature.geometry.y];
                        var evt = [];
                        evt.screenPoint =  new ScreenPoint(event.pageX, event.pageY);
                        evt.graphic = feature;
                        evt.graphic._layer = layer;
                        //convert from screen point to map point
                        evt.mapPoint = nisis.ui.mapview.util.getGeometryMapPoint(feature.geometry);
                        //bring up NISIS Tracked Object Profile
                        nisis.ui.mapview.util.bringUpNISISTOProfile(evt);
                    }
                });
                //failed handler
                query.fail(function(err){
                    nisis.ui.displayMessage(err, '', 'error');
                });
            });
    });
},

//format ans system
formatGBPA = function (assAns, category) {
    var systems = assAns.split(', '), value = "";
    $.each(systems, function(i, sys) {
        var v = sys.split(":");
        if (v.length==2) {
            var ans_id = v[0], ans_status = v[1];

            var val = "<span class='gbpa-label'>";
                val += "<img class='gbpa-icon img-m c-pt' src='";
                val += ansIcons[ans_status] + "' ";
                val += "width='20px' height='20px' ";
                val += "data-id='" + ans_id + "' ";
                val += "data-status='" + ans_status + "' ";
                val += "data-type='" + category + "' ";
                val += "title='" + ansLabels[ans_status] + "' />";
                val += " - ";
                val += ans_id;
                val += "</span>";

            value += val;
        }
    });

    //return the formatted text
    return value;
},


NISISProfile = function(profileData, evt, objectId) {
    //Build the json
    var theJson ={}, data = {};
    try {
        data = JSON.parse(profileData[0]['PROFILE']);
    }
    catch(err) {
        console.log("Unable to json parse ", profileData[0]['PROFILE']);
        nisis.ui.displayMessage("Unable to bring up Profile", 'error');
        return;
    }

    //get the template bases on the layerId
    var template = templates[evt.graphic._layer.id];
    this.template_ = template;
    this.layerId_ = evt.graphic._layer.id;

    var permissions = data.permissions[0];
    var overwrites = data.overwrites[0];
    //deep copy
    theJson = JSON.parse(JSON.stringify(template));

    var isGlobalReadOnly=jQuery.isEmptyObject(data.permissions[0]);

    var self = this;
    //go thru and add on the data
    $.each(theJson,function(i,section){
        if (section.hasOwnProperty("formElements") && section.type=="form") {
            var sectionsFormElements = section.formElements;
            $.each(sectionsFormElements,function(j,formElement){
                if (formElement.hasOwnProperty("value") && formElement.value=="#") {
                    var dbCol = formElement["dbcol"];
                    var dbActualVal = 0, permission = "r";

                    if (data.hasOwnProperty("formData") ) {
                            var sectionsFormData = data.formData;
                            $.each(sectionsFormData,function(jj,sectionFormData){
                                //find the actual value
                                if (sectionsFormData[0].hasOwnProperty(dbCol)) {
                                    dbActualVal = sectionsFormData[0][dbCol].hasOwnProperty('text') ? sectionsFormData[0][dbCol]['text'] : sectionsFormData[0][dbCol];
                                    return false;
                                }
                            });
                    }

                    // NISIS-2799 KOFI HONU - decode the html chars returned from stored proc.
                    //assign the actual value from the db
                    formElement.value = self.deSanitizeOutput(dbActualVal);

                    //assign the permission:
                    permission = (permissions.hasOwnProperty(dbCol))?permissions[dbCol]:"r" ;
                    formElement.disabled = (permission == "w")?"":"readonly";
                    // Modified by Kofi Honu.
                    // Red lock should only happen when update is by GFI Flat File update.
                    // if (overwrites.hasOwnProperty(dbCol)){
                    //     formElement.disabled = "readonly overwritten";
                    // }
                    if (overwrites.hasOwnProperty(dbCol) && overwrites[dbCol+".updated_by"] === "GFI Flat File"){
                        formElement.disabled = "readonly overwritten";
                    }

                    //overwrite with global read only if appropriate
                    if (isGlobalReadOnly) {
                        formElement.disabled = "readonly";
                    }
                }
            });
        }
    });

    //remember the json and data
    this.content=theJson;
    this.original_data_ = data;
    this.screenPoint_ = evt.screenPoint || "";
    this.mapPoint_ = evt.mapPoint || "";
    this.graphicId_ = evt.graphic || "";
    if (objectId) {
      this.objectId_ = objectId;
    }
    else {
      this.objectId_ = this.graphicId_.attributes.OBJECTID;
    }


    this.reallyDelete = false;

    //get rid of this!
    //this.airportId_ = this.graphicId_.attributes.FAA_ID;
    var nisisTrackedObjectIDs= {
            'airports_pr': this.graphicId_.attributes.FAA_ID,
            'airports_others': this.graphicId_.attributes.FAA_ID,
            'airports_pu': this.graphicId_.attributes.FAA_ID,
            'airports_military': this.graphicId_.attributes.FAA_ID,
            'atm': this.graphicId_.attributes.ATM_ID,
            'ans1': this.graphicId_.attributes.ANS_ID,
            'ans2': this.graphicId_.attributes.ANS_ID,
            'osf': this.graphicId_.attributes.OSF_ID,
            'tof': this.graphicId_.attributes.TOF_ID
          };

    this.nisisTrackedObjectId_ = nisisTrackedObjectIDs[this.layerId_];

    this.line_ = null;

    var observable = kendo.observable({
        objectId: this.objectId_,
        layerId: this.layerId_,
        nisisTrackedObjectId: this.nisisTrackedObjectId_,  //irena - was AirportId
        showDirtyFormIndicator: true,
        fields: {},
        //TODO: possibly add props for selects
        fieldsChanged: new Object(),
        explanation: null,
        note: 'test'
    });


    var sections = self.content;

    //put data-bind fields into observable
    $.each(sections,function(i,section){
        if (section.hasOwnProperty("formElements") && section.type=="form") {

            var sectionsFormElements = section.formElements;
            $.each(sectionsFormElements,function(j,formElement){
                if (formElement.hasOwnProperty("dbcol")) {
                    observable.fields[formElement["dbcol"]] = formElement["value"];
                }
            });
        }
    });

    observable.bind("set", function(e) {
        var observable = e.sender,
            columnName = e.field.replace("fields.", ""),
            columnValue = findAirportProfileDataFieldValue(self.original_data_, columnName), //O.G.
            changedColumns = observable.fieldsChanged;
            newColumnValue = e.value;

        if(columnValue === newColumnValue){
            console.log("same as in data")
            if (typeof changedColumns[columnName] !== 'undefined'){
                delete changedColumns[columnName];
                console.log("found", columnName, "in fieldsChanged and removed")
            }
        }else{
            console.log("new data")
            changedColumns[columnName] = String(e.value); //needed it to send Kofi statuses
            console.log("added", columnName, "in fieldsChanged and removed")
        }

        //check that there are indeed changes
        var dotObjectId = "nisis-object_" + facTypes[observable.layerId] + observable.objectId;
        var $kendoInfoWindow = $("#" + dotObjectId);

        //find our window and put up dirty form warning
        if ($kendoInfoWindow.length == 1 && observable.showDirtyFormIndicator && Object.keys(changedColumns).length > 3 && Object.keys(changedColumns).indexOf("_events" && "uid" && "parent") !== -1) {

            $kendoInfoWindow.find("div.nisisProfileDirtyForm").show();
        }else{
            $kendoInfoWindow.find("div.nisisProfileDirtyForm").hide();
        }

    });

    //hide datablocks
    clearTimeout(nisis.ui.datablockTimeOut);
    nisis.ui.hideDataBlocks();

    this.observable_ = observable;
    this.draw();
};

NISISProfile.prototype = {
    tip_ : 22,
    animationDuration_ : 100,
    div_ : null,
    resetMultiselects: function(profileWindow){
        var $multiselects = profileWindow.find('select.multiselect'), data = this.original_data_;
        $.each($multiselects,function(i,multiselect){
            var multiSelect =  $(multiselect).data("kendoMultiSelect");
            //bind-to attribute
            var bindTo = $(multiselect).attr("bind-to"),//res_dsg_ex
                initMultiSelectValue = findAirportProfileDataFieldValue(data, bindTo),
                initMultiSelectValueArray = initMultiSelectValue.split(',');
            if (initMultiSelectValueArray && initMultiSelectValueArray.length>0) {
                multiSelect.value(initMultiSelectValueArray); //like ["ISB", "FSA"]
            }
        });
    },

    getContent: function() {
        return this.content_;
    },

    findProfile: function(airportProfileWindowId) {
        var $kendoInfoWindow = $("#" + airportProfileWindowId);
        if ($kendoInfoWindow.length==1) {
            return $kendoInfoWindow.data('self');
        }
        return null;
    },
    showDirtyFormIndicator: function() {
        var dotObjectId = "nisis-object_" + facTypes[this.layerId_] + this.observable_.objectId;
        var $kendoInfoWindow = $("#" + dotObjectId);
        if ($kendoInfoWindow.length==1) {
              var $dirtyFormIndicator = $kendoInfoWindow.find("div.nisisProfileDirtyForm");
              $dirtyFormIndicator.show();
        }

    },

    mapExtendChangeHandler: function(evt) {
        var extent = evt.extent, zoomed = evt.levelChange;

        $(".nisis-profile-wrapper").each(function(ndx) {
          var $dotWindow = $( this ).find("div.kendoInfoWindow");
          if ($dotWindow.length>0) {
              var self = $dotWindow.data('self');
              if (self.line_  !== null) {
                  //remove the self.line from the map
                  nisis.ui.mapview.map.graphics.remove(self.line_);
                  //delete the line for now, then redraw it.
                  self.drawLineBetweenPoints(self);
              }
          }
        });
    },
    drawLine: function(evt) {
        //get the dot object window object
        var self = $(evt.sender.element.context).data('self');
        self.drawLineBetweenPoints(self);
    },
    drawLineBetweenPoints: function(self) {
        //draw line between the DOT Object graphic and its corresponding profile window
        var graphicMapPoint = [], arrayOfCorners=[], path = [], $profileWindow, dotObjectId,
            profileWindowOffset, profileWindowWidth, profileWindowHeight, screenPointTopLeft, screenPointTopRight, screenPointBottomLeft, screenPointBottomRight,
            graphicsScreenPoint, profileWindowScreenPoint, profileWindowClosestMapPoint, mapProfileWindowPoint=[],
            line, polyline, polylineSymbol;

        graphicMapPoint.push(self.mapPoint_.x);
        graphicMapPoint.push(self.mapPoint_.y);

        //find our window
        dotObjectId = "nisis-object_" +  facTypes[self.layerId_] +self.objectId_;
        if ($("#" + dotObjectId).length==0 || $("#" + dotObjectId).parents(".nisis-profile-wrapper").length==0) {
            //hm... not there?
            // console.log("drawLine - unable to find airport Object Profile Window" + dotObjectId);
            return;
        }

        $profileWindow = $("#" + dotObjectId).parents(".nisis-profile-wrapper");
        profileWindowOffset =  $profileWindow.offset();
        profileWindowWidth = $profileWindow.outerWidth();
        profileWindowHeight = $profileWindow.outerHeight();

        screenPointTopLeft = new Point(profileWindowOffset.left, profileWindowOffset.top, nisis.ui.mapview.map.spatialReference);
        screenPointTopRight = new Point(profileWindowOffset.left+profileWindowWidth, profileWindowOffset.top, nisis.ui.mapview.map.spatialReference);
        screenPointBottomLeft = new Point(profileWindowOffset.left, profileWindowOffset.top+profileWindowHeight, nisis.ui.mapview.map.spatialReference);
        screenPointBottomRight = new Point(profileWindowOffset.left+profileWindowWidth, profileWindowOffset.top+profileWindowHeight, nisis.ui.mapview.map.spatialReference);

        arrayOfCorners.push(screenPointTopLeft);
        arrayOfCorners.push(screenPointTopRight);
        arrayOfCorners.push(screenPointBottomLeft);
        arrayOfCorners.push(screenPointBottomRight);
        //convert to screenpoint
        graphicsScreenPoint = esri.geometry.toScreenPoint(nisis.ui.mapview.map.extent, nisis.ui.mapview.map.width, nisis.ui.mapview.map.height, self.mapPoint_);

        //find closest Point
        profileWindowScreenPoint =  this.findClosest(arrayOfCorners, graphicsScreenPoint);
        //convert from screen point to map point
        profileWindowClosestMapPoint = nisis.ui.mapview.map.toMap(profileWindowScreenPoint);

        mapProfileWindowPoint.push(profileWindowClosestMapPoint.x);
        mapProfileWindowPoint.push(profileWindowClosestMapPoint.y);

        //got points calculated, now just draw
        path.push(graphicMapPoint);
        path.push(mapProfileWindowPoint);
        polyline = new Polyline(nisis.ui.mapview.map.spatialReference);
        polyline.addPath(path);

        // create new Polyline Symbol
        polylineSymbol = new SimpleLineSymbol();
        polylineSymbol.setStyle(SimpleLineSymbol.STYLE_SHORTDASH);
        polylineSymbol.setColor(new dojo.Color([13, 121, 190, .9]));
        polylineSymbol.setWidth(3);

        line = new Graphic(polyline, polylineSymbol);
        if (self.line_  != null) {
            //remove the self.line from the map
            nisis.ui.mapview.map.graphics.remove(self.line_);
        }

        //add line to self and draw on the map
        self.line_ = line;
        nisis.ui.mapview.map.graphics.add(self.line_);
    },

    findClosest: function(a, point) {
        var lowestDistance = Number.MAX_SAFE_INTEGER;
        var closestPoint=0;
        var distance=0;
        for (var i = 0; i < a.length; i++) {
          distance = this.distance(a[i], point) ;
          if (distance < lowestDistance) {
              closestPoint = a[i];
              lowestDistance = distance;
          }
        }
        return closestPoint;
    },

    distance: function(point1, point2) {
        var xs = 0, ys = 0;
        xs = point2.x - point1.x;
        xs = xs * xs;

        ys = point2.y - point1.y;
        ys = ys * ys;

        return Math.sqrt( xs + ys );
    },

    draw: function() {
        var self =this;

        //see if window already exists - set focus on it if it does
        var dotObjectId = "nisis-object_" + facTypes[this.layerId_] + this.objectId_;
        if ($("#" + dotObjectId).length==1) {
            $("#" + dotObjectId).focus();
            return;
        }

        //convert tip to negative
        var kendoHeaderOffset=~this.tip_+1,
            ids = titleFields[this.layerId_],
            title = "<strong>" + facTypes[this.layerId_] + ": </strong>";
            //for airport only
            if (facTypes[this.layerId_] == 'APT') {
                var icao = findAirportProfileDataFieldValue(self.original_data_, "icao_id");
                var faaId = findAirportProfileDataFieldValue(self.original_data_, "faa_id");
                var icaoOrFaa = icao;
                if (icao == '') {
                    icaoOrFaa = faaId;
                }
                title += icaoOrFaa + " ";
            }
            //setup title
            for(var i = 0; i < ids.length; i++){
                var data = findAirportProfileDataFieldValue(self.original_data_, ids[i]);
                if(data != null && data != 'undefined' &&  data.length > 0 && data != ''){
                    title += data.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\? ])+/g, ' ') + " ";
                } else {
                    title += "";
                }
            }


        //then set up the window
        // kendoInfoWindow = $("<div class='kendoInfoWindow ' id='"+ dotObjectId+"'></div>").kendoWindow({
        kendoInfoWindow = $("<div class='kendoInfoWindow "+ facTypes[this.layerId_] +"' id='"+ dotObjectId+"'></div>").kendoWindow({
                              modal: false,
                              // title: "This is the title",
                              title: title,
                              /*maxHeight: 900,*/
                              resizable: false,
                              //draggable: false,
                              //pinned: true,
                              actions: ['Close'],
                              // position: {
                              //    left:  this.screenPoint_.x + "px",
                              //    top:  this.screenPoint_.y + kendoHeaderOffset + "px"
                              // },
                              animation: {
                                open: {
                                  duration: this.animationDuration_
                                }
                              },
                              close: function(e) {
                                if (self.line_  != null) {
                                    //remove the self.line from the map
                                    nisis.ui.mapview.map.graphics.remove(self.line_);
                                }
                                this.destroy();

                                //refreshes layers and map using map.setExtent(map.extent) function
                                nisis.ui.mapview.map.setExtent(nisis.ui.mapview.map.extent);
                              }
        });
        //remember it, save it in self data attribute
        $(kendoInfoWindow).data('self', this);

        //handle drag
        kendoInfoWindow.data("kendoWindow").dragging._draggable.bind("dragend", this.drawLine);
        var mapExtentChange = nisis.ui.mapview.map.on("extent-change", this.mapExtendChangeHandler);

        //move the line if needed
        var mapUpdateEnd = nisis.ui.mapview.map.on("update-end", this.updateGraphicLineHandler);

        //airport specific profile function
        var dotForm = this.buildNISISProfileForm();

        // $("<br/><br/>").appendTo(dotForm);

        dotForm.appendTo(kendoInfoWindow);

        //bind
        dotForm.find(".saveBtn").kendoButton({click: this.onSave });
        dotForm.find(".flyBtn").kendoButton({click: this.onZoom});
        dotForm.find(".resetBtn").kendoButton({click: this.onReset });


        //add on templated parts
        var forms = self.content;
        var profileWindow = $(kendoInfoWindow.data('kendoWindow').wrapper);
        var objectId = self.objectId_;
        var profileType = facTypes[this.layerId_];
        var uniqueObjId = facTypes[this.layerId_] + this.objectId_ ;

        var tpls,
            formsTpl,
            formsCount = 0,
            idname,
            formElements,
            formRender,
            qrsTpl,
            datatableRender,
            datatablesTpl,
            datatablesCount = 0;
        //  and whatever in the inner loop

        // forms
        formsTpl = kendo.template($("#profile_forms").html());
        for(var i in forms){

            if(forms[i].type == "form"){
            // TODO generate containers here
                var dropdowns = [],
                    formElements = forms[i].formElements,
                    formRender = kendo.render(formsTpl, formElements),
                    idname = Object.keys(forms)[formsCount];
                profileWindow.find("#form-" + idname + "-" + uniqueObjId).html(formRender);
                formsCount ++;

                forms[i].formElements.forEach(function(item){
                    if (item['type'] === "select"){
                         var isEnabled = item.disabled=='readonly overwritten';

                            $("[id="+ item.name.replace(/ /gi,'_') +"]").kendoDropDownList({
                            dataSource: item.selectOptions,
                            dataTextField: "label",
                            dataValueField: "option",
                            open: captureCurrentVal,
                            select: storeCurrentVal,
                            valueTemplate: '<span class="' + profileType + ' status-#: data.label.replace(/ /gi,\'_\').replace(\'/\',\'_\').replace(/,/gi,\'_\') #"></span><span>#: data.label #</span>',
                            template: '<span class="' + profileType + ' status-#: data.label.replace(/ /gi,\'_\').replace(\'/\',\'_\').replace(/,/gi,\'_\') #"></span><span>#: data.label #</span>',
                            height: 100,
                            enable: (item.disabled!='readonly overwritten')
                            });


                            // if(isEnabled){
                            //     var overwrittenItem = $(item);
                            //     console.log('YES! This is overwritten\nspan title: ', overwrittenItem.attr('name'), '\nspan: ', overwrittenItem, '\nLabel: ', overwrittenItem.next());
                            //     // $(item).attr('disabled', 'true');
                            // }

                    }
                });
            }
        }

        var nisisProfileSkeleton = nisisProfileSkeletons[this.layerId_];
        var datatables = nisisProfileSkeleton.datatables;
        var profileMenu =  nisisProfileSkeleton.profileMenu;
        var statuses =  nisisProfileSkeleton.statuses;


        //datatables
        for(var i in datatables){

                var  idname = Object.keys(datatables)[datatablesCount],
                    name = datatables[idname].name,
                    action = datatables[idname].action,
                    trackedObjId = self.observable_.nisisTrackedObjectId,
                    columns= datatables[idname].columns,
                    fields = datatables[idname].fields,
                    assoc_type = datatables[idname].assoc_type,
                    $datatable = $("#datatable-" + idname + "-" + uniqueObjId);

                //special case for runways - assoc_type depends on feature type (HELIPORT or AIRPORT)
                if (idname == "runways") {
                    assoc_type =  self.graphicId_.attributes.ARP_TYPE;
                }
                //initialize grid
                var gridProps = {};
                gridProps.action = action;
                gridProps.columns = columns;
                gridProps.fields = fields;
                gridProps.assoc_type = assoc_type;
                self.initGrid($datatable, gridProps, trackedObjId, self.isToolTipRequired(name));

                //add on a header
                $("#datatable-" + idname + "-" + uniqueObjId).prepend('<h3><hr class="med-blue2"/><div class="white med-dark-blue2 paddingR10px">' + datatables[i].name + '</div></h3>');
                datatablesCount ++;
        }

        //add last edited date
        var lastEditedDate = this.original_data_.last_edited_date;
        $(kendoInfoWindow).find('.last-updated').find('strong').html(lastEditedDate);


        //qsr menu
        var totes = this,
            data = this.observable_.fields,
            statuses_ = templates[this.layerId_].picklists[0];

       console.log("statuses_  .......",statuses_);

        var odd_statuses = ["ops_sl", "crt_rl", "crt_sl", "crt_s_lvl", "crt_secon_lvl"];
        var stss = {};
        for(var i in data){
            //all possible combination - how to recognize the status dbcols: xxx_status, xxx_sts, xxx_usts, etc
            if(i.match(/_status$/) !== null || i.match(/_sts$/) !== null || i.match(/_usts$/) !== null || $.inArray(i, odd_statuses) > -1){
                if(typeof statuses_[i] !== "undefined"){
                    statuses_[i].forEach(function(el, id, arr){
                        if(data[i] === el.value){
                            statuses.forEach(function(_el, _id, _ar){
                                if(_el.dbcol === i && data[i] !== ""){
                                    _el.status_text = el.label; //push
                                }
                            });
                        }else if(data[i] === ""){
                            statuses.forEach(function(_el, _id, _ar){
                                if(_el.dbcol === i && data[i] === ""){
                                    _el.status_text = "Unknown"; //push
                                }
                            });
                        }
                    });
                }
            }
        }


        qsrMenuTpl =  kendo.template($("#quick_status_list").html());
        profileWindow.find("menu.qsr").html(kendo.render(qsrMenuTpl, statuses));

        $(kendoInfoWindow).each(function(){
            var getID = $(this).attr('id'),
            ID = '#' + getID,
            $qsr_menu = $(ID).find('.qsr-menu-container'),
            $menu = $(ID).find('menu.qsr'),
            $link2status = $(ID).find('menu.qsr > menuitem'),
            $scrollToTop = $(ID).find('.backtotop'),
            $qsrScrollMore = $(ID).find('.qsr-more-scroll');

            // toggle qsr show more indicator
            var menuItemLength = $link2status.length;

            console.log(menuItemLength)

            if(menuItemLength > 9){
                console.log('yes')
                    $qsrScrollMore.fadeIn('slow');
            }

            $menu.on('scroll', function(e){
                var $menu = $(e.target),
                    $cont = $menu.parent(),
                    scrollTop = $menu.scrollTop(),
                    $scrollToTop = $cont.find('.backtotop'),
                    $qsrScrollMore = $cont.find('.qsr-more-scroll');
                if(scrollTop == 0){
                    $qsrScrollMore.fadeIn('slow');
                    $scrollToTop.hide();
                } else {
                    $qsrScrollMore.fadeOut('slow');
                    $scrollToTop.fadeIn('slow');
                }

            })

            $qsrScrollMore.on('click', function(e){
                var $this = $(e.currentTarget);
                var $cont = $this.parent();
                var $menu = $cont.find('menu.qsr');
                $menu.animate({scrollTop:$menu.height()}, 'slow');
            });

            $scrollToTop.on('click', function(e){
                var $this = $(e.currentTarget);
                var $cont = $this.parent();
                var $menu = $cont.find('menu.qsr');
                $menu.animate({scrollTop:0}, 'slow');
            });

        })




        var profileMenuForAirport = JSON.parse(JSON.stringify(profileMenu));
        $.each(profileMenuForAirport,function(i,menuItem){
            if (menuItem.hasOwnProperty("target")) {
                var temp = menuItem.target + '-' +uniqueObjId;
                menuItem.target = temp;
            }
        });

        profilesMenuTpl =  kendo.template($("#profiles_list").html());
        profileWindow.find("menu.profiles").html(kendo.render(profilesMenuTpl, profileMenuForAirport));

        //done adding on templated parts, now bind it
        kendo.bind(dotForm, self.observable_);

        //add on handlers and hooks   (vars.js & handlers.js)
        profileWindow = dotForm;
        var $edit = profileWindow.find('.btn-edit'),
        $closeModule = profileWindow.find('.btn-close-module'),
        $resizeModule = profileWindow.find('.btn-resize-module'),
        $closeFormsNav = profileWindow.find('.btn-close-nav-forms'),
        $toggleQSR = profileWindow.find('.btn-qsr'),
        $toggleProfiles = profileWindow.find('.btn-profiles'),
        $toggleSnap = profileWindow.find('.btn-snapshots'),
        $input = profileWindow.find('input[type=text]'),
        $textArea = profileWindow.find('textarea'),

        $clear = profileWindow.find('#clear'),
        $qsr_menu = profileWindow.find('.qsr-menu-container'),
        $profiles_menu = profileWindow.find('.profiles-menu-container'),
        $link2profile = profileWindow.find('menu.profiles').find('menuitem'),
        $link2status = profileWindow.find('menuitem > .qsr-mini-toolbar > i.fa.fa-external-link-square'),
        $numericInputs = profileWindow.find('.numeric'),
        // $numericInputFocusEl = $numericInput.prev(),
        $select = profileWindow.find('select'),
        $status_explanation = profileWindow.find("[id*='_Explanation']"),
        $status_notes = profileWindow.find("[id*='_Notes']"),
        $cont = profileWindow.find('div.col-1-1, div.col-1-2, div.col-1-3, div.col-1-4').find('.content'),
        $personnelTotal = profileWindow.find("[id='Personnel_Accounted_for_Total']"),
        $assignedPersonnelTotal = profileWindow.find("[id='Assigned_Personnel_Total']"),
        toggleList = {
                "btn-qsr": $qsr_menu,
                "btn-profiles": $profiles_menu
        };

        //bind events
        $toggleQSR.on('click',toggleMenu);
        $toggleProfiles.on('click', toggleMenu);
        $toggleSnap.on('click', toggleMenu);
        $resizeModule.on('click', toggleModuleCollapse);
        $closeModule.on('click', closeModule);
        $closeFormsNav.on('click', toggleMenu);
        $edit.on('click', edit);
        $clear.on('click', clearInput);
        $link2profile.on('click', route2profile);
        $link2status.on('click', jump2status);
        $.each($numericInputs,function(i,numericInput){
            $(numericInput).on('focus blur', paperCSSFocus);
            var $el = $(numericInput).parent();
            var $label = $el.find('label');

            if($label.attr('readonly')){
                $el.children().addClass('num-readonly');
            }
        });


        $select.on('change',handleSelect); //< - FB: 12/12 needed?

        $cont.hover(showTooltip);

        //navs.js
        profileWindow.find('menu.profiles').find('menuitem').on('click',function(e){
            //Jon - before you would only click on the anchor I changed it so now the click event is on the menuitem and then it looks for the anchor, gets the hash and so on.
            var $anchor = $(e.currentTarget).find('a'),
                getHash = $anchor.prop('hash'),
                profileWindow = $anchor.closest('.nisis_profile'),
                isDataTable =  JSON.parse($anchor[0].attributes[1].value),
                $forms = profileWindow.find('.forms-wrap'),
                $datatables = profileWindow.find('.datatables-wrap'),
                $target = $(getHash), //irena - make sure hash containes the -uniqueObjId part to jump to correct section of the correct window
                //see if section has dataTables
                sectionHasGrid = ($target.has(".section-grid").length != 0),
                isDataTable = isDataTable || sectionHasGrid;

                e.preventDefault();

                toggleModuleWidth(e,isDataTable);
                $forms.find('.showProfile').removeClass('showProfile');
                $datatables.find('.showProfile').removeClass('showProfile');
                $target.addClass('showProfile');

                //highlight current section
                profileWindow.find("menuitem").removeClass('current');
                $anchor.parent().addClass('current');

                //see if there are any datatable grids in this sections
                $target.find(".grid").addClass("showProfile");

                //scroll to the top of the section
                $target.animate({ scrollTop: (0) }, 'fast');
        });


        //look for multiselects
        var $multiselects = profileWindow.find('select.multiselect');
        $.each($multiselects,function(i,multiselect){
            var //multiSelect =  $(multiselect).data("kendoMultiSelect"),
                bindTo = $(multiselect).attr("bind-to"),// like res_dsg_ex
                data = findAirportProfileMultiOptions(self.template_, bindTo);//like "ISB, JRSOI"
            var initMultiSelectValue = findAirportProfileDataFieldValue(self.original_data_, bindTo);
            var initMultiSelectValueArray = initMultiSelectValue.split(',');
            var disabledAttr = $(multiselect).attr("disabled");

            var multiSelect = profileWindow.find(".multiselect").kendoMultiSelect({
                dataSource: data,
                change: function(e){
                    var selectedValue = this.value(),
                        bindTo = this.element.attr("bind-to"); //what is the dbcol its bound to? Like, res_dsg_ex
                    self.observable_.fieldsChanged[bindTo]=selectedValue.toString();  //like  "ISB, JRSOI";
                    //put up dirty message
                    profileWindow.find("div.nisisProfileDirtyForm").show();
                },
                enable: (disabledAttr!='readonly')
            }).data("kendoMultiSelect");
            //set the initial value
            multiSelect.value(initMultiSelectValueArray); //like ["ISB", "FSA"]
        });


        //look for each grid and hook up the kendo grids. Some of the forms have datatables in them, like Notams section
        var bareGrids = profileWindow.find(".toHookUp");
        $.each(bareGrids,function(i,gridToHookUp){
            var classList = gridToHookUp.classList;
            for (var j=0; j<classList.length; ++j) {
                if (classList[j].startsWith("datatable-")) {
                    className = classList[j];
                    break;
                }
            }
            var objId = self.observable_.nisisTrackedObjectId;
            var gridProps = self.findGridProperties(className);

            if (gridProps!=null && gridProps.action == "get_iwl_names") {
                gridProps.assoc_type = facTypesIWL[self.layerId_];
                objId = self.graphicId_.attributes.OBJECTID;
            }
            if (gridProps!=null) {
                //see if we are required to put the tooltip
                self.initGrid($(gridToHookUp), gridProps, objId, self.isToolTipRequired(className));
            }
            else {
                console.log("grid configuration is messed up");
            }
        });

        //so far, this is for airport only
        if (facTypes[this.layerId_] == 'APT') {
            //look for each link wrapper - add on airport ids for all. Some of the forms have datatables in them, like Notams section
            var bareLinks = profileWindow.find(".link-wrapper a");
            var airportObjectId = self.objectId_,
                icao = self.graphicId_.attributes.FAA_ID;
            $.each(bareLinks,function(i,link){
                var a_href = $(link).attr('href');
                var linkName = $(link).attr('name');
                if (linkName == 'AirNav') {
                    a_href = a_href + icao;
                } else if (linkName == 'AeroNav') {
                    a_href = a_href + "?cycle=1612&ident=" + icao;
                } else if (linkName == 'NFDC'){
                    a_href = a_href + "?airportId=" + icao;
                }
                $(link).attr('href',a_href);
            });
        }

         //hook up percentage calculator
        //calculate personnel percentage if applicable
        self.calcPersonnelPercentage(self, profileWindow);

        //hook up validation for numeric fields
        var numericFields = profileWindow.find(".numeric");
        $.each(numericFields,function(i,numericInput){
            var format =  $(numericInput).attr('format');
            var decimals = Number($(numericInput).attr('decimals'));
            $(numericInput).kendoNumericTextBox({
                round: false,
                decimals: decimals,
                max: 99999,
                min: -99999,
                format: format
                //decimals: 8,
                //format: "#.########"
            });

        });


        //hook up alerts and history per status
        self.hookUpStatusAlertsAndHistory(profileWindow, statuses);

        //hook up Files upload
        self.hookUpFilesUpload(profileWindow);

        //disable globally readonly fields (for example fields from the different profile that are displayed for information purposes on this profile)
        self.setGlobalReadOnlyFields(profileWindow);

        //hook up lat and lon validator
        self.setupLatitudeValidator(profileWindow);
        self.setupLongitudeValidator(profileWindow);




        //hackish for Airports only
        if (facTypes[this.layerId_] == 'APT') {
            //Associated ATMS - setup tower present and airport tower type
            if (self.original_data_.hasOwnProperty("ans_ops")  && self.original_data_.ans_ops.hasOwnProperty("formData") && self.original_data_.ans_ops.formData[0].hasOwnProperty("associated_atms")) {
                var atms = self.original_data_.ans_ops.formData[0].associated_atms.formDataTable;
                if (atms.length == 3) {
                    if (atms[0].hasOwnProperty('tower')) {
                       profileWindow.find( "input[name='Airport Tower Present']").val('Yes');
                    }
                    if (atms[1].hasOwnProperty('appr_ctrl')) {
                        var towerId=atms[1].appr_ctrl.basic_tp;
                        // console.log( "towerId.", towerId );
                        profileWindow.find( "input[name='Airport Tower Type']").val(towerId);
                    }
                }
            }
        }

        //extracting associated atms (for Airport Profile i believe? - need to redo)
        var all_assoc_atms = [];
        for(i in atms){
            for(j in atms[i])
            //fitsum, whats a swap???
            if(i !== "swap" && atms[i][j].hasOwnProperty("atm_id") && atms[i][j].hasOwnProperty("basic_tp") && atms[i][j].hasOwnProperty("lg_name") && atms[i][j].hasOwnProperty("ops_status_label")){
                all_assoc_atms.push(atms[i][j])
            }
        }

        $(".assoc-atm-grid").kendoGrid({
               dataSource: {
                 data: all_assoc_atms
             },
             sortable: true,
             pageable: {
                  pageSize: 10,
                  /*refresh: true, */
                  buttonCount: 10
              },
                columns:[
                    {
                        field: "atm_id",
                        title: "ATM ID",
                },
                {
                    field: "basic_tp",
                    title: "Basic Type",
                },
                {
                    field: "lg_name",
                    title: "Long Name",
                },
                {
                    field: "ops_status_label",
                    title: "Ops Status",
                    // hidden: true
                    template: "<span class='status-#: ops_status_label.replace(/ /gi,\'_\') #'>#: ops_status_label #</span>"
                },

            ]
         });



        var textAreas = profileWindow.find("textarea[id*='_Explanation'], textarea[id*='_Notes']");

           $.each(textAreas,function(i,textArea){
               //console.log( "hooking up to .",  $(textArea) );
               var col =  $(textArea).attr('data-bind');
               var parent = $(textArea).parent();
               col = col.replace('value:fields.','');
               var sigUser = findAirportProfileDataFieldValue(self.original_data_, col+".last_edited_user");
               var sigDate = findAirportProfileDataFieldValue(self.original_data_, col+".status_date");
               if (sigUser != null && sigDate != null && sigUser != '' && sigDate != '') {
                 //  console.log("hooking up signature for the ", col);
                   var sig = $('<span class="sig">Entered by <strong>'+sigUser+ '</strong> on ' + sigDate + '</span>');
                   sig.insertAfter($(parent));
               }
           });

           //add on
        $.getScript( "html/profiles_fr23/js/ui.js", function( data, textStatus, jqxhr ) {
          // console.log( "ui.js loaded successfully." );
        });


        //add nisis-profile-wrapper class to the closest k-widget k-window
        $(kendoInfoWindow.data('kendoWindow').wrapper).addClass("nisis-profile-wrapper");

        //open the window
        kendoInfoWindow = kendoInfoWindow.data('kendoWindow').open();
        //kendoInfoWindow = kendoInfoWindow.data('kendoWindow').center().open();

        //draw the line
        //wait until kendo finishes window animation so the window coordinates are all set. Animation set to 100, give it 100 more
        setTimeout(function(){
              self.drawLineBetweenPoints(self);
        }, self.animationDuration_+100);

    },


    isToolTipRequired: function(gridName) {
        var result = false;

        var whichProfile = facTypes[this.layerId_];

        if ( (whichProfile == 'APT' && (gridName == "Runways" || gridName == "datatable-other-rmls" || gridName == "datatable-open-rmls" || gridName == "datatable-eng-rates" || gridName == "datatable-arr-rates") )
          || (whichProfile == 'ANS' && (gridName == "datatable-other-rmls" || gridName == "datatable-open-rmls" ))
            )  {
            result = true;
        }
        return result;
    },

    //hooks up kendo grid
    initGrid: function(gridPlaceholder, gridProps, objectId, tooltipNeeded) {
        //console.log("initGrid called", gridProps);
        var query = gridProps.action, columns = gridProps.columns, fields = gridProps.fields, assoc_type = gridProps.assoc_type;
        var dataSource = null;
        //irena - if action is provided, use transport, otherwise datasource is already supplied with json
        if (query=="json") {
            //get to the right place where json is inside the profile; like XPath
            var root = this.original_data_;
            var json_offset = assoc_type; //for example, "supp_apt_rwy_proc.formDataTable.0.supp_apt_rwy_proc_list";
            path = json_offset.split('.');
            for(var i = 0; i < path.length; i++) {
                root = root[path[i]];
            }
            var jsonGridData = root;
            //console.log(jsonGridData);

            dataSource =  new kendo.data.DataSource({
                data: {
                    "items" :jsonGridData
                  },
                  schema: {
                    data: "items"
                  }
                  ,pageSize: 10
            });
        }
        else {
                    dataSource =  new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: "api/",
                            type: "POST",
                            dataType: "json",
                        },
                        parameterMap: function(data, type) {
                            if (type === 'read') {
                                // console.log(" got read in parameterMap");
                                data.action = query;
                                data.id = objectId;
                                data.assoc_type  = assoc_type;// like "arpt-rmls-open" or empty for remarks and notams ;
                                return data;
                            }
                            else {
                                // console.log("not a read type in parameterMap");
                            }
                        }
                    }
                    ,schema: {
                        model: {
                            fields: fields
                        },
                        data: "data", //get_remarks.php returns data
                        total: function (response) {
                            // console.log("total: ", response.data.length);
                            if (response && response.result == 'Success') {
                                return response.data.length;
                            }
                            else {
                                console.error("Error getting data for the grid: ", response);
                                return 0;
                            }
                        }
                    }
                    ,pageSize: 10
                    // ,serverPaging: true
                    // ,serverFiltering: true
                    // ,serverSorting: true
                });
        };



                //console.log("the length of the datasource is: ", dataSource.length);
                var $datatable = gridPlaceholder,
                    columns = columns;

                var grid = $datatable.kendoGrid({
                    dataSource: dataSource,
                    dataBinding: function(e) {
                        //console.log("dataBinding", e.items);
                    },
                    dataBound: function(e) {
                        // console.log("dataBound");
                        var grid = e.sender;
                        //_prestineData in the grid?
                        // console.log("grid.dataSource.total() ", grid.dataSource.total() );
                        if (grid.dataSource.total() == 0) {
                            var colCount = grid.columns.length;
                            $(e.sender.wrapper)
                                .find('tbody')
                                .append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data">There is no data to show in the grid.</td></tr>');
                        }
                        //a bit hardcoded, need to set it up better in template that this grid has an extra dataBound function
                        //may be better check $(e.sender.wrapper).attr("id").startsWith("datatable-arpt-gbpa");
                        if (e.sender.wrapper.find('img.gbpa-icon').length>0) {
                            dataBoundGBPA(e);
                        }
                    },
                    columns: columns,
                    // filterable: true,
                    // scrollable: true,
                    sortable: {
                            mode: "single",
                            allowUnsort: false
                    },
                    autoBind: false,
                    pageable: {
                        pageSize: 10,
                        /*refresh: true, */
                        buttonCount: 10
                    }
                });

                //tooltip if needed
                if (tooltipNeeded) {
                   //console.log('these are the columns....', columns, 'these are the columns....', fields);
                    var dataTooltip = $datatable.find('.k-grid-content').kendoTooltip({
                        filter: "tr",
                        content: toolTip,
                        autoHide: false,
                        showOn: "click",
                        show: function (e) {
                            var $el = $(this.popup.element);

                            $el.scrollTop(0)
                                .addClass("runway-tooltip") //add awesome new style that is waayyyyyy better
                                .removeClass("k-widget k-tooltip"); //remove bs kendo crap
                        },
                        animation: {
                            open: {
                                effects: "fade:in",
                                duration: 500
                            },
                            close: {
                                effects: "fade:out",
                                duration: 500
                            }
                        }
                    }).data("kendoTooltip");

                    var hoverTip = $datatable.find('.k-grid-content').kendoTooltip({
                        filter: "tr",
                        content: "<i class='fa fa-info-circle' /></i> Click for info",
                        showOn: "mouseenter",
                        autoHide: true,
                        show: function(e){
                            var $el = $(this.popup.element);
                            var $tip = $('div.k-callout.k-callout-n');
                            var styles = {
                                'box-shadow': 'none',
                                'border': 'none',
                                'background': 'rgba(0,0,0,0.7)',
                                'color': '#fff',
                                'border-radius': '3px',
                                'width': '110px',
                                'height': 'auto',
                                'text-align':'center',
                                'padding': '5px',
                            };

                            $tip.css({'display': 'none'}); //remove tip on tooltip
                            $el.removeClass("k-widget k-tooltip"); //remove kendo styles
                            $el.css(styles); //custom styles

                            //check if first td has data, change content if so.
                            var firstTr = $(e.sender.element).find('tr')[0];
                            var firstTd = $(e.sender.element).find('td')[0];

                            if($(firstTd).hasClass('no-data')){
                                $el.html("<i class='fa fa-info-circle' /></i> No Data");
                                $(firstTd, firstTr).on('click', function(e){
                                    e.stopPropagation();
                                });
                            }

                            $('tr').mousemove(function(e){
                                var offsetOps = {
                                    left: e.pageX + 20,
                                    right: e.pageY,
                                    top: e.pageY
                                };

                                $el.offset(offsetOps);
                            });

                            //hide if other tooltip is showing
                            $("tr").on("click", function(){
                                hoverTip.hide();
                            });

                        }
                    }).data("kendoTooltip");

                    function toolTip(e){
                        // console.log("building tooltip");
                        var target = $(e.target);
                        var tr = target;
                        var tds = tr.find('td');
                        var table = $("<table/>");
                        var dataCol1 = [];
                        var dataCol2 =[];

                        for(var i = 0; i < columns.length; i++){
                            var colTitle = columns[i].title;
                            dataCol1.push(colTitle);
                        }

                        // console.log('these are the TDs...', tds.parent());

                        $.each(tds, function(i,td) {
                            var row = $("<tr/>");
                            if($(td).text() == 'null'){
                                $(td).html(' ');
                            }
                            row.append($("<td/>").text(dataCol1[i]));
                            row.append($("<td/>").text($(td).text()));
                            table.append(row);
                        });
                        // console.log("building tooltip - tds", dataCol2)

                        var tableHtml = table.html();
                        return tableHtml;//"content for this id";
                    }
                }
            dataSource.read();

            //sort
            var sortField = this.findFirstSortableColumn(columns);
            if (sortField!=null) {
                dataSource.sort({field: sortField, dir: "asc"});
            }
    },

    findFirstSortableColumn: function (columns) {
        var sortField = columns[0].field;
        //go thru and find first column that is not hidden
        $.each(columns,function(i,column){
            if (column.hasOwnProperty("hidden")) {
                if (column.hidden == false) {
                    sortField=column.field;
                    return false;
                }
            }
            else {
                sortField = column.field;
                return false;
            }
        });

        return sortField;
    },

    findGridProperties: function (className) {
        var gridProperties = null;
        //go thru and add on the data
        $.each(this.content,function(i,section){
            if (section.hasOwnProperty("formElements")) {
                var sectionsFormElements = section.formElements;
                $.each(sectionsFormElements,function(j,formElement){
                    if (formElement.hasOwnProperty("type") && formElement.type=="grid" && formElement.datatableClass == className) {
                        gridProperties =  formElement;
                    }
                });
            }
        });

        return gridProperties;
    },

    hookUpStatusAlertsAndHistory: function(profileWindow, statuses) {
        var self = this;
        var feature = this.graphicId_;
        var layerId = this.layerId_;
        var facIds= {
                    'airports_pr': this.graphicId_.attributes.FAA_ID,
                    'airports_others': this.graphicId_.attributes.FAA_ID,
                    'airports_pu': this.graphicId_.attributes.FAA_ID,
                    'airports_military': this.graphicId_.attributes.FAA_ID,
                    'atm': this.graphicId_.attributes.ATM_ID,
                    'ans1': this.graphicId_.attributes.ANS_ID,
                    'ans2': this.graphicId_.attributes.ANS_ID,
                    'osf': this.graphicId_.attributes.OSF_ID,
                    'tof': this.graphicId_.attributes.TOF_ID
                    };
        var facId = facIds[layerId];
        var facType = facTypes[layerId];
        var oid = feature.attributes.OBJECTID;
        var alert = profileWindow.find('.alertsButton');
        if(alert){
            alert.each(function(i, element) {
                $(element).click(function(evt){
                    nisis.alerts.oid = feature.attributes.OBJECTID;
                    nisis.alerts.facId = facId;
                    nisis.alerts.facType = facType; //"APT";

                    var label = $(element).parent().parent().find(".qsr-label").text();//find next qsr-label
                    var field = findAirportProfileDataDbColByName(self.template_,label); //find itema and the col name that its bound to


                    nisis.alerts.statusType = field.statusId; //SELECT A.STATUS_ID AS ID, A.STATUS_COLUMN, A.STATUS_FULLNAME, A.FAC_TYPE  FROM FAC_STATUS_TYPE_VW A where A.FAC_TYPE = 'APT' order by A.STATUS_ID

                    var alertsGrid = $("#alertTable").data('kendoGrid');
                    var dataSource = alertsGrid.dataSource;
                    var newRowFilter = {field: "ALERTID", operator: "eq", value: 0};
                    var objFilter = { field: "OBJECTID", operator: "eq", value: nisis.alerts.oid};
                    var facIdFilter = { field: "FAC_ID", operator: "eq", value: nisis.alerts.facId };
                    var facTypeFilter = { field: "FAC_TYPE", operator: "eq", value: nisis.alerts.facType };
                    var statusFilter = { field: "STATUS_TYPE_ID", operator: "eq", value: nisis.alerts.statusType };

                    var facTypeAndStatusFilter = {
                        logic: "and",
                        filters: [ objFilter, statusFilter]
                    }

                    //clearing the filters
                    dataSource.filter({});

                    //Adding the facType and statusType filters
                    dataSource.filter({
                        logic: "or",
                        filters: [newRowFilter, facTypeAndStatusFilter]
                    });
                    dataSource.cancelChanges();

                    var alertsWindow = $('#alerts-div').data('kendoWindow');
                    dataSource.read();
                    alertsWindow.restore();
                    alertsWindow.center().open();

                    $('.k-grid-toolbar').show();
                    $("#alertTable").removeClass("hideGrid");
                    $("#readOnlyGrid").addClass("hideGrid");
                });
            });
        }//end if(alert)


        var statusHistory = profileWindow.find('.statusHistoryButton');
        if(statusHistory){
            statusHistory.each(function(i,element){
                $(element).click(function(evt){
                    var label = $(element).parent().parent().find(".qsr-label").text().trim();//find next qsr-label
                    var field = findAirportProfileDataDbColByName(self.template_, label).dbcol; //find col name that its bound to
                    historyNav.openFieldHistory(facId, field.toUpperCase(), label, "explanation");

                    //supplemental for notes
                });
            });
        };

        //notes history
        var notesHistory = profileWindow.find('.notesHistoryButton');
        if(notesHistory){
            notesHistory.each(function(i,element){
                 $(element).click(function(evt){
                    var label = $(element).parent().parent().find('label').text().trim();//find next qsr-label
                    var field = findAirportProfileDataDbColByName(self.template_, label).dbcol; //find col name that its bound to
                    historyNav.openFieldHistory(facId, field.toUpperCase(), label, "supplemental");
                });
            });
        };



        var facHistory =  profileWindow.find('.btn-history');
        if(facHistory){
            facHistory.each(function(i,element){
                $(element).click(function(evt){
                    historyNav.openFacilityHistory(facId);
                });
            });
        };


        //iwl - action = iwlAddButton
        var iwlAdd = profileWindow.find('button');
        //var iwlAdd = profileWindow.find("button[name='IWL Membership']");
        if(iwlAdd){
            iwlAdd.each(function(i,element){
                if ($(element).attr('action') == 'iwlAddButton') {
                    $(element).click(function(evt){
                        self.addToIWL(facTypesIWL[self.layerId_], oid, profileWindow);
                    });
                }
            });
        };
    },


    setGlobalReadOnlyFields: function($kendoInfoWindow) {
        //set globally defined read-only fields as read-only
        var nisisProfileSkeleton = nisisProfileSkeletons[this.layerId_];
        var readonlyFields =  nisisProfileSkeleton.readonlyFields;
        if (readonlyFields != undefined) {
            for (var j=0; j<readonlyFields.length; ++j) {
                if (readonlyFields[j].hasOwnProperty("dbcol")) {
                    var selectorDbCol = readonlyFields[j].dbcol;
                    var selector = "[data-bind='value:fields." + selectorDbCol +"']";
                    var $field = $kendoInfoWindow.find(selector);
                    var $label = $field.next();
                    //disable the field and the label
                    $label.attr('readonly','readonly');
                    $field.attr('readonly','readonly');
                }
            }
        }
    },

    setupLongitudeValidator: function($kendoInfoWindow) {

        var $lon = $kendoInfoWindow.find("input[data-bind='value:fields.longitude']");

        var longitudeValidator = $lon.kendoValidator({
            rules: {
              range: function(input) {
                var min = -180;
                var max = 180;
                var value = parseFloat($(input).val(), 10);

                if (isNaN(min) || isNaN(max) || isNaN(value)) {
                  return true;
                }

                return min <= value && value <= max;
              }
            }
            ,
            messages: {
              range: function(input) {
                 return kendo.format("The longitude should be <br> between -180 to 180.")
              }
            }
        }).data("kendoValidator");

        this.longitudeValidator_ = longitudeValidator;
    },

    setupLatitudeValidator: function($kendoInfoWindow) {

        var $lon = $kendoInfoWindow.find("input[data-bind='value:fields.latitude']");

        var latitudeValidator = $lon.kendoValidator({
            rules: {
              range: function(input) {
                var min = -90;
                var max = 90;
                var value = parseFloat($(input).val(), 10);

                if (isNaN(min) || isNaN(max) || isNaN(value)) {
                  return true;
                }

                return min <= value && value <= max;
              }
            }
            ,
            messages: {
              range: function(input) {
                 return kendo.format("The latitude should be <br> between -90 to 90.")
              }
            }
        }).data("kendoValidator");

        this.latitudeValidator_ = latitudeValidator;
    },

    getThumbnail: function(original, scale) {
      var canvas = document.createElement("canvas")

      canvas.width = original.width * scale
      canvas.height = original.height * scale

      canvas.getContext("2d").drawImage
        (original, 0, 0, canvas.width, canvas.height)

      return canvas
    },

    findFileByName: function(fileName, files) {
        var result = null;
        if (files!=undefined && files.length>0) {
            for (var i = 0; i < files.length; i++) {
                if (files[i].NAME == fileName) {
                    return files[i];
                }
            }
        };
        return result;
    },

    findLiByName: function($lis, fileName) {
        var result = null;
        $.each( $lis, function( key, value ) {
            if (fileName == $(this).find('.k-filename').text()) {
                result = $(this);
                return false;
            }
        });
        return result;
    },

    limitThumbnailsPromotionByDisablingCheckboxes: function(profileWindow){ 
        var maxThumbnailsOnBasicInfoSection = 1;
        if (this.layerId_.startsWith("airport")){
            maxThumbnailsOnBasicInfoSection=2;
        }
        if (profileWindow.find(".files-wrap input[type=checkbox]:checked").length == maxThumbnailsOnBasicInfoSection){
           profileWindow.find('.files-wrap input[type=checkbox]:not(:checked)').prop('disabled', true);   
        } else {
            profileWindow.find('.files-wrap :checkbox:not(:checked)').prop('disabled', false);            
            profileWindow.find('.files-wrap input[type=checkbox]:not(:checked)').prop('disabled', false);       
        }    
    },

    initUploadWidget: function(initialFilesRetrieved, $profileWindow, $filesWrap, $files, feature ) {
        var self = this;
        var layerId = this.layerId_;
        var facTable = facTables[layerId];

        var faaId = feature.attributes.FAA_ID;
        var objectId = feature.attributes.OBJECTID;

        
        //a bit of massaging from the files retrieved
        //should be like
        //[{"name":"00001AD.PDF","extension":".PDF","size":123909},{"name":"00001IL35R.PDF","extension":".PDF","size":289016},{"name":"00002HILX34.PDF","extension":".PDF","size":114872},{"name":"infowindow_720.jpg","extension":".jpg","size":142515}]
        var initialFiles = [];
        if (initialFilesRetrieved!=undefined && initialFilesRetrieved.length>0) {
            for (var i = 0; i < initialFilesRetrieved.length; i++) {
                var str = initialFilesRetrieved[i].EXTENSION;
                var fileExt = str.substr(str.indexOf("."));
                var current = {
                    // NISIS-HOTFIX, KOFI HONU - Added FILEID so that files are tracked by primary key.  Avoids potential bugs later.
                    fileid:initialFilesRetrieved[i].FILEID,
                    name:initialFilesRetrieved[i].NAME,
                    extension: fileExt,
                    size: initialFilesRetrieved[i].BLOBSIZE,
                    isPromoted: initialFilesRetrieved[i].ISPROMOTED
                }
                initialFiles.push(current);
            }
        };

        // console.log("Uploaded files:", initialFiles);
        //now initialized Kendo Upload widget
        var kendoUploadWgt = $files.kendoUpload({
            multiple: true,
            async: {
                    saveUrl: "api/image_upload.php",
                    removeUrl: "api/image_remove.php",
                    // autoUpload: true
            },
            select: function (e) {
                var filesSelectedLen = e.files.length;
                var filesUploadedLen = $filesWrap.find('li').length;
                // console.log("number of filesSelected", filesSelectedLen);
                 console.log("number of filesSelected", filesSelectedLen);
                // console.log("number of files already there", filesUploadedLen);
                var len = filesSelectedLen + filesUploadedLen;
                if (len>3) {
                    var msg = "Only up to 3 files can be uploaded";
                    nisis.ui.displayMessage(msg, 'error');
                    e.preventDefault();
                    return false;
                };

                $.each(e.files, function (index, value) {
                    // console.log("Name: " + value.name);
                    // console.log("Size: " + value.size + " bytes");
                    // console.log("Extension: " + value.extension);

                    if (!((value.extension.toLowerCase() == ".gif") ||
                            (value.extension.toLowerCase() == ".jpg") ||
                            (value.extension.toLowerCase() == ".jpeg") ||
                            (value.extension.toLowerCase() == ".pjpeg") ||
                            (value.extension.toLowerCase() == ".x-png") ||
                            (value.extension.toLowerCase() == ".png") ||
                            (value.extension.toLowerCase() == ".pdf") )) {
                        var msg = "Only .gif, .jpg, .jpeg, .pjpeg, .x-png, .png and .pdf files can be uploaded";
                        nisis.ui.displayMessage(msg, 'error');
                        e.preventDefault();
                        return false;
                    }

                    if (value.size > 8388608) {
                        var msg = "Maximum allowed file size is 8MB";
                        nisis.ui.displayMessage(msg, 'error');
                        e.preventDefault();
                        return false;
                    }

                    //see if file already exist
                    var fileExists = self.findLiByName($filesWrap.find('li.k-file'), value.name);
                    if (fileExists != null) {
                        var msg = "File(s) you are trying to upload has already been uploaded. Please choose different file(s)";
                        nisis.ui.displayMessage(msg, 'error');
                        e.preventDefault();
                        return false;
                    }
                });

            },
            remove: function (e) {                
                var uid= e.files[0].uid;  
                var entry = $(".k-file[data-uid='" + uid + "']");
                var input = entry.find("input[type='checkbox']");
                var fileId = input.attr("fileid");
                var isChecked =  $(input).prop("checked");

                console.log('remove ', e.files[0].uid);
                console.log('remove ', fileId);

                if (isChecked == true && self.reallyDelete == false ) {
                    //lets see if the checkbox is checked and we need to put warning                    
                    
                    e.preventDefault();                
                   
                    nisis.ui.displayConfirmation({
                        message: 'Are you sure you want to delete this uploaded file? It will also be removed from the Basic Information Section.',
                        buttons: ["REMOVE", "CANCEL"]
                    }).then(function(response){
                        if(response === 'REMOVE'){
                            self.reallyDelete = true;                        
                            entry.find( ".k-delete" ).trigger( "click" );
                        } 
                    },function(error){
                        nisis.ui.displayMessage('The app could not display confirmation dialog.', '', 'error');
                    });
                }
                else { 
                    e.data = {fileid: fileId};                     
                    success: onSuccess 
                }                  
            },
            cancel: function (e) {               
                // Process the Cancel event                            
                $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                          "action": "image_cleanup",                    
                          "objectid": objectId,
                          "filename": e.files[0].name,
                          "facTable": facTable
                    },
                    success: function(data, status, req) {
                        //do what kendo's _hideHeaderUploadstatus() does, ie clean up the total indicator, in this case Done with a check           
                        $filesWrap.find(".k-upload-status-total").remove();
                    },
                    error: function(req, status, err){
                        console.error("ERROR in image_cleanup");
                    }
                });
            },
            upload: function (e) {
                //payload that gets send with ajax on upload
                e.data = {
                            objectid: objectId,
                            facTable: facTable                            
                        };
                },
                files: initialFiles,
                success: onSuccess,
                error: onError
            }); //done with kendoUpload widget

            function onError(e) {               
                console.log(e)
            };
            function onSuccess(e) {
                console.log("onSuccess kendoUpload");
                //build the thumbnail for image or pdf here
                if (e.operation == "remove") {
                    var fileId = e.response.fileId;
                    console.log('Now removing this...', fileId);
                    //see if it was promoted to the basiic info
                    var thumbnails = $profileWindow.find('.snapshot-container .thumbnail');
                        $.each( thumbnails, function( key, value ) {                             
                            if (fileId == $(this).attr("fileid")) {
                                //remove it from DOM
                                $(this).remove();
                                return;
                            }                            
                        });                   
                    //readjust the checkboxes enable/disable
                    self.limitThumbnailsPromotionByDisablingCheckboxes($profileWindow);

                    //
                    self.reallyDelete = false;
                }

                if (e.operation == "upload") {                   
                    console.log("onSuccess upload", e.response);
                    if (e.response.result == "Error") {
                        console.log(e.response.message);
                        nisis.ui.displayMessage(e.response.message, '', 'error');
                        //remove the uid
                        var uid= e.files[0].uid;  
                        var entry = $(".k-file[data-uid='" + uid + "']");
                        entry.remove();
                        return;
                    }
                    for (var i = 0; i < e.files.length; i++) {
                        console.log("succcessfully loaded", e.files[i].name);
                        //match up with the ui - which li corresponds to this file?
                        $currentLi = self.findLiByName($filesWrap.find('li.k-file'), e.files[i].name); //better then this!
                        if ($currentLi != null) {                            
                            // NISIS-HOTFIX, KOFI HONU - Added FILEID so that files are tracked by primary key.  Avoids potential bugs later.
                            var file = e.files[i].rawFile;

                            var fileid = e.response.fileId;  //the primary key Kofi added returned in the e.response
                            var name = e.files[i].name;
                            var fileExt = e.files[i].extension;
                            var fileIsPromoted = e.files[i].isPromoted;
                            var fileName = name.substr(0, name.lastIndexOf('.'));

                            if (file) {
                                var reader = new FileReader();

                                var $label = $('<label>', {
                                    "class": "check"
                                });
                                var $box = $('<div>', {
                                    "class": "boxCheck"
                                });
                                var $span = $('<span>', {
                                    "class": "boxText"
                                }).html('Make Default');
                                var $input = $('<input>', {
                                    "name": fileName,
                                    "fileid": fileid,
                                    "type": "checkbox"
                                });

                                var $checkBox = $label.append($input, $box, $span);   
                                $currentLi.append($checkBox);       
                                $checkBox.on('change',handleFileToggle);                      

                                reader.onloadend = function () {
                                    console.log("succcessfully loaded");
                                    // create toggle button
                                    

                                    var $done = $('strong.k-upload-status.k-upload-status-total');
                                    var $perct = $('.k-upload-pct');

                                   if (fileExt.toUpperCase() == ".PDF" || fileExt.toLowerCase() == ".pdf") {
                                        fileType = 'pdf';
                                        var pdfStream = this.result,
                                            fileEncoding = 'data:application/pdf;base64';
                                        var pdfHtml = '<div class="thumbnail"><object data="'+pdfStream+'" type="application/pdf" >'+
                                                      '</object></div>';
                                        //remove the k-icon classes and stuff. Append image
                                        $currentLi.find('.k-icon:first').removeClass().text('').append(pdfHtml);
                                        setTimeout(function(){
                                            $done.fadeOut('slow');
                                            $perct.fadeOut('slow');
                                        }, 2000);

                                    }
                                    else {
                                        fileType = 'img'
                                        var imgStream = this.result,
                                            fileEncoding = 'data:image/jpeg;base64';
                                        var imgHtml = '<div class="thumbnail"><img src="'+imgStream+'" /></div>';
                                        //remove the k-icon classes and stuff. Append image
                                        $currentLi.find('.k-icon:first').removeClass().text('').append(imgHtml);
                                        setTimeout(function(){
                                            $done.fadeOut('slow');
                                            $perct.fadeOut('slow');
                                        }, 2000);
                                    }


                                      var fileNameContainer = $currentLi.find('.k-filename'),
                                        filePath = fileEncoding + "," + (fileType === "img" ? imgStream : pdfStream);

                                    // wrap with anchor tag, set filePath and download attributes
                                   $(fileNameContainer).wrap('<a class="label" href="' + filePath + '" download=' + fileName  +'></a>');


                                    self.limitThumbnailsPromotionByDisablingCheckboxes($profileWindow);

                                };
                                reader.readAsDataURL(file);
                            }
                        }
                    }                    
                }
            };
        

     


        console.log("now add on the thumbnails to the file list");
            var lis = $filesWrap.find('li.k-file');

                        
            //now add on a thumbnail
            $.each( lis, function( key, value ) {
                //lets find the appropriate content for this files li from initialFilesRetrieved
                var fileName = $(this).find('.k-filename').text();
                var uploadedFile = self.findFileByName(fileName, initialFilesRetrieved);
                if (uploadedFile==null) {
                    return false;
                }

                //is this pdf or img?
                var str = uploadedFile.EXTENSION;

                var fileExt = str.substr(str.lastIndexOf("."));


                //bleh  -- copied and pasted - TO DO: need to refactor and combine with line 2347
                                var $label = $('<label>', {
                                    "class": "check"
                                });
                                var $box = $('<div>', {
                                    "class": "boxCheck"
                                });
                                var $span = $('<span>', {
                                    "class": "boxText"
                                }).html('Make Default');
                                var $input = $('<input>', {
                                    "name": uploadedFile.fileName,
                                    "fileid": uploadedFile.FILEID,                                    
                                    "checked": (uploadedFile.IS_PROMOTED=='Y'),
                                    "type": "checkbox"
                                });

                                var $checkBox = $label.append($input, $box, $span);
                console.log("adding input",  $input);              

                $checkBox.on('change',handleFileToggle);
                var thumbnail;

                if (fileExt.toUpperCase() == ".PDF" || fileExt.toLowerCase() == ".pdf") {
                    fileType = 'pdf';
                    var pdfStream = uploadedFile['FILE_CONTENTS'],
                        fileEncoding = 'data:application/pdf;base64';

                    var pdfHtml = '<div class="thumbnail"><object data="'+ fileEncoding +','+pdfStream+'" type="application/pdf" >'+
                                  '</object></div>';
                    
                    $(this).append($checkBox);
                    $(this).find('.k-icon:first').removeClass().append(pdfHtml);
                    thumbnail = pdfHtml;
                }
                else {
                    fileType = 'img';
                    var imgStream = uploadedFile['FILE_CONTENTS'];
                    var imgHtml = '<div class="thumbnail"><img src="data:image/jpeg;base64,'+imgStream+'" /></div>';
                    
                    $(this).append($checkBox);

                    $(this).find('.k-icon:first').removeClass().append(imgHtml);
                    thumbnail = imgHtml;
                }
                if(uploadedFile.IS_PROMOTED=='Y'){
                    var el = $profileWindow.find('.snapshot-container');
                    
                    thumbnailClone = $(thumbnail).clone();
                    thumbnailClone.attr("fileid", uploadedFile.FILEID);                    
                    thumbnailClone.appendTo( $(el) );
                }

                // Clickable label for downloading IMGS and PDFs

                // Get snapshot's label and path
                var fileNameContainer = $(this).find('.k-filename'),
                    filePath = fileEncoding + "," + (fileType === "img" ? imgStream : pdfStream);

                // wrap with anchor tag, set filePath and download attributes
               $(fileNameContainer).wrap('<a class="label" href="' + filePath + '" download=' + fileName  +'></a>');
                //jon - just a me thing but when the user hits download it should show the name of the file you are downloanding instead of the file being saved as "download"
            });


            //make sure disable a checkbox if 2 already selected
            self.limitThumbnailsPromotionByDisablingCheckboxes($profileWindow);
           

    },
    hookUpFilesUpload: function(profileWindow) {
        var self = this;

        var feature = this.graphicId_;
        var layerId = this.layerId_;
        var facTable = facTables[layerId];
        var filesUploadBtn = profileWindow.find('.btn-snapshots');

        $(filesUploadBtn).click(function(evt){
                var $filesWrap = profileWindow.find(".files-wrap"),
                $formsWrap = profileWindow.find(".forms-wrap"),
                $datatablesWrap = profileWindow.find('.datatables-wrap'),
                $menu = profileWindow.find('.nav-container').children();
                if($menu.hasClass('menu-on') || $datatablesWrap){
                    $menu.removeClass('menu-on');
                }
                //toggle this
                $formsWrap.hide();
                $filesWrap.show();
                $datatablesWrap.hide();
                $filesWrap.css("position", "inherit");
        });

        var $filesWrap = profileWindow.find(".files-wrap"),
        $viewPlaceholder =  profileWindow.find(".images-pdf-display-placeholder"),
        $viewPDFPlaceholder =  profileWindow.find(".pdf-display-placeholder"),
        $files=$filesWrap.find(".files");

        $.ajax({
                url: "api/",
                type: "POST",
                data: {
                                      "action": "get_snapshots",
                                      "dataType": "json",
                                      "objectid": feature.attributes.OBJECTID,
                                      "tableName": facTable
                                },
                                success: function(data, status, req) {
                                    console.log("Success in ajax call to display image");
                                    //show either one or another. only one object
                                    if (data != null) {
                                        var resp = JSON.parse(data);
                                        if(resp.return === "Success") {
                                            var jsonInitFiles = resp.results;
                                            self.initUploadWidget(jsonInitFiles, profileWindow, $filesWrap, $files, feature);

                                            //check if image is loaded hide 100% text
                                            var $perct = $('.k-upload-pct');

                                            if($perct.is(':visible')){
                                                $perct.hide();
                                            }
                                        } else {
                                            console.error("ERROR retrieving snapshots");
                                        }
                                    }


                                },
                                error: function(req, status, err){
                                        console.error("ERROR in retrieving snapshots");
                                }
                            });


    },
    buildNISISProfileForm: function(){
        //start form
        var airportForm = "";
        var content=this.content;
        var self = this;
        var uniqueObjId = facTypes[this.layerId_] + this.objectId_ ;
        var nisisObjectSpecificFormPart = buildFormByType[this.layerId_](uniqueObjId);

        var airportHtmlString =
           '<main class="nisis_profile transition025s">'+
        '<header>'+
            '<div class="status-report flex">'+
                '<div class="f-item f-item-grow-1">'+
                    '<div class="last-updated" type="lastupdated">Profile Last Updated: <strong></strong></div>'+
                '</div>'+
                '<div class="f-item f-item-grow-1">'+
                '<div class="nisisProfileDirtyForm" style="display:none;"> <i class="fa fa-exclamation-circle" /></i> You have unsaved changes. </div>'+
                '</div>'+
                '<div class="f-item f-item-grow-0">'+
                    '<button class="red saveBtn" title="Save" ><i class="btn-save fa fa-save"></i><strong>Save</strong></button>'+
                    '<button class="red flyBtn " title="Fly-to" ><i class="fa fa-external-link"></i><strong>Fly-to</strong></button>'+
                    '<button class="red resetBtn " title="Clear" ><i class="fa fa-undo"></i><strong>Reset</strong></button>'+
                '</div>'+
            '</div>'+
        '</header>'+
        '<!-- Profiles -->'+
        '<!-- TODO: DONE/ PENDING BELOW profiles wrap > forms-wrap, datatables-wrap -->'+
        '<!-- FIXME: with-menu interaction -->'+
        '<!-- FIXME: make them all scrollable  -->'+
        '<main class=""  scrollable>'+
            nisisObjectSpecificFormPart+
            '<div class="files-wrap">' +
                '<div class="box"><h4>Upload files</h4>'+
            '</div>'+
                '<div class="upload-section k-content"> ' +
                    '<input name="files" class="files" id="files-'+ uniqueObjId +'" type="file"/>' +
                    '<div class="demo-hint">You can only upload <strong>GIF</strong>, <strong>JPG</strong>, <strong>PNG</strong>,  <strong>PDF</strong> files. Maximum allowed file size is <strong>8MB</strong></div>' +
                    '<div class="images-pdf-display-placeholder"></div>' +
                    '<div class="pdf-display-placeholder"></div>' +
                '</div>' +
            '</div>'+
        '</main>'+
        '<aside class="nav-container togglerSpacing">'+
            '<div class="qsr-menu-container med-blue menu-on">'+
                '<header class="dark-dark-blue">'+
                    '<h3 class="white2">Quick Status Report</h3>'+
                '</header>'+
                '<div class="qsr-more-scroll"><i class="fa fa-arrow-down"></i></div>' +
                '<div class="backtotop"><i class="fa fa-arrow-up"></i></div>' +
                '<menu class="qsr" scrollable></menu>'+
            '</div>'+
            '<div class="profiles-menu-container med-blue">'+
                '<header class="dark-dark-blue">'+
                    '<h3 class="white2">Profiles</h3>'+
                '</header>'+
                '<menu class="profiles" scrollable></menu>'+
           '</div>'+
            '<nav class="toggle-container med-dark-blue">'+
                '<ul>'+
                    '<li class="dark-dark-blue">'+
                        '<button><i class="btn-qsr fa fa-th-large" title="QSR"></i></button>'+
                    '</li>'+
                    '<li>'+
                        '<button><i class="btn-profiles fa fa-bars" title="Profiles"></i></button>'+
                    '</li>'+
                    // '<li>'+
                    //     '<button><i class="btn-notifications fa fa-bell"></i></button>'+
                    // '</li>'+
                    '<li>'+
                        '<button><i class="btn-history fa fa-clock-o" title="History"></i></button>'+
                    '</li>'+
                    '<li>'+
                        '<button><i class="btn-snapshots fa fa-upload" title="Snapshots"></i></button>'+
                    '</li>'+
                '</ul>'+
            '</nav>'+
        '</aside>' +
        '<!--<div id="window"></div>-->';




        airportForm = $(airportHtmlString);
        return airportForm;
    },


    onZoom: function(evt) {
        var map = nisis.ui.mapview.map;
        var zoom = map.getZoom() + 1;

        //get the dot object window object
        var self = $(evt.sender.wrapper).parents("div.kendoInfoWindow").data('self');
        var selFeat = self.graphicId_;
        //zoom in to a point or to polyline
        if(selFeat.geometry.type == 'point'){
          // map.centerAndZoom(selFeat.geometry,zoom);
          map.centerAndZoom(selFeat.geometry,10);
          console.log("point", selFeat.geometry, zoom )
        } else {
          map.setExtent(selFeat.geometry.getExtent(),true);
          console.log("non point", selFeat.geometry.getExtent(), zoom - 1 )
        }
    },

    calcPersonnelPercentage: function(self, profileWindow) {
        var $personnelPercentage = profileWindow.find("input#Personnel_Accounted_for_Percentage");
        //check if percentage calculated needed for this profile
        if (personnelFields[self.layerId_].length==0) {
            return;
        }
        var personnelTotalDbCol = personnelFields[self.layerId_][0],
        personnelAssignedDbCol = personnelFields[self.layerId_][1];

        var $personnelTotalInput = profileWindow.find("input[data-bind='value:fields." +personnelTotalDbCol +"']");
        var $assignedPersonnelTotalInput = profileWindow.find("input[data-bind='value:fields." +personnelAssignedDbCol +"']");
        //more then one "personnelTotal" element on the form
        if ($personnelTotalInput.length==0) {
            return;
        }
        $personnelTotal = $($personnelTotalInput[0]).val();
        $assignedPersonnelTotal = $($assignedPersonnelTotalInput[0]).val();

        //remove data-bind so we could write to it
        $personnelPercentage.removeAttr('data-bind');
        $personnelPercentage.removeClass('numeric');
        //calculate the percentage and write value or placeholder
        if ((typeof $personnelTotal === "undefined" || !$personnelTotal) || (typeof $assignedPersonnelTotal === "undefined" || !$assignedPersonnelTotal) || parseInt($assignedPersonnelTotal)==0){
            $personnelPercentage.val("Please provide data for Assigned Personnel and Accounted for Fields");
        }else{
            $personnelPercentage.val(parseInt(Math.round($personnelTotal)/parseInt($assignedPersonnelTotal)*100));
        }
    },

    updateLocation: function(self, fieldsChanged) {
        //console.log("updating location");
        if (fieldsChanged.hasOwnProperty("latitude") || fieldsChanged.hasOwnProperty("longitude")) {
            var layer = nisis.ui.mapview.map.getLayer(self.layerId_);
            if(layer.refresh){
                self.refreshGraphic = self.objectId_;
                layer.refresh();
            }
        }
    },

    updateSignature: function(self, fieldsChanged, $kendoInfoWindow, sigDate) {
        //console.log("updating Signature");
        var sigUser = $(".uname-bar").text().trim();
        var changes = fieldsChanged.toJSON();

        $.each(changes,function(fieldChanged, fieldChangedValue){
            //was it a text area?
            var selector = "textarea[data-bind='value:fields." + fieldChanged +"']";
            var $fields = $kendoInfoWindow.find(selector);
            $.each($fields,function(){
                var $field = $(this);
                if ($field.length>0) {
                    //console.log( "textarea that requires signature changes .", $field );
                    if ($field.parent().parent().length==1) {
                        var $sig = $field.parent().parent().find("span.sig");
                        //there were no signature
                        if ($sig.length==0) {
                           var sig = $('<span class="sig">Entered by <strong>'+sigUser+ '</strong> on ' + sigDate + '</span>');
                           sig.insertAfter( $field.parent());
                        }
                        else {
                           $sig.html('Entered by <strong>'+sigUser+ '</strong> on ' + sigDate);
                        }
                    }
                }
            });
        });


    },

    updateTitle: function(self, fieldsChanged, $kendoInfoWindow) {
        //console.log("updating location");
        if (fieldsChanged.hasOwnProperty("lg_name") || fieldsChanged.hasOwnProperty("icao_id") || fieldsChanged.hasOwnProperty("iata_id")
            || fieldsChanged.hasOwnProperty("assoc_city") || fieldsChanged.hasOwnProperty("assoc_state")) {
            var layer = nisis.ui.mapview.map.getLayer(self.layerId_);
            //refresh the layer so the updated lg_name shows up in datablock
            if(layer.refresh){
                self.refreshGraphic = self.objectId_;
                layer.refresh();
            }

            ids = titleFields[this.layerId_],
            title = "<strong>" + facTypes[this.layerId_] + ": </strong>";
            //for airport only
            if (facTypes[this.layerId_] == 'APT') {
                var $dataInputIcao = $kendoInfoWindow.find("input[data-bind='value:fields.icao_id']");
                var icaoOrFaa = $dataInputIcao.val().trim();
                if (icaoOrFaa == '') {
                    var $dataInputFaaId = $kendoInfoWindow.find("input[data-bind='value:fields.faa_id']");
                    icaoOrFaa = $dataInputFaaId.val().trim();
                }
                title += icaoOrFaa+ " ";
            }
            //setup title - irena - need to reconcile the spreadsheet with the requirement: The title of an Airport object profile window should be the ICAO, followed by the long name. If no ICAO is present (or not relevant), the FAA ID should be used.
            //For all other objects, the ID and the Long name should be used. But the spreadsheet says otherwise, and adds other fields to title
            for(var i = 0; i < ids.length; i++){
                var selector = "input[data-bind='value:fields." + ids[i] +"']";
                var $dataInput = $kendoInfoWindow.find(selector);
                var data = $dataInput.val();
                if(data != null && data != 'undefined' &&  data.length > 0 && data != ''){
                    title += data.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\? ])+/g, ' ') + " ";
                } else {
                    title += "";
                }
            }

            kendoInfoWindow.title(title);
        }
    },

    updatePersonnelPercentageCalc: function(self, fieldsChanged, $kendoInfoWindow) {
        //check if percentage calculated needed for this profile
        if (personnelFields[self.layerId_].length==0) {
            return;
        }
        var personnelTotalDbCol = personnelFields[self.layerId_][0],
        personnelAssignedDbCol = personnelFields[self.layerId_][1];

        if (fieldsChanged.hasOwnProperty(personnelTotalDbCol) || fieldsChanged.hasOwnProperty(personnelAssignedDbCol) ) {
            self.calcPersonnelPercentage(self, $kendoInfoWindow);
        }
    },

    updateGraphicLineHandler: function(evt)  {
        $(".nisis-profile-wrapper").each(function(ndx) {
            var $dotWindow = $( this ).find("div.kendoInfoWindow");
              if ($dotWindow.length>0) {
                var self = $dotWindow.data('self');

                //see if only this point refreshed
                if (self.hasOwnProperty("refreshGraphic") && self.refreshGraphic>0) {
                    var layer = nisis.ui.mapview.map.getLayer(self.layerId_);
                    var graphics = layer.graphics;
                    var foundGraphic = false;
                    for (var b=0;b<=graphics.length-1;++b) {
                        var graphic = graphics[b];
                        if (graphic.hasOwnProperty("attributes") && self.refreshGraphic == graphic.attributes.OBJECTID) {
                            foundGraphic = true;
                            //update with the new map Point
                            self.graphicId_.geometry = graphic.geometry;
                            var newMapPoint = nisis.ui.mapview.util.getGeometryMapPoint(graphic.geometry);
                            self.mapPoint_ = newMapPoint;
                            //remove the self.line from the map
                            nisis.ui.mapview.map.graphics.remove(self.line_);
                            self.drawLineBetweenPoints(self);
                            self.refreshGraphic=0;
                            break;
                        }
                    }

                    if (foundGraphic == false) {
                        //it went beyond the extent. Fly to it and then draw line
                        console.log("it went beyond the extent. Fly to it and then draw line");
                        var layer = nisis.ui.mapview.map.getLayer(self.layerId_);
                        var oid = self.graphicId_.attributes.OBJECTID;

                        self.flyBeyondExtent(self, layer, oid).then(function(geometry){
                            self.graphicId_.geometry = geometry;
                             //update with the new map Point
                            var newMapPoint = nisis.ui.mapview.util.getGeometryMapPoint(self.graphicId_.geometry);
                            self.mapPoint_ = newMapPoint;
                            //remove the self.line from the map
                            nisis.ui.mapview.map.graphics.remove(self.line_);
                            self.drawLineBetweenPoints(self);
                            self.refreshGraphic=0;

                        },function(error){
                            console.log(error);
                            nisis.ui.displayMessage(error, '', 'error');
                        });
                    }
                }
            }
        });
    },


    flyBeyondExtent: function(self, layer, oid) {
            var def = new $.Deferred();

            //query geometry
            var query = nisis.ui.mapview.util.queryFeatures({
                layer: layer,
                returnGeom: true,
                outFields: ["*"],
                where: "OBJECTID = " + oid
            });
            //success handler
            query.done(function(data){
                //console.log(data);
                var feature;
                if(data.features.length > 0){
                    feature = data.features[0];
                    var loc = [feature.geometry.x, feature.geometry.y];
                    //get the zoom level
                    var z,
                    zoom = nisis.ui.mapview.map.getZoom();
                    if ( zoom > 12 ) {
                        z = zoom ? zoom : 12;
                    }

                    var target = feature.geometry;
                    if (target.type === 'polyline'){

                    }else{
                        nisis.ui.mapview.map.centerAndZoom(target,z).
                        then(function(result){
                            //update coordinatates and redraw line
                            return def.resolve(feature.geometry);

                        },function(error){
                            console.log('Center and zoom failed for target: ' + target, " in location " + loc, error);
                            var msg = "App could not take you to the feature's location. Please report issue.";
                            nisis.ui.displayMessage(msg, '', 'error');
                            def.resolve(null);
                        });
                    }


                } else {
                    nisis.ui.displayMessage("App could not find the feature's location", '', 'error');
                }
            });
            //failed handler
            query.fail(function(err){
                nisis.ui.displayMessage(err, '', 'error');
                def.resolve(null);
            });


            return def.promise();
    },


    // NISIS-2799 KOFI HONU - convert special chars so DB doesn't choke.
    // NISIS-2896 KOFI HONU - more fixes for newline
    sanitizeInput: function(val){
        var res = val;
        res = res.replace(/(?:\&)/gi,'&#38;'); //ampersand
        res = res.replace(/(?:\')/gi,'&#39;'); //single quote
        res = res.replace(/(?:\\)/gi,'&#92;'); //backslash
        res = res.replace(/(?:\")/gi,'&#34;'); // double quotes
        res = res.replace(/(?:\r)/gi,'&#13;'); // carriage return
        res = res.replace(/(?:\n)/gi,'&#10;'); // newline
        return res;
    },
    deSanitizeOutput: function(val){
        if (typeof val === 'string' || val instanceof String) {
            var res = val;
            res = res.replace(/(?:&#39;)/gi,"'"); //single quote
            res = res.replace(/(?:&#92;)/gi,"\\"); //backslash
            res = res.replace(/(?:&#13;)/gi,"\r"); //carriage return
            res = res.replace(/(?:&#10;)/gi,"\n"); //newline
            res = res.replace(/(?:&#34;)/gi,'"'); // double quotes
            res = res.replace(/(?:&#38;)/gi,"&"); //ampersand
            return res;
        }
        else {
            return val;
        }
    },
    //this funciton is like saveChanges in profile.js
    onSave: function(evt) {


        console.log("saving changes to the profile");

        //get to the window and retrieve self so access to observable
        $domElement = $(evt.sender.wrapper);
        var $kendoInfoWindow = $domElement.parents("div.kendoInfoWindow");
        //show this when saved
        var saved = '<i class="fa fa-check-circle" /></i> Changes have been saved.';


        //see if our dirty form indicator is visible. If not, they just clicked on it without changing anything
        var $dirtyFormIndicator = $kendoInfoWindow.find("div.nisisProfileDirtyForm");
        //clone orginal state of the validation - jon
        var $dirtyFormOrg = $dirtyFormIndicator.clone().html();
        if ($dirtyFormIndicator.is(':visible') == false) {
            return;
        }

        //get self
        var self = $kendoInfoWindow.data('self');
        var $qsr = $kendoInfoWindow.find('.qsr-menu-container');



        //hook up lat lon validator
        if(self.latitudeValidator_.validate() == false || self.longitudeValidator_.validate() == false){
            // var $dirtyFormIndicator = $kendoInfoWindow.find("div.nisisProfileDirtyForm");
            // var errorsOnForm = '<i class="fa  fa-exclamation-circle" /></i> You have errors on the form.';

            // $dirtyFormIndicator.html(errorsOnForm);
            return;
        }

        var $latlongVal = $kendoInfoWindow.find('span.k-tooltip-validation.k-invalid-msg');
        $latlongVal.hide();

        //ok then
        if (JSON.stringify(self.observable_.fieldsChanged).length > 0) {


          // NISIS-2799 KOFI HONU - clean up special chars to protect db
          $.each( self.observable_.fieldsChanged.toJSON(), function( key, value ) {
            self.observable_.fieldsChanged[key] = self.sanitizeInput(self.observable_.fieldsChanged[key]);
          });

          var fieldChanges = self.observable_.fieldsChanged;

          //add on last edited user
          var lastEditedUser = {};
          var userName = $(".uname-bar").text().trim();
          fieldChanges['last_edited_user'] = userName;
          var changes = '[' + JSON.stringify(fieldChanges) +']';
          console.log("-------changes",changes);

          savePayloads= {
            'airports_pr': { action:"save_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'airports_others': { action:"save_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'airports_pu': { action:"save_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'airports_military': { action:"save_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'atm': { action:"save_profiles_atm", id: self.graphicId_.attributes.ATM_ID},
            'ans1': { action:"save_profiles_ans", id: self.graphicId_.attributes.ANS_ID},
            'ans2': { action:"save_profiles_ans", id: self.graphicId_.attributes.ANS_ID},
            'osf': { action:"save_profiles_osf", id: self.graphicId_.attributes.OSF_ID},
            'tof': { action:"save_profiles_tof", id: self.graphicId_.attributes.TOF_ID}
          };

          var savePayload = savePayloads[self.layerId_];
          //ajax call to save whats in observable
          $.ajax({
              url: "api/",
              type: "POST",
              data: {
                  "action": "save_profiles",
                  "dataType": "json",
                  "changes": changes,
                  "id": savePayload.id,
                  "save_what": savePayload.action
              },
              success: function(data, status, req) {
                    //see if update was successful
                        if (data != null) {
                            var response = JSON.parse(data);

                            if (response === undefined){
                                console.error("ERROR updating profile. Contact tech support.");
                                $(".resetBtn").click();
                            } else {
                                var resp = JSON.parse(response.result);
                                if(resp.cnt > 0) {
                                    console.log("Updated successfully ");
                                    var todayFormat = resp.last_edited_date;

                                    if (resp.qsr) {
                                        // var statusesArr = JSON.parse(resp.qsr[0]['STATUSES']).status[0];
                                        var statusesArr = resp.qsr.status[0];
                                        var $qsrStatuses = $qsr.find('.qsr-status');
                                        $qsrStatuses.each(function(ndx) {
                                            var $status = $(this);
                                            //get attribute dbcol off of it
                                            var statusDbCol = $(this).attr('dbcol');

                                            if (statusesArr.hasOwnProperty(statusDbCol)) {
                                                var newStatusTextArr = statusesArr[statusDbCol].split(":");
                                                var newStatusText = newStatusTextArr[1];
                                                var newStatusLabel = newStatusText;

                                                newStatusText = newStatusText.replace(/ /gi,'_').replace(/\//gi,'_').replace(/,/gi,'_');
                                                if (newStatusText!=''){
                                                    var newStatusTextFull = "status-" + newStatusText;
                                                    //change label and css
                                                    $status.removeClass();
                                                    $status.addClass("qsr-status");

                                                    //status-Open_with_normal_operations);
                                                    $status.addClass(newStatusTextFull);
                                                    var $qsrLabel = $status.parent().find(".qsr-status-text");
                                                    $qsrLabel.text(newStatusLabel);
                                                }
                                            }
                                        });
                                    }else {
                                    console.error("ERROR calling get_qsr_statuses");
                                }

                                    //let user know it is saved - jon
                                    $dirtyFormIndicator.addClass('successful').html(saved);

                                    $kendoInfoWindow.find('.last-updated').find('strong').html(todayFormat);
                                    //fade out and revert back to original text
                                    setTimeout(function(){
                                        $dirtyFormIndicator.fadeOut('fast', function(){
                                            $(this).removeClass('successful').html($dirtyFormOrg);
                                        });
                                    }, 1000);

                                    self.updateSignature(self, self.observable_.fieldsChanged, $kendoInfoWindow, todayFormat);

                                    self.updateLocation(self, self.observable_.fieldsChanged, self.observable_.fields['last_edited_date']);

                                    self.updateTitle(self, self.observable_.fieldsChanged, $kendoInfoWindow);

                                    self.updatePersonnelPercentageCalc(self, self.observable_.fieldsChanged, $kendoInfoWindow);


                                    //FB - reusable?
                                    dataBlockFeilds = ["lg_name", "icao_id", "iata_id", "assoc_city", "assoc_state"];
                                    dataBlockFeilds.forEach(function(el){
                                        if(self.observable_.fieldsChanged.hasOwnProperty(el)){
                                            console.log("fields changed")
                                            var layer = nisis.ui.mapview.map.getLayer(self.layerId_);
                                            if(layer.refresh){
                                                self.refreshGraphic = self.objectId_;
                                                layer.refresh();
                                                // console.log("layer refreshed", layer, self.refreshGraphic)
                                            }else{
                                                // console.log('no layer refresh')
                                            }
                                        }else{
                                            // console.log('no datablock field changed');
                                        }
                                    })


                                } else if (resp.cnt == 0)  {
                                    console.error("ERROR updating profile. Contact tech support.");
                                    nisis.ui.displayMessage("Unable to save profile. Contact tech support.", 'error');
                                    // NISIS-2881 KOFI HONU - no need to show this message in two places.  DirtyForm indicator was buggy anyway.
                                    //let user know it is saved - jon
                                    //$dirtyFormIndicator.html("Unable to save profile. Contact tech support.");
                                    //reset back to the last good knows state
                                    $(".resetBtn").click();
                                } else {
                                    console.error("ERROR updating profile. Error code:" + resp.error + ". Contact tech support.");
                                    nisis.ui.displayMessage("Unable to save profile. Contact tech support. [" + resp.error + "]", 'error');
                                    // NISIS-2881 KOFI HONU - no need to show this message in two places.  DirtyForm indicator was buggy anyway.
                                    //let user know it is saved - jon
                                    //$dirtyFormIndicator.html("Unable to save profile. Contact tech support. [" + resp.error + "]");
                                    //reset back to the last good knows state
                                    $(".resetBtn").click();
                                }
                            }

                          }
                        //clear out fields To Save
                        //go thru and remove all properties and keep events, parent, etc...
                        $.each( self.observable_.fieldsChanged.toJSON(), function( key, value ) {
                            delete self.observable_.fieldsChanged[key];
                            //also, save the json into the original data json to keep this current and in-synch with db
                            //set the new key, value in the self.original_data_
                            if (self.original_data_.formData[0].hasOwnProperty(key)) {
                                self.original_data_.formData[0][key]=value;
                            }
                        });



                      },
              error: function(req, status, err){
                          console.error("ERROR in ajax call to save NISIS Tracked Object profile");
                      }
          });
        }

    },

    onReset: function(evt) {
        console.log("reset! - clear all");

        //get to the window and retrieve self so access to observable
        $domElement = $(evt.sender.wrapper);
        var $kendoInfoWindow = $domElement.parents("div.kendoInfoWindow");

        //hide our dirty form indicator. If they just clicked on it without changing anything fieldsChanged is observable wont have properties
        var $dirtyFormIndicator = $kendoInfoWindow.find("div.nisisProfileDirtyForm");
        $dirtyFormIndicator.hide();
        var self = $kendoInfoWindow.data('self');

        //hook up lat lon validator
        if(self.latitudeValidator_.validate() == false || self.longitudeValidator_.validate() == false){
            var $latlongVal = $kendoInfoWindow.find('span.k-tooltip-validation.k-invalid-msg');
            $latlongVal.hide();
        }

        var getPayloads = {
            'airports_pr': { action:"get_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'airports_others': { action:"get_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'airports_pu': { action:"get_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'airports_military': { action:"get_profiles_airports", id: self.graphicId_.attributes.FAA_ID},
            'atm': { action:"get_profiles_atm", id: self.graphicId_.attributes.ATM_ID},
            'ans1': { action:"get_profiles_ans", id: self.graphicId_.attributes.ANS_ID},
            'ans2': { action:"get_profiles_ans", id: self.graphicId_.attributes.ANS_ID},
            'osf': { action:"get_profiles_osf", id: self.graphicId_.attributes.OSF_ID},
            'tof': { action:"get_profiles_tof", id: self.graphicId_.attributes.TOF_ID}
        };

        var getPayload = getPayloads[self.layerId_];

        $.ajax({
            url:"api/",
            type:"POST",
            data: {
                "action": "get_profiles",
                "dataType": "json",
                "get_what": getPayload.action,//get_profiles_xxx
                "id": getPayload.id, //'DCA' , 'PCT-TRACON'
            },
            success: function(data, status, req) {
                var profileData = JSON.parse(data);
                original_data_ = JSON.parse(profileData['profile'][0]['PROFILE'])
                //reset: just reload it from the save initial json that has been used to load the form
                //go thru the fieldsChanged and reset them in the observable and then trigger change
                $.each( self.observable_.fieldsChanged.toJSON(), function( key, value ) {
                    console.log('firing for fieldsChanged:', self.observable_.fieldsChanged.toJSON())
                    var originalDataValue = self.deSanitizeOutput( findAirportProfileDataFieldValue(original_data_, key));
                    console.log('originalData:', self.original_data_);
                    if (originalDataValue !== null) {
                        self.observable_.showDirtyFormIndicator = false;
                        self.observable_.set( "fields." + key, originalDataValue); // set the value
                        delete self.observable_.fieldsChanged[key];
                    }
                });
                //irena - trigger change in all multiselects - so reset to initial values ?
                self.resetMultiselects($kendoInfoWindow);
                self.observable_.showDirtyFormIndicator = true;

            },
            error: function(req, status, err) {
                console.error("ERROR in Reset Ajax");
            }
        });
    },


    addToIWL: function(facType, objectid, profileWindow){
        var iwlFormArea = "#iwlFormArea";
        var iwlForm = "#iwlForm";

        //inject the html for a form in the placeholder
        $(iwlFormArea).html(
            '<div id="iwlForm" class="">' +
                '<label>IWLs</label>' +
                '<br />' +
                '<select id="iwlSelect"/>' +
                '<br />' +
                '<button class="k-button" id="saveIWLbutton" data-bind="click:saveIwls">Save IWLs</button>' +
            '</div>'
        );

        //set up the form and multiselect
        var iwlViewModel = kendo.observable({
            saveIwls: function() {
                iwls = iwlSelect.value();
                // console.log("saving iwls!");
                // console.log("values: ", iwls);

                if(iwls.length === 0 || iwls[0] === "-1") {
                    nisis.ui.displayMessage("Nothing to save!", "", "info");
                    return;
                }

                console.log(facType);
                console.log(objectid);
                $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        action: "fac_to_iwl",
                        function: "assign_iwls",
                        iwls_to_add: iwls,
                        fac_type: facType,
                        fac_id: objectid
                    },
                    success: function(data, status, req) {
                        var resp = JSON.parse(data);
                        if(resp.result === "Success") {
                            console.log("IWLs updated successfully");
                            kendoW.close();
                            // refresh the grid
                            var $gridWrap = profileWindow.find('.datatable-iwls');
                            if ($gridWrap.length>0) {
                                var grid = $gridWrap.data("kendoGrid");
                                if (grid) {
                                    grid.dataSource.read();
                                    grid.pager.refresh();
                                }
                            }
                        } else {
                            console.error("ERROR updating IWLs");
                        }
                    },
                    error: function(req, status, err){
                        console.error("ERROR in ajax call to save a facility to IWLs from the profile");
                    }
                });
            }
        });
        kendo.bind($(iwlForm), iwlViewModel);

        //set up the multiselect
        var iwlDS = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "api/",
                    type: "POST",
                    dataType: "json",
                },
                parameterMap: function(data, type) {
                    if(type === 'read') {
                        return {
                            action: "fac_to_iwl",
                            function: "get_avail_iwls",
                            facType: facType,
                            objid: objectid,
                        };
                    }
                },
            },
            schema: {
                data: "iwls",
            },
        });
        var iwlSelect = $("#iwlSelect").kendoMultiSelect({
            dataSource: iwlDS,
            dataTextField: "NAME",
            dataValueField: "ID",
            placeholder: "Add this facility to IWLs...",
            dataBound: function() {
                var iwls = this.value();
                console.log("multiselect data bound, iwls: ", iwls);
            }
        }).data("kendoMultiSelect");

        //then set up the window, form, datasource with kendo
        var kendoW = $(iwlFormArea).kendoWindow({
            modal: true,
            title: "Add Facility to Group's IWLs",
            width: "30%",
        }).data("kendoWindow").open().center();
    },


};


    return NISISProfile;
});
