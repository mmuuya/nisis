/**
 * Icons
 */
define(function(){
	return {
		'apts': {
			'all': {
				'new': 'images/symbology/All_Airports/Airport_White.png',
				'unknown': 'images/symbology/All_Airports/Airport_Grey.png',
				'good': 'images/symbology/All_Airports/Airport_Green.png',
				'warn': 'images/symbology/All_Airports/Airport_Yellow.png',
				'down': 'images/symbology/All_Airports/Airport_Red.png',
				'unidentified': 'images/symbology/All_Airports/Airport_Unknown.png'
			},
			'airport': {
				'public': {
					'atct_csp_lh': {
						'new': 'images/symbology/Airports_PubUse/ATCT_CSP_LH/8_Apt_White.png',
						'unknown': 'images/symbology/Airports_PubUse/ATCT_CSP_LH/8_Apt_Grey.png',
						'good': 'images/symbology/Airports_PubUse/ATCT_CSP_LH/8_Apt_Green.png',
						'warn': 'images/symbology/Airports_PubUse/ATCT_CSP_LH/8_Apt_Yellow.png',
						'down': 'images/symbology/Airports_PubUse/ATCT_CSP_LH/8_Apt_Red.png'
					},
					'atct_csp_mh': {
						'new': 'images/symbology/Airports_PubUse/ATCT_CSP_MH/6_Apt_White.png',
						'unknown': 'images/symbology/Airports_PubUse/ATCT_CSP_MH/6_Apt_Grey.png',
						'good': 'images/symbology/Airports_PubUse/ATCT_CSP_MH/6_Apt_Green.png',
						'warn': 'images/symbology/Airports_PubUse/ATCT_CSP_MH/6_Apt_Yellow.png',
						'down': 'images/symbology/Airports_PubUse/ATCT_CSP_MH/6_Apt_Red.png'
					},
					'atct_csp_sh': {
						'new': 'images/symbology/Airports_PubUse/ATCT_CSP_SH/5_Apt_White.png',
						'unknown': 'images/symbology/Airports_PubUse/ATCT_CSP_SH/5_Apt_Grey.png',
						'good': 'images/symbology/Airports_PubUse/ATCT_CSP_SH/5_Apt_Green.png',
						'warn': 'images/symbology/Airports_PubUse/ATCT_CSP_SH/5_Apt_Yellow.png',
						'down': 'images/symbology/Airports_PubUse/ATCT_CSP_SH/5_Apt_Red.png'
					},
					'atct_csp_nh': {
						'new': 'images/symbology/Airports_PubUse/ATCT_CSP_NH/4_Apt_White.png',
						'unknown': 'images/symbology/Airports_PubUse/ATCT_CSP_NH/4_Apt_Grey.png',
						'good': 'images/symbology/Airports_PubUse/ATCT_CSP_NH/4_Apt_Green.png',
						'warn': 'images/symbology/Airports_PubUse/ATCT_CSP_NH/4_Apt_Yellow.png',
						'down': 'images/symbology/Airports_PubUse/ATCT_CSP_NH/4_Apt_Red.png'
					},
					'atct_csnp': {
						'new': 'images/symbology/Airports_PubUse/ATCT_CSNP/4_Apt_White.png',
						'unknown': 'images/symbology/Airports_PubUse/ATCT_CSNP/4_Apt_Grey.png',
						'good': 'images/symbology/Airports_PubUse/ATCT_CSNP/4_Apt_Green.png',
						'warn': 'images/symbology/Airports_PubUse/ATCT_CSNP/4_Apt_Yellow.png',
						'down': 'images/symbology/Airports_PubUse/ATCT_CSNP/4_Apt_Red.png'
					},
					'non_atct': {
						'new': 'images/symbology/Airports_PubUse/NON_ATCT/0_Apt_White.png',
						'unknown': 'images/symbology/Airports_PubUse/NON_ATCT/0_Apt_Grey.png',
						'good': 'images/symbology/Airports_PubUse/NON_ATCT/0_Apt_Green.png',
						'warn': 'images/symbology/Airports_PubUse/NON_ATCT/0_Apt_Yellow.png',
						'down': 'images/symbology/Airports_PubUse/NON_ATCT/0_Apt_Red.png'
					}
				},
				'nonpublic': {
					'new': 'images/symbology/Airports_NonPubUse/NP_White.png',
					'unknown': 'images/symbology/Airports_NonPubUse/NP_Grey.png',
					'good': 'images/symbology/Airports_NonPubUse/NP_Green.png',
					'warn': 'images/symbology/Airports_NonPubUse/NP_Yellow.png',
					'down': 'images/symbology/Airports_NonPubUse/NP_Red.png'
				},
				'milgov': {
					'atct': {
						'new': 'images/symbology/Airports_MilGov/With_ATCT/Mil_ATCT_White.png',
						'unknown': 'images/symbology/Airports_MilGov/With_ATCT/Mil_ATCT_Grey.png',
						'good': 'images/symbology/Airports_MilGov/With_ATCT/Mil_ATCT_Green.png',
						'warn': 'images/symbology/Airports_MilGov/With_ATCT/Mil_ATCT_Yellow.png',
						'down': 'images/symbology/Airports_MilGov/With_ATCT/Mil_ATCT_Red.png'
					},
					'natct': {
						'new': 'images/symbology/Airports_MilGov/Without_ATCT/No_ATCT_White.png',
						'unknown': 'images/symbology/Airports_MilGov/Without_ATCT/No_ATCT_Grey.png',
						'good': 'images/symbology/Airports_MilGov/Without_ATCT/No_ATCT_Green.png',
						'warn': 'images/symbology/Airports_MilGov/Without_ATCT/No_ATCT_Yellow.png',
						'down': 'images/symbology/Airports_MilGov/Without_ATCT/No_ATCT_Red.png'
					}
				}
			},
			'heliport': {
				'new': 'images/symbology/Airports_Heliport/Heliport_White.png',
				'unknown': 'images/symbology/Airports_Heliport/Heliport_Grey.png',
				'good': 'images/symbology/Airports_Heliport/Heliport_Green.png',
				'warn': 'images/symbology/Airports_Heliport/Heliport_Yellow.png',
				'down': 'images/symbology/Airports_Heliport/Heliport_Red.png'
			},
			'seaplane': {
				'new': 'images/symbology/Airports_SeaplaneBase/Seaplane_White.png',
				'unknown': 'images/symbology/Airports_SeaplaneBase/Seaplane_Grey.png',
				'good': 'images/symbology/Airports_SeaplaneBase/Seaplane_Green.png',
				'warn': 'images/symbology/Airports_SeaplaneBase/Seaplane_Yellow.png',
				'down': 'images/symbology/Airports_SeaplaneBase/Seaplane_Red.png'
			},
			'balloonport': {
				'new': 'images/symbology/Airports_Balloonports/Balloon_White.png',
				'unknown': 'images/symbology/Airports_Balloonports/Balloon_Gray.png',
				'good': 'images/symbology/Airports_Balloonports/Balloon_Green.png',
				'warn': 'images/symbology/Airports_Balloonports/Balloon_Yellow.png',
				'down': 'images/symbology/Airports_Balloonports/Balloon_Red.png'
			},
			'ultralight': {
				'new': 'images/symbology/Airports_Ultralights/Ultralight_White.png',
				'unknown': 'images/symbology/Airports_Ultralights/Ultralight_Gray.png',
				'good': 'images/symbology/Airports_Ultralights/Ultralight_Green.png',
				'warn': 'images/symbology/Airports_Ultralights/Ultralight_Yellow.png',
				'down': 'images/symbology/Airports_Ultralights/Ultralight_Red.png'
			},
			'gliderport': {
				'new': 'images/symbology/Airports_Gliderports/Glider_White.png',
				'unknown': 'images/symbology/Airports_Gliderports/Glider_Gray.png',
				'good': 'images/symbology/Airports_Gliderports/Glider_Green.png',
				'warn': 'images/symbology/Airports_Gliderports/Glider_Yellow.png',
				'down': 'images/symbology/Airports_Gliderports/Glider_Red.png'
			}
		},
		'atm': {
			'all': {
				'new': 'images/symbology/All_ATM/Square_White.png',
				'unknown': 'images/symbology/All_ATM/Square_Grey.png',
				'good': 'images/symbology/All_ATM/Square_Green.png',
				'warn': 'images/symbology/All_ATM/Square_Yellow.png',
				'down': 'images/symbology/All_ATM/Square_Red.png'
			},
			'artcc': {
				'new': 'images/symbology/ATM_Facility/ARTCC/ARTCC_White.png',
				'unknown': 'images/symbology/ATM_Facility/ARTCC/ARTCC_Grey.png',
				'good': 'images/symbology/ATM_Facility/ARTCC/ARTCC_Green.png',
				'warn': 'images/symbology/ATM_Facility/ARTCC/ARTCC_Yellow.png',
				'down': 'images/symbology/ATM_Facility/ARTCC/ARTCC_Red.png'
			},
			'tracon': {
				'new': 'images/symbology/ATM_Facility/TRACON/TRACON_White.png',
				'unknown': 'images/symbology/ATM_Facility/TRACON/TRACON_Grey.png',
				'good': 'images/symbology/ATM_Facility/TRACON/TRACON_Green.png',
				'warn': 'images/symbology/ATM_Facility/TRACON/TRACON_Yellow.png',
				'down': 'images/symbology/ATM_Facility/TRACON/TRACON_Red.png'
			},
			'atct_tracon': {
				'new': 'images/symbology/ATM_Facility/ATCT_TRACON/ATCT_TRACON_White.png',
				'unknown': 'images/symbology/ATM_Facility/ATCT_TRACON/ATCT_TRACON_Grey.png',
				'good': 'images/symbology/ATM_Facility/ATCT_TRACON/ATCT_TRACON_Green.png',
				'warn': 'images/symbology/ATM_Facility/ATCT_TRACON/ATCT_TRACON_Yellow.png',
				'down': 'images/symbology/ATM_Facility/ATCT_TRACON/ATCT_TRACON_Red.png'
			},
			'atct': {
				'new': 'images/symbology/ATM_Facility/ATCT/ATCT_White.png',
				'unknown': 'images/symbology/ATM_Facility/ATCT/ATCT_Grey.png',
				'good': 'images/symbology/ATM_Facility/ATCT/ATCT_Green.png',
				'warn': 'images/symbology/ATM_Facility/ATCT/ATCT_Yellow.png',
				'down': 'images/symbology/ATM_Facility/ATCT/ATCT_Red.png'
			},
			'fct': {
				'new': 'images/symbology/ATM_Facility/FCT/FCT_white.png',
				'unknown': 'images/symbology/ATM_Facility/FCT/FCT_gray.png',
				'good': 'images/symbology/ATM_Facility/FCT/FCT_green.png',
				'warn': 'images/symbology/ATM_Facility/FCT/FCT_yellow.png',
				'down': 'images/symbology/ATM_Facility/FCT/FCT_red.png'
			},
			// TODO: need to create their own icons for these two below ... 
			// For now, use FCT icons.
			'matct': {
				'new': 'images/symbology/ATM_Facility/FCT/FCT_white.png',
				'unknown': 'images/symbology/ATM_Facility/FCT/FCT_gray.png',
				'good': 'images/symbology/ATM_Facility/FCT/FCT_green.png',
				'warn': 'images/symbology/ATM_Facility/FCT/FCT_yellow.png',
				'down': 'images/symbology/ATM_Facility/FCT/FCT_red.png'
			},
			'international': {
				'new': 'images/symbology/ATM_Facility/FCT/FCT_white.png',
				'unknown': 'images/symbology/ATM_Facility/FCT/FCT_gray.png',
				'good': 'images/symbology/ATM_Facility/FCT/FCT_green.png',
				'warn': 'images/symbology/ATM_Facility/FCT/FCT_yellow.png',
				'down': 'images/symbology/ATM_Facility/FCT/FCT_red.png'
			},
			'fss_automated': {
				'new': 'images/symbology/ATM_Facility/FSS_AUTOMATED/FSS_AUTOMATED_White.png',
				'unknown': 'images/symbology/ATM_Facility/FSS_AUTOMATED/FSS_AUTOMATED_Grey.png',
				'good': 'images/symbology/ATM_Facility/FSS_AUTOMATED/FSS_AUTOMATED_Green.png',
				'warn': 'images/symbology/ATM_Facility/FSS_AUTOMATED/FSS_AUTOMATED_Yellow.png',
				'down': 'images/symbology/ATM_Facility/FSS_AUTOMATED/FSS_AUTOMATED_Red.png'
			},
			'fss_contract': {
				'new': 'images/symbology/ATM_Facility/FSS_CONTRACT/FSS_CONTRACT_White.png',
				'unknown': 'images/symbology/ATM_Facility/FSS_CONTRACT/FSS_CONTRACT_Grey.png',
				'good': 'images/symbology/ATM_Facility/FSS_CONTRACT/FSS_CONTRACT_Green.png',
				'warn': 'images/symbology/ATM_Facility/FSS_CONTRACT/FSS_CONTRACT_Yellow.png',
				'down': 'images/symbology/ATM_Facility/FSS_CONTRACT/FSS_CONTRACT_Red.png'
			},
			'fss_satellite': {
				'new': 'images/symbology/ATM_Facility/FSS_SATELLITE/FSS_SATELLITE_White.png',
				'unknown': 'images/symbology/ATM_Facility/FSS_SATELLITE/FSS_SATELLITE_Grey.png',
				'good': 'images/symbology/ATM_Facility/FSS_SATELLITE/FSS_SATELLITE_Green.png',
				'warn': 'images/symbology/ATM_Facility/FSS_SATELLITE/FSS_SATELLITE_Yellow.png',
				'down': 'images/symbology/ATM_Facility/FSS_SATELLITE/FSS_SATELLITE_Red.png'
			},
			'cerap': {
				'new': 'images/symbology/ATM_Facility/CERAP/CERAP_white.png',
				'unknown': 'images/symbology/ATM_Facility/CERAP/CERAP_gray.png',
				'good': 'images/symbology/ATM_Facility/CERAP/CERAP_green.png',
				'warn': 'images/symbology/ATM_Facility/CERAP/CERAP_yellow.png',
				'down': 'images/symbology/ATM_Facility/CERAP/CERAP_red.png'
			},
			'atct_non_rad_app': {
				'new': 'images/symbology/ATM_Facility/ATCT_NON_RAD_APP/ATCT_NON_RAD_APP_White.png',
				'unknown': 'images/symbology/ATM_Facility/ATCT_NON_RAD_APP/ATCT_NON_RAD_APP_Grey.png',
				'good': 'images/symbology/ATM_Facility/ATCT_NON_RAD_APP/ATCT_NON_RAD_APP_Green.png',
				'warn': 'images/symbology/ATM_Facility/ATCT_NON_RAD_APP/ATCT_NON_RAD_APP_Yellow.png',
				'down': 'images/symbology/ATM_Facility/ATCT_NON_RAD_APP/ATCT_NON_RAD_APP_Red.png'
			},
			'mil_twr': {
				'new': 'images/symbology/ATM_Facility/MIL_TWR/MIL_TWR_White.png',
				'unknown': 'images/symbology/ATM_Facility/MIL_TWR/MIL_TWR_Gray.png',
				'good': 'images/symbology/ATM_Facility/MIL_TWR/MIL_TWR_Green.png',
				'warn': 'images/symbology/ATM_Facility/MIL_TWR/MIL_TWR_Yellow.png',
				'down': 'images/symbology/ATM_Facility/MIL_TWR/MIL_TWR_Red.png'
			},
			'mil_twr_app': {
				'new': 'images/symbology/ATM_Facility/MIL_TWR_APP/MIL_TWR_APP_White.png',
				'unknown': 'images/symbology/ATM_Facility/MIL_TWR_APP/MIL_TWR_APP_Grey.png',
				'good': 'images/symbology/ATM_Facility/MIL_TWR_APP/MIL_TWR_APP_Green.png',
				'warn': 'images/symbology/ATM_Facility/MIL_TWR_APP/MIL_TWR_APP_Yellow.png',
				'down': 'images/symbology/ATM_Facility/MIL_TWR_APP/MIL_TWR_APP_Red.png'
			},
			'nfct': {
				'new': 'images/symbology/ATM_Facility/NFCT/NFCT_white.png',
				'unknown': 'images/symbology/ATM_Facility/NFCT/NFCT_gray.png',
				'good': 'images/symbology/ATM_Facility/NFCT/NFCT_green.png',
				'warn': 'images/symbology/ATM_Facility/NFCT/NFCT_yellow.png',
				'down': 'images/symbology/ATM_Facility/NFCT/NFCT_red.png'
			},
			'faa_staffed': {
                'new': 'images/symbology/ATM_Facility/FAA_Staffed/FAA_Staffed_White.png',
                'unknown': 'images/symbology/ATM_Facility/FAA_Staffed/FAA_Staffed_Grey.png',
                'good': 'images/symbology/ATM_Facility/FAA_Staffed/FAA_Staffed_Green.png',
                'warn': 'images/symbology/ATM_Facility/FAA_Staffed/FAA_Staffed_Yellow.png',
                'down': 'images/symbology/ATM_Facility/FAA_Staffed/FAA_Staffed_Red.png'
            }
		},
		'ans': {
			'all': {
				'new': 'images/symbology/All_ANS/ANS_White.png',
				'unknown': 'images/symbology/All_ANS/ANS_Grey.png',
				'good': 'images/symbology/All_ANS/ANS_Green.png',
				'warn': 'images/symbology/All_ANS/ANS_Yellow.png',
				'down': 'images/symbology/All_ANS/ANS_Red.png'
			},
			'com': {
				'new': 'images/symbology/ANS_Systems/COM/COM_White.png',
				'unknown': 'images/symbology/ANS_Systems/COM/COM_Grey.png',
				'good': 'images/symbology/ANS_Systems/COM/COM_Green.png',
				'warn': 'images/symbology/ANS_Systems/COM/COM_Yellow.png',
				'down': 'images/symbology/ANS_Systems/COM/COM_Red.png'
			},
			'nav': {
				'new': 'images/symbology/ANS_Systems/NAV/NAV_White.png',
				'unknown': 'images/symbology/ANS_Systems/NAV/NAV_Grey.png',
				'good': 'images/symbology/ANS_Systems/NAV/NAV_Green.png',
				'warn': 'images/symbology/ANS_Systems/NAV/NAV_Yellow.png',
				'down': 'images/symbology/ANS_Systems/NAV/NAV_Red.png'
			},
			'sur': {
				'new': 'images/symbology/ANS_Systems/SUR/SUR_White.png',
				'unknown': 'images/symbology/ANS_Systems/SUR/SUR_Grey.png',
				'good': 'images/symbology/ANS_Systems/SUR/SUR_Green.png',
				'warn': 'images/symbology/ANS_Systems/SUR/SUR_Yellow.png',
				'down': 'images/symbology/ANS_Systems/SUR/SUR_Red.png'
			},
			'infra': {
				'new': 'images/symbology/ANS_Systems/INFRA/infra_white.png',
				'unknown': 'images/symbology/ANS_Systems/INFRA/infra_gray.png',
				'good': 'images/symbology/ANS_Systems/INFRA/infra_green.png',
				'warn': 'images/symbology/ANS_Systems/INFRA/infra_yellow.png',
				'down': 'images/symbology/ANS_Systems/INFRA/infra_red.png'
			},
			'auto': {
				'new': 'images/symbology/ANS_Systems/AUTO/auto_white.png',
				'unknown': 'images/symbology/ANS_Systems/AUTO/auto_gray.png',
				'good': 'images/symbology/ANS_Systems/AUTO/auto_green.png',
				'warn': 'images/symbology/ANS_Systems/AUTO/auto_yellow.png',
				'down': 'images/symbology/ANS_Systems/AUTO/auto_red.png'
			},
			'power': {
				'new': 'images/symbology/ANS_Systems/POWER/power_white.png',
				'unknown': 'images/symbology/ANS_Systems/POWER/power_gray.png',
				'good': 'images/symbology/ANS_Systems/POWER/power_green.png',
				'warn': 'images/symbology/ANS_Systems/POWER/power_yellow.png',
				'down': 'images/symbology/ANS_Systems/POWER/power_red.png'
			},
			'weather': {
				'new': 'images/symbology/ANS_Systems/WEATHER/weather_white.png',
				'unknown': 'images/symbology/ANS_Systems/WEATHER/weather_gray.png',
				'good': 'images/symbology/ANS_Systems/WEATHER/weather_green.png',
				'warn': 'images/symbology/ANS_Systems/WEATHER/weather_yellow.png',
				'down': 'images/symbology/ANS_Systems/WEATHER/weather_red.png'
			},
			'other': {
				'new': 'images/symbology/ANS_Systems/Other/Other_ANS_White.png',
				'unknown': 'images/symbology/ANS_Systems/Other/Other_ANS_Grey.png',
				'good': 'images/symbology/ANS_Systems/Other/Other_ANS_Green.png',
				'warn': 'images/symbology/ANS_Systems/Other/Other_ANS_Yellow.png',
				'down': 'images/symbology/ANS_Systems/Other/Other_ANS_Red.png'
			}
		},
		'teams': {
			'all': {
				'new': 'images/symbology/All_TEAMS/TEAM_White.png',
				'unknown': 'images/symbology/All_TEAMS/TEAM_Grey.png',
				'good': 'images/symbology/All_TEAMS/TEAM_Green.png',
				'warn': 'images/symbology/All_TEAMS/TEAM_Yellow.png',
				'down': 'images/symbology/All_TEAMS/TEAM_Red.png'
			},
			'response': {
				'new': 'images/symbology/TEAMS/FAA_Deployed_White.png',
				'unknown': 'images/symbology/TEAMS/FAA_Deployed_Grey.png',
				'good': 'images/symbology/TEAMS/FAA_Deployed_Green.png',
				'warn': 'images/symbology/TEAMS/FAA_Deployed_Yellow.png',
				'down': 'images/symbology/TEAMS/FAA_Deployed_Red.png'
			}
		}
	};
});
