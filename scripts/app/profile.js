//NISIS Facility Profile ViewModel
define(["app/appUtils", "esri/geometry/webMercatorUtils"], 
    function(appUtils, webMercatorUtils){

var observable = kendo.observable({
    facType: null, //holds facility type "airports" | "ans" | "atm" | "osf" | "resp"
    origAttributes: {}, //attributes before saving
    feat: {}, //current feature
    layer: {}, //reference to the arcgis layer object to apply edits, refresh, etc
    fields: {},
    editDate: function() {
        var feat = this.get('feat'),
            date = feat.attributes.LAST_EDITED_DATE;
        if (date) {
            // return appUtils.formatDateToDTG(date, true);
            return appUtils.formatDateTimeToUtcDTG(new Date(date), "/", true);  
        } else {
            return "N/A";
        }
    },  
    getIWLs: function(){
        var iwlStr = "";
        var ds_fac = this.facType;
        var ds_obj = this.feat.attributes.OBJECTID;
        $.ajax({
            async: false, //allows us to set the iwlStr before returning
            url: "api/",
            type: "POST",
            data: {
                "action": "fac_to_iwl",
                "function": "get_iwl_names",
                "facType": ds_fac,
                "oid": ds_obj,
            },
            success: function(data, status, req) {
                var resp = JSON.parse(data);
                iwlStr = resp.iwls;
            },
            error: function(req, status, err) {
                console.error("Error in ajax call to get the IWLs");
                console.error("req: ", req);
                console.error("status: ", status);
                console.error("err: ", err);
            }
        });
        return iwlStr;
    },
    addToIWL: function(){
        var facType = this.facType;
        var objectid = this.feat.attributes.OBJECTID;

        var iwlFormArea = "#iwlFormArea";
        var iwlForm = "#iwlForm";

        //inject the html for a form in the placeholder
        $(iwlFormArea).html(
            '<div id="iwlForm" class="">' +
                '<label>IWLs</label>' +
                '<br />' +
                '<select id="iwlSelect"/>' +
                '<br />' +
                '<button class="k-button" id="saveIWLbutton" data-bind="click:saveIwls">Save IWLs</button>' +
            '</div>'
        );

        //set up the form and multiselect
        var iwlViewModel = kendo.observable({
            saveIwls: function() {
                iwls = iwlSelect.value();
                // console.log("saving iwls!");
                // console.log("values: ", iwls);

                if(iwls.length === 0 || iwls[0] === "-1") {
                    nisis.ui.displayMessage("Nothing to save!", "", "info");
                    return;
                }

                $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        action: "fac_to_iwl",
                        function: "assign_iwls",
                        iwls_to_add: iwls,
                        fac_type: facType,
                        fac_id: objectid
                    },
                    success: function(data, status, req) {
                        var resp = JSON.parse(data);
                        if(resp.result === "Success") {
                            console.log("IWLs updated successfully");
                            kendoW.close();
                        } else {
                            console.error("ERROR updating IWLs");
                        }
                    },
                    error: function(req, status, err){
                        console.error("ERROR in ajax call to save a facility to IWLs from the profile");
                    }
                });
            }
        });
        kendo.bind($(iwlForm), iwlViewModel);

        //set up the multiselect
        var iwlDS = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "api/",
                    type: "POST",
                    dataType: "json",
                },
                parameterMap: function(data, type) {
                    if(type === 'read') {
                        return {
                            action: "fac_to_iwl",
                            function: "get_avail_iwls",
                            facType: facType,
                            objid: objectid,
                        };
                    }
                },
            },
            schema: {
                data: "iwls",
            },
        });
        var iwlSelect = $("#iwlSelect").kendoMultiSelect({
            dataSource: iwlDS,
            dataTextField: "NAME",
            dataValueField: "ID",
            placeholder: "Add this facility to IWLs...",
            dataBound: function() {
                var iwls = this.value();
                console.log("multiselect data bound, iwls: ", iwls);
            }
        }).data("kendoMultiSelect");

        //then set up the window, form, datasource with kendo
        var kendoW = $(iwlFormArea).kendoWindow({
            modal: true,
            title: "Add Facility to Group's IWLs",
            width: "30%",
        }).data("kendoWindow").open().center();
    },
    statusExplanationChange:function(evt) {        
        // console.log("statusExplanationChange...");
        var statusExplanationWindow, dropdownlist, item, $statusExplanationSpan, observable, explanationColumn, $domElementToAttachTo;
                
        observable = this;

        //see if event came from span or from kendo dropdown
        var editOriginatedEvent= evt.hasOwnProperty('currentTarget') && (evt.currentTarget.tagName  === "SPAN");
        var dropdownOriginatedEvent = evt.hasOwnProperty('sender');
        
        if (editOriginatedEvent) {
            explanationColumn = $(evt.currentTarget).data().attr; //like OVR_STS_EX  
            $domElementToAttachTo = $(evt.currentTarget);
        }        
        else if (dropdownOriginatedEvent) {       
            //see which option they selected
            dropdownlist = evt.sender;
            item = evt.item;                        
            explanationColumn = dropdownlist.element.data().attr;  
            $domElementToAttachTo = $(dropdownlist.element);                                       
        }
        else {
            console.log("statusExplanationChange: error - unable to get attribute"); 
        }

        $statusExplanationSpan = $('span.status-explanation-' + explanationColumn);

        //$("body").append($("<div id='statusExplanation'><textarea  maxlength=250 /><div id='statusExplanationError' style='display:none;' > </div><br/><button class='k-button k-primary'  id='statusExplanationOKButton'>OK</button><button class='k-button k-primary'  id='statusExplanationCancelButton'>Cancel</button></div>"));
        //$("#irena").append($("<div id='statusExplanation'><textarea  maxlength=250 /><div id='statusExplanationError' style='display:none;' > </div><br/><button class='k-button k-primary'  id='statusExplanationOKButton'>OK</button><button class='k-button k-primary'  id='statusExplanationCancelButton'>Cancel</button></div>"));
        $domElementToAttachTo.append($("<div id='statusExplanation'><textarea  maxlength=250 /><div id='statusExplanationError' style='display:none;' > </div><br/><button class='k-button k-primary'  id='statusExplanationOKButton'>OK</button><button class='k-button k-primary'  id='statusExplanationCancelButton'>Cancel</button></div>"));

        //preset the textarea only if they edited through the pencil and not the dropdown
        if (editOriginatedEvent) {
            $("#statusExplanation textarea").val(observable.feat.attributes[explanationColumn]);
        }       

        /* bind the click    */
        $("#statusExplanationOKButton").click(function(){
            if ($("#statusExplanation textarea").val().length<6) {
                    $("#statusExplanationError").html("Status explanation must be at least 6 characters."); 
                    $("#statusExplanationError").show();
            }
            else if ($.trim($("#statusExplanation textarea").val()).length==0) {
                    $("#statusExplanationError").html("Status explanation can not be empty."); 
                    $("#statusExplanationError").show(); 
            }
            else {
                    //conjure up the signature and copy the whole thing into the status explanation span and database
                    //user name and DTG
                    var username = nisis.user.username;
                    var dtg = appUtils.formatDateTimeToUtcDTG(new Date(), "/", true);
                    var changeExplanationTextWithSignature = appUtils.formatTextWithSignature($("#statusExplanation textarea").val(), username, dtg);                    
                    $statusExplanationSpan.html(changeExplanationTextWithSignature);

                    //tell kendoui that span has been changed so data-bind and save works                                 
                    observable.set("feat.attributes."+explanationColumn, $("#statusExplanation textarea").val().toString() );
                    //tell kendo ui to save last edited user                    
                    observable.set("feat.attributes.LAST_EDITED_USER", username);
                    //TO DO IRENA also clear the supplemental notes that go with that span?                    

                    if (dropdownOriginatedEvent) {    
                        //select the item
                        dropdownlist.select(item);                        
                        //tell kendoui that span has been changed so data-bind and save works                         
                        var selectedOption = $(dropdownlist.element).find(":selected").val();  
                        var dataValueTemplateAttr = $(dropdownlist.element).attr("data-value-template");
                        var dropdownSelectBoundColumn = dataValueTemplateAttr.replace("-value","");                        
                        observable.feat.attributes[dropdownSelectBoundColumn]=selectedOption;
                        
                        //there are 2 dropdowns, Airport Overall Status on Airport Snapshot Panel and Airport Overall Status on Airport Overall Status Panel that bound to OVR_STATUS. 
                        //Sync them up, ie if one got certain option selected, manually select the same value in the other dropdown as well
                        if (dropdownSelectBoundColumn==="OVR_STATUS" || dropdownSelectBoundColumn==="OPS_STATUS" ||  dropdownSelectBoundColumn==="ANS_STATUS" || dropdownSelectBoundColumn==="BOPS_STS"  ) {
                            observable.syncOvrStatusDropDowns($(dropdownlist.element), dropdownlist.selectedIndex, dropdownSelectBoundColumn);
                        }                       
                    }

                    //destroy the explanation window
                    statusExplanationWindow.destroy();                                     
            };
        });

        $("#statusExplanationCancelButton").click(function(){                       
                    //destroy the window
                    statusExplanationWindow.destroy();                              
                    
                    //event propagation has been stopped, do not select an item; let the select stay where it was           
        })   

        //then set up the window, form, datasource with kendo
        statusExplanationWindow = $("#statusExplanation").kendoWindow({
            modal: true,
            title: "Please provide status change explanation",
            width: "400px",
            actions: []                 
        }).data("kendoWindow").open().center();         

        //set the focus on the textarea
        $("#statusExplanation textarea").focus();      
  
        if (dropdownOriginatedEvent) {        
            evt.preventDefault();        
        }
    },         
    syncOvrStatusDropDowns: function($selectToSkip, selectedIndex, dropdownSelectBoundColumn) {             
        //find all selects that have datavalue template as OVR_STATUS-value       
        var $selectsOVR_STATUS=$("select[data-value-template="+ dropdownSelectBoundColumn +"-value]");                                     
        $selectsOVR_STATUS.each(function( index ) {
            var $this = $( this );  
            //skip the select that was calling this                     
            if ($this.get(0) !== $selectToSkip.get(0)) {                    
                //make the same selection in the second select 
                kendo.widgetInstance($this).select(selectedIndex);                                    
            }            
        });              
    },    
    openSupplementalNotesWindow: function(evt) {
        var observable = this;
        var supplementalNotesColumn = $(evt.currentTarget).data().attr; //like OVR_STS_SN
        var $supplementalNotesSpan = $('span.supplemental-notes-' + supplementalNotesColumn);

        $("body").append($("<div id='supplementalNotes'><textarea  maxlength=250 /><br/><button class='k-button k-primary'  id='supplementalNotesOKButton'>OK</button><button class='k-button k-primary'  id='supplementalNotesCancelButton'>Cancel</button></div>"));
        
        $("#supplementalNotes textarea").val(observable.feat.attributes[supplementalNotesColumn]);  
        //$("#supplementalNotes textarea").val(this.feat.attributes["OVR_STS_SN"]);
     
        /* bind the click    */
        $("#supplementalNotesOKButton").click(function() {       
                   
                    //conjure up the signature and copy the whole thing into the status explanation span and database
                    //user name and DTG
                    var username = nisis.user.username;
                    var dtg = appUtils.formatDateTimeToUtcDTG(new Date(), "/", true);
                    var suppNotesUsersText=$("#supplementalNotes textarea").val();
                    var supplementalNotesTextWithSignature = "";
                    if (suppNotesUsersText!=="") {
                        supplementalNotesTextWithSignature = appUtils.formatTextWithSignature(suppNotesUsersText, username, dtg);                    
                    }                    
                    //copy the text and appended signature to span
                    $supplementalNotesSpan.html(supplementalNotesTextWithSignature);
                  
                    //tell kendoui that span has been changed so data-bind and save works                      
                    observable.set("feat.attributes."+supplementalNotesColumn, $("#supplementalNotes textarea").val().toString() );
                    
                    //destroy the window
                    supplementalNotesWindow.destroy();         
        });

        $("#supplementalNotesCancelButton").click(function(){                       
                    //destroy the window
                    supplementalNotesWindow.destroy();                     
        });    

        //then set up the window, form, datasource with kendo
       supplementalNotesWindow = $("#supplementalNotes").kendoWindow({
            modal: true,
            title: "Please provide supplemental notes for status change",
            width: "500px",
            actions: ['Close']        
        }).data("kendoWindow").open().center();   
        
         //set the focus on the textarea
        $("#supplementalNotes textarea").focus();   
    },  
    getProfileDirtyFormWarning: function(columnName) {
        var $dirtyFormMessage;
        // console.log("getProfileDirtyFormWarning:   "+columnName);
        switch(columnName){                                  
            case "ANS_STS_EX":
            case "BPWR_S_EX":
            case "BPWR_S_SN":
            case "ANS_NOTES":
                $dirtyFormMessage=$("#ansProfileDirtyForm");                
            break;
            case "OPS_STS_EX":
            case "OPS_STS_SN":            
            case "PWR_STS_EX":
            case "PWR_STS_SN":
            case "OPS_SL_EX":
            case "ATO_PAS_EX":
            case "ATO_PAT":
            case "ATO_PSI_EX":
            case "ATO_PSI_SN":            
            case "CRT_RL_EX":
            case "CRT_RL_SN":            
            case "CRT_SL_EX":  
            case "CRT_SL_SN":  
            case "ATM_NOTES":            
                $dirtyFormMessage=$("#atmProfileDirtyForm");                
            break;
            case "BOPS_EX": 
            case "BPWR_EX": 
            case "AOS_L_EX":
            case "AOS_L_SN": 
            case "AOPA_4S_EX": 
            case "AOPA_4S_SN":
            case "AOPS_IP_EX":
            case "AOPS_IP_SN":  
            case "CRT_RL_EX": 
            case "CRT_RL_SN": 
            case "CRT_SL_EX":  
            case "CRT_SL_SN":  
            case "OSF_NOTES":           
                $dirtyFormMessage=$("#osfProfileDirtyForm");                
            break;  
            case "RES_STS_EX":           
                $dirtyFormMessage=$("#respProfileDirtyForm");                
            break;            
            default: //airports profile
                $dirtyFormMessage=$("#airportsProfileDirtyForm");                
        }
        return $dirtyFormMessage;
    },
    saveChanges: function(){        
        var tableNm, fac_id, fType;

        switch(this.facType){
            case "airports":
                tableNm = "NISIS_AIRPORTS";
                fac_id = "FAA_ID";
                fType = "APT";
            break;
            case "ans":
                tableNm = "NISIS_ANS";
                fac_id = "ANS_ID";
                fType = "ANS";
            break;
            case "atm":
                tableNm = "NISIS_ATM";
                fac_id = "ATM_ID";
                fType = "ATM";
            break;
            case "osf":
                tableNm = "NISIS_OSF";
                fac_id = "OSF_ID";
                fType = "OSF";
            break;
            // NISIS-2742 - KOFI HONU
            // case "resp":
            //     tableNm = "NISIS_RESP";
            //     fac_id = "RES_ID";
            //     fType = "RES";
            // break;
            default:
                console.error("Could not save profile changes, facType not found: " + this.facType);
                nisis.ui.displayMessage("ERROR: Profile changes not saved");
                return;
        }

        var vm = this;
        var origAttributes = this.origAttributes;
        var newAttributes = this.feat.attributes;
        var fieldsToSave = [];

        for (var attr in newAttributes) {
            if(newAttributes[attr] !== origAttributes[attr]) {
                fieldsToSave.push(attr);
            }
        }

        if ( !fieldsToSave.length) {
            nisis.ui.displayMessage('There are no changes to the profile.', 'error');
            return;
        } 
        //verify (server-side) that the user is allowed to edit
        $.ajax({
            url: "api/",
            type: "POST",
            data: {
                "action": "check_save_profile",
                "changes": fieldsToSave,
                "table": tableNm,
            },
            success: function(data, status, req) {
                var resp = JSON.parse(data);
                if(resp.result === "true") { //server says user can update
                    /* The value of some fields come out of the db as null. 
                     * When those fields are used as dropdowns and the user changes it, 
                     * the value gets changed into an object. 
                     * Not sure if it's a problem with kendo, 
                     * but it needs to be fixed before applyEdits is called.
                    */
                    var feature = $.extend({}, vm.feat);

                    if (typeof(feature.geometry) == 'undefined'){
                        var pt, lat, lon;
                        lat = feature.attributes.LATITUDE;
                        lon = feature.attributes.LONGITUDE;
                        pt = new esri.geometry.Point( {"x": lon, "y": lat, "spatialReference": 4326 }); 
                        feature = new esri.Graphic(pt, null, feature.attributes, null);
                        //pt = new Point( {"x": lon, "y": lat, "spatialReference": 4326 }); 
                       // feature = new Graphic(pt, null, feature.attributes, null);

                        //console.log("feature1:",feature);
                    }
                    else{
                        feature.geometry = webMercatorUtils.webMercatorToGeographic(feature.geometry);
                        //console.log("feature2:",feature);
                    }
                    
                    $.each(feature.attributes, function(prop, val) {
                        var type = $.type(val),
                            value = val;
                        
                        if(type === "object") {
                            //feature.attributes[prop] = val.value;
                            value = val.value;
                        }

                        if (vm.fields[prop] && vm.fields[prop].type === 'esriFieldTypeInteger' && val) {
                            value = parseInt(value);
                        }

                        if ( $.inArray(prop, fieldsToSave) != -1) {
                            feature.attributes[prop] = value;
                        }
                    });
                    
                    vm.layer.applyEdits(null, [feature], null)
                    .then(
                        function(adds, updates, deletes) {
                            vm.layer.refresh();
                            nisis.ui.displayMessage('Profile saved', 'info');

                            //Code to insert the notifications when fields are being changed which will be displayed to the user in the screen
                            // NISIS-2660 : K.Honu - 
                            // Moved this code to trigger level to reduce network roundtrips on application and simplify inserts.
                            // This code block probably obsolete due to new profile designs, but I'm commenting it out still, just in case.


                           // $.ajax({
                           //      url: "api/",
                           //      type: "POST",
                           //      data: {
                           //          action: "get_all_noti_cols",
                           //          fac_type: fType
                           //      },
                           //      success: function(data, status, req){
                           //          var resp = JSON.parse(data);
                           //          // console.log("The ajax call to get all the notifications columns was done successfully.");
                           //          var allNotiColumns = resp.columns;
                           //          $.each(fieldsToSave, function(i, v){
                           //              if($.inArray(v, allNotiColumns) !== -1) {
                           //                  //Inserting the notifications 
                           //                  $.ajax({
                           //                      url: "api/",
                           //                      type: "POST",
                           //                      data: {
                           //                          action: "insert_notification",
                           //                          fac_type: fType,
                           //                          fac_id: origAttributes[fac_id],
                           //                          column_nm: v,
                           //                          old_value: origAttributes[v],
                           //                          new_value: newAttributes[v],
                           //                      },
                           //                      success: function(data, status, req){
                           //                          var resp = JSON.parse(data);
                           //                          console.log("The ajax call for insert_notification was done successfully.");
                           //                      },
                           //                      error: function(req, status, err){
                           //                          console.error("ERROR in ajax call for inserting the notification for the field " + v);
                           //                          console.error("req: ", req);
                           //                          console.error("status: ", status);
                           //                          console.error("err: ", err);
                           //                      }
                           //                  });                                        
                           //              }
                           //          });                                
                           //      },
                           //      error: function(req, status, err){
                           //          console.error("ERROR in ajax call to get all the notifications columns");
                           //          console.error("req: ", req);
                           //          console.error("status: ", status);
                           //          console.error("err: ", err);
                           //      }
                           //  });

                            //reset orig attributes, in case user leaves form open
                            vm.origAttributes = $.extend({}, feature.attributes);

                            //hide the unsaved changes text; we just saved the profile
                            //check which profile: ans, airports, atm
                            for (var ndx = 0; ndx < fieldsToSave.length; ndx++) {
                                // console.log("going thru -- "+fieldsToSave[ndx]);
                                vm.getProfileDirtyFormWarning(fieldsToSave[ndx]).hide();                                                        
                            }                                               
                        },
                        function(error){
                            console.log("Error saving profile, error: ", error);
                            nisis.ui.displayMessage('Error saving profile!', '', 'error');
                        }
                    );
                    
                } else {
                    nisis.ui.displayMessage('App could not verify user permissions. Process interrupted.', 'error');
                }
            },
            error: function(req, status, err) {
                console.error("Error in ajax call to save the profile - profile not saved to database");
            }
        });
    },
    getNtms: function(){
        var faaId = this.feat.attributes.FAA_ID;
        var ntmStr = "";
        $.ajax({
            async: false, //ntmStr must be set before returning
            url: "api/",
            type: "POST",
            data: {
                action: "get_profile_ntms",
                faaId: faaId
            },
            success: function(data, status, req) {
                var resp = JSON.parse(data);
                ntmStr = resp.notams;
            },
            error: function(req, status, err) {
                console.error("Error in ajax call to get the facility NOTAMs");
                console.error("req: ", req);
                console.error("status: ", status);
                console.error("err: ", err);
            }
        });
        return ntmStr;
    },
    naprsStr: function() {
        //Simply return a string for NAPRS if the facility is an ANS and the NAPRS_REPORTABLE field is "Y" - If not return a blank string
        if(this.feat.attributes.NAPRS_REPORTABLE && this.feat.attributes.NAPRS_REPORTABLE === "Y"){
            return " - NAPRS";
        }
        return "";
    },
    defaultImage: true,
    appImage: false,
    isEditable: false,
    isUploading: false,
    showImageEdits: function(e){
        this.set('isEditable', true);
    },
    hideImageEdits: function(e){
        this.set('isEditable', false);
    },
    changeImage: function(e){
        // console.log("in changeImage");
        nisis.ui.displayMessage('Not functional yet ...', '', 'warn');
    },
    uploadImage: function(e){
        nisis.ui.displayMessage('Upload clicked ...', '', 'warn'); 
        this.set('isUploading', true);
        this.uploadImageModal(e);
    },
    uploadCancel: function(e){
        // console.log("exiting upload.");
        this.set('isUploading', false);
    },
    uploadImageModal: function(e){
        var imageUploadArea = "#imageUploadArea", formHTML;
        var attribs = this.feat.attributes;
        var buttonId = e.event.currentTarget.id;
        var displayNum = buttonId.substring(0, buttonId.indexOf("_"));

         formHTML = '<div style="text-align:left" id="uploadDiv">' +
                        '<script type="text/javascript">' +
                            'function submitForm() {' +
                                'var fd = new FormData(document.getElementById("fileinfo"));' +
                                'fd.append("action", "image_upload");' +
                                'fd.append("faaId", "'+attribs.FAA_ID+'");' +
                                'fd.append("objectid", "'+attribs.OBJECTID+'");' +
                                'fd.append("facType", "'+this.facType+'");' +
                                'fd.append("displayType", "'+displayNum+'");' +
                                '$("#uploadDiv").spin(true);' + 
                                '$.ajax({' +
                                  'url: "api/",' +
                                  'type: "POST",' +
                                  'data: fd,' +
                                  'enctype: "multipart/form-data",' +
                                  'processData: false,' +
                                  'contentType: false,' +
                                '}).done(function( data ) {' +
                                    'console.log("done function start: " + data);' +
                                    '$("#uploadDiv").spin(false);' + 
                                    '$("#imageUploadArea").data("kendoWindow").close();' +
                                '}).then(function ( data ) { ' + 
                                '});' +
                                'return false;' +
                            '}' +
                        '</script>' +
                        '<label>Select a file:</label> </br>' + 
                        '<form method="post" id="fileinfo" name="fileinfo" onsubmit="return submitForm();"> ' + 
                            '<input type="file" name="file" class="k-button" required /> <br/> ' + 
                            '<span style="font-size:8">Note: Files must be smaller than 1MB and in .jpeg format.</span><br/>' +
                            '<div>' + 
                                '<input type="submit" value="Upload" /> ' + 
                            '</div>' + 
                        '</form>' + 
                    '</div>';     
        $(imageUploadArea).html(formHTML);
        var kendoW = $(imageUploadArea).kendoWindow({
            modal: true,
            title: "Upload Snapshot Image",
            resizable: false,
            width: "300",
            height: "110"
        }).data("kendoWindow").open().center();
    },
    openGbpa: function(e, action) {
        //console.log('Open GBPA:', e);
        
        var layer = nisis.ui.mapview.map.getLayer('ans1');

        if (layer) {

            if( !layer.visible ) {
                layer.setVisibility(true);
            }

            var id = e.target.dataset.id,
                query = new esri.tasks.Query();

            query.where = "ANS_ID = '" + id + "'";

            layer.queryFeatures(query, 
            function(fSet) {
                //console.log('ANS FeatureSet: ', fSet);
                if (fSet && fSet.features.length) {
                    var feat = fSet.features[0];

                    if ( action && action == 'infowin' ) {
                        nisis.ui.mapview.util.displayInfoWindow(feat, layer);
                    } else if ( action && action == 'profile' ) {
                        nisis.ui.mapview.util.buildFeatureProfile({
                            feature: feat,
                            layer: feat._layer,
                            oid: feat.attributes.OBJECTID
                        });
                    }

                } else {
                    console.log('ANS FeatureSet, no result');
                }
            }, 
            function(error) {
                console.log('ANS FeatureSet error: ', arguments);
            });
            

        } else {
            nisis.ui.displayMessage(' :) :) Ground Based Precision Approaches :) :)', 'error');
        }
    }
	
    // ----------

    // //from atm ----------
    // message: "",
    // snapshot: true,
    // basicInfo: false,
    // assocANS: false,
    // contact: false,
    // ansRel: false,
    // ansSTS: false,
    // atoPers: false,
    // emgOPS: false,
    // LAST_UPDT2: function(){
    //     var f = this.get('feat');
    //     if(f.attributes){
    //         return new Date(f.attributes.LAST_UPDT);
    //     }
    // },
    // //----------

    // //from ans ----------
    // last_updt_format: function(){
    //    //to call this, in div:
    //    //data-bind="text:last_updt_format"
    //    var f = this.get('feat');
    //    if(f.attributes){
    //        return nisis.ui.formatDateTime(f.attributes.LAST_UPDT);                
    //    }
    // },
    // ----------
});


observable.bind("set", function(e) {
    if (e.field !== "isEditable") {
        //see which form based on the e.field
        //ansProfileDirtyForm or airportsProfileDirtyForm 
        var columnName=e.field;  
        columnName=columnName.replace("feat.attributes.", "");
        var $dirtyFormMessage=observable.getProfileDirtyFormWarning(columnName);
        $dirtyFormMessage.show();        
    }
});
return observable;

});