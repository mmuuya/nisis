/*
*	Feature Templates
*/

define(["dojo/_base/Color",
    "esri/symbols/SimpleMarkerSymbol","esri/symbols/PictureMarkerSymbol","esri/symbols/MarkerSymbol",
    "esri/symbols/SimpleLineSymbol","esri/symbols/LineSymbol","esri/symbols/CartographicLineSymbol",
    "esri/symbols/SimpleFillSymbol","esri/symbols/PictureFillSymbol","esri/symbols/FillSymbol"],
	function(Color,
    	SimpleMarkerSymbol,PictureMarkerSymbol,MarkerSymbol,
    	SimpleLineSymbol,LineSymbol,CartographicLineSymbol,
    	SimpleFillSymbol,PictureFillSymbol,FillSymbol){

    var path = "M26,27.5H6c-0.829,0-1.5-0.672-1.5-1.5V6c0-0.829,0.671-1.5,1.5-1.5h20c0.828,0,1.5,0.671,1.5,1.5v20C27.5,26.828,26.828,27.5,26,27.5zM7.5,24.5h17v-17h-17V24.5z";
    var marker = new SimpleMarkerSymbol();
        marker.setPath(path);
        marker.setColor(new Color(255,266,200));
        marker.setOutline(null);

	return [{
		label: "item 1",
		description: "description 1",
		symbol: new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15, 
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, 
            new Color([0,0,0]), 2), 
            new Color([255,255,255]))
	},{
        label: "item 2",
        description: "Description 2",
        symbol: new SimpleMarkerSymbol()
            .setPath("M660,1207q125,0,235,-47.5t192,-129.5t129.5,-192t47.5,-235t-47.5,-235t-129.5,-192t-192,-129.5t-235,-47.5t-235,47.5t-192,129.5t-129.5,192t-47.5,235t47.5,235t129.5,192t192,129.5t235,47.5zM1222,54q-20,75,-59,141t-92,119t-119.5,92t-141.5,59l-17,-65,q66,-18,124.5,-52t105.5,-81.5t81,-106t52,-124.5l33,9zM660,111q102,0,191,38.5t156,105.5t105.5,156.5t38.5,191.5t-38.5,191.5t-105.5,156t-156,105.5t-191,39t-191.5,-39t-156.5,-105.5t-105.5,-156t-38.5,-191.5t38.5,-191.5t105.5,-156.5t156.5,-105.5t191.5,-38.5z,M509,1165q-75,-20,-141,-59t-119.5,-92t-92,-119t-58.5,-141l65,-18q18,66,52,124.5t81,106t106,81.5t125,52l-9,33zM98,452q20,-75,58.5,-141t92,-119.5t119.5,-92t141,-58.5l18,65q-66,18,-125,52t-106,81.5t-81,106t-52,124.5l-31,-9zM810,41q75,20,141.5,58.5t119.5,92,t92,119.5t59,141l-66,18q-18,-66,-52,-124.5t-81,-106t-105.5,-81.5t-124.5 -52l7,-29z")
            .setColor(new Color(0,0,0))
    }]
});