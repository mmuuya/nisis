var stringFilters = {}, numberFilters = {}, dateFilters = {};

define(["app/layersConfig","esri/geometry/Polygon", 
    "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleFillSymbol", 
    "esri/symbols/SimpleLineSymbol","dojo/_base/Color","dojo/_base/connect",
    "esri/tasks/query","esri/geometry/ScreenPoint", "esri/geometry/Point" ]
    ,function(layers,Polygon,SimpleMarkerSymbol, SimpleFillSymbol
        ,SimpleLineSymbol,Color,connect,Query, ScreenPoint, Point) {

    return kendo.observable({
        //drop down layers
        layers: function() {
            var $this = this,
                tblLayers = [{name:'Select a layer', value:""},{name:'All Tracked Objects', value:"all_objects"}],
                len = layers.length;

            var dataSource = dataSource = new kendo.data.DataSource({
                data: []
            });

            //NISIS-2837 - Kofi Honu - Using the new function added to get just layers user has access to.
            //solves pipelines layers privileges problem
            $.each(nisis.user.userAllowedLayers, function(i, lyr) {
               // if(lyr.add && (lyr.group === 'nisisto' || lyr.group === 'dot' || lyr.group === 'pipelines')){
                    dataSource.add({name:lyr.name, value:lyr.id});
               // }
            });
            return dataSource;
        },
        updateLayers: function(e) {
            //console.log(e);
            //var layers = this.layers();
            //console.log(layers.data());
            //$('#tbl-lyrs').data('kendoDropDownList').setDataSource(layers);
        },
        selLayer: "",
        selLayerName: "",
        lyrReady: false,
        queryDef: false,
        //view options
        viewOption: "hide",
        showViewOptions: function(e) {
            var tbl = $('#table').data('kendoGrid');
            if (!tbl) {
                return;
            }

            var el = $(e.currentTarget);
            //build tooltip only once
            if(el.data('kendoTooltip')){
                $(e.sender).show();
            } else { 
                //custom tooltip
                el.kendoTooltip({
                    //content: $('#tbl-view-options'), //==> Replaced with the main view options
                    content: $('#view-sub-menu'),
                    position: 'bottom'
                }).data('kendoTooltip').show();
            }
        },
        setViewOption: function(e){
            var $this = this,
                option = $(e.currentTarget).data('action');

            $this.set('viewOption', option);
            $this.applyViewOption();
        },
        applyViewOption: function(e){
            var $this = this,
            option = this.get('viewOption');

            if(e){
                option = e.currentTarget.value;
            } 
            //console.log('Table View Option change: ', option);
        },
        //Spatial Query options
        queryRel: [{name:'Intersects', value:'SPATIAL_REL_INTERSECTS'},
                    {name:'Proximity (Within)', value:'SPATIAL_REL_WITHIN'},
                    {name:'Disjoint (Outside)', value:'SPATIAL_REL_WITHIN'}],
        queryRelVal: "within",
        queryRelInput: false,
        showQueryRelVal: function(e){
            var qRel = this.get('queryRelVal');
            if(qRel === 'SPATIAL_REL_WITHIN'){
                this.set('queryRelInput', true);
            } else {
                this.set('queryRelInput', false);
            }
        },
        //Spatial Query inputs
        sqValue: 0,
        sqUnits: [{name:'Miles', value:'MI'},{name:'Nautical Miles', value:'NM'},{name:'Kilometers', value:'KM'},
                    {name:'Meters', value:'M'},{name:'Feet', value:'FT'}],
        sqUnit: 'MILES',
        //filter by area options
        sqExtents: [{name:"Current Map View", value:"mapview"}
            ,{name:"All Available Area",value:"fullextent"}
            ,{name:"Filter Shapes",value:"fs"}],
        fsActive: false,
        filterShapes: [],
        selFS: [],
        fsLogic: "AND",
        sqExtent: 'mapview',
        enableFS: function(e){
            var $this = this,
                ext = $this.get('sqExtent'),
                selLayer = $this.get('selLayer');
            
            if(ext === 'fs'){
                $this.set('fsActive', true);
                var selFS = $this.get('selFS');

                //update tableview if there are FS selected
                if(selFS.length > 0){
                    $this.populateTable();
                }
                
                nisis.ui.displayMessage('Retreving more filter shapes ...');
                //retreiving all filter shapes
                $this.refreshFS();
            } else {
                //hide filter shape options
                $this.set('fsActive',false);
                //update tableview if a layer has been selected
                if(selLayer !== ""){
                    $this.populateTable();
                }
            }
        },
        refreshFS: function(e){
            var $this = this;
            //query options
            var fsLayer = nisis.ui.mapview.map.getLayer('Polygon_Features');
            if(!fsLayer){
                nisis.ui.displayMessage('FS Layers has not been loaded','','error');
                return;
            }
            var opts = {
                layer: fsLayer,
                returnGeom: true,
                outFields: ['OBJECTID','NAME']
                ,where: "FS_DESIGN = '1'"
            };
            
            var objidlist = "-1";

            $.ajax({
                url: "admin/api/",
                type: "POST",
                data: {
                    action: 'acl',
                    function: 'getAllFeaturesVisibleToUser',
                    itemtype: 'polygon',
                    dataType: 'json',
                    id: nisis.user.userid
                },
                success: function(result) {
                    var resp = JSON.parse(result).items;
                    resp = JSON.parse(resp);

                    for (var i = 0; i < resp.length; i++) {
                        objidlist += "," + resp[i]["SHAPEID"];
                    }

                    opts.where += " AND OBJECTID IN (" + objidlist + ")";
                    var getFSs = nisis.ui.mapview.util.queryFeatures(opts);
                    getFSs.then(
                    function(d){
                        if (d && d.features.length > 0){
                            var feats = d.features;
                            $this.loadFS(feats);
                        } else {
                            nisis.ui.displayMessage('There are no FS(s) available.', '', 'error');
                        }
                    },
                    function(e){
                        nisis.ui.displayMessage(e.message,'','error');
                    });

                },
                error: function(result) {
                    console.error("Error: ", result);
                }
            });
        },
        loadFS: function(fShapes){
            if(fShapes && fShapes.length > 0){
                var fs = []; 
                $.each(fShapes,function(idx, fShape){
                    if($.trim(fShape.attributes.NAME) !== ""){
                        fs.push({
                            name: fShape.attributes.NAME,
                            id: fShape.attributes.OBJECTID,
                            geom: fShape.geometry
                        });
                    }


                });
                nisis.ui.sortArrayOfObjects(fs, 'name');
                this.set('filterShapes',fs);
            }
        },
        locateFS: function(e){
            var fs = this.get('selFS');
            var geoms = [], extent;
            if(fs.length > 0){
                $.each(fs,function(i,fshape){
                    if(i === 0){
                        extent = fshape.geom.getExtent();
                    } else {
                        extent.union(fshape.geom.getExtent());
                    }
                    geoms.push(fshape.geom);
                });
                //nisis.ui.mapview.map.setExtent(extent,true);
                nisis.ui.mapview.map.setExtent(nisis.ui.mapview.util.getGeometriesExtent(geoms),true);
            } else {
                nisis.ui.displayMessage('There is no Filter Shapes selected.', '', 'info');
            }
        },
        clearFS: function(e){
            this.set('selFS', []);
        },
        //iwls
        iwls: new kendo.data.DataSource({
            transport: {
                read: function(options){
                    var iwls = [{name: "Select Saved IWL", value:""}];
                    nisis.ui.populateIWL()
                    .then(function(data){
                        //console.log(data);
                        if(data.length > 0){
                            nisis.ui.sortArrayOfObjects(data, 'NAME');
                            $.each(data,function(i,iwl){
                                iwls.push({
                                    id: iwl.ID,
                                    name: iwl.NAME,
                                    value: iwl.NAME,
                                    layer: iwl.LAYER,
                                    // oids: iwl.OIDS
                                });
                            });
                            $("#tbl-saved-iwl").data("kendoDropDownList").enable(true);
                            options.success(iwls);
                        } else {
                            iwls = [{name: "(None Available)", value: ""}];
                            $("#tbl-saved-iwl").data("kendoDropDownList").enable(false);
                            options.success(iwls);
                        }
                    },function(error){
                        console.log('IWL Query Error: ',error);
                        options.error(error);
                        nisis.ui.displayMessage('The app could not load your IWLs. Try reloading the page.', '', 'error');
                    });
                }
            }
        }),
        selIWL: "",
        applyIWLFilter: function(e){
            console.log('Applying IWL Filter: ', e, this.get('selIWL'));
            var $this = this,
                selLayer = $this.get('selLayer'),
                selIWL = $this.get('selIWL'),
                ds = e.sender.dataSource,
                sel = ds.data()[e.sender.select()];

            $this.populateTable();

            console.log(ds.data(), sel);
        },
        //saved filters
        tblFilters: new kendo.data.DataSource({
            transport: {
                read: function(options){
                    var filters = [{name: "Select Saved Filter", value:""}]
                    options.success(filters);
                }
            }
        }),
        selTblFilter: "",
        applySavedFilter: function(e){
            console.log(e);
        },
        //Tables
        tblReady: false,
        tblLoading: false,
        tblMsg: "",
        tblSearch: null,
        tableColumns: null,
        tableSource: null,
        getFilterShapeGeoms: function(ids,fsLogic){           
            var $this = this;
            var def = new $.Deferred();
            //handle results when geoms > 1
            var handleGeoms = function(geoms){
                var logic;
                if(fsLogic && fsLogic !== ""){
                    logic = fsLogic;
                    getSingleGeom(geoms,logic);
                } else {
                    nisis.ui.displayConfirmation({
                        message: 'How do you want me to handle the Filter Shapes? "AND" ==> Get features within FS(s) intersection. "OR" ==> Get features within all FS(s)',
                        buttons: ['AND', 'OR']
                    }).then(function(resp){
                        logic = resp;
                        getSingleGeom(geoms,logic);
                        $this.set('fsLogic',logic);
                    },function(error){
                        nisis.ui.displayMessage('The app could not get your answer. "OR" will be used for your query', '', 'error');
                        logic = 'OR';
                        getSingleGeom(geoms,logic);
                        $this.set('fsLogic',logic);
                    });
                }
            };
            //union or intersect geoms
            var getSingleGeom = function(geoms, logic){
                if(logic === 'OR'){
                    nisis.ui.mapview.util.unionGeometries(geoms)
                    .then(function(resp){
                        // console.log("EXTEND: ", resp.getExtent());
                        def.resolve(resp);
                    },function(err){
                        console.log(err);
                        def.reject(err.message);
                    });
                } else if (logic === 'AND') {
                    var geom = geoms.pop();
                    nisis.ui.mapview.util.intersectGeometries(geoms,geom)
                    .then(function(resp){
                        //console.log(resp[0]);
                        if(resp && resp[0].getExtent()){
                            def.resolve(resp[0]);
                        } else {
                            def.reject('Your Filter Shapes do not intersect.');
                        }
                    },function(err){
                        console.log(err);
                        def.reject(err.message);
                    });
                } 
            };
            // query options
            var query = new Query();
            query.objectIds = ids;
            query.outFields = ['*'];

            var l = nisis.ui.mapview.map.getLayer('Polygon_Features'),
                lQuery = l ? l.getDefinitionExpression() : "";
            /*
             * some layers have been splited, 
             * so make sure this is not one of them
             */

            /*
             * BK: I have commented this out ealier bc
             * this query definition overwrite the default one.
             * This will work fine if the default query was applied
             * back at the end of the process.
             */
            //build query based on selected object only
            var querydef = "OBJECTID IN (" + ids.join(',') + ")";
            //reset definition query
            l.setDefinitionExpression(querydef);

            //query filter shapes
            l.queryFeatures(query,
            function(data){
                //There is a need to restore the default query
                var lyr = nisis.ui.mapview.map.getLayer('Polygon_Features');
                lyr.setDefinitionExpression(lQuery);

                var feats = data.features,
                len = feats.length;
                var geoms = [];
                if(len > 0){
                    $.each(feats,function(i,f){
                        geoms.push(f.geometry);
                    });
                    var l = geoms.length;
                    if(l === 1){
                        def.resolve(geoms[0]);
                    } else {
                        handleGeoms(geoms)
                    }
                } else {
                    def.reject('The app could not find anything that match your selection.','','warn');
                }

            },function(err){

                var lyr = nisis.ui.mapview.map.getLayer('Polygon_Features');
                lyr.setDefinitionExpression(lQuery);
                def.reject(err.message);
            });

            return def.promise();
        },
        //table elements
        selectedFeatures: [],
        //show of hide fly to buttons
        isFeatSelected: false,
        //open feature profile
        openFeatureProfile: function(e){
            console.log('Opening Table View Feature Profile: ', e);
            var $this = this, 
                lyr = $this.get('selLayer'),
                layer = nisis.ui.mapview.layers[lyr];
            
            if($this.get('selectedFeatures').length==0){
                nisis.ui.displayMessage('Please select a feature', '', 'error');
                return;
            }
            var oid = $this.get('selectedFeatures')[0].OBJECTID;


            if(!layer){
                nisis.ui.displayMessage('The selected layers is undefined', '', 'error');
                return;
            }

            if (layer.group === "nisisto" ) {
                console.log('Opening up NISIS Tracked Objects Profile: ');
                //query geometry
                var query = nisis.ui.mapview.util.queryFeatures({
                    layer: layer,
                    returnGeom: true,
                    outFields: ["*"],
                    where: "OBJECTID = " + oid
                });                
                //success handler
                query.done(function(data){
                    //console.log(data);                    
                    var feature;                    
                    if(data.features.length > 0){
                        feature = data.features[0];
                        var loc = [feature.geometry.x, feature.geometry.y];
                        var evt = [];                                    
                        evt.screenPoint =  new ScreenPoint(e.event.pageX, e.event.pageY);                                    
                        evt.graphic = feature;   
                        evt.graphic._layer = layer;
                        //convert from screen point to map point                                       
                        evt.mapPoint = nisis.ui.mapview.util.getGeometryMapPoint(feature.geometry); 
                        //bring up NISIS Tracked Object Profile
                        nisis.ui.mapview.util.bringUpNISISTOProfile(evt);
                    }                  
                });
                //failed handler
                query.fail(function(err){
                    nisis.ui.displayMessage(err, '', 'error');
                });
            }
            else if (layer.group === "dot" || layer.group === "pipelines" ) {
                console.log('Opening up DOT Profile: ');
                //query geometry
                var query = nisis.ui.mapview.util.queryFeatures({
                    layer: layer,
                    returnGeom: true,
                    outFields: ["*"],
                    where: "OBJECTID = " + oid
                });                
                //success handler
                query.done(function(data){
                    //console.log(data);
                    var feature;                    
                    if(data.features.length > 0){
                        feature = data.features[0];
                        var loc = [feature.geometry.x, feature.geometry.y];    
                         $.ajax({
                            url:"api/",
                            type:"POST",
                            data: {
                                action: "get_profiles_dot",
                                layerid: layer.id,     //       instead of layer.layer.id
                                objectid: oid,  // instead of ....
                                userid: Number(nisis.user.userid)
                            },                            
                            success: function(data, status, req) {                 
                                var resp = JSON.parse(data);
                                var profileData = JSON.parse(resp["profile"][0]["PROFILE"])["PROFILE"];
                                console.log(JSON.stringify(profileData));
                                if(profileData["METADATA"].length>0){
                                    var evt = [];                                    
                                    evt.screenPoint =  new ScreenPoint(e.event.pageX, e.event.pageY);                                    
                                    evt.graphic = feature;   
                                    //convert from screen point to map point                                       
                                    if (feature.geometry.type=="polyline") {                                     
                                        evt.mapPoint =  nisis.ui.mapview.util.getGeometryMapPoint(feature.geometry.getPoint(0,0)); 
                                    }
                                    else if (feature.geometry.type=="point") {                                        
                                        evt.mapPoint = nisis.ui.mapview.util.getGeometryMapPoint(feature.geometry);
                                    }               
                                    var infobox = new DOTObjectProfile(profileData, evt, oid); //e instead of evt
                                }else{
                                    nisis.ui.displayMessage(profileData["MSG"], 'error');
                                }
                            },
                            error: function(req, status, err) {
                                console.error("ERROR in ajax call to get DOT map objects");
                            }
                        });                       
                    } else {
                        nisis.ui.displayMessage("App could not find the feature's location", '', 'error');
                    }
                });
                //failed handler
                query.fail(function(err){
                    nisis.ui.displayMessage(err, '', 'error');
                });
            }
            else {
                var feature = {
                    oid: oid,
                    layer: layer
                };
                nisis.ui.mapview.util.buildFeatureProfile(feature);
            }
        },
        //fly to feature location
        flyToFeature: function(e){
            console.log('Flying to table view feature: ', e);
             var $this = this, 
                lyr = $this.get('selLayer'),
                layer = nisis.ui.mapview.layers[lyr],
                oid = $this.get('selectedFeatures')[0].OBJECTID;
           
            //check if layer has been loaded
            if(!layer){
                nisis.ui.displayMessage('App could not identify the selected layer.', '', 'error');
                return;
            }
            //ugly work around ==>TODO: Define a function to handle this
            if ( lyr === 'airports' ) {
                layers.push('airports_pu');
                layers.push('airports_pr');
                layers.push('airports_others');
                layers.push('airports_military');
            }
            else if ( lyr === 'ans' ) {
                layers.push('ans1');
                layers.push('ans2');
            } else if (lyr === 'all_objects') {
                layers.push('airports_pu');
                layers.push('airports_pr');
                layers.push('airports_others');
                layers.push('airports_military');
                layers.push('atm');
                layers.push('ans1');
                layers.push('ans2');
                layers.push('osf');
                //NISIS-2754 KOFI HONU
               // layers.push('teams');
                layers.push('tof');
            } else  {
                layers.push(lyr);
            }
            //query geometry
            var query = nisis.ui.mapview.util.queryFeatures({
                layer: layer,
                returnGeom: true,
                outFields: ["*"],
                where: "OBJECTID = " + oid
            });
            //success handler
            query.done(function(data){
                //console.log(data);
                var feature;
                if(data.features.length > 0){
                    feature = data.features[0];
                    var loc = [feature.geometry.x, feature.geometry.y];
                    //get the zoom level
                    var z,
                    zoom = nisis.ui.mapview.map.getZoom();
                    if ( zoom > 12 ) {
                        z = zoom ? zoom : 12;
                    }
                    
                    var target = feature.geometry;
                    if (target.type === 'polyline'){
                        var indexPoint = Math.ceil(target.paths[0].length/2) - 1;
                        var point = target.getPoint(0,indexPoint);
                        nisis.ui.mapview.map.centerAndZoom(point,15)
                        .then(function(result){
                            nisis.ui.mapview.util.flashAtLocation(point);
                        },function(error){
                            console.log('Center and zoom failed for target: ' + target, " in location " + loc, error);
                            var msg = "App could not take you to the feature's location. Please report issue.";
                            nisis.ui.displayMessage(msg, '', 'error');
                        });
                    }else{
                        nisis.ui.mapview.map.centerAndZoom(target,z).
                        then(function(result){
                            nisis.ui.mapview.util.flashAtLocation(target);
                        },function(error){
                            console.log('Center and zoom failed for target: ' + target, " in location " + loc, error);
                            var msg = "App could not take you to the feature's location. Please report issue.";
                            nisis.ui.displayMessage(msg, '', 'error');
                        });
                    }

                    //turn on layers
                    $.each(layers, function(i, l){
                        var layer = nisis.ui.mapview.map.getLayer(l);
                        if ( layer && !layer.visible ) {
                            layer.setVisibility(true);
                        }
                    });
                    //collapse all windows
                    nisis.ui.collapseWindows();
                } else {
                    nisis.ui.displayMessage("App could not find the feature's location", '', 'error');
                }
            });
            //failed handler
            query.fail(function(err){
                nisis.ui.displayMessage(err, '', 'error');
            });
        },
        //populate table
        populateTable: function(e){
            //console.log('Tableview layer changed: ', e);            
            var $this = this,
            lyr = $this.get('selLayer');

            if(lyr === ""){
                return;
            }

            //activate layer tree options
            require(['app/windows/layers'],function(layers){
                layers.attrIsOpened();
            });

            var layer = nisis.ui.mapview.layers[lyr];            
                tbl = $('#table'), 
                tblGrid = tbl.data('kendoGrid');           

            //check for iwls
            var dd = $('#tbl-lyrs').data('kendoDropDownList'),
                lyrs = dd.dataSource;
                
            //var lyrs = e.sender.options.dataSource;
            var selection = lyrs.data()[dd.select()],
            iwlName = null, 
            ids = null;

            //query options
            var options = {
                layer: layer,
                outFields: ['*'],
                returnGeom: false
            };
            //check if the selection is an IWL
            var checkIWLStatus = function(options){
                var userOptions = options;
                var download = layer.name;
                var iwls = $('#tbl-saved-iwl').data('kendoDropDownList');
                iwls.dataSource.read(); //may be a better place for this call, but we need to update the IWL dropdown
                var iwl = iwls.dataSource.data()[iwls.select()];
                var msg = "";

                if(iwl && iwl.value !== ""){
                    msg = 'Querying "' + iwl.name + '" from this layer';
                    $this.set('tblMsg', msg);
                    nisis.ui.displayMessage(msg, '', 'info');
                    //if iwl layer and tbl view layer does not match
                    if(iwl.layer !== lyr){
                        getTableData(userOptions);
                        msg = 'There are no features in "' + iwl.name + '" for this layer';
                        nisis.ui.displayMessage(msg, '', 'error');
                    } else {
                        // var oids = $.map(iwl.oids.split(','), function(id, i){
                        //     console.log(id);
                        //     return parseInt(id);
                        // });
                        //get oids for this iwl (iwl.id)
                        var oids;
                        $.ajax({
                            //get the oids, make it not async, and get it here
                            async: false,
                            url: "api/",
                            type: "POST",
                            data: {
                                action: "get_iwl_objectids",
                                iwl_id: iwl.id,
                            },
                            success: function(data, status, xhr){
                                var resp = JSON.parse(data);
                                oids = resp.results;
                            },
                            error: function(xhr, status, err){
                                console.log("ERROR getting object ids for iwl # " + iwl.id);
                                console.error(xhr);
                                console.error(status);
                                console.error(err);
                            }
                        });
                        //console.log(oids.length, oids);
                        userOptions.objectIds = oids;
                        getTableData(userOptions);
                    }

                } else {
                    getTableData(userOptions);
                }

                $this.set('selLayerName', download);
            };
            //get data from server
            var getTableData = function(options){
                // console.log(options);
                var add = false, 
                    moredata = [], 
                    n = 0;

                nisis.ui.mapview.util.queryFeatures(options)
                .then(function(data){
                    
                    if(add){ //more data coming down the pipe
                        moredata = moredata.concat(data.features);
                        $this.loadMoreData(moredata);
                        //console.log('Last: ', data.features.length);
                        //console.log('Done: ', moredata.length);
                        var msg = 'Loading ' + data.features.length + ' more features ...';
                        $this.set('tblMsg', msg);
                        var msg2 = "Done retreiving tableview data.";
                        $this.set('tblMsg', msg2);
                        $this.set('tblLoading', false);
                        nisis.ui.displayMessage(msg, '', 'info');
                    } else {                        
                        $this.buildTable(data);
                        $this.set('tblLoading', false);
                        if (data != null) {                            
                            var msg = "Done retreiving tableview data";
                            $this.set('tblMsg', msg);                            
                            nisis.ui.displayMessage(msg, '', 'info');
                        }                        
                    }
                    tbl.spin(false);
                },function(err){
                    console.log('Query tableview data error: ',err);
                    $this.set('tblMsg', err);
                    nisis.ui.displayMessage(err, 'error');
                    tbl.spin(false);
                    $this.set('tblLoading', false);
                },function(note){
                    //console.log('More: ', note);
                    var msg = "";
                    if(note && note.more && !add){
                        n++;
                        add = true;
                        //console.log('First: ', note.data.features.length);
                        var msg = 'Retreived first dataset: ' + note.data.features.length + '(' + n + ') features ...';
                        nisis.ui.displayMessage(msg, '', 'info');
                        $this.set('tblMsg', msg);
                        $this.buildTable(note.data);
                        tbl.spin(false);
                    } else if (note && note.more && add) {
                        n++;
                        //console.log('More: ',note.data.features.length);
                        moredata = moredata.concat(note.data.features);
                        var msg = 'Retreived ' + note.data.features.length + ' (' + n + ') more features ...';
                        $this.set('tblMsg', msg);
                        //nisis.ui.displayMessage(msg, '', 'info');
                    } else {
                        //console.log('Process info: ', note);
                        nisis.ui.displayMessage(note, '', 'info');
                        $this.set('tblMsg', note);
                    }
                });
            };
            //check user options
            if(lyr == ""){
                $this.set('lyrReady',false);
            } else {
                $this.set('lyrReady',true);
                 //show table processing spinner
                $this.set('tblLoading', true);
                $this.set('tblMsg', 'Gathering table options ...');
                //table spinner
                tbl.spin(true);
                //check type of spatial query
                var ext = this.get('sqExtent');
                //console.log('Spatial Query Option: ', ext);

                if(ext === 'fs'){
                    var fs = $this.get('selFS');
                   //return;
                    var len = fs.length;
                    var logic = $this.get('fsLogic');
                    var rel = $this.get('queryRelVal');
                    var msg = "";

                    if(len === 0){
                        //console.log('There is no Filter Shapes selected.');
                        msg = 'There is no Filter Shapes selected.';
                        nisis.ui.displayMessage(msg, '', 'error');
                        tbl.spin(false);
                        return;
                    } else {
                        //console.log('Filter Shapes: ', len);
                        var ids = [], names = [];
                        $.each(fs,function(i, fshape){
                            ids.push(fshape.id);
                            names.push(fshape.name);
                        });
                        // console.log(ids,names);
                        // console.log('Tableview filter shapes: ', names.join(', '));
                        nisis.ui.displayMessage('Tableview filter shapes: ' + names.join(', '), '', 'info');
                        //console.log('Tableview filter shapes query boolean: ', logic);
                        nisis.ui.displayMessage('Tableview filter shapes query boolean: ' + logic, '', 'info');

                        $this.getFilterShapeGeoms(ids,logic)
                        .then(function(geom){
                            //console.log(geom);
                            options.geom = geom;
                            //getTableData(options);
                            checkIWLStatus(options);
                        },function(error){
                            console.log('Filter Shapes query error: ', error);
                            nisis.ui.displayMessage(error, 'error');
                            tbl.spin(false);
                        });
                    }
                    //console.log(fs);
                } else if (ext === 'mapview') {
                    options.geom = nisis.ui.mapview.map.extent;
                    //getTableData(options);
                    checkIWLStatus(options);
                } else {                 
                    //options.geom = =nisis.ui.mapview.map.getLayer(lyr).fullExtent.expand(1.2);
                    options.geom = nisis.ui.mapview.map.extent.expand(1.2);
                    //check for valid extends
                    var xmin = -180,
                        xmax = 180,
                        ymin = -90,
                        ymax = 90;

                    if (options.geom.xmin > xmin) {
                        xmin = options.geom.xmin;
                    }
                    if (options.geom.xmax < xmax) {
                        xmax = options.geom.xmax;
                    }
                    if (options.geom.ymin > ymin) {
                        ymin = options.geom.ymin;
                    }
                    if (options.geom.ymax < ymax) {
                        ymax = options.geom.ymax;
                    }

                    options.geom.update(xmin, ymin, xmax, ymax, options.geom.spatialReference);
                    console.log("Extent: ", options.geom);
                    //getTableData(options);
                    checkIWLStatus(options);
                }
            }
        },
        dateFields: [],
        getColumnsToDisplay: function(){
            var $this = this,
            displayedColsArray = null;

            var aptColumns = ["FAA_ID", "LG_NAME", "ARP_CLASS", "HUB_TYPE", "TOWER_TP", "OVR_STATUS", "OVR_STS_EX", "IWL_MBER", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            var atmColumns = ["ATM_ID", "LG_NAME", "BASIC_TP", "OPS_STATUS", "OPS_STS_EX", "IWL_MBER", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            var ansColumns = ["ANS_ID", "BASIC_TP", "ANS_STATUS", "ANS_STS_EX", "RMLS_REP", "IWL_MBER", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            var osfColumns = ["OSF_ID", "LG_NAME", "BASIC_TP", "BOPS_STS", "BOPS_EX", "IWL_MBER", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //NISIS-2754- KOFI HONU
            var tofColumns = ['OBJECTID','TOF_ID','ATOW_CODE','LONG_NAME','BASIC_TYPE','CRT_RL','LAST_EDITED_USER','LAST_EDITED_DATE'];
            //var resColumns = ["RES_ID", "LG_NAME", "RES_TYPE", "RES_STS", "RES_STS_EX", "IWL_MBER", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Amtrak Rail Lines
            var ntad_amtrak_columns = ["FRAARCID", "RROWNER1","RROWNER2","RROWNER3","RR_STATUS", "RR_STS_EX","POC_NAME","POC_PHONE","COUNTY","CNTYFIPS","STATEAB","FRAREGIONS", "SWITCHES", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Amtrak Rail Stations
            var ntad_amtrk_sta_columns = ["STNCODE","STNNAME", "STN_STATUS", "STN_STS_EX", "POC_NAME", "POC_PHONE", "ADDRESS1", "ADDRESS2", "CITY", "STATE", "ZIP", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Ports
            var ntad_ports_columns = ["PORT", "PORT_NAME", "OWNER", "PRT_STATUS", "PRT_STS_EX", "POC_NAME", "POC_PHONE", "STPOSTAL", "TOTAL", "MAX_DRAFT", "NUM_BERTHS", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Interstate and US Highways
            var ntad_nhpn_ishwy_ushwy_columns = ["RECID", "LNAME", "SIGN1", "SIGN2", "SIGN3", "HWY_STATUS", "HWY_STS_EX", "STFIPS", "POC_NAME", "POC_PHONE", "FCLASS", "AADT", "NHS", "STRAHNET", "THRULANES", "OWNERSHIP", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Rail Lines
            var ntad_rail_ln_columns = ["FRAARCID", "RROWNER1","RROWNER2","RROWNER3", "RLL_STATUS", "RLL_STS_EX", "POC_NAME","POC_PHONE", "COUNTY" , "CNTYFIPS", "STATEAB", "CLASS", "FRAREGIONS", "FREIGHT_STATIONS", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Rail Line Stations
            var ntad_rail_st_columns = ["FRANODEID", "STNNAME", "OWNER", "RLN_STATUS", "RLN_STS_EX", "POC_NAME","POC_PHONE", "CITY", "COUNTY", "CNTYFIPS", "STATEAB", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Fixed Guideway Transit Lines
            var ntad_trans_ln_columns = ["RECID", "SYSTEM", "SYSTEM2", "TRL_STATUS", "TRL_STS_EX", "POC_NAME","POC_PHONE", "RTS_SRVD", "RTS_SRVD2", "UZA_CITY", "UZA_STATE", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Fixed Guideway Transit Stations
            var ntad_trans_st_columns = ["RECID", "STATION", "RTS_SRVD", "TRS_STATUS", "TRS_STS_EX", "POC_NAME","POC_PHONE", "STR_ADD", "UZA_CITY", "UZA_STATE", "STFIPS", "ZIPCODE", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //HSIP Natural Gas
            var hsip_nat_gas_comp_stations_columns = ["COMPID", "GCOMPID", "STATION", "PIPECO", "OPERNAME", "DESC_", "STA_STATUS", "STA_STS_EX", "STA_STS_SN", "POC_NAME", "POC_PHONE", "STATE", "COUNTRY", "ZIP", "LATDD", "LONGDD", "SOURCE", "SOURCEDATE", "POSREL", "NAICSCODE", "NAICSDESC", "SICCODE", "SICDESC", "STATUS", "HP", "NUMUNITS", "COST", "FUELCOST", "OTHCOST", "FUELUSE", "OPHRS", "PEAKOP", "PEAKDATE", "YEAR", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //HSIP Liquid Pipes
            var hsip_nat_gas_liquid_pipelines_columns = ["OGR_FID", "OPERATORNA","LN_STATUS","LN_STS_EX","LN_STS_SN","POC_NAME","POC_PHONE","LAST_EDITED_USER","LAST_EDITED_DATE"];
            //HSIP Oil Refinery
            var hsip_oil_refinery_columns = ["OPERNAME", "NAME", "DESC_", "STA_STATUS", "STA_STS_EX", "STA_STS_SN", "POC_NAME", "POC_PHONE", "CITY", "STATE", "COUNTRY", "ZIP", "LATDD", "LONGDD", "SOURCE", "SOURCEDATE", "POSREL", "NAICSCODE", "NAICSDESC", "SICCODE", "SICDESC", "STATUS", "FACCAP", "CAPUOM", "FACCAP2", "CAPUOM2", "INPUT", "SUPPLIER", "PRODUCT", "DEST", "INF", "PLATTSID", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //HSIP POL Terminals / Storage Facilities / Tank Farms
            var hsip_terms_store_fac_columns = ["OGR_FID", "NAME", "STA_STATUS", "STA_STS_EX", "STA_STS_SN", "POC_NAME", "POC_PHONE", "OWNER", "OPERNAME", "DESC_", "ADDRESS", "CITY", "STATE", "COUNTRY", "ZIP", "LATDD", "LONGDD", "TERMID", "SOURCE", "SOURCEDATE", "POSREL", "NAICSCODE", "NAICSDESC", "SICCODE", "SICDESC", "PHONE", "STATUS", "COMMODITY", "CAPACITY", "CAPUNIT", "TRUCK_IN", "TRUCK_OUT", "PIPEIN", "PIPEOUT", "MARINE_IN", "MARINE_OUT", "RAIL_IN", "RAIL_OUT", "ASPHALT", "CHEMICALS", "LNGS", "PROPANE", "BUTANE", "REFINED", "ETHANOL", "BIODIESEL", "CRUDE_OIL", "WEBSITE", "LAST_EDITED_USER", "LAST_EDITED_DATE"];
            //Pipelines
            var pipelines_columns = ["PLINE_ID","SYS_NM","PPL_STATUS","PPL_STS_EX","PPL_STS_SN","POC_NAME","POC_PHONE","CNTY_FIPS","CNTY_NAME","CNTY_MIXED","ST_FIPS","ST_NAME","ST_MIXED","ST_ABBR","LINE_DESIG","SYSTEM_ID","OPID","OPER_NM","SUBSYS_NM","DIAMETER","MILES","COMMODITY","CMDTY_DTL1","CMDTY_DTL2","CMDTY_DTL3","CMDTY_DESC","CMDTY_GEN","FAC_TYPE","INTERSTATE","LOW_STRESS","STATUS_CD","QUALITY_CD","REVIS_CD","SUB_DATE","REVIS_DATE","SUBMIT_ID","CONTACT_ID","SOLD_OPID","ABAND_OPID","OS_YN","OS_JURIS","OS_DESC","OS_ST_NM","OS_ST_ABBR","OS_ST_FIPS","OS_CO_NM","OS_CO_FIPS","OS_FIPS","HCA","HPA_NAME","OPA_NAME","USA_DW","USA_ECO","CNW_XINGS","CONG_DIST","CONG_REPNM","CONG_PARTY","FED_AGENCY","INSP_JURIS","PHMSA_REG","INTER_INSP","INTRA_INSP","ROUTE_ID","BEGIN_SERI","END_SERIES","BEGIN_STAT","END_STATIO","BEGIN_MEAS","END_MEASUR","DL_DATE","IU_ID","IU_NAME","IU_REGION","LENGTHOLD","LNGTH_DIFF","UNIQ_ID","INTER_PREV","INTRA_PREV","INSPJ_PREV","FRP_ZONE","SHAPE_LENG","LAST_EDITED_USER","LAST_EDITED_DATE"];

            //Checking which array of columns to display
            switch($this.selLayer) {
                case 'airports':
                case 'airports_pu':
                case 'airports_pr':
                case 'airports_others':
                case 'airports_military':
                    displayedColsArray = aptColumns;
                    break;
                case 'atm':
                    displayedColsArray = atmColumns;
                    break;
                case 'ans':
                case 'ans1':
                case 'ans2':
                    displayedColsArray = ansColumns;
                    break;
                case 'osf':
                    displayedColsArray = osfColumns;
                    break;
                //NISIS-2754 - KOFI HONU
                // case 'teams':
                //     displayedColsArray = resColumns;
                //     break;
                case 'tof':
                    displayedColsArray = tofColumns;
                    break;
                case 'ntad_amtrak':
                    displayedColsArray = ntad_amtrak_columns;
                    break;
                case 'ntad_amtrk_sta':
                    displayedColsArray = ntad_amtrk_sta_columns;
                    break;
                case 'ntad_ports':
                    displayedColsArray = ntad_ports_columns;
                    break;
                case 'ntad_nhpn_ishwy':
                case 'ntad_nhpn_ushwy':
                    displayedColsArray = ntad_nhpn_ishwy_ushwy_columns;
                    break;
                case 'ntad_rail_ln':
                    displayedColsArray = ntad_rail_ln_columns;
                    break;
                case 'ntad_rail_st':
                    displayedColsArray = ntad_rail_st_columns;
                    break;
                case 'ntad_trans_ln':
                    displayedColsArray = ntad_trans_ln_columns;
                    break;
                case 'ntad_trans_st':
                    displayedColsArray = ntad_trans_st_columns;
                    break;
                case 'hsip_nat_gas_comp_stations':
                    displayedColsArray = hsip_nat_gas_comp_stations_columns;
                    break;
                case 'hsip_nat_gas_liquid_pipelines':
                    displayedColsArray = hsip_nat_gas_liquid_pipelines_columns;
                    break;
                case 'hsip_oil_refinery':
                    displayedColsArray = hsip_oil_refinery_columns;
                    break;
                case 'hsip_terms_store_fac':
                    displayedColsArray = hsip_terms_store_fac_columns;
                    break;
                case 'pipelines_aa':
                case 'pipelines_co2':
                case 'pipelines_crd':
                case 'pipelines_epg':
                case 'pipelines_epl':
                case 'pipelines_eth':
                case 'pipelines_hg':
                case 'pipelines_lpg':
                case 'pipelines_ng':
                case 'pipelines_ngl':
                case 'pipelines_ohv':
                case 'pipelines_otg':
                case 'pipelines_pg':
                case 'pipelines_prd':
                case 'pipelines_sg':
                    displayedColsArray = pipelines_columns;
                    break;
                default:
                    break;
            }

            return displayedColsArray;
        },
        reorderColumns: function(columns, colsToDisplay){
            var newColumns = [];

            // Adding OBJECTID to the new array of columns as first element because the second will be the PK id
            $.each(columns, function(i,col){
                if (col.field === 'OBJECTID'){
                    newColumns.push(col);
                    columns.splice(i, 1);
                    return false;
                }

            });

            $.each(colsToDisplay, function(i,v){
                $.each(columns, function(i,col){
                    if (v === col.field){
                        newColumns.push(col);
                        //Remove element from array to leave in columns array only the hidden columns
                        columns.splice(i, 1);
                        return false;
                    }
                });
            });

            //concatenatening the "newColumns" array which contains the ordered visible columns with "columns" array (the hidden columns at the end)
            columns = newColumns.concat(columns);
            //Locking the field in the second position, after OBJECTID
            columns[1].locked = true;
            return columns;

        },
        buildTable: function(data){
            //console.log('Tableview data: ', data.features.length);
            var $this = this,
                tbl = $('#table'), 
                tblGrid = tbl.data('kendoGrid'),
                msg = "";
            

            if(data != null && data.fields && data.features && data.features.length && data.features.length>0 ){
                l = data.fields.length;
            } else {
                //console.log(data);
                msg = 'There is no features that matches your selection.';
                $this.set('tblMsg', msg);
                nisis.ui.displayMessage(msg, '', 'warn');
                //destroy the previous grid, if any
                if(tblGrid){
                    tblGrid.destroy();
                    tbl.empty();
                    $this.set('selectedFeatures', []);
                }
                return;
            }

            var feats = data.features,
                len = feats.length,
                l = null;            

            msg = 'Building table grid with ' + len + ' features ...';
             $this.set('tblMsg', msg);
            nisis.ui.displayMessage(msg, '', 'info');
            //get read of the old table
            if(tblGrid){
                tblGrid.destroy();
                tbl.empty();
                $this.set('selectedFeatures', []);
            }

            var displayedColsArray = $this.getColumnsToDisplay();

            var fields = {}, 
            columns = $.map(data.fields,function(field,i){

                var hiddenCol = false;
                //show everthing if the layer does not have preset field
                if ( displayedColsArray !== null) {
                    hiddenCol = ($.inArray(field.name, displayedColsArray) !== -1) ? false : true;
                }

                var column = {
                    title: field.alias,
                    field: field.name,
                    width: 150,
                    menu: true,
                    lockable: true,
                    hidden : hiddenCol
                };

                if(field.type === "esriFieldTypeOID"){
                    //console.log('Hidden field: ', field.name);
                    fields[field.name] = {
                        type: "number"
                    };
                    column.type = "number";
                    if (field.alias == "OBJECTID") {
                        column.hidden = true;
                    }
                } else if (field.type === "esriFieldTypeDate"){
                    //console.log('Date: ', field.name);
                    $this.dateFields.push(field.name);
                    fields[field.name] = {
                        type: "date"
                    };
                    column.type = "date";
                    column.format = "{0:yyyy/MM/dd HH:mm:ss}";
                    // column.filterable = {
                    //     ui: "datetimepicker"
                    // };
                } else if (field.type === "esriFieldTypeString"){
                    //console.log('String: ', field.name);
                    fields[field.name] = {
                        type: "string"
                    };
                    column.type = "string";
                    // column.filterable = {
                    //     ui: function(element){
                    //         stringFilter(element, field.name);
                    //     }
                    // };
                } else if ($.inArray(field.type, ['esriFieldTypeSmallInteger','esriFieldTypeInteger',
                    'esriFieldTypeSingle','esriFieldTypeDouble']) !== -1){
                    //console.log('Number: ', field.name);
                    //If the fields contains a picklist then its type will be string
                    if(nisis.picklists[field.name]){
                        fields[field.name] = {
                            type: "string"
                        };
                        column.type = 'string';
                    }else{
                        fields[field.name] = {
                            type: "number"
                        };
                        column.type = "number";
                        // column.filterable = {
                        //     ui: numberFilter
                        // };
                    }
                } else {
                    console.log('Unknown type: ', field.name, "Type: " + field.type);
                }
                    
                // if(i == l-1){
                    //console.log('Grid Schema: ', l, fields);
                // }

                // function numberFilter(element) {
                //     console.log("Field Element: ", element);
                //     element.kendoAutoComplete({
                //         dataSource: ["one","two","three"]
                //     });
                // }

                // function stringFilter(element, fName){
                //     console.log("Field Element: ", arguments);
                //     element.kendoAutoComplete({
                //         dataSource: ["One fdfs","Two fdsfds","Three fdfd"]
                //     });
                // }

                return column;
            });

            //Reorder the columns to be displayed in the correct order
            columns = $this.reorderColumns(columns, displayedColsArray);

            var gridData = $.map(data.features,function(feat,idx){
                for(var field in feat.attributes) {
                    if (feat.attributes[field] !== null){
                        if($.inArray(field, $this.dateFields) !== -1){
                            feat.attributes[field] = new Date(feat.attributes[field]);
                        }

                        //If 'field' is an ACL_FIELD with a picklist, so replace the value of the item (number) in the datasource
                        // for the label (string) so the label can be displayed and filtered in the table
                        if(nisis.picklists[field]){
                            //console.log(feat.attributes[field]);
                            //console.log(nisis.picklists[field][feat.attributes[field]]);
                            $.each(nisis.picklists[field], function(i,v){
                                if(i == feat.attributes[field]){
                                    feat.attributes[field] = v;
                                }
                            });
                        }
                    }
                }

                return feat.attributes;
            });

            var gridOptions = {
                columns: columns,
                dataSource: gridData,
                sortable: {
                    mode: 'single'
                },
                height: '100%',
                // filterable: true,
                groupable: true,
                columnMenu: true,
                resizable: true,
                reorderable: true,
                //selectable: true,
                selectable: "multiple row",
                pageable: {
                    refresh: false,
                    pageSizes: [10,25,50,100],
                    pageSize: 10,
                    buttonCount: 10
                },
                change: function(e){ //this is used to enable / disable profile and fly to buttons;
                    //console.log('Table View Grid Changed: ', e);
                    var feats = e.sender.select(),
                    len = feats.length,
                    selFeats = [];
                    $.each(feats, function(idx,feat){
                        //because of the column locks, the selected rows are doubled
                        if(idx > (len/2) -1){
                            return;
                        } else {
                            selFeats.push(e.sender.dataItem($(feat)));
                        }
                    });
                    //update selected features 
                    if(selFeats.length > 0){
                        $this.set('isFeatSelected', true);
                    } else {
                        $this.set('isFeatSelected', false);
                    }
                    $this.set('selectedFeatures', selFeats);
                }
            };
            //create a new table 
            var grid = tbl.kendoGrid(gridOptions);

            tbl.data('kendoGrid').dataSource.bind('change', function(e){
                //console.log('Table View Data: ', this.data().length);
                $this.tableSource = tbl.data('kendoGrid').dataSource;
            });

            //save a reference to table source and columns
            $this.tableSource = tbl.data('kendoGrid').dataSource;
            $this.tableColumns = tbl.data('kendoGrid').options.columns;
            $this.set('tblReady',true);
            $this.set('isAttrFilterVisible',false);
            $this.set('enableAttrFilter',true);

            //populate table fields
            $this.populateTableFields();

            //Removing all the divs with filters in the screen
            $this.cleanAllFilters();
        },
        loadMoreData: function(features){
            console.log('Tableview additional data: ', features.length);
            var $this = this;
            var tbl = $('#table');
            var tblGrid = tbl.data('kendoGrid');
            var len = features.length; 
            var msg = 'Adding additional ' + len + ' features to the table ...';

            $this.set('tblMsg', msg);

            if(tblGrid.dataSource &&  len > 0){
                //console.log(tblGrid.dataSource.data().length);
                var oldData = tblGrid.dataSource.data().toJSON();
                var newData = [], data = null;
                $.each(features, function(i,feat){
                    newData.push(feat.attributes);

                    //This loop is executed in buildTable method as well when the columns are being created for the grid
                    for(var field in feat.attributes) {
                        if (feat.attributes[field] !== null){
                            if($.inArray(field, $this.dateFields) !== -1){
                                feat.attributes[field] = new Date(feat.attributes[field]);
                            }

                            //If 'field' is an ACL_FIELD with a picklist, so replace the value of the item (number) in the datasource
                            // for the label (string) so the label can be displayed and filtered in the table
                            if(nisis.picklists[field]){
                                $.each(nisis.picklists[field], function(i,v){
                                    if(i == feat.attributes[field]){
                                        feat.attributes[field] = v;
                                    }
                                });
                            }
                        }
                    }

                    if (i === len -1){
                        data = oldData.concat(newData);
                        //console.log(oldData.length,newData.length,data.length);
                        $('#table').data('kendoGrid').dataSource.data(data);
                        msg = 'Done loading the rest of the data.';
                        $this.set('tblMsg', msg);
                        nisis.ui.displayMessage(msg, '', 'info');
                    }
                });
            } else {
                msg = 'Could not load ' + len + ' features into the tableview ...';
                $this.set('tblMsg', msg);
                nisis.ui.displayMessage(msg, '', 'error');
            }
        },
        updateLayer: function(e){
            console.log('Updating layer from tableview');
            var $this = this;
            //options for layer update
            var data = null,
            dataOptions = ['Page', 'Filter', 'All'],
            display = null,
            displayOptions = ['Hide', 'De-emphasize'];
            //ask for user preferences
            nisis.ui.displayConfirmation({
                message: 'What subset do you want to use to update the layer?',
                buttons: dataOptions
            }).then(function(resp){
                if($.inArray(resp,dataOptions) !== -1){
                    data = resp;
                } else {
                    return;
                }
                //ask for display options
                nisis.ui.displayConfirmation({
                    message: 'How do you want to update the layer?',
                    buttons: displayOptions
                }).then(function(resp){
                    if($.inArray(resp, displayOptions) !== -1){
                        display = resp;
                        //$this.applyUpdateToLayer(data,display);
                        $this.showTblViewFilter(display);
                    }
                },function(error){
                    console.log(error);
                });

            },function(error){
                console.log(error);
            });
        },
        showTblViewFilter: function(action /*hide or gray*/){
            console.log('showTblViewFilter: ', action);
            var action = action;
            //in case action is an event
            if (action.currentTarget) {
                action = action.currentTarget.dataset.action;
            }
            if (!action) {
                return;
            }

            var $this = this,
                dataset = null,
                lyr = $this.get('selLayer'),
                layer = nisis.ui.mapview.map.getLayer(lyr),
                dataSource = $this.get('tableSource'),
                filter = false,
                ids = [];

            var filters = dataSource.filter(),
                query = new kendo.data.Query(dataSource.data());

            dataset = query.filter(filters).data;

            if(dataset.length === 0){
                dataset = dataSource.data();
            }
            //get the unique identifier of each feature
            var len = dataset.length;
            $.each(dataset,function(i,feat){
                if(feat.OBJECTID){
                    ids.push(feat.OBJECTID);
                } else {
                    return;
                }
                if(i === len -1){
                    //apply view update
                    $this.applyUpdateToLayer(ids,action);
                }
            });            
        },
        updateViewOption: "",
        showIwlFeatures: function(action /*hide or gray*/){
            //console.log(action);
            var action = action;
            //in case action is an event
            if (action.currentTarget) {
                action = action.currentTarget.dataset.action;
            }
            if (!action) {
                return;
            }

            this.set('viewOption',action);
            $('#tbl-review-iwl').trigger('click');
        },
        applyUpdateToLayer: function(ids, display, layer){
            //console.log(arguments);
            var $this = this,
                lyr = layer ? layer : $this.get('selLayer'),
                layers = [],
                dataSource = this.get('tableSource');

            if ( lyr === 'airports' ) {
                layers.push('airports_pu');
                layers.push('airports_pr');
                layers.push('airports_others');
                layers.push('airports_military');
            }
            else if ( lyr === 'ans' ) {
                layers.push('ans1');
                layers.push('ans2');
            } else if (lyr === 'all_objects') {
                layers.push('airports_pu');
                layers.push('airports_pr');
                layers.push('airports_others');
                layers.push('airports_military');
                layers.push('atm');
                layers.push('ans1');
                layers.push('ans2');
                layers.push('osf');
                //NISIS-2754 - KOFI HONU
                //layers.push('teams');
                layers.push('tof');
            } else  {
                layers.push(lyr);
            }
            
            var layerTree = $('#overlays-toggle').data('kendoTreeView');
            //go thru each layer in layers
            $.each(layers, function(i, id){
                var l = nisis.ui.mapview.layers[id];              
                if( !l ) {
                    var msg = 'App could not identify layer on the map.';
                    nisis.ui.displayMessage(msg,'','error');
                    return;
                }
              

                var node = layerTree.dataSource.get(id);
                if(node == null){
                    console.log('toggleLayers: unable to query from Map Layer Tree View');                            
                    return;
                }                        
       
                //load layer on if its not already on                       
                if(node.checked == false){                             
                    //select the node from the tree and load the layer
                    layerTree.findByUid(node.uid);
                    var selectitem = layerTree.findByUid(node.uid);
                    selectitem.checked = true;
                    layerTree.select(selectitem);//that will load the layer
                    require(["app/windows/layers"], function(layers){
                           layers.updateTreeNode(node.id,true);
                    });  
                }                     

              
                if(display.toLowerCase() === 'hide'){
                    //var def = "OBJECTID = " + ids.join(' OR OBJECTID = ');
                    var def = "OBJECTID IN (" + ids.join(',') + ")";
                    //some layers have been splited, so make sure this is not one of them
                    if(l.query && l.query != ""){
                        def += " AND (" + l.query + ")";
                    }
                    //apply definition query
                    l.setDefinitionExpression(def);
                }
                else {
                    $this.layerUpdates[l.id] = l.on('update-end',function(evt){
                        $this.deemphasizeFeatures(evt, ids);
                    });
                    //force layer to redraw
                    l.refresh();
                }
            });
            //collapse all windows
            nisis.ui.collapseWindows();
            //enable clear def query button
            $this.set('queryDef',true);
            //clear current view options
            $this.set('updateViewOption', "");
        },
        //use this object to store layer updates (de-emphasize);
        layerUpdates: {},
        deemphasizeFeatures: function(evt,ids){
            var ids2 = $.map(ids, function(i){
                return parseInt(i);
            });          
            
            var fillSymbol = new SimpleFillSymbol();                
                fillSymbol.outline.setColor(new Color([150, 150, 150, 1]));
                fillSymbol.outline.setWidth(5);
            var pointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15, 
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, 
                new Color([100,100,100]), 1), 
                new Color([150,150,150]));         

            var zoom = evt ? evt.target.getMap().getZoom() : nisis.ui.mapview.map.getZoom();

            console.log('De-emphasize is active for ' + evt.target.id);

            $.each(evt.target.graphics,function(i,g){
                // console.log('i', i, 'g', g, 'g.attributes.objectid', g.attributes.OBJECTID, 'ids', ids);
                if($.inArray(parseInt(g.attributes.OBJECTID), ids2) === -1){
                    // console.log(g.attributes.OBJECTID, zoom, g.symbol);
                    if (g.geometry.type==='point') {
                        symbol = pointSymbol;
                        if(g.symbol){
                            symbol.setSize(g.symbol.size);
                        } else {
                            symbol.setSize(zoom);
                        }
                    }
                    else {
                        symbol = fillSymbol;
                    }
                    //set the symbol for this geometry
                    g.setSymbol(symbol);
                }
            });
        },
        /*clearUpdate: function(e, lyrId){
            console.log('Clearing IWL Features updates ...');
            var $this = this;
            var id = lyrId ? lyrId : this.get('selLayer');
            var layer = nisis.ui.mapview.map.getLayer(id);
            //console.log(e,id,layer);
            if(layer.getDefinitionExpression() !== "" || !layer.getDefinitionExpression()){
                layer.setDefinitionExpression('');
            }
            if($this.get('layerUpdates')[layer.id]){
                dojo.disconnect($this.get('layerUpdates')[layer.id]);
            }
            //reset the layer
            layer.redraw();
            //disable clear filter button
            this.set('queryDef',false);
        },*/
        resetDefaultView: function(e, lyrId){
            //console.log('Reseting mapview to default styles: ',lyrId);
            var $this = this;
            var id = lyrId ? lyrId : this.get('selLayer');
            var layers = $('#tbl-lyrs').data('kendoDropDownList').dataSource.data();
            var cleared = [];

            if(layers && layers.length > 0){
                $.each(layers,function(idx, lyr){
                    var layer = null;
                    if(lyr.value == "" || lyr.value == "all_objects"){
                        return;
                    }
                    var layer = nisis.ui.mapview.map.getLayer(lyr.value);
                    //return;
                    if( layer ){
                        if($.inArray(layer.id,cleared) == -1){
                            if(layer.getDefinitionExpression() !== "" || !layer.getDefinitionExpression()){
                                //check for default queries
                                if ( layer.query && layer.query !== "" ) {
                                    layer.setDefinitionExpression(layer.query);
                                } else {
                                    layer.setDefinitionExpression('');
                                }
                            }
                            //
                            if($this.get('layerUpdates')[layer.id]){
                                dojo.disconnect($this.get('layerUpdates')[layer.id]);
                            }

                            cleared.push(layer.id);
                            //reset the layer
                            layer.redraw();
                        }
                    } else {
                        //all good lyr was not loaded yet nothing to reset
                       // nisis.ui.displayMessage('App could not reset ' + lyr.name + ' default view.', '', 'error');
                    }
                });
            } else {
                nisis.ui.displayMessage("Tableview's has not been loaded", '', 'error');
            }
            //disable clear filter button
            this.set('queryDef',false);
        },
        // clearTableFilter: function(e){
        //     this.get('tableSource').filter({});
        //     this.set('selAttrValue', []);
        // },
        // filterString: '',
        keyUpSearchTable: function(e){
            var $this = this;
            //If the key pressed was the Enter key
            if(e.keyCode === 13){
                $this.searchTable(e);
            }
        },
        searchTable: function(e){
            var $this = this,
            datasource = $('#table').data('kendoGrid').dataSource,
            value = $('#tbl-search').val(),
            // columns = $this.get('tableColumns'), //Gets the static columns defined in the object
            columns = $('#table').data('kendoGrid').columns, //Gets the actual dynamic columns displayed in the grid
            stringCols = [];

            var mainFilter = {logic: 'or', filters: []};

            if(value != null && value.length > 0){
                //Getting the string visible columns
                $.each(columns,function(i,v){
                    if (!v.hidden && v.type === 'string'){
                        stringCols.push(v.field);
                    }
                });

                //Building the filter
                $.each(stringCols,function(i,v){
                    mainFilter.filters.push({
                        field: v,
                        operator: "contains",
                        value: value
                    });
                });  
            }
            datasource.filter(mainFilter);
        },
        downloadTable: function(e){
            // console.log('Download table as csv file: ', e);
            var $this = this,
            csvData = [],
            datasource = $('#table').data('kendoGrid').dataSource,
            columns = $.map($this.get('tableColumns'),function(field,i){
                return field.title;
            });
            csvData.push(columns);
            //get table data

            
            var query = new kendo.data.Query(datasource.data());
            var dataview = datasource.filter()?query.filter(datasource.filter()).data:datasource.data();
            var data = $.each(dataview,function(i,row){
                var values = [];
                $.each($this.get('tableColumns'),function(i,f){
                    if (typeof(row[f.field]) !== 'string'){
                        if (row[f.field] instanceof Date){
                            values.push(kendo.toString(row[f.field], "yyyy/MM/dd HH:mm:ss"));
                        }else{
                            values.push(row[f.field]);
                        }                        
                    }else{
                        //Replacing all the double quotes inside the value to keep the value with the double quotes after the csv file is created. This is the way to escape the double quotes for a csv file
                        row[f.field] = row[f.field].replace(/"/g, '""');

                        if(row[f.field].indexOf(",") >= 0){
                            //Adding double quotes at the beggining and the end of the value to keep the commas as part of value and not as a separator
                            row[f.field] = '"'+row[f.field]+'"';

                        }
                        values.push(row[f.field]);                        
                    }
                });
                csvData.push(values);
            });

            var csv = csvData.join("%0A");
            
            //create a csvfile link
            var a = document.createElement('a');
            a.href ='data:text/csv;charset=utf-8,' + csv;
            a.target = '_blank';
            //get layer name
            var id = $this.get('selLayer'),
                layer = nisis.ui.mapview.map.getLayer(id),
                name = layer ? layer.name : 'tableview';

            //a.download = this.selectedLayerName() + '.csv';
            a.download = name + '.csv';
            document.body.appendChild(a)
            a.click();
        },
        //IWLs
        iwl: false,
        iwlName: "",
        iwlDescription: "",
        iwlMsg: "",
        iwlMsgCol: "black",
        manageIWLs: function(e){
            $('#iwl-div').data('kendoWindow').center().open();
        },
        createIWL: function(evt){
            this.set('iwl',true);
            this.set('iwlName',"");
            this.set('iwlMsg','');
            this.set('iwlMsgCol','black');
        },
        cancelIWL: function(e){
            console.log('Canceling IWL creation');
            this.clearIWLMsg();
            this.resetIWL();
        },
        saveIWL: function(e){
            var $this = this;
            var name = $this.get('iwlName'),
                desc = $this.get('iwlDescription');

            if($.trim(name).length > 0 || $.trim(desc).length > 0){
                console.log('Saving IWL ...');
                
                var lyr = {
                    name: name + " (IWL)",
                    value: $this.get('selLayer'),
                    iwl: true
                };

                var dataOptions = ['Page', 'Filter', 'All'];

                nisis.ui.displayConfirmation({
                    message: 'What subset do you want to use for your IWL?',
                    buttons: dataOptions
                }).then(function(resp){
                    if($.inArray(resp,dataOptions) !== -1){
                        var ids = $this.getIWLFeatures(resp);
                        insertIWLs(ids);
                    }
                },function(error){
                    console.log(error);
                    nisis.ui.displayMessage(error,'','error');
                });
            } else {
                this.set('iwlMsg','Invalid IWL name and/or description.');
                this.set('iwlMsgCol','white');
            }

            var insertIWLs = function(ids){
                var layer = $this.get('selLayer');
                //save iwls
                nisis.ui.createIWL(layer,name,desc,ids)
                .then(function(d){
                    console.log(d);
                    $('#tbl-lyrs').data('kendoDropDownList').dataSource.add(lyr);
                    
                    $this.set('iwlMsg','A new IWL is added.');
                    $this.set('iwlMsgCol','black');
                    $this.resetIWL();

                    setTimeout(function(){
                        $this.clearIWLMsg();
                    },1000);

                },function(e){
                    console.log(e);
                    nisis.ui.displayMessage(e,'','error');
                });
            };
        },
        getIWLFeatures: function(subset){
            // var dataSource = this.get('tableSource');
            var dataSource = $('#table').data('kendoGrid').dataSource;
            var ids = [], dataset = null;
            //get the specified data
            /*if(subset === 'Page'){
                dataset = dataSource.view();
            } else if (subset === 'Filter'){
                var filters = dataSource.filter();
                var query = new kendo.data.Query(dataSource.data());
                dataset = query.filter(filters).data;
            } else if (subset === 'All') {
                dataset = dataSource.data();
            }*/

            var filters = dataSource.filter();
            var query = new kendo.data.Query(dataSource.data());
            dataset = query.filter(filters).data;

            if(dataset.length === 0){
                dataset = dataSource.data();
            }

            //get the unique identifier of each feature
            if(dataset){
                $.each(dataset,function(i,feat){
                    if(feat.OBJECTID){
                        ids.push(feat.OBJECTID);
                    } else {
                        return;
                    }
                });
                return ids;
            } else {
                return [];
            };
        },
        resetIWL: function(){
            this.set('iwlName', "");
            this.set('iwlDescription', "");
            this.set('iwl', false);
        },
        clearIWLMsg: function(){
            this.set('iwlMsg','');
            this.set('iwlMsgCol','white');
        },
        //costum filtering
        enableAttrFilter: false,
        isAttrFilterVisible: false,
        showAttrFilterOptions: function(e){
            //show attr filter options
            this.set('isAttrFilterVisible',true);
            //desable filter option button. Active only once
            this.set('enableAttrFilter', false);
        },
        populateTableFields: function() {
            //get a copy of the table field
            var fields = this.attrFields;

            //Cleaning the datasource for fields (tbl-fields, attrFields) before to start to populate them again
            fields.data([]);

            fields.add({
                field: "",
                title: "Select a field",
                type: "string"
            });
            
            //sort field
            var columns = this.tableColumns;

            columns = this.sortFields(columns, 'title');
            
            //add existing field in the table
            $.each(columns,function(i,f){
                fields.add({
                    field: f.field,
                    title: f.title,
                    type: f.type
                });
            });
        },
        attrFields: new kendo.data.DataSource({
            data: []
        }),
        selAttrField: "",
        attrValues: new kendo.data.DataSource({
            data: []
        }),
        selAttrValue: [],
        tableFilters: [],
        stringMultiselectValues: [],
        isAppliedFiltersVisible : false,
        isStringFilterGUIVisible: false,
        isNumberFilterGUIVisible: false,
        isNumberSecondFilterVisible: false,
        isDateFilterGUIVisible: false,
        isDateSecondFilterVisible: false,
        sortFields: function (elements, field){
            elements.sort(
                function(a, b){
                    if (a[field] < b[field]){
                        return -1;
                    } else if (a[field] > b[field]){
                        return 1;
                    } else {
                        return 0;   
                    }
                }
            );
            return elements;
        },
        numOptions: [  {'value': '', 'title': 'Select an option'},
                        {'value': 'eq', 'title': 'Is equal to'},
                        {'value': 'neq', 'title': 'Is not equal to'}, 
                        {'value': 'gte', 'title': 'Is greater than or equal to'},
                        {'value': 'gt', 'title': 'Is greater than'},
                        {'value': 'lte', 'title': 'Is less than or equal to'}, 
                        {'value': 'lt', 'title': 'Is less than'}
        ],
        andOrOptions: [    {'value': '', 'title': 'Add/Remove another filter'},
                            {'value': 'and', 'title': 'And'},
                            {'value': 'or', 'title': 'Or'}  
        ],
        dateOptions: [  {'value': '', 'title': 'Select an option'},
                        {'value': 'eq', 'title': 'Is equal to'},
                        {'value': 'neq', 'title': 'Is not equal to'}, 
                        {'value': 'gte', 'title': 'Is after or equal to'},
                        {'value': 'gt', 'title': 'Is after'},
                        {'value': 'lte', 'title': 'Is before or equal to'}, 
                        {'value': 'lt', 'title': 'Is before'}
        ],
        changeNumberSelectAnd: function(e){
            var $this = this;
            if (e.sender.value() === ''){
                //hide the second number filter until the user change the conjunction select to AND or OR
                $this.set('isNumberSecondFilterVisible', false);
                //Clearing the values
                $("#num-select-ops2").data("kendoDropDownList").value("");
                $('#num-input2').data('kendoNumericTextBox').value("");
            }else{
                $this.set('isNumberSecondFilterVisible', true);
            }
        },
        changeDateSelectAnd: function(e){
            var $this = this;
            if (e.sender.value() === ''){
                //hide the second date filter until the user change the conjunction select to AND or OR
                $this.set('isDateSecondFilterVisible', false);
                //Clearing the values
                $("#date-select-ops2").data("kendoDropDownList").value("");
                $('#date-input2').data('kendoDateTimePicker').value("");
            }else{
                $this.set('isDateSecondFilterVisible', true);
            }
        },
        closeNumDialogOp1: function(e){
            var op1 = $('#num-select-ops1').data('kendoDropDownList');
            op1.open();
        },
        closeNumDialogOp2: function(e){
            var op2 = $('#num-select-ops2').data('kendoDropDownList');
            op2.open();
        },
        closeNumDialogIn1: function(e){
            var in1 = $('#num-input1').data('kendoNumericTextBox');
            in1.focus();
        },
        closeNumDialogIn2: function(e){
            var in2 = $('#num-input2').data('kendoNumericTextBox');
            in2.focus();
        },
        closeDateDialogOp1: function(e){
            var op1 = $('#date-select-ops1').data('kendoDropDownList');
            op1.open();
        },
        closeDateDialogOp2: function(e){
            var op2 = $('#date-select-ops2').data('kendoDropDownList');
            op2.open();
        },
        closeDateDialogIn1: function(e){
            var in1 = $('#date-input1').data('kendoDateTimePicker');
            in1.open();
        },
        closeDateDialogIn2: function(e){
            var in2 = $('#date-input2').data('kendoDateTimePicker');
            in2.open();
        },
        changeDropdownFields: function(e){
            var $this = this;
            var fields = this.attrFields;
            var dropdownFields = $("#tbl-fields").data("kendoDropDownList");
            var selectedIndex = dropdownFields.select();

            var dataItem = fields.at(selectedIndex);

            if (dataItem.type === 'string'){
                //Showing the String filtering GUI and hidding the other GUIs
                $this.set('isNumberFilterGUIVisible', false);
                $this.set('isDateFilterGUIVisible', false);
                //Checking for the 'Select a field' item
                if (dataItem.field === ''){
                    $this.set('isStringFilterGUIVisible', false);
                }else{
                    $this.set('isStringFilterGUIVisible', true);
                    $this.loadStringMultiselectValues();
                }
                
                // $this.addStringFilterGUI();
            } else if(dataItem.type === 'number'){
                //Showing the number filtering GUI and hidding the other GUIs
                $this.set('isNumberFilterGUIVisible', true);
                $this.set('isDateFilterGUIVisible', false);
                $this.set('isStringFilterGUIVisible', false);
            } else if (dataItem.type === 'date') {
                //Showing the date filtering GUI and hidding the other GUIs
                $this.set('isDateFilterGUIVisible', true);
                $this.set('isNumberFilterGUIVisible', false);
                $this.set('isStringFilterGUIVisible', false);
            } else {
                console.log("Invalid Type: " + dataItem.type);
            }
        },
        loadStringMultiselectValues: function(){
            var $this = this,
            data = $('#table').data('kendoGrid').dataSource.data(),
            field = $this.get('selAttrField');

            var values = [], distincts = [];

            $.each(data,function(i, r){
                if($.inArray(r[field], distincts) === -1){
                    distincts.push(r[field]);
                    values.push({
                        'title': r[field],
                        'value': r[field]
                    });
                }
            });

            values = $this.sortFields(values, 'field');
            $this.set('stringMultiselectValues', values);

            if (!(field in stringFilters)){
                //Cleaning the previous values selected in the multiselect
                $('#string-multiselect').data('kendoMultiSelect').value([]);
            }else{
                $('#string-multiselect').data('kendoMultiSelect').value(stringFilters[field]);
            }
        },
        changeStringMultiselect: function(e){
            var $this = this,
            field = $this.get('selAttrField'),
            filterDiv = '',
            text = 'Filter: ';

            var values = $('#string-multiselect').data('kendoMultiSelect').value();

            if (values.length > 1){
                text = 'Filters: ';
            }

            $.each(values, function(i, v) {
                text += (v === null) ? 'Null' : v;
                // text += v;
                if (!(i === values.length-1)){
                    text += ', '; 
                }
            });
            
            if (!(field in stringFilters)){                    
                var labelDiv = $('<div id="' + field + '-div" class="marg clear of-h"><span id="' + field +'-remove" class="pointer fa fa-times-circle"></span> &nbsp;' + $('#tbl-fields').data('kendoDropDownList').text() + '</div>');

                filterDiv = $('<div id="'+ field +'-filterDiv" class="clear of-h"></div>');

                var mainDiv = labelDiv.append(filterDiv);
                $('#filters-div').append(mainDiv);

                 //Adding handler for the big X in order to remove all the filters for that field
                $('#'+field+'-remove').click(function(){
                    $('#'+field+'-div').remove();
                    delete stringFilters[field];
                    if ($this.get('selAttrField') === field){
                        $('#string-multiselect').data('kendoMultiSelect').value([]);
                    }
                    $this.applyFilters();
                });

            }else{
                filterDiv = $('#'+ field +'-filterDiv');
            }

            if (values.length === 0){
                $('#'+field+'-div').remove();
                delete stringFilters[field];
            }else{
                filterDiv.text(text);
                //Creating a hard copy of the values array in order to assign it because kendo keeps the references for all stringFilters[field] and the last values will be assigned to all the keys in stringFilters
                stringFilters[field] = $.extend([], values);
            }

            $this.applyFilters();
        },
        applyFilters: function(){
            var $this = this,
            datasource = $('#table').data('kendoGrid').dataSource;

            var mainFilter = {};
            var numStringKeys = Object.keys(stringFilters).length;
            var numNumberKeys = Object.keys(numberFilters).length;
            var numDatesKeys = Object.keys(dateFilters).length;
            var totalNumKeys = numStringKeys + numNumberKeys + numDatesKeys;

            if (totalNumKeys === 0){
                $this.set('isAppliedFiltersVisible', false);
            }else {
                $this.set('isAppliedFiltersVisible', true);
            }

            if (totalNumKeys > 1){
                mainFilter = { logic: "and", filters: [] };
            }

            //Creating filters for Strings
            for(var key in stringFilters){
                var currentStringFilter = {logic: "or", filters: []};

                if (stringFilters[key].length === 1){
                        currentStringFilter = {field: key, operator: "eq", value: stringFilters[key][0]};
                }else{
                    $.each(stringFilters[key], function(i, v) {
                        currentStringFilter.filters.push({field: key, operator: "eq", value: v});
                    });
                }

                if( (numStringKeys === 1) && (numNumberKeys === 0) && (numDatesKeys === 0) ){
                    mainFilter = currentStringFilter;
                }else{
                    mainFilter.filters.push(currentStringFilter);
                }
            }

            //Creating filters for Numbers
            for(var key in numberFilters){
                var currentNumberFilter = {logic: "or", filters: []};

                if (numberFilters[key].length === 1){
                        currentNumberFilter = numberFilters[key][0];
                }else{
                    $.each(numberFilters[key], function(i, v) {
                        currentNumberFilter.filters.push(v);
                    });
                }

                if( (numStringKeys === 0) && (numNumberKeys === 1) && (numDatesKeys === 0) ){
                    mainFilter = currentNumberFilter;
                }else{
                    mainFilter.filters.push(currentNumberFilter);
                }
            }

            //Creating filters for Dates
            for(var key in dateFilters){
                var currentDateFilter = {logic: "or", filters: []};

                if (dateFilters[key].length === 1){
                        currentDateFilter = dateFilters[key][0];
                }else{
                    $.each(dateFilters[key], function(i, v) {
                        currentDateFilter.filters.push(v);
                    });
                }

                 if( (numStringKeys === 0) && (numNumberKeys === 0) && (numDatesKeys === 1) ){
                    mainFilter = currentDateFilter;
                }else{
                    mainFilter.filters.push(currentDateFilter);
                }
            }

            // console.log("Strings Filters:", stringFilters);
            // console.log("Number Filters:", numberFilters);
            // console.log("Dates Filters:", dateFilters);
            // console.log("Main Filter:", mainFilter);

            datasource.filter(mainFilter);

        },
        clickNumButtonApply: function(){
            var $this = this;
            var field = this.get('selAttrField');
            var filterNum = 0;
            var filterDiv = '';

            var op1 = $('#num-select-ops1').data('kendoDropDownList');
            var in1 = $('#num-input1').data('kendoNumericTextBox');
            var op2 = $('#num-select-ops2').data('kendoDropDownList');
            var in2 = $('#num-input2').data('kendoNumericTextBox');
            var opAnd = $('#num-select-and').data('kendoDropDownList'); 

            var dialogOp1 = $('#num-dialogOp1').data("kendoWindow");
            var dialogOp2 = $('#num-dialogOp2').data("kendoWindow");
            var dialogIn1 = $('#num-dialogIn1').data("kendoWindow");
            var dialogIn2 = $('#num-dialogIn2').data("kendoWindow");
            dialogOp1.bind("close", $this.closeNumDialogOp1);
            dialogOp2.bind("close", $this.closeNumDialogOp2);
            dialogIn1.bind("close", $this.closeNumDialogIn1);
            dialogIn2.bind("close", $this.closeNumDialogIn2);
            $('#num-okOp1').click(function() {
                dialogOp1.close();
            });
            $('#num-okIn1').click(function() {
                dialogIn1.close();
            });
            $('#num-okOp2').click(function() {
                dialogOp2.close();
            });
            $('#num-okIn2').click(function() {
                dialogIn2.close();
            });
            var newDiv = '', text ='';
            var currentFilter = {};
            var success = false;

            if (op1.value() === ''){
                dialogOp1.center().open();
            }else if(in1.value() === '' || in1.value() === null){
                dialogIn1.center().open();
            }else if (opAnd.value() !== ''){
                if (op2.value() === ''){
                    dialogOp2.center().open();
                }else if(in2.value() === '' || in2.value() === null){
                    dialogIn2.center().open();
                }else{
                    success = true;
                    text = 'Filter: '+op1.text()+' '+in1.value()+' '+opAnd.text()+' '+op2.text()+' '+in2.value();
                    currentFilter = {   logic: opAnd.value(), 
                                        filters: [  {field: field, operator: op1.value(), value: in1.value()}, 
                                                    {field: field, operator: op2.value(), value: in2.value()}
                                                ]
                    };
                }
            }else{
                success = true;
                text = 'Filter: '+op1.text()+' '+in1.value();
                currentFilter = {field: field, operator: op1.value(), value: in1.value()};
            }

            //If pass the validation then add the newDiv
            if (success){
                if (!(field in numberFilters)){
                    numberFilters[field] = [];
                    
                    var labelDiv = $('<div id="' + field + '-div" class="marg clear of-h"><span id="' + field +'-remove" class="pointer fa fa-times-circle"></span> &nbsp;' + $('#tbl-fields').data('kendoDropDownList').text() + '</div>');

                    filterDiv = $('<div id="'+ field +'-filterDiv" class="clear of-h"></div>');

                    var mainDiv = labelDiv.append(filterDiv);
                    $('#filters-div').append(mainDiv);

                     //Adding handler for the big X in order to remove all the filters for that field
                    $('#'+field+'-remove').click(function(){
                        $('#'+field+'-div').remove();
                        delete numberFilters[field];
                        $this.applyFilters();
                    });

                }else{
                    filterDiv = $('#'+ field +'-filterDiv');
                    filterNum =  filterDiv.attr("lastFilterNum");
                }

                var i = filterNum;

                newDiv = $('<div id="'+ field +'-filter'+i+'" class="margBottom">'+text+'&nbsp;<span id="' + field +'-remove-filter'+i+'" class="pointer fa fa-times"></span></div>');
                filterDiv.append(newDiv);

                //Handler for the little X for each filter
                $('#'+ field +'-remove-filter'+i).click(function(){
                    var children = $(filterDiv).children();
                    var n = children.index($(this).parent());
                    $('#'+field+'-filter'+i).remove();

                    var allFilters = numberFilters[field];
                    allFilters.splice(n, 1);

                    //Deleting the key field from numberFilter if there is no filters
                    if (allFilters.length === 0){
                        $('#'+field+'-div').remove();
                        delete numberFilters[field];
                    }else{
                        numberFilters[field] = allFilters;
                    }

                    $this.applyFilters();

                });

                filterNum++;
                //Adding the last index used in the filter div in order to use it when the user comes back from another field to this one and create more filters for this specific field.
                $('#'+field+'-filterDiv').attr("lastFilterNum", filterNum);

                //Clearing the values
                op1.value("");
                in1.value("");
                opAnd.value("");
                op2.value("");
                in2.value("");

                $this.set('isNumberSecondFilterVisible', false);

                numberFilters[field].push(currentFilter);
                $this.applyFilters();
            }
        },
        clickDateButtonApply: function(){
            var $this = this;
            var field = this.get('selAttrField');
            var filterNum = 0;
            var filterDiv = '';

            var op1 = $('#date-select-ops1').data('kendoDropDownList');
            var in1 = $('#date-input1').data('kendoDateTimePicker');
            var op2 = $('#date-select-ops2').data('kendoDropDownList');
            var in2 = $('#date-input2').data('kendoDateTimePicker');
            var opAnd = $('#date-select-and').data('kendoDropDownList'); 

            var dialogOp1 = $('#date-dialogOp1').data("kendoWindow");
            var dialogOp2 = $('#date-dialogOp2').data("kendoWindow");
            var dialogIn1 = $('#date-dialogIn1').data("kendoWindow");
            var dialogIn2 = $('#date-dialogIn2').data("kendoWindow");
            dialogOp1.bind("close", $this.closeDateDialogOp1);
            dialogOp2.bind("close", $this.closeDateDialogOp2);
            dialogIn1.bind("close", $this.closeDateDialogIn1);
            dialogIn2.bind("close", $this.closeDateDialogIn2);
            $('#date-okOp1').click(function() {
                dialogOp1.close();
            });
            $('#date-okIn1').click(function() {
                dialogIn1.close();
            });
            $('#date-okOp2').click(function() {
                dialogOp2.close();
            });
            $('#date-okIn2').click(function() {
                dialogIn2.close();
            });
            var newDiv = '', text ='';
            var currentFilter = {};
            var success = false;

            if (op1.value() === ''){
                dialogOp1.center().open();
            }else if(in1.value() === '' || in1.value() === null){
                dialogIn1.center().open();
            }else if (opAnd.value() !== ''){
                if (op2.value() === ''){
                    dialogOp2.center().open();
                }else if(in2.value() === '' || in2.value() === null){
                    dialogIn2.center().open();
                }else{
                    success = true;
                    
                    text = 'Filter: '+op1.text()+' '+kendo.toString(in1.value(), "yyyy/MM/dd HH:mm")+' '+opAnd.text()+' '+op2.text()+' '+kendo.toString(in2.value(), "yyyy/MM/dd HH:mm");
                    currentFilter = {   logic: opAnd.value(), 
                                        filters: [  {field: field, operator: op1.value(), value: in1.value()}, 
                                                    {field: field, operator: op2.value(), value: in2.value()}
                                                ]
                    };
                }
            }else{
                success = true;
                text = 'Filter: '+op1.text()+' '+kendo.toString(in1.value(), "yyyy/MM/dd HH:mm");
                currentFilter = {field: field, operator: op1.value(), value: in1.value()};
            }

            //If pass the validation then add the newDiv
            if (success){
                if (!(field in dateFilters)){
                    dateFilters[field] = [];
                    
                    var labelDiv = $('<div id="' + field + '-div" class="marg clear of-h"><span id="' + field +'-remove" class="pointer fa fa-times-circle"></span> &nbsp;' + $('#tbl-fields').data('kendoDropDownList').text() + '</div>');

                    filterDiv = $('<div id="'+ field +'-filterDiv" class="clear of-h"></div>');

                    var mainDiv = labelDiv.append(filterDiv);
                    $('#filters-div').append(mainDiv);

                     //Adding handler for the big X in order to remove all the filters for that field
                    $('#'+field+'-remove').click(function(){
                        $('#'+field+'-div').remove();
                        delete dateFilters[field];
                        $this.applyFilters();
                    });

                }else{
                    filterDiv = $('#'+ field +'-filterDiv');
                    filterNum =  filterDiv.attr("lastFilterNum");
                }

                var i = filterNum;

                newDiv = $('<div id="'+ field +'-filter'+i+'" class="margBottom">'+text+'&nbsp;<span id="' + field +'-remove-filter'+i+'" class="pointer fa fa-times"></span></div>');
                filterDiv.append(newDiv);

                //Handler for the little X for each filter
                $('#'+ field +'-remove-filter'+i).click(function(){
                    var children = $(filterDiv).children();
                    var n = children.index($(this).parent());
                    $('#'+field+'-filter'+i).remove();

                    var allFilters = dateFilters[field];
                    allFilters.splice(n, 1);

                    //Deleting the key field from numberFilter if there is no filters
                    if (allFilters.length === 0){
                        $('#'+field+'-div').remove();
                        delete dateFilters[field];
                    }else{
                        dateFilters[field] = allFilters;
                    }

                    $this.applyFilters();

                });

                filterNum++;
                //Adding the last index used in the filter div in order to use it when the user comes back from another field to this one and create more filters for this specific field.
                $('#'+field+'-filterDiv').attr("lastFilterNum", filterNum);

                //Clearing the values
                op1.value("");
                in1.value("");
                opAnd.value("");
                op2.value("");
                in2.value("");

                $this.set('isDateSecondFilterVisible', false);

                dateFilters[field].push(currentFilter);
                $this.applyFilters();
            }
        },
        cleanAllFilters: function(){
            var $this = this;
            //Cleaning the filters Div
            $("#filters-div").empty();
            stringFilters = {};
            numberFilters = {};
            dateFilters = {};
            $("#filters-div").append("<span>Applied Filters:</span>");
            $this.set('isAppliedFiltersVisible', false);
            // $this.set('enableAttrFilter', false);

            //Hiding the filters GUI
            $this.set('isStringFilterGUIVisible', false);
            $this.set('isNumberFilterGUIVisible', false);
            $this.set('isDateFilterGUIVisible', false);

            //Clearing the values for the string filter GUI
            /*$('#string-multiselect').data('kendoMultiSelect').value([]);
            //Clearing the values for the number filter GUI
            $('#num-select-ops1').data('kendoDropDownList').value("");
            $('#num-input1').data('kendoNumericTextBox').value("");
            $('#num-select-and').data('kendoDropDownList').value("");
            $('#num-select-ops2').data('kendoDropDownList').value("");
            $('#num-input2').data('kendoNumericTextBox').value("");
            $this.set('isNumberSecondFilterVisible', false);
            //Clearing the values for the dates filter GUI
            $('#date-select-ops1').data('kendoDropDownList').value("");
            $('#date-input1').data('kendoDateTimePicker').value("");
            $('#date-select-ops2').data('kendoDropDownList').value("");
            $('#date-input2').data('kendoDateTimePicker').value("");
            $('#date-select-and').data('kendoDropDownList').value("");
            $this.set('isDateSecondFilterVisible', false);*/
           
        }
    });
});
