/*
 * TFR Builder ==> builder
*/
define(["app/appConfig", "app/appUtils", "app/mapview/geomUtils", "app/renderers", "esri/renderers/UniqueValueRenderer",
	"app/windows/tfrOptions", "libs/latlong", "app/windows/tfr/tfr-aixm-input", "app/windows/tfrShapesGrid",
	"app/mapview/mapUtils", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol", "esri/Color", "esri/symbols/Symbol" ]
	,function(appConfig, appUtils, geomUtils, renderers, UniqueValueRenderer,
		tfrOptions, LatLon, aixmInput, tfrShapesGrid,
		mapUtils, SimpleFillSymbol, SimpleLineSymbol, Color, Symbol) {

	var self =  kendo.observable({
		dialogReady: function(e) {
			console.log('Dialog: ', e);
		},
		tfrDiv: 'tfr-div',
		tfrMsg: "",
		tfrModes: ['create','edit','review','publish','published'],
		tfrMode: "",
		tfrModified: false,
		// for quick conversion from true|false <==> 1|0 and vice versa
		// for toggled vals like include local time or FRD info
		// since db doesn't have BOOLEAN datatype
        convertTruth: function(state){
            switch(typeof state){
                case 'boolean':
                    return Number(state);
                case 'string':
                    return Boolean(Number(state));
                default:
                    console.log("conversion error");
            }
        },
		shapesGrid: [],
		updateTfrTab: function(e) {
			if ( !e || !e.contentElement ) return;

			var $this = this,
				entry = $(e.contentElement).hasClass('tfr-entry'),
				status = $(e.contentElement).hasClass('tfr-status'),
				notam = $(e.contentElement).hasClass('tfr-notam-text'),
				webText = $(e.contentElement).hasClass('tfr-web-text'),
				airports = $(e.contentElement).hasClass('tfr-airports');

			if (notam) {
				var editor = $("#notamEditor").data("kendoEditor"),
				editorBody = $(editor.body);
				editorBody.add("td", editorBody).removeAttr("contenteditable");
				$this.updateTfrNotamText();
			}

			if (webText) {
				$this.updateTfrWebText();
			}

			if (airports) {
				$('#tfr-arps').data('kendoGrid').refresh();
			}
		},
		tfrShowForm: false,
		tfrTypes2: new kendo.data.DataSource({
			data: []
		}),
		tfrTypes: function () {
			return new kendo.data.DataSource({
				transport: {
	                read: function(options) {
	                	$.ajax({
	                		url: "api/tfr/",
	                		type: "POST",
	                		dataType: 'json',
	                		data: {action: 'tfrtypes', filter: ''},
	                		success: function(data) {
	                			if (data) {
	                				options.success(data);
	                			} else {
		                			options.success([]);
		                		}
	                		},
	                		error: function(err) {
	                			options.error(err);
	                		}
	                	});
	                }
	            }
			});
		},
		tfrTypeId: 0,
		tfrType: null,
		tfrDesc: "",
		tfrName: "",
		notam_id: "",
		notam_usns_status: "",
		tfrTitle: "",
		tfrTemplate: "",
		notamTemplate: "",
		updateTfrType: function(e) {
			// console.log("updateTfrType - event ....",e);
			// console.log("updateTfrType - e.sender.dataItem() ....",e.sender.dataItem());

			var $this = this,
			name = $this.get('tfrName'),
			item = e.sender.dataItem();

			//To prevent the ID undefined error. for some reason this function is being executed when the dropdown has not selected any element, that's why ID is undefined, then later the function is executed normally.
			if(typeof item !== 'undefined'){
				var	id = item['ID'],
					type = item['TYPE'],
					desc = item['DESCRIPTION'],
					template = item['TEMPLATE'],
					notamTemplate = item['TEMPLATE'];

				$this.set('tfrType', type);
				$this.set('tfrTemplate', template);
				$this.set('notamTemplate', template);

				var tfrSource = $('#tfr-source-sel');

				if ( tfrSource && tfrSource.length ) {
					tfrSource.data('kendoDropDownList')
					.dataSource.read();
				}

				//update title
				$this.updateTfrTitle();
				//update form
				$this.updateTfrForm.call($this, id);
				//only necessary with dropdown
				$this.updateAreaInstructions.call($this, e);
			}
		},
		getTfrTypeFromSource: function(e) {
			var $this = this,
				type = $this.get('tfrTypeId'),
				dt = e.sender.dataItem(),
				t = dt && dt['TYPE'] ? dt['TYPE'] : null,
				typeSel = $('#tfr-type-sel');

			if (t && t !== type) {
				$this.set('tfrTypeId', t);

				typeSel.data('kendoDropDownList').value(t);
				typeSel.data('kendoDropDownList').trigger('change');
			}
		},
		updateTfrTitle: function(e) {
			var $this = this,
				name = $this.get('tfrName'),
				typeId = $this.get('tfrTypeId'),
				type = $this.get('tfrType'),
				desc = $this.get('tfrDesc'),
				template = $this.get('tfrTemplate'),
				title;

			//update title
			title = name && name != "" ? name : "TFR";
			title += " - ";
			title += type && type != "" ? type : "00.0";
			//set new name
			$this.set('tfrTitle', title);
		},
		updateTfrForm: function(type) {
			var $this = this,
				authTypes = [4,5,6,7,9,10,11]
				type = type ? type : $this.get('tfrTypeId');

			type = parseInt(type);

			//check flight auth
			if ( $.inArray(type, authTypes) != -1 ) {
				$this.set('allowFlights', true);
			} else {
				$this.set('allowFlights', false);
			}

			switch (type) {
				//hazard ==> 1 - 3
				case 1:
				case 2:
				case 3:
					$this.set('gOpsVisible', true);
					$this.set('gOpsLabel', "Point of Contact");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', true);
					$this.set('tfrTxtRsVisible', true);
					$this.set('tfrSelRsVisible', false);
				break;
				//VIP ==> 4 - 7
				case 4:
				case 5:
				case 6:
				case 7:
					$this.set('gOpsVisible', false);
					$this.set('gOpsLabel', "");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', false);
					$this.set('tfrTxtRsVisible', false);
					$this.set('tfrSelRsVisible', false);
				break;
				//AIRSHOW ==> 8
				case 8:
					$this.set('gOpsVisible', true);
					$this.set('gOpsLabel', "Point of Contact");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', true);
					$this.set('tfrTxtRsVisible', true);
					$this.set('tfrSelRsVisible', false);
				break;
				//SECURITY ==> 9 - 11
				case 9:
				case 10:
				case 11:
					$this.set('gOpsVisible', true);
					$this.set('gOpsLabel', "Group in charge of the operation");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', true);
					$this.set('tfrTxtRsVisible', false);
					$this.set('tfrSelRsVisible', true);
				break;
				//SPACE OPERATIONS ==> 12
				case 12:
					$this.set('gOpsVisible', false);
					$this.set('gOpsLabel', "");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', false);
					$this.set('tfrTxtRsVisible', false);
					$this.set('tfrSelRsVisible', false);
				break;
				//HAWAII ==> 13
				case 13:
					$this.set('gOpsVisible', false);
					$this.set('gOpsLabel', "");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', false);
					$this.set('tfrTxtRsVisible', false);
					$this.set('tfrSelRsVisible', false);
				break;
				//EMERGENCY ==> 14
				case 14:
					$this.set('gOpsVisible', false);
					$this.set('gOpsLabel', "");
					$this.set('crdFacVisible', true);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', false);
					$this.set('tfrTxtRsVisible', false);
					$this.set('tfrSelRsVisible', false);
				break;
				//default
				default:
					$this.set('gOpsVisible', false);
					$this.set('gOpsLabel', "");
					$this.set('crdFacVisible', false);
					$this.set('ctlFacVisible', false);
					$this.set('tfrRsVisible', false);
					$this.set('tfrTxtRsVisible', false);
					$this.set('tfrSelRsVisible', false);
				break;
			}
		},
		trackTfrForm: function() {
			var $this = this;

			$('form#tfr-data-form').on('change', function(evt) {
				//console.log('Tracking tfr form:', evt);

				var target = evt.target,
					time = evt.timeStamp,
					except = $(target).hasClass('do-not-track');
				//console.log('Exceptions: ', except);
				if ( except ) return;

				$this.set('tfrModified', true);
			});
		},
		tfrCopied: false,
		tfrSrcEnabled: false,
		tfrSource: '',
		tfrSources: function () {
			var $this = this;

			return new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "api/tfr/"
                        ,type: 'GET'
                        ,dataType: 'json'
                    }
                    ,parameterMap: function(data, type) {
                        var filter = "",
                        	type = $this.get('tfrTypeId');

                        if (data && data.filter) {
                            if (data.filter.filters && data.filter.filters.length) {
                                filter = data.filter.filters[0].value;
                            }
                        }

                        return {
                            'action': 'tfr-list',
                            'filter': filter,
                            'type': type
                            //'type': type ? type : 4
                        };
                    }
                },
                serverFiltering: true,
                error: function(e) {
                    //console.log('TFR List Error:', e);
                    var msg = 'App was not able to retrieve existing TFRs.';
                    nisis.ui.displayMessage(msg, 'error');
                }
            });
		},
		//tfr artcc
		artccId: '',
		artccName: '',
		tfrArtcssCached: null,
		tfrArtccs: function () {			
			if (this.tfrArtcssCached == null) {				
				var tfrArcssDS= new kendo.data.DataSource({
					transport: {
		                read: function(options) {
		                	$.ajax({
		                		url: "api/tfr/",
		                		type: "POST",
		                		dataType: 'json',
		                		data: {action: 'artccs', filter: ''},
		                		success: function(data) {
		                			if (data) {
		                				options.success(data);
		                			} else {
			                			options.success([]);
			                		}
		                		},
		                		error: function(err) {
		                			options.error(err);
		                		}
		                	});
		                }
		            }
				});
				this.tfrArtcssCached = tfrArcssDS;				
				return this.tfrArtcssCached;

			}
			else {				
				return this.tfrArtcssCached;
			}
		},
		updateArtccName: function(e) {
			var $this = this,
				id = $this.get('artccId'),
				name = $this.get('artccName'),
				idx = e.sender.select(),
				item = e.sender.dataItem(),
				nameEl = e.sender.element.data('artcc'),
				nameDD = $('select.' + nameEl).data('kendoDropDownList');

			//$this.setTfrTimeZone([item.LON, item.LAT]);
			
			if (nameDD && idx) {
				nameDD.select(idx);
				$this.set('artccName', nameDD.text());
			} else {
				$this.set('artccName', null);
			}
		},
		updateArtccId: function(e) {
			var $this = this,
				id = $this.get('artccId'),
				name = $this.get('artccName'),
				idx = e.sender.select(),
				item = e.sender.dataItem(),
				idEl = e.sender.element.data('artcc'),
				idDD = $('select.' + idEl).data('kendoDropDownList');

			//$this.setTfrTimeZone([item.LON, item.LAT]);

			if (idDD && idx) {
				idDD.select(idx);
				$this.set('artccId', idDD.text());
			} else {
				$this.set('artccId', null);
			}
		},
		resetArtcc: function() {
			this.set('artccId', '');
			this.set('artccName', '');
		},
		//time zones
		effTimeZone: "",
		expTimeZone: "",
		//CC - These variables will be used to make the calculations instead of the ones above.
		effTimeZoneOffset: 0,
		expTimeZoneOffset: 0,
		effTimeZoneOnChange: function (e) {
			var $this = this,
			dropdown,
			effTimeZoneOffset="";

			if (e){
				dropdown = e.sender;
			}else{
				dropdown = $("#effectiveTimeZone").data("kendoDropDownList");
			}

			var id = dropdown.value();
			//CC - Added this validation to prevent errors when a TFR without Timezones is opened.
			if (id == null || id == ""){
				return;
			}else{
				var dataSource = dropdown.dataSource;
				effTimeZoneOffset = dataSource.get(id).OFFSET; //Getting the offset for the selected item
				
				$this.set("effTimeZoneOffset", effTimeZoneOffset);
			}
		},
		expTimeZoneOnChange: function (e) {
			var $this = this,
			dropdown,
			expTimeZoneOffset="";

			if (e){
				dropdown = e.sender;
			}else{
				var dropdown = $("#expirationTimeZone").data("kendoDropDownList");
			}

			var id = dropdown.value();
			//CC - Added this validation to prevent errors when a TFR without Timezones is opened.
			if (id == null || id == ""){
				return;
			}else{
				var dataSource = dropdown.dataSource;
				expTimeZoneOffset = dataSource.get(id).OFFSET; //Getting the offset for the selected item
				
				$this.set("expTimeZoneOffset", expTimeZoneOffset);
			}
		},
		tfrTimeZonesCached: null,
		tfrTimeZones: function () {
			if (this.tfrTimeZonesCached == null) {
				var tfrTimeZonesDS = new kendo.data.DataSource({
					transport: {
		                read: function(options) {
		                	$.ajax({
		                		url: "api/tfr/",
		                		type: "POST",
		                		dataType: 'json',
		                		data: {action: 'timezones', filter: ''},
		                		success: function(data) {
		                			if (data) {
		                				options.success(data);
		                			} else {
			                			options.success([]);
			                		}
		                		},
		                		error: function(err) {
		                			options.error(err);
		                		}
		                	});
		                }
		            },
					schema: {
						// data: "data",
						model: {
							id: "ID",
							fields: {
								ID: {editable: false},
								NAME: {editable: false},
								OFFSET: {editable: false}
							}
						}
					}
				});
				this.tfrTimeZonesCached = tfrTimeZonesDS;				
				return this.tfrTimeZonesCached;
			}
			else {				
				return this.tfrTimeZonesCached;
			}
		},
		setTfrTimeZone: function(center) {
			if (!center) {
				return;
			}

			if ( !center[0] || center[0] == "" || !center[1] || center[1] == "") {
				return;
			}

			var $this = this,
				geom = null,
				query = null,
				mapview = nisis.ui.mapview,
				z = mapview.map.getZoom(),
				tzLayer = mapview.map.getLayer('timezones');

			if ( !tzLayer ) {
				return;
			}

			geom = new esri.geometry.Point(center, new esri.SpatialReference({wkid: 4326}));
			// geom2 = esri.geometry.geographicToWebMercator(geom);
			//zoom to ARTCC Center
			//mapview.map.centerAt(geom2);

			//Comment this line out to fix NISIS-1489
			//mapview.map.centerAndZoom(geom2, 7);

			//get time zone of ARTCC
			query = mapview.util.queryFeatures({
				layer: tzLayer,
				geom: geom,
				returnGeom: false
			});

			if (!query) return;

			query.then(function(result) {
				//console.log('Timezone result: ', result);
				if ( !result || !result.features || !result.features.length) {
					return;
				}
				var feat = result.features[0],
					zone = feat.attributes.ZONE;

				if ( zone && $.isNumeric(zone) ) {
					$this.set('effTimeZoneOffset', zone);
					$this.set('expTimeZoneOffset', zone);
				}

				var daylight = new Date(),
					curSelection = $(".tfr-eff-tz")[1].selectedIndex,
					selectionText = $(".tfr-eff-tz")[1].options[curSelection].innerHTML,
					effTimezoneDD = $(".tfr-eff-tz").find(".k-input")[0],
					expTimezoneDD = $(".tfr-exp-tz").find(".k-input")[0];

					if (effTimezoneDD.innerHTML != selectionText){
						effTimezoneDD.innerHTML;
						//console.log(effTimezoneDD.innerHTML);
					}
					if (expTimezoneDD.innerHTML){
						expTimezoneDD.innerHTML;
						//console.log(expTimezoneDD.innerHTML);
					}
			}, function(error) {
				//console.log('Timezone error: ', error);
				$this.set('tfrMsg', 'App could not update the time zones.');
			}, function(note) {
				//console.log('Timezone note: ', note);
			});
		},
		/*
		JS - both this and updateExpTimeZone were being used to update one or
		the other at the same time now this is not the case so we don't need these.
		*/
		/*updateEffTimeZone: function(e) {
			var $this = this,
				item = e.sender.dataItem(),
				eff = e.sender.element.data('timezone'),
				idx = e.sender.select(),
				tz = null;

			if (!item || !item['VALUE']) {
				return;
			}
			tz = item['VALUE'];

			var effDD = $('select.' + eff).data('kendoDropDownList');

			if (effDD && idx) {
				$this.set('effTimeZoneOffset', effDD.value());
				effDD.select(idx);
			} else {
				$this.set('effTimeZoneOffset', 0);
			}
		},
		updateExpTimeZone: function(e) {
			var $this = this,
				item = e.sender.dataItem(),
				exp = e.sender.element.data('timezone'),
				idx = e.sender.select(),
				tz = null;

			if (!item || !item['VALUE']) {
				return;
			}
			tz = item['VALUE'];

			var expDD = $('select.' + exp).data('kendoDropDownList');

			if (expDD && idx) {
				$this.set('expTimeZoneOffset', expDD.value());
				expDD.select(idx);
			} else {
				$this.set('expTimeZoneOffset', 0);
			}
		},*/
		resetTimeZones: function() {
			this.set('effTimeZone', "");
			this.set('expTimeZone', "");
			this.set('effTimeZoneOffset', 0);
			this.set('expTimeZoneOffset', 0);
		},
		//locations
		rmLocVisible: false,
		getLocTemplate: function() {
			return {
				city: null,
				cityEnabled: false,
				state: null,
				stName: "",
			};
		},
		locations: {
			loc0: {
				city: null,
				cityEnabled: false,
				state: null,
				stName: "",
			}
		},
		tfrCities: function() {
			return [{
				ID: 0,
				NAME: "",
				STATE: "",
				LAT: 0,
				LON: 0
			}];
		},
		tfrGetCities: function(state) {
			var def = new $.Deferred();

			$.ajax({
        		url: "api/tfr/",
        		type: "GET",
        		dataType: 'json',
        		data: {action: 'cities', filter: state},
        		success: function(data) {
        			if(data.length) {
        				def.resolve(data);
        			} else {
        				//console.log('Cities success (empty): ', data);
        				def.resolve([{'NAME': 'Select a city'}]);
        			}
        		},
        		error: function(err) {
        			//console.log('Cities error: ', err);
        			def.reject([{'NAME': 'Select a city'}]);
        		}
        	});

        	return def.promise();
		},
		tfrStates: function() {
			return new kendo.data.DataSource({
	            transport: {
	                read: function(options) {
	                	//console.log('States:', options);

	                	$.ajax({
	                		url: "api/tfr/",
	                		type: "POST",
	                		dataType: 'json',
	                		data: {action: 'states', filter: ''},
	                		success: function(data) {
	                			if (data) {
	                				options.success(data);
	                			} else {
		                			options.success([]);
		                		}
	                		},
	                		error: function(err) {
	                			options.error(err);
	                		}
	                	});
	                }
	            }
	        });
		},
		updateCities: function (e) {
			if(!e || !e.sender) {
				//console.log('States change event is empty');
				return;
			}

			var $this = this,
				st = e.sender.value(),
				item = e.sender.dataItem(),
				fItem = [{
					ID: 0,
					NAME: "",
					STATE: "",
					LAT: 0,
					LON: 0
				}],
				row = e.sender.element.parent().parent(),
				loc = e.sender.element.data('location'),
				cEnabled = "locations." + loc + ".cityEnabled",
				stN = 'locations.' + loc + '.stName',
				stAB = 'locations.' + loc + '.state',
				ctN = 'locations.' + loc + '.city',
				city = row.find('select.tfr-loc-city'),
				ctDD = city.data('kendoComboBox'),
				ctDS = ctDD.dataSource;

			if (!st || st === "") {
				ctDS.data(fItem);
				$this.set(cEnabled, false);
				ctDD.value(null);

				return;
			}

			var stName = item['NAME'];
			$this.set(stN, stName);

			var stAbbrev = item['ABBR'];
			$this.set(stAB, stAbbrev);

			var data = $this.tfrGetCities(st);

			data.then(function(data){
				if (data && data.length) {
					ctDS.data(data);
					$this.set(cEnabled, true);
				} else {
					ctDS.data(fItem);
					$this.set(ctN, null);
					$this.set('tfrMsg', 'Error: App could not retreive matching cities.');
				}
			},function(err){
				ctDS.data(fItem);
				$this.set(cEnabled, false);
				$this.set(ctN, null);
				$this.set('tfrMsg', 'Error: App could not retreive matching cities.');
			});
		},
		updateStates: function (e) {
			var $this = this,
				val = e.sender.value(),
				idx = e.sender.select(),
				row = e.sender.element.parent().parent(),
				state = row.find('select.tfr-loc-state'),
				stDD = state.data('kendoDropDownList'),
				stDS = stDD.dataSource,
				sel = e.sender.dataSource.get(idx);

			//set the corresponding state name
			stDD.value(sel.state);
		},
		updateLocData: function(e) {
			//console.log("Location Data: ", arguments);
			var $this = this,
				city = e.sender.value(),
				idx = e.sender.select(),
				item = e.sender.dataItem(),
				loc = e.sender.element.data('location');

			var cityN = "locations." + loc + ".city";
			if (city) {
				$this.set(cityN, city);
			} else {
				$this.set(cityN, null);
			}
		},
		addNewLocation: function(e) {
			var $this = this,
				//row = $(e.currentTarget).parent(),
				div = $('.tfr-locs');

			var loc = div.children('div.tfr-loc:last'),
				n = 0,
				newLoc = {
					"city": null,
					"cityEnabled": false,
					"state": "",
					"stName": ""
				};

			loc = $(loc).data('location');

			if (!loc) {
				//console.log('Location is not labeled: ', loc);
				return;
			}

			n = loc.substring(3);
			n = parseInt(n);

			if (!$.isNumeric(n)) {
				//console.log('Location: ', n);
				return;
			}

			n++;

			if ($this.locations["loc" + n]) {
				//console.log('Location exist: ', $this.locations["loc" + n]);
				return;
			}
			var newLocData = "locations.loc" + n;
			//$this.locations["loc" + n] = newLoc;
			$this.set(newLocData,newLoc);

			var newRow = $("<div/>", {
				'class': 'tfr-loc marg clr of-h',
				"data-location": "loc" + n
			});

			$("<select />", {
				"class": "tfr-loc-state w-300 marg m-r15",
				"data-location": "loc" + n,
				"data-role": "dropdownlist",
				"data-auto-bind": "false",
				"data-filter": "contains",
				"data-option-label": "Select a state",
				"data-text-field": "NAME",
               	"data-value-field": "ABBR",
               	"data-bind": "source:tfrStates,value:locations.loc" + n + ".state,events:{change:updateCities}"
			}).appendTo(newRow);

			$("<select />", {
				"class": "tfr-loc-city w-300 marg",
				"data-location": "loc" + n,
				"data-role": "combobox",
				"data-auto-bind": "false",
				"data-filter": "contains",
				"data-option-label": "Select a city",
				"data-text-field": "NAME",
               	"data-value-field": "NAME",
               	"data-bind": "source:tfrCities,enabled:locations.loc" + n + ".cityEnabled,events:{change:updateLocData}"
			}).appendTo(newRow);

			$("<i/>", {
				"class": "pointer marg m-l fa fa-times",
				"title": "Remove",
				"data-location": "loc" + n,
				"data-bind": "click:removeNewLocation,visible:rmLocVisible"
			}).appendTo(newRow);

			div.append(newRow);
			//add new dom and init
			kendo.init(newRow);
			kendo.bind(newRow, $this);
			//newRow.find('select.tfr-loc-city').data('kendoDropDownList').value(null);
			//user can now remove a location
			$this.set('rmLocVisible', true);
		},
		removeNewLocation: function(e) {
			var $this = this,
				row = $(e.currentTarget).parent();
				loc = $(row).data('location');

			if ($('.tfr-loc').length > 1) {
				row.remove();
			}

			if ($('.tfr-loc').length < 2) {
				$this.set('rmLocVisible', false);
			} else {
				$this.set('rmLocVisible', true);
			}

			if ($this.locations[loc]) {
				delete $this.locations[loc];
			}
		},
		cleanLocations: function() {
			var locs = $('div.tfr-loc'),
				locTemp = this.getLocTemplate(),
				$this = this;
				
			if ( locs.length < 2) {
				$this.locations['loc0'].city = "";
				$this.locations['loc0'].state = "";
				$this.locations['loc0'].stName = "";
				$(locs).find('select.tfr-loc-state')
					.each(function() {
						$(this).data('kendoDropDownList')
						.value('');
					});

					$(locs).find('select.tfr-loc-city')
					.each(function() {
						$(this).data('kendoComboBox')
						.value('');
					});

				return;
			}

			$.each(locs, function(i, loc) {
				if ( i === 0 ) {

					$(loc).find('select.tfr-loc-state')
					.each(function() {
						$(this).data('kendoDropDownList')
						.value('');
					});

					$(loc).find('select.tfr-loc-city')
					.each(function() {
						$(this).data('kendoComboBox')
						.value('');
					});

				} else {
					$(loc).remove();
				}
			});

			this.set('rmLocVisible', false);

			this.set('locations', {});
			this.set('locations.loc0', locTemp);
		},
		//Ops Instructions & Area Rep: BK: This has been removed from areas section
		includeFRD: true,
		includeLocTime: true,
		frdOption: "component",
		//Point of Contact
		gOpsLabel: "Group in charge of the operation",
		//Group in charge
		gOpsVisible: true,
		gOpsName: "",
		gOpsOrg: "",
		gOpsTels: "",
		gOpsFreqs: "",
		resetOpsGrp: function() {
			this.set('gOpsLabel', "Group in charge of the operation");
			this.set('gOpsVisible',  true);
			this.set('gOpsName', "");
			this.set('gOpsOrg', "");
			this.set('gOpsTels', "");
			this.set('gOpsFreqs', "");
		},
		//Coordinating Fac
		crdFacVisible: true,
		crdFacId: "",
		crdFacName: "",
		crdFacType: null,
		crdFacTypesCached: null,
		crdFacTypes: function () {
			if (this.crdFacTypesCached == null) {
				var crfFacTypesDS = new kendo.data.DataSource({
		            transport: {
		                read: function(options) {
		                	$.ajax({
		                		url: "api/tfr/",
		                		type: "POST",
		                		dataType: 'json',
		                		data: {action: 'fac-type', filter: ''},
		                		success: function(data) {
		                			if (data) {
		                				options.success(data);
		                			} else {
			                			options.success([]);
			                		}
		                		},
		                		error: function(err) {
		                			options.error(err);
		                		}
		                	});
		                }
		            }
		        });
		        this.crdFacTypesCached = crfFacTypesDS;				
				return this.crdFacTypesCached;
			}
			else {				
				return this.crdFacTypesCached;
			}
		},
		crdFacTels: "",
		crdFacFreqs: "",
		resetCrdFac: function() {
			this.set('crdFacVisible', true);
			this.set('crdFacId', "");
			this.set('crdFacName', "");
			this.set('crdFacType', null);
			this.set('crdFacTels', "");
			this.set('crdFacFreqs', "");
		},
		//controlling Fac
		ctlFacVisible: false,
		ctlFacName: "",
		ctlFacType: "",
		ctlFacTels: "",
		ctlFacFreqs: "",
		resetCtlFac: function() {
			this.set('ctlFacVisible', false);
			this.set('ctlFacName', "");
			this.set('ctlFacType', "");
			this.set('ctlFacTels', "");
			this.set('ctlFacFreqs', "");
		},
		//tfr reason
		tfrRsVisible: true,
		tfrTxtRsVisible: false,
		tfrSelRsVisible: true,
		tfrReason: null,
		tfrReasons: [
			{
				id: 1,
				name: "National Security",
				value: "National Security"
			},{
				id: 2,
				name: "NFL Super Bowl",
				value: "NFL Super Bowl"
			}
		],
		resetTfrReason: function() {
			this.set('tfrRsVisible', true);
			this.set('tfrTxtRsVisible', false);
			this.set('tfrSelRsVisible', true);
			this.set('tfrReason', null);
		},
		//mod prev notam
		modTfrNum: "",
		modTfrReason: "",
		resetTfrMod: function() {
			this.set('modTfrNum', "");
			this.set('modTfrReason', "");
		},
		//reset tfr extra fields
		resetTfrExtra: function() {
			this.resetOpsGrp();
			this.resetCrdFac();
			this.resetCtlFac();
			this.resetTfrReason();
			this.resetTfrMod();
		},
		//tfr airports
		getArpCols: '[{"field":"id","title":"ID","width":50},{"field":"area","title":"Area","width":80},{"field":"arp_id","title":"Airport ID","width":60},{"field":"arp_name","title":"Airport Name","width":150},{"field":"location","title":"Location","width":200}]',
		arpList: [{},{},{},{},{},{},{},{},{},{}],
		arpQueries: 0,
		getArpList: function(e) {
			var $this = this,
				tfrName = $this.get('tfrName'),
				areas = $this.get('areas'),
				aIds = [];
				airports = [];

			//get tfr areas shapes id
			areas = areas.toJSON ? areas.toJSON() : areas;
			for (var idx in areas) {
				var area = areas[idx];
				//console.log('Area #' + idx + ': ', area);
				if (area.shape && area.shape.length) {
					var shapesArray = area.shape;
					$.each(shapesArray, function(i, shp) {
						aIds = aIds.concat(shp.OBJECTID);
					});
				}
			}

			if ( !aIds.length ) {
				var msg = 'There there are no areas selected for this TFR.';
				$this.set('tfrMsg', msg);
				return;
			}

			$this.getAreasShapes(aIds, 'tfr');
			console.log("are shapes:", $this.getAreasShapes(aIds, 'tfr'))
			$this.set('tfrMsg', 'Processing request ...');

			//reset airports tables
			$('#tfr-arps').data('kendoGrid').dataSource.data([]);
			$this.set('arpList', airports);
		},
		resetArpList: function() {
			//reset airports tables
			var arpList = [{},{},{},{},{},{},{},{},{},{}],
				arpTable = $('#tfr-arps').data('kendoGrid');

			if (arpTable && arpTable.dataSource) {
				arpTable.dataSource.data(arpList);
				this.set('arpList', arpList);
			}
		},
		getAreasShapes: function(ids, target) {
			if (!ids) return;

			var $this = this,
				mapview = nisis.ui.mapview,
				tfrLayer = mapview.map.getLayer('tfr_polygon_features');

			if (!tfrLayer) return;
			$this.set('tfrMsg', 'Querying areas shapes ...');

			mapview.util.queryFeatures({
				layer: tfrLayer,
				objectIds: ids,
				returnGeom: true

			}).then(function(result) {
				if (!result || !result.features || !result.features.length) {
					$this.set('tfrMsg', 'App could not retreive TFR Shapes.');
					return;
				}
				var feats = result.features,
					geom = result.features[0].geometry;

				$this.set('arpQueries', feats.length);

				//query airports for each area
				$.each(feats, function(i, feat) {
					$this.getAirports(feat.geometry, feat.attributes.NAME);
				});

			}, function(error) {
				var msg = error.message ? error.message : error;
				$this.set('tfrMsg', msg);
			}, function(node) {
				$this.set('tfrMsg', node);
			});
		},
		getAirports: function(geom, area) {
			var $this = this,
				query = null,
				mapview = nisis.ui.mapview,
				aptsLayer = mapview.map.getLayer('airports');

			if (!aptsLayer) return;

			$this.set('tfrMsg', 'Querying airports data ...');

			try {
				query = mapview.util.queryFeatures({
					layer: aptsLayer,
					geom: geom,
					returnGeom: false,
					outFields: ['FAA_ID', 'LG_NAME', 'ASSOC_CITY', 'ADDRESS']
				});
			} catch (err) {
				$this.set('tfrMsg', 'Error processing request.');
			}

			if (!query) return;

			query.then(function(result) {
				//Reducing the number of 'q' to know when the last query is executed to finally clear the TfrMsg if there are results in the other queries.
				var q = $this.get('arpQueries');
				q--;
				$this.set('arpQueries', q);

				if (!result || !result.features || !result.features.length) {
					$this.set('tfrMsg', 'App could not retreive airports.');
					return;
				}

				var airports = [],
					len = $('#tfr-arps').data('kendoGrid').dataSource.data().length;

				$.each(result.features, function(i, arp) {

					var id = i+1;

					if ( len ) {
						id += len;
					}

					var a = {
						'id': id,
						'area': area ? area : 'Area - XX',
						'arp_id': arp.attributes.FAA_ID,
						'arp_name': arp.attributes.LG_NAME,
						'location': arp.attributes.ADDRESS ? arp.attributes.ADDRESS : arp.attributes.ASSOC_CITY
					};

					airports.push(a);

					$('#tfr-arps').data('kendoGrid').dataSource.add(a);
				});

				if (q <= 0) {
					//If the query is the last and it went successfully then clear the previous message
					$this.set('tfrMsg', '');
				}

			}, function(error) {
				var msg = error.message ? error.message : error;
				$this.set('tfrMsg', msg);

				var q = $this.get('arpQueries');
				q--;
				$this.set('arpQueries', q);
				if (q <= 0) {
					$this.set('tfrMsg', '');
				}

			}, function(node) {
				$this.set('tfrMsg', node);
			});
		},
		//dates
		crtTimeZone: 0,
		setCrtTimeZone: function(e){
			var offset = new Date().getTimezoneOffset() / 60;

			var tz = -1 * offeset;

			this.set('crtTimeZone', tz);
		},
		crtDateTime: "",
		getCrtDateTime: function(e) {
			var d = new Date();

			return d.toString();
		},
		exitTfrMenu: true,
		exitTfrMode: function(e) {
			var $this = this,
			msg = "",
			options = {
				title: 'Exit TFR Mode',
				message: 'Are you sure you want to exit TFR Mode?',
				buttons: ['YES', 'NO']
			};

			if (!$this.get('exitTfrMenu')) return;
			//ask for user confirmation
			nisis.ui.displayConfirmation(options)
			.then(
			function(resp){
				if(resp == options.buttons[0]) {
					var tfrDiv = $this.get('tfrDiv'),
						tfrWin = $('#' + tfrDiv);
					//close tfr window
					tfrWin.data('kendoWindow').close();

					var saveIcon = $("#tfr-menu").children()[1];
					if (saveIcon.getAttribute('class').indexOf('tfr-save-copy') >= 1){
						saveIcon.remove();
					}
					//update shapetools
                    require(['app/windows/layers', 'app/windows/shapetools'],
                    	function(layers, shapetools) {

                        shapetools.set('nisisShapes', true);
                        shapetools.set('app', 'nisis');
                        layers.set('isNisisActive', true);
                    });
                    //reset form
					$this.resetTfrData();
					//reset application mode
					appConfig.set('app', 'nisis');
					appConfig.set('tfrMode', false);
					$('#app').trigger('module-change');
					//notify user
					msg = 'You have successfully exited TFR Mode';
					//nisis.ui.displayMessage(msg);
					//BK: Reset styles to normal
					$this.grayOutTfrShapes(false);
					//reset active shapes to empty array
					renderers.tfrActiveShapes = [];

 					//retrieve saved query defintion and set it for  Shapes Polygon layer to display shapes
                    var shapeLayer = nisis.ui.mapview.map.getLayer('Polygon_Features');       
                    if (shapeLayer) {
                        var shapeLayerQuery = appConfig.get('shapeLayerDefQuery');                                          
                        shapeLayer.setDefinitionExpression(shapeLayerQuery );
                    }
				}
			},
			function(error){
				msg = 'App could not capture your answer. Please try again.';
				nisis.ui.displayMessage(msg, 'error');
			});
		},
		createTfrReady: false,
		grayOutTfrShapes: function(gray) {
			var tfrLayer = nisis.ui.mapview.map.getLayer("tfr_polygon_features"),
				renderer = tfrLayer ? tfrLayer.renderer : null;

			if (!renderer) return;
			//set default symbol as gray outline with no fill
			var dSymbol = new SimpleFillSymbol(),
            	style = SimpleFillSymbol.STYLE_SOLID,
            	outSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([211,211,211,1]), 2),
            	fillColor = new Color([255,255,255,0]),
            	color = new Color([211,211,211,1]),
            	rend = null;

            dSymbol.setStyle(style);
            dSymbol.setOutline(outSymbol);
            dSymbol.setColor(fillColor);

            //Reset the active shapes selection
			renderers.tfrActiveShapes = [];

			if (gray) {
				//set renderer based on OBJECTID
				rend = new UniqueValueRenderer(dSymbol, 'OBJECTID');
				tfrLayer.setRenderer(rend);
				//force a refresh after updating the rendering
				//tfrLayer.refresh(); //BK: 01/12/2016 ==> This is throwing drawing error. Not a big deal but I am still removing it.

			} else {
				//set renderer based on TFR_SHAPE
				rend = new UniqueValueRenderer(dSymbol, 'TFR_SHAPE');
				//Add unique values
				rend.addValue({
                    value: "base",
                    symbol: new SimpleFillSymbol(
                    	SimpleFillSymbol.STYLE_SOLID,
                    	new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,112,255,1]), 2),
                    	fillColor)
                });
                rend.addValue({
                    value: "add",
                    symbol: new SimpleFillSymbol(
                    	SimpleFillSymbol.STYLE_SOLID,
                    	new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([76,230,0,1]), 2),
                    	fillColor)
                });
                rend.addValue({
                    value: "subtract",
                    symbol: new SimpleFillSymbol(
                    	SimpleFillSymbol.STYLE_SOLID,
                    	new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255,0,0,1]), 2),
                    	fillColor)
                });
                rend.addValue({
                    value: "none",
                    symbol: new SimpleFillSymbol(
                    	SimpleFillSymbol.STYLE_SOLID,
                    	new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0,1]), 2),
                    	fillColor)
                });

                tfrLayer.setRenderer(rend);
				//force a refresh after updating the rendering
				//tfrLayer.refresh(); //BK: 01/12/2016 ==> This is throwing drawing error. Not a big deal but I am still removing it.
			}
		},
		createTfr: function(e) {

			//gray out all tfr polygon features from the start
			/*
			 * BK: 12/30/2015
			 *     Applying the gray color to all tfr graphics like this
			 *     will be applied only to graphics visible in the map extent
			 *     and refreshingt the tfr layer will remove the gray color.
			 * 	   The best way to do this is to user a renderer
			 */
			/*var allMappedPolygonGraphics = nisis.ui.mapview.layers.tfr_polygon_features.graphics;
			var grayOutInactiveSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([211,211,211]),2),new Color([255,255,255,0]));

			for(var g in allMappedPolygonGraphics){
				if(allMappedPolygonGraphics[g].symbol === undefined){
					allMappedPolygonGraphics[g].setSymbol(grayOutInactiveSymbol);
				} else{
					allMappedPolygonGraphics[g].setSymbol(grayOutInactiveSymbol);
				}
			}*/

			//BK: Gray out all layers
			this.grayOutTfrShapes(true);

			var $this = this,
				menu = $('#tfr-menu').data('kendoMenu'),
				mode = $this.get('tfrMode'),
				saveMenu = $('#saveMenu'),
				mod = $this.get('tfrModified');

			menu.close();
			//Check if there is unsaved data in the new/open tfr
			if ( mod ) {
				//check new / edit
				options = {
					title: 'Open TFR',
					message: 'The current TFR has unsaved changes. <br>Would you like like to save them?',
					buttons: ['YES', 'NO']
				};

				nisis.ui.displayConfirmation(options)
				.then(function(action) {
					if (action && action === options.buttons[0]) {
						$this.saveTfr();
						$this.createNewTfr();
					}

				}, function() {
					$this.set('tfrMsg', 'An error occured while opening confirmation dialog.');
				});

			} else {
				$this.createNewTfr();
			}
		},
		createNewTfr: function() {
			var $this = this,
				saveMenu = $('#saveMenu'),
				tempName = new Date();

			saveMenu.hide();

			//get ride of the open grid
			if ( $('.open-tfr').length ) {
				$('#tfr-content').remove('.open-tfr');
			}
			//should not be able to add new withou finishing the current one
			if ( $('.new-tfr').length ) {
				return;
			}
			//reset the data
			$this.resetTfrData();

			$this.set('tfrMsg', 'Initialazing a new TFR ...');
			$this.set('tfrType', 0);

			var displayName = tempName.getMonth() < 10 ? 0 + "" + (tempName.getMonth()+1) : tempName.getMonth() + 1;
			displayName += "" + tempName.getDate() < 10? 0 + "" + tempName.getDate() : tempName.getDate();
			displayName += "" + tempName.getFullYear() + "" + tempName.getHours() + "" + tempName.getMinutes();
			displayName += "" + tempName.getSeconds() < 10 ? 0 + "" + tempName.getSeconds() : tempName.getSeconds();

			displayName = appUtils.formatDateToDTG(tempName, true);

			displayName = 'TFR-' + displayName;

			$this.set('tfrName', displayName);
			$this.set('tfrTitle', $this.get('tfrName') + " - 00.0");
			$this.set('tfrMode','create');
			$this.set('notam_id', null);
			$this.set('custom_text', 0);
			$this.set('areasdata', null);

			var newTfr = $("<div/>", {
				"class": "new-tfr clr"
			});

			$('#tfr-content').append(newTfr);

			var sect = $("<div/>", {
					"class": "m-t clr"
				}),
				header = $("<p/>", {
					"class": "txt-b",
					"text": "Select the type of TFR you want to create"
				});

			newTfr.append(header);

			var typeSel = $("<input/>", {
					'id': 'tfr-type-sel',
					'class': 'w-300',
					"data-role": "dropdownlist",
					"data-option-label": "Select TFR Type",
					"data-text-field": "TYPE",
					"data-value-field": "ID",
					"data-bind": "source:tfrTypes,value:tfrTypeId"
				}),
				desc = $("<div/>", {
					'class': 'tfr-type-desc m-t pad clr',
					'data-bind': 'html:tfrDesc'
				}),
				tpDiv = sect.clone();

			tpDiv.append(typeSel);
			newTfr.append(tpDiv);

			var	nmLbl = $("<p class='txt-b'>TFR Name</p>"),
				nmField = $("<input/>", {
					"type": 'text',
					"class": 'k-textbox w-300',
					"placeholder": 'TFR Name',
					"data-value-update": "keyup",
					"data-bind": "value:tfrName,events:{keyup:validateCreateTfrFields}"
				}),
				cpOpt = $("<input/>", {
					"type": 'checkbox',
					"id": 'copy-tfr',
					"data-bind": "checked:tfrCopied,events:{change:validateCreateTfrFields}"
				}),
				cpLbl = $("<label for='copy-tfr'> Create as copy of following:</label>"),
				tfrSel = $("<input/>", {
					"id": 'tfr-source-sel',
					"class": 'w-300',
					"data-role": "dropdownlist",
					"data-option-label": "Select source",
					"data-auto-bind": true,
					"data-filter": "contains",
					"data-text-field": "NAME",
					"data-value-field": "ID",
					"data-bind": "source:tfrSources,value:tfrSource,enabled:tfrSrcEnabled"
				}),
				createBtn = $("<button/>", {
					"text": 'Create',
					"class": "w-100",
					"data-role": "button",
					"data-sprite-css-class": "k-icon k-add",
					"data-bind": "enabled:createTfrReady,click:loadNewTfr"
				}),
				ccelBtn = $("<button/>", {
					"text": 'Cancel',
					"class": "w-100 m-l",
					"data-role": "button",
					"data-sprite-css-class": "k-icon k-cancel",
					"data-bind": "click:cancelNewTfr"
				});

			var nmDiv = sect.clone();
			nmDiv.append(nmField);
			newTfr.append(desc, nmLbl, nmDiv);

			var cDiv = $("<p class='txt-b'></p>");
			cDiv.append(cpOpt, cpLbl);
			newTfr.append(cDiv);

			var lDiv = sect.clone();
			lDiv.append(tfrSel);
			newTfr.append(lDiv);

			var btnDiv = sect.clone();
			btnDiv.append(createBtn, ccelBtn);
			newTfr.append(btnDiv);

			//init & bind
			kendo.init(newTfr);
			kendo.bind(newTfr, $this);

			//read data sources
			typeSel.data('kendoDropDownList').bind('change', function(e) {
				//console.log('selecting TFR Type: ', e, e.sender.dataItem());
				$this.updateTfrType(e);
				$this.validateCreateTfrFields(e);
			});
			//check for valid id
			tfrSel.data('kendoDropDownList').bind('change', function(e) {
				//console.log('TFR source: ', e, e.sender.dataItem());
				$this.getTfrTypeFromSource(e);
				$this.validateCreateTfrFields(e);
			});

			var test = $this.get('notam_id');
			//test;
			//console.log("test notamID:", test);
		},
		validateCreateTfrFields: function(e) {
			//console.log('TFR Create Fields Validation:', e);
			var $this = this,
				errors = [];

			var typ = $this.get('tfrTypeId');

			if (typ == 0 || typ == '') {
				errors.push('Missing TFR Type');
			}

			var nm = $this.get('tfrName');
			nm = $.trim(nm);

			if (nm == "") {
				errors.push('Missing TFR Name');
				nm = 'TFR';
			}

			var cp = $this.get('tfrCopied');
			//console.log('Copy:', nm);
			if (cp) {
				var tfr = $this.get('tfrSource');
				$this.set('tfrSrcEnabled', true);

				if (tfr == 0 || tfr == '') {
					errors.push('Missing source TFR name');
				}

			} else {
				$this.set('tfrSrcEnabled', false);
				$this.set('tfrSource', '');
			}

			var title = nm + ' - ';
			var t = "00.0";

			if ( typ !== 0 && e.sender ) {
				var ds = e.sender.dataSource,
					dti = e.sender.dataItem(),
					desc = dti['DESCRIPTION'];

				if ( ds && dti && desc) {
					$this.set('tfrDesc', desc);
				}
			}

			var l = errors.length;

			if ( l > 0 ) {
				var msg = errors.join(", ");
				$this.set('tfrMsg', msg);
				$this.set('createTfrReady', false);

			} else {
				$this.set('tfrMsg', "");
				$this.set('createTfrReady', true);
			}
		},
		loadNewTfr: function(e){
			var $this = this,
				copy = $this.get('tfrCopied'),
				source = $this.get('tfrSource'),
				name = $this.get('tfrName'),
				saveMenu = $('#saveMenu'),
				tfrTypeId = $this.get('tfrTypeId');

			$this.set('tfrMsg', 'Building TFR input fields ...');
			
			//CC - Added this piece of code in order to set the initial values for the timezones dropdowns to UTC which is the value = 0 in order to prevent empty values for new TFRs
			var effectiveTimeZone = $("#effectiveTimeZone").data("kendoDropDownList");
			var expirationTimeZone = $("#expirationTimeZone").data("kendoDropDownList");
			effectiveTimeZone.value("0");
			expirationTimeZone.value("0");

			//$('#tfr-content').html('');
			$('.new-tfr').fadeOut(1000, function() {
				$(this).remove();
				$this.set('tfrMsg', '');
				$this.set('tfrShowForm', true);
				$this.set('tfrMode','create');

				//update form
				$this.updateTfrForm();
				//update radio buttons
				$this.updateAreaRadioBtns.call($this);

				//if copying, load tfr info from source
				if ( copy && source && source !== "" ) {
					//copy the tfr
					$.ajax({
						url: 'html/windows/tfr/tfr-copy.php',
						type: 'GET',
						dataType: 'json',
						data: {
	                            notamid: source,
	                            userid: Number(nisis.user.userid)
	                    },
						success: function(data){
						    //parse the result
						    try {
						    	//new notam id
								notamId = JSON.parse(data.result);
								//end then open it up with edit
								$this.editTfr({
									notamId: notamId
								});

							} catch (err) {
								console.log('Unable to parse results returned from tfr-copy-shapes.php. Error: ', err);
							}
						},
						error: function(){
							console.log(arguments);
							$this.set('tfrMsg', 'App could not copy exiting TFR.');
						}
					});

				} else {
					$this.trackTfrForm();
				}
			});

			//load new save menu
			saveMenu.show();
		},
		cancelNewTfr: function(e){
			var $this = this;

			// $('#tfr-content').html('');
			//$('#tfr-content').remove('.new-tfr');
			$this.resetTfrData();

			$this.set('tfrMsg', '');
			$this.set('tfrMode', '');
			$this.set('tfrTypeId', "");
			$this.set('tfrType', "");
			$this.set('tfrName', "");
			$this.set('tfrTitle', "");
			$this.set('tfrCopied', false);
			$this.set('tfrSource', '');

			$this.set('tfrModified', false);
		},
		resetTfrData: function() {
			var $this = this,
				tfrAreas = $('#tfr-areas').data('kendoTabStrip'),
				tabs = tfrAreas ? tfrAreas.tabGroup.children("li") : null,
				saveMenu = $('#saveMenu'),
				blankArea = $this.getAreasTemplate();
			//remove tfr list
			if ( $('.open-tfr').length ) {
				$('.open-tfr').remove();
			}
			//remove new tfr form
			if ( $('.new-tfr').length ) {
				$('.new-tfr').remove();
			}

			$this.set('tfrMsg', '');
			$this.set('createTfrReady', false);
			$this.set('tfrShowForm', false);
			$this.set('tfrModified', false);

			$this.set('tfrMode', '');
			$this.set('notam_id', '');
			$this.set('notam_usns_status', '');
			$this.set('tfrName', "");
			$this.set('tfrTypeId', "");
			$this.set('tfrType', '');
			$this.set('tfrDesc', '');
			$this.set('tfrTitle', '');

			$this.set('vor', '');

			//reset area data
			$.each($this.areas.toJSON(), function(i){
				if (i!=1){
					delete $this.areas[i];
				} else {
					$this.areas[i] = blankArea;
				}
			});

			$('#' + $this.get('activeArea') )
				.find('input.area-dtg')
				.each(function(){
					//console.log('date element', this);
					var dt = $(this).data('kendoDateTimePicker');

					dt.max(new Date('12/31/9999 11:59:59 PM'));
					dt.min(new Date('01/01/1970 12:00:00 AM'));
				});


			//reset artcc and time zones
			$this.resetArtcc();
			$this.resetTimeZones();
			//get rid of extra locations
			$this.cleanLocations();
			//reset tfr extra data ==> form element below areas
			$this.resetTfrExtra();

			//reset notam text
			$this.set('tfrNotamText', '');
			//reset airports list
			$this.resetArpList();

			//activate 'Quick Entry' tab
			var tfrForm = $('#tfr-form').data('kendoTabStrip');
			if (tfrForm){
				tfrForm.select(tfrForm.tabGroup.children("li:first"));
			}

			//get ride of extra tabs
			if (tfrAreas && tabs.length > 1) {
				tfrAreas.select(tfrAreas.tabGroup.children("li:first"));

				$.each(tabs, function(i, tab) {
					if (i > 0) {
						tfrAreas.remove($(tab));
					}
				});
			}
			tabs[0].querySelector('span.k-link').textContent = "Area A";
			$('#tfr-areas-1').find('input.area-name')[0].value = 'Area A';
			//return area data
			$this.resetTfrAreas();
		},
		loadTfrData: function(data) {
			var $this = this;
			$this.set('tfrMsg', 'Loading tfr data ...');
		},
		//areas
		tfrShapes: function () {
			return new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "api/tfr/"
                        ,type: 'GET'
                        ,dataType: 'json'
                    }
                    ,parameterMap: function(data,type) {
                        //console.log('APT Filter: ', data, type);
                        var filter = "";
                        if (data && data.filter) {
                            if (data.filter.filters && data.filter.filters.length) {
                                filter = data.filter.filters[0].value;
                            }
                        }

                        return {
                            'action': 'notam-areas',
                            'filter': filter
                        };
                    }
                },
                serverFiltering: true,
                error: function(e){
                    //console.log('TFR Shapes Error:', e);
                    nisis.ui.displayMessage(e.status, 'error');
                }
            });
		},
		tfrAreas2: [{name: 'Select Area XXX', value:0}, {name: 'Area A', value:1}],
		tfrAreas: [{name: 'Area A', value:1}],
		updateAreaName: function(e) {
			var $this = this,
				nm = e.target.value,
				areaId= $(e.currentTarget).closest('.k-state-active').attr('id'),
				tab = areaId.split('-').pop(),
				tabLabel = tab - 1;

				//sets input label
				$this.areas[tab].areaName = $.trim(nm);
				//sets tab label
				$('#tfr-areas').find('li:eq(' +  tabLabel + ')').find('.k-link').text($.trim(nm));

			// console.log($('#tfr-areas').find('li:eq(' +  tabLabel + ')').find('.k-link').text());
			// console.log('updateAreaName: Area name: ', $this.areas[tab]);
		},
		enableInstsReuse: function() {
			var tfrAreas = $('#tfr-areas').data("kendoTabStrip");

			if ( tfrAreas.tabGroup.children("li").length > 1 ) {
				this.set('useInstEnabled', true);
			} else {
				this.set('useInstEnabled', false);
				//force a check action on the add instruction radio button
				$('input.add-inst.ops-inst').prop('checked', true);
				$('input.add-inst.ops-inst').trigger('change');
			}
		},
		addBlankArea: function(e) {
			console.log("Adding blank area:", e);
			$('#tfr-content').spin(true);
			var $this = this,
			tfrAreas = $('#tfr-areas').data("kendoTabStrip"),
			blankArea = {
				text: 'Area ',
				contentUrl: './html/windows/tfr/tfr-areas.php'
			},
			len = tfrAreas.tabGroup.children("li").length,
			next = len + 1;

			blankArea.text += appUtils.getCharAtPos(next);

			var areas = $this.get('tfrAreas');

			if(areas) {
				var a = areas.toJSON(),
					b = a.concat([{name: blankArea.text, value:len}]);

				$this.set('tfrAreas', b);
			}
			//add a new tab at the end
			var newTab = tfrAreas.insertAfter(blankArea, tfrAreas.tabGroup.children("li:last"));

            tfrAreas.unbind("contentLoad", self.onAreaTabTemplateLoadComplete);
			tfrAreas.bind("contentLoad", self.onBlankAreaTabTemplateLoadComplete);

			//activate blank area
			var $lastTab = tfrAreas.select(tfrAreas.tabGroup.children("li:last"));

            $('#tfr-content').spin(false);

			self.applyReorderable();
		},
		addNewArea: function(openareas, updateShapes) {
			//BK: 01/04/2016 ==> This adds a new tab and/or populates its data
			//console.log('Add new areas:', openareas, updateShapes);
			var areaName,
				areasdata = null;

			if ( openareas && !openareas.sender) {
				areasdata = openareas;
			} else {
				return;
			}

			var $this = this,
				tfrAreas = $('#tfr-areas').data("kendoTabStrip");

            // Kofi: Had to unbind first because subsequent area tabs (after first tab)
            // were loading multiple times and including the date areas multiple times.
            // Jira: NISIS-1930
            tfrAreas.unbind("contentLoad", self.onAreaTabTemplateLoadComplete);
			//BK: ??
            tfrAreas.bind("contentLoad", self.onAreaTabTemplateLoadComplete);
			//remember areasdata
			$this.set('areasdata', areasdata);

			$.each(areasdata, function(i, area) {
				//first tab will always be in the form
				if (i === 0) {
					//do what the onComplete does but for the first tab
					var content = $('div#tfr-areas-1');
					//initialize kendo widget and bind viewmodel
					kendo.init(content);
					kendo.bind(content, self);

					//update the first element, the html template for it is already there
					//???self.updateActiveFristArea(e); this['area'] = newArea 1.
					$this.set('activeArea', 'tfr-areas-1');
					self.updateAreaElementsForTabArea(areasdata, 0);
				}
				else {//not the first tab
					var newArea = {
							text: 'Area ',
							contentUrl: './html/windows/tfr/tfr-areas.php'
						},
						len = tfrAreas.tabGroup.children("li").length;

					newArea.text += appUtils.getCharAtPos(len + 1);

					if (area.name) {
						//newArea.text = area.name;
					}
					var areas = $this.get('tfrAreas');
					if(areas) {
						var a = areas.toJSON(),
							b = a.concat([{name: newArea.text, value:len}]);

						$this.set('tfrAreas', b);
					}
					//set it up to add a new tab at the end by loading html template (described in trf-areas.php), after that we need to stuff it with the areasdata
					var newTab = tfrAreas.insertAfter(newArea, tfrAreas.tabGroup.children("li:last"));
				}
			});

			//now preload the tabs
			//preload
            $.each($('#tfr-areas .k-item'), function(i, tab) {
                //if (i!=0) {
                    // console.log("preloading all tabs exept the first one which aleady has been preloaded");
                    $('#tfr-areas').data("kendoTabStrip").reload(tab);
                //}
            });
		},
		onBlankAreaTabTemplateLoadComplete: function(e) {
			//ajax call to tfr-areas.php is complete. The html described in tfr-areas.php is loaded into DOM by kendo tab strip
			//get a reference to a new tab
			var content = $('div#' + e.contentElement.id);
			//initialize kendo widget and bind viewmodel
			kendo.init(content);
			kendo.bind(content, self);

			var tabNdx = Number(e.contentElement.id.replace("tfr-areas-","")); //"tfr-areas-N"

			$('#tfr-content').spin(true);
			self.updateActiveArea(e);
			//update name
			var name = "Area " +appUtils.getCharAtPos(tabNdx);
			content.find('input.area-name')[0].value = name;
		 	self.areas[tabNdx].areaName = name;

			//update name attribute of radio buttons
		 	self.updateAreaRadioBtns.call(self);

		    //update Shapes
			tfrShapesGrid.createGrid([], content.find('.shapesGrid') );
			self.updateAreaShape({},[]);

			$('#tfr-content').spin(false);
		},
		onAreaTabTemplateLoadComplete: function(e) {
			//ajax call to tfr-areas.php is complete. The html described in tfr-areas.php is loaded into DOM by kendo tab strip
			//get a reference to a new tab
			var content = $('div#' + e.contentElement.id);
			//initialize kendo widget and bind viewmodel
			kendo.init(content);
			kendo.bind(content, self);

			var areasdata = self.get('areasdata');
			var tabNdx = Number(e.contentElement.id.replace("tfr-areas-","")); //"tfr-areas-N"

			$('#tfr-content').spin(true);
			self.updateActiveArea(e);
			self.updateAreaElementsForTabArea(areasdata, tabNdx-1);
			$('#tfr-content').spin(false);
			self.assertAreaNames();
			var areasCount = self.get('areasdata').length;

			if(areasCount == tabNdx){
				self.set('activeArea', 'tfr-areas-1');
			}

		},
		updateAreaElementsForTabArea: function(areasdata, tabIdx) {
			var $this = this;

			if (!areasdata[tabIdx]) return;

			var name = areasdata[tabIdx].name,
				options = areasdata[tabIdx].options;

			if (!options) return;

            //dont need to select the last one
			// if ( i > 1) {
			// 	$('#tfr-areas').data("kendoTabStrip")
			// 	.select(tfrAreas.tabGroup.children("li:nth-child(" + options.tab + ")"));
			// }

			//new this bc activating a tab is new
			//console.log("updateAreaElements....", options.shapes );
			//initialize kendo widget and bind viewmodel
			if ( options.tab != 1) {
				kendo.init($('div#tfr-areas-'+ options.tab));
				kendo.bind($('div#tfr-areas-'+ options.tab), $this);
			}
			//update name attribute of radio buttons
			$this.updateAreaRadioBtns.call($this);

			//select the current tab. BK ==> this is a dublicate. Is there a valid reason?
			/*$('#tfr-areas').data("kendoTabStrip")
			.select(tfrAreas.tabGroup.children("li:nth-child(" + options.tab + ")"));*/
			//get a ref of the tab content
			var tab = $("#tfr-areas-" + options.tab);

            //console.log("tab.find('input.area-name')[0]", tab.find('input.area-name')[0]);
			//set area name
			if (tab.find('input.area-name').length > 0) {
				tab.find('input.area-name')[0].value = name;

				// CC - NISIS-1973: After setting the value in the area-name field, the tab name is being renamed
				var tabIndex = Number(options.tab) - 1;
				$('#tfr-areas').find('li:eq(' +  tabIndex + ')').find('.k-link').text($.trim(name));
			}
			//console.log("$this.areas[options.tab] is", $this.areas[options.tab]);
			$this.areas[options.tab].areaName = name;

			//console.log("options.shapes.....", options.shapes );
			tfrShapesGrid.createGrid(options.shapes, tab.find('.shapesGrid'));

			//create kendo grid for shapes
			$this.updateAreaShape({}, options.shapes);

			//set default drop down instruction selection that was saved to database
			//BK: 01/04/2016 ==> This has been removed from areas and should be done only once.
			tab.find(".area-insts")
			.each(function(){
				if ( $(this).length && $(this).data("kendoDropDownList") && options.instruction ) {
					// console.log("area instructions this: ", $(this).data("kendoDropDownList"));
					$(this).data("kendoDropDownList").value(options.instruction);
					$this.areas[options.tab].instruction = options.instruction;
				}
			});

			//add the instruction saved in the database to the web interface/javascript object
			//BK: 01/04/2016 ==> This has been removed from areas and should be done only once.
			if (options.instructions) {
				$.each(options.instructions, function(i, d) {
					var instText = d['INSTRUCTION_TEXT'],
						instId = options.instruction;

					if(!$this.areas[options.tab].instructionText){
						$this.areas[options.tab].instructionText = [];
					}
					$this.areas[options.tab].instructionText[i] = d['INSTRUCTION_TEXT'];
					var div = $("<div/>", {
						"class": "area-inst marg pad b-sq clr",
						"text": instText.replace(/<(?:.|\n)*?>/gm, ''),
						"on": {
							"click": $this.selectAreaInst.bind($this),
							"dblclick": $this.updateAreaInst.bind($this)
						}
					});
					tab.find('.area-insts-list').append(div);
				});
			}

			//update flight auth
			tab.find(".area-allowAuthCheck")
			.each(function() {
				if ( $(this).length && options.flightAuth ) {
					$this.areas[options.tab].allowFlightAuth = options.flightAuth;
					$(this)[0].checked = options.flightAuth;
					// $(this).attr("checked", options.flightAuth);
				}
			});
			//update area definition ==> name or component
			tab.find(".area-definition")
			.each(function() {
				if ( $(this).length && $(this).data("kendoDropDownList") ) {
					$(this).data("kendoDropDownList").value(options.definition);
					$this.areas[options.tab].frdOption = options.definition;
				}
			});
			//update effective date/times
			tab.find(".eff-dtz")
			.each(function(){
				if ( $(this).val() === options.areaOption ) {
					//$(this).attr("checked", options.areaOption);
					$(this)[0].checked = true;
					$this.areas[options.tab].areaDTOption = options.areaOption;
				}
			});
			//update effective date/time schedule
			tab.find(".eff-dtg")
			.each(function(){
				if ( $(this).val() === options.areaSchedule ) {
					// $(this).attr("checked", options.areaSchedule);
					$(this)[0].checked = true;
					$this.areas[options.tab].areaDTSchOption = options.areaSchedule;
				}
			});

			if (options.areaSchedule == "schedule") {
				//set the area schedule option
				$this.areas[options.tab]['areaDTSchOption'] = "schedule";

				//disable non-schedule inputs
				tab.find("input.area-dtg[data-schedule='non-schedule']")
				.each(function() {
					$(this).data('kendoDateTimePicker')
					.enable(false);
				});
				//enable schedule inputs
				tab.find("input.area-dtg[data-schedule='schedule']")
				.each(function() {
					$(this).data('kendoDateTimePicker')
					.enable(true);
				});

				//set selected dates
				var dayGroup = tab.find(".day-checks");

				dayGroup.find("input:checkbox.sch-day").each(function(){
					var day = $(this)[0].defaultValue;

					$(this)[0].disabled = false;
					$(this)[0].checked = options[day] == 1 ? true : false;
					if(day != ""){
						$this.areas[options.tab]["areaDTSchedule"][day]=$(this)[0].checked;
					}
				});
				//if all checkboxes are enabled, check the "Daily" selection
				dayGroup.find("input:checkbox.sch-day.sch-daily").each(function(){
					if (options['monday'] && options['tuesday'] && options['wednesday']
						&& options['thursday'] && options['friday'] && options['saturday']
						&& options['sunday']){
						$(this)[0].checked = true;
					}
				});
				//add dates
				if (options.dates) {

					$.each(options.dates, function(i, d) {

						var idx = i+1,
							eff = tab.find("input.area-dtg.area-eff-dtg[data-schedule='schedule']:nth-child(" + idx +")"),
							exp = tab.find("input.area-dtg.area-exp-dtg[data-schedule='schedule']:nth-child(" + idx +")");

						if ( eff.length && exp.length ) {

							var effW = $(eff[0]).data('kendoDateTimePicker'),
								effD = null, inUTC, offset, effTz, expTz;
								inUTC = $this.areas[d['AREA_ID']].areaDTOption == 'utc' ? true : false;
								effTz = $this.get('effTimeZoneOffset');
								expTz = $this.get('expTimeZoneOffset');
								offset = 60 * 60 * 1000;

							//console.log("idx", idx, "utc?", $this.areas[idx].areaDTOption, 'd', d);

							//if value returned from database
							if(d['EFFECTIVE_DATE_TIME']){
								//if value indicated to be in localtime, apply transformation
								effD = inUTC? parseInt(d['EFFECTIVE_DATE_TIME']): parseInt(d['EFFECTIVE_DATE_TIME']) + (effTz * offset);
							}

							effW.value(new Date(parseInt(effD)));
							$this.areas[d.AREA_ID]['areaDateTimes']['schedule']['dt'+idx].eff = parseInt(d['EFFECTIVE_DATE_TIME']);

							var expW = $(exp[0]).data('kendoDateTimePicker'), expD;

							if(d['EXPIRATION_DATE_TIME']){
								expD = inUTC ? parseInt(d['EXPIRATION_DATE_TIME']) : parseInt(d['EXPIRATION_DATE_TIME']) + (expTz * offset);
							}

							expW.value(new Date(parseInt(expD)));
							$this.areas[d.AREA_ID]['areaDateTimes']['schedule']['dt'+idx].exp = parseInt(d['EXPIRATION_DATE_TIME']);

						} else {
							$this.addDateTimeSet({}, 'schedule', d);
						}
					});
				}

			} else {
				//set the area schedule option
				$this.areas[options.tab]['areaDTSchOption'] = "non-schedule";
				//enable non-schedule inputs
				tab.find("input.area-dtg[data-schedule='non-schedule']")
				.each(function() {
					$(this).data('kendoDateTimePicker')
					.enable(true);
				});
				//disable schedule inputs
				tab.find("input.area-dtg[data-schedule='schedule']")
				.each(function() {
					$(this).data('kendoDateTimePicker')
					.enable(false);
				});

				//add dates
				if (options.dates) {

					$.each(options.dates, function(i, d) {

						var idx = i+1,
							eff = tab.find("input.area-dtg.area-eff-dtg[data-schedule='non-schedule']:nth-child(" + idx +")"),
							exp = tab.find("input.area-dtg.area-exp-dtg[data-schedule='non-schedule']:nth-child(" + idx +")");

						if ( eff.length && exp.length ) {
							var effW = $(eff[0]).data('kendoDateTimePicker'),
								effD = null, inUTC, offset, effTz, expTz;
								inUTC = $this.areas[d['AREA_ID']].areaDTOption == 'utc' ? true : false;
								effTz = $this.get('effTimeZoneOffset');
								expTz = $this.get('expTimeZoneOffset');
								offset = 60 * 60 * 1000;

							//if value returned from database
							if(d['EFFECTIVE_DATE_TIME']){
								//if value indicated to be in localtime, apply transformation
								effD = inUTC? parseInt(d['EFFECTIVE_DATE_TIME']): parseInt(d['EFFECTIVE_DATE_TIME']) + (effTz * offset);
							}

							effW.value(new Date(parseInt(effD)));
							$this.areas[d.AREA_ID]['areaDateTimes']['non-schedule']['dt'+idx].eff = parseInt(d['EFFECTIVE_DATE_TIME']);

							var expW = $(exp[0]).data('kendoDateTimePicker'), expD;

							if(d['EXPIRATION_DATE_TIME']){
								expD = inUTC ? parseInt(d['EXPIRATION_DATE_TIME']) : parseInt(d['EXPIRATION_DATE_TIME']) + (expTz * offset);
							}
							expW.value(new Date(parseInt(expD)));
							$this.areas[d.AREA_ID]['areaDateTimes']['non-schedule']['dt'+idx].exp = parseInt(d['EXPIRATION_DATE_TIME']);
						} else {
							$this.addDateTimeSet({}, 'non-schedule', d);
						}
					});
				}
			}

			///////////////////////open saved tfr - change shape color/opacity/////////////////////////

			//array of current tfr polygon object Ids
			//BK: 01/12/2016 ==> This is being taken care by grayOutTfrShapes function
			/*var currentShapesArray = tfrShapesGrid.getCurrentTFRShapeIds();
			//polygon features on map
			var mapview = nisis.ui.mapview;
			var targetFeature = {
				"nisis": {
					text: mapview.layers.text_features,
					point: mapview.layers.Point_Features,
					polyline: mapview.layers.Line_Features,
					polygon: mapview.layers.Polygon_Features
				},
				"tfr": {
					point: mapview.layers.tfr_point_features,
					polyline: mapview.layers.tfr_line_features,
					polygon: mapview.layers.tfr_polygon_features
				}
			};

			var polygonGraphics = targetFeature.tfr.polygon.graphics;
			var inactiveGrpahics = targetFeature.tfr.polygon;
			var inactiveSymbol = new SimpleFillSymbol(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([211,211,211]),2));
			var resetInactiveSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([211,211,211]),2),new Color([255,255,255,0]));
			//BK: 01/04/2016 ==> Changing the shape styles this way will last only 60s till the next refresh.
			for(var a in polygonGraphics){
				polygonGraphics[a].setSymbol(resetInactiveSymbol);
			}

			//check to see if shapes exists in add shape box
			if(currentShapesArray.length > 0){
				//loop through both arrays and set the colors if match is found
				for(var i in polygonGraphics){
					for(var x in currentShapesArray){
						if(currentShapesArray[x] == polygonGraphics[i].attributes.OBJECTID){

							var tfrShapeString = polygonGraphics[i].attributes.TFR_SHAPE;

							switch(tfrShapeString) {
								case "none":
								var newSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
							    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
							    new Color([0,0,0,1]), 2),new Color([0,255,255,.5]));
								polygonGraphics[i].setSymbol(newSymbol);
								break;
								case "add":
								var newSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
							    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
							    new Color([0,255,127,1]), 2),new Color([0,255,255,.5]));
								polygonGraphics[i].setSymbol(newSymbol);
								break;
								case "subtract":
								var newSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
							    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
							    new Color([255,0,0,1]), 2),new Color([0,255,255,.5]));
								polygonGraphics[i].setSymbol(newSymbol);
								break;
								case "base":
								var newSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
							    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
							    new Color([0,0,255,1]), 2),new Color([0,255,255,.5]));
								polygonGraphics[i].setSymbol(newSymbol);
								break;
							}
						}
						//gray out symbols if symbol node is 'undefined'
						if(polygonGraphics[i].symbol === undefined){
							polygonGraphics[i].setSymbol(inactiveSymbol);
						}
					}
				}

			} else {
				console.log("no shapes in Add Shape Area.");
			}*/

			//irena - fitsum not sure
			self.applyReorderable();
		},
		assertAreaNames: function(){
			var areas = $('#tfr-areas'),
				areasCount = areas.children("div").length;
			//console.log("areas count:", areasCount);
			for (var i = 0; i < areasCount; i++){
				// fieldName = $(areas.find('.area-name')[i]).context.value;
				fieldName = $(areas.find('.area-name')[i])[0].value;
				tabName = $(areas.find('li')[i]).find('.k-link')[0].textContent;

				if(tabName !== fieldName){
					$(areas.find('li')[i]).find('.k-link')[0].textContent = fieldName;
				}
			}
		},
        applyReorderable: function () {
			tfrAreas = $('#tfr-areas').data("kendoTabStrip");
            var reorderable = tfrAreas.tabGroup.data('kendoReorderable');
            if ( reorderable ) {
            	reorderable.destroy();
            }
            tfrAreas.tabGroup.kendoReorderable({
                // group: 'tabs',
                filter:'.k-item',
                hint: function(element) {
                    return element.clone().wrap('<ul class="k-tabstrip-items k-reset"/>').parent().css({opacity: 0.8});
                    },
                change: $.proxy(this.onReorderableChange, this)
            });
        },
        onReorderableChange: function ( event ) {
        	console.log("Reording tabs: ", event);
			var $this = this,
				tfrAreas = $('#tfr-areas').data("kendoTabStrip");

            if ( event.newIndex < event.oldIndex ) {
                tfrAreas.tabGroup.children('.k-item:eq('+event.newIndex+')').before( tfrAreas.tabGroup.children('.k-item:eq('+event.oldIndex+')') );
                tfrAreas.element.children('.k-content:eq('+event.newIndex+')').before(tfrAreas.element.children('.k-content:eq('+event.oldIndex+')') );
            }
            else {
                tfrAreas.tabGroup.children('.k-item:eq('+event.newIndex+')').after( tfrAreas.tabGroup.children('.k-item:eq('+event.oldIndex+')') );
                tfrAreas.element.children('.k-content:eq('+event.newIndex+')').after(tfrAreas.element.children('.k-content:eq('+event.oldIndex+')') );
            }
            tfrAreas._updateClasses();
            $this.reorderAreas();
            $this.updateTfrNotamText();
        },
        reorderAreas: function(){
        	//console.log("Reorder tabs ..............");
        	var $this = this,
        		areaNames = [],
        		reorderedObj = {},
        		area = tfrAreas.tabGroup.children("li");

				activeArea = $('#tfr-areas').find('li.k-state-active').attr('aria-controls');
				$this.set('activeArea', activeArea);

			for(var i = 0; i < area.length; i++ ){
				//for(var j in $this.areas){
				for(var j in $this.get("areas").toJSON()){ //BK:01/13/2016 ==> Kendo always add extra key to and array/object

					if($this.areas[j].areaName == area[i].textContent){
						reorderedObj[i + 1] = $this.areas[j];
					}
				}
			}

			//BK: 01/13/2016 ==> Why are you looking if you are keeping the same keys?
			/*for(var k = 1; k < area.length + 1; k++) {
				$this.areas[k] = reorderedObj[k];
			}*/
			//BK: 01/13/2016 ==> I have noticed that kendo prefer get / set functions.
			$this.set("areas", reorderedObj);
			//BK: 01/13/2016 ==> What this this function do? Found it. Nice trick. It's only being used on reorder tho!!!
            $this.assertAreaNames();
			//console.log("reorderedObj:", reorderedObj);
			//console.log("$this.areas AFTER:", $this.areas);
        },
		removeCrtArea: function(e) {
			var $this = this,
				tfrAreas = $('#tfr-areas').data("kendoTabStrip"),
				len = tfrAreas.tabGroup.children("li").length,
				activeArea = tfrAreas.select(),
				tab = $this.activeArea.split("-").pop();

			console.log("Removing current area: ", tab);

			if (!len || len < 2) {

				var msg = "A TFR will require at least one area.";
				nisis.ui.displayMessage(msg, 'error');
				return;
			}

			if (activeArea) {
				//remove area shapes first
				var content = $("#tfr-areas-" + tab);
				//remove all shapes in the area
				var remShapes = tfrShapesGrid.removeAreaShapes(content);

				if (!remShapes) {
					console.log("App fails to initiate shapes removal");
					return;
				}

				//make sure the shapes are removed successfully before removing the content
				remShapes.then(function(d) {
					var idx = tfrAreas.tabGroup.children("li").index(activeArea),
						newIdx = 1;

					$.each($this.areas.toJSON(), function(key, value) {
						delete $this.areas[key];

						if(key !== tab){
							$this.areas[newIdx] = value;
							newIdx++;
						}
					});

					tfrAreas.remove(activeArea);

					var l = tfrAreas.tabGroup.children("li").length;

					if (l > idx + 1) {
						tfrAreas.select(idx);
					} else {
						tfrAreas.select(idx-1);
					}

					var areas = $this.get('tfrAreas'),
						a = areas.toJSON();
					//remove the area from instruction
					a.splice(idx, 1);
					//reset the areas
					$this.set('tfrAreas', a);

					//instructions can only be reused when there is more than 1 area
					$this.enableInstsReuse();

				}, function(error) {
					console.log("Area shapes were not removed");
				});
			}
			//console.log("removing an area");
		},
		moveCrtAreaLeft: function(e) {
			//console.log('Move tab to the left');
		},
		moveCrtAreaRight: function(e) {
			//console.log('Move tab to the right');
		},
		//areas data
		resetTfrAreas: function() {
			this.resetFirstAreaShape();
			this.resetAreaInsts();
			this.resetOutputOptions();
			this.resetAreaDTOption();
			this.cleanDateTimeSets();
		},
		updateAreaRadioBtns: function() {
			//console.log('Updating area radio buttons ...');

			var $this = this,
				id = $this.get('activeArea'),
				tab = null,
				content = null;

			if ( id ) {
				tab = id.split('-').pop();
				content = $('#'+id);
				//console.log('Area content: ', id, tab, content);
				if (!content) {
					//console.log('No area content: ', id);
					return;
				}
				//console.log("Area content ops: ", content.find("input:radio.ops-inst"));
				//updates radio buttons
				content.find("input:radio.ops-inst")
				.each(function(){
					var nm = $(this).attr('name'),
						name = nm.replace('x', tab);

					$(this).attr('name', name);
				});

				content.find('input:radio.eff-dtz')
				.each(function(){
					var nm = $(this).attr('name'),
						name = nm.replace('x', tab);

					$(this).attr('name', name);
				});

				content.find('input:radio.eff-dtg')
				.each(function(){
					var nm = $(this).attr('name'),
						name = nm.replace('x', tab);

					$(this).attr('name', name);
				});

			} else {
				console.log('Updating area radio buttons failed:', id);
			}
		},
		updateAreaShapesGrid: function(shapes, tab) {
			console.log('Updating area shapes grid ...');
			var $this = this,
				//id = $this.get('activeArea'),
				//tab = null,
				content = null;

			//tab = id.split('-').pop();
			//irena
			content = $('#'+id);
			//console.log('Area content: ', id, tab, content);
			if (!content) {
				console.log('No area content: ', id);
				return;
			}
			//console.log("Area content ops: ", content.find("input:radio.ops-inst"));
			//updates radio buttons
			$wrapper = content.find(".shapesGrid");
			tfrShapesGrid.createGrid([], $wrapper);
		},
		updateActiveArea: function(e) {
			var $this = this,
				content = null,
				id = 'tfr-areas-1'
				tab = '1';

			if(e) {
				tab = e.item;
				content = e.contentElement;
				id = $(content).attr('id');
				tab = id.split('-').pop();

				if (!$this.areas[tab]) {
					var newArea = $this.getAreasTemplate();
					$this.areas[tab] = newArea;
					//console.log('updateActiveArea" Active area tab: ', tab);
				}
			}
			//console.log('Active area tab: ', e);
			$this.set('activeArea', id);
		},
		activeArea: 'tfr-areas-1', //this will be reset on tab activate
		allowFlights: false,
		getAreasTemplate: function() {
			return {
				shape: "",
				instruction: "",
				areaName: "Area A",
				includeFRD: true,
				frdOption: "component",
				includeLocTime: true,
				allowFlightAuth: false,
				areaDTOption: 'utc',
				areaDTSchOption: 'non-schedule',
				areaDTSchedule: {
					monday: false,
					tuesday: false,
					wednesday: false,
					thursday: false,
					friday: false,
					saturday: false,
					sunday: false
				},
				areaDateTimes: {
					"schedule": {
						"dt1": {

						}
					},
					"non-schedule": {
						"dt1": {

						}
					}
				}
			};
		},
		areas: {
			"1": {
				shape: "",
				instruction: "",
				//text output
				areaName: "Area A",
				includeFRD: true,
				frdOption: "component",
				includeLocTime: true,
				allowFlightAuth: false,
				//date time
				areaDTOption: 'utc',
				areaDTSchOption: 'non-schedule',
				areaDTSchedule: {
					monday: false,
					tuesday: false,
					wednesday: false,
					thursday: false,
					friday: false,
					saturday: false,
					sunday: false
				},
				areaDateTimes: {
					"schedule": {
						"dt1": {

						}
					},
					"non-schedule": {
						"dt1": {

						}
					}
				}
			}
		},
		//store all FRDs as json
		frds: {},
		//set shapes ids
		updateAreaShape: function(e, a) {
			/*
			 * BK: 01/04/2016: a ==> array of shape objects:
			 * {NAME: "POLYGON -ABC", OBJECTID: "1111", VOR: "HPW-VOR "}
			 */
			//console.log("updateAreaShape: ", e, a);
			var $this = this,
				shapes = a ? a : e.sender.value(),
				//MARKER - 1
				shapeIds = [],
				//shapeData = e.sender.dataItem(),
				area = this.get('activeArea').split('-').pop();

			//trying to find where the shapes are actually stored and passed to the notamText.
			$.each(shapes, function(i, shape) {
				//console.log("New Shape:", shape);
				shapeIds.push(parseInt(shape.OBJECTID));
			});
			//BK: 01/04/2016 ==> Another way to recreate an array
			/*shapeIds = $.map(shapes, function(shape, i) {
				return shape.OBJECTID;
			});*/

			//set the shapes
			if ( this['areas'][area] ) {
				//update grid data for area
				this['areas'][area]['shape'] = shapes;

			} else {
				var msg = "Selected area error: area #" + area + ' is not set';
				nisis.ui.displayMessage(msg, 'error');
			}

			//how to get the vertices - MARKER 2 - not sure if this actually is doing anything
			/*
			 * BK: 01/03/2016 ==> This was supposed to work with the old multi-select widget.
			 * Now that the multi-select has been replaced by a grid, you have to find a work around.
			 */
			var v = this.getAreaVertices(shapeIds, this['areas'][area]['includeFRD'], area);

			if (!v) {
				//explicitly nullify vertices information in case a user removes shape information.
				$this['areas'][area]['vertices'] = [];
				return;
			}

			v.then(function(vertices) {
				//console.log("Area Vertices:", vertices);

				$this['areas'][area]['vertices'] = vertices;

			}, function(error) {
				//console.log('Getting vertices error: ', error);
				$this['areas'][area]['vertices'] = null;
			});
		},
		updateCurrentTFRShapeAttribute: function(shapeId, attributeName, attributeValue) {
			//make sure we are in tfr builder mode
			if (appConfig.app != "tfr") {
				return;
			}
			//console.log('updateCurrentTFRShapeAttribute attribute: ', shapeId, attributeName, attributeValue);
			switch(attributeName) {
				case "NAME":
					this.updateCurrentTFRShapeName(shapeId, attributeValue);
					break;
				case "TFR_SHAPE":
					this.updateCurrentTFRShapeType(shapeId, attributeValue);
					break;
				case "FLOOR":
					this.updateCurrentTFRShapeSimpleAttribute(shapeId, 'floor', attributeValue);
					break;
				case "FL_TYPE":
					this.updateCurrentTFRShapeSimpleAttribute(shapeId, 'ftype', attributeValue);
					break;
			    case "CEILING":
					this.updateCurrentTFRShapeSimpleAttribute(shapeId, 'ceil', attributeValue);
					break;
				case "CEIL_TYPE":
					this.updateCurrentTFRShapeSimpleAttribute(shapeId, 'ctype', attributeValue);
					break;
				default:
					console.log('updateCurrentTFRShapeAttribute unsupported attribute: ', attributeName);
			}
		},
		updateCurrentTFRShapeSimpleAttribute: function(shapeId, attributeName, attributeValue) {
			//they can change attribute of the shape that is not in their active area
			var $this = this,
				areas = this.get('areas').toJSON();

			for (var tab in areas) {
				var area = areas[tab],
					shapes = area['shape'],
					vertices = area['vertices'];

				if (!shapes || (shapes && !shapes.length)) return;

				$.each(shapes, function(i, shape) {
					if (shape.OBJECTID == shapeId) {
			        	var vertice = area['vertices'][i];
						//update it
						vertice[attributeName] = attributeValue;
					}
				});
			}
		},
		updateCurrentTFRShapeName: function(shapeId, name) {
			//they can change name of the shape that is not in their active area
			//console.log("TFR Name change: ", shapeId, name);
		    var $this = this, grids = $('#tfr-div').find('.shapesGrid'),
		    	dataSource,
		    	datasourcedata;
		    	// tab = this.get("activeArea").split("-").pop(),
		    	// area = this.get("areas")[tab];
		    	//find area based on the grid


		    if ( grids.length > 0 ) {
		    	//BK: Why not get the grid of the active area directly? - they could change the name of the shape that is not their active area
		    	$.each(grids, function(i, grid) {
		    		//find area based on the grid
		    		var areaId = $($(grid).closest('.tfr-area').parent()[0]).attr('id');
		    		var tabNdx = Number(areaId.replace("tfr-areas-","")); //"tfr-areas-N"
		    		area =  $this.get("areas")[tabNdx];

					dataSource = $(grid).find('.k-grid').data("kendoGrid").dataSource;
		            datasourcedata = dataSource.data();

                    //go through dataSource and change name
		            for (var ndx = 0; ndx < datasourcedata.length; ndx++) {
		            	if (datasourcedata[ndx].OBJECTID == shapeId) {
		            		datasourcedata[ndx].NAME = name;
		            		dataSource.data(datasourcedata);
		            		//BK: update areas data as well
		            		if (area.shape && area.shape.length) {
			            		$.each(area.shape, function(i, s) {
			            			if ( s.OBJECTID == shapeId) {
			            				s.NAME = name;
			            			}
			            		});

			            	} else {
			            		area.shape.push({
			            			OBJECTID: shapeId,
			            			NAME: name
			            		});
			            	}
			            	//console.log("TFR Name Updated: ", area.shape, shapeId);
		            		break;
		            	}
					}

				});
		    }
		},
		updateCurrentTFRShapeType: function(shapeId, updatedTfrShapeType) {
			//console.log("Update Current TFR Shape Type:", shapeId, type);
			//they can change type of the shape that is not in their active area
			var $this = this,
				areas = this.get('areas').toJSON();

			for (var tab in areas) {
				var area = areas[tab],
					shapes = area['shape'],
					vertices = area['vertices'];

				if (!shapes || (shapes && !shapes.length)) return;

				$.each(shapes, function(i, shape) {
					if (shape.OBJECTID == shapeId) {
			        	//console.log("updateCurrentTFRShapeType() => if (shape.OBJECTID==shapeId) -->  vertices:", vertices);
			        	//get the matched vertice
						var vertice = area['vertices'][i];

			        	//find the old value for vertices and update it
			        	if (vertice.base) {
							vertice[updatedTfrShapeType] =  vertice.base;
						} else if (vertice.add) {
							vertice[updatedTfrShapeType] =  vertice.add;
						} else if (vertice.subtract) {
							vertice[updatedTfrShapeType] =  vertice.subtract;
						} else if (vertice.none) {
							vertice[updatedTfrShapeType] =  vertice.none;
						}
						else {
							//BK: 01/06/2016 ==> Sometime this rare case accurs
							//vertice[updatedTfrShapeType] = ??;
							console.log("There is no: BASE - ADD - SUBTRACT - NONE");
						}

						//delete all others
						if (updatedTfrShapeType == 'none' ) {
							//delete all others
							delete vertice.base;
							delete vertice.add;
							delete vertice.subtract;
						}
						else if (updatedTfrShapeType == 'base' ) {
							//delete all others
							delete vertice.none;
							delete vertice.add;
							delete vertice.subtract;
						} else if (updatedTfrShapeType == 'subtract' ) {
							//delete all others
							delete vertice.none;
							delete vertice.add;
							delete vertice.base;
						} else if (updatedTfrShapeType == 'add' ) {
							//delete all others
							delete vertice.none;
							delete vertice.base;
							delete vertice.subtract;
						}
					}
				});
			}
		},
		updateCurrentTfrVor(shapeId, newVor) {
			var $this = this,
				areas = this.get('areas').toJSON();

			for (var tab in areas) {
				var area = areas[tab],
					shapes = area['shape'],
					vertices = area['vertices'];

				if (!shapes || (shapes && !shapes.length)) return;

				$.each(shapes, function(i, shape) {
					if (shape.OBJECTID && shape.OBJECTID == shapeId) {
						shape.VOR = newVor;
						//BK: store the vor in the vertices. Quick and dirty
						vertices[i]['vor'] = newVor;
					}
				});
			}
		},
		resetFirstAreaShape: function() {
			var $this = this,
				tab = $('#tfr-areas-1');

			if (!tab.length) return;

			//clear out shapes grid for the area tab
			//tfrShapesGrid.clearArea(tab);

			//set it as ActiveArea, so updateAreaShapes works
			$this.set('activeArea', 'tfr-areas-1');
			//update Shapes
			tfrShapesGrid.createGrid([], tab.find('.shapesGrid') );
			self.updateAreaShape({}, []);
		},
		setArtcc: function(bFeature) {
			var $this = this,
				geom = null,
				attr = null,
				center = null,
				lat = null,
				lon = null,
				layer = null;

			if ( !bFeature || !bFeature.geometry) return;

			geom = bFeature.geometry;
			attr = bFeature.attributes;
			//get the center of the artcc
			center = geom.getCentroid();

			if ( center ) {
				lat = center.y,
				lon = center.x
			}

			if (!lat || !lon) return;

			if ( $this.get('artccId') && $this.get('artccId') !== '' ) {
				//just center map to artcc
				//Fix to bug NISIS 1489, the customer does not want the map to zoom to the shape they selected from Area(A) tab - Shape(s) section
				//nisis.ui.mapview.map.centerAndZoom(center, 7);

				return;
			}

			layer = nisis.ui.mapview.map.getLayer('artcc_sa');

			if (!layer) return;

			var msg = "App could not retrieve the ARTCC information. Try to reset it manually.";

			nisis.ui.mapview.util.queryFeatures({
				layer: layer,
				returnGeom: true,
				geom: center
			})
			.then(function(result) {

				if ( result && result.features && result.features.length ) {
					var attr =  result.features[0].attributes,
						artcc = attr ? attr.ID : null;

					if ( artcc && (!$this.get('artccId') || $this.get('artccId') === '') ) {
						$this.set('artccId', artcc);

						$('select.tfr-artcc-id').data('kendoDropDownList')
						.trigger('change');

					}

				} else {
					nisis.ui.displayMessage(msg, 'error');
				}

			}, function(err) {
				nisis.log('ARTCC Error: ', err);
				nisis.ui.displayMessage(msg, 'error');
			});
		},
		getAreaVertices: function(shapeIds, frd, areaId) {
			//console.log("GetAreaVertices:", shapeIds, frd, areaId);
			var $this = this,
				layer = nisis.ui.mapview.map.getLayer('tfr_polygon_features');

			if (!layer || !shapeIds || (shapeIds && !shapeIds.length)) {
				return;
			}

			var def = new $.Deferred(),
				base = false,
				bFeat = null;

			nisis.ui.mapview.util.queryFeatures({
				layer: layer,
				returnGeom: true,
				objectIds: shapeIds
			})
			.then(function(result) {
				//console.log("Area shapes query result:", result);
				var feats = result ? result.features : [],
					shapes = [];
				//filter all tfr shapes
				$.each(feats, function(i, feat) {
					//console.log(feat);
					var vertices = {},
						attr = feat.attributes,
						gType = attr['GEOM_TYPE'],
						shapeType = attr['TFR_SHAPE'];
					//shape types ==> base, add, subtract, none (default value)

					vertices[shapeType] = [];
					//save decimal degrees
					vertices['dd'] = [];
					//set shape name
					vertices['shapeName'] = feat.attributes['NAME'];
					//set shape altitudes: floor/ceiling
					vertices['objectID'] = feat.attributes['OBJECTID'];
					vertices['inf'] = feat.attributes['IN_FLOOR'];
					vertices['floor'] = feat.attributes['FLOOR'];
					vertices['ftype'] = feat.attributes['FL_TYPE'];
					vertices['geomtype'] = feat.attributes['GEOM_TYPE'];

					vertices['inc'] = feat.attributes['IN_CEIL'];
					vertices['ceil'] = feat.attributes['CEILING'];
					vertices['ctype'] = feat.attributes['CEIL_TYPE'];

					var geom = feat.geometry,
						str = attr['VERTICES'];
					//track base shape in area 1
					if (shapeType == 'base' && areaId == 1) {
						base = true;
						bFeat = feat;
					}
					//process circles
					if ( gType && gType.toLowerCase() === 'circle' ) {

						if (str && str.toLowerCase().indexOf('lat') === 0) {

							var parts = str.split(', ');

							if (!parts || !parts.length) {
								return null;
							}

							if (parts.length === 3) {

								var lat = parts[0].split(':')[1],
								lon = parts[1].split(':')[1],
								rad = parts[2].replace(/[^0-9.]+/g, '');

								if (!lat || !$.isNumeric(lat)) {
									return null;
								}
								if (!lon || !$.isNumeric(lon)) {
									return null;
								}
								if (!rad || !$.isNumeric(rad)) {
									return null;
								}
								//using this to get the dms
								var pt = new LatLon(lat, lon),
									ptt = pt.toString('dms', 0);
								//reformat the outpu
								ptt = ptt.replace(/[^0-9NSEW,]+/g, '');
								ptt = ptt.replace(',', '');
								//concatenate the center and radius
								ptt += ", ";
								ptt += rad;
								//add to the main shapes
								vertices[shapeType].push(ptt);
								vertices['dd'].push([lon,lat]);
								shapes.push(vertices);

								return;

							} else {
								return null;
							}

						} else {
							return null;
						}
					}
					//process buffered polylines
					else if ( gType && (gType.toLowerCase() === 'buffered polyline' || gType.toLowerCase() === 'buffered polygon' )) {

						if ( !str || str.toLowerCase().indexOf('v1') !== 0) {
							return null;
						}

						var parts = str.split(', '),
							radius = 0;

						if (!parts || !parts.length || parts.length < 3) {
							return null;
						}

						$.each(parts, function(i, part) {
							var part = part.split(':');

							if ( part[0].toLowerCase().indexOf('v') == 0 ) {

								var ll = $.trim(part[1]).split("/"),
									lat = ll[0],
									lon = ll[1],
									pt = new LatLon(lat, lon),
									ptt = pt.toString('dms', 0);

								ptt = ptt.replace(/[^0-9NSEW,]+/g, '');
								ptt = ptt.replace(',', '');

								//store vertice
								vertices[shapeType].push(ptt);
								vertices['dd'].push([lon,lat]);

							} else if ( part[0].toLowerCase().indexOf('radius') == 0 ) {
								radius = $.trim(part[1]).split('nm')[0];
							}
						});

						vertices[shapeType].push(radius);
						shapes.push(vertices);
						return;
					}
					else if (gType && gType.toLowerCase() === 'arc' ) {
						if (!str || str.toLowerCase().indexOf('center') !== 0) {
							return null;
						}

						var parts = str.split(', '),
							center = parts[0],
							ptA = parts[1],
							ptB = parts[2],
							rad = parts[3],
							ptt = "";

						var ctr = center.split(':')[1],
							clat = $.trim(ctr.split('/')[0]),
							clon = $.trim(ctr.split('/')[1]),
							cpt = new LatLon(clat, clon),
							cptt = cpt.toString('dms', 0);

							cptt = cptt.replace(/[^0-9NSEW,]+/g, '');
							cptt = cptt.replace(',', '');

						vertices[shapeType].push(cptt);
						vertices['dd'].push([clon, clat]);

						var a = ptA.split(':')[1],
							alat = $.trim(a.split('/')[0]),
							alon = $.trim(a.split('/')[1]);
							apt = new LatLon(alat, alon),
							aptt = apt.toString('dms', 0);

							aptt = aptt.replace(/[^0-9NSEW,]+/g, '');
							aptt = aptt.replace(',', '');

						vertices[shapeType].push(aptt);
						vertices['dd'].push([alon, alat]);

						var b = ptB.split(':')[1],
							blat = $.trim(b.split('/')[0]),
							blon = $.trim(b.split('/')[1]);
							bpt = new LatLon(blat, blon),
							bptt = bpt.toString('dms', 0);

							bptt = bptt.replace(/[^0-9NSEW,]+/g, '');
							bptt = bptt.replace(',', '');

						vertices[shapeType].push(bptt);
						vertices['dd'].push([blon, blat]);

						var r = $.trim(rad.split(':')[1]);
						vertices[shapeType].push(r);
						shapes.push(vertices);

						return;
					}

					//this runs only when the geom type is not a circle
					$.each(geom.rings, function(j, ring) {
						//get ring's vertices
						$.each(ring, function(z, v) {
							var pt = new LatLon(v[1], v[0]);
							//get DMS of the vertice
							var ptt = pt.toString('dms', 0);
							//get rid of extra chars
							ptt = ptt.replace(/[^0-9NSEW,]+/g, '');
							ptt = ptt.replace(',', '/');

							//store vertice
							vertices[shapeType].push(ptt);
							vertices['dd'].push(v);
						});

						shapes.push(vertices);
					});
				});
				//check for base shape in area1
				if (base && bFeat) {
					//set artcc
					$this.setArtcc(bFeat);
				}
				// console.log("GetAreaVertices:", shapes);
				//return shapes
				def.resolve(shapes);

			}, function(error) {
				def.reject(error);

			}, function(note) {
				def.notify(note);
			});

			return def.promise();
		},
		getFRDfromVertice: function(pt, vertice, shapeVor) {
			//console.log("Get VOR Vertice:", pt, vertice, shapeVor);
			if ( !pt || !vertice || !shapeVor ) return;

			var $this = this,
				frds = $this.frds,
				vor = $this.get('vor'),
				vID = null,
				vItem = $this.get('vorItem'), //BK: 01/22/2016 ==> where is this??
				vData = null,
				vLat = null,
				vLon = null,
				vMag = null, //BK: 03/15/2016 ==> place holder for magnatic variation
				lat = pt[1],
				lon = pt[0],
				newFrd = null,
				msg = 'App could not retreive VOR details.';

			//generate FRD text
			var generateFRD = function(vorName) {				
				//check for valid lat/lon of VOR
				//console.log('generateFRD: vorName is ', vorName);
				if ( !vLat || !vLon ){return;}

				var vorsObj = $this.get("vorsObj"), //BK: 03/15/2015 ==> Where is this being set? It's always getting the first element and not the selected one. It's sometimes null.
                    magnetic_var = vorsObj && vorsObj['MAGNETIC_VAR_N'] ? parseInt(vorsObj['MAGNETIC_VAR_N']) : null, //BK: 03/15/2015 ==> No need to parse if you get MAG key.
                    ptA = new LatLon(vLat, vLon),
					ptB = new LatLon(lat, lon),
					brg = ptA.bearingTo(ptB) - vMag,
					dkm = ptA.distanceTo(ptB),
					//convert distance from km to nm
					dnm = nisis.ui.mapview.util.convertUnits(dkm, 'km', 'nm'),
					frdStr = "";
					vertice = vertice.replace("/", ""); //BK ==> Should not need this but adding it for safety.

				frdStr += vID;
				var b = parseInt(parseFloat(brg).toFixed(0));
				
				//use 0 instead of 360
				if (b == 360) {
					b = 0;
				}

				//normalize brearings
				if (b < 0) {
					b += 360;
				}
				else if (b > 360) {
					b -= 360;
				}
				else {
					b = parseInt(b);
				}
				
				//format bearing to 3digits
				if (b < 100) {
					if (b < 10) {
						frdStr += "00" + b;
					} else {
						frdStr += "0" + b;
					}

				} else {
					frdStr += b;
				}

				var d=0;
				if (dnm !== "unknown") {
				    d = dnm.toFixed(1);
				   
				    //format distance to 4digits
				    if (d < 100) {
				        if (d < 10) {
				            frdStr += "00" + d;

				        } else {
				            frdStr += "0" + d;
				        }

				    } else {
				        frdStr += d;
				    }

				    //special name if distance is 0.1
				    if (d<=0.1) {
				    	frdStr = vorName;
				    }

				    frds[vertice] = frdStr;

				} else {
				    frdStr = frdStr + "000000.0";
				}

				var notamText = $this.get('tfrNotamText');
				
				//if the distance is less then .1, no need for parenthesis, otherwise put parenthesis around FRD string
				if (d<=0.1) {
					notamText = notamText.replace("({"+vertice+"})", frdStr);
				}
				else {
					notamText = notamText.replace("({"+vertice+"})", "("+frdStr+")");
				}
				
				//BK: 01/22/2016 ==> This is replacing way too much text. Anything between the first "{" and the last "}".
				//notamText = notamText.replace(/{.*}/g, frdStr);

				$this.set('tfrNotamText', notamText);
				//console.log('notam after FRD set:', $this.get('tfrNotamText'));

				return frdStr;
			};

			if ( vor && vor !== "" ) {
					//query database for VOR details
					if(vor.indexOf(",") !== -1){
						vor = vor.split(",")[0];
					}
					
					//special name for combined VOR and TACR, like LAX-VOR, TACR in case distance is less then 0.1
					var vName = shapeVor;
					if (vName.indexOf("TACR")>0) {
						vName = "OR THE "+shapeVor.split("-")[0] + " VORTAC";
					} 
					else if (vName.indexOf("VOR")>0 && vName.indexOf("DME")>0 && vName.indexOf("TACR")==-1) {
						vName = "OR THE "+shapeVor.split("-")[0] + " VOR/DME";
					} 
					else if (vName.indexOf("DME")==-1 && vName.indexOf("TACR")==-1 && vName.indexOf("VOR")>0) {
						vName = "OR THE " +shapeVor.split("-")[0] + "VOR";
					} 
					//var vQuery = geomUtils.getVorByID(vor);
					var vQuery = geomUtils.getVorByID(shapeVor.split(",")[0]); //BK: 03/15/2015 ==> Use the function argument instead
					if ( vQuery ) {

						vQuery.then(
						function(data) {
							console.log("Get Vor by ID:", data);
							if (data && data.length) {
								vData = data[0];
								vID = vData['ANS'].split('-')[0];
								vLat = vData['LATITUDE'];
								vLon = vData['LONGITUDE'];
								vMag = vData['MAG'];
								
								//get FRD
								newFrd = generateFRD(vName);

								if (newFrd) {
									return newFrd; //BK: 01/22/2016 ==> This will not be returned by the parent function "generateFRD"
								}

							} else {
								// console.log("No VORs found:", vor, data);
								nisis.ui.displayMessage(msg, 'error');
							}
						},
						function(err) {
							// console.log("Error getting VORs:", vor, err);
							nisis.ui.displayMessage(msg, 'error');
						});

					} else {
						// console.log("Unable to start VORs query:", vor);
						nisis.ui.displayMessage(msg, 'error');
					}

				} else {
					// console.log("Invalid:", vor);
					nisis.ui.displayMessage(msg, 'error');
			}
			

			//var def = new $.Deferred();
			/*var query = geomUtils.getClosestVOR(lat, lon, 1);

			if (query) {
				query.then(
				function(data) {
					if (data && data.length) {

						var frd = data[0],
							ansId = frd['ANS_ID'],
							id = ansId.split('-')[0],
							lt = frd['LATITUDE'],
							lg = frd['LONGITUDE'],
							dist = frd['DISTANCE'];

						var ptA = new LatLon(lt, lg),
							ptB = new LatLon(lat, lon),
							brg = ptA.bearingTo(ptB),
							dkm = ptA.distanceTo(ptB),
							//convert distance from km to nm
							dnm = nisis.ui.mapview.util.convertUnits(dkm, 'km', 'nm');

							//.toFixed(0)
						var frdStr = "";

						frdStr += id;
						var b = brg.toFixed(0);
						//format bearing to 3digits
						if (b < 100) {
							if (b < 10) {
								frdStr += "00" + brg.toFixed(0);
							} else {
								frdStr += "0" + brg.toFixed(0);
							}
						} else {
							frdStr += brg.toFixed(0);
						}

						var d = dnm.toFixed(1);
						//format distance to 4digits
						if (d < 100) {
							if (d < 10) {
								frdStr += "00" + dnm.toFixed(1);
							} else {
								frdStr += "0" + dnm.toFixed(1);
							}
						} else {
							frdStr += dnm.toFixed(1);
						}

						//set frd string to $thid.frds;
						frds[vertice] = frdStr;
						var notamText = $this.get('tfrNotamText');
						notamText = notamText.replace("{"+vertice+"}", frdStr);
						$this.set('tfrNotamText', notamText);

					} else {
						frds[vertice] = null;
					}
				},
				function() {
					console.log('App could not get FRD from: ', pt);
					frds[vertice] = null;
				});

			} else {
				console.log('App could not get FRD from: ', pt);
				frds[vertice] = null;
			}*/
		},
		//area ops instructions.
		useInstEnabled: false,
		updateAreaInstructions: function(e) {
			var $this = this,
				typeId = e.sender.value(),
				idx = e.sender.select(),
				ds = e.sender.dataSource,
				item = e.sender.dataItem();

			var areaInsts = $this.getAreaInstructions.call($this, typeId);

			areaInsts.then(
			function(data){
				if (data && data.length) {
					$this.set('areaInstructions', data);
				} else {
					$this.set('areaInstructions', []);
				}
			},
			function(err){
				$this.set('areaInstructions', []);
				$this.set('tfrMsg', err);
			});
		},
		//areaInstructions: [],
		// THIS FUNCTION NEEDS TO BE CALLED getAreaInstructionTypes !!!!! (kofi and fitsum)
		getAreaInstructions: function(typeId) {
			var def = new $.Deferred();
			var $this = this,
				id = typeId ? typeId : $this.get('tfrTypeId');

			if (!id) {
				id = 0;
			}

			$.ajax({
        		url: "api/tfr/",
        		type: "GET",
        		dataType: 'json',
        		data: {action: 'instructions', filter: id},
        		success: function(data) {
        			if(data.length) {
        				def.resolve(data);
        			} else {
        				def.resolve([]);
        			}
        		},
        		error: function(req, status, err) {
        			def.reject(err);
        		}
        	});

        	return def.promise();
		},
		updateOpsInst: function(e) {
			var $this = this,
				el = $('#tfr-form'),
				wrap = $('#oprInst'),
				action = el.data('action');
				//areas = $('#tfr-areas').data('kendoTabStrip');

			if (action === 'use') {
				wrap.find('select.use-area-insts')
				.data('kendoDropDownList')
				.enable(true);

				wrap.find('select.area-insts')
				.data('kendoDropDownList')
				.enable(false);

				wrap.find('button.area-apply-insts')
				.data('kendoButton')
				.enable(false);

				wrap.find('button.area-inst')
				.data('kendoButton')
				.enable(false);

			} else if (action === 'add') {
				wrap.find('select.use-area-insts')
				.data('kendoDropDownList')
				.enable(false);

				wrap.find('select.area-insts')
				.data('kendoDropDownList')
				.enable(true);

				wrap.find('button.area-apply-insts')
				.data('kendoButton')
				.enable(true);

				wrap.find('button.area-inst-add')
				.data('kendoButton')
				.enable(true);
			}
		},
		updateCrtInstsBtns: function(e) {
			var //$this = this,
				areas = $('#tfr-form').data('kendoTabStrip'),
				//activeTab,
				//tabId = $this.get('activeArea'),
				tab = $('#oprInst');

			/*if ( !areas ) return;

			if (!tab || !tab.length) {
				activeTab = areas.select();
				tabId = $(activeTab[0]).attr('aria-controls');
				tab = $("#"+tabId);

				if (!tab.length) {
					return;
				}
			}*/

			//btns
			var add = tab.find('button.area-inst-add'),
				del = tab.find('button.area-inst-del'),
				up = tab.find('button.area-inst-up'),
				down = tab.find('button.area-inst-down'),
				insts = tab.find('div.area-insts-list'),
				len = insts.children("div.area-inst").length,
				sel = null;

			del = $(del[0]).data('kendoButton');
			up = $(up[0]).data('kendoButton');
			down = $(down[0]).data('kendoButton');

			if (!len) {
				del.enable(false);
				up.enable(false);
				down.enable(false);
				return;

			} else {
				sel = insts.children("div.area-inst.b-sel");
			}

			if (sel && sel.length) {
				del.enable(true);
				var idx = sel.index();
				if (idx === 0 && len === 1) {
					up.enable(false);
					down.enable(false);
				} else if (idx === 0 && len > 1) {
					up.enable(false);
					down.enable(true);
				} else if (idx > 0 && idx === len -1) {
					up.enable(true);
					down.enable(false);
				} else if (idx > 0 && idx < len -1) {
					up.enable(true);
					down.enable(true);
				}

			} else {
				del.enable(false);
				up.enable(false);
				down.enable(false);
			}
		},
		applyCopiedInsts: function(e){
			var //$this = this,
				//tabId = $this.get('activeArea'),
				selectedIndex = e.sender.selectedIndex,
				tab = $('#oprInst'),
				sel = tab.find('div.area-inst').remove(),
				textToCopy = $("#tfr-form-" + selectedIndex).find('div.area-insts-list').children();

			var instsList = tab.find('div.area-insts-list');

			$.each(textToCopy, function(i){
				$("<div/>", {
					"class": "area-inst noselect marg pad b-sq clr",
					"text": textToCopy[i].textContent,
					"on": {
						"click": $this.selectAreaInst.bind($this),
						"dblclick": $this.updateAreaInst.bind($this)
					}
				}).appendTo(instsList);
			});
			this.updateCrtInstsBtns();
		},
		loadOperatingInsts: function(){
			var $this = this;
			var data = $this.get('operatingInstructions');

			instsList = $('.area-insts-list');
			instsOpts = $('select.area-insts');
			tfr = $this.get('tfrTypeId');

			if (!data || !data.length) {
				$this.set("tfrMsg", "App could not get the Operating Instructions Options.");
				instsList.html("");
				return;
			}
			// var tab = $this.activeArea.split("-").pop();

			// $this.areas[tab].instruction = inst;
			// $this.areas[tab].instructionText = [];

			$.each(data, function(i) {
				// var txt = this['NAME'];
				var txt = this['INSTRUCTION_TEXT'],
					id = this['INSTRUCTION_ID'],
					txt = txt ? txt : "";

				var div = $("<div/>", {
					"class": "area-inst noselect marg pad b-sq c-pt clr",
					"instructionID": id,
					"text": txt.replace(/<(?:.|\n)*?>/gm, ''),
					"on": {
						"click": $this.selectAreaInst.bind($this),
						"dblclick": $this.updateAreaInst.bind($this)
					}
				});
				instsList.append(div);
				// $this.areas[tab].instructionText[i] = txt;
			});
			// REMOVE
			//update buttons
			//$this.updateCrtInstsBtns.call($this);
		},
		applyAreaInsts: function(e, tab, instId, instText) {

			var $this = this, btn, instsList, instsOpts, instsDD, instSel, tfr, inst;

			// console.log("e", e, "tab", tab, "instId", instId, "instText", instText);
			// if (instText){
			// 	var thisArea = $("#tfr-areas-" + tab);
			// 	instsList = thisArea.find('.area-insts-list');
			// 	instsOpts = thisArea.find('select.area-insts');
			// 	instsDD = instsOpts.data('kendoDropDownList');
			// 	console.log("thisArea:", thisArea, instsList, instsOpts, instsDD);
			// 	instSel = instsDD.dataItem();
			// 	inst = instId;
			// } else {
				btn = e.sender.element;
				instsList = btn.parent().parent().find('.area-insts-list');
				instsOpts = btn.parent().find('select.area-insts');
				instsDD = instsOpts.data('kendoDropDownList');
				instSel = instsDD.dataItem();
				tfr = $this.get('tfrTypeId');
				inst = instSel['ID'];
			// }

			var options = $this.getInstsOptions(tfr, inst);

			if (!options) {
				return;
			}

			options.then(
			function(data){
				if (!data || !data.length) {
					$this.set("tfrMsg", "App could not get the Operating Instructions Options.");
					return;
				}
				var tab = $this.activeArea.split("-").pop();

				$this.areas[tab].instruction = inst;
				$this.areas[tab].instructionText = [];

				$.each(data, function(i) {
					var txt = this['NAME'];
					txt = txt ? txt : "";

					var div = $("<div/>", {
						"class": "area-inst noselect marg pad b-sq c-pt clr",
						"text": txt.replace(/<(?:.|\n)*?>/gm, ''),
						"on": {
							"click": $this.selectAreaInst.bind($this),
							"dblclick": $this.updateAreaInst.bind($this)
						}
					});
					instsList.append(div);
					$this.areas[tab].instructionText[i] = txt;
				});

				//update buttons
				$this.updateCrtInstsBtns.call($this);
			},
			function(err){
				instsList.html("");
				//update buttons
				$this.updateCrtInstsBtns();
			});
		},
		getInstsOptions: function(tfr, inst) {
			var $this = this,
				tfrId = tfr ? tfr : $this.get('tfrTypeId'),
				instId = inst;
				// shapeInst = null,
				// count = 1,

			if (!tfrId || !instId) {
				return;
			}

			var def = new $.Deferred();

			$.ajax({
        		url: "api/tfr/",
        		type: "GET",
        		dataType: 'json',
        		// data: {action: 'ints-options', filter: {tfr:tfrId, inst:instId, shape:shapeInst}},
        		data: {action: 'ints-options', filter: {tfr:tfrId, inst:instId}},
        		success: function(data) {
        			if(data.length) {
        				def.resolve(data);
        			} else {
        				console.log('Instructions options success (empty): ', data);
        				def.resolve([]);
        			}
        		},
        		error: function(req, status, err) {
        			console.log('Instructions options error: ', arguments);
        			def.reject(err);
        		}
        	});

        	return def.promise();
		},
		addAreaInst: function(e){
			var $this = this,
				tab = $this.get('activeArea');

			/*if (!tab) {
				var areas = $('#tfr-form').data('kendoTabStrip'),
					activeTab = areas.select(),
					tabId = $(activeTab[0]).attr('aria-controls'),
					tab = tabId;
			}*/

			tab = $("#tfr-form");

			var instsList = tab.find('div.area-insts-list');

			$("<div/>", {
				"class": "area-inst noselect marg pad b-sq clr",
				"text": " ",
				"on": {
					"click": $this.selectAreaInst.bind($this),
					"dblclick": $this.updateAreaInst.bind($this)
				}
			}).appendTo(instsList);
		},
		selectAreaInst: function(evt) {
			var $this = this,
				div = evt.currentTarget;

			if ($(div).hasClass('b-sel')) {
				$(div).removeClass('b-sel');

			} else {
				$(div).siblings().removeClass('b-sel');
				$(div).addClass('b-sel');
			}
			//update buttons
			$this.updateCrtInstsBtns();
		},
		updateAreaInst: function(evt) {
			var $this = this,
				//tab = $this.get('activeArea').split('-').pop(),
				tab = $('#tfr-form'),
				target = evt.currentTarget,
				txt = $(target).text(),
				options = {
					title: 'Update instruction',
					desc: 'Update default instruction and click "UPDATE" to apply.',
					width: 300,
					height: 100,
					message: txt,
					buttons: ['update','cancel']
			    };

			nisis.ui.displayPrompt(options).then(
			function(resp) {
				if (resp.action === options.buttons[0].toLowerCase() && resp.input !== "") {
					$(target).text("");
					$(target).text(resp.input);
					// console.log('target', target, 'evt', evt);
					// $this.areas[tab]
				} else if (resp.action === 'update' && resp.input == "") {
					$this.set('tfrMsg', 'Instruction values can not be empty. Consider removing it completly.');
				} else {
					console.log('Instruction: ', "Unknown");
				}
			},
			function(err){
				$this.set('tfrMsg', 'App could not display dialog box. Please Contact NDP/Apps Support.');
			});
		},
		delAreaInst: function(e){
			var tab = $('#tfr-form'),
				sel = tab.find('div.area-inst.b-sel').remove();

			this.updateCrtInstsBtns();
		},
		upAreaInst: function(e){
			var tab = $('#tfr-form'),
				sel = tab.find('div.area-inst.b-sel'),
				prev = sel.prev();

			sel.insertBefore(prev);
			this.updateCrtInstsBtns();
		},
		downAreaInst: function(e){
			var tab = $('#tfr-form'),
				sel = tab.find('div.area-inst.b-sel'),
				next = sel.next();

			sel.insertAfter(next);
			this.updateCrtInstsBtns();
		},
		setAreaInstruction: function() {

		},
		resetAreaInsts: function() {
			var $this = this,
				tab = $("#tfr-form");
				//activeArea = $this.get('activeArea');

			tab.find('input.add-inst').prop('checked', true);
			tab.find('input.add-inst').trigger('change');

			tab.find('div.area-insts-list').html("");
			$this.updateCrtInstsBtns();
		},
        //text output options
        loadFRDInclusion: function(){
            $('.area-frdCheck').prop("checked", this.get('includeFRD'));
        },
        setFRDInclusion: function(e) {
            var $this = this,
                checked = $(e.target).is(':checked');
            $this.set('includeFRD', checked);
        },
        //JS - Global variable for component definition
        componentDefinition: "component",
        setFRDOption: function(e) {
            var $this = this,
                dropdown = $('#area-definition').data('kendoDropDownList'),
                value = dropdown.value();

            $this.set('componentDefinition', value);
        },
        loadLocTimeInclusion: function(){
            $('.area-locTimeCheck').prop('checked', this.get('includeLocTime'));
        },
        setLocTimeInclusion: function(e) {
            var checked = $(e.target).is(':checked'),
                $this = this;
            $this.set('includeLocTime', checked);
        },
		/*setAllowFlightAuth: function(e) {
			var tab = this.get('activeArea').split('-').pop(),
				checked = $(e.target).is(':checked');

			this['areas'][tab]['allowFlightAuth'] = checked;
		},*/
		resetOutputOptions: function() {
			var $this = this,
				tab = $('#tfr-form');
				//activeArea = $this.get('activeArea');


			tab.find('input.area-frdCheck').prop('checked', true);
			tab.find('input.area-locTimeCheck').prop('checked', true);
			//tab.find('input.area-allowAuthCheck').prop('checked', false);
		},
		//area date/time options
		setAreaDTOption: function(e){
			var $this = this,
				tab = this.get('activeArea').split('-').pop(),
				dtOpt = $(e.target).val(),
				effTz = $this.get('effTimeZoneOffset'),
				expTz = $this.get('expTimeZoneOffset'),
				timeZoneOffset = 0,
				msHr = 60 * 60 * 1000;

			$this['areas'][tab]['areaDTOption'] = dtOpt;
			//convert existing values if any
			$('#' + $this.get('activeArea') )
			.find('input.area-dtg')
			.each(function(){
				var date,
					newDate,
					dt = $(this).data('kendoDateTimePicker');

				if (dt) {
					date = dt.value();
				}

				//CC - Check if the the element (Date/Time Picker) is an effective or an expiration one
				if ($(this).hasClass('area-eff-dtg')){
					timeZoneOffset = effTz;
				}else{//CC - This code will be executed when the date/time picker is an Expiration Date/Time 
					timeZoneOffset = expTz;
				}

				if ( date && dtOpt === 'utc' ) {
					newDate = Date.parse(date);
					newDate += timeZoneOffset < 0 ? (Math.abs(timeZoneOffset) * msHr) : (-1 * timeZoneOffset * msHr);
					newDate = new Date(newDate);

				} else if ( date && dtOpt === 'local' ) {
					newDate = Date.parse(date);
					newDate += timeZoneOffset < 0 ? (timeZoneOffset * msHr) : (timeZoneOffset * msHr);
					newDate = new Date(newDate);
				}

				if (newDate) {
					//set the min/max date values if necessary
					var min = dt.min(), max = dt.max();
					//console.log("e", e, "dt", dt, "min", min, "max", max);
					if (newDate > max) {
						dt.max(newDate);
					}
					if (newDate < min){
						dt.min(newDate);
					}
					dt.value(newDate);
				}

			});
		},
		resetAreaDTOption: function() {
			var $this = this,
				tab = this.get('activeArea').split('-').pop();

			//CC - Resetting the Date/Time Time Zone to 'local' per NISIS-2152
			$('#tfr-areas-' + tab).find("input.eff-dtz[value='local']").prop('checked', true);
			// $('#tfr-areas-' + tab).find("input.eff-dtz[value='utc']").prop('checked', true);

			// console.log("tab:", tab);
			// console.log("this:", $this);

			//$this['areas'][tab]['areaDTOption'] = 'utc';
			// $this.areas[tab].areaDTOption = 'utc';
			//CC - Resetting the Date/Time Time Zone to 'local' per NISIS-2152
			$this.areas[tab].areaDTOption = 'local';
		},
		convertToLocalTime: function(date) {

		},
		convertToUTCTime: function(date) {

		},
		//area dates
		addDateTimeSet: function(e, areaSch, dts) {
			var $this = this,
				// activeTab = $('#tfr-areas').find('li.k-state-active').attr('aria-controls'),
				activeTab = $this.get('activeArea'),
				tab = activeTab.split('-').pop(),
				schOption = $this['areas'][tab]['areaDTSchOption'],
				areaDT = 'areaDateTimes',
				sch = e.target ? $(e.target).data('schedule') : areaSch,
				row = e.currentTarget ? $(e.currentTarget).parent() : null,
				div = row ? row.parent() : null;


			//CC - This code was below, now it's here in order to prevent the bug in NISIS-1966
			var currTab = $('#' + activeTab),
				dates = null;

			if ( sch === 'schedule' ) {
				dates = currTab.find('div.tfr-dates.sch');
			} else {
				dates = currTab.find('div.tfr-dates.non-sch');
			}

			//CC - NISIS - 1966: Updating the row to be the last child of the div containing the data/time sets
			row = dates.find('div.area-dtg-input:last-child');
			row = $(row[0]);

			if (schOption && schOption !== sch) {
				return;
			}

			var dt = $this['areas'][tab][areaDT][sch];

			dt = dt.toJSON ? dt.toJSON() : dt;

			if ( !dt ) return;

			var keys = Object.keys(dt);

			keys.sort();
			var dtgroup = keys.pop().substr(2);

			if (!$.isNumeric(dtgroup)) {
				return;
			}

			dtgroup = parseInt(dtgroup) + 1;
			dtgroup = "dt" + dtgroup;

			$this['areas'][tab][areaDT][sch][dtgroup] = {};

			var newRow = $("<div/>", {
				"class": 'area-dtg-input marg clr of-h',
				"data-dtgroup": dtgroup
			});

			var effDtg = $("<input/>", {
				"class": "area-dtg area-eff-dtg",
				"data-schedule": schOption,
				"data-dtg": "eff",
				"data-role": "datetimepicker",
				"data-format": "yyyy/MM/dd HH:mm",
			    "data-time-format": "HH:mm",
				"data-bind": "events:{change:setAreaDates}"
			});
			effDtg.appendTo(newRow);

			var expDtg = $("<input/>", {
				"class": "area-dtg area-exp-dtg m-l",
				"data-schedule": schOption,
				"data-dtg": "exp",
				"data-role": "datetimepicker",
				"data-format": "yyyy/MM/dd HH:mm",
			    "data-time-format": "HH:mm",
				"data-bind": "events:{change:setAreaDates}"
			});
			expDtg.appendTo(newRow);

			$("<i/>", {
				"class": "pointer marg m-l10 fa fa-plus",
				"data-schedule": schOption,
				"title": "Add",
				"data-bind": "click:addDateTimeSet"
			}).appendTo(newRow);

			$("<i/>", {
				"class": "pointer marg fa fa-times",
				"data-schedule": schOption,
				"title": "Remove",
				"data-bind": "click:removeDateTimeSet"
			}).appendTo(newRow);

			//on for existing tfr
			// if (!row){
				// var currTab = $('#' + activeTab),
				// dates = null;

				// if ( sch === 'schedule' ) {
				// 	dates = currTab.find('div.tfr-dates.sch');
				// } else {
				// 	dates = currTab.find('div.tfr-dates.non-sch');
				// }

				// row = dates.find('div.area-dtg-input:last-child');
				// row = $(row[0]);
			// }
			//insert the new dtg row
			newRow.insertAfter(row);
			//initialize kendo widgets
			kendo.init(newRow);
			kendo.bind(newRow, $this);
			//set dates if nay
			if ( dts ) {
				// console.log("dts", dts, "tab", tab);

				var effD = null, expD = null, inUTC, offset, effTz, expTz;
				inUTC = $this.areas[tab].areaDTOption == 'utc' ? true : false;
				effTz = $this.get('effTimeZoneOffset');
				expTz = $this.get('expTimeZoneOffset');
				offset = 60 * 60 * 1000;


				//if value indicated to be in localtime, apply transformation
				effD = inUTC? parseInt(dts.EFFECTIVE_DATE_TIME): parseInt(dts.EFFECTIVE_DATE_TIME) + (effTz * offset);
				expD = inUTC? parseInt(dts.EXPIRATION_DATE_TIME): parseInt(dts.EXPIRATION_DATE_TIME) + (expTz * offset);

				$this['areas'][tab][areaDT][sch][dtgroup]['eff'] = parseInt(dts.EFFECTIVE_DATE_TIME);
				effDtg.data('kendoDateTimePicker')
					.value(new Date(parseInt(effD)));

				$this['areas'][tab][areaDT][sch][dtgroup]['exp'] = parseInt(dts.EXPIRATION_DATE_TIME);
				expDtg.data('kendoDateTimePicker')
				.value(new Date(parseInt(expD)));
			}
		},
		removeDateTimeSet: function(e) {
			var $this = this,
				tab = tab = this.get('activeArea').split('-').pop(),
				schOption = $this['areas'][tab]['areaDTSchOption'],
				sch = $(e.target).data('schedule'),
				row = $(e.currentTarget).parent(),
				dt = row.data('dtgroup'),
				div = row.parent(),
				len = div.find('input.area-eff-dtg').length;

			if (sch !== schOption) {
				return;
			}

			if(len > 1) {
				row.remove();
				var newIdx = 1;

				$.each($this.areas[tab].areaDateTimes[schOption], function(key, value){
					delete $this.areas[tab].areaDateTimes[schOption][key];
					if(key !== dt){
						$this.areas[tab].areaDateTimes[schOption]['dt'+newIdx] = value;
						newIdx++;
					}
				});

				// if($this['areas'][tab]['areaDateTimes'][sch][dt]) {
				// 	delete $this['areas'][tab]['areaDateTimes'][sch][dt];
				// 	// console.log($this['areas'][tab]['areaDateTimes'][sch][dt]);
				// }
			}
		},
		cleanDateTimeSets: function() {
			var $this = this,
				area = $this.get('activeArea'),
				tab = $('#' + area);

			tab.find('div.tfr-dates.non-sch > .area-dtg-input')
			.each(function(i, dt) {
				if ( i > 0 ) {
					$(dt).remove();

				} else {
					$(dt).find('input.area-dtg')
					.each(function() {
						$(this).data('kendoDateTimePicker')
						.value("");
					});
				}
			});

			tab.find('div.tfr-dates.sch > .area-dtg-input')
			.each(function(i, dt) {
				if ( i > 0 ) {
					$(dt).remove();

				} else {
					$(dt).find('input.area-dtg')
					.each(function() {
						$(this).data('kendoDateTimePicker')
						.value("");
					});
				}
			});
			//reset scheduled dates
			tab.find("input.sch-day")
			.prop('checked', false);
			//set non-sch as default
			tab.find("input.eff-dtg[value='non-schedule']")
			.prop('checked', true);

			tab.find("input.eff-dtg[value='non-schedule']")
			.trigger('change');
		},
		updateEffDTG: function(e){
			var $this = this,
				tab = this.get('activeArea').split('-').pop(),
				sch = 'areas.' + tab + '.areaDTSchOption',
				option = e.target.dataset.option,
				target = $(e.currentTarget),
				isChoice = target.is(':checked'),
				div = target.parent().parent().parent();

			$this['areas'][tab]['areaDTSchOption'] = option;

			if (option === 'schedule' && isChoice) {
				var isDaily = $(div.find('input.sch-daily')[0]).is(':checked');

				div.find('input.sch-day')
				.each(function() {
					if( $(this).hasClass('sch-daily') ) {
						$(this).removeAttr('disabled');
						return;
					}

					if (isDaily) {
						$(this).attr('disabled', true);
					} else {
						$(this).removeAttr('disabled');
					}
				});

				div.find('input.area-dtg')
				.each(function() {
					var dtg = $(this).data('kendoDateTimePicker');
					if(dtg) {
						dtg.enable(true);
					}
				});

				div.siblings()
				.each(function() {
					$(this).find('input.area-dtg')
					.each(function() {
						var dtg = $(this).data('kendoDateTimePicker');
						if(dtg) {
							dtg.enable(false);
						}
					});
				});

			} else if (option === 'non-schedule' && isChoice) {
				div.find('input.area-dtg')
				.each(function() {
					var dtg = $(this).data('kendoDateTimePicker');
					if(dtg) {
						dtg.enable(true);
					}
				});


				div.siblings()
				.each(function() {
					$(this).find('input.sch-day')
					.each(function() {
						$(this).attr('disabled', true);
					});

					$(this).find('input.area-dtg')
					.each(function() {
						var dtg = $(this).data('kendoDateTimePicker');
						if(dtg) {
							dtg.enable(false);
						}
					});
				});
			}
		},
		setDailySchedule: function(e) {
			var $this = this,
				tab = this.get('activeArea').split('-').pop(),
				target = $(e.currentTarget),
				div = target.parent().parent().parent().parent(),
				isDaily = target.is(':checked'),
				days = 'areaDTSchedule',
				sch = {
					monday: true,
					tuesday: true,
					wednesday: true,
					thursday: true,
					friday: true,
					saturday: true,
					sunday: true
				};

			div.find('.sch-day')
			.each(function(){
				var input = $(this);
				if ( input.hasClass('sch-daily') ) {
					return;
				}

				if (isDaily) {
					input.attr("checked", true);
					input.attr("disabled", true);

				} else {
					var d = input.val(),
						day = $this['areas'][tab][days][d];
					input.attr("checked", day);
					input.removeAttr("disabled");
				}
			});

			if (isDaily) {
				$this['areas'][tab][days] = sch;
			}
		},
		setScheduledDays: function(e) {
			var $this = this,
				tab = this.get('activeArea').split('-').pop(),
				day = $(e.target).val(),
				checked = $(e.target).is(':checked'),
				days = 'areaDTSchedule';

			if ($this['areas'][tab][days] && day) {
				$this['areas'][tab][days][day] = checked;
			}
		},
		setAreaDates: function(e) {
			var $this = this,
				tab = $this.get('activeArea').split('-').pop(),
				dates = 'areaDateTimes',
				schOption = $this['areas'][tab]['areaDTSchOption'],
				dtgEl = e.sender.element,
				dtgroup = e.sender.wrapper.parent().data('dtgroup'),
				dtg = dtgEl.data('dtg'),
				dt = e.sender.value(),
				dtNum = dt ? Date.parse(dt) : null;

			var datesVal = $this['areas'][tab][dates],
				schOpt = datesVal[schOption],
				dtValues = schOpt[dtgroup];

			var effOffset = $this.get('effTimeZoneOffset'),
			expOffset = $this.get('expTimeZoneOffset'),
			areaDTOption = $this.areas[tab].areaDTOption,
			msHr = 60 * 60 * 1000;

			//if local radio button is toggled, apply timezone offset.
			if (areaDTOption === 'local'){
				dtNum = dtNum + (-1 * (effOffset * msHr));
			}

			if (dtgEl.data('schedule') !== schOption) {
				return;
			}

			if (schOption && dtg && dt && dtNum && dtValues) {
				$this['areas'][tab][dates][schOption][dtgroup][dtg] = dtNum;

			} else {
				$this['areas'][tab][dates][schOption][dtgroup][dtg] = null;
			}

			if (dtg === 'eff') {
				var exp = e.sender.wrapper.next()
					.find('input.area-dtg.area-exp-dtg')
					.data('kendoDateTimePicker');

				if (exp) {

					var expVal = exp.value();

					if(!dtNum && dt == null){
						exp.min(new Date('01/01/1970 12:00:00 AM'));
						return;
					}

					if (!expVal || (expVal && dtNum > Date.parse(expVal)) ) {
						exp.value(dt);
						$this['areas'][tab][dates][schOption][dtgroup]['exp'] = dtNum;
					}

				} else {
					//console.log('Set Area Dates: no exp dates');
				}

			} else if (dtg === 'exp') {

				var eff = e.sender.wrapper.prev()
					.find('input.area-dtg.area-eff-dtg')
					.data('kendoDateTimePicker');

				if ( eff ) {

					var effVal = eff.value();

					// if(dtNum && dt !== null){
					// 	eff.max(new Date(dtNum));
					// } else {
					// 	eff.max(new Date('12/31/9999 11:59:59 PM'));
					// 	return;
					// }

					if(!dtNum && dt == null){
						eff.max(new Date('12/31/9999 11:59:59 PM'));
						return;
					}

					if (!effVal || (effVal && dtNum < Date.parse(effVal)) ) {
						eff.value(dt);
						$this['areas'][tab][dates][schOption][dtgroup]['eff'] = dtNum;
					}

				} else {
					console.log('Set Area Dates: no eff dates');
				}
			}
		},
		//open tfr
		openTfr: function(e) {
			this.set('tfrMsg', 'Opening existing TFR');
			var $this = this,
				menu = $('#tfr-menu').data('kendoMenu'),
				saveMenu = $('#saveMenu'),
				mode = $this.get('tfrMode');

			menu.close();

			//BK: Gray out all layers
			this.grayOutTfrShapes(true);

			if (mode && mode !== "") {
				var options,
					msg = 'Your TFR is under ' + mode + ' mode.';
				$this.set('tfrMsg', msg);

				//check new / edit
				options = {
					title: 'Open TFR',
					message: 'The current TFR is under ' + mode + ' mode. Would you like to proceed?',
					buttons: ['YES', 'NO']
				};

				nisis.ui.displayConfirmation(options)
				.then(function(action) {
					if (action && action === options.buttons[0]) {
						saveMenu.hide();
						$this.listTfrs();
					}

				}, function() {
					$this.set('tfrMsg', 'An error occured while opening confirmation dialog.');
				});

			} else {
				$this.listTfrs();
			}
		},
		publishTfr: function(e) {
			var $this = this,
				msg = 'Publishing an existing TFR',
				notam_id = $this.get('notam_id');

			//if notam_id is null, return error
			if(notam_id == undefined || notam_id == "") {
				$this.set('tfrMsg', 'Error: TFR must be saved prior to being submitted!');
				return;
			}

			//check if there is ! in tfrText
			notamText = $this.get('tfrNotamText');
			if (notamText.indexOf('!',1) != -1)	{
				$this.set('tfrMsg', 'Error: TFR must not contain the "!" character');
				return;
			}


			$('#tfr-dialog').spin(true);

			$this.set('tfrMsg', msg);
			$this.submitTfrToNES(notam_id, "DRAFT");
	    },
	    openTfrHelp: function(e){
            //console.log("Help document:", e);
            //var help = "/nisis/docs/?type=help";
            var help = "/nisis/docs/TFR_Quick_Reference_Guide.pdf";

            window.open(help, '_blank');
        },
	    cancelTfr: function(e) {
			var $this = this,
				msg = 'Canceling an existing TFR',
				notam_id = $this.get('notam_id');
				notam_usns_status = $this.get('notam_usns_status');

			if (notam_id == undefined || notam_id == "" || notam_usns_status == undefined || notam_usns_status == "" ||  notam_usns_status.indexOf("Active NOTAM") == -1) {
				$this.set('tfrMsg', 'Error: TFR must be active prior to being cancelled!');
				return;
			}

			$('#tfr-dialog').spin(true);

			$this.set('tfrMsg', msg);
			$this.submitTfrToNES(notam_id, "CANCEL");
	    },
	    deleteTfr: function(e) {
	    	var $this = this;
	    	msg = 'Deleting an existing TFR',
	    	    $selectedTfr= $(e.currentTarget).closest("tr");
				notam_id = $selectedTfr.find('td:first').text();
				notam_usns_status = $selectedTfr.find('td:nth-child(6)').text();

			//business rules

	    	//1.Cannot delete published/active/submitted NOTAMs until they have expired.
			//2.Users can delete other NOTAMs only if they are in the same group. For example the SOSCs can delete each other NOTAMs because they work together, and sometimes need to delete an old drafted NOTAM to create a new one.
			if (notam_id == undefined || notam_id == "" || notam_usns_status.indexOf("Pending edit/review by USNOF") >=0  ) {
				$this.set('tfrMsg', 'Error: Unable to delete TFR, TFR has already been submitted to USNS.');
				return;
			}

			$('#tfr-dialog').spin(true);

			$this.set('tfrMsg', msg);

	    	$.ajax({
				url: 'html/windows/tfr/tfr-delete.php',
				type: 'GET',
				dataType: 'json',
				data: {
                        notamid: Number(notam_id),
                        userid: Number(nisis.user.userid)
                },
				success: function(data){
					$('#tfr-dialog').spin(false);
				    //parse the result
				    try {
				    	//new notam id
						result = JSON.parse(data.result);
						if (result==1) {
							//now delete the row from the grid
							//unable to delete a row from the grid
							var grid = $("#tfrGrid").data("kendoGrid"),
				        	dataSource = grid.dataSource,
				        	uid = $selectedTfr.attr("data-uid"),
				        	dataItem = dataSource.getByUid(uid);
								dataSource.remove(dataItem);
				        	grid.refresh();

				        	//refresh the tfr polygon layer
	            			var polygons = nisis.ui.mapview.map.getLayer('tfr_polygon_features');
	            			polygons.refresh();

				        	//refresh the grid and the pager bar, go to the previous page if the last shape on this page has been deleted
        					grid.pager.refresh();
        					pager = grid.pager;
        				    //number of rows on the current page
				        	var numberOfRowsCurrentPage=$("#tfrGrid table tbody tr").length;
	            			//uh oh we are deleting the last row on the page. Go to a previous page so we dont display empty page. NISIS-1972
	            			if (numberOfRowsCurrentPage == 1) {
		            			currentPage = pager.page();
		            			if (currentPage>1) {
		            				grid.pager.page(currentPage-1);
		            			}
	            			}
						}
						else if (result == -1) {
							console.log('Unable to delete TFR created by the user from the different group.');
							$this.set('tfrMsg', 'Error: Unable to delete TFR created by the user from the different group.');
						}
						else {
							console.log('Invalid result returned from tfr-delete.php ', result);
						}

					} catch (err) {
						console.log('Unable to parse results returned from tfr-delete.php. Error: ', err);
					}
				},
				error: function(){
					$('#tfr-dialog').spin(false);
					console.log(arguments);
					$this.set('tfrMsg', 'App could not delete exiting TFR.');
				}
			});
	    },
	    submitTfrToNES: function(notam_id, action) {
	    	var $this = this;
	    	var $action = action;
			$.ajax({
				url : 'api/tfr/tfr-request.php',
				type : 'POST',
				data : {notamId: notam_id, action:action},
				success: function(data, status, xhr) {
					//console.log("TFR Publish: successful ajax call:", arguments);
					var resp = null;

					try {
						resp = JSON.parse(data);
						$this.set('tfrMsg', resp.message);

						//if grid was open and they selected a tfr and published it, update the DOM with SUBMITTED status for NISIS status and Pending edit/review by USNOF for USNS status
						//the database has already been updated with the SUBMITTED/Pending edit/review by USNOF statuses if TFR was successully submitted
						var $selectedTfr = $("#tfrGrid tr.k-state-selected");
						if ($selectedTfr.length>0) {
							var $statusTd = $selectedTfr.find('td:nth-child(5)');
							if ( $statusTd.length>0 /* && $statusTd.text() === "DRAFT" */) {
								$statusTd.html("SUBMITTED");
							}
							//if notam has been submitted successfully then set initial status from USNS
							if (resp.message.indexOf("NOTAM has been submitted successfully") !=-1 || resp.message.indexOf("NOTAM cancellation request has been submitted successfully") !=-1 ) {
								//Pending edit/review by USNOF
								var $usnsStatusTd = $selectedTfr.find('td:nth-child(7)');
								if ( $usnsStatusTd.length>0 ) {
									$usnsStatusTd.html("Pending edit/review by USNOF");
								}
							}
							else {
								if ( $statusTd.length>0) {
									$statusTd.html("DRAFT");
								}
							}
						}
					} catch (err) {
						$this.set('tfrMsg', "App could not interpret the response.");
					}

					$('#tfr-dialog').spin(false);
				},
				error: function(xhr, status, err) {
					$this.set('tfrMsg', "App could not process your input: " + status);
					$('#tfr-dialog').spin(false);
				}
			});
	    },
		listTfrs: function() {
			var $this = this;
			//clear tfr name
			$this.set("tfrName", "");
			//reset tfr data
			$this.resetTfrData();

			var loadTfr = $("<div/>", {
				"class": "open-tfr clr"
			});

			//$("#tfr-content").children().remove();
			$("#tfr-content").append(loadTfr);

			var gridSel = $("<div id='tfrGrid'>").kendoGrid({
				dataSource: [],
				dataSource: new kendo.data.DataSource({
					type: 'odata',
					transport: {
						read: "html/windows/tfr/tfr-open.php"
						,type: 'GET'
                    	,dataType: 'json'
					}
					,schema: {
						data: "result",
						model: {
							fields: {
								notam_name: {type : "string"},
								notam_id: {type : "string"},
							    notam_type: {type : "string"},
								work_number_id: {type: "string"},
								status: {type: "string"},
								usns_status : {type : "string"},
								last_update: {type: "date"},
								user_name: {type: "string"}
							}
						}
					}
					,pageSize: 20
				}),
				height: 550,
				groupable: false,
				sortable: true,
				filterable : true,
				pageable: true,
				selectable: 'single',
				change: function(e) {
					//console.log("TFR is selected from the grid");
				    var selectedRows = this.select();
				    //make sure its only one, we got single select
				    var dataItem = this.dataItem(selectedRows[0]);

				    // console.log("TFR is selected from the grid: dataItem.NOTAM_ID: ", dataItem.NOTAM_ID);
			        // console.log("TFR is selected from the grid:  dataItem.USNS_STATUS: ", dataItem.USNS_STATUS);

			        $this.set('notam_id', dataItem.NOTAM_ID);
			        $this.set('notam_usns_status', dataItem.USNS_STATUS);
			        //CC - We should not pass the event nor the grid (this should be a call with no parameters since we can get the 'notam_id' from the global variable) to call editTFR function but I have no other option to call editTFR() in the same way it is being called before, faking an event with notamId in it, otherwise it will break the app.
			        $this.editTfr({notamId: dataItem.NOTAM_ID});
				},
				columns: [{
					field: "NOTAM_ID",
					title: "NOTAM_ID",
					hidden: true
				}, {
					field: "NOTAM_NAME",
					title: "NOTAM Name",
					width: 100
				}, {
					field: "NOTAM_TYPE",
					title: "Type",
					width: 100
				}, {
					field: "NOTAM_STATUS",
					title: "Status",
					width: 75
				}, {
					field: "WORK_NUMBER_ID",
					title: "USNS Work number id",
					hidden: true
				}, {
					field: "USNS_STATUS",
					title: "USNS Status",
					width: 100
				}, {
					field: "LAST_UPDATE",
					title: "Last Updated",
					width: 150
				}, {
					field: "USER_NAME",
					title: "Created By",
					width: 100
				}, {
					command: {
						text: "Delete",
						click: function(e) {
							$this.deleteTfr(e);
						}
					},
					title: "",
					width: "65px"
				}]
			});
            //Append tfr grid
			loadTfr.append(gridSel);
			//get tfr data from db
			$.ajax({
				url: 'html/windows/tfr/tfr-open.php',
				type: 'GET',
				success: function(data){
					var d;
					try {
						d = JSON.parse(data);
					} catch (e) {
						console.log(e);
					}
					if (d) {
						gridSel.data('kendoGrid').dataSource.data(d);
					}
				},
				error: function(){
					console.log(arguments);
					$this.set('tfrMsg', 'App could not retreive existing TFRs.');
				}
			});
		},
		operatingInstructions: [],
		//edit tfr
		editTfr: function(e, grid) {
			var $this = this,
				copy = false,
				typeDDL = "",
				saveMenu = $('#saveMenu'),
				dataItem = {};

			if (e.currentTarget && grid) {  //open button click
				dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

			} else {//create NOTAM by copy
			    //irena - have new notam id after deep copy
			    dataItem.NOTAM_ID = e.notamId;
			}

			//set the usns_status
			$this.set('notam_usns_status', dataItem.USNS_STATUS);

			//this is how you can get selected row data and use only 1 'Open' button
			//console.log("Open TFR dataItem:", grid.dataItem(grid.select()));

			//lookup NOTAM from database
			$.ajax({
				url: 'html/windows/tfr/tfr-load.php',
				type: 'POST',
				//dataType: 'json',
				data: {
					action : 'loadBody',
					notam_id: dataItem.NOTAM_ID
				},
				success: function(data) {
					var notamData = JSON.parse(data);
					if ( !notamData ) return;

					//for editing only
					if ( !copy ) {
						$this.set('notam_id', notamData[0].NOTAM_ID);
						$this.set('tfrName', notamData[0].NOTAM_NAME);
						$this.set('status', notamData[0].STATUS);
					} else {
						$this.set('tfrName', dataItem.NOTAM_NAME);
					}

					$this.set('tfrTypeId', notamData[0].NOTAM_TYPE);
					//$this.updateAreaInstructions(notamData[0].NOTAM_TYPE);
					$this.set('artccId', notamData[0].ARTCC_ID);
					$this.set('artccName', notamData[0].ARTCC_NAME);
					$this.set('effTimeZone', notamData[0].EFFECTIVE_DATE_UTC);
					$this.set('expTimeZone', notamData[0].EXPIRATION_DATE_UTC);
					//Getting the offsets for effective and expiration Time zones the first time the TFR is loaded.
					$this.effTimeZoneOnChange();
					$this.expTimeZoneOnChange();
					$('select.tfr-artcc-id').data('kendoDropDownList').trigger('change');
					$this.set('custom_text', notamData[0].CUSTOM_TEXT);
					$this.set('tfrTitle', notamData[0].DISPLAY_NAME);
                    $this.set('includeLocTime',$this.convertTruth(notamData[0].INCLUDE_LOCAL_TIME));
                    $this.set('includeFRD',$this.convertTruth(notamData[0].INCLUDE_FRD));

                    $this.loadLocTimeInclusion();
                    $this.loadFRDInclusion();

					if ( !copy ) {
						$(".open-tfr").fadeOut(1500, function() {
							$(this).remove();
							//Commented out this line because this message cannot be cleared after it's being loaded/set - CC
							// $this.set('tfrMsg', 'Populating input fields...');
							$this.set('tfrShowForm', true);
							$this.set('tfrMode', 'edit');
						});
					}
				},
				error: function(){
					$this.set('tfrMsg', 'App could not open the selected TFR.');
					return;
				}
			});

			//get location information
			$.ajax({
				url: 'html/windows/tfr/tfr-load.php',
				type: 'POST',
				data: {
					action : 'loadLocations',
					notam_id: dataItem.NOTAM_ID
				},
				success: function(data){
					var notamData = JSON.parse(data);
					var count = notamData.length;

					//update locations drop down(s)
					var updateDD = function(i, cities) {
						var states = $($('select.tfr-loc-state')[i]);

						if (cities.length) {
							cities.data('kendoComboBox').enable(true);
							$this.tfrGetCities(notamData[i].STATE_ABBREV)
							.then(
							function(data) {
								if (cities.data('kendoComboBox').dataSource) {
									cities.data('kendoComboBox').dataSource.data(data);
										cities.data('kendoComboBox').value(notamData[i].CITY);
										states.data('kendoDropDownList').value(notamData[i].STATE_ABBREV);
								} else {
									console.log("No data source");
								}
							});

						} else {
							console.log("No cities");
						}
					}

					for (i = 0; i < count; i++){
						if(i > 0){
							$this.addNewLocation();
						}
						var cities = $($('select.tfr-loc-city')[i]);
						updateDD(i, cities);
						$this.locations.get("loc" + i).city = notamData[i].CITY;
						$this.locations.get("loc" + i).stName = notamData[i].STATE_TERRITORY;
						$this.locations.get("loc" + i).state = notamData[i].STATE_ABBREV;
					}
				},
				error: function(){
					console.log(arguments);
					return;
				}
			});

			//lookup NOTAM Area information from database
			$.ajax({
				url: 'html/windows/tfr/tfr-load.php',
				type: 'POST',
				data: {
					action : 'loadAreas',
					notam_id: dataItem.NOTAM_ID
				},
				success: function(data){
					// console.log("loading areas:", data);
					var areaDates = {},
						areaShapes = {},
						//areaInsts = {},
						notam = null;
					try {
						notam = JSON.parse(data);
					} catch (err) {
						console.log('Notam areas error: ', err);
					}
					if (!notam) return;

					var	notamData = notam.areas,
						notamDates = notam.dates,
						notamShapes = notam.shapes,
						//notamInst = notam.inst,
						count = notamData ? notamData.length : 0;

					//console.log('Notam Areas: ', count, notamData, notamDates, notamShapes);
					//get area dates as a json object
					$.each(notamDates, function(i, d) {
						if ( !areaDates[d['AREA_ID']] ) {
							areaDates[d['AREA_ID']] = [];
						}
						areaDates[d['AREA_ID']].push(d);
					});

                    //irena - load shapes per area. Multiple shapes per area
                    //create shapes grids
					//get area shapes as a json object
					if (!copy) {
						$.each(notamShapes, function(i, d) {
							if ( !areaShapes[d['AREA_ID']] ) {
								areaShapes[d['AREA_ID']] = [];
							}
							// console.log('AreaShape: ', i, d);

							var row = {};
							row["OBJECTID"] = d['SHAPE_ID'];
							row["NAME"] = d['NAME'];
							row["VOR"] = d['REFERENCE_VOR'];

							areaShapes[d['AREA_ID']].push(row);
						});
					}

					//get area instructions as a json object
					// $.each(notamInst, function(i, d) {
					// 	if ( !areaInsts[d['AREA_ID']] ) {
					// 		areaInsts[d['AREA_ID']] = [];
					// 	}
					// 	areaInsts[d['AREA_ID']].push(d);
					// });

					//store all the options here
					var areaOpts = [];

					for (i = 0; i < count; i++) {

						var flightAuthCheck = notamData[i].ALLOW_AUTHORIZATION == true? true:false;

						var dates = [],
						shapes = [],
						area = {
							name: notamData[i].AREA_NAME,
							options: {
								tab: i+1,
								//instruction:notamData[i].INST_ID,
								flightAuth: flightAuthCheck,
								definition: notamData[i].DEFINITION_OR_NAME,
								areaOption: notamData[i].UTC_TIME_ZONE,
								areaSchedule: notamData[i].SCHEDULED,
								monday: notamData[i].MONDAY,
								tuesday: notamData[i].TUESDAY,
								wednesday: notamData[i].WEDNESDAY,
								thursday: notamData[i].THURSDAY,
								friday: notamData[i].FRIDAY,
								saturday: notamData[i].SATURDAY,
								sunday: notamData[i].SUNDAY
							}
						};

						// if areaDates information is present, add to area parameter
						if ( areaDates[area.options.tab] ) {
							area.options.dates = areaDates[area.options.tab];
						}

						//if areaShapes information is present, add to area parameter
						/*
						 * eg. Shape Info
					 	{
						 	AREA_ID: "1"
							NAME: "POLYGON - 2016/01/02 2154Z"
							NOTAM_ID: "732"
							REFERENCE_VOR: "ATR-VOR "
							SHAPE_ID: "4117"
						}
						*/
						if ( areaShapes[area.options.tab] ) {
							area.options.shapes = areaShapes[area.options.tab];
						}
						else {
							area.options.shapes = [];
						}

						if (copy) {
							area.options.shapes = [];
						}

						//if areaInsts information is present, add to area parameter
						// if ( areaInsts[area.options.tab] ) {
						// 	area.options.instructions = areaInsts[area.options.tab];
						// 	//console.log("areaInsts", areaInsts);
						// }
						//compile data
						areaOpts.push(area);
					}
					if ( areaOpts.length ) {
						$this.addNewArea(areaOpts, !copy);
					}
				},
				complete: function(){
					typeDDL = $("#typeDropDown").getKendoDropDownList();
					//database id: 1-base. Javascript: 0-based
					typeDDL.select(parseInt($this.tfrTypeId)-1);
					typeDDL.trigger("change");

					//instructions can only be reused when there is more than 1 area
					$this.enableInstsReuse();
				},
				error: function(){
					console.log(arguments);
					return;
				}
			});

			//Retrieving NOTAM operating Instructions from database
			$.ajax({
				url: 'html/windows/tfr/tfr-load.php',
				type: 'POST',
				data: {
					action : 'loadOprInsts',
					notam_id: dataItem.NOTAM_ID
				},
				success: function(data){
					var oprInsts = {},
						notam = null;
					try {
						notam = JSON.parse(data);
					} catch (err) {
						console.error('Error retrieving the Notam operating instructions: ', err);
					}

					if (!notam){
						console.error('notam variable is not defined in the ajax to call loadOprInsts action');
						return;
					} else {
						var	notamOprInsts = notam.oprInsts

						if (notamOprInsts.length > 0){
							$this.set('operatingInstructions', notamOprInsts);
							$this.loadOperatingInsts();
						}

						//get operating instructions as a json object
						// $.each(notamOprInsts, function(i, d) {
						// 	if ( !oprInsts[d['INSTRUCTION_ID']] ) {
						// 		oprInsts[d['INSTRUCTION_ID']] = [];
						// 	}
						// 	oprInsts[d['INSTRUCTION_ID']].push(d);
						// });


						// console.log("oprInsts", oprInsts);
						// if ( oprInsts.length ) {
						// 	$this.addNewArea(oprInsts, !copy);
						// }
					}
				},
				complete: function(){
					typeDDL = $("#typeDropDown").getKendoDropDownList();
					//database id: 1-base. Javascript: 0-based
					typeDDL.select(parseInt($this.tfrTypeId)-1);
					typeDDL.trigger("change");

					//instructions can only be reused when there is more than 1 area
					$this.enableInstsReuse();
				},
				error: function(){
					console.log(arguments);
					return;
				}
			});

			//lookup NOTAM Extraneous Information from database
			$.ajax({
				url: 'html/windows/tfr/tfr-load.php',
				type: 'POST',
				data: {
					action : 'loadExtra',
					notam_id: dataItem.NOTAM_ID
				},
				success: function(data){
					var notamData = JSON.parse(data);

					if(notamData[0]){
						$this.set('gOpsName', notamData[0].POINT_OF_CONTACT_NAME?notamData[0].POINT_OF_CONTACT_NAME:null);
						$this.set('gOpsOrg', notamData[0].POC_ORGANIZATION?notamData[0].POC_ORGANIZATION:null);
						$this.set('gOpsTels', notamData[0].POC_PHONES?notamData[0].POC_PHONES:null);
						$this.set('gOpsFreqs', notamData[0].POC_FREQUENCIES?notamData[0].POC_FREQUENCIES:null);
						$this.set('crdFacId', notamData[0].COORDINATING_FACILITY_ID?notamData[0].COORDINATING_FACILITY_ID:null);
						$this.set('crdFacName', notamData[0].COORD_FAC_NAME?notamData[0].COORD_FAC_NAME:null);
						$this.set('crdFacType', notamData[0].COORD_FAC_TYPE_ID?{NAME : notamData[0].COORD_FAC_TYPE, ID : notamData[0].COORD_FAC_TYPE_ID}:"{NAME:'',ID:''}");
						$this.set('crdFacTels', notamData[0].COORD_FAC_PHONES?notamData[0].COORD_FAC_PHONES:null);
						$this.set('crdFacFreqs', notamData[0].COORD_FAC_FREQUENCIES?notamData[0].COORD_FAC_FREQUENCIES:null);
						// HH -> Controlling Facility is never used.
						// $this.set('ctlFacName', notamData[0].CONTROLLING_FACILITY_NAME?notamData[0].CONTROLLING_FACILITY_NAME:null);
						// $this.set('ctlFacType', notamData[0].CNTRL_FAC_TYPE?notamData[0].CNTRL_FAC_TYPE:null);
						// $this.set('ctlFacFreqs', notamData[0].CNTRL_FAC_FREQ?notamData[0].CNTRL_FAC_FREQ:null);
						$this.set('tfrReason', notamData[0].REASON?notamData[0].REASON:null);
						$this.set('modTfrNum', notamData[0].MODIFY_ID?notamData[0].MODIFY_ID:null);
						$this.set('modTfrReason', notamData[0].MODIFY_REASON?notamData[0].MODIFY_REASON:null);
						$this.set('crdFacVisible', notamData[0].SHOW_COORD == 1? true : false);
						// $this.set('ctlFacVisible', notamData[0].SHOW_CNTRL == 1? true : false);
						$this.set('gOpsVisible', notamData[0].SHOW_POC == 1? true : false);
					}
				},
				error: function() {
					console.log(arguments);
					return;
				}
			});

			//lookup NOTAM Custom Text from database
			$.ajax({
				url: 'html/windows/tfr/tfr-load.php',
				type: 'POST',
				data: {
					action : 'loadText',
					notam_id: dataItem.NOTAM_ID
				},
				success: function(data){
					var notamData = JSON.parse(data);
					if (notamData && notamData[0]) {
						$this.set('tfrNotamText', notamData[0].TEXT);
						//console.log('notam <== datastore:', notamData[0].TEXT);
						//console.log('notam <== app data:', $this.get('tfrNotamText'));

					} else {
						$this.set('tfrNotamText', '');
					}
				},
				error: function() {
					console.log(arguments);
					return;
				}
			});

			//load new save menu
			saveMenu.show();
		},
		//update all area date/time information based off of javascript variables
		updateAreaDates: function () {
			var $this = this,
				tfrArea = $("#tfr-areas"),
				len = $("#tfr-areas").find(".k-tabstrip-items").children.length;

				/*for(i=1;i<=len;i++){
					var webArea = $("#tfr-areas-" + i),
						javaArea = $this.areas[i],
						schedule = javaArea.areaDTSchOption;
						console.log(i, webArea, javaArea);
				}*/
			// console.log($this, tfrArea);
		},
		//review tfr
		reviewTfr: function(e) {
			this.set('tfrMsg', 'Review an existing TFR');
		},
		//save tfr
		saveTfr: function (e) {
			$('#tfr-menu').data('kendoMenu').close();

			var $this = this,
				mode = $this.get('tfrMode'),
				name = $this.get('tfrName'),
				numTabs = $(".tfr-area").length;

			$this.set('tfrMsg', 'Saving TFR: ' + name);

			if (name && name === "") {
				$this.set('tfrMsg', 'Invalid TFR Name. Please enter a name.');
				return;
			}

			//Getting the operating instructions from the DOM and put them into an array
			var oprInstructions = $(".tfrInstructions").children();
			var numOprInstructions = oprInstructions.length;
			var operatingInstructions = [];

			for (var j = 0; j < numOprInstructions; j++){
				var instructionID = j;
				var text = oprInstructions[j].innerHTML;
				operatingInstructions.push({"instId": instructionID, "text": text});
			}

			$this.set("operatingInstructions", operatingInstructions);

			//Grab the shapes for each area
			//BK: 01/05/2016 ==> This needs to be update as the shape name, type and vor changes.
			/*for (var i = 1; i <= numTabs; i++) {
				areaTab = $("#tfr-areas-" + i);
				$this.areas[i].shape = tfrShapesGrid.getShapes(areaTab);
			}*/

            //update TFR NOTAM text
            $this.updateTfrNotamText();

            formattedNotamText = $this.get('tfrNotamText').replace(/<BR\/>/gi, ' ').replace(/<br>/gi, ' ');
            formattedNotamText = formattedNotamText.replace(/[\uE000-\uF8FF]/g, '');

			//pass the data to the server for parsing & insert/update
			$.ajax({
				url: "html/windows/tfr/tfr-save.php",
				data: {
					notam_id : $this.get('notam_id'),
					notam_type : $this.get('tfrTypeId'),
					notam_name : $this.get('tfrName'),
					locations_array : JSON.stringify($this.get('locations')),
					status : 1,
					//instruction : $this.find,
					artcc_id : $this.get('artccId'),
					artcc_name : $this.get('artccName'),
					effective_date_utc : $this.get('effTimeZone'),
					expiration_date_utc : $this.get('expTimeZone'),
					custom_text : $this.get('custom_text'),
					operatingInstructions : JSON.stringify($this.get('operatingInstructions')),
					//save NOTAM Text by default
					//save_text : $this.get('custom_text') == 1 ? $("#notamEditor").children().text(): null,
					//save_text : $this.get('custom_text') == 1 ? $this.get('tfrNotamText'): null,
					save_text : formattedNotamText,
					area_array : JSON.stringify($this.get('areas')),
					poc_name : $this.get('gOpsName')?$this.get('gOpsName'):null,
					poc_org : $this.get('gOpsOrg')?$this.get('gOpsOrg'):null,
					poc_phone : $this.get('gOpsTels')?$this.get('gOpsTels'):null,
					poc_freq : $this.get('gOpsFreqs')?$this.get('gOpsFreqs'):null,
					coord_id : $this.get('crdFacId')?$this.get('crdFacId'):null,
					coord_name : $this.get('crdFacName')?$this.get('crdFacName'):null,
					coord_type : JSON.stringify($this.get('crdFacType'))?JSON.stringify($this.get('crdFacType')):null,
					coord_phone : $this.get('crdFacTels')?$this.get('crdFacTels'):null,
					coord_freq : $this.get('crdFacFreqs')?$this.get('crdFacFreqs'):null,
					ctrl_name : $this.get('ctlFacName')?$this.get('ctlFacName'):null,
					ctrl_type : $this.get('ctlFacType')?$this.get('ctlFacType'):null,
					ctrl_freq : $this.get('ctlFacFreqs')?$this.get('ctlFacFreqs'):null,
					tfr_reason : $this.get('tfrReason')?JSON.stringify($this.get('tfrReason')):null,
					modify_id : $this.get('modTfrNum')?$this.get('modTfrNum'):null,
					modify_reason : $this.get('modTfrReason')?$this.get('modTfrReason'):null,
                    include_local_time : $this.get('includeLocTime')?$this.convertTruth($this.get('includeLocTime')):0, // FB - changed `null` to 0 in false clause
                    include_frd : $this.get('includeFRD')?$this.convertTruth($this.get('includeFRD')):0,
				},
				type: "POST",
				async: false,
				success: function(d) {
					// console.log("d",d);
					if(d['error']){
						$this.set('tfrMsg', 'Error: ' + d['errormessage']);

					} else {
						$this.set('notam_id', d);

						var msg = 'NOTAM ' + $this.get('tfrName') + ' saved!';
						$this.set('tfrMsg', msg);
						//reset tfr modification
						$this.set('tfrModified', false);

						setTimeout(function() {
							//make sure you don't override other messages
							if (msg == $this.get('tfrMsg')) {
								$this.set('tfrMsg', '');
							}

						}, 3000);
					}
				},
				dataType:"json",
				error: function(xhr, status, err){
					//console.log("NOTAM Save error:", arguments);
					$this.set('tfrMsg', "App could not save your NOTAM: " + status);

					$('#tfr-dialog').spin(false);
				}
			});
		},
		//TFR Notam Text
		tfrNotamText: "",
		notamTextLen: 0,
		copyNotamText: function(e) {
			var $this = this,
				notamTxt = document.getElementById('notamEditor');

			if (document.selection) {
	            var range = document.body.createTextRange();
	            range.moveToElementText(notamTxt);
	            range.select();
	        } else if (window.getSelection()) {
	            var range = document.createRange();
	            range.selectNodeContents(notamTxt);
				window.getSelection().removeAllRanges();
	            window.getSelection().addRange(range);
	        }
	        nisis.ui.displayMessage('Press CTRL+C to copy selected text');
		},
		updateTfrNotamText: function(e) {
			//console.log("Updating NOTAM.....");
			var $this = this,
				template = $this.get('notamTemplate'),
				notamTxt = "",
				//instructions = $(".tfrInstructions").children(),
				instructions = $(".tfrInstructions"),
				instText = "";

			if ( $this.custom_text != 1 ) {
				var artcc = $this.get('artccId');

				var locationInfo = "",
					stateInfo = "",
					areasText = "";

				var startDate,
					endDate,
					formattedStartDate,
					formattedEndDate;

				var tfrReason = $this.get("tfrReason");

				var coordName = $this.get("crdFacName"),
					coordID = $this.get("crdFacId"),
					coordType = $this.get("crdFacType") ? $this.get('crdFacType').NAME : "",
					coordPhone = $this.get("crdFacTels"),
					coordFreq = $this.get("crdFacFreqs");

				var pocOrg = $this.get("gOpsOrg"),
					pocName = $this.get("gOpsName"),
					pocPhone = $this.get("gOpsTels"),
					pocFreq = $this.get("gOpsFreqs");

				var monthArray = ['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY',
								  'AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'];

				var replaceArray = [];
				var includesText = "",
					excludesText = "",
					baseText = "";

				var count = 0;

				//append a 0 if number is less than 10;
				var padLeft = function(number) {
					var toReturn;
					if (number < 10){
						toReturn = '0' + number;
					} else {
						toReturn = number;
					}
					return toReturn;
				}

				function quickSort(a, low, high) {
					if(high > low){
						var index = getRandomInt(low,high);
						var pivot  = a[index].start;
						a = partition(a,pivot);
						quickSort(a,low,index-1);
						quickSort(a,index+1,high);
					}

					return a;
				}

				function partition (a, pivot) {
					var i = 0;
					for( var j=0; j < a.length; j++ ){
						if( a[j].start!= pivot && a[j].start < pivot ){
							var temp = a[i];
							a[i] = a[j];
							a[j] = temp;
							i++;
						}
					}
					return a;
				}

				function getRandomInt (min, max) {
				    return Math.floor(Math.random() * (max - min + 1)) + min;
				}

				//generate FRD Text
				function createFRDText(vertice, pt, shapeVor, j) {
					// console.log("createFRDText(vertice, pt, shapeVor, j):", vertice, pt, shapeVor, j);
					var toReturn = "";

					if (!vertice || !pt) {
						return toReturn;
					} else {
						/*
						 * BK: 01/22/2016 => there are some inconsistency in the format of vertices.
						 * Circle don't have '/' and other shapes do.
						 * Solution: Get ride of the '/'
						 */
						vertice = vertice.replace("/", "");
					}

					var frd = $this.frds[vertice],
						vor = $this.get('vor'), //BK: 01/06/2016 ==> What happens when you have more than 1 shape for the tfr?
						vorId = vor ? vor.split('-')[0] : null,
						newFrd = null;

					//console.log("FRDs, vorID, vertice, $this.frds:", JSON.stringify($this.frds[vertice]), vorId, vertice, JSON.stringify($this.frds) );
					if ( frd && vorId && frd.indexOf(vorId) === 0 ) {
						//console.log("$this.frds[vertice]:", JSON.stringify($this.frds[vertice]));
						toReturn += "(" + $this.frds[vertice] + ")";
					}
					else {
						// this might take a while so append a place holder
						toReturn += "({" + vertice + "})";
						//get new frd value BK==> The generated FRDs will replace vertices in the the notam text
						$this.getFRDfromVertice(pt, vertice, shapeVor);
					}
					//console.log("FRD Text:", toReturn);
					return toReturn;
				}
				//generate altitude text
				function createAltitudeText(thisVert) {
					var toReturn = " ";
					var floorFL = thisVert.ftype === 'fl'?true:false;
					var ceilFL = thisVert.ctype === 'fl'?true:false;
					// console.log("this", $this, "thisVert", thisVert);
					//add floor measurement or "SFC" if 0
					toReturn += thisVert.floor === 0 ? "SFC" : floorFL? "FL" + thisVert.floor : thisVert.floor + "FT";
					if(thisVert.floor !== 0){
						toReturn += thisVert.ftype === thisVert.ctype ? "" :  !floorFL? " " + thisVert.ftype : "";
					}
					toReturn += "-";
					toReturn += ceilFL? "FL":"";
					toReturn += thisVert.ceil;
					toReturn += ceilFL? "" : "FT " + thisVert.ctype;

					//There needs to be a space after the period here.
					toReturn += ". ";

					return toReturn.toUpperCase();
				}
				//format all vertices
				function formatVertices(vertices, isCircle, isBuffer, isArc, frd, dds, shapeVor) {
					//console.log("Format Vertices: ", vertices, isCircle, isBuffer, isArc, frd, dds, shapeVor);
					var text = "";

					text += "AN AREA DEFINED AS ";

					//var reqFRD = (frd && dds && $this.get('vor')) ? true : false;
					var reqFRD = (frd && dds && shapeVor) ? true : false;
					//console.log("Format Vertices, vor: ", frd, dds, $this.get('vor'));
					//console.log("Format Vertices, vor: ", frd, dds, shapeVor);

					if (isCircle) {
						var vertString = vertices[0].split(", ");

						text += vertString[1] + "NM RADIUS OF " + vertString[0];

						if ( reqFRD ) {
							text += " ";
							text += createFRDText(vertString[0], dds[0], shapeVor);
						} else {
							console.log("Format Vertices: FRD will not be created.");
						}

					} else {

						if (isBuffer){
							var radius = vertices[vertices.length-1];
							text += radius + " NM FROM ANY POINT ALONG A LINE STARTING AT ";
							for (j = 0; j < vertices.length-2; j++) {
								text += j > 0 ? " TO " + vertices[j] : vertices[j];
								//space-holder for FRD Information (if selected in corresponding area)
								if ( reqFRD ) {
									text += " ";
									text += createFRDText(vertices[j], dds[i], shapeVor, j);
									console.log(i, j, vertices[j], dds[i]);
								}
							}

							if (vertices[0] == vertices[vertices.length-2]) {
								text += " TO THE POINT OF ORIGIN";
							} else {
								// text += " ENDING AT " + vertices[vertices.length-2];
								text += " ENDING AT " + vertices[vertices.length-2];
								if ( reqFRD ) {
									text += " ";
									// text += createFRDText(vertices[vertices.length-2], dds[i], shapeVor);
									text += createFRDText(vertices[vertices.length-2], dds[j], shapeVor);
									console.log(i, j, vertices[vertices.length-2], dds[j]);
								}
							}

						} else if (isArc) {

							//var vertextFRD = (frd && dds)?true:false;
							//var vertextFRD = (frd && dds && $this.get('vor')) ? true : false;

							text += vertices[0];
							text += reqFRD ? " " + createFRDText(vertices[0], dds[0], shapeVor) : "";
							text += " TO ";
							text += vertices[1];
							text += reqFRD ? " " + createFRDText(vertices[1], dds[1], shapeVor) : "";
							text += " THEN CLOCKWISE ON A " + vertices[3] + " NM ARC CENTERED ON ";
							text += vertices[0];
							text += reqFRD ? " " + createFRDText(vertices[0], dds[0], shapeVor) : "";
							text += " TO ";
							text += vertices[2];
							text += reqFRD ? " " + createFRDText(vertices[2], dds[2], shapeVor) : "";


						} else {

							for (j = 0; j < vertices.length-1; j++) {
								text += j > 0 ? " TO " + vertices[j] : vertices[j];
								//space-holder for FRD Information (if selected in corresponding area)
								if ( reqFRD ) {
									text += " ";
									text += createFRDText(vertices[j], dds[j], shapeVor);
								}
							}

							text += " TO THE POINT OF ORIGIN";
						}
					}
					//console.log("Formatted Vertices: ", text);
					return text;
				};
				//get instructions
				function getAreaInstructions(area) {
					console.log('getting area instructions');

					if (!instructions.length || !area) return;

					var instsText = "",
						aInsts = $(instructions[area-1]).children();

					$.each(aInsts, function(i, inst) {
						instsText += "<br>" + $(inst).text();
					});

					//console.log(" getAreaInstructions - current Area:", area);
					//console.log("get Instructions - current Instructions:", instsText);

					return instsText;
				};

				//determine days of week and account for consecutive days
				function checkDays(daysArray, givenIndex, isFirstFound, isContinuous){
					var toReturn = "";
					var dayText = ['MON', 'TUES', 'WED', 'THURS', 'FRI', 'SAT', 'SUN'];
					var keyArray = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
					//start at index 0 by default, or at givenIndex
					var index = givenIndex == undefined?0:givenIndex;
					//if we have found the first day listed, track that it has been
					var firstFound = isFirstFound == undefined?false:isFirstFound;
					//if the days of the week are continuous, flag them as so
					var continuous = isContinuous == undefined?false:isContinuous;
					//if we are beyond the end of the array, exit
					if (index > dayText.length-1){
						return "";
					}

					//if we have already found the first day, append next day(s);
					if (firstFound) {
						if (continuous) {
							toReturn += "-";
						} else {
							toReturn += ",";
						}
					}
					if (daysArray[keyArray[index]]){
						//if days are continuous, enable flag
						if (daysArray[keyArray[index+1]]){
							continuous = true;
						} else {
							continuous = false;
							//firstFound = false;
						}
						if (!firstFound){
							toReturn += dayText[index];
							firstFound = true;
						} else if (!continuous){
							toReturn += dayText[index];
						} else if (firstFound && continuous){
							toReturn += dayText[index];
						}
					}
					var temp = checkDays(daysArray, index+1, firstFound, continuous);
					if (continuous){
						// console.log('toReturn', toReturn , 'last index of - ', toReturn.lastIndexOf("-"), 'length', toReturn.length, 'temp', temp);
						if (toReturn.lastIndexOf("-") == 0 && temp.indexOf("-") == 0){
							return temp;
						} else {
							return toReturn += temp;
						}
					} else {
						// console.log('toReturn', toReturn , 'last index of , ', toReturn.lastIndexOf(","), 'length', toReturn.length, 'temp', temp);
						if (toReturn.length > 1){
							return toReturn += temp;
						} else {
							return temp;
						}
					}
				}

				var locations = $this.locations.toJSON(),
					locs = [],
					cityStates = [];

				for ( var l in locations ) {
					locs.push( locations[l] );
					cityStates.push( locations[l].city + ", " + locations[l].state);
				}

				var distStates = appUtils.getDistinctItems(locs, 'state');
				stateInfo += distStates.join("..");
				locationInfo += cityStates.join("..");

				count = 1;

				var areas = $this.areas,
					areasJSON = $this.areas.toJSON();
				//console.log("areas:", areas, areasJSON);
				//console.log("Updating NOTAM TEXT with Area data: ", areasJSON);
	            //count tabs
	            var tfrAreas = $('#tfr-areas').data('kendoTabStrip'),
	            	tabs = tfrAreas.tabGroup.children(),
	            	tabOrder = [],
	            	//has to match areas' counter
	            	notamCounter = 1,
	            	notamOrder = {};

				function reorderNotams(tabLabel) {
					//var areasJSON = $this.areas.toJSON();
					for(var i in areasJSON){

						// console.log("areasJSON:",areasJSON);
						// console.log("notamCounter:",notamCounter);
						// console.log("areaName:",areasJSON[i].areaName);
						// console.log("areaName's label:",tabLabel);


						if(areasJSON[i].areaName === tabLabel){
							notamOrder[notamCounter] = areasJSON[i];
							notamCounter++;
						}
					}
				}

				//why reordering?
	            //for(i = 0; i < tabs.length; i++){
	            // 	var tabLabel = $(tabs[i]).find('.k-link').text();
	            // 	reorderNotams(tabLabel);
	            // 	tabOrder.push(tabLabel);
	            //}

	            // areasJSON = notamOrder;
				var areaDateTextArray = [];
				/*
				 *	loop through all area tabs gathering date/time and shape information
				 *	would use the quicksort array, but there is no guarantee that the latest
				 *	end date will be in the final position since it is sorted by start date only
				*/
				while (areasJSON[count] || Object.keys(areasJSON).length >= count) {
					//console.log("areasJSON = ", areasJSON, "count = ", count);
					if(!areasJSON[count]){
						count++;
						continue;
					}

					//console.log("areasJSON[count] current:", areasJSON[count]);

					var option = areasJSON[count].areaDTSchOption;
					var i = 1;
					var areaDateArray = [];
					var areaWeekDayArray = areasJSON[count].areaDTSchedule;
					var dayText = "", startTime = "", endTime = "";
					areaDateTextArray[count] = "";
					//Identify the earliest and the latest dates
					while (areasJSON[count].areaDateTimes[option]['dt'+i]) {

						var dtX = areasJSON[count].areaDateTimes[option]['dt'+i];

						if (i == 1 && startDate == undefined) {
							//set start and end date to first area date/time value by default
							startDate = dtX.eff;
							endDate = dtX.exp;
						} else {
							// if value encountered that's smaller than start date, replace
							startDate = startDate > dtX.eff ? dtX.eff : startDate;
							// if value encountered that's larger than end date, replace
							endDate = endDate < dtX.exp ? dtX.exp : endDate;
						}

						areaDateArray.push({index: i, start: dtX.eff, end: dtX.exp});
						i++;
					}

					// console.log(count, areaDateArray, areaWeekDayArray);
					if (option == 'schedule'){
						dayText = " " + checkDays(areaWeekDayArray);
					}
					areaDateArray = quickSort(areaDateArray);
					//BK ==> this could have been done in the previous loop
					//format all Date/Time information per tab
					for (i = 0; i < areaDateArray.length; i++) {
						// console.log("areaDateArray = ",  areaDateArray[i]);
						areaDateTextArray[count] += i > 0 ? " AND " : " EFFECTIVE ";
						//var tempDate = new Date(areaDateArray[i].start);

						if (areaDateArray[i].start == "" || !areaDateArray[i].start) {
							areaDateTextArray[count] += "IMMEDIATELY ";

						} else {
							startTime = appUtils.formatDateToDTG(areaDateArray[i].start);
							areaDateTextArray[count] += startTime;
							areaDateTextArray[count] + " UTC";

							/*Formatting to local is slightly more problematic as the offset needs to
							  be taken into consideration. Only show if checkbox for local time is enabled*/
							// if (areasJSON[count].includeLocTime) {
							if ($this.get('includeLocTime') == 1) {
								var localDT = appUtils.formatDateToText2(areaDateArray[i].start, $this.get('effTimeZoneOffset'));

								if ( !localDT ) {
									localDT = ['0000', '12/31/69'];
								}

								areaDateTextArray[count] += " (" + localDT.join(' LOCAL ') + ")";
							}
						}

						if (areaDateArray[i].end == "" || !areaDateArray[i].end) {
							areaDateTextArray[count] += " UNTIL FURTHER NOTICE";
							if (option == 'schedule'){
								areaDateTextArray[count] += " " + dayText + " UTC";
							}

						} else {
							areaDateTextArray[count] += " UNTIL ";
							endTime = appUtils.formatDateToDTG(areaDateArray[i].end);
							areaDateTextArray[count] += endTime;
							areaDateTextArray[count] += dayText;
							if (option == 'schedule'){
								areaDateTextArray[count] += " ";
								areaDateTextArray[count] += (startTime && endTime) ? String(startTime).substring(6) + "-" +String(endTime).substring(6) : "";
							}
							areaDateTextArray[count] += " UTC";

							/*Formatting to local is slightly more problematic as the offset needs to
							  be taken into consideration. Only show if checkbox for local time is enabled*/
							// if (areasJSON[count].includeLocTime) {
							if ($this.get('includeLocTime') == 1) {
								var localDT = appUtils.formatDateToText2(areaDateArray[i].end, $this.get('expTimeZoneOffset'));

								if ( !localDT ) {
									localDT = ['0000', '12/31/69'];
								}

								areaDateTextArray[count] += " (" + localDT.join(' LOCAL ') + ")";
							}
						}
					}
					//Generate shape text per shape per area tab
					//loop through shape information (0-many)
					if ( areasJSON[count].vertices ) {
						//console.log("Area vertices for notam:", $this);
						//console.log("areasJSON[" +count+"].vertices", areasJSON[count].vertices);

						baseText = "";
						includesText = "",
						excludesText = "";
						//look only for base, add, and exclude vertices
						for (i = 0; i < areasJSON[count].vertices.length; i++) {
							//loop through formatted vertices information (0-many)
							//vertice information can be in "base", "subtract" or "add"
							// $this.set('vor', areasJSON[count].shape[i].VOR);

							$this.updateCurrentTfrVor(areasJSON[count].shape[i].OBJECTID, areasJSON[count].shape[i].VOR);

							var base = false;
							var thisVert = areasJSON[count].vertices[i];
							var thisVOR = areasJSON[count].vertices[i]
							areasJSON[count].shape.forEach(function(_el, _idx, _arr){
								if (Number(_el.OBJECTID) === Number(thisVert.objectID)){
									$this.set('vor', _el.VOR);
								}
							});
							var isCircle = String(thisVert.geomtype).toLowerCase() == "circle" ? true : false;
							var isBuffer = String(thisVert.geomtype).toLowerCase().indexOf("buffered") > -1 ? true : false;
							var isArc = String(thisVert.geomtype).toLowerCase() == "arc" ? true : false;
							//BK: frdOption has moved out of areas section and need to be tracked here
							var frdOption = $this.get("frdOption");
							//var shapeVor = thisVert['vor'] ? thisVert['vor'] : "Select VOR"; //BK 03/19/2016 ==> There is no 'vor' key in thisVert
							var shapeVor = areasJSON[count].shape[i].VOR ? areasJSON[count].shape[i].VOR : "Select VOR";
							//loop for base shape
							if (thisVert.base) {
								base = true;
								baseText += "<br/>WITHIN ";
								//you don't need the vertices when frdOption is set to 'component'
								if ($this.get('componentDefinition') === 'component') {
									//you can now get the vertices string from a single place
									// baseText += formatVertices(thisVert.base, isCircle, isBuffer, isArc, $this.get('includeFRD'), thisVert.dd, shapeVor);
									baseText += formatVertices(thisVert.base, isCircle, isBuffer, isArc, $this.get('includeFRD'), thisVert.dd, $this.get('vor'));


								} else {
									//user areaName instead of shapeName
									baseText += areasJSON[count].areaName;
								}
								//get area altitude info
								baseText += createAltitudeText(thisVert);
								//add replace variables for included/excluded areas
								baseText += " {INCLUDINGSHAPE}{EXCLUDINGSHAPE}";
								//add dates to area vertices
								baseText += areaDateTextArray[count];
							}

							/*	loop for Subtracted shape
							 *	This should only be added if there is a base shape
							 */
							if (thisVert.subtract) {
								excludesText += "; EXCLUDING ";

								if ($this.get('componentDefinition') === 'component') {
									excludesText += formatVertices(thisVert.subtract, isCircle, isBuffer, isArc, $this.get('includeFRD'), thisVert.dd, $this.get('vor'));

								} else {
									excludesText += areasJSON[count].areaName + " ";
								}

								excludesText += createAltitudeText(thisVert);
							}

							//loop for Added shape
							if (thisVert.add) {
								includesText += "; INCLUDING ";

								if ($this.get('componentDefinition') === 'component') {
									// includesText += formatVertices(thisVert.add, isCircle, isBuffer, isArc, $this.get('includeFRD'), thisVert.dd, shapeVor);
									includesText += formatVertices(thisVert.add, isCircle, isBuffer, isArc, $this.get('includeFRD'), thisVert.dd, $this.get('vor'));

								} else {
									includesText += areasJSON[count].areaName + " ";
								}

								includesText += createAltitudeText(thisVert);
							}

							//none shapes
							if (thisVert.none) {
								console.log("Shape type not defined:", thisVert);
							}
						}

						var regexAdd = new RegExp("{INCLUDINGSHAPE}", 'gi'),
							regexSubtract = new RegExp("{EXCLUDINGSHAPE}", 'gi');

						//append include and exclude vertices
						baseText = baseText.replace(regexAdd, includesText);
						baseText = baseText.replace(regexSubtract, excludesText);
					} else {
						console.log("There is no vertices for area: ", count, areasJSON);
					}
					// baseText from iteration
					//console.log("what is baseText:",baseText);
					//if allow authorization is checked, prepend the following:
					// console.log("count", count, "this", $this);
					if ($this.areas[count]["allowFlightAuth"]){
						areasText += " WITHIN THE FOLLOWING AREA(S) UNLESS OTHERWISE AUTHORIZED BY ATC";
						if($this.tfrTemplate.indexOf("SECURITY") > -1) {
							areasText += " IN CONSULTATION WITH THE DOMESTIC EVENTS NETWORK (DEN)";
						}
					}
					//append to area text
					areasText += baseText;

					count++;
				}
				// append instructions to NOTAM after areatext info
				areasText = ["<br/>", areasText, "<br/><br/>", instructions.text()].join("");
				// console.log("areasText post iteration:", areasText);
				//area and dates replaces portions
				replaceArray.push({variable : "{TFR AREA SECTION} {TFR EFFECTIVE TIMES SECTION}", value : areasText});
				//generate formatted date text
				formattedStartDate = startDate ? appUtils.formatDateToDTG(startDate) : "";
                formattedEndDate = endDate ? appUtils.formatDateToDTG(endDate) : "";
				//start and finish dates
				var dtDuration = formattedStartDate != "" && formattedEndDate != "" ? "<br/><br/>" + formattedStartDate + "-" + formattedEndDate : "";
				//calculate end date string format
				var endDateString;

				function padDateZero(date){
					return date < 10 ? '0' + date : date;
				}

				if (endDate){
					var diffMonths = new Date(startDate).getMonth() !== new Date(endDate).getMonth();
					var diffDays = new Date(startDate).getDate() !== new Date(endDate).getDate();
					if (diffMonths) {
						endDateString = monthArray[new Date(endDate).getMonth()] + ' ' + padDateZero(new Date(endDate).getDate());
					} else {						
						endDateString = padDateZero(new Date(endDate).getDate());
					}
					//format output
					if (diffDays){
						endDateString = '-' + endDateString + ',';
					} else {
						if (diffMonths){
							endDateString = '-' + endDateString + ',';
						} else {
							endDateString = '';
						}
					}
				} else {
					endDateString = " UNTIL FURTHER NOTICE";
				}
				//local date/time for VIP and SECURITY
				var formattedLocalStartDateTemp = endDate && startDate ? appUtils.formatDateToText(startDate).replace( ',', endDateString) : appUtils.formatDateToText(startDate) + endDateString;

				formattedLocalStartDate = startDate ? formattedLocalStartDateTemp : "";

				//Default NOTAM info
				replaceArray.push({variable : "{ARTCC}", value : artcc});
				replaceArray.push({variable : "{TERRITORY/STATE}", value: stateInfo});
				replaceArray.push({variable : "{CITY, STATE}", value: locationInfo});
				replaceArray.push({variable : "{EFFECTIVE LOCAL DATE}", value: "<br/>" + formattedLocalStartDate});
				replaceArray.push({variable : "{EARLIEST START DTG}-{LATEST END DTG}", value: dtDuration});

				if (tfrReason != "" & tfrReason != null) {
					if(tfrReason["name"]){
						replaceArray.push({variable : "{TFR REASON}", value : " WITHIN THE FOLLOWING AREA(S) UNLESS OTHERWISE AUTHORIZED BY ATC IN CONSULTATION WITH THE DOMESTIC EVENTS NETWORK (DEN) DUE TO " + (tfrReason["name"] == "NFL Super Bowl"? " THE " + tfrReason["name"] : tfrReason["name"])});
					} else {
						replaceArray.push({variable : "{TFR REASON}", value : " DUE TO " + tfrReason});
					}
				} else {
					replaceArray.push({variable : "{TFR REASON}", value : ""});
				}
				//Coordinating facility info
				//If Coordinating facility info is null, this template is not used.
				if (coordName != "" && coordName != null) {
					replaceArray.push({variable : "{COORDINATION FACILITY TEMPLATE}", value: "<br/><br/>THE {COORDINATION FACILITY NAME}/{COORDINATION FACILITY ID}/{COORDINATION FACILITY TYPE}{COORDINATION FACILITY PHONE}{COORDINATION FACILITY FREQUENCY} IS THE COORDINATION FACILITY."});
					replaceArray.push({variable : "{COORDINATION FACILITY NAME}", value: coordName});
					replaceArray.push({variable : "{COORDINATION FACILITY ID}", value: coordID});
					replaceArray.push({variable : "{COORDINATION FACILITY TYPE}", value: coordType});
					replaceArray.push({variable : "{COORDINATION FACILITY PHONE}", value: coordPhone? ", PHONE " + coordPhone : ""});
					replaceArray.push({variable : "{COORDINATION FACILITY FREQUENCY}", value: coordFreq? ", FREQ " + coordFreq : ""});
				} else {
					replaceArray.push({variable : "{COORDINATION FACILITY TEMPLATE}", value: ""});
				}

				//POC info
				//if PoC Organization is null, this template is not used.
				if (pocOrg != "" && pocOrg != null) {
					replaceArray.push({variable : "{POC TEMPLATE}", value: "<br/><br/>THE {POC ORGANIZATION}, {POC NAME}{POC PHONE}{POC FREQUENCY} IS IN CHARGE OF THE OPERATION."});
					replaceArray.push({variable : "{POC ORGANIZATION}", value: pocOrg});
					replaceArray.push({variable : "{POC NAME}", value: pocName});
					replaceArray.push({variable : "{POC PHONE}", value: pocPhone? ", PHONE " + pocPhone : ""});
					replaceArray.push({variable : "{POC FREQUENCY}", value: pocFreq? ", FREQ " + pocFreq : ""});
					// console.log("pocOrg", pocOrg, "pocName", pocName, "pocPhone", pocPhone, "pocFreq", pocFreq);
				} else {
					replaceArray.push({variable : "{POC TEMPLATE}", value: ""});
				}

				//get Tfr Template
				notamTxt = template + "<br/><br/>";

				for (var j = 0; j < replaceArray.length; j++) {
					var regexp = new RegExp(replaceArray[j].variable, 'gi');
					notamTxt = notamTxt.replace(regexp, replaceArray[j].value);
				}

				//notamTxt += instText;

				$this.set('tfrNotamText', notamTxt.toUpperCase());
				$this.set('notamTextLen', notamTxt.length);

			} else {
				var checkbox = $('#custom_text_checkbox');
				checkbox.attr("checked", true);
				checkbox.trigger("change");
			}
			console.log("done updating notam");
			//FB
			//$this.updateNewNotamText();

		},
		enableTfrEdit: function(e) {
			var $this = this,
				editor = $("#notamEditor").data("kendoEditor"),
				editorBody = $(editor.body),
			    notamText = $this.get('tfrNotamText');
			//console.log(editor);
			if($(e.target).is(":checked")){
				editorBody.add("td", editorBody).attr("contenteditable", true);
				$this.custom_text = 1;
				//editor.toolbar.remove();
				//console.log(editor.toolbar);
				//editor.toolbar.element[0].setAttribute("style", "z-index:1000 !important");
				//$('.k-editor-toolbar').hide();
			} else {
				var msg = "",
				options = {
					title: "Freeform NOTAM Text",
					message: "Disabling Freeform mode will reload the NOTAM template text.\nAre you sure you wish to proceed?",
					buttons: ['YES', 'NO']
				};
				$this.custom_text = 0;

				nisis.ui.displayConfirmation(options).then(function(resp){
					if(resp == options.buttons[0]){
						editorBody.add("td", editorBody).removeAttr("contenteditable");
						$this.updateTfrNotamText();
					}
				},
				function(error){
					msg = "App could not capture your answer. Please try again.";
					nisis.ui.displayMessage(msg, 'error');
				});
			}
			this.updateTfrNotamText;
		},
		//Insert variables from quick-entry form into NOTAM Text
		// replaceText: function(str){
		// 	var id = str.getAttribute("id"),
		// 	toReturn = "";
		// 	switch(id) {
		// 		case "NMR":
		// 			toReturn = "test";
		// 			break;
		// 		default:
		// 			toReturn = "X";
		// 	}
		// 	return toReturn;
		// },
		//remove toolbar from rendering
		hideTfrToolbar: function(e){
			$("#notamEditor").kendoEditor({
				tools: []
			});
			$('.k-editor-toolbar').hide();
		},
		addNewShape: function(e) {
         	var $this = this,
         		$tfrDiv = $("#tfr-div"),
	        	$drawDiv = $('#drawing-div'),
	         	$drawOpt = $('#draw-options-div'),
	            target = $(e.currentTarget);

         	$drawDiv.data('drawInitiatorBtn', target);
         	//JS-This is not needed in production. testing purposes.
         	//$tfrDiv.data('kendoWindow').minimize();
         	$drawDiv.data('kendoWindow').open();
			$drawOpt.data('kendoTabStrip').activateTab($('#draw-shapes'));
		},
		getCurrentTfrNotamId: function(e) {
			var currentTfrId = [];

			// var $this = this;

			// var getNotamId = $this.get('notam_id');

			// getNotamId;

			console.log("getNotamId:", notam_id);

			return currentTfrId;
		},
		deleteShape: function(e) {
			//console.log("Delete seleted shape");
			var oid=tfrShapesGrid.remove(e);

            if (oid) {
				//also remove the shape from the areas and vertices object
				var area = self.get('activeArea').split('-').pop(),
				shapes = self['areas'][area]['shape'],
				vertices = self['areas'][area]['vertices'];
				for (var i = 0; i < shapes.length; i++) {
					if (shapes[i].OBJECTID==oid) {
						//delete shapes and vertices
						shapes.splice(i, 1);
						vertices.splice(i, 1);
						break;
					}
				}
			}
		}
	});

	return self;

});
