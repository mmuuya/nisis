$(document).ready(function() {
		var usrValidator = $("#chPwd").kendoValidator().data("kendoValidator");

		var chPwdViewModel = kendo.observable({
		selected: {},
		changePassword: function(e) {
			e.preventDefault();
			$this = this;
            if (usrValidator.validate()) {
            	//Since appsMultiSelect cannot be bound to a value like 'selected.apps' directly from
            	// MVVM, so the value has to be set manually as following
            	$.ajax({
					url: "./admin/api/",
					type: "POST",
					data: {
						"action": "acl",
						"function": "chPwd",
						"password": $this.selected.password,
						"id": $this.selected.usrid,
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						var success = '<div class="password-success"><i class="fa fa-check-circle"></i>Password successfully changed!</div>';
						// console.log(resp);
						if(req.status === 200){
							console.log(resp.result);
							console.log(req.status);
							$('#validation').append($(success).css('display', 'flex').fadeIn('slow'));
							$('.password-success').delay(3500).fadeOut('slow');
							$('#password').val("");
						}
					},
					error: function(req, status, err) {
						console.error("ERROR");
					},
				});

            } else {
            	
            }
		}
	});
	kendo.bind($("#change-password"), chPwdViewModel);

});

