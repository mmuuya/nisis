

define([], function() {
	//TFR Web Text
	updateTfrWebText: function(e) {
		console.log('Web Text: ');
	},
	moveToSection: function(e) {
		var sect = e.target.dataset.target;

		$('.tfr-web-text').animate({
			scrollTop: $('#'+sect).offset().top 
		});
	},
	moveTopPage: function(e){
		$('.tfr-web-text').animate({
			scrollTop: $('#tfr-web-notam').offset().top 
		});
	}
});