/*
 * TFR Builder Status
*/
define([], function() {
	//status & publish tfr
	publishTfr: function(e) {
		
		$('#tfr-dialog').spin(true);
		var $this = this,
			notam = JSON.stringify(aixmInput),
			msg = 'Publishing an existing TFR';

			notam = JSON.stringify(notam);
			console.log('Notam: ', notam);
			$this.set('tfrMsg', msg);

		$.ajax({
			url : 'api/tfr/tfr-publish.php',
			type : 'POST',
			data : {notam: notam},
			success: function(data, status, xhr) {
				console.log("TFR Publish:", arguments);
				var resp = null;
				
				try {
					resp = JSON.parse(data);
					console.log("TFR Publish:", resp);
					$this.set('tfrMsg', resp.message);
				} catch (err) {
					console.log("TFR Publish:", err);
					$this.set('tfrMsg', "App could not inerpret the response.");
				}
				
				$('#tfr-dialog').spin(false);
			},
			error: function(xhr, status, err) {
				console.log("TFR Publish:", arguments);
				$this.set('tfrMsg', "App could not process your input: " + status);
				$('#tfr-dialog').spin(false);
			}
		});
	},
	tfrStatus: function() {
		var $this = this,
			sts = {
				'create': 'In Construction',
				'edit': 'In Re-Construction',
				'review': 'Under Review',
				'publish': 'Ready to be published'
			},
			mode = $this.get('tfrMode');

		return sts[mode];
	},
	tfrStatusExp: function() {
		var $this = this,
			stsExp = {
				'create': 'The TFR is In Construction which means that the TFR is currently being built.  Once the TFR contains valid data you can submit it for review.',
				'edit': 'The TFR has been Rejected which means that the TFR is currently being built.  Once the TFR contains valid data you can submit it for review.',
				'review': 'The TFR is under review.',
				'publish': 'The TFR is ready to be published.'
			},
			mode = $this.get('tfrMode');

		return stsExp[mode];
	},
	tfrFeedbacks: function() {
		var $this = this,
			fBacks = {
				'create': 'Once you have created the TFR and it is free from errors you can check the TFR into the repository and submit it for review.',
				'edit': 'Once you have edited the TFR and it is free from errors you can check the TFR into the repository and submit it for review.',
				'review': 'Once you have reviewed the TFR, submit it for publication.',
				'publish': 'You can now publish the TFR.'
			},
			err = 'The TFR contains errors. Correct the errors before submitting the TFR.',
			local = 'The TFR is a local TFR. Please add the TFR to the repository before it can be submitted.',
			mode = $this.get('tfrMode'),
			fBack = fBacks[mode];

		return fBack;
	},
	updateTfrMenu: function() {
		var $this = this,
			mode = $this.get('tfrMode'),
			menu = $('#tfr-menu').data('kendoMenu'),
			save = {
				text: "<i class='tfr-save fa fa-save'></i> Save",
				encoded: false
			},
			sLen = $('li.tfr-save-copy').length,
			publish = {
				text: "<i class='tfr-publish fa fa-share-square-o'></i> Publish",
				encoded: false
			},
			pLen = $('li.tfr-publish-copy').length;

		return;

		if (mode == '' && !sLen) {
			menu.enable('li.tfr-save', false);
			menu.enable('li.tfr-save-copy', false);
			menu.enable('li.tfr-save-xls', false);
			menu.enable('li.tfr-print-web', false);
			menu.enable('li.tfr-print-text', false);
		} else if (mode !== 'publish' && !sLen) {
			menu.insertAfter(save, "li.tfr-menu-start");
			$("li.tfr-menu-start").next()
			.addClass('tfr-save-copy')
			.bind('click', $this.saveTfr.bind($this));

			menu.enable('li.tfr-save', true);
			menu.enable('li.tfr-save-copy', true);
			menu.enable('li.tfr-save-xls', true);
			menu.enable('li.tfr-print-web', true);
			menu.enable('li.tfr-print-text', true);

		} else if(mode !== 'publish' && sLen) {
			menu.enable('li.tfr-save', true);
			menu.enable('li.tfr-save-copy', true);
			menu.enable('li.tfr-save-xls', true);
			menu.enable('li.tfr-print-web', true);
			menu.enable('li.tfr-print-text', true);
			if (pLen) {
				menu.enable('li.tfr-publish-copy', false);
			}

		} else if(mode !== 'publish' && pLen) {
			menu.enable('li.tfr-save', true);
			menu.enable('li.tfr-save-copy', true);
			menu.enable('li.tfr-save-xls', true);
			menu.enable('li.tfr-print-web', true);
			menu.enable('li.tfr-print-text', true);
			if (pLen) {
				menu.enable('li.tfr-publish-copy', false);
			}

		} else {
			menu.enable('li.tfr-save', true);
			menu.enable('li.tfr-save-copy', true);
			menu.enable('li.tfr-save-xls', true);
			menu.enable('li.tfr-print-web', true);
			menu.enable('li.tfr-print-text', true);
			if (pLen) {
				menu.enable('li.tfr-publish-copy', false);
			}
		}

		if (mode === 'publish' && !pLen) {
			menu.insertBefore(publish, "li.tfr-exit");
			$("li.tfr-exit").prev()
			.addClass('tfr-publish-copy')
			.bind('click', $this.publishTfr.bind($this));

			menu.enable('li.tfr-save', false);
			menu.enable('li.tfr-save-copy', false);
			menu.enable('li.tfr-save-xls', false);
			
		} else if (mode === 'publish' && pLen) {
			menu.enable('li.tfr-publish-copy', true);

			menu.enable('li.tfr-save', false);
			menu.enable('li.tfr-save-copy', false);
			menu.enable('li.tfr-save-xls', false);
		}
	},
	checkWorkflow: function() {
		var resp = false,
			mode = this.get('tfrMode');

		if (mode !== 'publish') {
			resp = true;
		}

		return resp;
	},
	workflowLabel: function(e) {
		var $this = this,
			labels = {
				'create': 'Review',
				'edit': 'Review',
				'review': 'Publication',
				'publish': 'Transmission'
			},
			mode = $this.get('tfrMode'),
			label = "Review";

		if (labels[mode]) {
			label = labels[mode];
		}

		return label;
	},
	submitTfr: function (e) {
		var $this = this,
			modes = this.get('tfrModes').toJSON(),
			mode = this.get('tfrMode'),
			mIdx = modes.indexOf(mode),
			nMode = modes[mIdx + 1];

			//skip Re-Construction
			if (mIdx === 0) {
				nMode = modes[mIdx + 2];
			}

		$this.set('tfrMode', nMode);
		//disable publish menu if not ready
		$this.checkPublish();
	},
	checkRecycle: function() {
		var resp = false,
			mode = this.get('tfrMode'),
			vModes = ['review', 'publish'];

		if ($.inArray(mode, vModes) !== -1) {
			resp = true;
		}

		return resp;
	},
	rejectTfr: function(e) {
		var $this = this,
			modes = $this.get('tfrModes').toJSON(),
			mode = $this.get('tfrMode'),
			mIdx = modes.indexOf(mode),
			nMode = modes[mIdx -1];

		$this.set('tfrMode', nMode);

		$this.checkPublish();
	},
	checkPublish: function() {
		var $this = this,
			mode = $this.get('tfrMode'),
			menu = $('#tfr-menu').data('kendoMenu');

		if (mode === 'publish') {
			menu.enable('li.tfr-publish', true);
		} else {
			menu.enable('li.tfr-publish', false);
		}

		$this.updateTfrMenu();
	},
	tfrHisCmts: "",
	showTfrHistory: function (e) {
		var hist = "TFR History: ....";

		this.set('tfrHisCmts', hist);
	},
	getTfrComments: function (e) {
		var cmts = "TFR Comments: ....";

		this.set('tfrHisCmts', cmts);
	},
	userComments: "",
	validComments: false,
	checkCmtInput: function (e) {
		var cmts = this.get('userComments'),
			valid = true;

		if ($.trim(cmts) === "" || $.trim(cmts).length < 10) {
			valid = false;
		}

		this.set('validComments', valid);
	},
	addTfrComments: function (e) {
		
	},
	clearTfrComments: function (s) {
		this.set('userComments', '');
	}
});