/*
 * IWL window ==> Manage IWLs
*/
define(["app/windows/tableview"],function(tableview){
	return kendo.observable({
		resizeFilterTable: function(e){
			//console.log(e);
			var h = e.height - 100 + 'px';
			//$('twl-talbe').css('height', h)
		},
		//mapview object
		mapview: nisis.ui.mapview,
		loadLabel: "Load",
		canLoadIWL: true,
		//create iwls
		createLabel: 'Create',
		canCreateIWL: true,
		isCreateMode: false,
		iwlName: "",
		iwlDescription: "",
		iwlMsg: "",
		iwlMsgCol: "inherit",
		saveLabel: 'Save',
		//edit iwls
		canEdit: false,
		editLabel: "Edit",
		iwlSelected: false,
		selectedIWLs: null,
		numIWLObjects: 0,
		iwlObjects: null,
		iwlObjectsListed: false,
		iwlColumns: [{
			field: 'ID',
			title: 'ID',
			width: 60
		},{
			field: 'NAME',
			title: 'NAME',
			width: 150
		},{
			field: 'DESCRIPTION',
			title: 'DESCRIPTION',
			width: 200
		},{
			field: 'CREATED_USER',
			title: 'CREATED BY',
			width: 150
		},{
			field: 'CREATED_DATE',
			title: 'CREATION DATE',
			width: 150
		},{
			field: 'LAST_UPDATED_USER',
			title: 'LAST UPDATED USER',
			width: 200
		},{
			field: 'LAST_UPDATED_DATE',
			title: 'LAST UPDATED DATE',
			width: 200
		}],
		iwlSource: new kendo.data.DataSource({
			data: [],
			/*transport: {
				read: function(options){
					console.log(options);
					$.ajax({
						url: "api/",
						success: function(results){
							console.log(results);
							options.success(results);
						},
						error: function(results){
							console.log(results);
							options.error(results);
						}
					});
				},
				parameterMap: function(options, operation){
					console.log(arguments);
					if(operation === 'read'){
						return {
							action: 'get_iwls',
							username: nisis.user.username,
							usergroups: nisis.user.groups
						}
					}
				}
			},*/
			pageSize: 10,
		}),
		loadIWLs: function(e) {
			console.log('Loading IWLs ...');
			var $this = this;
			$this.set('canLoadIWL', false);
			$this.set('loadLabel', 'Loading');
			//get list of iwls
			$.ajax({
				url: "api/",
				get: "GET",
				data: {
					action: 'get_iwls',
					username: nisis.user.username,
					usergroups: nisis.user.groups
				},
				success: function(data, status, xhr){
					//console.log('IWLs: ', data);
					var resp;
					try {
						resp = JSON.parse(data);
					}
					catch(err){
						nisis.ui.displayMessage(err.message, '', 'error');
					}
					console.log('IWLs: ',resp.results.length);
					if(resp && resp.results.length > 0){
						$this.iwlSource.data(resp.results);
						$this.buildIWLTable();
					} else {
						nisis.ui.displayMessage('There is no IWL for ' + nisis.user.username, '', 'error');
						$this.set('canLoadIWL', true);
						$this.set('loadLabel', 'Load');
					}
				},
				error: function(xhr, status, err){
					console.log(err);
					nisis.ui.displayMessage(err.message, '', 'error');
					$this.set('canLoadIWL', true);
					$this.set('loadLabel', 'Load');
				}
			});
		},
		buildIWLTable: function(){
			//console.log('Building IWL Table: ');
			var $this = this,
				iwlTbl = $('#iwl-table'),
				payload = $this.get('iwlSource').data();

			if(iwlTbl.data('kendoGrid')){
				iwlTbl.data('kendoGrid').refresh();
			} else {

				if(payload.length >= 50){
					var dTitle = "Loading IWLs",
				        box = $('<div/>', {"class": "profile-spin"}),
				        spinner = $("<i/>", {"class": "fa fa-cog fa-spin fa-4x"});

				    //remove kendo styles
				    spinner.appendTo(box);

				    //append dialog to app content
				    $('#app-content').append(box);

				    //kendo dialog
				    var loadModal = box.kendoWindow({
				                    actions: ["Close"],
				                    title: dTitle,
				                    // visible: false,
				                    visible: true,
				                    modal: true,
				                    width: '300',
				                    height: '80',
				                    animation: {
				                        open: {
				                            effects: "fade:in"
				                        },
				                        close: {
				                            effects: "fade:out"
				                        }
				                    }
				    }).data('kendoWindow').center();
				}



				//NISIS-3057: so that we can sort by ID column
				var iwlData = $this.get('iwlSource');
				iwlData._data.forEach(function(datum){
					datum.ID = Number(datum.ID);
				})


				iwlTbl.kendoGrid({
					columns: $this.get('iwlColumns'),
					// dataSource: $this.get('iwlSource'),

					dataSource: iwlData,

					sortable: {
	                    mode: 'multiple'
	                },
					filterable: true,
	                columnMenu: true,
	                resizable: true,
	                reorderable: true,
	                selectable: "multiple row",
	                pageable: {
	                    pageSizes: [10,20,30,40],
	                    pageSize: 10,
	                    buttonCount: 5
	                },
	                change: function(e){
	                	$this.getSelectedIWL(e);
	                },
					dataBound: function(e){
						if(loadModal){
							loadModal.close();
						}
					}
				});
			}
			$this.set('canLoadIWL',true);
			$this.set('loadLabel', 'Reload');
			//center the window
			$('#iwl-div').data('kendoWindow').center();
		},
		getSelectedIWL: function(e){
			var $this = this,
				iwls = [],
				n = 0,
				oids = [],
				iwlLayers = [];

			$.each(e.sender.select(),function(i,r){
				var iwl = e.sender.dataItem($(r));
				iwls.push(iwl);
				//console.log(iwl);
				// var ids = $.map(iwl.OIDS.split(','),function(id){
				// 	return parseInt(id);
				// });
				var ids;
				$.ajax({
                    //get the oids, make it not async, and get it here
                    async: false,
                    url: "api/",
                    type: "POST",
                    data: {
                        action: "get_iwl_objectids",
                        iwl_id: iwl.ID,
                    },
                    success: function(data, status, xhr){
                        var resp = JSON.parse(data);
                        ids = resp.results;
                    },
                    error: function(xhr, status, err){
                        console.log("ERROR getting object ids for iwl # " + iwl.id);
                        console.error(xhr);
                        console.error(status);
                        console.error(err);
                    }
                });
				n += ids.length;
				//keep iwl objects of each layer together
				var pos = $.inArray(iwl.LAYER, iwlLayers);
				if(pos == -1){
					iwlLayers.push(iwl.LAYER);
					oids.push({
						layer: iwl.LAYER,
						oids: ids
					});
				} else {
					oids[pos].oids = oids[pos].oids.concat(ids);
				}
			});

			this.set('selectedIWLs', iwls);
			this.set('iwlObjects', oids);
			this.set('iwlSelected',true);
			this.set('numIWLObjects', n);
			if(iwls.length === 1){
				var edit = false;
				//limit edit to user or admins
				if(nisis.user.username === iwls[0].CREATED_USER){
					edit = true;
				} else if ($.inArray(0, nisis.user.groups) !== -1 /*&& $.inArray(0, iwls[0].USER_GROUP.split(',')) !== -1*/) {
					edit = true;
				}
				this.set("canEdit",edit);
			} else {
				this.set("canEdit",false);
			}
		},
		getSelectedIWLObjects: function(){
			var oids = [],
				iwl = this.get('selectedIWLs');

			if(iwl){
				var ids = iwl.OIDS.split(',');
				$.each(ids,function(i,id){
					oids.push(parseInt(id));
				});
				return oids;
			} else {
				return oids;
			}
		},
		isIWLCorrect: function(){
			var $this = this,
				name = $.trim($this.get('iwlName')),
				desc = $.trim($this.get('iwlDescription')),
				tbl = $('#table').data('kendoGrid');

			var save = true;

			if(name === "") {
				save = false;
			}
			else if (desc === "") {
				save = false;
			}
			else if(!tbl) {
				$this.set('iwlMsg','There is no data in the Table View');
				$this.set('iwlMsgCol','inherit');
			}

			return save;
		},
		createIWL: function(e){
			console.log('Create IWL: ', e);
			var $this = this;
			$this.set('isCreateMode', true);
		},
		cancelIWL: function(){
			var $this = this;

			$this.set('iwlName','');
			$this.set('iwlDescription','');
			$this.set('isCreateMode',false);

			$this.set('iwlMsg','');
			$this.set('iwlMsgCol','inherit');
			$this.set('saveLabel','Save');
		},
		saveIWL: function(e){
			var $this = this,
				name = $this.get('iwlName'),
				desc = $this.get('iwlDescription');

			var tbl = $('#table').data('kendoGrid');
			if(!tbl){
				$this.set('iwlMsg','There is no data in the Table View');
				$this.set('iwlMsgCol','red');
				return;
			}

			$this.set('iwlMsg','');
			$this.set('iwlMsgCol','inherit');
			$this.set('saveLabel','Saving ...');

			var ids = tableview.getIWLFeatures();

			// NISIS-2946
			// FB auto-load IWLs on save
			$this.insertIWLs(ids).then(function(d){
				$this.loadIWLs(e);
				console.log(">>>reloading IWLs<<<", d);
			}, function(e){
				console.log(">>>failed IWL reload<<<", e);
			});
		},
		insertIWLs: function(ids){
			var $this = this;
			if(!ids){
				return;
			} else if (ids && ids.length && ids.length === 0){
				$this.set('iwlMsg','Can not save an empty IWL');
				$this.set('iwlMsgCol','red');
			}

            var	layer = $('#tbl-lyrs').data('kendoDropDownList').value(),
            	name = $this.get('iwlName'),
            	desc = $this.get('iwlDescription');
            //save iwls
            nisis.ui.createIWL(layer,name,desc,ids)
            .then(function(d){
                console.log(d);
                $('#tbl-lyrs').data('kendoDropDownList')
                .dataSource.add({
                    name: name + " (IWL)",
                    value: layer,
                    iwl: true
                });

                $this.set('iwlMsg', name + ' has been saved.');
                $this.set('iwlMsgCol','green');

                setTimeout(function(){
                    $this.cancelIWL();
                },500);

            },function(e){
                console.log('Error Saving IWL: ', e);
                $this.set('iwlMsg', e);
                $this.set('iwlMsgCol','red');
            });

			$def = new $.Deferred();
			$def.resolve(true).promise();
			return $def;
        },
		editIWLs: function(e){
			var $this = this,
			iwl = this.get('selectedIWLs')[0];
			//console.log('Selected IWL: ', iwl);

			var editor = $('<div class="iwl-editor"></div>');
			$('#content').append(editor);

			editor.kendoWindow({
				visible: false,
				title: 'Incident Watch List Editor (' + iwl.NAME + ')',
				actions: ["Minimize","Maximize","Close"],
				modal: true,
				width: '600',
				height: 'auto',
				maxHeight: "100%"
				,content: {
					url: "html/windows/iwlEditor.php"
				},
				refresh: function(e) {
					//console.log('Refesh: ', e);
					e.sender.center().open();
					initEditContent();
				},
				close: function(e){
					e.sender.destroy();
				}
			});

			var w = editor.data('kendoWindow'),
				grid = null;
			//return;
			//w.bind('refresh', function(e) {
			function initEditContent() {
				grid = $(editor.find('#iwl-feats-list')).text('Loading content ...');
				//$this.getIwlFeatures(iwl.ID, iwl.LAYER, this.element[0]);
				$this.getIwlFeatures(iwl.ID, iwl.LAYER, editor);
				//bind iwl name
				var name = $(editor.find('.iwl-name')[0]);
				name.val(iwl.NAME);
				//bind iwl desc
				var desc = $(editor.find('.iwl-desc')[0]);
				desc.val(iwl.DESCRIPTION);
				var groups;

				var grps = editor.find('.iwl-groups').kendoMultiSelect({
					placeholder: 'Select groups'
					,value: ""
				}).data('kendoMultiSelect');

				$.ajax({
					async: false,
					url: "api/",
					type: "POST",
					data: {
						action: "get_iwl_groups",
						iwl_id: iwl.ID
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						if(resp.error){
							nisis.ui.displayMessage('No group data for this IWL', 'error');
							grps.value([]);

						} else {
							groups = resp.groups;
							grps.value(groups);
						}
					},
					error: function(req, status, err){
						console.log("ERROR getting iwl groups: ", arguments);
					},
				});

				//save changes
				var save = $(editor.find('.iwl-save')[0]);
				save.click(function(evt){
					if($.trim(name.val()) === ""){
						nisis.ui.displayMessage('Name is required', '', 'error');
						return;
					} else if ($.trim(name.val()) === iwl.NAME &&
						//TODO - IWL USER GROUP
							$.trim(desc.val()) === iwl.DESCRIPTION &&
							grps.value().join(',') === $.trim(iwl.USER_GROUP)) {
						nisis.ui.displayMessage('There are no changes to be saved.', '', 'error');
						return;
					}
					//console.log(iwl.ID,name.val(),desc.val(),grps.value().join(","));
					save.html('Saving ...');
					//get confirmation
					nisis.ui.displayConfirmation({
						message: 'Are you sure you want to save your changes?',
						buttons: ["YES", "NO"]
					}).then(function(response){
						if(response == "NO" || response === 'close'){
							name.val(iwl.NAME);
							desc.val(iwl.DESCRIPTION);
							//TODO - IWL USER GROUP
							var g = iwl.USER_GROUP ? iwl.USER_GROUP.split(",") : "";
							grps.value(g);
							save.html('Save');
						} else {
							//save changes
							$.ajax({
								url: 'api/',
								type: 'POST',
								data: {
									action: 'edit_iwls',
									task: 'update',
									iwl_id: iwl.ID,
									name: name.val(),
									desc: desc.val(),
									groups: grps.value().join(',')
								},
								success: function(response){
									console.log(JSON.parse(response));
									var resp = JSON.parse(response);
									if(resp['return'] && resp['return'] === 'Success'){
										iwl.NAME = name.val();
										iwl.DESCRIPTION = desc.val();
										//TODO - IWL USER GROUP
										iwl.USER_GROUP = grps.value().join(',');
										save.html('Save');
										$('#load-iwls').trigger('click');
										nisis.ui.displayMessage('Your edits were successfully saved.', '', 'info');
									} else {
										save.html('Save');
										nisis.ui.displayMessage(resp.result, '', 'error');
									}
								},
								error: function(error){
									console.log(error);
									save.html('Save');
									nisis.ui.displayMessage('Your edits were not successfully saved.', '', 'error');
								}
							});
						}
					},function(error){
						nisis.ui.displayMessage('The app could not display confirmation dialog.', '', 'error');
					});
				});
				//delelte iwl
				var del = $(editor.find('.iwl-delete')[0]);
				del.click(function(evt){
					del.html('Deleting ...');

					nisis.ui.displayConfirmation({
						message: 'Are you sure you want to delete this IWL?',
						buttons: ["YES", "NO"]
					}).then(function(response){
						if(response === 'YES'){
							//delete iwl
							$.ajax({
								url: 'api/',
								type: 'POST',
								data: {
									action: 'edit_iwls',
									task: 'delete',
									iwl_id: iwl.ID
								},
								success: function(response){
									//console.log(JSON.parse(response));
									var resp = JSON.parse(response);
									if(resp['return'] && resp['return'] === 'Success'){
										w.close();
										$('#load-iwls').trigger('click');
										nisis.ui.displayMessage('Your IWL was successfully deleted.', '', 'info');
									} else {
										nisis.ui.displayMessage(resp.result, '', 'error');
									}
									del.html('Delete IWL');
								},
								error: function(error){
									console.log(error);
									del.html('Delete IWL');
									nisis.ui.displayMessage('Your IWL was  not successfully deleted.', '', 'error');
								}
							});
						} else {
							del.html('Delete IWL');
						}
					},function(error){
						nisis.ui.displayMessage('The app could not display confirmation dialog.', '', 'error');
					});
				});
				//remove features
				var remove = $(editor.find('.iwl-remove')[0]);
				remove.click(function(evt){
					remove.html('Removing Features ...');
					var oids,
						feats = $this.get('iwlSelectedFeats');

					if(feats && feats.length && feats.length > 0){
						oids = $.map(feats,function(feat){
							return parseInt(feat);
						});
					} else {
						remove.html('Remove Feature(s)');
						nisis.ui.displayMessage('There is no feature selected in this IWL', '', 'error');
						return;
					}

					nisis.ui.displayConfirmation({
						message: 'Are you sure you want to remove these features from this IWL?',
						buttons: ["YES", "NO"]
					}).then(function(response){
						if(response === 'YES'){
							//save changes
							$.ajax({
								url: 'api/',
								type: 'POST',
								data: {
									action: 'edit_iwls',
									task: 'remove',
									iwl_id: iwl.ID,
									oids: oids
								},
								success: function(response){
									var resp = JSON.parse(response);

									if(resp['return'] && resp['return'] === 'Success'){
										nisis.ui.displayMessage('(' + oids.join(',') + ' successfully removed.', '', 'info');

										//$this.getIwlFeatures(iwl.ID, iwl.LAYER);
										var iwls = $('div.iwl-feats-list').data('kendoGrid'),
											ds = iwls.dataSource,
											items = iwls.select();
										//remove selected items
										if (items && items.length) {
											$.each(items, function(i, item) {
												iwls.removeRow(item);
											});
										}
										//redraw grid
										iwls.refresh();
										//update count
										var n = $this.get('numIWLObjects');
										n = parseInt(n);
										n -= oids.length;
										$this.set('numIWLObjects', n);

									} else {
										nisis.ui.displayMessage(resp.result, '', 'error');
									}
									remove.html('Remove Feature(s)');
								},
								error: function(error){
									remove.html('Remove Feature(s)');
									nisis.ui.displayMessage('Your IWL was  not successfully deleted.', '', 'error');
								}
							});
						} else {
							remove.html('Remove Feature(s)');
						}
					}, function(){
						nisis.ui.displayMessage('The app could not display confirmation dialog.', '', 'error');
					});
				});
				//cancel
				var cancel = $(editor.find('.iwl-cancel')[0]);
				cancel.click(function(evt){
					name.val(iwl.NAME);
					desc.val(iwl.DESCRIPTION);
					//TODO - IWL USER GROUP
					var g = $.map($.trim(iwl.USER_GROUP).split(","),function(gp){
						return gp;
					});
					grps.value(g);
				});
			//});
			};
		},
		getIwlFeatures: function(id, layer, editor) {
			//NISIS-2754 - KOFI HONU
			//var $this = this, grid, action, NISISTrackedObjectsLayers=["airports", "atm", "ans", "osf", "teams"];
			var $this = this, grid, action, NISISTrackedObjectsLayers=["airports", "atm", "ans", "osf", "tof"];
			if(editor){
				grid = $(editor).find('div.iwl-feats-list')[0];
			}
			//if there's no grid stop here
			if( !grid ) return;
			//get iwl features
			if ($.inArray(layer, NISISTrackedObjectsLayers) == -1) {
				action = 'get_iwl_dot_features';
			}
			else {
				action = 'get_iwl_features';
			}

			$.ajax({
					url: "api/",
					type: 'GET',
					data: {
						action: action,
						layer: layer,
						iwl_id: id
					},
					success: function(resp){
						var response = JSON.parse(resp), data = [], columns =  [];
						//check for success
						if(response['return'] === 'Success' && response.results && response.results.length>0) {
							if (action == 'get_iwl_dot_features') {
								var iwl = JSON.parse(response.results[0].IWL);
								data = iwl.IWL.DATA;
								iwlFeatsColumns = iwl.IWL.METADATA;

								//create iwlFeatsColumns for DOT layer objects, hide object id column
								iwlFeatsColumns.forEach(function(columnsMetadata){
								   	columns.push({field:columnsMetadata["COL_NM"],title:columnsMetadata["FIELD_DISPLAY_NAME"], hidden:(columnsMetadata["COL_NM"]=="OBJECTID")});
	    						});
							}
							else {//IWL contains NISIS tracked objects; columns are hardcoded and the same for all NISIS Tracked objects for an iwl grid
								data = response.results;
								columns = $this.get('iwlFeatsColumns');
							}
							//build grid
							$this.buildIwlFeatTable(grid, data, columns);
						} else {
							nisis.ui.displayMessage(response.message, '', 'error');
						}
					},
					error: function(error){
						console.log(error);
						nisis.ui.displayMessage(error.message, '', 'error');
					}
				});
			//}
		},
		buildIwlFeatTable: function(grid, data, columns) {
			var $this = this;

			//build features grid
			$(grid).kendoGrid({
				columns: columns,
				dataSource: data ? data : [] ,
				height: "500",
				sortable: {
                    mode: 'multiple'
                },
				filterable: true,
                columnMenu: true,
                resizable: true,
                reorderable: true,
                selectable: "multiple row"
                //,pageable: true
                ,pageable: {
                    pageSizes: [10,25,50,100]
                    ,pageSize: 10
                    ,buttonCount: 5
                },
                change: function(e){
                	//console.log('IWL Features Changed: ',e);
                	var feats = e.sender.select(),
                	ids = $.map(feats, function(feat,idx){
                		return e.sender.dataItem($(feat)).OBJECTID;
                	});
                	//update selected feats var
                	$this.set('iwlSelectedFeats', ids);
                }
			});
			//force grid updates
			$(grid).data('kendoGrid').dataSource.fetch();
			$(grid).data('kendoGrid').pager.page(1);
			$(grid).data('kendoGrid').refresh();
			//recenter the parent window
			$('.iwl-editor').data('kendoWindow').center();
		},
		iwlFeatsColumns: [
			{
				field: 'OBJECTID',
				title: 'OID',
				width: 60
				,hidden: true
			},{
				field: 'FAC_ID',
				title: 'FEATURE ID',
				width: 120
			},{
				field: 'TYPE',
				title: 'TYPE',
				width: 120
			},{
				field: 'NAME',
				title: 'NAME',
				width: 200
			},{
				field: 'CITY',
				title: 'CITY',
				width: 100
			},{
				field: 'COUNTY',
				title: 'COUNTY',
				width: 100
			},{
				field: 'STATE',
				title: 'STATE',
				width: 100
			}
		],
		iwlFeatsSource: new kendo.data.DataSource({
			data: []
		}),
		iwlSelectedFeats: [],
		loadIWLFeatures: function(e){
			console.log('Loading IWL Features: ', e);
		},
		updateLayers: function(e){
			var layer = this.get("iwlObjects")[0].layer;
			//console.log('Updating Map Layers: ', layer);

			var $this = this,
				//display = tableview.updateViewOption,
				display = tableview.viewOption;

			var btns = ['Hide', 'De-emphasize'];

			if(!display || display == ""){
				nisis.ui.displayConfirmation({
					title: 'Layer updates',
					message: "How would you like to display your layer? 'Hide' or 'De-emphasize' features?",
					buttons: btns
				}).then(function(display){
					console.log(display);
					if($.inArray(display, btns) !== -1){
						$this.applyUpdateToLayer(display);
					}
				},function(e){
					console.log('App could not retreive display options: ', e);
				});
			} else {
				$this.applyUpdateToLayer(display);
			}
		},
		applyUpdateToLayer: function(display){
			var $this = this,
				layers = [];
				//iwl = this.get('selectedIWLs'),
				//lyrId = iwl['LAYER'];
			//var oids = this.getSelectedIWLObjects(lyrId);
			var req = null;
			//console.log(iwl, oids, display);
			$.each($this.get("iwlObjects"),function(i,iwl){
				if(!iwl || iwl.oids.length === 0){
					var msg = 'The application could not apply your request. Try again.';
					nisis.ui.displayMessage(msg, '', 'error');
				} else {
					var lyr = iwl.layer;
					//clear existing updates
					tableview.resetDefaultView(null, lyr);

					//check for splited layers
					if ( lyr === 'airports' ) {
		                layers.push('airports_pu');
		                layers.push('airports_pr');
		                layers.push('airports_others');
		                layers.push('airports_military');
		            }
		            else if ( lyr === 'ans' ) {
		                layers.push('ans1');
		                layers.push('ans2');
		            } else if (lyr === 'all_objects') {
		                layers.push('airports_pu');
		                layers.push('airports_pr');
		                layers.push('airports_others');
		                layers.push('airports_military');
		                layers.push('atm');
		                layers.push('ans1');
		                layers.push('ans2');
		                layers.push('osf');
		                //NISIS-2754 KOFI HONU
		                layers.push('tof');
		               // layers.push('teams');
		            } else  {
		                layers.push(lyr);
		            }

		            var layerTree = $('#overlays-toggle').data('kendoTreeView');
		            $.each(layers, function(i, id){
		            	var l = nisis.ui.mapview.layers[id];
		                if( !l ) {
		                    var msg = 'App could not identify layer on the map.';
		                    nisis.ui.displayMessage(msg,'','error');
		                    return;
		                }

		            	var node = layerTree.dataSource.get(id);
                        if(node == null){
                            console.log('toggleLayers: unable to query from Map Layer Tree View');
                            return;
                        }

						//load layer on if its not already on
						if(node.checked == false){
	                            //select the node from the tree and load the layer
	                            layerTree.findByUid(node.uid);
	                            var selectitem = layerTree.findByUid(node.uid);
	                            selectitem.checked = true;
	                            layerTree.select(selectitem);//that will load the layer
	                            require(["app/windows/layers"], function(layers){
	                               layers.updateTreeNode(node.id,true);
	                            });
						}
						//apply view option
		                if(display.toLowerCase() === 'hide'){
			                //var def = "OBJECTID = " + iwl.oids.join(' OR OBJECTID = ');
			                var def = "OBJECTID IN (" + iwl.oids.join(',') + ")";
		            		if(l.query && l.query != ""){
	                            def += " AND (" + l.query + ")";
	                        }
			                l.setDefinitionExpression(def);
			            } else {
			                //store the update-end handler and use pointer to remove it
			                tableview.layerUpdates[l.id] = l.on('update-end',function(evt){
			                	tableview.deemphasizeFeatures(evt, iwl.oids);
			                });
			                //force layer to redraw
			                l.redraw();
			            }
			        });
		            //reset the display option if you are comming from the main menu
		            tableview.updateViewOption = "";
		            tableview.viewOption = "";
		            //collapse all windows
		            nisis.ui.collapseWindows();
				}
			});
		},
		clearIWLFeatures: function(e){
			var lyr = this.get('selectedIWLs')['LAYER'];
			tableview.resetDefaultView(lyr);
		}
	});
});
