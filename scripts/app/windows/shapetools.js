/**
 * app shape tools
 */
define(["app/states","esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol","esri/symbols/SimpleFillSymbol"
    ,"dojo/_base/Color","app/symbols/pushpins", "app/renderers"
    ,"app/geomUtils"
    , "libs/geo", "libs/latlong"]

,function(states,SimpleMarkerSymbol
    ,SimpleLineSymbol,SimpleFillSymbol
    ,Color,pushpins,renderers
    ,geomUtils
    ,Geo,LatLon){

    return kendo.observable({
        mapview: nisis.ui.mapview,
        app: 'nisis',
        nisisShapes: true,
        addToUserFeatures: false,
        // addToResponseTeams: false,
        featureTool: null,
        featureSymbol: null,
        featureSymbolValue: 0,
        //for cut and download
        featureOpsActive: false,
        featureOpsTool: null, // 'cut' or 'extract'
        dotObjectId: null,
        activateDrawing: function(e) {
            //console.log("Activate Drawing: ", e);
            var $this = this,
                target = e.currentTarget,
                tool = e.currentTarget.dataset.tool,
                value = e.currentTarget.dataset.value,
                symbol = e.currentTarget.dataset.symbol;

            if (!value) {
                value = e.currentTarget.dataset.ops;
            }

            if (e.currentTarget.dataset.dotobjectid) {
                $this.set('dotObjectId', e.currentTarget.dataset.dotobjectid);
            }


            var active = $(target).hasClass('nisis-item-active');

            if(active){

                $this.set('addToUserFeatures', false);
                // $this.set('addToResponseTeams', false);

                $this.set('featureTool', null);
                $this.set('featureSymbol', null);
                $this.set('featureSymbolValue', 0);

                if ( $this.get('featureOpsActive') ) {
                    $this.set('featureOpsActive', false);
                }

                nisis.ui.mapview.util.deactivateDraw();
                $(target).removeClass('nisis-item-active');

            } else {

                var n = $('.nisis-item-active').length;

                if(n > 0) {
                    //deactivate previous tools
                    $('.nisis-item').each(function(){
                        if($(this).hasClass('nisis-item-active')){
                            $(this).removeClass('nisis-item-active');
                        }
                    });

                    nisis.ui.mapview.util.deactivateDraw();
                }

                //activate current tool
                /*
                if(symbol === 'teams'){
                    $this.set('addToResponseTeams', true);

                } else
                */
                if (symbol === 'cut-extract') {
                    symbol = $this.get('featureOpsTool');
                    //console.log('Draw Tool: ', symbol);
                } else {
                    $this.set('addToUserFeatures', true);
                }

                var sym = pushpins[symbol] ? pushpins[symbol].symbol() : null;

                $this.set('featureTool', tool);
                $this.set('featureSymbol', sym);
                $this.set('featureSymbolValue', value);

                $(target).addClass('nisis-item-active');
                nisis.ui.mapview.util.activateDraw(tool);
            }
        },
        showDescription: function(e){
            //console.log('Show Tool Description: ', e);
            e.preventDefault();
            var el = $(e.currentTarget),
                content = el.attr('title');
            //build tooltip only once
            if(el.data('kendoTooltip')){
                $(e.sender).show();
            } else {
                //custom tooltip
                el.kendoTooltip({
                    content: content,
                    position: 'bottom'
                }).data('kendoTooltip').show();
            }
        },
        hideDescription: function(e){
            //console.log('Hide Tool Description: ', e);
            $(e.sender).hide();
        },
        drawOption: null,
        selectDrawOption: function(e){
            var option = $(e.sender.wrapper);
            option.addClass('k-state-active');
            this.set('drawOption', option.find('input').val());
            option.siblings().removeClass('k-state-active');
            var target = option.data('target');
            $('#'+target).show().siblings().hide();
            //templatePicker needs to be updated
            if(this.get('drawOption') === 'templates') {
                nisis.ui.mapview.templatePicker.update();
            }
        },
        //drawing tools
        shapes: [{label:'Select a shape', value:''},{label: 'Point', value: 'point'},
                {label:'Polyline', value:'polyline'},{label: 'Freehand Polyline',value: 'freehand_polyline'},
                {label: 'Polygon',value: 'polygon'},{label: 'Freehand Polygon',value: 'freehand_polygon'},
                {label: 'Rectangle',value: 'rectangle'},{label: 'Triangle',value: 'triangle'},
                {label: 'Circle',value: 'circle'},{label: 'Ellipse',value: 'ellipse'},
                {label: 'Text Label',value: 'label'},{label: 'Deactivate Tool',value: 'deactivate'}],
        selTool: "",
        circleRadius: 0,
        circleInput: false,
        ellipseWidth: 0,
        ellipseHeight: 0,
        ellipseInput: false,
        unitInput: false,
        labelText: "",
        labelInput: false,
        labelValid: false,
        labelCheck: function(){
            var label = $.trim(this.get('labelText'));
            if(label.length > 1) {
                this.set('labelValid',true);
            } else {
                this.set('labelValid',false);
            }
        },
        shapeInputMsg: "",
        //
        shapeUnits: [{name:'Miles', value:'MILES'},{name:'Kilometers', value:'KILOMETERS'},
                    {name:'Meters', value:'METERS'},{name:'Feets', value:'FEETS'}],
        shapeUnit: 'MILES',
        setShapeUnit: function(e){
            console.log(this.get('shapeUnit'));
        },
        activateLabelTool: function(e){
            this.set('labelValid',false);
            //console.log('Label: ', this.get('labelText'));
            var styles = $("<div/>");

            var tSymbol = null;
        },
        activateDrawTool: function(e){
            //console.log(e);
            var tool = this.get('selTool');

            this.set('circleInput',false);
            this.set('circleRadius',0);
            this.set('ellipseInput',false);
            this.set('ellipseWidth',0);
            this.set('ellipseHeight',0);
            this.set('unitInput',false);
            this.set('shapeInputMsg',"");
            //$('#map').css('cursor','default');

            nisis.ui.mapview.util.deactivateDraw();

            if (tool === '' && nisis.ui.mapview.draw.tools.active){
                nisis.ui.mapview.util.deactivateDraw();
            }
            else if (tool === 'deactivate'){
                nisis.ui.mapview.util.deactivateDraw();
                this.set('selTool','');
            }
            else if (tool === 'circle'){
                this.set('circleInput',true);
                this.set('circleRadius',0);
                this.set('unitInput',true);
                this.set('shapeInputMsg',"Enter the radius the your circle");
                //$('#map').css('cursor','crosshair');
                nisis.ui.mapview.util.activateDraw('circle');
            }
            else if (tool === 'ellipse'){
                this.set('ellipseInput',true);
                this.set('ellipseWidth',0);
                this.set('ellipseHeight',0);
                this.set('unitInput',true);
                this.set('shapeInputMsg',"Enter the width and height");
                //$('#map').css('cursor','crosshair');
                nisis.ui.mapview.util.activateDraw('ellipse');
            }
            else if (tool === 'label') {
                this.set('labelInput',true);
                this.set('labelText', "");
                this.set('shapeInputMsg','Enter your label here');
                //nisis.ui.mapview.util.activateDraw('label');
            }
            else {
                nisis.ui.mapview.util.activateDraw(tool);
            }
        },
        //poste copied coordinates
        populateCoordValues: function(e) {
            //console.log('Pasting values: ', e);
            if ( !e.isDefaultPrevented() ) {
                return;
            }

            var $this = this,
                msg = "",
                row = $(e.target).parent().parent(),
                units = $this.get('verticeUnit'),
                lat,
                lon,
                coords = $this.get('copiedCoordinates');

            if (!coords || coords === "") {
                msg = "There is not valid lat/lon.";
                nisis.ui.displayMessage(msg, 'error');
                return;
            }
            //in decimal degrees
            coords = coords.split(',');

            if (coords && coords.length && coords.length === 2){
                lat = parseFloat(coords[0], 10).toFixed(5);
                lon = parseFloat(coords[1], 10).toFixed(5);
            } else {
                msg = "App could not parse your lat/lon.";
                nisis.ui.displayMessage(msg, 'error');
                return;
            }

            switch (units) {
                case 'DD':
                    row.find('input.dt-lat').val(lat);
                    row.find('input.dt-lon').val(lon);
                break;

                case 'DMS':
                    var ltD = lat < 0 ? 'S' : 'N',
                        lt = nisis.ui.mapview.util.convertDDtoDMS(lat),
                        lgD = lon < 0 ? 'W' : 'E',
                        lg = nisis.ui.mapview.util.convertDDtoDMS(lon);

                    lt = lt.replace(/[^0-9]+/g, "-").replace('--','-').split('-');
                    lg = lg.replace(/[^0-9]+/g, "-").replace('--','-').split('-');

                    row.find('input.dt-lat1').val(lt[0]);
                    row.find('input.dt-lat2').val(lt[1]);
                    row.find('input.dt-lat3').val(lt[2]);
                    row.find('input.dt-lat4').val(ltD);

                    row.find('input.dt-lon1').val(lg[0]);
                    row.find('input.dt-lon2').val(lg[1]);
                    row.find('input.dt-lon3').val(lg[2]);
                    row.find('input.dt-lon4').val(lgD);

                break;

                case 'GARS':
                    var gars = nisis.ui.mapview.util.convertDDtoGARS(lon, lat);
                    row.find('input.dt-gars').val(gars);
                break;

                case 'FRD':

                    console.log('FRD:');
                break;

                default:
                    console.log('App is not set to paste values for ' + units);
            }

            //console.log('Copied coords: ', coords, lat, lon);
        },
        ddInput: function(e) {
            var $this = this,
                val = e.target.value;

            if (val.length < 2) {
                return;
            }

            switch (e.target.classList[0]) {

                case 'dt-lat':
                    if ( (val.length > 1 && !$.isNumeric(val)) || ($.isNumeric(val) && ( parseInt(val) > 90 || parseInt(val) < -90 ) ) ) {
                        var msg = "Not a good latitude value: ";
                        msg += val;
                        msg += ". Enter a value between -90 & 90.";

                        $this.set('verticesMsgType', 'tc-r');
                        $this.set('verticesMsg', msg);

                        nisis.ui.displayMessage(msg, "error");

                    } else {
                        $this.set('verticesMsgType', 'tc-n');
                        $this.set('verticesMsg', "");
                    }

                break;

                case 'dt-lon':
                    if ( (val.length > 1 && !$.isNumeric(val))  || ( $.isNumeric(val) && ( parseInt(val) > 180 || parseInt(val) < -180) ) ) {
                        var msg = "Not a good longitude value: ";
                        msg += val;
                        msg += ". Enter a value between -180 & 180.";

                        $this.set('verticesMsgType', 'tc-r');
                        $this.set('verticesMsg', msg);

                        nisis.ui.displayMessage(msg, "error");

                    } else {
                        $this.set('verticesMsgType', 'tc-n');
                        $this.set('verticesMsg', "");
                    }

                break;
            }
        },
        //Advance the cursor to next vertice input box automagically (if value okay)
        dmsInput: function(e){
            var val = e.target.value;

            //validate input as it's being edited, advance focus if it's good
            switch (e.target.classList[0]) { //0'th class of input element is unique id
                case 'dt-lat1':
                    if (val.length === 2 && $.isNumeric(val) && parseInt(val) <= 90) {
                        focusFwd(true, "input.dt-lat2", "degree");
                    } else if (val.length === 2 && (!$.isNumeric(val) || parseInt(val) > 90)) {
                        focusFwd(false, "input.dt-lat2", "degree");
                    }
                break;
                case 'dt-lat2':
                    if (val.length === 2 && $.isNumeric(val) && parseInt(val) <= 60) {
                        focusFwd(true, "input.dt-lat3", "minutes");
                    } else if (val.length === 2 && (!$.isNumeric(val) || parseInt(val) > 60)) {
                        focusFwd(false, "input.dt-lat2", "minutes");
                    }
                break;
                case 'dt-lat3':
                    if (val.length === 5 && $.isNumeric(val) && parseInt(val) <= 60) {
                        focusFwd(true, "input.dt-lat4", "seconds");
                    } else if (val.length === 5 && (!$.isNumeric(val) || parseInt(val) > 60)) {
                        focusFwd(false, "input.dt-lat2", "seconds");
                    }
                break;
                case 'dt-lat4':
                    if (val.length === 1 && $.inArray(val.toUpperCase(), ['N', 'S']) == -1 ) {
                        setVal("");
                        focusFwd(false, "input.dt-lon1", "direction");
                    } else if (val.length === 1 && $.inArray(val.toUpperCase(), ['N', 'S']) != -1) {
                        setVal(val.toUpperCase());
                        focusFwd(true, "input.dt-lon1", "direction");
                    }
                break;

                case 'dt-lon1':
                    if (val.length === 3 && $.isNumeric(val) && parseInt(val) <= 180) {
                        focusFwd(true, "input.dt-lon2", "degree");
                    } else if (val.length === 3 && (!$.isNumeric(val) || parseInt(val) > 180)) {
                        focusFwd(false, "input.dt-lat2", "degree");
                    }
                break;
                case 'dt-lon2':
                    if (val.length === 2 && $.isNumeric(val) && parseInt(val) <= 60) {
                        focusFwd(true, "input.dt-lon3", "minutes");
                    } else if (val.length === 2 && (!$.isNumeric(val) || parseInt(val) > 60)) {
                        focusFwd(false, "input.dt-lat2", "minutes");
                    }
                break;
                case 'dt-lon3':
                    if (val.length === 5 && $.isNumeric(val) && parseInt(val) <= 60) {
                        focusFwd(true, "input.dt-lon4", "seconds");
                    } else if (val.length === 5 && (!$.isNumeric(val) || parseInt(val) > 60)) {
                        focusFwd(false, "input.dt-lat2", "seconds");
                    }
                break;
                case 'dt-lon4':
                    if(val.length === 1 && $.inArray(val.toUpperCase(), ['E', 'W']) != -1) {
                        setVal(val.toUpperCase());
                        focusNextLine();

                    } else if ( val.length === 1 && $.inArray(val.toUpperCase(), ['E', 'W']) == -1 ) {
                        nisis.ui.displayMessage("Not a good direction value.", "error");
                        setVal("");
                    }
                break;
            }

            //set the value of the dms input being edited
            function setVal(str){
                e.target.value = str;
            }

            //If test evaluates to true, focus moves to specified dms selector. Specify type for err message if test evaluates to false.
            //Returns result of test
            function focusFwd(test, selector, type){
                if (test){
                    if(selector !== ""){
                        //$(selector).focus();
                        var next = $(e.target).parent()
                        .parent().find(selector);

                        if (next && next.length) {
                            $(next[0]).focus();
                        }
                    }
                } else {
                    nisis.ui.displayMessage("Cursor not advanced. Not a good " + type + " value.", "error");
                }
                return test;
            }

            function focusNextLine() {
                var curr = $(e.target).parent()
                    .parent().parent(),
                    next = null;

                if (curr && curr.length) {
                    next = curr.next();
                }

                if (next && next.length) {
                    $(next[0]).find('input.dt-lat1').focus();
                }
            };
        },
        moveToRadialInput: function(e) {
            var wrap = e.sender.wrapper.parent().parent();

            wrap.find('input.dt-frd-radial').focus();
        },
        frdRadialInput: function(e) {

        },
        frdDistInput: function(e) {

        },
        frdInput: function(e) {
            var input,
                val = $(e.target).val();

            if ( val && !$.isNumeric(val) ) {
                input = "";

                $.each(val, function(i, v) {
                    if ( $.isNumeric(v) ) {
                        input += v;
                    }
                });

                if ( val != input ) {
                    $(e.target).val(input);
                }

            } else {
                //console.log('Check FRD input:', val);
            }

        },
        addVertice: function(e){
            var $this = this,
                vertice = $(e.currentTarget).parent().parent(),
                vertices = vertice.parent();

            var copy = vertice.clone(true);

            copy.find('input.dt-ll').val('');

            var apt = copy.find('input.dt-ll.dt-apt')[0],
                vor = copy.find('input.dt-ll.dt-vor')[0],
                frd = copy.find('input.dt-ll.dt-frd')[0];

            //create drop down for airports
            if ( apt ) {

                $(copy.find('span.dt-ll.dt-apt')[0]).remove();

                var input = $("<input/>", {
                    "class": "w-400 dt-apt dt-ll",
                });

                $(copy.find('span.dt-input.dt-apt')[1]).append(input);

                input.kendoDropDownList({
                    optionLabel: "Select an Airport",
                    autoBind: false,
                    minLength: 2,
                    dataTextField: "LABEL",
                    dataValueField: "FAA",
                    filter: "contains",
                    serverFiltering: true,
                    dataSource: $this.airportVertices()
                });

                input.data('kendoDropDownList').value("");
            }
            //create drop down for VORs
            if ( vor ) {

                $(copy.find('span.dt-ll.dt-vor')[0]).remove();

                var input = $("<input/>", {
                    "class": "w-400 dt-vor dt-ll",
                });

                $(copy.find('span.dt-input.dt-vor')[1]).append(input);

                input.kendoDropDownList({
                    optionLabel: "Select a VOR",
                    autoBind: false,
                    minLength: 2,
                    dataTextField: "LABEL",
                    dataValueField: "ANS",
                    filter: "contains",
                    serverFiltering: true,
                    dataSource: $this.vorVertices()
                });

                input.data('kendoDropDownList').value("");
            }
            //create drop down for frd
            if ( frd ) {

                $(copy.find('span.dt-ll.dt-frd')[0]).remove();

                var input = $("<input/>", {
                    "class": "w-200 dt-frd dt-ll",
                });

                $(copy.find('span.dt-input.dt-frd')[1]).append(input);

                input.kendoDropDownList({
                    optionLabel: "Select a VOR",
                    autoBind: false,
                    minLength: 2,
                    dataTextField: "LABEL",
                    dataValueField: "ANS",
                    filter: "contains",
                    serverFiltering: true,
                    dataSource: $this.vorVertices()
                });

                input.data('kendoDropDownList').value("");
            }

            vertice.after(copy);

            copy.find('div.dt-arc-options').hide();
            copy.find('input.dt-curve-line').attr('checked', false);

            $this.updateVerticesCount(vertices);
        },
        removeVertice: function(e){
            var vertice = $(e.currentTarget).parent().parent(),
            vertices = vertice.parent(),
            len = vertices.children().length;

            if(len === 1){
                nisis.ui.displayMessage('You will need at least 1 vertice.','Vertices Count!','warn');
                return;
            }
            vertice.remove();
            this.updateVerticesCount(vertices);
        },
        toggleVerticeOption: function(e){
            var $this = this;
                shape = $this.get('selectedShape'),
                options = $(e.currentTarget)
                    .parent().parent().find('div.dt-arc-options');
                    //.parent().next().next();

            if ( shape === 'point' ) {
                options.hide();
                return;
            }

            if (!options.length) {
                return;
            }

            options.toggle();

            if ( shape === 'circle' ) {
                options.find('input.dt-semi-circle')
                    .parent().show();

                options.find('input.dt-curve-line')
                    .parent().hide();

            } else {
                options.find('input.dt-semi-circle')
                    .parent().hide();
                options.find('input.dt-curve-line')
                    .parent().show();
            }
        },
        toggleCurveFields: function (e) {
            //console.log('Arc options:', e);
            var $this = this,
                units = $this.get('verticeUnit'),
                target = $(e.currentTarget),
                wrap = target.parent().parent(),
                checked = target.is(':checked');

            var sep1, sep2, dir, dirCheck,
                ctrLabel, mPt, mPtCheck, curve,
                latVal, lonVal, coordLabel, lat, lon;


            if (checked) {

                dir = $("<label/>", {
                    'class': 'dt-curve-dir',
                    'text': ' Counter clockwise'
                });
                dirCheck = $("<input />", {
                    'class': 'dt-curve-dir',
                    'type': 'checkbox'
                });

                dir.prepend(dirCheck).appendTo(wrap);

                //this was use to calculate mid point for curved lines
                /*
                sep1 = $("<br/><br/>");
                sep2 = $("<br/><br/>");

                ctrLabel = $("<label/>", {
                    'class': 'dt-curve-center d-b m-t10',
                    'text': 'Enter / Select center of arc arc'
                });

                wrap.append(ctrLabel);

                mPt = $("<label/>", {
                    'class': 'dt-curve-center d-b m-t10',
                    'text': ' Use mid-point for center'
                });
                mPtCheck = $("<input />", {
                    'class': 'dt-curve-mid-pt',
                    'type': 'checkbox'
                });

                mPt.prepend(mPtCheck).appendTo(wrap);

                curve = $("<div/>", {
                    'class': 'dt-curve-center marg m-l0 m-t10 clr of-h'
                });

                latVal = "Lat (38.99999)";
                lonVal = "Lon (-76.99999)";

                coordLabel = $('<span/>', {
                    "class": "dt-curve-input",
                    "text": "Center coordinates: "
                });

                lat = $('<input/>', {
                    "type": "text",
                    "class": "dt-curve-input dt-lat m-r15 k-textbox",
                    "placeholder": latVal
                }).wrap("<span class='' />");
                lat.on('paste', function(evt){
                    evt.preventDefault();
                    $this.populateCoordValues.call($this, evt);
                });

                lon = $('<input />', {
                    "type": "text",
                    "class": "dt-curve-input dt-lon k-textbox",
                    "placeholder": lonVal
                }).wrap("<span class='' />");
                lon.on('paste', function(evt){
                    evt.preventDefault();
                    $this.populateCoordValues.call($this, evt);
                });

                curve.append(coordLabel, lat, lon).appendTo(wrap);

                mPtCheck.on('change', function(evt) {
                    var checked = $(this).is(':checked');

                    if (checked) {

                        var currIdx,
                            currVertice = wrap.parent(),

                            nextIdx,
                            nextVertice;

                        if (currVertice) {
                            currIdx = currVertice.index();
                        }

                        if (currIdx >= 0) {
                            currIdx++;
                            nextVertice = currVertice.next();

                        } else {
                            $this.set('verticesMsgType', 'tc-r');
                            $this.set('verticesMsg', 'App could not identify starting vertice.');
                            return;
                        }

                        if (nextVertice) {
                            nextIdx = nextVertice.index();
                        }

                        if (nextIdx >= 0) {
                            nextIdx++;

                        } else {
                            $this.set('verticesMsgType', 'tc-r');
                            $this.set('verticesMsg', 'App could not identify ending vertice.');
                            return;
                        }

                        var x, y,
                            ptA = $this.getVerticeCoords.call($this, currIdx),
                            ptB = $this.getVerticeCoords.call($this, nextIdx);

                        var midPt = $this.populateMidPoint.call($this, evt, ptA, ptB);

                        if (midPt && midPt.length) {
                            x = midPt[0];
                            y = midPt[1];

                            lat.val('');
                            lat.attr('disabled', true);

                            lon.val('');
                            lon.attr('disabled', true);

                            lat.val(y);
                            lon.val(x);

                            $this.set('verticesMsgType', 'tc-n');
                            $this.set('verticesMsg', '');
                        } else {
                            var msg = "App failed to retreive mid point";

                            $this.set('verticesMsgType', 'tc-r');
                            $this.set('verticesMsg', msg);
                        }

                    } else {
                        lat.val('');
                        lat.removeAttr('disabled');

                        lon.val('');
                        lon.removeAttr('disabled');
                    }
                });
                */

            } else {
                wrap.find('.dt-curve-dir, .dt-curve-center').remove();
            }
        },
        populateMidPoint: function(evt, ptA, ptB) {
            //console.log('Populating mid point: ', this, evt);

            if(!ptA || !ptB) {
                return null;
            }

            var mPt = [];

            require(['libs/geo', 'libs/latlong'], function(Geo, LatLon) {

                var a = new LatLon(ptA[1], ptA[0]),
                    b = new LatLon(ptB[1], ptB[0]);

                var m = a.midpointTo(b);

                if (m && m.lat && m.lon) {
                    mPt.push(m.lon);
                    mPt.push(m.lat);
                }
            });

            return mPt;
        },
        updateVerticesCount: function(vertices) {
            var $this = this,
                vertices = vertices,
                shape = $this.get('selectedShape'),
                vShapes = ['polyline', 'polygon'],
                t = $(vertices).children().length;

            if (!vertices) {
                vertices = $('div.dt-vertices');
            }

            $(vertices).children().each(function(i, div) {

                var index = i+1;

                $(div).find('.dt-vertice-count').html(index + '- ');

                if ( index === 1 ) {
                    if ( t > 1) {
                        $(div).find('span.dt-remove-vertice').show();
                    } else {
                        $(div).find('span.dt-remove-vertice').hide();
                    }

                    if ( $.inArray(shape, vShapes) !== -1 ) {
                        $(div).find('span.dt-options').show();
                    } else {
                        $(div).find('span.dt-options').hide();
                    }

                } else {

                    $(div).find('span.dt-remove-vertice').show();

                    if ( index !== t ) {

                        if ( $.inArray(shape, vShapes) !== -1 ) {
                            $(div).find('span.dt-options').show();
                        } else {
                            $(div).find('span.dt-options').hide();
                        }

                    } else {
                        $(div).find('span.dt-options').hide();
                    }
                }
                //remove extra vertices for pie shapes only
                if ( i > 0 && shape === 'pie' ) {
                    $(div).remove();
                    return;
                }
            });
        },
        verticeUnit: "DMS",
        hideDD: false,
        hideGARS: true,
        hideUTM: true,
        verticeUnits: [
            {name:"Decimal Degrees (DD)",value:"DD"}
            ,{name:"Degrees Minutes Seconds (DMS)",value:"DMS"}
            ,{name:"Global Area Reference System (GARS)",value:"GARS"}
            //,{name:"Universal Transverse Mercator (UTM)",value:"UTM"}
            ,{name:"Airports (ICAO)",value:"APT"}
            ,{name:"Very high frequency Omni-Directional Range (VOR)",value:"VOR"}
            ,{name:"Fix Radial Distance (FRD)",value:"FRD"}
        ],
        resetVerticesValue: function(e) {
            //$('.dt-dd, .dt-utm, .dt-gars, .dt-dms').hide();
            $('.dt-input').hide();
            //show pie shape options
            if ( this.get('isShapePie') ) {
                $('.dt-input.dt-pie').show();
            }

            var value = e ? e.sender.value() : this.get('verticeUnit');

            switch ( value ) {
                case 'GARS':
                    $('.dt-input.dt-gars').show();

                break;

                case 'UTM':
                    $('.dt-input.dt-utm').show();

                break;

                case 'DD':
                    $('.dt-input.dt-dd').show();

                break;

                case 'DMS':
                    $('.dt-input.dt-dms').show();

                break;

                case 'APT':
                    $('.dt-input.dt-apt').show();

                break;

                case 'VOR':
                    $('.dt-input.dt-vor').show();

                break;

                case 'FRD':
                    $('.dt-input.dt-frd').show();

                break;

                default:
                    //console.error("bad value for vertices type: ", e.sender.value());
            }
        },
        verticesReady: true,
        verticesReset: true,
        selectedShape: 'circle',
        showVertOpts: true,
        isShapePie: false,
        isShapeCircle: true,
        vCircleRadius: 1,
        vCircleRaduisUnit: "nm",
        showBufferOption: false,
        bufferShape: false,
        showKeepBothOption: false,
        keepBothShapes: false,
        copiedCoordinates: "",
        verticesMsg: "",
        verticesMsgType: "",
        vCircleRadiusUnits: new kendo.data.DataSource({
            data: [{name:'Centimeters',value:"cm"}, {name:'Feet',value:"ft"}, {name:'Inches',value:"in"},
                    {name:'Kilometers',value:"km"}, {name:'Meters',value:"me"}, {name:"Miles",value:"mi"}, {name:'Millimeters',value:"mm"},
                    {name:"Nautical Miles",value:"nm"}, {name:'Yard',value:"yd"}]
        }),
        airportVertices: function() {
            //return a unique data store for each field
            return new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "api/lookup.php"
                        ,type: 'GET'
                        ,dataType: 'json'
                    }
                    ,parameterMap: function(data,type) {
                        var filter = "";
                        if (data && data.filter) {
                            if (data.filter.filters && data.filter.filters.length) {
                                filter = data.filter.filters[0].value;
                            }
                        }

                        return {
                            'action': 'lookup',
                            'target': 'airports',
                            'filter': filter
                        };
                    }
                },
                serverFiltering: true,
                error: function(e){
                    console.log('Airports data source:', e);
                    nisis.ui.displayMessage(e.status, 'error');
                }
            });
        },
        vorVertices: function() {
            //return a unique data store for each field
            return new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "api/lookup.php"
                        ,type: 'GET'
                        ,dataType: 'json'
                    }
                    ,parameterMap: function(data,type) {
                        var filter = "";
                        if (data && data.filter) {
                            if (data.filter.filters && data.filter.filters.length) {
                                filter = data.filter.filters[0].value;
                            }
                        }

                        return {
                            'action': 'lookup',
                            'target': 'vor',
                            'filter': filter
                        };
                    }
                },
                serverFiltering: true,
                error: function(e) {
                    //console.log('VOR error: ', e);
                    var m = "App could not retreive VORs. Try again or Report the issue.";
                    nisis.ui.displayMessage(m, 'error');
                }
            });
        },
        resetCircleRadius: function(e){
            console.log('Update radius value:', e);

            var unit = this.get('vCircleRaduisUnit'),
                prevUnit = null,
                radius = this.get('vCircleRadius'),
                checked = $(e.target).is(':checked');

            //console.log('Update radius value:', checked, unit, radius);
            if (checked) {
                this.set('vCircleRadius', 1);
                this.set('vCircleRaduisUnit', 'nm');
            }

        },
        selectShapeType: function(e) {
            var $this = this,
                option = e ? $(e.sender.wrapper) : $('label.dt-type.k-state-active'),
                shape = option.find('input').val(),
                vertices = $('.dt-vertices');

            option.addClass('k-state-active');
            option.siblings().removeClass('k-state-active');

            this.set('selectedShape', shape);
            this.set('showVertOpts', true);

            if(shape === 'circle') {
                this.set('isShapeCircle', true);

                this.set('showBufferOption', false);
                this.set('bufferShape', false);
                this.set('keepBothShapes', false);

                this.set('isShapePie', false);

            } else {

                this.set('showBufferOption', true);

                if ( this.get('bufferShape') ) {
                    this.set('isShapeCircle', true);

                } else {
                    this.set('isShapeCircle', false);
                }

                if (shape === 'pie') {
                    this.set('isShapePie', true);
                    this.set('showBufferOption', false);
                    this.set('showVertOpts', false);
                    $("div.dt-arc-options").hide();

                } else {
                    this.set('isShapePie', false);
                }

                if (shape === 'point') {
                    $("div.dt-arc-options").hide();
                    this.set('isShapePie', false);
                }
            }

            $this.updateVerticesCount.call($this, vertices);
            // kofi for NISIS-1473
            this.resetVerticesValue();
        },
        showBufferFields: function(e){
            var buffer = this.get('bufferShape'),
                circle = this.get('isShapeCircle');

            if (buffer) {
                this.set('isShapeCircle', true);
                this.set('vCircleRadius', 1);
                this.set('vCircleRaduisUnit', 'nm');

                this.set('showKeepBothOption', true);

            } else {
                this.set('isShapeCircle', false);
                this.set('showKeepBothOption', false);
            }
        },
        checkVerticesValues: function(e) {
            var errors = 0,
                values = 0;
            var units = this.get('verticeUnit');
            //check the value of all inputs
            $('.dt-vertice').each(function(i,v){
                $(v).find('input.dt-ll').each(function(j,input){
                    var val = $(input).val();
                    //console.log(j,val);
                    if(val === 0 || val === "" || isNaN(val)){
                        errors += 1;
                    } else {
                        values += 1;
                    }
                });
            });
            //check the radius
            if(this.get('isShapeCircle') && this.get('verticeCircleRadius') === ''){
                errors += 1;
            }
            //check for errors
            if(errors > 0){
                this.set('verticesReady', false);
            } else {
                this.set('verticesReady', true);
            }
            //check for valid values
            if(values > 0){
                this.set('verticesReset', true);
            } else {
                this.set('verticesReset', false);
            }
        },
        clearVerticeFields: function(e){
            this.set('verticesMsgType', 'tc-n');
            this.set('verticesMsg', '');

            //reset shape type
            this.set('selectedShape', "circle");

            $("#dt-circle").prop('checked', true);
            $("#dt-circle").trigger('change');
            $('label.dt-type').removeClass('k-state-active');
            $("#dt-circle").parent().addClass('k-state-active');
            //reset selections
            this.set('verticeUnit', 'DMS');
            //reset circle options
            this.set('vCircleRadius', 1);
            this.set('vCircleRaduisUnit', 'nm');
            //reset buffer options
            this.set('bufferShape', false);
            this.set('keepBothShapes', false);
            this.set('showKeepBothOption', false);
            //reset arc/pie options
            this.set('isShapePie', false);
            $('input.dt-pie.dt-ll').val('');
            // semi circle options
            $("div.dt-arc-options").hide();
            $("input.dt-curve-line").prop('checked', false);
            $("input.dt-curve-dir").prop('checked', false);

            //clear input fields
            $('input.dt-ll, input.dt-lls, input.dt-llm').each(function(i,input) {
                $(input).val('');
            });
            //clear dropdownlists
            $("input.dt-ll[data-role='dropdownlist']").each(function(i, input) {
                var ddl = $(input).data('kendoDropDownList');

                if (ddl) {
                    ddl.value("");
                }
            });
            // reset vertices value ==> this may have been done aleady above
            // this.resetVerticesValue();
            /**
             *  force ui to refresh ==>
             *  this.updateVerticesCount()
             *  &&
             *  this.showBufferFields()
            **/
            this.selectShapeType.call(this);
        },
        getVerticeCoords: function(index) {

            if (!index) return;

            var vertice = $('.dt-vertice:nth-child(' + index + ')');

            if (!vertice) {
                console.log('Retreiving vertice fields:', vertice);
                return null;
            }

            var $this = this,
                gUnit = $this.get('verticeUnit'),
                oLat, oLon,
                lat, lon, gars, utm;

            //lat/lon units
            switch (gUnit) {
	            case 'DD':
	                lat = vertice.find('input.dt-lat').val();
	                lat = parseFloat(lat,10);
	                lon = vertice.find('input.dt-lon').val();
	                lon = parseFloat(lon,10);

	                if ( !$.isNumeric(lat) || !$.isNumeric(lon) ) {
	                    return null;
	                }

	            break;

            	case 'DMS':
	                lat = nisis.ui.mapview.util.convertDMStoDD(
	                        vertice.find('.dt-lat1').val(),
	                        vertice.find('.dt-lat2').val(),
	                        vertice.find('.dt-lat3').val(),
	                        vertice.find('.dt-lat4').val()
	                    );
	                lon = nisis.ui.mapview.util.convertDMStoDD(
	                        vertice.find('.dt-lon1').val(),
	                        vertice.find('.dt-lon2').val(),
	                        vertice.find('.dt-lon3').val(),
	                        vertice.find('.dt-lon4').val()
	                    );

	                if ( !$.isNumeric(lat) || !$.isNumeric(lon) ) {
	                    return null;
	                }

                break;

            	case 'UTM':
	                utm = vertice.find('input.dt-utm').val();
	                var ll = nisis.ui.mapview.util.convertUTMtoDD(utm);

	                if(!$.isNumeric(lat) || !$.isNumeric(lon)){
	                    return null;
	                }

                break;

            	case 'GARS':
	                gars = vertice.find('input.dt-gars').val();
	                //console.log('GARS: ',gars);

	                gars = nisis.ui.mapview.util.convertGARStoDD(gars);
	                console.log('GARS Converted: ', gars);
	                lat = gars.lat;
	                lon = gars.lon;

	                if(!$.isNumeric(lat) || !$.isNumeric(lon)){
	                    return null;
	                }

	            break;

	            case 'APT':
	            	var dd, ds, d,
                        apt = vertice.find('input.dt-apt');

                    if ( !apt ) {
                        console.log('App did not find input field');
                        return;

                    } else {
                        dd = apt.data('kendoDropDownList');
                    }

                    if ( !dd ) {
                        console.log('App did not find kendoDropDownList');
                        return;

                    } else {
                        ds = dd.dataSource;
                        d = dd.dataItem();

                        lat = d['LATITUDE'] ? d['LATITUDE'] : null;
                        lon = d['LONGITUDE'] ? d['LONGITUDE'] : null;

                        if ( !$.isNumeric(lat) || !$.isNumeric(lon) ) {
                            return null;
                        }
                    }

	            break;

	            case 'VOR':
                    var dd, ds, d,
                        vor = vertice.find('input.dt-vor');

                    if ( !vor ) {
                        console.log('App did not find input field');
                        return;

                    } else {
                        dd = vor.data('kendoDropDownList');
                    }

                    if ( !dd ) {
                        console.log('App did not find kendoDropDownList');
                        return;

                    } else {
                        ds = dd.dataSource;
                        d = dd.dataItem();

                        lat = d['LATITUDE'] ? d['LATITUDE'] : null;
                        lon = d['LONGITUDE'] ? d['LONGITUDE'] : null;

                        if ( !$.isNumeric(lat) || !$.isNumeric(lon) ) {
                            return null;
                        }
                    }

	            break;

	            case 'FRD':
                    var dd, ds, d,
                        vor = vertice.find('input.dt-frd'),
                        radial = vertice.find('input.dt-frd-radial'),
                        distance = vertice.find('input.dt-frd-dist');

                    if ( !vor.length ) {
                        console.log('App did not find input field');
                        return;
                    }

                    dd = $(vor[0]).data('kendoDropDownList');

                    if ( !dd ) {
                        console.log('App did not find kendoDropDownList');
                        return;

                    } else {
                        ds = dd.dataSource;
                        d = dd.dataItem();

                        if ( !d ) {
                            return null;
                        }

                        var lt = d['LATITUDE'] ? d['LATITUDE'] : null,
                            lg = d['LONGITUDE'] ? d['LONGITUDE'] : null,
                            mag = d['MAG'],
                            from, to, brg, dist;

                        if ( !$.isNumeric(lt) || !$.isNumeric(lg) ) {
                            return null;
                        }

                        oLat = lt;
                        oLon = lg;

                        if (!radial) {
                            console.log('App did not find radial field');
                            return null;

                        } else {
                            brg = radial.val();
                        }

                        if (!distance) {
                            console.log('App did not find distance field');
                            return null;

                        } else {
                            dist = distance.val();
                        }

                        if ( !$.isNumeric(brg) || !$.isNumeric(dist) || !$.isNumeric(mag)) {
                            return null;
                        }

                        //This is the first check for valid inputs
                        if ( brg < 0 || brg > 360 ) {
                            console.log('User Selection. Invalid bearing:', brg);
                            return null;
                        }

                        brg = parseInt(brg) + parseInt(mag);
                        dist = parseFloat(dist);

                        //normalized bearing so it's within 0 & 360
                        if (brg < 0) {
                            brg += 360;
                        }
                        else if (brg > 360) {
                            brg -= 360;
                        }
                        //This might not happen but still double check. if it does, use a function to normalize
                        if ( brg < 0 || brg > 360 ) {
                            console.log('User Selection. Invalid bearing 2:', brg);
                            return null;
                        }

                        if ( dist <= 0 ) {
                            console.log('User Selection. Invalid distance:', dist);
                        }

                        dist *= 1.852;

                        from = new LatLon(lt, lg);
                        to = from.destinationPoint(brg, dist);

                        if (to && to.lat && to.lon) {
                            lat = to.lat;
                            lon = to.lon;
                        }
                    }

	            break;

            	default:
                	nisis.ui.displayMessage('Unknown units:', gUnit);
                	return null;
            }
            //check for valid latitude
            if ( lat < -90 || lat > 90 ) {
                return null;
            }
            //check for valid longitude
            if ( lon < -180 || lon > 180 ) {
                return null;
            }

            var result = [lon, lat];

            //using this just for testing. Remove when done
            if (oLat && oLon) {
                //result.push([oLon, oLat]);
            }

            return result;
        },
        getVerticesMidPoint: function(ptA, ptB) {
            //==>> ptA = [lon, lat], ptB = [lon, lat]
            if(!ptA || !ptB) {
                return null;
            }

            var mPt = [];

            require(['libs/geo', 'libs/latlong'], function(Geo, LatLon) {

                var a = new LatLon(ptA[1], ptA[0]),
                    b = new LatLon(ptB[1], ptB[0]);

                var m = a.midpointTo(b);

                if (m && m.lat && m.lon) {
                    mPt.push(m.lon);
                    mPt.push(m.lat);
                }
            });

            return mPt;
        },
        createVerticesShape: function(e){
            // return;
            var $this = this,
                app = $this.get('app'),
                pts = [],
                errors = 0,
                msg = "Processing ",
                gType = $this.get('selectedShape'),
                gUnit = $this.get('verticeUnit'),
                rUnit = $this.get('vCircleRaduisUnit'),
                radius = $this.get('vCircleRadius'),
                buffer = $this.get('bufferShape'),
                keepOrig = $this.get('keepBothShapes');

            //for pies only
            var pieR = null,
                pieC = null,
                pieA = null,
                pieB = null;
            //console.log(radius);
            radius = radius ? parseFloat(radius) : null;
            //console.log('Vertices Circle Radius: ',radius);

            msg += gType;
            msg += " (" + gUnit + ") ...";

            $this.set('verticesMsgType', 'tc-n');
            $this.set('verticesMsg', msg);
            //get vertices values
            $('.dt-vertice').each(function(i, div) {

                var vertice = $(this),
                    lat, lon, gars, utm,
                	nextIdx = i + 2;

                //get coords of the vertice
                var point = $this.getVerticeCoords(i + 1);

                if (point) {
                    lat = point[1];
                    lon = point[0];
                    //just to test the origin in certain case. Remove when done
                    if (point[2] && point[2].length) {
                        nisis.ui.displayMessage('The origin was added as well', 'error');
                        pts.push(point[2]);
                    }
                }

                var pt = [];

                if (lat && lon) {
                    pt.push(lon);
                    pt.push(lat);

                } else {
                    var m = 'Invalid lat/lon for vertice #' + (i+1);
                    $this.set('verticesMsgType', 'tc-r');
                    $this.set('verticesMsg', m);
                    nisis.ui.displayMessage(m, 'error');
                    return;
                }

                var arc = $(this).find('input.dt-curve-line').is(':checked'),
                    dir = null,
                    cLat = null,
                    cLon = null;

                if (arc) {
                    dir = $(this).find('input.dt-curve-dir').is(':checked');

                    //set arc center
                    var center = [],
                        nextPt = $this.getVerticeCoords(i + 2),
                        a = [lon, lat],
                        b = nextPt ? nextPt : null,
                        c = null;

                    if (a && b) {
                        c = $this.getVerticesMidPoint(a, b);
                    }

                    if (c) {
                        center = c;
                    } else {
                        return;
                    }

                    var start = new LatLon(lat, lon),
                        mid = new LatLon(center[1], center[0]),
                        dist = mid.distanceTo(start).toFixed(0),
                        endPt = $this.getVerticeCoords.call($this, nextIdx),
                        end = new LatLon(parseFloat(endPt[1], 10), parseFloat(endPt[0], 10));

                    var startBrg = mid.bearingTo(start).toFixed(0),
                        endBrg = mid.bearingTo(end).toFixed(0);

                    pts.push(pt);

                    //number of pie needed for a perfect curve
                    var n = Math.abs(startBrg - endBrg),
                        angle = parseInt(startBrg);

                    if (!dir) { //clockwise
                        for (var i = 1; i < n; i++) {
                            var sect = angle + 1;
                            var curvePt = mid.destinationPoint(sect, dist);

                            pts.push([curvePt.lon, curvePt.lat]);

                            if (sect === 360 && i < n) {
                                angle = 0;
                            }
                            angle++;
                        }

                    } else { //counter clockwise
                        for (var i = 1; i < n; i++) {
                            var sect = angle + 1;
                            var curvePt = mid.destinationPoint(sect, dist);

                            pts.push([curvePt.lon, curvePt.lat]);

                            if (sect === 0 && i < n) {
                                angle = 360;
                            }
                            angle--;
                        }
                    }

                } else {
                    pts.push(pt);
                }

                if (gType === 'pie') {
                    var b1 = vertice.find("input.dt-pie-brg1").val(),
                        b2 = vertice.find("input.dt-pie-brg2").val(),
                        npts = parseInt(b2) - parseInt(b1),
                        r = vertice.find("input.dt-pie-dist").val();

                    var msg = "";

                    if ( $.isNumeric(b1) ) {
                        b1 = parseInt(b1);
                    } else {
                        msg += ", Invalid Bearing A for veritce #" + (i + 1);
                    }

                    if ( $.isNumeric(b2) ) {
                        b2 = parseInt(b2);
                    } else {
                        msg += ", Invalid Bearing B for veritce #" + (i + 1);
                    }

                    if (b1 === b2) {
                        msg += ", Bearing A cannot be the same as B for vertice #" + (i + 1);
                    }

                    if ( $.isNumeric(r) ) {
                        pieR = parseFloat(r);
                        r = parseInt(r) * 1.852;

                    } else {
                        msg += ", Invalid Radius for veritce #" + (i + 1);
                    }

                    if ( !$.isNumeric(npts) ) {
                        msg += ", Invalid Bearing A & B combination for veritce #" + (i + 1);
                    }

                    if (msg !== "") {
                        msg = "Pie errors" + msg;

                        $this.set('verticesMsgType', 'tc-r');
                        $this.set('verticesMsg', msg);
                        return;
                    }
                    //return;
                    var center = new LatLon(lat, lon),
                        start = center.destinationPoint(b1, r),
                        end = center.destinationPoint(b2, r);
                    // pie values for geom creation
                    pieC = [center.lon, center.lat];
                    pieA = [start.lon, start.lat];
                    pieB = [end.lon, end.lat];
                    //add the center and the first point
                    pts.push([center.lon, center.lat]);
                    pts.push([start.lon, start.lat]);
                    //get ride of the negative numbers
                    var total = npts < 0 ? ( 360 + npts) : npts;

                    for (var i = 0; i < total; i++) {
                        var sect =  b1 + 1;

                        var curvePt = center.destinationPoint(sect, r);
                        pts.push([curvePt.lon, curvePt.lat]);

                        if (sect === 360 && i < total) {
                            b1 = 0;
                        }

                        b1++;
                    }

                    pts.push([end.lon, end.lat]);
                }
            });

            //check if there is any valid points
            if(pts.length === 0) {
                //nisis.ui.displayMessage('There is no valid input for your vertices');
                $this.set('verticesMsgType', 'tc-r');
                msg += ' There is no valid input for your vertices.';
                $this.set('verticesMsg', msg);

            } else {
                //check for errors
                if(errors > 0){
                    $this.set('verticesMsgType', 'tc-r');
                    msg += ' There are errors that require your attention.';
                    $this.set('verticesMsg', msg);
                    return;
                }
                //check geom type and validate number of vertices
                if( gType === 'polyline' && pts.length < 2 ) {
                    $this.set('verticesMsgType', 'tc-r');
                    msg += ' Insuffisant number of vertices for ' + gType;
                    $this.set('verticesMsg', msg);
                    return;

                } else if ( (gType === 'polygon' || gType === 'pie') && pts.length < 3) {
                    $this.set('verticesMsgType', 'tc-r');
                    msg += ' Insuffisant number of vertices for ' + gType;
                    $this.set('verticesMsg', msg);
                    return;

                } else if ( gType === 'circle' && (!$.isNumeric(radius) || radius < 0 || radius === 0) ) {
                    $this.set('verticesMsgType', 'tc-r');
                    msg += ' Invalid radius for ' + gType;
                    $this.set('verticesMsg', msg);
                    return;

                }

                var opts = {
                    data: pts,
                    type: gType
                };
                //overwrite the
                if (gType === 'pie') {
                    //opts.type = 'polygon';
                    opts.type = 'pie';
                    opts.pier = pieR;
                    opts.piec = pieC;
                    opts.piea = pieA;
                    opts.pieb = pieB;
                }

                if ( gType !== 'circle' && buffer && (!$.isNumeric(radius) || radius === 0 ) ) {
                    $this.set('verticesMsgType', 'tc-r');
                    $this.set('verticesMsg', 'Invalid Radius for buffering feature.');
                    return;

                } else {
                    opts.buffer = buffer;
                    opts.keepOriginal = keepOrig;
                }

                if (radius) {
                    opts.radius = nisis.ui.mapview.util.convertUnits(radius, rUnit, 'me');

                } else {
                    $this.set('verticesMsgType', 'tc-r');
                    $this.set('verticesMsg', 'Invalid radius. Check your inputs.');
                    return;
                }

                $this.set('verticesMsgType', 'tc-n');
                var m = 'App is now generating your shape(s) ...';
                $this.set('verticesMsg', m);

                //create geometry
                nisis.ui.mapview.util.createGeometry(opts, app);
                //clear message
                setTimeout(function() {
                    $this.set('verticesMsg', '');
                }, 3000);
            }
        },
        //styles
        stylesMsg: 'There is no editable feature selected.',
        stylesReady: false,
        styleFeatType: "",
        editFeatType: false,
        stylesType: "",
        editStylesType: false,
        styleFeature: null,
        styleFeatSelected: false,
        //==> points
        pointFeature: true,
        ptMarkerType: 'circle',
        ptMarkerTypes: [{name:'Circle', value:'circle'}, {name:'Cross', value:'cross'},
        		{name:'Diamond', value:'diamond'}, {name:'Square',value:'square'},
        		{name:'X', value:'x'}],
        ptSize: 15,
        ptOulineType: 'solid',
        ptOutlineWidth: 1,
        ptColor: "#fff",
        ptTrans: 0,
        ptOutlineType: "solid",
        ptOutlineColor: "#000",
        //==> lines
        polylineFeature: false,
        plType: 'solid',
        plTypes: [{name:'Dash',value:'dash'}, {name:'Dot',value:'dot'},
        		{name:'Dash-Dot',value:'dashdot'}, {name:'Dash-Dot-Dot',value:'longdashdotdot'},
        		{name:'Solid',value:'solid'}],
        plWidth: 2,
        plColor: '#000',
        plTrans: 0,
        //==> polygons
        polygonFeature: false,
        pgFillType: 'solid',
        pgFillTypes: [{name:'Backward Diagonal', value:'backwarddiagonal'},
        		{name:'Diagonal Cross', value:'diagonalcross'},
        		{name:'Forward Diagonal', value:'forwarddiagonal'},
        		{name:'Horizontal', value:'horizontal'},
        		{name:'Solid', value:'solid'}, {name:'Vertical', value:'vertical'}],
        pgFillColor: '#fff',
        //pgFillColActive: true,
        pgFillColActive: function(){
            var $this = this,
                fillType = $this.get('pgFillType');

            if ( fillType !== 'solid' ) {
                return false;
            } else {
                return true;
            }
        },
        pgFillTrans: 0,
        //pgFillTransActive: true,
        pgFillTransActive: function(){
            var $this = this,
                fillType = $this.get('pgFillType');

            if ( fillType !== 'solid' ) {
                return false;
            } else {
                return true;
            }
        },
        pgOutlineType: 'solid',
        pgOutlineWidth: 2,
        pgOutlineColor: '#000',
        updatePgStyleOptions: function(e){
            console.log('Pg Style Option: ', e.sender.value());
            var $this = this,
                fillType = e.sender.value();

            if ( fillType !== 'solid' ) {
                $this.set('pgFillColActive', false);
                $this.set('pgFillTransActive', false);
            } else {
                $this.set('pgFillColActive', true);
                $this.set('pgFillTransActive', true);
            }
        },
        //clear style form
        clearStylesForm: function(e){
        	var $this = this,
            	feat = $this.get('styleFeature');

            if ( feat ) {
            	$this.set('styleFeature', null);
            }

            $this.setStylesForm();
        },
        //reset the form
        setStylesForm: function(){
            //console.log('Setting up styles form:', graphic);
            var $this = this,
                feat = $this.get('styleFeature');
            //console.log('Style feature: ',feat);

            if(feat !== null){
                //console.log(feat.getLayer());
                var layer = feat.getLayer();

                if (layer.id.toLowerCase() === 'user_graphics') {
                    var msg = 'This feature can not be styled.';
                    nisis.ui.displayMessage(msg, '', 'error');
                    $this.set('styleFeature', null);
                    $this.setStylesForm();
                    return;
                }

                var sMsg = "Update Styles of features #";
                sMsg += feat.attributes.OBJECTID;
                sMsg += " of '";
                sMsg += layer.name;
                sMsg += "'";

                $this.set('stylesMsg', sMsg);
                $this.set('stylesReady', true);
                $this.set('styleFeatSelected', true);
                $this.set('styleFeatType', feat.geometry.type);
                $this.set('stylesType', 'marker');

                var symbol = feat.symbol,
                    style = layer.id,
                    styles,
                    len;

                //check for custom styles
                /*if ( renderers[style] ) {
                    if ( renderers[style].styles && renderers[style].styles.length ) {
                        styles = renderers[style].styles;
                        len = styles.length;

                        $.each(styles,function(i, s){
                            //console.log('Predefined Style: ', i, ":", "s:", s);
                            if ( s.layer.id === layer.id && s.oid == feat.attributes.OBJECTID ) {
                                symbol = s.symbol;
                                return;
                            }
                        });
                    }
                }*/
                //Some layers have their symbol defined in the renderer
                if ( !symbol ) {
                    symbol = layer.renderer.getSymbol(feat);
                }

                //console.log('Current Symbol: ', symbol);

                if(feat.geometry.type === 'point'){
                    $this.set('pointFeature', true);
                    $this.setPointStyle(symbol);
                } else {
                    $this.set('pointFeature', false)
                }
                if(feat.geometry.type === 'polyline'){
                    $this.set('polylineFeature', true);
                    $this.setPolylineStyle(symbol);
                } else {
                    $this.set('polylineFeature', false);
                }
                if(feat.geometry.type === 'polygon'){
                    $this.set('polygonFeature', true);
                    $this.setPolygonStyle(symbol);
                } else {
                    $this.set('polygonFeature', false);
                }
            } else {
                $this.set('stylesMsg', 'There is no editable feature selected.');
                $this.set('stylesReady', false);
                $this.set('styleFeatSelected', false);
                $this.set('styleFeatType', '');
                $this.set('stylesType', '');
                //reset point styles
                $this.set('ptMarkerType', "circle");
                $this.set('ptSize', 15);
                $this.set('ptColor', "#fff");
                $this.set('ptTrans', 0);
                $this.set('ptOutlineType', 'solid');
                $this.set('ptOutlineWidth', 1);
                $this.set('ptOutlineColor', "#000");
                //reset line styles
                $this.set('plType', 'solid');
                $this.set('plWidth', 2);
                $this.set('plColor', "#fff");
                $this.set('plTrans', 0);
                //reset polygon styles
                $this.set('pgFillType', 'solid');
                $this.set('pgFillColor', "#fff");
                $this.set('pgFillTrans', 0);
                $this.set('pgOutlineType', "solid");
                $this.set('pgOutlineWidth', 2);
                $this.set('pgOutlineColor', "#000");
            }
        },
        isFirstTime : true,
		updateFeatureStyles: function(e, cancelUpdate){
			var $this = this,
			graphic = $this.get('styleFeature');

			if ( !graphic ) {
				var msg = "App could not identify the selected feature. Please try again.";
				nisis.ui.displayMessage(msg, '', 'error');
				$this.clearStylesForm();
				return;
			}

			var layer = graphic.getLayer(),
			symbol = null,
			style = layer.id,
			value = 9000,
			oid = graphic.attributes.OBJECTID;

			value += graphic.attributes.OBJECTID;

			var userStyle = null;
			//CC - NISIS-2274: Saving the original style in memory to put it back in the case the user clicks the X for closing the window without saving it.
			if (!cancelUpdate && $this.isFirstTime){
				$this.oldStyle = $this.getOldStyle(layer.id, oid);
				$this.isFirstTime = false;
			}

			if (cancelUpdate && $this.oldStyle != null){
				userStyle = $this.oldStyle;
			}else{
				if(graphic && graphic.geometry.type === 'point'){
                    // console.log("symbol from updateFeatureStyles:", graphic.symbol)
                    // FB: NISIS-3012 - won't redraw on save and apply/save
                    //  w/o passing symbol as arg. Not clear why.
                    symbol = $this.getPointStyle(graphic.symbol);
                    // symbol = $this.getPointStyle();
				} else if (graphic && graphic.geometry.type === 'polyline'){
					symbol = $this.getPolylineStyle();
				} else if (graphic && graphic.geometry.type === 'polygon'){
					symbol = $this.getPolygonStyle();
				} else {
					nisis.ui.displayMessage('Unknown geometry type.', '', 'error');
					return;
				}

				userStyle = {
					layer: layer.id,
					oid: oid,
					value: value,
					label: 'Custom styles',
					symbol: symbol
				};
			}

			//check for layer's renderer
			if ( !renderers[style] ) {
				renderers[style] = {};
			}
			//check for custom styles
			if ( !renderers[style].styles ) {
				renderers[style].styles = [];
			}

			//new style
			var len = renderers[style].styles.length;

			//Remove the old style
			var exist = false,
			index = null;

			if( len > 0 ) {
				$.each(renderers[style].styles, function(i, s){
					//console.log('Style # ' + i + ": ", s);
					if(s.oid === oid && s.layer === layer.id){
						exist = true;
						index = i;
						renderers[style].styles[index] = userStyle;
						return false;
					}
				});
			}

			if( !exist ) {
				renderers[style].styles.push(userStyle);
			}

			//apply new styles to selected graphic
			layer.refresh();

			//clear style form
			// $this.clearStylesForm();
		},
        getOldStyle: function (layerId, oid){
            var symbol = null;
            // KOFI: NISIS-2245, 2339: Skip function if User layers.
            var r = renderers[layerId];
            if (r){
                $.each(renderers[layerId].styles, function(i, s){
                    if(s.oid === oid && s.layer === layerId){
                        symbol = renderers[layerId].styles[i];
                        return false;
                    }
                });
            }
            return symbol;
        },
        getPointStyle: function(){
            console.log('getting point style')
            var symbol = new SimpleMarkerSymbol(),
            	s = this.get('ptMarkerType'),
            	ps = this.get('ptSize'),
            	fc = this.get('ptColor'),
            	ft = 1 - this.get('ptTrans'),
            	ot = this.get('ptOutlineType'),
            	ow = this.get('ptOutlineWidth'),
            	oc = this.get('ptOutlineColor');

            //FB: this is probably bad
            if(typeof arguments[0] === 'undefined'){
                return;
            }else{
                // s =
                symbol.setPath(arguments[0].path);
            }

            symbol.setStyle(s);
            symbol.setSize(ps);


            fc = new Color(fc);
            fc = fc.toRgba();
            fc[3] = ft;
            symbol.setColor(new Color(fc));

            var outline = new SimpleLineSymbol();

            outline.setStyle(ot);
            outline.setWidth(ow);
            outline.setColor(new Color(oc));

            symbol.setOutline(outline);
            // symbol.setOpacity(ft);

            //console.log('Get Point Style: ', symbol);
            return symbol;
        },
        setPointStyle: function(symbol){
            // console.log('Current Point Symbol: ', symbol);
            var $this = this,
                p = symbol.path,
                s = symbol.style,
                ps = symbol.size,
                fc = symbol.color,
                // ft = symbol.color.a ? 1 - symbol.color.a : 0,
                os = symbol.outline.style,
                ow = symbol.outline.width,
                oc = symbol.outline.color;

                if (typeof symbol.color.a !== 'undefined'){
                    ft = 1 - symbol.color.a;
                }else{
                    ft = 0;
                }

                // NISIS-3012
                // FB: to be continued
                // if(typeof fc.length === "undefined"){
                //     console.log('obj: returning')
                //     return;
                // }else{
                //     console.log('array: processing')
                //     var fc_obj = {},
                //         fc_opts = {
                //             0: 'r',
                //             1: 'g',
                //             2: 'b',
                //             3: 'a'
                //         };
                //     fc.forEach(function(an_fc, i){
                //         if(typeof an_fc !== "number" || i >= 4){
                //             return;
                //         }else{
                //             fc_obj[fc_opts[i]] = fc[1];
                //         }
                //     })
                //     var fc = fc_obj;
                // }

            //needs to be commented out but breaks toLowerCase() call
            //FB: NISIS-3012
            fc = fc.toHex();
            //CC - This converts the color object into an hex String which is the correct format to use to be able to edit the feature
            oc = oc.toHex();

            // FB: settings from data
            // if ( s === 'path' ) {
            //     s = 'circle';
            //     ps = 15;
            //     fc = '#fff';
            //     ft = 0;
            //     os = 'solid';
            //     ow = 1;
            //     oc = "#000";
            // }

            $this.set('ptMarkerType', s);
            $this.set('ptSize', ps);
            $this.set('ptColor', fc);
            $this.set('ptTrans', ft);
            $this.set('ptOutlineType', os);
            $this.set('ptOutlineWidth', ow);
            $this.set('ptOutlineColor', oc);
            //console.log('Style form attr: ', s, ps, fc, ft, os, ow, oc);
        },
        getPolylineStyle: function(){
            var symbol = new SimpleLineSymbol(),
            	s = this.get('plType'),
            	w = this.get('plWidth'),
            	c = this.get('plColor'),
            	t = 1 - this.get('plTrans');

            symbol.setStyle(s);
            symbol.setWidth(w);

            var color = new Color(c);
            color = color.toRgba();
            color[3] = t;
            symbol.setColor(new Color(color));
            //console.log('Get Polyline Style: ', symbol);
            return symbol;
        },
        setPolylineStyle: function(symbol){
            //console.log('Current Polyline Symbol: ', symbol);
            var $this = this,
                s = symbol.style,
                w = symbol.width,
                c = symbol.color;

                if (typeof symbol.color.a !== 'undefined'){
                    t = 1 - symbol.color.a;
                }else{
                    t = 0;
                }

            var col = c.toHex();

            $this.set('plType', s);
            $this.set('plWidth', w);
            $this.set('plColor', col);
            $this.set('plTrans', t);
        },
        getPolygonStyle: function(){
            var symbol = new SimpleFillSymbol(),
            	s = this.get('pgFillType'),
            	fc = this.get('pgFillColor'),
            	ft = 1 - this.get('pgFillTrans'),
            	os = this.get('pgOutlineType'),
            	ow = this.get('pgOutlineWidth'),
            	oc = this.get('pgOutlineColor');

            symbol.setStyle(s);

            fc = new Color(fc);
            fc = fc.toRgba();
            fc[3] = ft;
            symbol.setColor(new Color(fc));

            var outline = new SimpleLineSymbol();
            outline.setStyle(os);
            outline.setWidth(ow);
            outline.setColor(new Color(oc));

            symbol.setOutline(outline);

            // console.log('Get Polygon Style: ', symbol);
            return symbol;
        },
        setPolygonStyle: function(symbol){
            // console.log('Current Polygon Symbol: ', symbol);
            var $this = this,
                s = symbol.style,
                fc = null,
                ft = 0,
                os = symbol.outline.style,
                ow = symbol.outline.width,
                oc = symbol.outline.color;
                oc = oc.toHex();

            $this.set('pgFillType', s);

            if(s === 'solid' && symbol.color){
                //fc = 'rgba(' + symbol.color.r + ',' + symbol.color.g + ',' + symbol.color.b + ',' + symbol.color.a + ')';

                fc = symbol.color.toHex();
                if (typeof symbol.color.a !== 'undefined'){
                    ft = 1 - symbol.color.a;
                }else{
                    ft = 0;
                }

            } else {
                fc = "#fff";
            }

            $this.set('pgFillColor', fc);
            $this.set('pgFillTrans', ft);

            $this.set('pgOutlineType', os),
            $this.set('pgOutlineWidth', ow),
            $this.set('pgOutlineColor', oc);

            // console.log('Style form attr: ', s, fc, ft, os, ow, oc);
        },
        saveFeatureStyles: function(e){
            //nisis.ui.displayMessage("Not Implemented yet. Yes really :)", "", "error");
            var $this = this,
                graphic = $this.get('styleFeature'),
                layer = graphic.getLayer(),
                graphicType = null,
                graphicData;

            if(graphic && graphic.geometry.type === 'point'){
                graphicType = 'point';
                graphicData = {
                    symType: graphicType,
                    objid: graphic.attributes.OBJECTID,
                    // FB:
                    symStyle: this.get('ptMarkerType'),
                    symSize: this.get('ptSize'),
                    symColor: this.get('ptColor'),
                    symTrans: this.get('ptTrans'),
                    outStyle: this.get('ptOutlineType'),
                    outWidth: this.get('ptOutlineWidth'),
                    outColor: this.get('ptOutlineColor')
                };

            } else if (graphic && graphic.geometry.type === 'polyline'){
                graphicType = 'polyline';
                graphicData = {
                    symType: graphicType,
                    objid: graphic.attributes.OBJECTID,
                    symStyle: this.get('plType'),
                    outWidth: this.get('plWidth'),
                    outColor: this.get('plColor'),
                    outTrans: this.get('plTrans')
                };

            } else if (graphic && graphic.geometry.type === 'polygon'){
                graphicType = 'polygon';
                graphicData = {
                    symType: graphicType,
                    objid: graphic.attributes.OBJECTID,
                    symStyle: this.get('pgFillType'),
                    symColor: this.get('pgFillColor'),
                    symTrans: this.get('pgFillTrans'),
                    outStyle: this.get('pgOutlineType'),
                    outWidth: this.get('pgOutlineWidth'),
                    outColor: this.get('pgOutlineColor')
                };

            } else {
                nisis.ui.displayMessage('Unknown geometry type.', '', 'error');
                return;
            }

            graphicData.task = 'create';
            graphicData.action = 'save_styles';
            graphicData.layer = layer.id;

            console.log("posting features:", graphicData);

            $.ajax({
                url: 'api/',
                type: 'POST',
                data: graphicData,
                success: function(data, status, xhr){
                    //update the feature
                    $this.updateFeatureStyles();

                    var res = JSON.parse(data),
                        msg = null;
                    console.log("some success message");
                    if(res['return'].toLowerCase() === 'success'){
                        msg = 'Feature styles has been saved.';
                        nisis.ui.displayMessage(msg, '', 'info');
                        //Closing the window after the style has been saved successfully.
                        $("#drawing-div").data('kendoWindow').close();
                    } else {
                        msg = 'Feature styles has not been saved.';
                        nisis.ui.displayMessage(msg, '', 'error');
                    }
                },
                error: function(xhr, status, err){
                	//console.log('Saving styles failed: ', err);
                    var msg = 'Feature styles has not been saved.';
                    nisis.ui.displayMessage(msg, '', 'error');
                }
            });


        },
        //upload files
        featFile: null,
        fileType: null,
        fileMsg: "",
        fileReady: false,
        checkFile: function(e){
            console.log(e);
            var $this = this;
            //return;
            var file, type,
            files = e.target.files,
            len = files.length;

            var validFiles = ['csv','xlsx','xls','kml','zip'];

            if(len > 0){
                file = files[0].name;

                type = file.split('.');
                //FB: 5/17
                console.log("extracting type", type);
                // type = type[type.length-1].toLowerCase();
                type = type[type.length-1];



                if($.inArray(type, validFiles) === -1){
                    this.set('fileMsg', 'Invalid file type.');
                    return;
                }

                if((file.size / 1024) > 100){
                    this.set('fileMsg', 'File too large');
                    return;
                }

                this.set('featFile', file);
                this.set('fileType', type);

                this.set('fileReady', true);
            } else {
                this.set('fileMsg', 'No file upladed.');
                this.set('fileReady', true);
            }
        },
        removeFile: function(e){
            console.log(this.get('featFiles').length);
        },
        fileLoaded: function(e){
            console.log(e);
        },
        clrearFile: function(e){
            console.log('Clear file: ', e);
            e.preventDefault();
        },
        processFile: function(e) {
            //e.preventDefault();
            //console.log(e.sender.files());
            //var files = e.sender.files();
            this.get('mapview').util.processInputFile(this.get('file'), this.get('type'));
        },
        //List
        fListSources: [{name:"Select a layer ...",value:""},{name:"U.S. States",value:"US_States"},
              {name:"U.S. Counties",value:"US_Counties"},{name:"U.S. Zip Codes",value:"US_Zip_Codes"}],
        fListLyr: "",
        setFListLayer: function(e){
            //reset the selected layer after change event is fired
            this.set('fListLyr', e.sender.value());
            this.checkListForm();
        },
        fList: "",
        fListReady: false,
        fListMsg: "",
        fListReady: false,
        checkListForm: function(e){
            var lyr = this.get('fListLyr'),
                list = this.get('fList');

            if($.trim(lyr) !== "" && $.trim(list) !== ""){
                this.set("fListReady", true);
            } else {
                this.set("fListReady", false);
            }
        },
        clearFeatList: function(e){
            this.set('fListLyr', "");
            this.set('fList', "");
            this.set('fListMsg', "");
            this.set('fListReady', false);
            this.checkListForm();
        },
        submitFeatList: function(e){
            var $this = this;
            $this.set("fListReady", false);

            var layer = this.get('fListLyr'),
            list = $this.get('fList').toString(),
            msg = $('#lt-processMsg');
            var url, fields, values = [], stateNames = [];
            //console.log(layer);, stateNames
            if(layer !== ''){
                url = nisis.ui.mapview.layers[layer].url;
            } else {
                $this.set('fListMsg', 'Select a valid layer');
                return;
            }

            if(layer.indexOf('States') !== -1){
                fields = ['STATE_NAME'];
            } else if (layer.indexOf('Counties') !== -1){
                fields = ['COUNTY'];
            } else if (layer.indexOf('Zip') !== -1){
                fields = ['ZIPCODE'];
            }
            if (list.length > 0) {
                //console.log("list = " + list.toString() + ", fields = " + fields.toString());
                list = list.toUpperCase();
                if (fields.toString().indexOf('COUNTY') != -1){
                    console.log('List: ', list);
                    if (list.indexOf(';') == -1 || list.lastIndexOf(';') < list.trim().length-1){
                        list += ';';
                    }
                    list = list.split(';');
                    for(var j=0, l=list.length-1; j<l; j++){
                        list[j] = list[j].split(',');
                    }
                    for(var j=0, l=list.length-1; j<l; j++){
                        //console.log("values" + j +", " + list[j][0]);
                        values.push(list[j][0]);
                    }
                    for(var j=0, l=list.length-1; j<l; j++){
                        //console.log("stateNames" + j +", " + list[j][1]);
                        stateNames.push(list[j][1]);
                    }
                } else {
                    if (list.indexOf(',') != -1){
                        list = list.split(',');
                        for(var j=0, l=list.length; j<l; j++){
                            values.push(list[j]);
                        }
                    } else {
                        values.push(list);
                    }
                }
                this.set("fListMsg", 'Processing ...');

                nisis.ui.mapview.util.findFeatures(url, fields, values)
                .then(function(data) {
                    console.log(data);
                    if(!data) {
                        return;
                    }
                    var features = data;
                    //Filter unique features only
                    if (fields.toString().indexOf('COUNTY') != -1){
                        var filteredData = [];
                        var stateAbbr = [];
                        stateAbbr = states.abbreviations();
                        var dataCountyName, dataStateName, stateNamesHolder;
                        for (var i = 0; i<data["length"]; i++){
                            for (var j = 0; j<stateNames["length"]; j++){
                                console.log(data[i].feature);
                                dataStateName = $.trim(data[i].feature.attributes["STATE NAME"]).toUpperCase();
                                dataCountyName = $.trim(data[i].feature.attributes["COUNTY"]).toUpperCase();

                                stateNamesHolder = "";
                                if (stateNames[j]) {
                                    stateNamesHolder = stateNames[j].trim().toUpperCase();
                                    if ((dataStateName.indexOf(stateNames[j].trim()) > -1 ||
                                            dataStateName.indexOf(stateAbbr[stateNamesHolder]) > -1)
                                         && dataCountyName.indexOf(values[j].trim()) > -1   ){
                                        filteredData.push(data[i]);
                                    }
                                } else {
                                    filteredData.push(data[i]);
                                }
                            }
                        }
                        features = filteredData;
                    }
                    //add results to map. Should be in Graphic layers
                    nisis.ui.mapview.util.showFindFeaturesResults(features);
                    $this.set("fListMsg",'Done. App retreived ' + data.length + ' feature(s)');
                    $this.set("fListReady", true);

                },function(error){
                    console.log(error);
                    $this.set("fListMsg", error);
                    $this.set("fListReady", true);
                },function(p){
                    $this.set("fListMsg", p);
                });
            }
        }
    });
});
