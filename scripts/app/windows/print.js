/*
 * print dialog
 */

define([],function(){
    return kendo.observable({
        test: 'Print widget is loaded',
        printMsg: "",
        printOutput: '',
        printName: "",
        printReady: false,
        title: "",
        pageLayout: "",
        pageLayouts: [{label:"Page Layout",value:""},{label:"A3 Landscape",value:"A3 Landscape"},{label:"A3 Portrait",value:"A3 Portrait"},
            {label:"A4 Landscape",value:"A4 Landscape"},{label:"A4 Portrait",value:"A4 Portrait"},
            {label:"Letter ANSI A Landscape",value:"Letter ANSI A Landscape"},{label:"Letter ANSI A Portrait",value:"Letter ANSI A Portrait"},
            {label:"Tabloid ANSI B Landscape",value:"Tabloid ANSI B Landscape"},{label:"Tabloid ANSI B Portrait",value:"Tabloid ANSI B Portrait"},
            {label:"MAP ONLY",value:"MAP ONLY"}],
        fileFormat: "",
        fileFormats: [{label:"File Format",value:""},{label:"PDF",value:"PDF"},{label:"Image JPG",value:"JPG"},
            {label:"Image GIF",value:"GIF"},{label:"Image PNG8",value:"PNG8"},{label:"Image PNG32",value:"PNG32"},
            {label:"Image EPS",value:"EPS"},{label:"Image SVG",value:"SVG"},{label:"Image SVGZ",value:"SVGZ"}],
        isReady: false,
        setPrint: function(e){
            //console.log('Changing print options: ',e ? e : "");
            var title = this.get('title'),
            layout = this.get('pageLayout'),
            format = this.get('fileFormat');
            
            if(title.length > 0 && layout !== "" && format !== ""){
                this.set('isReady', true);
            }
            else {
                this.set('isReady', false);
            }
        },
        resetPrint: function(){
            this.set('title','');
            this.set('pageLayout','');
            this.set('fileFormat','');
            this.set('isReady',false);
        },
        print: function(e){
            var $this = this;
            this.set('printOutput', '');
            this.set('printName', '');
            this.set('isReady',false);
            var sec = 0,
            pOptions = {
                title: $this.get('title'),
                page: $this.get('pageLayout'),
                file: $this.get('fileFormat')
            },
            updateMsg = setInterval(function(){
                sec++;
                $this.set('printMsg', 'Processing ... ' + sec);
            },1000);
            //initiate print task
            nisis.ui.mapview.util.generatePrintFile(pOptions)
            .done(function(f){
                clearInterval(updateMsg);
                //get the link from the result and the name of the file
                var link = f.url, name = 'Printout File',
                title = $this.get('title');
                if(title.length > 0){
                    name = title;
                }
                //display result as a link
                $this.set('printMsg','Done processing. (' + sec + ' seconds)');
                $this.set('printOutput', link);
                $this.set('printName', name);
                $this.set('printReady', true);
                //enable the print button
                $this.resetPrint();
            }).fail(function(e){
                clearInterval(updateMsg);
                $this.set('printMsg', e.message);
                $this.set('isReady',true);
            }).progress(function(p){
                $this.set('printMsg', p);
            });
        }
    });
});