/*
 * TFR Builder ==> Model
*/
define([], function() {
	return {
		tfrTypes: function () {
			var types = [
				{
					'id': 0,
					'type': 'Select TFR type',
					'description': "Undefined",
					'ref': '',
					'value': '',
					'details': '',
					'instructions': []
				},{
					'id': 1,
					'type': "91.137(a)(1) HAZARD (surface hazard)",
					'description': "To protect persons and property in the air or on the surface from an existing or imminent hazard associated with an incident on the surface when the presence of low flying aircraft would magnify, alter, spread, or compound that hazard. This is the most restrictive of TFR's and is rarely issued for wildland fire incidents. It is more commonly used for the following: <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Toxic gas leaks, spills, fumes from flammable agents <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Volcano eruptions <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Nuclear accident or incident <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Hijacking incidents <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Aircraft accident sites at the discretion of the FAA",
					'ref': '91.137(a)(1)',
					'value': 'HAZARDS',
					'details': 'surface hazard',
					'instructions': [1]
				},{
					'id': 2,
					'type': "91.137(a)(2) HAZARD (wildfires, disaster relief)",
					'description': "To provide a safe environment for the operation of disaster relief aircraft. This is the most common of TFR's for land management agencies that deal wildland fires. It includes but is not limited to: <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Wildland fires which are being fought by aviation resources <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Aircraft relief activities following a disaster (earthquake, tidal wave, flood, hurricane, etc) <br/>&nbsp;&nbsp;&nbsp;&nbsp; &#149; Aircraft accident sites",
					'ref': '91.137(a)(2)',
					'value': 'HAZARDS',
					'details': 'wildfires, disaster relief',
					'instructions': [1]
				},{
					'id': 3,
					'type': "91.137(a)(3) HAZARD (restrict sightseeing)",
					'description': "Disaster/hazard incidents of limited duration that would attract an unsafe congestion of sightseeing aircraft, such as aircraft accident sites.",
					'ref': '91.137(a)(3)',
					'value': 'HAZARDS',
					'details': 'restrict sightseeing',
					'instructions': [1]
				},{
					'id': 4,
					'type': "91.141 VIP",
					'description': "To protect the President, Vice President, or other public figures.",
					'ref': '91.141',
					'value': 'VIP',
					'details': '',
					'instructions': [3,4,5,6]
				},{
					'id': 5,
					'type': "91.145 AIRSHOW",
					'description': "To provide a safe environment for aircraft operations associated with an airshow.",
					'ref': '91.145',
					'value': 'AIRSHOW',
					'details': '',
					'instructions': [7]
				},{
					'id': 6,
					'type': "99.7 SECURITY",
					'description': "To provide special security instructions to pilots.",
					'ref': '99.7',
					'value': 'SECURITY',
					'details': '',
					'instructions': [2]
				},{
					'id': 7,
					'type': "94.143 SPACE OPERATIONS",
					'description': "Space operations area: aircraft operations are prohibitied.",
					'ref': '94.143',
					'value': 'SPACE OPERATIONS',
					'details': '',
					'instructions': [8]
				}
			];

			return types;
		},
		tfrReasons: function() {
			var types = [
				{
					id: 0,
					name: "- None -",
					value: ""
				},{
					id: 1,
					name: "National Security",
					value: "National Security"
				},{
					id: 2,
					name: "NFL Super Bowl",
					value: "NFL Super Bowl"
				}
			];

			return types;
		},
		facTypes: function() {
			var types = [
				{
					id: 0,
					name: "ARTCC",
					value: "AFSS"
				},{
					id: 1,
					name: "AFSS",
					value: "AFSS"
				},{
					id: 2,
					name: "AIFSS",
					value: "AIFSS"
				}
			];

			return types;
		},
		tfrList: function() {
			var tfrs = [
				{
					id: 0,
					name: 'Select existing TFR',
					type: 1,
					Description: 'Testing ...'
				}, {
					id: 1,
					name: 'TFR 0',
					type: 1,
					Description: 'Testing ...'
				}
			];

			return tfrs;
		},
		getArtccs: function() {
			return [
				{
					id: 0,
					artcc: 'Select ARTCC',
					name: 'Select ARTCC Name'
				},{
					id: 1,
					artcc: 'ZDC',
					name: 'Washington Center'
				},{
					id: 2,
					artcc: 'ZNY',
					name: 'New York Center'
				},{
					id: 3,
					artcc: 'ZMA',
					name: 'Miami Center'
				}
			];
		},
		timeZones: function() {
			return [
				{
					id: 0,
					name: 'UTC (0)',
					value: 0
				},{
					id: 1,
					name: 'AKST (-9) - Alaska Standard',
					value: -9
				},{
					id: 2,
					name: 'AST (-4) - Atlantic Standard',
					value: -4
				},{
					id: 3,
					name: 'CST (-6)',
					value: -6
				},{
					id: 4,
					name: 'EST (-5)',
					value: -5
				},{
					id: 5,
					name: 'HST (-10)',
					value: -10
				},{
					id: 6,
					name: 'MST (-7)',
					value: -7
				},{
					id: 7,
					name: 'PST (-8)',
					value: -8
				},{
					id: 8,
					name: 'Guam (10)',
					value: 10
				},{
					id: 9,
					name: 'AKDT (-8) - Alaska Daylight',
					value: -8
				},{
					id: 10,
					name: 'ADT (-3) - Atlantic Daylight',
					value: -3
				},{
					id: 11,
					name: 'CDT (-5)',
					value: -5
				},{
					id: 12,
					name: 'EDT (-4)',
					value: -4
				},{
					id: 13,
					name: 'MDT (-6)',
					value: -6
				},{
					id: 14,
					name: 'PDT (-7)',
					value: -7
				}
			];
		},
		getCities: function() {
			return [
				{
					id: 0, 
					city: 'Select a city',
					state: ''
				},{
					id: 1, 
					city: 'Boston',
					state: 'Massachussets'
				},{
					id: 2, 
					city: 'New York',
					state: 'New York'
				},{
					id: 3, 
					city: 'Albany',
					state: 'New York'
				},{
					id: 4, 
					city: 'Niagara Falls',
					state: 'New York'
				},{
					id: 5, 
					city: 'Washington',
					state: 'District of Columbia'
				}
			];
		},
		getStates: function() {
			return [
				{
					id: 0, 
					state: 'Select a state'
				},{
					id: 1, 
					state: 'Massachussets'
				},{
					id: 2, 
					state: 'New York'
				},{
					id: 3, 
					state: 'District of Columbia'
				}
			];
		},
		getAreaInstructions: function(options) {
			var insts = [],
			instructions = [
				{ //TFR - HAZARDs
					id: 1,
					name: "Standard",
					value: "standard",
					options: [
						"Only relief aircraft operations under the direction of ARTCC are authorized in the airspace."
					]
				},{  //TFR - Security
					id: 2,
					name: "Standard",
					value: "standard",
					options: [
						"Except as specified below and/or unless authorized by ATC: ",
						"1. All aircraft entering or exiting the TFR must be on an active IFR or VFR flight plan with a discrete code assigned by an Air Traffic Control (ATC) facility.  Aircraft must be squawking the discrete code prior to departure and at all times while in the TFR. ",
						"2. All aircraft entering or exiting the TFR must remain in two-way radio communications with ATC."
					]
				},{ // TFR - VIP
					id: 3,
					name: "Prohibited Area / Inner Core",
					value: "prohibited_area",
					options: [
						"Except as specified below and/or unless authorized by ATC in consultation with the air traffic security coordinator via the domestic events network (DEN): ",
						"A. ALL AIRCRAFT OPERATIONS WITHIN THE 10 NMR AREA(S) LISTED ABOVE, KNOWN AS THE INNER CORE(S), ARE PROHIBITED EXCEPT FOR: MILITARY AIRCRAFT DIRECTLY SUPPORTING THE UNITED STATES SECRET SERVICE (USSS) AND THE OFFICE OF THE PRESIDENT OF THE UNITED STATES. COORDINATED AND APPROVED LAW ENFORCEMENT, AIR AMBULANCE AND FIREFIGHTING OPERATIONS MUST RECEIVE APPROVAL PRIOR TO ENTERING THIS AIRSPACE AT XXX-XXX-XXXX TO AVOID POTENTIAL DELAYS. REGULARLY SCHEDULED COMMERCIAL PASSENGER AND ALL-CARGO CARRIERS OPERATING UNDER ONE OF THE FOLLOWING TSA-APPROVED STANDARD SECURITY PROGRAMS/PROCEDURES: AIRCRAFT OPERATOR STANDARD "
					]
				},{
					id: 4,
					name: "Restricted Area / Outer Core",
					value: "restricted_area",
					options: [
						"B. For operations within the airspace between the 10 nmr and 30 nmr area(s) listed above, known as the outer ring(s): All aircraft operating within the outer ring(s) listed above are limited to aircraft arriving or departing local airfields, and workload permitting, ATC may authorize transit operations. Aircraft may not loiter. All aircraft must be on an active IFR or VFR flight plan with a discrete code assigned by an air traffic control (ATC) facility. Aircraft must be squawking the discrete code prior to departure and at all times while in the TFR and must remain in two-way radio communications with ATC. ",
						"C. THE FOLLOWING OPERATIONS ARE NOT AUTHORIZED WITHIN THIS TFR: FLIGHT TRAINING, PRACTICE INSTRUMENT APPROACHES, AEROBATIC FLIGHT, GLIDER OPERATIONS, SEAPLANE OPERATIONS, PARACHUTE OPERATIONS, ULTRALIGHT, HANG GLIDING, BALLOON OPERATIONS, AGRICULTURE/CROP DUSTING, ANIMAL POPULATION CONTROL FLIGHT OPERATIONS, BANNER TOWING OPERATIONS, SIGHTSEEING OPERATIONS, MAINTENANCE TEST FLIGHTS, RADIO CONTROLLED MODEL AIRCRAFT OPERATIONS, MODEL ROCKETRY, UNMANNED AIRCRAFT SYSTEMS (UAS), AND UTILITY AND PIPELINE SURVEY OPERATIONS.",
						"D. FAA recommends that all aircraft operators check notams frequently for possible changes to this TFR prior to operations within this region. "
					]
				},{
					id: 5,
					name: "VPOTUS",
					value: "vpotus",
					options: [
						"Except the flight operations listed below: ",
						"1. All IFR arrivals or departures to/from airports within this TFR. ",
						"2. Approved; law enforcement, fire fighting, military aircraft directly supporting the United States Secret Service (USSS) and the office of the Vice President of the United States, and MEDEVAC/air ambulance flights. ",
						"3. Aircraft operations necessitated for safety or emergency reasons. ",
						"4. Aircraft that receive ATC authorization in consultation with the air traffic security coordinator (ATSC) via the domestic events network(DEN). "
					]
				},{
					id: 6,
					name: "Remarks",
					value: "remarks",
					options: [
						""
					]
				},{ //TFR - Airshow
					id: 7,
					name: "Standard",
					value: "standard",
					tfrs: [4],
					options: [
						"Unless authorized by ATC."
					]
				},{ //TFR - Space Operations
					id: 8,
					name: "Standard",
					value: "standard",
					options: [
						"Space operations area: aircraft operations are prohibitied."
					]
				}
			];

			$.each(instructions, function() {
				if ($.inArray(this.id, options) !== -1) {
					insts.push(this);
				}
			});

			return insts;
		}
	};
});