/*
 * => Directions and Locations
 */

define(["libs/latlong"], function(LatLon) { 

	return kendo.observable({
		//mapview: nisis.ui.mapview,
		locMsg: "",
		streetAddress: "",
		city: "",
		state: "",
		zipcode: "",
		country: "",
		useMapExtent: false,
		btnEnabled: true,
		showResults: false,
		countResults: 0,
		geocodeAddress: function(e) {
			var $this = this;

			$this.set('btnEnabled', false);
			$this.set('locMsg', 'Processing your request ...');

			var	mapview = nisis.ui.mapview,
				street = this.get('streetAddress'),
				city = this.get("city"),
				state = this.get("state"),
				zipcode = this.get("zipcode"),
				country = this.get("country"),
				extent = this.get('useMapExtent'),
				params = {},
				address = {
					Street: street,
					City: city,
					State: state,
					Zone: zipcode
				},
				input = "";

			if (street !== "") {
				input += street;
			}

			if (city !== "" && input !== "") {
				input += ", " + city;
			} else if (city !== "" && input == "") {
				input += city;
			}

			if (state !== "" && input !== "") {
				input += ", " + state;
			} else if (state !== "" && input == "") {
				input += state;
			}

			if (zipcode !== "" && input !== "") {
				input += ", " + zipcode;
			} else if (zipcode !== "" && input == "") {
				input += zipcode;
			}

			if (country !== "" && input !== "") {
				input += ", " + country;
			} else if (country !== "" && input == "") {
				input += country;
			}

			if (input === "") {
				$this.set('btnEnabled', true);
				$this.set('locMsg', 'Invalid inputs');
				return;
			}
			//console.log('Input:', input);
			//params.address = address;
			params.address = {
				"SingleLine": input
			};
			params.outFields = ["*"];

			if (extent) {
				params.searchExtent = mapview.map.extent
			}

			mapview.locator.addressToLocations(params)
			.then(function(candidates) {
				$this.set('btnEnabled', true);

				if (candidates && candidates.length) {
					$this.set('locMsg', 'Processing results ...');

					$this.processLocations.call($this, candidates);
				} else {
					$this.set('locMsg', 'The geocoding service could not find any matching place.');
				}
			
			}, function(error) {
				$this.set('btnEnabled', true);
				$this.set('locMsg', 'The geocoding service failed to process your input.');
				console.log('Location error: ', error);
			});
		},
		resetAddress: function(e) {
			this.set('locMsg',"");
			this.set("streetAddress", "");
			this.set("city", "");
			this.set("state", "");
			this.set("zipcode", "");
			this.set("country", "");
			this.set("useMapExtent", false);

			$('#geocode-results').empty();
			this.set('showResults', false);
			this.set('countResults', 0);
		},
		processLocations: function(candidates) {
			// console.log('Geocode locations:', candidates.length);
			// console.log('Geocode locations:', candidates);

			var $this = this,
				results = $('#geocode-results');

			if (!results) {
				$this.set('locMsg', 'App could not find the results container');
				return;
			}

			if ( $this.get('showResults') ) {
				$this.set('showResults', false);
			}

			results.empty();

			$.each(candidates, function(i, l) {
				//exclude location with lower scores
				if (l.score && l.score < 70) {
					return;
				}

				var loc = $("<li/>", {
					"class": "geo-location",
					"text": l.address + '.',
					"data-lat": l.attributes.Y,
					"data-lon": l.attributes.X
				});

				var index = $("<span/>", {
					"class": "ft-b marg",
					"text": (i+1) + ':'
				});
				var sep = $("<span/>", {
					"class": "marg",
					"text": ' - '
				});

				var locate = $("<i/>", {
					"class": "marg pointer fa fa-crosshairs",
					"title": "Fly to location"
				})
				.on('click', function(evt) {
					$this.locateAddress.call($this, evt);
				});

				var copy = $("<i/>", {
					"class": "marg pointer fa fa-copy",
					"title": "Copy to clipboard"
				})
				.on('click', function(evt) {
					$this.copyAddress.call($this, evt);
				});

				var add = $("<i/>", {
					"class": "marg pointer fa fa-plus",
					"title": "Add point to the map"
				})
				.on('click', function(evt) {
					$this.addAddress.call($this, evt);
				});

				var remove = $("<i/>", {
					"class": "marg pointer fa fa-trash-o",
					"title": "Remove point from the list"
				})
				.on('click', function(evt) {
					$this.removeAddress.call($this, evt);
				});

				results.append(loc);

				var lat = l.attributes.Y,
					lon = l.attributes.X,
					dd = " DD (" + lat + ", " + lon + ")",
					dms = new LatLon(lat, lon).toString('dms',2);
					dms = " - DMS (" + dms + ").";

				loc.append(dd);
				loc.append(dms);

				loc.prepend(sep);
				loc.prepend(remove);
				loc.prepend(add);
				loc.prepend(copy);
				loc.prepend(locate);
				loc.prepend(index);
			});

			$this.set('showResults', true);
			$this.set('countResults', candidates.length);
			$this.set('locMsg', 'Done. See results below.');
		},
		locateAddress: function(evt) {
			//console.log('Locate Address:', evt);
			var $this = this,
				li = $(evt.target).parent(),
				lat = li.data('lat'),
				lon = li.data('lon');
			
			if (lat && lon) {
				//center and flash at location
				nisis.ui.mapview.util.flashAtLocation([lon, lat], true);
			} else {
				$this.set('locMsg', 'App could not find the location details');
			}
		},
		copyAddress: function(evt) {
			//console.log('Locate Address:', evt);
			var $this = this,
				li = $(evt.target).parent(),
				lat = li.data('lat'),
				lon = li.data('lon');

			if (!lat || !lon) {
				$this.set('locMsg', 'App could not find the location details');
				return;
			}

			require(['app/windows/shapetools']
				,function(shapetools, webMercatorUtils){

				var ll = lat + "," + lon;

				shapetools.set('copiedCoordinates', ll);
				//fire copy event
				$('#dt-copy-coords').select();
				$('#dt-copy-coords').trigger('copy');
			});
		},
		addAddress: function(evt) {
			//console.log('Add Address:', evt);
			var $this = this,
				app = null,
				li = $(evt.target).parent(),
				lat = li.data('lat'),
				lon = li.data('lon');

			if (!lat || !lon) {
				$this.set('locMsg', 'App could not find the location details');
				return;
			}

			require(["app/appConfig"], function(appConfig) {
				app = appConfig.get('app');
			});

			if (app) {
				var opts = {
					"type": "point",
					"data": [[lon, lat]]
				};

				nisis.ui.mapview.util.createGeometry(opts, app);
			} else {
				nisis.ui.mapview.util.createGeometry(opts);
				$this.set('locMsg', 'App could not identify the current module.');
			}
		},
		removeAddress: function(evt) {
			//console.log('Remove Address:', evt);
			var $this = this,
				li = $(evt.target).parent(),
				count = $this.get('countResults');

			li.remove();
			count--;

			$this.set('countResults', count);
			//CC - NISIS 2119: Updating the results list enumeration after an element from the list is removed.
			$this.updateResultListEnumeration();
		}, 
		updateResultListEnumeration: function(){
			var results = $('#geocode-results span.ft-b.marg')

			$.each(results, function(i, item) {
				$(item).text(i+1);
			});
		}
	});
});