/*
 * Features.js
 * This file was created with the purpose of binding the 'features'
 * kendo object and the javascript kendo widgets code with 'features.php'
 * file which contains the GUI for My Features/Group features/Folders module.
 * Contributions by: KH, JS, FB, IB
 */

define(["app/windows/shapesManager"], function(shapesManager) {
	features = kendo.observable({
		dragStartParentID: null,
		dragEndParentID: null,
		dragItemID: null,
		dragItemIsShared: 'false',
		userOrGroupTab: 'user', // It's either USER's feature tab or GROUP's feature tab
		selectedGroup : "-1",
		checkedCnt: 0,
		selectedNodes: [],
		userGroupPriv: 1,
		newShapeIdCreated: null,
		featureMgmtLayers: ["Point_Features", "Line_Features", "Polygon_Features", "text_features"],
		waitForRenderComplete: 0,
		waitAndOpenEditTries: 10,
		// KOFI: DO NOT REMOVE THIS VARIABLE "shallWeRefreshMap" AND MAKE IT A FUNCTION PARAMETER
		// Tree refresh happens asynchronously with the read() function.
		// Needs to access this variable after tree refresh to decide if map should also be refreshed.
		shallWeRefreshMap: true,
		refreshTreesAndByPassMap: false,
		enableButton: function (btn) {
			// FB 5/3/2016 - below not necessary if you set style for :disabled
			// $("#" + btn).css('color','#000');
			$("#" + btn).prop("disabled", false);
		},
		disableButton: function (btn) {
			// FB 5/3/2016 - below not necessary if you set style for :disabled
			// $("#" + btn).css('color','#CCC');
			$("#" + btn).prop("disabled", true);
		},
		disableGroupMenuButtons: function (){
			this.disableButton('fmcreatefolder2');
			this.disableButton('fmedit2');
			this.disableButton('fmdel2');
			this.disableButton('fmshare2');
			this.disableButton('fmFlyTo2');
			this.disableButton('fmrefresh2');
		},
		disableUserMenuButtons: function (){
			this.disableButton('fmcreatefolder');
			this.disableButton('fmedit');
			this.disableButton('fmdel');
			this.disableButton('fmshare');
			this.disableButton('fmFlyTo');
			// this.disableButton('fmrefresh');
		},
		// FB - 4/21/2016 - optimizing
		tabRefreshes: {
			user: "myFeaturesDataSource",
			group: "groupFeaturesDataSource"
		},
		//FB - 4/21/2016 - optimizing
		tabSelects: {
			user: "#myFeaturesTree",
			group: "#groupFeaturesTree",
			manager: "activateShapesManagementTool"
		},
		treeInitialLoadDone: {
			user: false,
			group: false
		},
		// called from:
		// 1. User clicking on Refresh button in features.php ==> Refresh the map
		// 2. User clicking on another tab ==> Don't refresh map
		refreshData : function (evt){
			this.shallWeRefreshMap = false;
			// Force map refresh if command coming from user clicking refresh buttons.
			if (evt && (evt.currentTarget.id === "fmrefresh" || evt.currentTarget.id === "fmrefresh2")){
				this.shallWeRefreshMap = true;
			}

			if (this.userOrGroupTab === "group" && $("#groupsDropDown").val() === ""){
				return;
			}
			var selItem = this.getSelectedItem();
			var selItemId = null;
			// If selected item is undefined, simply refresh the tab AND then highlight the root ("0")
			if (selItem){
				selItemId = selItem.ITEMID;
			}
			this.refreshCurrentTab(selItemId);
		},
		refreshCurrentTab: function (parentId){
			var userOrGroup = this.userOrGroupTab;

			//FB - 5/17/2016 - testing switch
			if(userOrGroup === "manager"){
				shapesManager.activateShapesManagementTool();
				//$("#shapes-manager-pager").resize();
				$("#shapes-manager-list").resize();
			}else{
				//Saving the nodes to be expanded after the datasource read (treeview refresh)
				//The nodes will be expanded every time the datasource is being read. See datasource read() for details
				this.saveNodesToExpandAndCheck(userOrGroup, parentId);
				this[this.tabRefreshes[this.userOrGroupTab]].read();
			}
		},
		refreshShapesOnMap: function(){
			// Need this logic here to prevent MAP REFRESH when user clicks to another tab.
			// TAB CHANGE IS NOT DATA CHANGE, and therefore no need for MAP REFRESH!
			if (!this.shallWeRefreshMap){
            	// set value back to default.
            	this.shallWeRefreshMap = true;
				return;
			}
			nisis.user.styles = [];

		    for (var i = 0; i < this.featureMgmtLayers.length; i++) {
		    	var lyr = nisis.ui.mapview.map.getLayer(this.featureMgmtLayers[i]);
		    	if (lyr){
					//nisis.user.userVisibleFeatures[this.getItemTypeByLayerId(this.featureMgmtLayers[i])] = "-1";
                	this.refreshShapesOnMapLayer(lyr);
                }
            }
            if(nisis.user.styles.length){
                require(["app/renderers"], function(renderers){
                    renderers.buildCustomStyles(nisis.user.styles);
                });
            }
            // set value back to default.
            this.shallWeRefreshMap = true;
		},
		refreshShapesOnMapLayer: function(shapeLayer){
			if (!shapeLayer)
				return;

			var query = "";
			var checkedShapeNodeIds = [];
			var itemtype = this.getItemTypeByLayerId(shapeLayer.id);
			nisis.user.userVisibleFeatures[itemtype] = "-1";
			this.getCheckedShapeNodeIdsForLayer("user", this.myFeaturesDataSource.view(), shapeLayer, checkedShapeNodeIds);
			this.getCheckedShapeNodeIdsForLayer("group", this.groupFeaturesDataSource.view(), shapeLayer, checkedShapeNodeIds);

			if (checkedShapeNodeIds.length == 0){
				query = "OBJECTID = -1";
			}
			else {
				var queryObjectIds = nisis.user.userVisibleFeatures[itemtype];
				// value set when new shape is drawn on map
				if (this.newShapeIdCreated !== null){
					queryObjectIds += "," + this.newShapeIdCreated; // query it in map
					checkedShapeNodeIds.push(this.newShapeIdCreated); // check it in the tree
				}
				//reset it after using it.
				this.newShapeIdCreated = null;
				query = "OBJECTID IN (" + queryObjectIds + ")";
			}
			// console.log(shapeLayer.id);
			// console.log(query);
			shapeLayer.setDefinitionExpression(query);
		},
		getSelectedItem: function(){
			var treeview = $(this.tabSelects[this.userOrGroupTab]).data("kendoTreeView");
			if(!treeview){
				return null;
			}
			var selected = treeview.select();
			var item = treeview.dataItem(selected);
			// console.log("ITEM SELECTED:", item);

			return item;
		},
		deleteShape: function(shapeId, name, dontAsk, itemType){
			if(this.userOrGroupTab == "manager"){
				// delete from the Arrange Order tab
				//console.log("deleteShape:", itemType);
				shapesManager.deleteShape(shapeId, itemType);
			}
			var fmItem = features.getItemByObjectId(shapeId, itemType);
		    if (fmItem) {
				this.deleteItem(fmItem, dontAsk);
			}
			else {
			 	nisis.ui.displayMessage("Unable to find shape in the Feature Manager", 'error');
			}

		},
		deleteItem: function(e, dontAsk){
			var $this = this;
			var item;
			var dontAsk = (dontAsk === undefined) ? false : true;

			if (e.hasOwnProperty("SHAPEID")) {
				item = e;
			}
			else {
				item = $this.getSelectedItem();
			}
			var dbFunction = 'fm_delete_item';
			var defaultPrompt = 'Delete this item?';

			// enforce biz rules
			if (!item || item.ITEMID === "0" || item.ITEMID === "1") {
				return;
			}

			if (item.ISSHARED === true){
				dbFunction = 'fm_reject_shared_item';
			 	defaultPrompt = 'Deleting a shared item will unshare it with you.  Continue?';
				$this.deleteItemPrompt(defaultPrompt, dbFunction, item, false); //do ask, its shared
			} else {
				// user owns the item.  First find out if user has shared this item with anyone else.
				// NOTE: Checking if item IS SHARED and WAS DRAGGED BY SHAREE OUT OF THE "Items Shared With Me/Group" AREA.
				$.ajax({
					async: false,
					url: "api/",
					type: "POST",
					data: {
						action: "fm_get_item_share_cnt",
						itemid: item.ITEMID,
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						var shareecnt = null;
						shareecnt = resp.itemsharecnt;
						if (shareecnt && shareecnt > 0){
							// prompt the user saying "item is shared with itemShareCnt users."
							defaultPrompt = "Warning! Item was shared with " + shareecnt + " users/groups. " +  defaultPrompt;
						}
						$this.deleteItemPrompt(defaultPrompt, dbFunction, item, dontAsk);
					},
					error: function(req, status, err){
						console.log("ERROR getting shareecnt: ", arguments);
					},
				});
			}
		},
		deleteItemPrompt: function (defaultPrompt, dbFunction, item, dontAsk) {
			var $this = this;
			if (dontAsk) {
				this.deleteItemAjax(dbFunction, item);
			}
			else {
				var msg = "",
				options = {
					title: 'Delete',
					message: defaultPrompt,
					buttons: ['YES', 'NO']
				};''

				nisis.ui.displayConfirmation(options).then(
					function(resp){
						if(resp == options.buttons[0]) {
							$this.deleteItemAjax(dbFunction, item);
						}
					},
					function(error){
						msg = 'Unable to capture your answer.  Try again.';
						nisis.ui.displayMessage(msg, 'error');
					}
				);
			}
		},
		deleteItemAjax: function(dbFunction, item) {
			var $this = this;
			$.ajax({
							url: "api/",
							type: "POST",
							data: {
								action: dbFunction,
								itemid: item.ITEMID,
								parentid: item.PARENTID,
								userorgroupmode: $this.userOrGroupTab,
								groupid: $this.selectedGroup,
								itemtype: item.ITEMTYPE,
								shapeid: item.SHAPEID
							},
							success: function(data, status, xhr){
								var resp = JSON.parse(data);
								if (resp.result !== "1"){
									var msg = "",
										//FB - 4/27/2016 -
										ajaxErrors = {
											//"0"	: "Unexpected error. Please contact technical support.",
											"-1": "Error. Invalid parameters. Please contact technical support.",
											"-2": "Error. Insufficient permissions to delete this item. Please contact technical support.",
											"-3": "Error. Insufficient permissions on parent folder. Please contact technical support.",
											"-4": "Error. Folder has shape items.  Delete shapes first.",
										};
										//0 is fine
										if (resp.result !== "0") {
											msg = ajaxErrors[resp.result];
											nisis.ui.displayMessage(msg, 'error');
										}

								} else {
									//ok, were able to remove from NISIS_POLYGONS and
									//see if it was visible on map extent, so remove it from the map graphics
									$this.removeFromMapGraphics(item.SHAPEID);
									var tree = $(features.tabSelects[features.userOrGroupTab]).data("kendoTreeView");
									var itemParentId = item.parentNode().ITEMID;
									tree.remove(tree.findByUid(item.uid));

									//$this.shallWeRefreshMap = true; // kofi: DO NOT MAKE THIS VARIABLE A FUNCTION PARAM TO refreshCurrentTab()!!!
									//$this.refreshCurrentTab(itemParentId);
								}
							},
							error: function(xhr, status, err){
								console.log(err);
							}
						});
		},
		removeFromMapGraphics: function(shapeId) {
			for (var i = 0; i < this.featureMgmtLayers.length; i++) {
				if (this.removeFromUserLayerGraphics(nisis.ui.mapview.map.getLayer(this.featureMgmtLayers[i]), shapeId)) {
					//done, removed the shape from the layer
					break;
				}
            }
            //clear out ESRI info window
            if (nisis.ui.mapview.featureEditor!='undefined') {
            	var feat = nisis.ui.mapview.featureEditor._currentGraphic;
				if (feat) {
					nisis.ui.mapview.map.infoWindow.hide();
					if (feat._graphicsLayer) {
						feat._graphicsLayer.clearSelection();
					}
					nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.deactivate();
				}
            }
		},
		removeFromUserLayerGraphics: function(layer, shapeId) {
			var result=false;
			for (var i = layer.graphics.length; i--;) {
				graphic=layer.graphics[i];
                if  (graphic.hasOwnProperty("attributes") && graphic.attributes.OBJECTID == shapeId) {
                    //remove
                    layer.remove(graphic);
                    return true;
                }
            }
            return false;

		},
		tabStripSelects: {
			// FB 5/13/2016 - value obj for
			"My Features": "user",
			"Group Features": "group",
			"Arrange Order": "manager"
		},
		onSelectTabStrip: function(e) {
			var $this = this,
				text = e.item.textContent;
			$this.userOrGroupTab = $this.tabStripSelects[text];

			$this.refreshData();
		},
		initTooltips: function(tab){
			var $this = this;

			//initialize the tooltip if it has not been initialized before
			//if ($("#features-div").data("kendoTooltip") == undefined) {
				//initialize it
				var tooltip = $("#features-div").kendoTooltip({
					filter: "li span.k-in",
					// filter: "li[role=treeitem]",
					// content: "Loading...",
					position: "right",
					animation: {
						open: {
							effects: "fade:in",
							duration: 500
						},
						close: {
							effects: "fade:out",
							duration: 500
						}
					},
					content: function (e) {
						var target = e.target; // the element for which the tooltip is currently shown
						//Getting the actual tree-item from the tree
						treeItem = target.closest('.k-item');
						var treeviewDataSource = $($this.tabSelects[tab]).data("kendoTreeView").dataSource;
						var uid = treeItem.data('uid');
						var dataItem = treeviewDataSource.getByUid(uid);
						if (dataItem){

							if (dataItem.ITEMID === "0" || dataItem.ITEMID === "1" ){
					        	return "<ul>" +
						        		"<li><span>Name: </span>" + dataItem.ITEMNAME + "</li>" +
						        		"</ul>" ;
							} else {
						        return "<ul>" +
						        		"<li><span>Name: </span>" + dataItem.ITEMNAME + "</li>" +
						        		"<li><span>Date Created: </span>" + dataItem.CREATEDATE + "</li>"+
						        		"<li><span>Owner: </span>" + dataItem.OWNERNAME + "</li>" +
						        		"</ul>" ;
						    }
						}
				    },
				    show: function (e) {
				        var target = this.target(); // the element for which the tooltip is currently  shown
						var $el = $(this.popup.element);

						$el.removeClass("k-widget k-tooltip"); //remove bs kendo crap
						$el.addClass("toolytip"); //add awesome new style that is waayyyyyy better
					}
				}).data("kendoTooltip");
			//}
		},
		nodesToExpand: [],
		nodesToCheck: [],
		parentIdToExpand: null,
		saveNodesToExpandAndCheck: function(tab, parentId) {
			var $this = this;
			$this.parentIdToExpand = null;
			// If it's groups tab, run the rest of this block of
			// code only when dropdown has been changed.
			if (tab === "group" && !$this.treeInitialLoadDone["group"]){
				return;
			}
			$this.loadExpandedNodes(tab, parentId);

			if( (typeof parentId !== 'undefined') && (parentId !== null) ){
				$this.nodesToExpand.push(parentId);
				$this.parentIdToExpand = parentId;
			}
			// reload checked nodes array
			$this.loadCheckedNodes(tab);
		},
		expandAndCheckNodes: function(tab) {
			var $this = this;
			var arrayNodes = $this.nodesToExpand;
			var tree = $($this.tabSelects[tab]).data("kendoTreeView");
			//The first time the datasources are read nothing should be expanded
			if (arrayNodes && arrayNodes.length > 0 ){
				tree.expandPath(arrayNodes);

				if( (typeof $this.parentIdToExpand !== 'undefined') && ($this.parentIdToExpand !== null) ){
					$this.forceSelect(tab, $this.parentIdToExpand);
				}
			}
			// check nodes that need checking
			for (var i=0; i < $this.nodesToCheck.length;++i) {
				var ds = tree.dataSource;
			    var dataItem = ds.get($this.nodesToCheck[i]);
				if (dataItem){ // do a check in case node was just deleted.
				    var node = tree.findByUid(dataItem.uid);
					tree.dataItem(node).set("checked", true);
				}
			}
		},
		forceSelect : function (tab, itemid){
			var $this = this;
			var tree = $($this.tabSelects[tab]).data("kendoTreeView");

			var pitem = tree.dataSource.get(itemid);
			if (pitem) {
				var pnode = tree.findByUid(pitem.uid);
				tree.select(pnode);
			}
			if (tab === 'user'){
				$this.selectMyFeature();
			} else if (tab === 'group') {
				$this.selectGroupFeature();
			}
		},
		myFeaturesDataSource : new kendo.data.HierarchicalDataSource({
			transport: {
				read: function(options) {
					$.ajax({
						url: "api/",
						type: "POST",
						data: {
							action: "get_my_features_tree"
						},
						success: function(result) {
							var resp = JSON.parse(result);

							options.success(resp);
							features.disableUserMenuButtons();
							// Check ALL check boxes on ONLY FIRST LOAD OF TREE.
							if (!features.treeInitialLoadDone["user"]){
								features.doTreeInitialLoad("user");
							}
							// Expand and check checkboxes only if data has changed.
							// Avoid all this work when tree data hasn't changed.
							features.initTooltips("user");
							features.expandAndCheckNodes("user");
							//Select the element previously selected before the refresh and expand
							var treeview = $(features.tabSelects["user"]).data("kendoTreeView");
							if (features.parentIdToExpand){
								//var item = features[features.tabRefreshes[features.userOrGroup]].get(features.parentIdToExpand);
								// var item = treeview.dataSource.get(features.parentIdToExpand);
								// var node = treeview.findByUid(item.uid);
								// treeview.select(node);
								features.forceSelect("user",features.parentIdToExpand);
							}

							if (features.refreshTreesAndByPassMap){
								features.saveNodesToExpandAndCheck("group", null);
                                features.groupFeaturesDataSource.read();
							}
							else {
								features.refreshShapesOnMap();
							}
						},
						error: function(result) {
							console.error("Error: ", result);
							console.error("ERROR in ajax call to get myFeatures datasource tree in features.js");
							options.error(result);
						}
					});
				}
			},
			schema: {
				data: "tree",
				model: {
					id: "ITEMID",
					hasChildren: "HASCHILDREN",
					children: "ITEMS",
				}
			}
		}),
		doTreeInitialLoad : function (userOrGroupTab){
			if (userOrGroupTab === "user"){
				this.treeInitialLoadDone[userOrGroupTab] = true;
				this.checkAllTreeItems(userOrGroupTab, this.myFeaturesDataSource.view());
			} else if (userOrGroupTab === "group") {
				this.treeInitialLoadDone[userOrGroupTab] = true;
				this.checkAllTreeItems(userOrGroupTab, this.groupFeaturesDataSource.view());
			}
		},
		getCheckedShapeNodeIdsForLayer : function (tab, nodes, layer, checkedShapeNodeIds){
			var node;
			//irena
			var tree = $(features.tabSelects[tab]).data("kendoTreeView");
			for (var i = 0; i < nodes.length; i++) {
				node = nodes[i];
				if (tree.dataItem(tree.findByUid(node.uid)).checked && node.SHAPEID != 0 && this.getLayerByItemType(node.ITEMTYPE).layerId==layer.layerId){
                    checkedShapeNodeIds.push(node.SHAPEID);
                    nisis.user.userVisibleFeatures[node.ITEMTYPE] += "," + node.SHAPEID;
                  	if (node.STYLE.hasOwnProperty("CREATED_USER")){
                        if ( nisis.user.styles ) {
                            nisis.user.styles = nisis.user.styles.concat(node.STYLE);
                        } else {
                            nisis.user.styles = [];
                            nisis.user.styles = nisis.user.styles.concat(node.STYLE);
                        }
                    }
				}
				if (node.hasChildren) {
					this.getCheckedShapeNodeIdsForLayer(tab, node.children.view(), layer, checkedShapeNodeIds);
				}
			}

		},
		// function to be used recursively to get checked nodes and their checked children
        checkedNodeIds : function (nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked && nodes[i].SHAPEID != "0" ) {
                    checkedNodes.push(nodes[i].SHAPEID);
                }

                if (nodes[i].hasChildren) {
                    this.checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        },
        uncheckedNodeIds : function (nodes, uncheckedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked == false && nodes[i].SHAPEID != "0" ) {
                    uncheckedNodes.push(nodes[i].SHAPEID);
                }

                if (nodes[i].hasChildren) {
                    this.uncheckedNodeIds(nodes[i].children.view(), uncheckedNodes);
                }
            }
        },
        // function to be used recursively to get checked nodes and their checked children
        getNodeIds : function (nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].SHAPEID != "0" ) {
                    checkedNodes.push(nodes[i].SHAPEID);
                }

                if (nodes[i].hasChildren) {
                    this.getNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        },
        getShapes : function (nodes, shapes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].SHAPEID != "0" ) {
                    shapes.push(nodes[i]);
                }

                if (nodes[i].hasChildren) {
                    this.getShapes(nodes[i].children.view(), shapes);
                }
            }
        },
		// Recursive function:
		// Called on first load of tree data
		// Checks all tree nodes by default, and
		checkAllTreeItems : function (tab, nodes){
			var node;
			var tree = $(this.tabSelects[tab]).data("kendoTreeView");

			for (var i = 0; i < nodes.length; i++) {
				node = nodes[i];
				tree.dataItem(tree.findByUid(node.uid)).set("checked", true);
				if (node.hasChildren) {
					this.checkAllTreeItems(tab, node.children.view());
				}
			}
		},
		// DO NOT CHANGE PARAM tab to local var this.userOrGroup!!
		loadCheckedNodes : function (tab){
			var $this = this;
			var tree = $($this.tabSelects[tab]).data("kendoTreeView");
			$this.nodesToCheck = [];
			tree.element.find('.k-item').each(function () {
				var item = tree.dataItem(this);
				// if expanded and make sure we're not adding parentid twice
				if (item.checked) {
					$this.nodesToCheck.push(item.id);
				}
			});
		},
		loadExpandedNodes : function (tab, parentId){
			var $this = this;
			var tree = $($this.tabSelects[tab]).data("kendoTreeView");
			$this.nodesToExpand = [];
			tree.element.find('.k-item').each(function () {

				var item = tree.dataItem(this);

				// if expanded and make sure we're not adding parentid twice
				if (item.expanded && item.ITEMID !== parentId) {
					$this.nodesToExpand.push(item.id);
				}
			});
		},
		onCheck : function (e){
			var	treeview = $(this.tabSelects[this.userOrGroupTab]).data("kendoTreeView");
			// reload checked nodes array
			// this.loadCheckedNodes(this.userOrGroupTab, treeview.dataSource.view());
			this.loadCheckedNodes(this.userOrGroupTab);
          	this.refreshShapesOnMap();
		},
		selectMyFeature : function (e){
			var item;
			var treeview = $("#myFeaturesTree").data("kendoTreeView");
			if (e){
				item = treeview.dataItem(e.node);
			} else {
				item = treeview.dataItem(treeview.select());
			}

			if (!item || item == undefined){
				return;
			}

			this.disableButton('fmcreatefolder');
			this.disableButton('fmdel');
			this.disableButton('fmshare');
			this.disableButton('fmedit');
			this.disableButton('fmFlyTo');

			if (item.ITEMID != "1" && item.parentNode().ITEMID != "1" && (item.ITEMTYPE === "folder" || item.parentNode().ITEMTYPE === "folder")){
				this.enableButton('fmcreatefolder');
			}

			if (item.ITEMID != "0" &&
				item.ITEMID != "1" &&
				item.ITEMTYPE === "folder" &&
				item.ISSHARED === false &&
				item.OWNERID === nisis.user.userid
				){
				this.enableButton('fmdel');
				this.enableButton('fmshare');
				this.enableButton('fmedit');
			} else if (item.ISSHARED === true ){
				//see if item is shared but a read-only for this user
				var isSharedItemReadOnly = ( (item.USERORGROUPITEM === "user" && item.OWNERID !== nisis.user.userid) || (item.USERORGROUPITEM === "group" && features.userGroupPriv === 1 ) ) ;
				if (isSharedItemReadOnly == false) {
					this.enableButton('fmdel');
					this.enableButton('fmedit');
				}
			} else if (item.ISSHARED === false && item.SHAPEID != "0"){
				this.enableButton('fmdel');
				this.enableButton('fmshare');
				this.enableButton('fmedit');
			}
			if (item.SHAPEID != "0"){
				this.enableButton('fmFlyTo');
			}
		},
		dragStartItem : function (e){
			if (!e) {
				return;
			}
			var $this = this;

			$this.dragEndParentID = null; // just in case
			$this.dragStartParentID = null; // just in case
			$this.dragItemID = null;
			$this.dragItemIsShared = 'false';

			var item = e.sender.dataItem(e.sourceNode);

			// don't allow dragging of root folders. Or shared items that are read only for this user
			var isSharedItemReadOnly = ( (item.USERORGROUPITEM === "user" && item.OWNERID !== nisis.user.userid) || (item.USERORGROUPITEM === "group" && features.userGroupPriv === 1 ) ) ;
			if (item && (item.ITEMID == "0" || item.ITEMID === "1") ||
				(item.ITEMTYPE === "folder" && item.ISSHARED === true) || isSharedItemReadOnly){
				e.preventDefault();
			}
			var parent = item.parentNode();

			$this.dragItemID = item.ITEMID;
			$this.dragItemIsShared = (item.ISSHARED).toString();
			$this.dragStartParentID = parent.ITEMID;
			if ($this.dragStartParentID == undefined){
				$this.dragStartParentID = 0;
			}
		},
		dropItem : function (e){
			var $this = this;
			var destinationNode;
			var msg = "Invalid item move.";
			// using dropPosition check to prevent reordering nodes willy-nilly
			// AND, MORE IMPORTANTLY, to prevent drag-and-drop to empty location outside the root folders!!
			if (!e || !e.valid || e.dropPosition === "before" || e.dropPosition === "after") {
				e.setValid(false);
			} else {
				destinationNode = e.sender.dataItem(e.destinationNode);
				if (destinationNode == undefined ||
						destinationNode.ITEMID == undefined ||
						destinationNode.ITEMID === "1" || // prevent drag into "shared with me" ROOT folder
						destinationNode.ISSHARED === true || // prevent drag into any "shared with me" folder
						(destinationNode.ISSHARED === true &&
							e.sender.dataItem(e.sourceNode).ISSHARED === true) // prevent all dragging of shared into another shared
						){
					msg = "Move to 'Shared' folder not allowed.  You can delete the item if not needed.  This will 'unshare' it with you.";
					e.setValid(false);
				}
				else{
					$this.dragEndParentID = destinationNode.ITEMID;
					if ($this.dragEndParentID === $this.dragStartParentID || destinationNode.SHAPEID != "0"){
						e.setValid(false);
					}
				}
			}

			if (e.valid){
				$.ajax({
					url: "api/",
					type: "POST",
					data: {
						action: 'fm_move_item_to_folder',
						usertype: $this.userOrGroupTab,
						itemid: $this.dragItemID,
						frmparentid: $this.dragStartParentID,
						toparentid: $this.dragEndParentID,
						selectedgroup: $this.selectedGroup,
						isshared: $this.dragItemIsShared
					},
					success: function(data, status, xhr){
						var resp = JSON.parse(data);
						if (resp.result !== "1"){
							e.preventDefault();

							//FB - 4/27/2016 - optimizing stuff
							var ajaxErrors = {
								"0"	: "Unexpected error while moving item. Please contact technical support.",
								"-1": "Error. Invalid parameters. Please contact technical support.",
								"-2": "Error. Insufficient permissions to move this item. Please contact technical support.",
								"-3": "Error. Insufficient permissions on source folder. Please contact technical support.",
								"-4": "Error. Insufficient permissions on destination folder. Please contact technical support.",
								"-5": "Move shared item to a valid folder."
							};
							if(!resp.result){resp.result = "0";}
							msg = ajaxErrors[resp.result];
							nisis.ui.displayMessage(msg, 'error');
						}
					},
					error: function(xhr, status, err){
						console.log(err);
						e.preventDefault();
					}
				});
			} else {
				e.preventDefault();
				nisis.ui.displayMessage(msg, 'error');
			}
		},
		groupsDropdownOnChange : function (e){
			var $this = this;
			var ds = $this.get("groupsDatasource");
			var id = e.sender.value();
			$this.set('userGroupPriv', parseInt(ds.get(id).PRIV));
			$this.treeInitialLoadDone["group"] = false;
			//Force map refresh when groups dropdown changes.
            $this.shallWeRefreshMap = true;
			$this.refreshCurrentTab();
		},
		groupsDatasource : new kendo.data.DataSource({
			transport: {
				read: function(options) {
					// KOFI: NISIS-2327-AGAIN - reduce calls to getUserGroups function.
					// if (nisis.user.groupObjects == undefined || nisis.user.groupObjects.length == 0){
					// 	$.ajax({
					// 		url: "admin/api/",
					// 		type: "POST",
					// 		data: {
					// 			action: 'acl',
					// 			function: 'getusergroups',
					// 			id: nisis.user.userid
					// 		},
					// 		success: function(result) {
					// 			var resp = JSON.parse(result);
					// 			options.success(resp);
					// 			nisis.user.groupObjects = resp;
					// 			// var ds = $("#groupsDropDown").data('kendoDropDownList').dataSource;
					// 			if (resp.items && resp.items.length == 0){
					// 				// $grpFt = $('#groupFeaturesTree');
					// 				// $ul = $grpFt.find('ul');
					// 				// $input = $('.g-container');
					// 				// $input.hide();
					// 				// $ul.hide();
					// 				// $grpFt.append("<div id='noGrpItem'> You do not belong to any groups. <br> Please contact NISIS Admin. </div>");
					// 				var cont = $('.g-container');
					// 				features.set('groupsTreeVisible', false);
					// 				cont.append("<div id='noGrpItem'> You do not belong to any groups. <br> Please contact NISIS Admin. </div>");
					// 			}
					// 		},
					// 		error: function(result) {
					// 			console.error("Error: ", result);
					// 			options.error(result);
					// 		}
					// 	});
					// } else {
					// }
					options.success(nisis.user.groupObjects);
				}
			},
			schema: {
				//data: "items",
				model: {
					id: "GROUPID",
					fields: {
						GROUPID: {editable: false},
						GNAME: {editable: false},
						PRIV: {editable: false}
					}
				}
			},
		}),
		usersDatasource : new kendo.data.DataSource({
			transport: {
				read: function(options) {
					$.ajax({
						type: "POST",
						url: "admin/api/",
						data: {
							'action': "acl",
							'function': 'getUsersVisibleToUser',
							id: nisis.user.userid
						},
						success: function(result) {
							var resp = JSON.parse(result);
							options.success(resp);
						},
						error: function(result) {
							console.error("Error: ", result);
							options.error(result);
						}
					});
				}
			},
			schema: {
				data: "items",
				model: {
					id: "USERID",
					fields: {
						USERID: {editable: false},
						USRNAME: {editable: false}
					}
				}
			},
		}),
		//CC - This variable will be used to hide and show the groupsDropDown and groupFeaturesTree
		groupsTreeVisible: true,
		groupFeaturesDataSource : new kendo.data.HierarchicalDataSource({
			transport: {
				read: function(options) {
					$.ajax({
						url: "api/",
						type: "POST",
						data: {
							action: "get_group_features_tree",
							grpId: features.selectedGroup
						},
						success: function(result) {
							var resp = JSON.parse(result);
							options.success(resp);
							features.disableGroupMenuButtons();
							if ($("#groupsDropDown").val() != ""){
								features.enableButton("fmrefresh2");
							}

							if (!features.treeInitialLoadDone["group"]){
								features.doTreeInitialLoad("group");
							}

							features.initTooltips("group");
							features.expandAndCheckNodes("group");
							var treeview = $(features.tabSelects["group"]).data("kendoTreeView");
							if (features.parentIdToExpand){
								features.forceSelect("group",features.parentIdToExpand);
							}

							if (features.refreshTreesAndByPassMap){
								shapesManager.refreshShapesManagementWindow();
								features.refreshTreesAndByPassMap = false;
							} else {
								features.refreshShapesOnMap();
							}
						},
						error: function(result) {
							console.error("Error: ", result);
							console.error("ERROR in ajax call to get groupFeatures datasource tree in features.js");
							options.error(result);
						}
					});
				}
			},
			schema: {
				data: "tree",
				model: {
					id: "ITEMID",
					hasChildren: "HASCHILDREN",
					children: "ITEMS"
				}
			},
		}),
		selectGroupFeature : function (e){
			var item;

			var treeview = $("#groupFeaturesTree").data("kendoTreeView");
			if (e){
				item = treeview.dataItem(e.node);
			} else {
				item = treeview.dataItem(treeview.select());
			}

			this.disableButton('fmcreatefolder2');
			this.disableButton('fmdel2');
			this.disableButton('fmshare2');
			this.disableButton('fmedit2');
			this.disableButton('fmFlyTo2');

			if (this.userGroupPriv > 1 && item.ITEMID != "1" && item.parentNode().ITEMID != "1" && (item.ITEMTYPE === "folder" || item.parentNode().ITEMTYPE === "folder")){
				this.enableButton('fmcreatefolder2');
			}

            var isSharedItemReadOnly=false;
			if (item.ITEMID != "0" &&
				item.ITEMID != "1" &&
				item.ITEMTYPE === "folder" &&
				item.ISSHARED === false &&
				this.userGroupPriv > 1){
				this.enableButton('fmdel2');
				this.enableButton('fmshare2');
				this.enableButton('fmedit2');
			} else if (item.ISSHARED === true && this.userGroupPriv > 1){
				this.enableButton('fmdel2');
				this.enableButton('fmFlyTo');
				this.enableButton('fmedit2');
			} else if (item.ISSHARED === true ){
				//see if item is shared but a read-only for this user
				isSharedItemReadOnly = ( (item.USERORGROUPITEM === "user" && item.OWNERID !== nisis.user.userid) || (item.USERORGROUPITEM === "group" && features.userGroupPriv === 1 ) ) ;
				if (isSharedItemReadOnly == false) {
					this.enableButton('fmdel2');
					this.enableButton('fmedit2');
				}
			};

			if (item.SHAPEID != "0"){
				if (isSharedItemReadOnly == false) {this.enableButton('fmedit2');}
				this.enableButton('fmFlyTo2');
			}
		},
		displayItemNamePrompt : function(foldername){
			var def = new $.Deferred();

			var fnameDialog = $('<div id="fname-dialog"></div>');
			var outerDiv = $('<div class="marg clear"></div>');
			fnameDialog.append(outerDiv);

			var msgDiv = $("<div class='marg clear'></div>");
			var msg = $("<span>Enter name</span>");
			msgDiv.append(msg);

			var fnameDiv = $("<div class='marg clear'></div>");
			var fname = $('<input id="foldername" size="20" maxlength="100" type="text" value=""></input>');

			fnameDiv.append(fname);

			var buttonsDiv = $("<div class='marg clear'></div>");
			var save = $("<button class='k-button'>Save</button>");
			var cancel = $("<button class='margLeft k-button'>Cancel</button>");
			buttonsDiv.append(save,cancel);

			outerDiv.append(msgDiv,fnameDiv,buttonsDiv);

			//append dialog to app content
			$('#app-content').append(fnameDialog);
			//kendo dialog
			var dialog = fnameDialog.kendoWindow({
				title: "Item Name",
				visible: false,
				modal: true,
				width: '300',
				height: 'auto',
				open: function (e){
					fname.val(foldername);
					setTimeout(function(){
						fname.focus()
					}, 500);
				},
				close: function(e){
					this.destroy();
					def.resolve(null);
				}
			}).data('kendoWindow');

			//handle saving
			save.kendoButton({
				enable: false,
				click: function(e){
					//	FB 6/23/2016 validates foldernames before submission
					//	ideally this would be it's own module or augment an existing one, maybe
					var fn = fname.val();
					e.preventDefault();
					var fnameValidator = fname.kendoValidator({
					    rules:{
					        legalChars: function(fname){
					            var toMatch = /~|`|!|@|#|\$|%|\^|&|\*|\(|\)|_|\+|\[|\]|\|;|'|,|\.|,|\/|<|>|\?|:|"|{|}|\||\\/,
					                toReturn = fname.val().match(toMatch) === null;
					            return toReturn;
					        }
					    },
					    messages:{
					        legalChars: "Your input contains illegal characters"
					    }
					}).data("kendoValidator");

					if(fnameValidator.validate()){
					    fnameDialog.hide();
					    dialog.destroy();
					    def.resolve(fn);
					}else{
					    return;
					}
				}
			});
			cancel.on('click',function(evt){
				fnameDialog.hide();
				dialog.destroy();
				def.resolve(null);
			});

			submitFname = function(newFolderName){
				var submitIfTrue = newFolderName.length > 0 && newFolderName !== foldername;
				save.data('kendoButton').enable(submitIfTrue);
			};

			fname.on('change textInput input',function(e){
				submitFname(this.value);
			}).on('keyup',function(e){
				if(e.keyCode === 13){
					save.trigger("click");
				}
			});

			//open the dialog
			dialog.center().open();

			return def.promise();
		},
		displaySharePrompt : function(shareeusertype, datatextfield, datavaluefield, datasource){
			var $this = this;
			var def = new $.Deferred();

			var shareDialog = $('<div id="share-dialog"></div>');
			var outerDiv = $('<div class="marg clear"></div>');
			shareDialog.append(outerDiv);

			var msgDiv = $("<div class='marg clear'></div>");
			var msg = $("<span>Share with:</span>");
			msgDiv.append(msg);


			var shareesDiv = $("<div class='marg clear'></div>");
			var shareesList = $('<select id="sharing-users"></select>');
			shareesDiv.append(shareesList);

			var buttonsDiv = $("<div class='marg clear'></div>");
			var share = $("<button class='k-button'>Save</button>");
			var cancel = $("<button class='margLeft k-button'>Cancel</button>");
			buttonsDiv.append(share,cancel);

			outerDiv.append(msgDiv,shareesDiv,buttonsDiv);

			//append dialog to app content
			$('#app-content').append(shareDialog);
			//kendo dialog
			var dialog = shareDialog.kendoWindow({
				title: shareeusertype.charAt(0).toUpperCase() + shareeusertype.slice(1) + "s",
				visible: false,
				modal: true,
				width: '300',
				height: 'auto',
				close: function(e){
					this.destroy();
					def.resolve(null);
				}
			}).data('kendoWindow');

			//set the multiselect for sharees
			var g = shareesList.kendoMultiSelect({
				placeholder: "Click to select.",
				dataTextField: datatextfield,
				dataValueField: datavaluefield,
				dataSource: datasource
			}).data('kendoMultiSelect');
			//handle sharing
			share.kendoButton({
				enable: false
				,click: function(e){
					var sharees = g.dataItems();
					shareDialog.hide();
					dialog.destroy();
					def.resolve(sharees);
				}
			});
			cancel.on('click',function(evt){
				shareDialog.hide();
				dialog.destroy();
				def.resolve(null);
			});
			g.bind('change',function(e){
				var values = this.value();
				share.data('kendoButton').enable(true);
			});

			var sharees;
			$.ajax({
				async: false,
				url: "api/",
				type: "POST",
				data: {
					action: "fm_get_item_shares_list",
					itemid: $this.getSelectedItem().ITEMID,
					userorgroup: shareeusertype
				},
				success: function(data, status, req) {
					var resp = JSON.parse(data);
					if(resp.error){
						g.value([]);
					} else {
						sharees = resp.sharees;
						g.value(sharees);
					}
				},
				error: function(req, status, err){
					console.log("ERROR getting sharees: ", arguments);
				},
			});
			//open the dialog
			dialog.center().open();
			return def.promise();
		},
		// called by shapesManager.js at end of creatShape() function.
		// Ensures new shapes are added to the shape arrays in FM.
		synchShapesWithFM: function (shapetype, shapeid){
			this.newShapeIdCreated = shapeid;
			var parentId =  null;
			if (this.getSelectedItem()){
				parentId = this.getSelectedItem().ITEMID;
			}
			this.saveNodesToExpandAndCheck("user", parentId);
			// Set a flag to control what functions run after the .read()
			this.shallWeRefreshMap = true;
			this[this.tabRefreshes["user"]].read();
		},
		createFolder: function(e){
			var $this = this;
			var parentid = 0; //by default, parent is root folder
			var parent = $this.getSelectedItem();
			var ownerid;
			var userOrGroup = $this.userOrGroupTab;

			// enforce some business rules
			if (!parent || parent.ITEMID === "1") {
				return;
			}

			if (userOrGroup === "user"){
				ownerid = nisis.user.userid;
			} else if (userOrGroup === "group"){
				ownerid = $this.selectedGroup;
			}

			if (parent){
				if (parent.ITEMTYPE !== "folder"){
					parent = parent.parentNode();
				}
				parentid = parent.ITEMID;
			}
			$this.displayItemNamePrompt(null)
			.then(function(foldername){
				if(foldername && foldername.length > 0){
					$.ajax({
						url: "api/",
						type: "POST",
						data: {
							action: 'fm_create_update_item',
							ownertype: $this.userOrGroupTab,
							ownerid: ownerid,
							foldername: foldername,
							parentid: parentid,
							actiontype: 'create_folder'
						},
						success: function(data, status, xhr){
							var resp = JSON.parse(data);
							//prevent map refresh after Tab data refresh
							$this.shallWeRefreshMap = false;
							$this.refreshCurrentTab(parentid);
							// select, highlight the new item
							// and enable/disable menu buttons
							$this.forceSelect($this.userOrGroupTab, resp.result);
						},
						error: function(xhr, status, err){
							console.log(err);
							nisis.ui.displayMessage('Unable to create folder. Contact tech support.', '', 'error');

						}
					});
				}
			},function(error){
				console.log(error);
				nisis.ui.displayMessage(error, '', 'error');
			});
		},
		getLayerByItemType: function(itemType){
			var layer = {};
			switch(itemType) {
				case "polygon":
					layer = nisis.ui.mapview.map.getLayer('Polygon_Features');
					break;
				case "line":
					layer = nisis.ui.mapview.map.getLayer('Line_Features');
					break;
				case "point":
					layer = nisis.ui.mapview.map.getLayer('Point_Features');
					break;
				case "text":
					layer = nisis.ui.mapview.map.getLayer('text_features');
					break;
				default:
					console.log("Invalid itemType passed in getLayerByItemType");
			}
			return layer;
		},
		getItemTypeByLayerId: function(layerId){
			switch(layerId) {
				case 'Polygon_Features':
					return 'polygon';
					break;
				case 'Line_Features':
					return 'line';
					break;
				case 'Point_Features':
					return 'point';
					break;
				case 'text_features':
					return 'text';
					break;
				default:
					return null;
			}
		},
		flyToFeature: function(e){
			var $this = this,
				shapeID = null;
				itemType = null;
			if($this.userOrGroupTab === "manager"){
				var item = {};
				var grid = $("#shapes-manager-list").data("kendoGrid"),
	        	dataSource = grid.dataSource,
	        	selRow = grid.select(),
	        	selData = grid.dataItem(selRow).toJSON();

        		shapeID = selData['OBJECTID'];
				itemType = selData['TYPE'];
			}else{
				var item = $this.getSelectedItem();
				//enforce business rules
				if (!item || item.ITEMTYPE === "folder" || item.SHAPEID == "0"){
					return;
				}

				shapeID = item.SHAPEID;
				itemType = item.ITEMTYPE;
			}
			nisis.ui.mapview.util.flyToShape(itemType, shapeID);
		},
		editItem: function(e){
			var $this = this;
			var item = $this.getSelectedItem();
			//enforce business rules
			if (!item || item.ITEMID === "0" || item.ITEMID === "1"){
				return;
			}

			var shapeId = item.SHAPEID;


			if (item.ITEMTYPE === "folder") {
				$this.displayItemNamePrompt(item.ITEMNAME)
				.then(function(itemname){
					var newitemname = "";
					if(itemname && itemname.length > 0){
						newitemname = itemname.trim();
					}
					if (newitemname.length> 0 && newitemname !== item.ITEMNAME){
						$.ajax({
							url: "api/",
							type: "POST",
							data: {
								action: 'fm_create_update_item',
								itemid: item.ITEMID,
								itemname: newitemname,
								itemtype: item.ITEMTYPE,
								userorgroupmode: $this.userOrGroupTab,
								actiontype: 'edit_item_name'
							},
							success: function(data, status, xhr){
								var resp = JSON.parse(data);
								//Prevent map refresh after tab data refreshed.
								$this.shallWeRefreshMap = false;
								$this.refreshCurrentTab(item.ITEMID);
							},
							error: function(xhr, status, err){
								console.log(err);
							}
						});
					}
				},function(error){
					console.log(error);
					nisis.ui.displayMessage(error, '', 'error');
				});
			} else if (shapeId != "0" && shapeId != 0){
				console.log("NISIS 3012 >>> this is a Folder")
				// if (item.ITEMTYPE === "point" || item.ITEMTYPE === "text"){
				// 	nisis.ui.displayMessage("This item cannot be edited", 'error');
				// 	return;
				// }else{
					if (features.waitAndOpenEdit(item.ITEMTYPE,shapeId)==true) {
						//lucky, graphic was on extent
						console.log("NISIS 3012 >>> this is in extent")						
					}
					else {
							//Fly to the feature before the edit
							//flyToShape is Deferred, map.centerAt is also Deferred, but chaining them together is not enough: even though map.centerAt will trigger
							//when map is panned to an extent, it still will take some time for the graphic layers to load. So wait for it.
							nisis.ui.mapview.util.flyToShape(item.ITEMTYPE, shapeId).then(
								function(data) {
									//wait for layers to render
									//make sure we did not start yet. Happens if they double click on Pencil
									if (features.waitForRenderComplete==0) {
										features.waitAndOpenEditTries=0;
										features.waitForRenderComplete = setInterval(features.waitAndOpenEdit,500,item.ITEMTYPE,shapeId);
										//console.error('startInterval: ', features.waitForRenderComplete);
									}
								},
								function(error) {
									console.error('Error in flyToShape execution in editItem function: ', error);
									nisis.ui.displayMessage(error.message, '', 'error');
								}
							);

					}
				// }
			}
		},
		waitAndOpenEdit: function(itemType, shapeId) {
			//console.error('waitAndOpenEdit: ');
			console.log('>>>>>>>>>>>>>>>>>>>>> opening edit')
			var result=false;
			var found = false;
			if (features.waitAndOpenEditTries>10) {
				clearInterval(features.waitForRenderComplete);
			}
			features.waitAndOpenEditTries++;
			//NISIS-2049: need to edit shapes Fill Type, Fill Color, Outline Type, Outline Color, etc
			var graphics = features.getLayerByItemType(itemType).graphics;
			//find the shape
			$.each(graphics,function(index, graphic){
				if(graphic.hasOwnProperty("attributes") && graphic.attributes.OBJECTID == shapeId){
					//console.error('is it open?: ', $("#drawing-div").closest(".k-window").css("display"));
					var shapeToolsWindow = $('#drawing-div').data('kendoWindow');
					require(['app/windows/shapetools'],function(shapetools){
						shapetools.set('styleFeature', graphic);
						shapetools.setStylesForm();
					});
					require(['app/appConfig'],function(appConfig){
						if (!appConfig.checkIfHandlerExists(shapeToolsWindow, "close", "onShapeToolsClosed")){
							shapeToolsWindow.bind("close", appConfig.onShapeToolsClosed);
						}
						appConfig.showHideStylesFeatureTab("show");
					});
					//open the form window for the user

					shapeToolsWindow.title("Edit Feature");
					shapeToolsWindow.open();
					$('#draw-options-div').data('kendoTabStrip').activateTab($('#styles'));
					clearInterval(features.waitForRenderComplete);
					found=true;
					return false;
				}
			});
			return (found==true);
		},
		toggleDropdown: function(e){
			var $ddmenu = $('.dropdown-menu'),
				$caret = $('.dropdown-toggle').find("span[class*='fa-caret-']");
			$ddmenu.is(":visible") ? $ddmenu.hide() : $ddmenu.show();
			$caret.toggleClass("fa-caret-right fa-caret-down");
		},
		shareItem: function(params){
			var $this = this;
			var item = $this.getSelectedItem();
			// enforce some biz rules
			if (!item || item.ITEMID === "0" || item.ITEMID === "1" || item.ISSHARED === true) {
				return;
			}

			var shareeusertype = params.target.id;
			var shareelist = "";

			if (shareeusertype === 'group'){
				$this.displaySharePrompt(shareeusertype, "GNAME", "GROUPID", $this.groupsDatasource)
				.then(function(grps){
					// THIS CODE NEEDS TO RUN AFTER THE .then()
					if(grps && (grps instanceof Array)){
						$.each(grps,function(i,g){
							shareelist += "," + g.GROUPID;
						});
						$this.shareItemAjax($this.userOrGroupTab, item.ITEMID, shareelist, shareeusertype);
					}
				},function(error){
					console.log(error);
				});
			}
			else if (shareeusertype === 'user'){
				$this.displaySharePrompt(shareeusertype, "USRNAME", "USERID", $this.usersDatasource)
				.then(function(usrs){
					// THIS CODE NEEDS TO RUN AFTER THE .then()
					if(usrs && (usrs instanceof Array)){
						$.each(usrs,function(i,u){
							shareelist += "," + u.USERID;
						});
						$this.shareItemAjax($this.userOrGroupTab, item.ITEMID, shareelist, shareeusertype);
					}
				},function(error){
					console.log(error);
				});
			}
		},
		shareItemAjax: function (userorgroupmode, itemidlist, shareeuseridlist, shareeusertype){
			$.ajax({
				url: "api/",
				type: "POST",
				data: {
					action: 'fm_share_unshare_items',
					userorgroupmode: userorgroupmode,
					itemidlist: itemidlist,
					shareeuseridlist: shareeuseridlist,
					shareeusertype: shareeusertype
				},
				success: function(data, status, xhr){
					var resp = JSON.parse(data);
					nisis.ui.displayMessage(resp.result, 'info');
				},
				error: function(xhr, status, err){
					console.log(err);
				}
			});
		},
		updateFeatureManagerShapeName: function(shapeId, itemType, attributeName, attributeValue) {
			switch(attributeName) {
				case "NAME":
					//update which tab? Features/Group Features trees or Arrange Order
					if(features.userOrGroupTab === "manager"){
						//change the name in the grid
						var item = features.getItemByObjectId(shapeId, itemType);
						shapesManager.updateShapeName(item.ITEMID, attributeValue);
					}else{
						var treeview = $(this.tabSelects[this.userOrGroupTab]).data("kendoTreeView");

			            //find the shape on the FM tree
			            var nodes = [];
				    	features.getShapes(treeview.dataSource.view(), nodes);
				    	for (var i = 0; i < nodes.length; i++) {
				    		var itmtype = (itemType === 'polyline' ? 'line' : itemType);
		                	if (nodes[i].SHAPEID == shapeId && nodes[i].ITEMTYPE == itmtype ) {
		                		nodes[i].ITEMNAME=attributeValue;
			            		var $nde = treeview.findByUid(nodes[i].uid);
			            		treeview.text($nde, attributeValue);
			            		break;
		                	}
	            		}
            		}
					break;
				default:
					console.log('updateFeatureManagerShapeName unsupported attribute: ', attributeName);
			}
		},
		//handles clicks in shapes manager window
		shapesManagerID: function(e){
			this.enableButton('fmFlyTo3');
			// if (e.hasOwnProperty("sender")) {
				//var $shapeWrap = $("#shapes-manager-pager");
				var $shapeWrap = $("#shapes-manager-list");
				var grid = $shapeWrap.data("kendoGrid");
				var selectedItem = grid.dataItem(grid.select());
				if (selectedItem!=null) {
					console.log("selectedItem:", selectedItem.OBJECTID);
					return selectedItem.OBJECTID;
				}
				else {
					console.log("shapesManagerID: nothing is selected in the Arrange Order grid");
					return -1;
				}
			// }

		},
		getItemByObjectId: function(shapeId, itemType) {
			var treeview = $("#myFeaturesTree").data("kendoTreeView");
			var nodes = [];
			features.getShapes(treeview.dataSource.view(), nodes);
			for (var i = 0; i < nodes.length; i++) {
		           	if (nodes[i].SHAPEID == shapeId && nodes[i].ITEMTYPE == itemType) {
		           		return nodes[i];
		            }
	        }
	        //no luck, try group Features tree ?
			treeview = $("#groupFeaturesTree").data("kendoTreeView");
			nodes = [];
			features.getShapes(treeview.dataSource.view(), nodes);
			for (var i = 0; i < nodes.length; i++) {
		    	if (nodes[i].SHAPEID == shapeId && nodes[i].ITEMTYPE == itemType) {
		        	return nodes[i];
		        }
	        }
	        console.log("Feature manager: getItemByObjectId cannot find the shape", shapeId);
	        return null;
		},
		updateShapesDisplayOrder: function(shapes) {
			for (var i = 0; i < shapes.length; i++) {
		        updateShapesDisplayOrder(shapes[i].OBJECTID, shapes[i].DISPLAY_ORDER);
	        }
		},
		updateShapesDisplayOrder: function(shapeId, displayOrder) {
			var treeview = $("#myFeaturesTree").data("kendoTreeView");
			var nodes = [];
			features.getShapes(treeview.dataSource.view(), nodes);
			for (var i = 0; i < nodes.length; i++) {
		       	if (nodes[i].SHAPEID == shapeId) {
		       		console.log("Feature manager: update shapeId", shapeId, displayOrder);
		           	nodes[i].STYLE.DISPLAY_ORDER=displayOrder;
		        }
	        }
	        //no luck, try group Features tree ?
			treeview = $("#groupFeaturesTree").data("kendoTreeView");
			nodes = [];
			features.getShapes(treeview.dataSource.view(), nodes);
			for (var i = 0; i < nodes.length; i++) {
		    	if (nodes[i].SHAPEID == shapeId) {
		    		console.log("Feature manager:  update shapeId", shapeId, displayOrder);
		        	nodes[i].STYLE.DISPLAY_ORDER=displayOrder;
		        }
	        }
		}

	});

	//Returning the 'features' object created before
	return features;

});
