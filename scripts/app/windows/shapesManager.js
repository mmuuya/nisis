var stringFilters = {}, numberFilters = {}, dateFilters = {};
/*
 * DOJO's shape.moveToBack() or shape.moveToFront() moves the shape all the way to the front/back. We need an ability to move the shapes through z-index /powerpoint like.
 */

define(["app/layersConfig","esri/geometry/Polygon",
    "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol","dojo/_base/Color","dojo/_base/connect",
    "esri/tasks/query","esri/geometry/ScreenPoint", "esri/geometry/Point", "esri/geometry/webMercatorUtils"]
    ,function(layers,Polygon,SimpleMarkerSymbol, SimpleFillSymbol
        ,SimpleLineSymbol,Color,connect,Query, ScreenPoint, Point, webMercatorUtils) {

    Array.prototype.swap = function (x,y) {
                          var b = this[x];
                          this[x] = this[y];
                          this[y] = b;
                          return this;
                        };
    var self = kendo.observable({
        selectedOBJECTID: 0,
        getMyShapes: function() {
            //console.log("The getMyShapes:");
            //build shapes
            var shapes = [];

            //get user layers
            var graphics = nisis.ui.mapview.map.getLayer("Polygon_Features").graphics;
            for (var i=0;i<=graphics.length-1;++i) {
                    if  (graphics[i] && graphics[i].hasOwnProperty("attributes") && graphics[i].attributes.OBJECTID && graphics[i].getNode()!=null) {
                        var newShape = {};
                        var fmItem = features.getItemByObjectId(graphics[i].attributes.OBJECTID, 'polygon');                       
                        newShape.ITEMID = fmItem.ITEMID;                     
                        newShape.OBJECTID = graphics[i].attributes.OBJECTID;
                        newShape.NAME = graphics[i].attributes.NAME;
                        newShape.DISPLAY_ORDER = (fmItem==null)?0:fmItem.STYLE.DISPLAY_ORDER;
                        newShape.TYPE = graphics[i].geometry.type;
                        shapes.push(newShape);
                    }
            }
            if (shapes != []) {
                //shapes
                shapes.sort(function(a, b) {
                return -(a.DISPLAY_ORDER - b.DISPLAY_ORDER);
                });
            }            

            return shapes;
        },         
        createShapesGrid: function(){
            var $this = this;
            //var $shapeWrap = $("#shapes-manager-pager");
            var $shapeWrap = $("#shapes-manager-list");
            var myShapes = this.getMyShapes();

            if (!myShapes) {
                return;
            }
            else {
                //go thru myShapes and build a list
                var dataSource = new kendo.data.DataSource({
                    data: myShapes,
                });

                $shapeWrap.kendoGrid({
                    dataSource: {
                        data: myShapes,                       
                        pageSize: 10,
                        schema: {
                            model: {
                                fields: {
                                    ITEMID: {
                                        type: 'string', editable: false
                                    },
                                    OBJECTID: {
                                        type: 'string', editable: false
                                    },
                                    NAME: {
                                        type: 'string', editable: false
                                    },
                                    DISPLAY_ORDER: {
                                        type: 'string', editable: false
                                    },
                                    TYPE: {
                                        type: 'string', editable: false
                                    }
                                }
                            }
                        }
                    },
                    pageable: {},
                    scrollable: true,
                    selectable: 'single',
                    editable: false,
                    columns: [{
                            field: 'NAME',
                            title: 'Shape Name'
                        },
                        {
                            field: 'DISPLAY_ORDER',
                            title: 'Display Order',
                            hidden: true
                        },
                        {
                            field: 'ITEMID',
                            title: 'Item ID',
                            hidden: true
                        },
                        {
                            field: 'OBJECTID',
                            title: 'Object ID',
                            hidden: true
                        },
                        {
                            field: 'TYPE',
                            title: 'Object Type',
                            hidden: true
                        }
                    ]
                });
            }
        },
        activateShapesManagementTool: function() {
            self.createShapesGrid();
            //bind the tool bar buttons
            kendo.bind($("#shapes-manager-actions-bar"), self);
            //renderers take care of refreshing the shapes management window (if up) and the map                      
        },
        refreshShapesManagementWindow: function() {
            if (features && features.userOrGroupTab != "manager") {
                return;
            }           
           
            //refresh grid and pager          
            var $shapeWrap = $("#shapes-manager-list");
            if ($shapeWrap.length>0) {
                var grid = $shapeWrap.data("kendoGrid");
                if (grid) {
                    var myShapes = self.getMyShapes();   
                    var dataSource = new kendo.data.DataSource({
                        data: myShapes,                       
                        pageSize: 10,
                        schema: {
                            model: {
                                fields: {
                                    ITEMID: {
                                        type: 'string', editable: false
                                    },
                                    OBJECTID: {
                                        type: 'string', editable: false
                                    },
                                    NAME: {
                                        type: 'string', editable: false
                                    },
                                    DISPLAY_ORDER: {
                                        type: 'string', editable: false
                                    },
                                    TYPE: {
                                        type: 'string', editable: false
                                    }
                                }
                            }
                        }
                    });

                    //refresh the grid with the datasource               
                    grid.setDataSource(dataSource);                     
                    grid.refresh();                                      
                    grid.pager.refresh();
                }    
            }        
        },
        createShape: function(shapeId, itemtype, graphic, symbolvalue) {
            var $this = this;
            var parentid = 0;
            var fmItem = null;
            if (features){
                fmItem = features.getSelectedItem();
                if (fmItem && fmItem.ITEMTYPE === "folder"){
                    parentid = fmItem.ITEMID;
                }
            }

            $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        "action": "create_shape",
                        "dataType": "json",
                        "shapeid": shapeId,
                        "parentid": parentid,
                        "itemtype": itemtype,
                        "symbolvalue": symbolvalue,
                        "userid": Number(nisis.user.userid)
                    },
                    success: function(data, status, req) {
                        var resp;
                        if (data){
                            resp = JSON.parse(data);
                        }
                        if (resp.result === "-99"){
                            nisis.ui.displayMessage("Unable to create a shape:", resp.result);
                            console.log("Unable to get create a shape");
                        }
                        else if (resp.result === "-1"){
                            nisis.ui.displayMessage("Unable to create a shape. Invalid values error:", resp.result);
                            console.log("Unable to create a shape. Invalid values:", resp.result);
                        }
                        else if (resp.result === "-3"){
                            nisis.ui.displayMessage("Unable to create a shape . Folder permissions error:", resp.result);
                            console.log("Unable to create a shape. Invalid values:", resp.result);
                        }
                        else {
                            console.log("shapesManager: created Shape successfully"+data);                                                     
                            if (features){  
                                //put new shape into FM
                                features.synchShapesWithFM(itemtype, shapeId);
                            }
                           
                            console.log("shapesManager: created Shape successfully, now poping up the Window");                            
                            nisis.ui.mapview.map.infoWindow.setFeatures([graphic]);     
                        }
                    },
                    error: function(req, status, err){
                        console.error("ERROR in ajax call to create a shape");
                        nisis.ui.displayMessage(" Unable to call create_shape", 'error');
                    }
            });

        },
        deleteShape: function(shapeId, itemType) {
            var $this = this;
            var $shapeId = shapeId;
            console.log("deleteShape", shapeId);
            $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        "action": "delete_shape",
                        "dataType": "json",                
                        "shapeid": shapeId,                
                        "itemtype": itemType,   
                        "parentid": 0,   
                        "userid": Number(nisis.user.userid)
                    },
                    success: function(data, status, req) {
                        if (data!= null && data.result != "-99") {
                            console.log("shapesManager: deleted shape successfully"+data); 
                            //see if it was visible on map extent, so remove it from the map graphics
                            features.removeFromMapGraphics($shapeId);   
                            $this.refreshShapesManagementWindow();//instead of on add/delete hookup                                                  
                        }     
                        else {
                            nisis.ui.displayMessage("Unable to delete a shape", 'error');
                            console.log("Unable to get delete a shape");                                                       
                        }                                    
                    },
                    error: function(req, status, err){
                        console.error("ERROR in ajax call to deleted a shape");                                                  
                        nisis.ui.displayMessage(" Unable to call delete_shape", 'error');                                                  
                    }
            });

        },      
        printKids: function(kids) {
            for (var b=1;b<=kids.length-1;++b) {
                    var kid = kids[b];
                    if (kid.rawNode==null) {
                        console.error("printKids: kid.rawNode number is null:", b);
                    }
                    else {
                        console.log(kid.rawNode.e_graphic.attributes.OBJECTID);
                    }
            }
        },
        printGraphics: function(graphics) {
            for (var b=0;b<=graphics.length-1;++b) {
                    var graphic = graphics[b];
                    if (graphic.hasOwnProperty("attributes") ) {
                        //console.log("printGraphics: graphic is:", graphic.attributes.OBJECTID});
                    }
            }
        },
        printDOMTree: function(tree) {
            for (var b=0;b<=tree.length-1;++b) {
                    var node = tree[b];
                    if (node.hasOwnProperty("attributes") ) {
                        // console.log("printGraphics: node is:", node.e_graphic.attributes.NAME);
                    }
            }
        },
        move: function (array, fromIndex, toIndex) {
            array.splice(toIndex, 0, array.splice(fromIndex, 1)[0] );
            return array;
        },
        getShapes: function () {
            //get them orderby DISPLAY_ORDER
            var orderedShapes = this.getMyShapes();
            //build shapes just with OBJECTID
            var shapes = [];
            for (var i = orderedShapes.length; i--;) {
                shapes.push(orderedShapes[i].OBJECTID);
            }

            return shapes;
        },
        moveShapeForward: function(shape, intersectingShapes) {
            //console.log("moveShapeForward .....", shape.rawNode.e_graphic.attributes.OBJECTID);

            //done the spatial query to see if intersect. calll  nisis.ui.mapview.util.queryFeatures with option=SPATIAL_REL_INTERSECTS
            //move shape right after the first intersecting shape or dont move it at all
            var parent=shape.getParent();
            var shapeObject = shape.shape;
            var kids = parent.children;

            //console.log("moveShapeForward: kids b4 reordering:");
            //this.printKids(kids);

            //WHICH child is the kid with shape?
            var shapeIndex = -1;
            for (var i=1;i<kids.length;++i) {
                if (kids[i] == shape) {
                    shapeIndex = i;
                    break;
                }
            }

            //the first child is the overacrching <g> group tag so the path tag elements start after that
            var pathElementsCount = kids.length-1;
            if(kids[pathElementsCount].shape==shapeObject) {
                //already the last element, just leave it there
                console.log("already the last element");
            }
            else {
                //start with b=1 since the first child is <g>
                for (var b=1;b<kids.length;++b) {
                    var kid = kids[b];
                    if (kid.rawNode!=null) {
                        var kidOBJECTID = kid.rawNode.e_graphic.attributes.OBJECTID;
                        if (this.doesShapeIntersect(kidOBJECTID, intersectingShapes)) {
                            //console.log("doesShapeIntersect - yes, swapping kids");
                            kids = this.move(kids, shapeIndex, b);
                            break;
                        }
                    }
                    else {
                        console.error("moveShapeForward: kid.rawNode is null, something wrong!!!!!!!!!");
                    }
                }
            }

            //console.log("moveShapeForward: kids after reordering:");
            //this.printKids(kids);

            var sibling = shape.rawNode.nextSibling;
            if (sibling == null) {
                //already the last element, just leave it there
                console.log("not moving sibling: already the last element");
            }
            else {
                //see if sibling is an intersecting shape. If not, continue go up the tree
                while (sibling!=null) {
                    var siblingOBJECTID = sibling.e_graphic.attributes.OBJECTID;
                    if (this.doesShapeIntersect(siblingOBJECTID, intersectingShapes)) {
                        //console.log("doesShapeIntersect - yes, swapping kids 2, inserting after ", siblingOBJECTID);
                        var sibling2 = sibling.nextSibling;
                        if (sibling2 == null) {
                            //append to the end
                            shape.rawNode.parentNode.appendChild(shape.rawNode);
                        }
                        else {
                            shape.rawNode.parentNode.insertBefore(shape.rawNode, sibling2);
                        }
                        break;
                    }
                    else { //go up the tree
                        sibling = sibling.nextSibling;
                    }
                }
            }

            this.persistShapesOrder();
        },
        moveShapeBackward: function(shape, intersectingShapes) {
            //console.log("moveShapeBackward .....", shape.rawNode.e_graphic.attributes.OBJECTID);
            var TOPGHtml = "<g></g>";
            //done the spatial query to see if intersect. calll  nisis.ui.mapview.util.queryFeatures with option=SPATIAL_REL_INTERSECTS
            //move shape right after the first intersecting shape or dont move it at all
            var parent=shape.getParent();
            var shapeObject = shape.shape;
            var kids = parent.children;

            //console.log("moveShapeBackward: kids b4 reordering:");
            //this.printKids(kids);

            //WHICH child is the kid with shape?
            var shapeIndex = -1;
            for (var i=1;i<kids.length;++i) {
                if (kids[i] == shape) {
                    shapeIndex = i;
                    break;
                }
            }

            //the first child is the overacrching <g> group tag so the path tag elements start after that
            if(kids[1].shape==shapeObject) {
                //already the first element, just leave it there
                console.log("already the first element");
            }
            else {
                //start with b=1 since the first child is <g>
                for (var b=kids.length-1;b>1;--b) {
                    var kid = kids[b];
                    if (kid.rawNode!=null) {
                        var kidOBJECTID = kid.rawNode.e_graphic.attributes.OBJECTID;
                        if (this.doesShapeIntersect(kidOBJECTID, intersectingShapes)) {
                            //console.log("doesShapeIntersect - yes, swapping kids");
                            kids = this.move(kids, shapeIndex, b);
                            break;
                        }
                    }
                    else {
                        console.error("moveShapeBackward: kid.rawNode is null, something wrong!!!!!!!!!");
                    }
                }
            }

            //console.log("moveShapeBackward: kids after reordering:");
            //this.printKids(kids);

            var sibling = shape.rawNode.previousSibling;
            if (sibling == null) {
                //something is wrong
                console.log("moveShapeBackward: something is wrong, this cant be");
            }
            if (sibling.outerHTML == "<g></g>") {
                //already the first element, just leave it there
                console.log("not moving sibling: already the first element");
            }
            else {
                //see if sibling is an intersecting shape. If not, continue go down the tree
                while (sibling.outerHTML != "<g></g>") {
                    var siblingOBJECTID = sibling.e_graphic.attributes.OBJECTID;
                    if (this.doesShapeIntersect(siblingOBJECTID, intersectingShapes)) {
                        //console.log("doesShapeIntersect - yes, swapping kids 2, inserting before ", siblingOBJECTID);
                        shape.rawNode.parentNode.insertBefore(shape.rawNode, sibling);

                        break;
                    }
                    else { //go down the tree
                        sibling = sibling.previousSibling;
                    }
                }
            }

            this.persistShapesOrder();
        },
        moveShapeToFront: function(shape, intersectingShapes) {
            //console.log("moveShapeToFront .....", shape.rawNode.e_graphic.attributes.OBJECTID);
            if (intersectingShapes.length==0) {
                //nothing to do
                return;
            }

            var lastIntersectingShape = intersectingShapes[intersectingShapes.length-1];

            //done the spatial query to see if intersect. calll  nisis.ui.mapview.util.queryFeatures with option=SPATIAL_REL_INTERSECTS
            //move shape right after the first intersecting shape or dont move it at all
            var parent=shape.getParent();
            var shapeObject = shape.shape;
            var kids = parent.children;

            //console.log("moveShapeForward: kids b4 reordering:");
            //this.printKids(kids);

            //WHICH child is the kid with shape?
            var shapeIndex = -1;
            for (var i=1;i<kids.length;++i) {
                if (kids[i] == shape) {
                    shapeIndex = i;
                    break;
                }
            }

            //the first child is the overacrching <g> group tag so the path tag elements start after that
            var pathElementsCount = kids.length-1;
            if(kids[pathElementsCount].shape==shapeObject) {
                //already the last element, just leave it there
                console.log("already the last element");
            }
            else {
                //start with b=1 since the first child is <g>
                for (var b=1;b<kids.length;++b) {
                    var kid = kids[b];
                    if (kid.rawNode!=null) {
                        var kidOBJECTID = kid.rawNode.e_graphic.attributes.OBJECTID;
                        if (kidOBJECTID == lastIntersectingShape.OBJECTID) {
                            //console.log("doesShapeIntersect - yes, swapping kids");
                            kids = this.move(kids, shapeIndex, b);
                            break;
                        }
                    }
                    else {
                        console.error("moveShapeForward: kid.rawNode is null, something wrong!!!!!!!!!");
                    }
                }
            }

            //console.log("moveShapeForward: kids after reordering:");
            //this.printKids(kids);

            var sibling = shape.rawNode.nextSibling;
            if (sibling == null) {
                //already the last element, just leave it there
                console.log("not moving sibling: already the last element");
            }
            else {
                //see if sibling is an intersecting shape. If not, continue go up the tree
                while (sibling!=null) {
                    var siblingOBJECTID = sibling.e_graphic.attributes.OBJECTID;
                    if (siblingOBJECTID == lastIntersectingShape.OBJECTID) {
                        //console.log("doesShapeIntersect - yes, swapping kids 2, inserting after ", siblingOBJECTID);
                        var sibling2 = sibling.nextSibling;
                        if (sibling2 == null) {
                            //append to the end
                            shape.rawNode.parentNode.appendChild(shape.rawNode);
                        }
                        else {
                            shape.rawNode.parentNode.insertBefore(shape.rawNode, sibling2);
                        }
                        break;
                    }
                    else { //go up the tree
                        sibling = sibling.nextSibling;
                    }
                }
            }

            this.persistShapesOrder();
        },
        moveShapeToBack: function(shape, intersectingShapes) {
            //console.log("moveShapeBackward .....", shape.rawNode.e_graphic.attributes.OBJECTID);
            //move it the last intersecting shape

            if (intersectingShapes.length==0) {
                //nothing to do
                return;
            }

            var firstIntersectingShape = intersectingShapes[0];

            //done the spatial query to see if intersect. calll  nisis.ui.mapview.util.queryFeatures with option=SPATIAL_REL_INTERSECTS
            //move shape right after the first intersecting shape or dont move it at all
            var parent=shape.getParent();
            var shapeObject = shape.shape;
            var kids = parent.children;

            // console.log("moveShapeBackward: kids b4 reordering:");
            // this.printKids(kids);

            //WHICH child is the kid with shape?
            var shapeIndex = -1;
            for (var i=1;i<kids.length;++i) {
                if (kids[i] == shape) {
                    shapeIndex = i;
                    break;
                }
            }

            //the first child is the overacrching <g> group tag so the path tag elements start after that
            if(kids[1].shape==shapeObject) {
                //already the first element, just leave it there
                console.log("already the first element");
            }
            else {
                //start with b=1 since the first child is <g>
                for (var b=kids.length-1;b>1;--b) {
                    var kid = kids[b];
                    if (kid.rawNode!=null) {
                        var kidOBJECTID = kid.rawNode.e_graphic.attributes.OBJECTID;
                        if (kidOBJECTID == firstIntersectingShape.OBJECTID) {
                            //console.log("doesShapeIntersect - yes, swapping kids");
                            kids = this.move(kids, shapeIndex, b);
                            break;
                        }
                    }
                    else {
                        console.error("moveShapeBackward: kid.rawNode is null, something wrong!!!!!!!!!");
                    }
                }
            }

            // console.log("moveShapeBackward: kids after reordering:");
            // this.printKids(kids);

            var sibling = shape.rawNode.previousSibling;
            if (sibling == null) {
                //something is wrong
                console.log("moveShapeBackward: something is wrong, this cant be");
            }
            if (sibling.outerHTML == "<g></g>") {
                //already the first element, just leave it there
                console.log("not moving sibling: already the first element");
            }
            else {
                //see if sibling is an intersecting shape. If not, continue go down the tree
                while (sibling.outerHTML != "<g></g>") {
                    var siblingOBJECTID = sibling.e_graphic.attributes.OBJECTID;
                    if (siblingOBJECTID == firstIntersectingShape.OBJECTID) {
                        //console.log("doesShapeIntersect - yes, swapping kids 2, inserting before ", siblingOBJECTID);
                        shape.rawNode.parentNode.insertBefore(shape.rawNode, sibling);

                        break;
                    }
                    else { //go down the tree
                        sibling = sibling.previousSibling;
                    }
                }
            }

            this.persistShapesOrder();
        },
        //everytime the Feature layer draws, we need to re-order the shapes based on DISPLAY_ORDER
        renderShapesInOrder: function() {
            var graphics = nisis.ui.mapview.map.getLayer('Polygon_Features').graphics;           
            //this.printGraphics(graphics);

            //reorder the array based on graphics[i].attributes.DISPLAY_ORDER
            function compare(a,b) {  
                var aa = features.getItemByObjectId(a.attributes.OBJECTID, 'polygon').STYLE.DISPLAY_ORDER;
                var bb = features.getItemByObjectId(b.attributes.OBJECTID, 'polygon').STYLE.DISPLAY_ORDER;
                var ax = (aa == "" ? 0 : parseInt(aa));
                var bx = (bb == "" ? 0 : parseInt(bb));

                if (ax < bx)
                    return -1;
                if (ax > bx)
                    return 1;
                return 0;
            }

            graphics.sort(compare);

            //this.printGraphics(graphics);


            var $tree = $("#Polygon_Features_layer"); //find polygon tree

            //this.printDOMTree($tree.find("path"));


            //reorder Tree
            jQuery.fn.sortDomElements = (function() {
                return function(comparator) {
                    return Array.prototype.sort.call(this, comparator).each(function(i) {
                          this.parentNode.appendChild(this);
                    });
                };
            })();

            $tree.find("path").sortDomElements(function(a,b){
                var aa = features.getItemByObjectId(a.e_graphic.attributes.OBJECTID, 'polygon').STYLE.DISPLAY_ORDER;
                var bb = features.getItemByObjectId(b.e_graphic.attributes.OBJECTID, 'polygon').STYLE.DISPLAY_ORDER;
                akey = (aa == "" ? 0 : parseInt(aa));
                bkey = (bb == "" ? 0 : parseInt(bb));
                if (akey == bkey) return 0;
                if (akey < bkey) return -1;
                if (akey > bkey) return 1;
            })

            //this.printDOMTree($tree.find("path"));
        },
        moveShape: function(e) {
            var action = '', v, shape, graphic, $this = this;

            if (e.hasOwnProperty("sender")) {
                //var $shapeWrap = $("#shapes-manager-pager");
                var $shapeWrap = $("#shapes-manager-list");
                var grid = $shapeWrap.data("kendoGrid");
                var selectedItem = grid.dataItem(grid.select());

                action = e.sender.wrapper[0].id;
                if (selectedItem !=null) {
                    objectId = selectedItem.OBJECTID;
                }
                else {
                    console.log("nothing has been selected in the Arrange Order Grid");
                }
            }
            else {
                //if click coming from the context menu
                action = e[1];
                shape = e[0].getDojoShape();
                objectId = shape.rawNode.e_graphic.attributes.OBJECTID;
            }

            //find the shape and graphic
            var graphics = nisis.ui.mapview.map.getLayer('Polygon_Features').graphics;
            for (var i = graphics.length; i--;) {
                if  (graphics[i]!='undefined' && graphics[i].hasOwnProperty("attributes") && graphics[i].attributes.OBJECTID == objectId) {
                    //bringForward
                    graphic = graphics[i];
                    shape = graphics[i].getDojoShape();
                    break;
                }
            }

            //find overlapping shapes first
            v = nisis.ui.mapview.util.findIntersectedUserShapes(graphic);

            v.then(function(intersectedShapes) {
                //take out the ones that are not in our Shapes - thats the mapview queryDefiniition
                //sort them first                
                var intersectedShapes = $this.sortIntersectedShapes(intersectedShapes);
                switch(action) {
                case "bringForward":
                    //console.log("bringForward .....", shape);
                    $this.moveShapeForward(shape, intersectedShapes);
                    break;
                case "sendBackward":
                    //console.log("sendBackward .....", shape);
                    $this.moveShapeBackward(shape, intersectedShapes);
                    break;
                case "bringToFront":
                    //console.log("bringToFront .....", shape);
                    $this.moveShapeToFront(shape, intersectedShapes);
                    break;
                case "sendToBack":
                    //console.log("sendToBack .....", shape);
                    $this.moveShapeToBack(shape, intersectedShapes);
                    break;
                default:
                    console.log("moveShape: invalid action, check app.php");
                }
            }, function(error) {
                console.log('Getting intersecting shapes error: ', error);
            });
        },
        sortIntersectedShapes: function(intersectedShapes) {            
            //sort intersectedShapes in ascending order by shape OBJECTID
            for (var i = 0; i < intersectedShapes.length; i++) {
                //set display order
                var fmItem = features.getItemByObjectId(intersectedShapes[i].OBJECTID, 'polygon');                                             
                intersectedShapes[i].DISPLAY_ORDER = (fmItem==null)?0:fmItem.STYLE.DISPLAY_ORDER;
            }
            intersectedShapes.sort(function(a, b) {  
                return a.DISPLAY_ORDER - b.DISPLAY_ORDER; 
            });             
            return intersectedShapes;
        },
        moveToBack: function(e) {
            //console.log("moveToBack !", e);
            if (e.hasOwnProperty("currentTarget")) {
                var graphics = nisis.ui.mapview.map.getLayer('Polygon_Features').graphics;
                //find the shape
                for (var i = graphics.length; i--;) {
                    if  (graphics[i].hasOwnProperty("attributes") &&  graphics[i].attributes.OBJECTID == self.selectedOBJECTID) {
                        var shape = graphics[i].getDojoShape();
                        shape.moveToBack();
                        break;
                    }
                }
            }
            else {
                var shape = e.getDojoShape();
                shape.moveToBack();
            }

        },
        toggleVisibility: function(e) {
            // console.log("toggleVisibility !", e);
            var objectId = Number($(e).parent().find('span').text());
            var graphics = nisis.ui.mapview.map.getLayer('Polygon_Features').graphics;
            //find the shape
            for (var i = graphics.length; i--;) {
                var graphic = graphics[i];
                if  (graphic.hasOwnProperty("attributes") && graphic.attributes.OBJECTID == objectId) {
                    if (graphic._visible) {
                        graphic.hide();
                    }
                    else {
                        graphic.show();
                    }
                    break;
                }
            }
        },
        findShapesPrevSiblingId: function(objectId) {
            var graphics = nisis.ui.mapview.map.getLayer('Polygon_Features').graphics;
            //find the shape
            for (var i = graphics.length; i--;) {
                var graphic = graphics[i];
                if  (graphic.hasOwnProperty("attributes") && graphic.attributes.OBJECTID == objectId) {
                    var shape = graphic.getDojoShape();
                    var previousSibling = shape.rawNode.previousSibling;
                    if (previousSibling!=null && previousSibling.hasOwnProperty("e_graphic")) {
                        return previousSibling.e_graphic.attributes.OBJECTID;
                    }
                    else {
                        return 0; //error or at the top already
                    }
                }
            }
        },
        doesShapeIntersect: function(OBJECTID, intersectedShapes) {
            //is the objectId in the array of intersectedShapes?
            for (var j= intersectedShapes.length; j--;) {
                    if  (intersectedShapes[j].OBJECTID == OBJECTID) {
                        //console.log("found intersected shape: ", OBJECTID);
                        return true;
                    }
            }
            return false;
        },
        persistShapesOrder: function() {
            var shapes = [];
            var $tree = $("#Polygon_Features_layer");

            $tree.find("path").each(function(i){
                if ($(this).length>0 && $(this)[0].e_graphic.hasOwnProperty("attributes")){
                    var shape = $(this)[0].e_graphic.attributes.OBJECTID;
                    shapes.push(shape);
                }
            });
            //console.log("about to persist shapes ",shapes);

            $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        "action": "save_shape_order",
                        "dataType": "json",
                        "shapes": shapes,
                        "userid": Number(nisis.user.userid)
                    },
                    success: function(data, status, req) {
                        if (data!= null) {
                            var resp = JSON.parse(data);
                                if (resp!=null && resp.result == "1") {                                    
                                    console.log("Updated shapes order successfully"+data);
                                    //update the shapes display order in the FM tree. Chain updating the shapes DISPLAY_ORDER in the My Features tree, Groups Tree and finally update the Arrange Order Tab                                                                      
                                    features.refreshTreesAndByPassMap = true;
                                    features.saveNodesToExpandAndCheck("user", null);
                                    features.myFeaturesDataSource.read();                                   
                                }
                                else  {
                                    nisis.ui.displayMessage("Unable to reorder shapes", 'error');
                                    console.log("Unable to reorder shapes");
                                }
                        }
                        else {
                                nisis.ui.displayMessage("Unable to reorder shapes", 'error');
                                console.log("Unable to call save_shape_order");
                        }
                    },
                    error: function(req, status, err){
                        console.error("ERROR in ajax call to reorder shapes");
                        nisis.ui.displayMessage(" Unable to reorder shapes", 'error');
                    }
            });
        },
        updateShapeName: function(itemId, name) {
            var $shapeWrap = $("#shapes-manager-list");
            if ($shapeWrap.length>0) {
                var grid = $shapeWrap.data("kendoGrid");
                if (grid) {            
                    var dataSource = grid.dataSource;
                    var datasourcedata = grid.dataSource.data();

                    //go through dataSource and change name
                    for (var ndx = 0; ndx < datasourcedata.length; ndx++) {
                        if (datasourcedata[ndx].ITEMID == itemId) {
                            datasourcedata[ndx].NAME = name;
                            dataSource.data(datasourcedata);
                            break;
                        }
                    }
                }
            }
        },
        getVisibleShapesOrder: function() {
            var shapes = this.getShapes();
            if (shapes.length>0) {
                $.ajax({
                    url: "api/",
                    type: "POST",
                    data: {
                        "action": "get_visible_shapes_ordered",
                        "dataType": "json",
                        "shapes": shapes,
                        "userid": Number(nisis.user.userid)
                    },
                    success: function(data, status, req) {
                        if (data!= null) {
                            var resp = JSON.parse(data); //"data.result":"7502,7505"
                                if (resp!=null && resp.result!=null) {
                                    console.log("Updated shapes order successfully"+resp.result);
                                }
                                else  {
                                    nisis.ui.displayMessage("Unable to reorder shapes", 'error');
                                    console.log("Unable to reorder shapes");
                                }
                        }
                        else {
                            nisis.ui.displayMessage("Unable to get visible shapes order", 'error');
                            console.log("Unable to reorder shapes");
                        }
                    },
                    error: function(req, status, err){
                        console.error("ERROR in ajax call to get visible shapes order");
                        nisis.ui.displayMessage(" Unable to get visible shapes order", 'error');
                    }
                });
            }
        }

    });


    return self;

});
