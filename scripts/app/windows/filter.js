/*
 * Filter window ==> Filter layers base on IWLs or Filter Shapes
*/
define(["app/windows/tableview"],function(tableview){
	return kendo.observable({
		resizeFilterTable: function(e){
			//console.log(e);
			var h = e.height - 100 + 'px';
			//$('twl-talbe').css('height', h)
		},
		canLoadIWL: true,
		iwlSelected: false,
		selectedIWL: null,
		NumIWLObjects: 0,
		iwlObjectsListed: false,
		iwlColumns: [{
			field: 'ID',
			title: 'ID',
			width: 60 
		},{
			field: 'NAME',
			title: 'NAME',
			width: 150 
		},{
			field: 'DESCRIPTION',
			title: 'DESCRIPTION',
			width: 200 
		},{
			field: 'CREATED_USER',
			title: 'CREATED BY',
			width: 100 
		},{
			field: 'CREATED_DATE',
			title: 'CREATION DATE',
			width: 100 
		},{
			field: 'LAST_UPDATED_USER',
			title: 'LAST UPDATED USER',
			width: 100 
		},{
			field: 'LAST_UPDATED_DATE',
			title: 'LAST UPDATED DATE',
			width: 100 
		}],
		iwlSource: new kendo.data.DataSource({
			data: [],
			/*transport: {
				read: function(options){
					console.log(options);
					$.ajax({
						url: "api/",
						success: function(results){
							console.log(results);
							options.success(results);
						},
						error: function(results){
							console.log(results);
							options.error(results);
						}
					});
				},
				parameterMap: function(options, operation){
					console.log(arguments);
					if(operation === 'read'){
						return {
							action: 'get_iwls',
							username: nisis.user.username,
							usergroups: nisis.user.groups
						}
					}
				}
			},*/
			pageSize: 10,
		}),
		loadIWLs: function(e){
			console.log('Loading IWLs: (filter.js) ', e);
			var $this = this;
			//get list of iwls
			$.ajax({
				url: "api/",
				get: "GET",
				data: {
					action: 'get_iwls',
					username: nisis.user.username,
					usergroups: nisis.user.groups
				},
				success: function(data, status, xhr){
					//console.log(data);
					var resp = JSON.parse(data);
					$this.iwlSource.data(resp.results);
					$this.buildIWLTable();
				},
				error: function(xhr, status, err){
					console.log(err);
				}
			});
		},
		buildIWLTable: function(){
			console.log('Building IWL Table: ');
			var $this = this;
			$('#iwl-table').kendoGrid({
				columns: $this.get('iwlColumns'),
				dataSource: $this.get('iwlSource'),
				sortable: {
                    mode: 'single'
                },
				filterable: true,
                columnMenu: true,
                resizable: true,
                reorderable: true,
                selectable: true,
                pageable: {
                    pageSizes: [10,20,30,40],
                    pageSize: 10,
                    buttonCount: 5
                },
                change: function(e){
                	$this.getSelectedIWL(e);
                }
			});
		},
		getSelectedIWL: function(e){
			var iwls = e.sender.dataItem(e.sender.select());
			console.log('Selected IWL: ',iwls,e);
			this.set('selectedIWL', iwls);
			this.set('iwlSelected',true);
		},
		loadIWLFeatures: function(e){
			console.log('Loading IWL Features: ',e);
		},
		updateLayers: function(e){
			console.log('Updating Map Layers: ',e);
			var $this = this;
			var btns = ['Hide', 'De-emphasize'];
			nisis.ui.displayConfirmation({
				title: 'Layer updates',
				message: "How would you like to display your layer? 'Hide' or 'De-emphasize' features?",
				buttons: btns
			}).then(function(response){
				console.log(response);
				if($.inArray(response, btns) !== -1){
					$this.applyUpdateToLayer(response);
				}
			},function(e){
				console.log(e);
			});
		},
		applyUpdateToLayer: function(response){
			var iwl = this.get('selectedIWL');
			var req = null;
			if(!iwl){
				nisis.ui.displayMessage('The application could not apply your request. Try again.','','error');
			} else {
				req = {
					layer: iwl.LAYER,
					name: iwl.NAME
				};
				nisis.ui.getIncidentWatchList(req)
				.then(function(ids){
                    //console.log(ids);
                    var oids = ids;
                    nisis.ui.displayMessage('We found: ' + oids.length + ' features in the selected IWL ...', '', 'info');
                    var layer = nisis.ui.mapview.map.getLayer(iwl['LAYER']);
                    if(response === 'Hide'){
		                var def = "OBJECTID = " + oids.join(' OR OBJECTID = ');
		                layer.setDefinitionExpression(def);
		            } else if ('De-emphasize') {
		                //store the update-end handler and use pointer to remove it
		                tableview.layerUpdates[layer.id] = layer.on('update-end',function(evt){
		                	tableview.deemphasizeFeatures(evt, ids);
		                });
		                //force layer to redraw
		                layer.redraw();
		            }
		            //enable clear def query button
                },function(e){
                    console.log(e);
                    nisis.ui.displayMessage(e, '', 'error');
                },function(note){
                    nisis.ui.displayMessage(note, '', 'info');
                });
			}
		},
		clearIWLFeatures: function(e){
			tableview.clearUpdate(this.get('selectedIWL')['LAYER']);
		}
	});
});