
 define(["app/renderers"], function(renderers) {
	return kendo.observable({
	    clearArea: function(areaTab) {
		    areaTab.find('.shapesGrid').find('.k-grid').remove();
		},
		//BK: 01/04/2016 ==> Why is this here if it can't access tfrBuilder content?
		updateCurrentTFRShapeName: function(shapeId, name) {
		    var grids = $('#tfr-div').find('.shapesGrid'),
		    	dataSource,
		    	datasourcedata;

		    if ( grids.length > 0 ) {
		    	$.each(grids, function(i, grid) {
					dataSource = $(grid).find('.k-grid').data("kendoGrid").dataSource;
		            datasourcedata = dataSource.data();

                    //go through dataSource and change name
		            for (var ndx = 0; ndx < datasourcedata.length; ndx++) {
		            	if (datasourcedata[ndx].OBJECTID == shapeId) {
		            		datasourcedata[ndx].NAME = name;
		            		dataSource.data(datasourcedata);
		            		break;
		            	}
					}
				});
		    }
		},
		getCurrentTFRShapeIds: function() {
		    var shapeIds = [],
		    	grids = $('#tfr-div').find('.shapesGrid'),
		    	dataSource,
		    	datasourcedata;

		    if (grids.length>0) {
		    	$.each(grids, function(i, grid) {
					dataSource = $(grid).find('.k-grid').data("kendoGrid").dataSource;
		            datasourcedata = dataSource.data();

		            for (var ndx = 0; ndx < datasourcedata.length; ndx++) {
		            	shapeIds.push(datasourcedata[ndx].OBJECTID);
					}
				});
		    }

	        return shapeIds;
		},
		getShapes: function(areaTab) {
			/*
			 * BK: 01/05/2016 ==> This function is being called only
			 * when the user saves the tfr. This means the areas[i]shape
			 * item will not have an updated VOR value from the Grid.
			 * Solution: I have added a function called 'updateCurrentTfrVor'
			 * that should take care of this issue.
			 */
		    var grid = areaTab.find('.shapesGrid').find('.k-grid'),
		    dataSource = grid.data("kendoGrid").dataSource,
	        data = dataSource.data(),
	        result = JSON.stringify(data);

	        return result;
		},
		add: function(objectid, shapeName) {
		    //determine which shapes grid the add button (that was clicked on) was underneath

		    var $drawDiv = $('#drawing-div'),
		    	shapeIds = [],
		    	drawInitiatorBtn = $drawDiv.data('drawInitiatorBtn');

		    //find the closest grid
		    var grid = $(drawInitiatorBtn).parent().siblings('.shapesGrid').find('.k-grid');

		    if (grid.length>0) {
		        var dataSource = grid.data("kendoGrid").dataSource;

		        var list = dataSource.data();
		        var row = {};

				row["OBJECTID"] = objectid;
				row["NAME"] = shapeName;
				row["VOR"] = "Select VOR";

		        list.push(row);
		        dataSource.data(list);

		        for (i=0; i<list.length; i++) {
			        item = list[i];
			        var newShape = {};
				    newShape.OBJECTID = list[i].OBJECTID;
				    newShape.NAME = list[i].NAME;
				    newShape.VOR = list[i].VOR;
			        shapeIds.push(newShape);
	    		}
	    	}

	    	//cleanup
            $drawDiv.removeData('drawInitiatorBtn');
	        $drawDiv.data('kendoWindow').close();

	        return shapeIds;
		},
		remove: function(e) {
		    //determine which shapes grid the delete button (that was clicked on) was underneath
            var grid = $(e.currentTarget).parent().siblings('.shapesGrid').find('.k-grid').data("kendoGrid"),
	        	dataSource = grid.dataSource,
	        	selRow = grid.select(),
	        	selData = null,
	        	oid = null;

	        var numberOfRowsCurrentPage=$(e.currentTarget).parent().siblings('.shapesGrid').find('.k-grid table tbody tr').length;

	        //make sure there is a seleted row
	        if (!selRow || (selRow && !selRow.length)) {
	        	console.log("There is no shape selected");
	        	return;
	        }
	        //get grid data and selected shape id
        	selData = grid.dataItem(selRow).toJSON();
        	oid = selData['OBJECTID'];

        	var polygons = nisis.ui.mapview.map.getLayer('tfr_polygon_features'),
	        	graphics = polygons.graphics;

	        //make sure the layer has been loaded
	        if (!polygons) {
	        	console.log("TFR Polygon layer does not seem to be loaded");
	        	return null;
	        }

	        /*
	         * BK: 01/03/2016 => it's always better to query the layer for selected features.
	         * Using graphic to find the selected feature might not return anything if the
	         * graphic is not in the current view extent
	         */
	        nisis.ui.mapview.util.queryFeatures({
	        	layer: polygons,
	        	objectIds: [oid],
				returnGeom: true
	        }).then(function(result) {

	        	if (result && result.features && result.features.length) {
				    var item = grid.dataItem(selRow),
				    	feature = result.features[0],
	            		//rem = nisis.ui.mapview.util.removeFeatureFromUserLayers(feature, true); //BK: 01/03/2016 ==> this will fail if you query the db.
	            		rem = polygons.applyEdits(null,null,[feature]); //BK: 01/03/2016 ==> Use the polygons layer here
	            	//always make sure the feature is deleted before removing the item from the grid
	            	if (rem) {
	            		rem.then(function(d) {
	            			dataSource.remove(item);
	            			grid.refresh();
	            			//refresh the grid and the pager bar, go to the previous page if the last shape on this page has been deleted
	            			grid.pager.refresh();

	            			pager = grid.pager;
	            			pageSize=pager.pageSize();
	            			//uh oh we are deleting the last row on the page. Go to a previous page so we dont display empty page. NISIS-1972
	            			if (numberOfRowsCurrentPage == 1) {
		            			currentPage = pager.page();
		            			if (currentPage>1) {
		            				grid.pager.page(currentPage-1);
		            			}
	            			}


	            		},function(error) {
	            			console.log("The selected shape was not removed:", error);
	            		});
	            	} else {
	            		console.log("There was an error removing selected shape", item);
	            	}
				}

			}, function(error) {
				console.log("The selected shape was not found:", error);
			});

	        return oid;
		},
		removeAreaShapes: function(area) {
		    //remove all shapes from the area specified
	        var def = new $.Deferred(),
	        	polygons = nisis.ui.mapview.map.getLayer('tfr_polygon_features'),
	            grid = area.find('.shapesGrid').find('.k-grid'),
		        dataSource = grid.data("kendoGrid").dataSource,
			    list = dataSource.data();

			//make sure there are any shapes associated with the area
			if (!list.length) {
				console.log("There are no shapes associated with this area");
				return def.resolve(0);
			}

			list = list.toJSON();

			var oids = $.map(list, function(l, i) {
				return l.OBJECTID;
			});

			//query features to be deleted
			nisis.ui.mapview.util.queryFeatures({
	        	layer: polygons,
	        	objectIds: [oids],
				returnGeom: true
	        }).then(function(result) {
	        	//check for valid result
	        	if (result && result.features && result.features.length) {
				    var features = result.features,
				    	len =  features.length,
				    	nDel = 0,
				    	errors = 0;
				    //delete one feature at the time. Note: I tried deleting all at the same time without success
				    $.each(features, function(i, feat) {
				    	nDel++;
				    	//apply edit with list of features to be deleted
				    	var rem = polygons.applyEdits(null, null, [features[i]]);
		            	//always make sure the feature is deleted before removing the item from the grid
		            	if (rem) {
		            		rem.then(function(d) {
		            			console.log("The selected shape was removed:", d);
		            			if (nDel == len && errors == 0) {
		            				def.resolve("seccess");
		            			} else if (nDel == len && errors > 0) {
		            				def.reject(errors + " (s) features have not been deleted");
		            			}
		            		},function(error) {
		            			errors++;
		            			console.log("The selected shape was not removed:", error);
		            			if (nDel == len) {
		            				def.reject(error);
		            			}
		            		});
		            	} else {
		            		console.log("There was an error removing selected shape", list);
		            		if (nDel == len) {
		            			def.reject("There was an error removing selected shape");
		            		}
		            	}
	            	});
				}

			}, function(error) {
				console.log("The selected shape was not found:", error);
				def.reject(error);
			});

	        return def.promise();

		    /*for (i=0; i < list.length; i++) {
			   	var item = list[i];
			  	//go through the graphics
			    $.each(polygons.graphics, function(i,graphic) {
				    //find the match
				    if (graphic.attributes['OBJECTID'] == item.OBJECTID) {
				       	nisis.ui.mapview.util.removeFeatureFromUserLayers(graphic, true);
				    }
				}); //for each graphic in User Polygon Layer
		    }	*/
		},
        dropDownEditorVORSAjax: function(container, options) {
        	//console.log("Adding VOR DD to Shape Grid:", arguments);
			var $this = this,
				$container = container,
				$options = options,
				oid = options.model.OBJECTID;

            var currentInputValue = $options.model.VOR;
            var	qOptions = {
				layer:  nisis.ui.mapview.map.getLayer('tfr_polygon_features'),
				//where: "OBJECTID = " + oid,
				objectIds: [oid], //BK: 01/02/2016 ==> This works better
				returnGeom: true
			};

            nisis.ui.mapview.util.queryFeatures(qOptions)
			.then(function(result) {
				if (!result || !result.features || !result.features.length) {
				    console.error('dropDownEditorVORSAjax : queryFeatures failed');
					return;
				}
				//we know its only one
				geom = result.features[0].geometry;
				center = geom.getCentroid();

				if ( center ) {
					lat = center.y,
					lon = center.x
				}

				if (!lat || !lon) {
					console.error('dropDownEditorVORSAjax Search error: ');
					return;
				};
				//handle drop down change event
				var chgFunction = function(e) {
					var indexes, ndx,
						kgrid = $(e.sender.span).closest('.k-grid'),
						vor = e.sender.text();

			        var dataSource = kgrid.data("kendoGrid").dataSource;
			        var tr = $(e.sender.span).closest('tr');
			        var td = tr.find('td:first');
			        var objectId = td.text();
			        //BK: Just update the VOR value of the matched item
			        $.each(dataSource.data(), function(i, shape) {
			        	if(shape.OBJECTID == objectId) {
			                shape["VOR"] = vor;
			            }
			        });

			        /*indexes = $.map(dataSource.data(), function(obj, index) {
			            if(obj.OBJECTID == objectId) {
			                return index;
			            }
			        });
			        //only one
			        ndx = indexes[0];
			        //update the VOR column for ndx row
			        dataSource.data()[ndx]["VOR"] = e.sender.text();*/

			        //BK: The vor is only being updated in the grid
			        nisis.ui.mapview.util.updateCurrentTfrVor(objectId, vor);
				};

				//Get closest VOR from Shape's centroid
	            $.ajax({
					url: 'api/lookup.php',
					type: 'GET',
					data: {
						target: 'closest-vor',
						filter: 'vor',
						lat: lat,
						lon: lon,
						num: 30
					},
					dataType: 'json',
					success: function(data, status, xhr) {
						// console.log("Closest VOR Data:", data);
						if (data) {
							var vors = [];

							for(var i = 0; i < data.length; i++) {	
  								var vor = data[i];
  								var item = {};
  								var label = vor.LABEL;
  								label = label.replace(/\(.*?\)/, '');
  								//BK: 01/05/2016 ==> Get ride of the extra space
  								label = $.trim(label);
								item["text"] = label;
								item["value"] = label;
								item["ans_status"] = vor.ANS_STATUS;
                                item["mag_var_num"] = vor.MAGNETIC_VAR_N;
  								vors.push(item);
							}
							console.log("vors:", vors);
							// for (var i = 0; i < vors.length; i++) {
							//     var item = vors[i];
						    //     if (item["text"].indexOf("-VOR")==3) {
							//     	//see if we got a TACR for this VOR
							//     	var first3LettersVOR = item["text"].substring(0,3);
							//     	for (var j = 0; j < vors.length; j++) {
							// 		    var itemJ = vors[j];
							// 		    if (itemJ["text"].indexOf("-TACR")>0) {
							// 		    	//see if we got a TACR for this VOR
							// 		    	var first3LettersTACR = itemJ["text"].substring(0,3);
							// 		    	if (first3LettersTACR==first3LettersVOR) {
							// 		    		//tack on the TACR to VOR
							// 		    		item["text"] = item["text"]+", TACR";
							// 					item["value"] = item["text"];
							// 					itemJ["text"] = ""; //delete later
							// 					break;
							// 		    	}
							// 		    }
							// 		}
							//     }
							// }
                            for (var i = 0; i < vors.length; i++) {
							    var item = vors[i];
						        if (item["text"].indexOf("-VOR")==3) {
							    	//possible VOR suffixes
                                    var vorSuffixes = ['TACR', 'DME'];
                                    //search for and concatenate VOR suffices from above list
                                    vorSuffixes.forEach(function(_el, _idx, _arr){
                                        var first3LettersVOR = item["text"].substring(0,3);
    							    	for (var j = 0; j < vors.length; j++) {
    									    var itemJ = vors[j];
    									    if (itemJ["text"].indexOf("-" + _el)>0) {
    									    	//see if we got a TACR for this VOR
    									    	var first3LettersTACR = itemJ["text"].substring(0,3);
    									    	if (first3LettersTACR==first3LettersVOR) {
    									    		//tack on the TACR to VOR
    									    		item["text"] = item["text"] + ", " + _el;
    												item["value"] = item["text"];
    												itemJ["text"] = ""; //delete later
    												break;
    									    	}
    									    }
    									}
                                    });
							    }
							}
							//now delete all empty ones
							for(var i = vors.length; i--;) {
					            if(vors[i]["text"] === '') {
					                 vors.splice(i, 1);
					            }
					        }

					        //OK, now see if the resulting vors list has more then 5 items, and delete all other then the first 5 items
					        if (vors.length>=5) {
					        	vors.splice(5, vors.length-5);
					        }

	               		    //need to update datasource
							$('<input data-bind="value:VOR"/>')
				            .appendTo($container)
				            .kendoDropDownList({
					            dataTextField: "text",
					            dataValueField: "value",
					            change: chgFunction,
								dataBound: function() {
									/* When the data is bind the first element is always selected,
									 * so every time the databound is called the dropdown will select
									 * the correct value since this dropdown is create every time
									 * the user clicks the element in the grid.
									 */
									this.value(currentInputValue);
									this.trigger("change");
								},
					            dataSource: vors,
					            //Template for selected value
					            valueTemplate: '#data.ans_class = data.ans_status ? "ans_status_" + data.ans_status : "";#'+
					            '<span class="ans_status #:data.ans_class#"></span><span>#:data.text#</span>',
					            //Template for default values
					            template: '#data.ans_class = data.ans_status ? "ans_status_" + data.ans_status : "";#'+
					            '<span class="k-state-default ans_status #:data.ans_class#"></span>'+'<span class="k-state-default">#:data.text#</span>',
					        });
							//var dropdownlist = $container.find('input').data("kendoDropDownList");
							// dropdownlist.value(currentInputValue);
							// dropdownlist.trigger("change");
						} else {
							console.log('VOR Search error: ', arguments);
						}
					},
					error: function(xhr, status, err) {
						console.log('VOR Search error: ', arguments);
					}
				});

			}, function(error) {
				console.log('VOR Search error: ', arguments);
			});
		},
		createGrid: function(shapes, $shapesGridWrapper) {
			'use strict';

            //console.log("shapes:", shapes);

			var ul, li,
				div, key, grid;

			//check make sure $shapesGridWrapper has class .shapesGrid
			if ( $shapesGridWrapper.length == 0 || $shapesGridWrapper.hasClass("shapesGrid") == false) {
				console.error('unable to create grid, .shapesGrid placeholder not there: ');
				return;
			}

			//there is only one shapes grid per tab. If it happens that there is an empty grid and
			//we are asked to create new grid with shapes, remove the old grid and create the new one
			grid = $shapesGridWrapper.find('.k-grid');

			if (grid.length > 0) {
				grid.remove();
			};

			//BK: Keep track of shape ids
			var oids = $.map(shapes, function(shape,i) {
				return parseInt(shape.OBJECTID);
			});
			renderers.tfrActiveShapes = renderers.tfrActiveShapes.concat(oids);
			var lyr = nisis.ui.mapview.map.getLayer("tfr_polygon_features");

			if(lyr && lyr.refresh) {
				lyr.refresh();
			}

			grid = $('<div>').appendTo($shapesGridWrapper);

			grid.kendoGrid({
				dataSource: {
					data: shapes,
					pageSize: 2,
					schema: {
						model: {
							fields: {
								OBJECTID: {
									type: 'string', editable: false
								},
								NAME: {
									type: 'string', editable: false
								},
								VOR:   {
									type: 'string', editable: true, nullable: false
								}
							}
						}
					}
				},
				height: 200,
				//scrollable: true,
				//sortable: true,
				//filterable: true,
				selectable: 'single',
				editable: true,
				pageable: {
					//input: true,
					//numeric: true
				},
				columns: [{
						field: "OBJECTID",
						title: "OBJECTID",
						hidden: true
					}, {
						field: 'NAME',
						title: 'Shape Name'
					},
					    {
					    field: 'VOR',
					    title: 'Closest VOR',
	                    attributes: {"class": "vor-picklist-arrow" },
	                    name: "vor_picklist",
	                    editor: this.dropDownEditorVORSAjax
	                }
				]
			});
		}
	});
});
