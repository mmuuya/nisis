/*
 * Map Layers and layers tree
 */
define(["dojo/_base/Color",
	"esri/virtualearth/VETiledLayer","esri/layers/ArcGISDynamicMapServiceLayer","esri/layers/FeatureLayer",
	"esri/graphic","esri/layers/GraphicsLayer","esri/layers/TableDataSource","esri/layers/LayerDataSource",
	"esri/layers/wms","esri/layers/WMSLayer","esri/layers/FeatureTemplate","esri/layers/LabelLayer"
	,"esri/graphic","esri/layers/ArcGISImageServiceLayer"
	,"esri/symbols/TextSymbol","esri/renderers/SimpleRenderer"
	,"app/layersConfig"
	,"app/attributes"
	,"app/renderers"
	,"app/windows/tableview"
	,"app/fonts"
	],function(Color,
		VETiledLayer,ArcGISDynamicMapServiceLayer,FeatureLayer,
    	Graphic,GraphicsLayer,TableDataSource,LayerDataSource,
    	Wms,WMSLayer,FeatureTemplate,LabelLayer
    	,Graphic,ArcGISImageServiceLayer
    	,TextSymbol,SimpleRenderer
    	,layersConfig
    	,attributes
    	,renderers
    	,tableview
    	,fonts
	) {
    //object to be returned
	var mapview = nisis.ui.mapview,
	layers = kendo.observable({
		container: "overlays-toggle",
		isNisisActive: true,
		nodes: function(){
			return {
				"tfr": {"text":"TFR Features", "id": "tfr", "type": "folder", "parent": null,
					"hasChildren": true, "checked": true, "expanded": true, "items": []},
				"user": {"text":"My Features", "id": "user", "type": "folder", "parent": null,
					"hasChildren": true, "checked": true, "expanded": true, "items": []},
				"groups": {"text":"NISIS Group Features", "id": "groups", "type": "folder", "parent": null,
					"hasChildren": true, "checked": false, "expanded": false, "items": [{
						"text": 'No groups found', "id": 'empty', "type": "image", "hasChildren": false, "checked": true}]},
				"nisisto": {"text":"NISIS Tracked Objects", "id": "nisisto", "type": "folder", "parent": null, "hasChildren": true, "checked": false, "expanded": true, "items": []},
		        "dot": {"text":"DOTMAP Tracked Objects", "id": "dot", "type": "folder", "parent": null, "hasChildren": true, "checked": false, "expended": false, "items": []},
		        "oLayers": {"text":"Other NISIS Layers", "id": "oLayers", "type": "folder", "parent": null, "hasChildren": true, "checked": false, "expanded": false, "items": []},
	            "faa": {"text":"FAA", "id": "faa", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            "fema": {"text":"FEMA", "id": "fema", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
				"ntad": {"text":"NTAD", "id": "ntad", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            "pipelines": {"text":"Pipelines", "id": "pipelines", "type": "folder", "parent": "dot",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            "hsip": {"text":"HSIP", "id": "hsip", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            /*"noaa": {"text":"NOAA", "id": "noaa", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},*/
	            "census": {"text":"CENSUS", "id": "census", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            "ago": {"text":"ArcGIS Online", "id": "ago", "type": "folder", "parent": "null",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            "inter": {"text":"INTERNATIONAL", "id": "inter", "type": "folder", "parent": "oLayers",
	            	"hasChildren": true, "checked": false, "expended": false, "items": []},
	            //Threat Layers
		        "tlyrs": {"text":"Threat Layers", "id": "tlyrs", "type": "folder", "parent": null,
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "tweather": {"text":"Weather", "id": "tweather", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
 	            "rivers": {"text":"Advanced Hydrologic Prediction Service", "id": "rivers", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},

		        //
            "wcforc": {"text":"Forecasted Tropical", "id": "wcforc", "type": "folder", "parent": "tlyrs",
              "hasChildren": true, "checked": false, "expended": false, "items": []},
	            "wmodels": {"text":"Models", "id": "wmodels", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wmhpa": {"text":"Heavy Precipitation Algorithm", "id": "wmhpa", "type": "folder", "parent": "wmodels",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wmlpa": {"text":"Lightning Prediction Algorithm", "id": "wmlpa", "type": "folder", "parent": "wmodels",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wmlaps": {"text":"Local Analysis and Prediction System", "id": "wmlaps", "type": "folder", "parent": "wmodels",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wmqpe": {"text":"Quantitative Prediction Estimate", "id": "wmqpe", "type": "folder", "parent": "wmodels",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "rivers": {"text":"Advanced Hydrologic Prediction Service", "id": "rivers", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},

            "wcforc": {"text":"Forecasted Tropical", "id": "wcforc", "type": "folder", "parent": "tlyrs",
              "hasChildren": true, "checked": false, "expended": false, "items": []},
		        //
		        "wobs": {"text":"Observations", "id": "wobs", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wocl": {"text":"CONUS Lightning", "id": "wocl", "type": "folder", "parent": "wobs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "woso": {"text":"Surface Observations", "id": "woso", "type": "folder", "parent": "wobs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wocww": {"text":"US Watches and Warnings", "id": "wocww", "type": "folder", "parent": "wobs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wofpc": {"text":"Fronts and Pressure Centers", "id": "wofpc", "type": "folder", "parent": "wobs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "woco": {"text":"Convective Outlook", "id": "woco", "type": "folder", "parent": "wobs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        //
		        "wrsat": {"text":"Radar and Satellite", "id": "wrsat", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wctrop": {"text":"Current Tropical", "id": "wctrop", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []},
		        "wfires": {"text":"Fires", "id": "wfires", "type": "folder", "parent": "tlyrs",
		        	"hasChildren": true, "checked": false, "expended": false, "items": []}
			};
		},
    	getTreeNodes: function(whichTree){    		
			var $this = this,
				data = null,
				len = layersConfig.length,
				tree = this.nodes(),
				groups = [];			

            var layers = $("#overlays-toggle");
            if (!layers) {
            	//console.log('Layers tree update: ', 'no layers div');
            	return;
            }

            var overlays = layers.data("kendoTreeView");
            if (!overlays) {
            	//console.log('Layers tree update: ', 'no overlays');
            	return;
            }
            //console.log('Layers tree update: ', this);
            var olDS = overlays.dataSource,
                treeNodes = olDS.data();

			//add nodes from layersConfig file
			$.each(layersConfig, function(i,layer){
				if(layer.add){
					if(layer.group){
						if(tree[layer.group] /*&& layer.id !== 'all_objects'*/){
							//add only layers that needs to be on the map
							if( layer.maplayers === undefined || layer.maplayers === true ) {
								//console.log(layer.id + ": " + layer.query);

								tree[layer.group].items.push({
									"text": layer.name,
									"id": layer.id,
									"type": "image",
									"parent": layer.group,
									"hasChildren": false
									,"checked": layer.visible
								});
								//add this node only for layers with label option
								if (layer.label) {
									//labels
									tree[layer.group].items.push({
										"text": layer.name + " Labels",
										"id": layer.id + "_labels",
										"type": "image",
										"parent": layer.group,
										"hasChildren": false
										,"checked": layer.visible
										//,"hasLabel": true ==> not been added
									});
								}
							}
						}
					} else {
						console.log("No group assignment for: ", layer.id);
					}
				}

				if(i == (len-1)){
					//tree.groups.items = nisis.user.features;
					//console.log("==============tree.groups.items=================",tree.groups.items);
					tree.dot.items.push(tree.pipelines);
					tree.oLayers.items.push(tree.faa,tree.fema,tree.ntad,tree.hsip,tree.census,tree.inter);
					//Weather layers ==> Uncomment this when ready
					tree.wmodels.items.push(tree.wmhpa,tree.wmlpa,tree.wmlaps,tree.wmqpe);
					tree.tweather.items.push(tree.wmodels); 
					tree.wobs.items.push(tree.wocl,tree.woso,tree.wocww,tree.wofpc,tree.woco);
					tree.tweather.items.push(tree.wobs, tree.wrsat, tree.wctrop, tree.wcforc, tree.wfires);
          			tree.tlyrs.items.push(tree.tweather, tree.rivers);	
					

					if (whichTree == 'threat-layers') {
						data = [tree.tweather, tree.rivers];
					}
					else if (whichTree == 'overlays-toggle') {
						data = [tree.tfr,tree.nisisto,tree.dot,tree.oLayers];
					}
					else if (whichTree == 'arcgis-online') {						    	
					    data = [tree.ago];						
					}
				}
	    	});
	    	// return data
			return data;
    	},
    	
    	dataSource: null,
    	
    	treeNodes: function(whichTree){    		
    		console.log("doing 'treeNodes()'");
    		var $this = this,
    			source;
    		var data = this.getTreeNodes(whichTree);

			source = new kendo.data.HierarchicalDataSource({
	    		data: data
	    	});

			$this.set('dataSource', source);
			return source;			
		},
		// laodUserGroups: function(){
		// 	var $this = this, groups,
		// 		treeSource = $this.get('dataSource');
		// },
		// polulateTree: function(lyrs){
		// 	//console.log("Populating layer tree ...");
		// 	var layers = lyrs ? lyrs : layersConfig;
		// 	console.log("Layer config: ",layers);
		// 	var overlays = [],
		// 	treeSource = $("#overlays-toggle").data("kendoTreeView").dataSource;

		// 	$.each(layers, function(i,layer){
		// 		if(layer.add){
		// 			if(layer.group){
		// 				console.log('Layer group:', layer.group);
		// 				var parentNode = treeSource.get(layer.group);
		// 				if(!parentNode){
		// 					console.log("App Could not find node: ", layer.group);
		// 					//"parent"Node = treeSource.get("oLayers");
		// 				}
		// 				//console.log(parentNode);
		// 				treeSource.get(layer.group).append({
		// 					"text": layer.name + 'baboyma',
		// 					"id": layer.id,
		// 					"type": "image",
		// 					"checked": layer.visible
		// 				});
		// 			} else {
		// 				console.log("No group assignment for: ", layer.group);
		// 			}
		// 		}
		//	});
		// },
		updateLayersTree: function() {
			//console.log("updating layers on Tree");
			var $this = this,
                layers = nisis.ui.mapview.layers,
                mapview = nisis.ui.mapview,
            	activeLayers = [],
            	apps = nisis.user.apps,
                app = null;

            require(['app/appConfig'], function(appConfig) {
            	app = appConfig.get('app');
            });

            if (!app || !apps.length || $.inArray(app, apps) !== -1 ) {
                return;
            }

            var layers = $("#overlays-toggle");
            if (!layers) {
            	//console.log('Layers tree update: ', 'no layers div');
            	return;
            }

            var overlays = layers.data("kendoTreeView");
            if (!overlays) {
            	//console.log('Layers tree update: ', 'no overlays');
            	return;
            }
            //console.log('Layers tree update: ', this);
            var olDS = overlays.dataSource,
                treeNodes = olDS.data();

            var tfrFeats = olDS.get('tfr'),
                nisisMyFeats = olDS.get('user'),
                nisisGrpFeats = olDS.get('groups'),
                nisisTO = olDS.get('nisisto'),
                dotmapTO = olDS.get('dot'),
                pipelines = olDS.get('pipelines'),
                activeLayers = [];

            // show|hide layers according to layer groups
            priveledgedLayers = function(){
        		// KOFI: NISIS-2327 make sure getUserGroups is being run once during login.
        		// KOFI: NISIS-2327-AGAIN:
        		//nisis.user.getUserGroups(nisis.user.userid).then(function(resp){
    			var myGroups = nisis.user.groupObjects;
        		var hidePipelines = true;

    			$.each(myGroups, function(i, group) {
    				var name = group.GNAME.toLowerCase(); //Converting to lower-case to be case insensitive
    				if (name === "administrators"){
    					nisis.user.isAdmin = true;
    				}
    				if((name.indexOf("pipeline") !== -1)){
		            	//If Pipelines group is found even once, then don't hide pipelines layer
    					hidePipelines = false;
    				}
    			});
    			if (hidePipelines){
					if(pipelines){
						olDS.remove(pipelines);
					}
					$this.pipelines = pipelines;
            		$this.updateLayersFromTree.call($this, $this.pipelines, 'hide');
				}
        		//});
            }
			priveledgedLayers();

            switch (app) {
            	case 'nisis':
            		// console.log('Layers for nisis');
            		if (tfrFeats) {
            			olDS.remove(tfrFeats);
            			$this.tfrFeatures = tfrFeats;
            			$this.updateLayersFromTree.call($this, $this.tfrFeatures, 'hide');
            		}

            		if (!nisisTO && $this.nisisTrackedObjects) {
            			olDS.insert(0, $this.nisisTrackedObjects);
            			$this.updateLayersFromTree.call($this, $this.nisisTrackedObjects, 'show');
            		}

            		if(!dotmapTO && $this.dotmapTrackedObjects){
            			// console.log('no dotmapTP');
            			olDS.insert(0, $this.dotmapTrackedObjects);
            			$this.updateLayersFromTree.call($this, $this.dotmapTrackedObjects, 'show');
            			// console.log('but dotmapTP shown');
            		}

            		if (!nisisGrpFeats && $this.nisisGroupFeatures) {
            			olDS.insert(0, $this.nisisGroupFeatures);
            			$this.updateLayersFromTree.call($this, $this.nisisGroupFeatures, 'show');
            		}

            		if (!nisisMyFeats && $this.nisisMyFeatures) {
            			olDS.insert(0, $this.nisisMyFeatures);
            			$this.updateLayersFromTree.call($this, $this.nisisMyFeatures, 'show');
            		}

            	break;

            	case 'tfr':
            		if (nisisMyFeats) {
            			olDS.remove(nisisMyFeats);
            			$this.nisisMyFeatures = nisisMyFeats;
            			$this.updateLayersFromTree.call($this, $this.nisisMyFeatures, 'hide');
            		}

            		if (nisisGrpFeats) {
            			olDS.remove(nisisGrpFeats);
            			$this.nisisGroupFeatures = nisisGrpFeats;
            			$this.updateLayersFromTree.call($this, $this.nisisGroupFeatures, 'hide');
            		}

            		if (nisisTO) {
            			olDS.remove(nisisTO);
            			$this.nisisTrackedObjects = nisisTO;
            			$this.updateLayersFromTree.call($this, $this.nisisTrackedObjects, 'hide');
            		}

            		if(dotmapTO){
            			// console.log('has dotmapTP');
            			olDS.remove(dotmapTO);
            			$this.dotmapTrackedObjects = dotmapTO;
            			$this.updateLayersFromTree.call($this, $this.dotmapTrackedObjects, 'hide');
            			// console.log('but dotmapTP removed');
            		}
                    
            		if (!tfrFeats && $this.tfrFeatures) {
            			//see if the tfr layers loaded - need to preload so they can draw in tfr mode
       
      	     		olDS.insert(0, $this.tfrFeatures);
						$this.updateLayersFromTree.call($this, $this.tfrFeatures, 'show');
            		}
            		
            		if (nisis.ui.mapview.map.getLayer('tfr_polygon_features')==null || 
						nisis.ui.mapview.map.getLayer('tfr_point_features')==null || 
						nisis.ui.mapview.map.getLayer('tfr_line_features')==null) {            					
            				layer = nisis.ui.mapview.layers['tfr_polygon_features'];
                            if (layer) {                    
                                nisis.ui.mapview.map.addLayer(layer);
                                layer.setVisibility(true);
                            }
                            layer = nisis.ui.mapview.layers['tfr_point_features'];
                            if (layer) {                    
                            	nisis.ui.mapview.map.addLayer(layer);
                            	layer.setVisibility(true);
                            }
                            layer = nisis.ui.mapview.layers['tfr_line_features'];
                            if (layer) {                    
                            	nisis.ui.mapview.map.addLayer(layer);
                                layer.setVisibility(true);
                            }
            		}           		

            		//console.log('Layers tree: ', treeNodes, tfrFeats, nisisMyFeats, nisisGrpFeats, nisisTO);

            	break;

            	default:
            		console.log('Can not update layers for unknown app module');

            		if (tfrFeats) {
            			olDS.remove(tfrFeats);
            			$this.tfrFeatures = tfrFeats;
            			$this.updateLayersFromTree.call($this, $this.tfrFeatures, 'hide');
            		}

            		if (nisisMyFeats) {
            			olDS.remove(tfrFeats);
            			$this.nisisMyFeatures = nisisMyFeats;
            			$this.updateLayersFromTree.call($this, $this.nisisMyFeatures, 'hide');
            		}

            		if (nisisGrpFeats) {
            			olDS.remove(tfrFeats);
            			$this.nisisGroupFeatures = nisisGrpFeats;
            			$this.updateLayersFromTree.call($this, $this.nisisGroupFeatures, 'hide');
            		}

            		if (nisisTO) {
            			olDS.remove(tfrFeats);
            			$this.nisisTrackedObjects = nisisTO;
            			$this.updateLayersFromTree.call($this, $this.nisisTrackedObjects, 'hide');
            		}
            }
		},
		updateLayersFromTree: function(node, action) {
			//console.log("updating layers from Tree, node:", node);

			if (!node || !action) {
				return;
			}

			var $this = this,
				map = nisis.ui.mapview.map,
				activeLayers = $this.get('activeLayers');

			var updateLayers = function(node) {

				$.each(node.items, function(i, nod) {

					if (nod.items && nod.items.length) {
						updateLayers(nod);
						return;
					}

					var l = map.getLayer(nod.id);

					if (!l) {
						return;
					}

					if (action === 'hide') {
						l.setVisibility(false);
					}
					else if (action === 'show' && nod.checked) {
						l.setVisibility(true);
					}
					//this is just for TFR ...
					else if (action === 'show' && !nod.checked && node.id === 'tfr') {
						l.setVisibility(true);
					}
				});
			}

			updateLayers(node);
		},
		getTabIdFromLayerGroup: function(group) {
			var result = "overlays-toggle";
			console.log("getTabIdFromLayerGroup ", group);

			if (group == 'ago') {
				result = 'arcgis-online'
			}
			else if (group == 'rivers' || group == 'wobs' || group == 'wrsat' || group == 'wctrop'  || group == 'wcforc' || group == 'wfires' || group == 'wmlpa' 
				  || group =='wmhpa' || group == 'wmlpa' || group=='wmlaps' || group=='wmqpe' || group == 'woso' || group=='wocl' || group == 'wocww' || group=='woco') {						
				result = 'threat-layers'
			}					
			
			//rest are 'overlays-toggle'
					
			return result;
		},
	    addTreeNode: function(lyr){
			console.log("adding tree node");//irena - which tree which tab			
	    	var layer = lyr.layer,
	    	    tabId = this.getTabIdFromLayerGroup(layer.group),
	    		//layerTree = $('#overlays-toggle').data('kendoTreeView'),
	    		layerTree = $('#' + tabId).data('kendoTreeView'),
	    		treeNodes = this.get("dataSource"),
	    		node = layerTree.dataSource.get(layer.id);

	    	//check if tree node exist already
	    	if(node){
	    		console.log(node.id,': tree node exist already');
	    		return;
	    	}

	    	//add layer to layer tree
			if(treeNodes && layer.group){
				var group = treeNodes.get(layer.group);
				//check if group exist
				if(!group){
					group = layerTree.dataSource.get(layer.group);
					if(!group){
						group = layerTree.dataSource.get('ago');
						nisis.ui.displayMessage('Could not find the layer group specified: ' + layer.group, '', 'error');
						nisis.ui.displayMessage('Your well will be added to ArcGIS Online Folder', '', 'info');
					}
				}
				//expand group node before append a new node
				groupNode = layerTree.findByUid(group.uid);
				layerTree.expand(groupNode);
				//append a new node
				group.append({
					"text": layer.name ? layer.name : layer.id.replace(/_/g, ' '),
					"id": layer.id,
					"type": "image",
					"checked": layer.visible,
					"parent": layer.group,
					"hasChildren": false
				});
			} else {
				nisis.ui.displayMessage('App could not append the new layer to the layers tree', '', 'error');
			}
	    },
	    removeTreeNode: function(nodeid,grp){

			console.log("removing tree node");


	    	var $this = this,
	    		layerTree = $('#overlays-toggle').data('kendoTreeView'),
	    		tNode = nodeid ? layerTree.dataSource.get(nodeid) : $this.get('selectedNode');

	    	if(!tNode){
	    		return;
	    	}

	    	var node = tNode,
	    		pNode = null,
	    		group = null,
	    		layer = nisis.ui.mapview.map.getLayer(node.id);

	    	if(grp){
	    		group = grp;
	    	} else {
	    		return;
	    	}

	    	pNode = layerTree.dataSource.get(group);

	    	if(pNode){
	    		pNode.items.splice(node.index, 1);
	    		$.each(pNode.items,function(i,n){
	    			//reset node indexes
	    			n.index = i;
	    		});
	    	} else {
	    		console.log('Could not identify parent of ',node);
	    	}
	    },
	    //hold the selected node
	    selectedNode: null,
	    selectedLayer: function(){
	    	//console.log("Selected Node: ",this.get("selectedNode"));
	    	var lyr, lyrType,
	    		$this = this,
	    		treeview = $('#overlays-toggle').data('kendoTreeView'),
	    		node = this.get("selectedNode");

	    	//check if there is a seleced node
	    	if(node === null){
	    		//console.log("No selected node");
	    		this.set("zoomEnabled",false);
	    		this.set("refreshEnabled",false);
	    		this.set('moveUpEnabled', false);
	    		this.set('moveDownEnabled', false);
	    		this.set('hasFeatures', false);
	    		this.set("hasAttributes",false);
	    		//this.set("attrFiltered",false); //TODO ==> change this option from tableview
	    		this.set("layerFiltered",false);
	    		this.set("hasTempGraphics",false);
	    		this.set("saveTempGraphics", false);
	    		this.set("removeTempGraphics", false);
	    	} else {
	    		//console.log("Selected Node: ", node);
	    		lyr = nisis.ui.mapview.layers[node.id];	    		
	    		//update layer opacity
	    		if(lyr){
		    		var opac = parseFloat(lyr.opacity, 10) * 100;
		    		opac = opac > 100 ? 100 : opac;
		    		this.set('layerOpacity', opac);
	    		} else {	    			
	    			nisis.ui.displayMessage('App could not identify layer','','error');
	    			return;
	    		}

	    		//this.changeLayerOpacity();
	    		//layers loading orders
	    		var fLayers = nisis.ui.mapview.map.graphicsLayerIds,
	    			flLen = fLayers.length;
	    		var dLayers = nisis.ui.mapview.map.layerIds,
	    			dlLen = dLayers.length;
	    		var layers = fLayers.concat(dLayers);
	    			lLen = layers.length;

	    		//check if layer can be moved up
	    		if($.inArray(node.id, layers) < (lLen - 1)){
	    			this.set('moveUpEnabled', false);
	    		} else {
	    			this.set('moveUpEnabled', true);
	    		}
	    		//check if layer can be moved down
	    		// || $.inArray(node.id, fLayers) === -1
	    		if($.inArray(node.id, layers) < lLen - 2){
	    			this.set('moveDownEnabled', false);
	    		} else {
	    			this.set('moveDownEnabled', true);
	    		}
	    		//enable zoom and refresh
	    		this.set("zoomEnabled", true);
	    		this.set("refreshEnabled", true);
	    		//check if layer can be open in he table
	    		if(lyr && lyr.type === 'Feature Layer'){
	    			if(lyr.group === 'nisisto'){
	    				this.set("hasAttributes", true);
	    			} else {
	    				this.set("hasAttributes", false);
	    			}
	    			this.set("hasFeatures", true);
	    		} else {
	    			this.set("hasAttributes", false);
	    			this.set("hasFeatures", false);
	    		}
	    		//check if layer has been filtered
	    		var qDef;
	    		if(lyr && lyr.getDefinitionExpression){
	    			qDef = lyr.getDefinitionExpression();
	    		}
	    		if(qDef !== undefined || !qDef === ""){
	    			this.set("layerFiltered", true);
	    		} else {
	    			this.set("layerFiltered", false);
	    		}
	    		//save and remove features/layers
	    		if(lyr && lyr.id === 'User_Graphics' && lyr.graphics.length > 0){
	    			this.set("saveTempGraphics", true);
	    			this.set("removeTempGraphics", true);
	    		} else if (lyr.group && lyr.group === 'ago' && lyr.temp){
	    			this.set("saveTempGraphics", false);
	    			this.set("removeTempGraphics", true);
	    		} else if (lyr && lyr.type === 'Label Layer') {
	    			this.set("saveTempGraphics", false);
	    			this.set("removeTempGraphics", true);
	    		} else {
	    			this.set("saveTempGraphics", false);
	    			this.set("removeTempGraphics", false);
	    		}
	    	}
	    },
	    selectLayer: function(e){
	    	if (!e) {
	    		return;
	    	}
	        var tv = e.sender,
	        	data = tv.dataItem(e.node),
	        	id = data.id;
	        //console.log("selected node data: ", data);
	        if(data.hasChildren){
	        	this.set("selectedNode", null);
	        } else {
	        	this.set("selectedNode", data);
	        }
	        //reveil selected layer options
	        this.selectedLayer();
	    },
	    changeLayerVisibility: function(id, visible){
	    	//console.log(id + ' is ' + visible);
		    var layer = nisis.ui.mapview.map.getLayer(id);
		    if(layer) {
		    	layer.setVisibility(visible);
		    } else {
		    	var msg = "App could not get layer (" + id + ") from the map.";
	    		nisis.ui.displayMessage(msg, "", "error");
		    }
	    },
	    updateTreeNode: function(id, checked){
	    	var dataSource = $("#overlays-toggle").data('kendoTreeView').dataSource,
	    		node = dataSource.get(id),
	    		layer = nisis.ui.mapview.map.getLayer(id);

	    	if ( node ) {
	    		node.set('checked', checked);
	    		//update labels if any
    			l = dataSource.get(id+"_labels");

    			if (l) {
    				l.set('checked', checked);
    			}

	    	} else {
	    		//layers maybe hidden b/c of the active app module
	    		if (!layer) {
		    		var msg = "Layer (" + id + ") has not been added to the map layers tree.";
		    		nisis.ui.displayMessage(msg, "", "error");
		    	}
	    	}
	    },
	    applyNavigate: function(e){
            //console.log(e);
        },
        handleDragStart: function(e){
        	//console.log('Tree node drag start: ', e);
        	//e.sender.select($());
        	/*var source = e.sender.dataItem(e.sourceNode);
        	var parent = source.parentNode();
        	console.log('Tree node drag start source: ', source);
        	console.log('Tree node drag start parent: ', parent);
        	if(parent && parent.container && parent.container === 'overlays-toggle'){
        		console.log("Parent nodes can not be moved: ", parent.id);
        		//e.setValid(false);
        		//e.preventDefault();
        	}*/
        },
        handleDragEnd: function(e){
        	/*console.log('Tree node drag end: ', e);
        	var dest = e.destinationNode;
        	console.log('Tree destination node: ', e.sender.dataItem(dest), e.sender.dataItem(dest).children);
        	if(e.dropPosition === 'over' && !dest.hasChildren){
        		e.preventDefault();
        	}*/
        },
        reorderLayers: function(e){
        	var $this = this;

        	/*console.log('A layer has moved: ', e);
        	console.log('Tree Data source: ', e.sender.dataSource.data());
        	console.log('Treeview: ', e.sender);
        	console.log('Selected node: ', $(e.sourceNode).find(".k-state-selected").length);
        	console.log('Treeview selection: ', e.sender.select());*/

        	var s = e.sender.select().length;
        	var sel = $(e.sourceNode).find(".k-state-selected").length;
        	var dest = e.sender.dataItem(e.destinationNode);
        	var source = e.sender.dataItem(e.sourceNode);
        	var sParent = source.parentNode();
        	var dParent = dest.parentNode();
        	var msg = "";

        	/*console.log('Drop selection: ', sel);
        	console.log('Drop source: ', source);
        	console.log('Drop destination: ', dest);
        	console.log('Drop source parent: ', sParent);
        	console.log('Drop dest parent: ', dParent);*/

        	if (s > 0) {
        		msg = 'There is a selected layer that might be duplicate after your action.';
        		e.setValid(false);
        		nisis.ui.displayMessage(msg, '', 'error');
        		//clear selection
        		e.sender.select($());
        		//reset layers options
        		$this.set('selectedNode', null);
        		$this.selectedLayer();

        		return;
        	}

        	//prevent moving layer out of their folder
        	if(sParent && dParent && !dParent.id){
        		msg = "Layer can not be moved out of their folder.";
        		nisis.ui.displayMessage(msg, '', 'error');
        		e.setValid(false);
        		return;
        	}
        	//prevent setting a layer as parent
        	if(sParent && dParent && sParent.id && !dParent.id){
        		msg = "Layer can not be moved out of their folder.";
        		nisis.ui.displayMessage(msg, '', 'error');
        		e.setValid(false);
        		return;
        	}
        	//prevent sharing layer accross folders
        	if(sParent && dParent && sParent.id && dParent.id && sParent.id !== dParent.id){
        		msg = "Layer can not be moved accross folders.";
        		nisis.ui.displayMessage(msg, '', 'error');
        		e.setValid(false);
        		return;
        	}

        	if(e.dropPosition === 'over' && source.hasChildren){
        		msg = "This folder can not become child.";
        		nisis.ui.displayMessage(msg, '', 'error');
        		e.setValid(false);
        		return;
        	}
        	//prevent dropping layer over other layers
        	if(e.dropPosition === 'over' && !dest.hasChildren){
        		msg = "Layer can not become a folder.";
        		nisis.ui.displayMessage(msg, '', 'error');
        		e.setValid(false);
        		return;
        	}

        	var index = 0;
        	var getChildNodes = function(nodes){
            	$.each(nodes,function(i,node){
            		if(node.hasChildren){
            			getChildNodes(node.children.view());
            		} else {
            			index++;
            			var layer = nisis.ui.mapview.map.getLayer(node.id);
            			if(layer){
            				nisis.ui.mapview.map.reorderLayer(layer,index);
            			}
            		}
            	});
            };
            //reorder the layers
            getChildNodes(e.sender.dataSource.view());
        },
	    applyLayersTreeChange: function(e){
	    	//console.log('Layer tree change event: ', e);
	    	var $this = this;

	    	if(!e.field || (e.field && e.field !== 'checked')) {
	    		return;
	    	}

            if(e.items && e.items.length && e.items.length > 0){
            	var node = e.items[0];
            	if(node.hasChildren){
            		//timeout ==> wait for the checkboxes to be updated
            		setTimeout(function(){
            			$this.getChildNodes(node.items);
            		},100);

            	} else {
            		//BK: 20160731 => This run only with non parent nodes
            		$this.updateLayerVisibility(node);
            	}
            } else {
            	console.log('Layers tree nodes changed: ', e);
            }
        },
        getChildNodes: function(nodes){
        	var $this = this;
        	$.each(nodes,function(i,node){
        		if(node.hasChildren){
        			$this.getChildNodes(node.items);
        		} else {
        			$this.updateLayerVisibility(node);
        		}
        	});
        },

        //NISIS-2222: KOFI - COMMENTED OUT
   //      updateUserLayers: function(){
   //      	var def = "";
   //      	//get user layers
   //      	var points = nisis.ui.mapview.map.getLayer('Point_Features');
			// var lines = nisis.ui.mapview.map.getLayer('Line_Features');
			// var polygons = nisis.ui.mapview.map.getLayer('Polygon_Features');

			// var tree = $("#overlays-toggle").data("kendoTreeView"),
   //          user = tree.dataSource.get('user').checked,
   //          groups = [],
   //          gNode = tree.dataSource.get('groups');

   //          $.each(gNode.items,function(i,g){
   //          	if(g.checked){
   //          		groups.push(parseInt(g.id));
   //          	}
   //          });
   //          var gLen = groups.length;
			// //build query
   //      	if (user){
   //      		def = "CREATED_USER = '" + nisis.user.username + "'";
   //      		if(gLen > 0){
   //      			def += " OR USER_GROUP IN (" + groups.join(',') + ")";
   //      		}
   //      	} else {
   //      		if(gLen > 0){
   //      			def += "USER_GROUP IN (" + groups.join(',') + ")";
   //      		}
   //      	}

   //      	if(def === ""){
   //      		def = "CREATED_USER = 'SYSTEM'";
   //      	}

   //      	//console.log('User layer query: ', def);
   //      	//update user layers
   //      	if(points && def !== ""){
			// 	points.setDefinitionExpression(def);
			// }
			// if(lines && def !== ""){
			// 	lines.setDefinitionExpression(def);
			// }
			// if(polygons && def !== ""){
			// 	polygons.setDefinitionExpression(def);
			// }
   //      },

        updateLayerVisibility: function(node){
        	//console.log('Updating layer visibility: ', node.id);
        	var $this = this,
        		layer = null;

        	if(!node || (node && !node.id) ){        		
        		return;
        	}

        	layer = nisis.ui.mapview.map.getLayer(node.id);



			if(layer){						
				//destroy the layer
				if (node.checked == false) {
					console.log("Removing layer & legend: ", node.id);
					Array.prototype.deleteLayer = function(layerId) {
						for (var i = 0; i < this.length; i++){
					        if (this[i].layer.id === layerId)
					            break;
	    				}				    
					    if (i >= 0) this.splice(i, 1);
					    return this;
					}; 
					nisis.ui.mapview.map.removeLayer(layer) ;
					nisis.ui.mapview.legendLayerInfos.deleteLayer(layer.id);
					//console.log("updateLayerVisibility forcing legend refresh......");
					nisis.ui.mapview.util.refreshMapLegend();
					//clear out ESRI info window
		            if (nisis.ui.mapview.featureEditor!='undefined') {
		            	var feat = nisis.ui.mapview.featureEditor._currentGraphic;
						if (feat) {
							nisis.ui.mapview.map.infoWindow.hide();
							if (feat._graphicsLayer) {
								feat._graphicsLayer.clearSelection();
							}
							nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.deactivate();
						}
		            }
				}	
				else {
					console.log("Tree Node's layer not node.checked is false: ",node);
				}	
			} else {
				console.log("Tree Node's layer not loaded: ",node);
				//BK: 20160731 ==> not sure what this is doing??
				var p = node.parent();

				if (p && p.id && p.id == 'user') {
					$this.updateUserLayers();
				}

				//BK: 20160731 ==> See if the layer has been held for on demand loading
				layer = nisis.ui.mapview.layers[node.id];

				if (layer) {
					//if this was a label layer - and its graphics was not loaded - load it so labels will be seen
					if (layer.type == "Label Layer" && layer.graphics.length==0 && node.checked==true) {
						console.log("Label Layer: ", layer);
						var labeledLayerName = node.id;
						var labeledLayerName = labeledLayerName.replace("_labels", ""); 
						var labeledLayer =  nisis.ui.mapview.layers[labeledLayerName];
						nisis.ui.mapview.map.addLayer(labeledLayer);
						labeledLayer.setVisibility(true);
					}

					console.log("Change held layer visibility: ", node.checked);
					layer.setVisibility(node.checked);
					console.log("Loading held layer: ", node.id);
					//NISIS-3049 kofi honu
					renderers.applyFeatureLayersRenderer(layer);
					nisis.ui.mapview.map.addLayer(layer); 
				}				
			}
        },
        layerOpacity: 100,
        changeLayerOpacity: function(e){
        	//console.log('Opacity change events: ', e);
        	var node = this.get('selectedNode'),
        		opac = this.get('layerOpacity'),
        		layer = nisis.ui.mapview.map.getLayer(node.id);
        	//arcgis opacity is in decimal digrees
        	opac /= 100;
        	opac = opac.toFixed(3);
        	//apply slider value to layer
        	layer.setOpacity(opac);

        	console.log('Changing layer opacity: ' + node.text + ' (' + (opac * 100) + '%' + ')');
        },
        zoomEnabled: false,
        zoomToLayerExtent: function(e){
        	var node = this.get('selectedNode'),
        	layer =  nisis.ui.mapview.layers[node.id];
        	var extent = null;
        	if(layer.fullExtent){
        		extent = layer.fullExtent;
        		if(!extent.xmin){
        			extent = nisis.ui.mapview.util.getGraphicsExtent(layer.graphics);
        		}
        	} else {
        		extent = nisis.ui.mapview.util.getGraphicsExtent(layer.graphics);
        	}
        	//zoom to layer extent
        	nisis.ui.mapview.map.setExtent(extent, true);
        },
        refreshEnabled: false,
        refreshLayer: function(e){
        	var node = this.get('selectedNode'),        	
        	layer =  nisis.ui.mapview.layers[node.id];
        	if(layer.refresh){
	        	layer.refresh();
	        } else {
	        	nisis.ui.displayMessage('Refresh is not enabled on this layer', '', 'error');
	        }
        },
        moveUpEnabled: false,
        moveLayerUp: function(e){
        	//get selected layer info
        	var node = this.get("selectedNode"),
        		map = nisis.ui.mapview.map,
        		lyr = map.getLayer(node.id),
        		order = 0;
        	//reorder selected layer
        	if($.inArray(node.id, map.graphicsLayerIds) !== -1){
        		order = $.inArray(node.id, map.graphicsLayerIds);
        		map.reorderLayer(lyr, order + 1);
        		this.selectedLayer();
        		console.log('Moving layer up: ', node.text);
        	}
        },
        moveDownEnabled: false,
        moveLayerDown: function(e){
        	//get selected layer info
        	var node = this.get("selectedNode"),
        		map = nisis.ui.mapview.map,
        		lyr = map.getLayer(node.id),
        		order = 0;
        	//reorder selected layer
        	if($.inArray(node.id, map.graphicsLayerIds) !== -1){
        		order = $.inArray(node.id, map.graphicsLayerIds);
        		map.reorderLayer(lyr, order - 1);
        		this.selectedLayer();
        		console.log('Moving layer down: ', node.text);
        	}
        },
        //label feature layers
        hasFeatures: false,
        labelLayer: function(e){
        	var $this = this,
        		node = this.get("selectedNode"),
        		map = nisis.ui.mapview.map,        		
        		lyr = nisis.ui.mapview.layers[node.id],        		
        		fields = lyr.fields,
        		visibleFields = lyr._outFields;
        	//check layers
        	if(!lyr){
        		nisis.ui.displayMessage('App could not identify the selected layer.', '', 'info');
        		return;
        	}
        	//check fields
        	if(!fields){
        		nisis.ui.mapview.util.getLayerFields(lyr)
        		.then(function(fields){
        			fields = fields;
        			$this.displayLabelOptions(lyr,fields);
        		},function(errorMsg){
        			nisis.ui.displayMessage(errorMsg,'','error');
        		});

        	} else {
        		var vf = [];
        		if($.inArray('*', visibleFields) != -1){
        			$this.displayLabelOptions(lyr,fields);
        			return;
        		}
        		//show only preselected fields
        		$.each(fields, function(i,f){
        			if($.inArray(f.name,visibleFields) != -1){
        				vf.push(f);
        			}
        		});

        		$this.displayLabelOptions(lyr,vf);
        	}

        	console.log('Labeling layer: ' + node.text);
        },
        displayLabelOptions: function(layer, fields){
        	var $this = this;
        	//console.log('Building label options ...', layer.id);
        	if(!layers || !fields){
        		return;
        	}

        	var id = "_user_labels",
        		labelId = layer.id + id,
        		group = layer.group ? layer.group : 'ago',
        		labelName = (layer.name ? layer.name : layer.id.replace(/_/g, ' ')) + " Labels",
        		labelLayer = nisis.ui.mapview.map.getLayer(labelId),
        		labelDiv = $("<div id='" + layer.id + "'></div>");

        	var fieldsOptText = $("<span class='l-headers'>Select label field</span>"),
        		fieldsOpt = $("<select class='w300'></select>"),

        		fontFamText = $("<span class='l-headers'>Select label font</span>"),
        		fontFam = $("<input class='w300'/>");

        	var fontOptsText = $("<span class='l-headers'>Set font options</span>"),
        		fontOpts = $("<div class='marg clear of-h'></div>"),
        		fontSize = $("<input class='w75'/>"),
        		fontColor = $("<input />"),
        		bold = $("<span class='ft-format fw-b margLeft k-button'>B</span>"),
        		italic = $("<span class='ft-format fs-i margLeft k-button'>I</span>"),
        		under = $("<span class='ft-format td-u margLeft k-button'>U</span>");
        	fontOpts.append(fontColor,fontSize,bold,italic,under);

        	var buttons = $("<div class='marg clear'></div>"),
        		addLabel = $("<span class='k-button'>Apply</span>"),
        		remove = $("<span class='margLeft k-button'>Remove</span>");
        		cancel = $("<span class='margLeft k-button'>Cancel</span>");
        	buttons.append(addLabel,remove,cancel);

        	$('#app').append(labelDiv);
        	labelDiv.append(fieldsOptText,fieldsOpt,fontFamText,fontFam,fontOptsText,fontOpts,buttons);

        	$('div#'+layer.id+' span.l-headers').wrap("<div class='marg clear'></div>");
        	fieldsOpt.wrap("<div class='marg clear'></div>");
        	fontFam.wrap("<div class='marg clear'></div>");
        	fontSize.wrap("<div class='margLeft left'></div>");
        	fontColor.wrap("<div class='left'></div>");

        	//field options
        	nisis.ui.sortArrayOfObjects(fields, 'alias');
        	fieldsOpt.kendoDropDownList({
        		dataTextField: "alias",
                dataValueField: "name",
                dataSource: fields
        	});
        	//font familly
        	fontFam.kendoDropDownList({
        		dataTextField: "field",
                dataValueField: "field",
                dataSource: $.map(fonts['sans-sherif'],function(f,i){
                	return {field: f};
                })
        	});
        	//
        	fontSize.kendoNumericTextBox({
        		value: 12
        	});
        	//
        	fontColor.kendoColorPicker({
        		//palette: "websafe",
	            value: "#000000",
	            preview: false,
	            buttons: true
	        });

	       	var labelOptions = labelDiv
	       	.kendoWindow({
	       		title: "Label options",
                width: "auto",
                minWidth: 200,
                height: "auto",
                visible: false,
                actions: ["Minimize","Close"]
                ,close: function(e){
                	this.destroy();
                }
            }).data('kendoWindow');

	       	//font formats
            $('.ft-format').click(function(evt){
        		//console.log(evt);
        		var active = $(evt.currentTarget).hasClass('k-state-active');
        		if(active){
        			$(evt.currentTarget).removeClass('k-state-active');
        		} else {
        			$(evt.currentTarget).addClass('k-state-active');
        		}
        	});

        	if(labelLayer){
        		remove.show();
        	} else {
        		remove.hide();
        	}

	       	addLabel.click(function(evt){
	       		labelDiv.spin(true);
	       		//labelOptions.close().destroy();
	       		var label = new TextSymbol();
	       		var color = fontColor.data("kendoColorPicker").value();
	       		//need to use Dojo Color object here
	       		label.setColor(new Color(color));
	       		var size = fontSize.data('kendoNumericTextBox').value() + "px";
	       		label.font.setSize(size);
	       		var font = fontFam.data('kendoDropDownList').value();
	       		label.font.setFamily(font);
	       		//label.setOffset(5,5);
	       		//font formats
	       		var b = bold.hasClass('k-state-active'),
	       			i = italic.hasClass('k-state-active'),
	       			u = under.hasClass('k-state-active');
	       		//bold
	       		if(b){
	       			label.font.setWeight('bold');
	       		} else {
	       			label.font.setWeight('normal');
	       		}
	       		//italic
	       		if(i){
	       			label.font.setStyle('italic');
	       		} else {
	       			label.font.setStyle('normal');
	       		}
	       		//docoration
	       		if(u){
	       			label.font.setDecoration('underline');
	       		} else {
	       			label.font.setDecoration('none');
	       		}
	       		//console.log(label);
	       		var labelRenderer = new SimpleRenderer(label);
	       		//create label layer
	       		var previousLabel = nisis.ui.mapview.map.getLayer(labelId);
	       		//check for default labels
	       		if(!previousLabel){
	       			//id for default labels
	       			var id = layer.id + '_default_labels';
	       			previousLabel = nisis.ui.mapview.map.getLayer(id);
	       		}
	       		//remove previous labels
	       		if(previousLabel){
	       			nisis.ui.mapview.map.removeLayer(previousLabel);
	       		}
	       		//create a label layer
	       		var labelLayer = new LabelLayer({
	       			id: labelId
	       		});
	       		//set label options
	       		labelLayer.name = labelName;
	       		labelLayer.group = layer.group;
	       		labelLayer.type = "Label Layer";

	       		var field = fieldsOpt.data('kendoDropDownList').value();
	       		//this is weird. Why is the field replacement not allowing the $ sign? "${field}" ==> "{field}"
	       		labelLayer.addFeatureLayer(layer, labelRenderer, "{" + field + "}");
	       		try {
		       		nisis.ui.mapview.map.addLayer(labelLayer);
		       		labelDiv.spin(false);
		       		remove.show();
		       	} catch (err){
		       		console.log(err);
		       		nisis.ui.displayMessage(err.message,'','error');
		       		labelDiv.spin(false);
		       	}
	       	});

			remove.click(function(evt){
				var labelLayer = nisis.ui.mapview.map.getLayer(labelId);
				nisis.ui.mapview.map.removeLayer(labelLayer);
				$this.removeTreeNode(labelId,labelLayer.group);
	       		labelOptions.close().destroy();
	       	});

	       	cancel.click(function(evt){
	       		labelOptions.close().destroy();
	       	});

            labelOptions.center().open();
        },
        hasAttributes: false,
        openLayerAttributes: function(e){
        	//open the tableview and change the layer selection
        	$("#tableview-div").data('kendoWindow').open();
        	//window content need to be refreshed only once
            if($('#tableview-content').data('reset') === true){
                $('#tableview-content').data('kendoSplitter').toggle('#tbl-panel-left');
                $('#tableview-content').data('reset',false)
            }
            //window will the centered on once
            if($("#tableview-div").data('center') === true){
                $("#tableview-div").data('kendoWindow').center();
                $("#tableview-div").data('center',false);
            }
            //set the selected layer
            $('#tbl-lyrs').data('kendoDropDownList').value(this.get('selectedNode').id);
            $('#tbl-lyrs').data('kendoDropDownList').trigger('change');
        },
        attrIsOpened: function(){
        	var tblLayer = $('#tbl-lyrs').data('kendoDropDownList').value(),
        	selNode = this.get('selectedNode');
        	if(selNode){
	        	if(tblLayer !== "" && tblLayer === selNode.id){
	        		return true;
	        	} else {
	        		return false;
	        	}
        	} else {
        		return false;
        	}
        },
        applyAttrFilter: function(e){
   			//apply filter from tableview
        	$('#tbl-update-map').trigger('click');
        },
        clearAttrFilter: function(e){
        	//clear filter from tableview
        	$('#tbl-clear-filter').trigger('click');
        },
        hasTempGraphics: false,
        saveTempGraphics: false,
        removeTempGraphics: false,
        removeFeatures: function(e){
        	var $this = this;
        	var options = {
        		title: 'Removing Features / Layers',
        		message: 'Are you sure you want to remove all the features / layer?',
        		buttons: ['Yes', 'No']
        	};
        	var id = this.get('selectedNode').id,
        		group = null;
        	//get the selected layer
        	var layer = nisis.ui.mapview.map.getLayer(id);
        	group = layer.group;

        	if(!layer){
        		nisis.ui.displayMessage('App could not identify the layer you are tryin to remove.','','error');
        		return;
        	}

        	nisis.ui.displayConfirmation(options)
        	.then(function(response){
        		if(response === 'Yes'){
        			if((layer.type && layer.type === 'Feature Layer') || layer.id.toLowerCase() === 'user_graphics'){
	        			layer.clear();
	        			$this.selectedLayer();
	        		} else {
	        			nisis.ui.mapview.map.removeLayer(layer);
	        			$this.removeTreeNode(id, group);
	        		}
        		}
        	},function(e){
        		console.log(e);
        		nisis.ui.displayMessage('App could not display confirmation dialog.', '', 'error');
        	});
        },
        saveFeatures: function(e){
        	var $this = this;
        	var options = {
        		title: 'Saving Features',
        		message: 'How would you like these features saved?',
        		buttons: ['Single', 'Merge', 'Intersect']
        	};

        	//get the selected layer
        	var layer = nisis.ui.mapview.map.getLayer(this.get('selectedNode').id);

        	nisis.ui.displayConfirmation(options)
        	.then(function(response){

        		var features = {
        			point: [],
        			polyline: [],
        			polygon: []
        		}

        		$.each(layer.graphics, function(i,g){
        			//console.log(g);
        			features[g.geometry.type].push(g.geometry);
        		});

        		var msg = 'There is no polygons in your layer';

        		switch (response) {
        			case options.buttons[0]:
        				$.each(features,function(featType, geoms){
        					//console.log(featType);
        					$.each(geoms,function(i,g){
        						console.log('Geometry Type: ', g.type);
        						$this.addUserFeature(g);
        					});
        				});

        				break;

        			case options.buttons[1]:
        				if(features.polygon.length > 0){
        					$this.mergeFeatures(features.polygon)
        					.then(function(id){
        						layer.clear();
        						nisis.ui.displayMessage('Feature #' + id + ' has been saved.', '', 'info');
        					},function(msg){
        						nisis.ui.displayMessage(msg, '', 'error');
        					});
        				} else {
        					nisis.ui.displayMessage(msg, '', 'warn');
        				}

        				break;

        			case options.buttons[2]:
        				if(features.polygon.length > 0){
        					$.each(features.polygon,function(i,geom){
        						nisis.ui.displayMessage('Can not interset', '', 'warn');
        					});
        				} else {
        					nisis.ui.displayMessage(msg, '', 'warn');
        				}

        				break;

        			default:
        				nisis.ui.displayMessage('User action: ' + response, '', 'info');
        				break;
        		}
        	});
        },
        mergeFeatures: function(geoms){
        	var $this = this;
        	var def = new $.Deferred();
        	nisis.ui.mapview.geometryService
			.union(geoms,
			function(result){
				console.log(result);
				$this.addUserFeature(result)
				.then(function(id){
					def.resolve(id);
				},function(msg){
					def.reject(msg);
				});
			},
			function(err){
				console.log(err);
			});

			return def.promise();
        },
        addUserFeature: function(geom){
        	var def = new $.Deferred();

        	var userLayers = attributes.user(),

        	layer = nisis.ui.mapview.map.getLayer(userLayers[geom.type].id),

        	attr = userLayers[geom.type].fields;

        	console.log(userLayers,attr);

        	var graphic = new Graphic(geom,null,attr);

        	layer.applyEdits([graphic],null,null)
        	.then(
        		function(resp){
        			console.log(resp);
        			if(resp[0].success){
        				def.resolve(resp[0].objectId);
        			}

        		},function(err){
        			console.log(err);
        			def.reject(err.message);
        		}
        	);

        	return def.promise();
        },
        //arcgis online staff
        agoKeyword: "",
        agoLogin: true,
        agoLogout: false,
        agoFullName: "",
        agoUsername: "NisisDev",
        agoPassword: "Nisis123",
        agoAuth: "Sign In",
        agoUserMsg: "No Services ...",
        agoSearchMsg: "No Services ...",
        agoUserContent: "",
        agoSearchContent: "",
        agoSearchNextQuery: null,
        agoItems: {},
        agoGetCredentials: function(e){
        	//console.log(e);
        	var $this = this,
        		ago = nisis.ui.mapview.agsonline;

        	ago.signIn()
        	.then(function(res){
        		//console.log(res);
        		nisis.ui.mapview.agoContent = res;
        		$this.set('agoLogin',false);
        		$this.set('agoLogout',true);
        		$this.set('agoFullName', 'Hello, ' + res.fullName);
        		$this.getAgoUserContent(res);
        	});
        },
        agoSignOut: function(e){
        	var $this = this,
        		loggedin = !this.get('agoLogin'),
        		ago = nisis.ui.mapview.agsonline;

    		ago.signOut();
   			$this.set('agoLogin',true);
   			$this.set('agoLogout',false);
        	$this.set('agoFullName', '');
        	$this.set('agoUserMsg', 'No Services ...');
        	$this.set('agoUserContent', '');
        },
        getAgoUserContent: function(res){
        	var $this = this,
        		ago = nisis.ui.mapview.agsonline;
        		$this.set('agoUserMsg', 'Retreiving content ...');

        	res.getContent()
        	.then(function(content){
        		//console.log('User AGO Content: ', content);
        		$this.displayAgoItems(content.items, 'user-content');
        	},function(error){
        		console.log(err);
        		nisis.ui.displayMessage(err.message, '', 'error');
        	});
        },
        displayAgoItems: function(items, type){
        	var $this = this;
        	if(!items || (items && !items.length)){
        		return;
        	}
        	//console.log('AGO ' + type + ' items: ', items);

    		if(items.length > 0){
    			var wrap = "",
    				len = items.length,
    				l = 0;

    			$.each(items, function(i,item){
	        		//console.log(i,item.type);
	        		//console.log(i,item);
	        		$this.agoItems[item.id] = item;

        			l++;

        			var title = "<hr>";
        			title += "<div class='margTop clear'>";
        			if(item.thumbnailUrl){
	        			title += "<img alt='AGO Layer' class='agoImg' width='30' height='30' align='middle' src='";
	        			title += item.thumbnailUrl + "'/> ";
	        		}
    				title += "<span class='ft-b'>" + l + ": " + item.title + "</span>";
    				title += "</div>";

        			var cont = "<div class='left'>";
        			cont += "<span><b>Item Type: </b>" + item.type + "</br>";
        			cont += item.destination ?  "<span><b>Description: </b>" + item.description + "</span><br>" : "";
        			cont += "</div>";
        				//console.log(cont);
        			var footer = "<div class='clear'>";
    				footer += "<span><b>Owner</b>: " + item.owner + "</span><br>";
    				footer += "<span><b>Created on: </b>" + item.created + "</span>";
    				footer += "</div>";

        			var actions = "<div class='margBottom clear'><span>";
    				actions += "<a href='#' class='agoServices' data-id='" + item.id + "' ";
    				actions += "data-title='" + item.title + "' data-type='" + item.type + "' ";
    				actions += "data-url='" + item.url + "' data-bind='click:addAgoServiceToMap'><b>Add</b></a> ";
    				actions += "<a class='agoLink' href='";

    				if(item.type === 'Web Mapping Application'){
    					actions += "http://www.arcgis.com/apps/OnePane/basicviewer/index.html?appid=";
    					actions += item.id + "'";
    				} else if (item.type === 'Web Map'){
        				actions += "http://www.arcgis.com/home/webmap/viewer.html?webmap=";
        				actions += item.id + "'";
        			} else {
        				actions += "http://www.arcgis.com/home/webmap/viewer.html?useExisting=1&layers=";
        				actions += item.id + "'";
        			}

        			actions += " target='_blank'><b>View</b></a>";
    				actions += "</span></div>";

        			wrap += title + cont + footer + actions;

		        	if(i === len -1){
		        		wrap += "<div class='clear'>" + wrap + "</div>";

		        		if(type == 'search'){
			        		$this.set('agoSearchMsg', 'Result: ' + l + ' items');
			        		$this.set('agoSearchContent', wrap);
			        	} else {
			        		$this.set('agoUserMsg', l + 'Result: ' + l + ' items');
			        		$this.set('agoUserContent', wrap);
			        	}
		        		//bind ags online service
				    	$('.agoServices').click(function(evt){
				    		$this.addAgoServiceToMap(evt);
				    	});
		        	}
	        	});

    		} else {
    			if(type == 'search'){
        			$this.set('agoSearchMsg', 'Result: 0 items');
        			$this.set('agoSearchContent', '');
        		} else {
        			$this.set('agoUserMsg', 'Result: 0 item(s)');
        			$this.set('agoUserContent', '');
        		}
    		}
        },
        searchAgoContent: function(e){
        	//console.log(e);
        	var $this = this,
        		ago = nisis.ui.mapview.agsonline,
        		key = this.get('agoKeyword');
        		//key += " AND (type = 'Map Service')";
        	//console.log('AGO Search keyword: ', key);
        	if(e.which === 13 && key.length > 2){
        		//console.log(key);
        		$this.set('agoSearchMsg', 'Processing ...');
        		ago.queryItems({
        			'q': key,
        			'f': 'json',
        			'start': 0,
        			'num': 20,
        			'sortField': 'numviews',
        			'sortOrder': 'desc',
        			'bbox': '-135.6072,4.2288,-54.8357,61.4454'
        		}).then(function(res){
	        		//console.log('AGO Search result: ', res);

	        		$this.displayAgoItems(res.results, 'search');
	        		$this.set('agoSearchNextQuery', res.nextQueryParams);
	        	},function(err){
	        		console.log('AGO Search Error: ', err);
	        		$this.set('agoSearchMsg', err.message);
	        		nisis.ui.displayMessage(err.message, '', 'error');
	        	});
        	}
        },
        addAgoServiceToMap: function(evt){
        	//console.log(evt);
        	var $this = this,
        		data = evt.currentTarget.dataset,
        		item = $this.get('agoItems')[data.id];
        	//console.log(data.id, item);

        	var layer = null;

        	if(item.url){
        		if (item.type === "Web Map") {
	        		console.log('App is now checking ItemDataUrl');
					$this.extractLayers(item.itemDataUrl, item.type);
	        	} else if (item.type === "Web Mapping Application") {
	        		console.log('App is now checking ItemDataUrl');
					$this.extractWebMap(item.itemDataUrl, item.type);
	        	} else {
		        	$this.isValidUrl(item.url)
		        	.then(function(valid){
	        			if(!valid){
	        				var msg = 'Service can not be access at the moment.';
	        				console.log(msg);
	        				nisis.ui.displayMessage(msg,'','error');
	        				return;
	        			}
	        			console.log('AGO Data Type: ', data.type);
	        			//check type of item
			        	switch (data.type) {
			        		case "Web Map":
			        			console.log('App is now checking ItemDataUrl');
	        					$this.extractLayers(item.itemDataUrl, item.type);
			        			break;

			        		case "Web Mapping Application":
			        			console.log('App is now checking ItemDataUrl');
	        					$this.extractWebMap(item.itemDataUrl, item.type);
			        			break;

			        		case "Map Service":
			        			layer = new ArcGISDynamicMapServiceLayer(data.url, {
				        			id: data.id,
				        			name: data.title,
				        			visible: true
				        		});
			        			break;

			        		case "Feature Service":
			        			var url = data.url;
			        			var urlParts = url.split('/');
			        			console.log(urlParts[-1]);
			        			if($.isNumeric(urlParts[-1])){
			        				layer = new FeatureLayer(service, {
					        			id: data.id,
					        			name: name,
					        			outFields: ['*'],
					        			visible: true
					        		});
					        		layer.type = 'Feature Layer';
			        			} else {
				        			$.ajax({
										url: url,
										data: {'f':'pjson'},
										success: function(data, status, xhr){
											console.log(arguments);
											var idx = 0, l = 0;
											if(status === 'success' && data.layers){
												l = data.layers.length;
												$.each(data.layers,function(i, lyr){
													var service = url + '/' + lyr.id;
													var name = lyr.name ? lyr.name.replace(/_/g, ' ') : lyr.id;

													var layer = new FeatureLayer(service, {
									        			id: data.id,
									        			name: name,
									        			outFields: ['*'],
									        			visible: true
									        		});
									        		layer.type = 'Feature Layer';
									        		//add to arcgis online layers group
									        		layer.group = 'ago';
									        		//flag layer for removal
									        		layer.temp = true;
									        		nisis.ui.mapview.map.addLayer(layer);
												});
											} else {
												if (data.error && data.error.message){
													nisis.ui.displayMessage(data.error.message, '', 'error');
												} else {
													nisis.ui.displayMessage("App could not retreive layers information", '', 'error');
												}
											}
										},
										error: function(e){
											console.log(arguments);
										}
										,dataType: 'json'
									});
				        		}
			        			break;

			        		case "Image Service":
			        			layer = new ArcGISImageServiceLayer(data.url, {
				        			id: data.id,
				        			name: data.title,
				        			visible: true
				        		});
			        			break;

			        		case "Shapefile":
			        			console.log('AGO Item: ', data.type);
			        			break;

			        		case "Document Link":
			        			console.log('AGO Item: ', data.type);
			        			break;

			        		default:
			        			console.log('AGO Item: ', data.type);
			        			break;
			        	}

						if(layer){
							//layer group
			        		layer.group = 'ago';
			        		//flag layer for removal
							layer.temp = true;
			        		//add layer to the map
			        		nisis.ui.mapview.map.addLayer(layer);
			        	} else {
			        		console.log('Unknown layer type: ', layer);
			        	}

		        	},function(error){
		        		var msg = 'Service can not be access at the moment.';
	        			console.log(msg);
	        			nisis.ui.displayMessage(msg,'','error');
	        		});
				}
        	} else {
        		console.log('AGO Item does not have a valid url');
        		console.log('App is now checking ItemDataUrl');
        		$this.extractLayers(item.itemDataUrl, item.type);
        	}
        },
        extractWebMap: function(itemDataUrl, itemType){
        	console.log("Extracting 'Web Map' from 'Web Mapping Application': ", itemDataUrl);
        	if(!itemDataUrl){
        		return;
        	}
        	var $this = this;
        	$.ajax({
        		url: itemDataUrl,
        		dataType: 'json',
        		success: function(data, status, xhr){
        			console.log(arguments);
        			if(status == 'success' && data.values && data.values.webmap){
        				var id = data.values.webmap,
        					itemType = "Web Map",
        					itemDataUrl = "http://www.arcgis.com/sharing/rest/content/items/";
        					itemDataUrl += id;
        					itemDataUrl += "/data";

        				$this.extractLayers(itemDataUrl, itemType);
        			} else {
        				console.log('Error extracting Web Map from Web Mapping Application: ', arguments);
        				nisis.ui.displayMessage('App could not extract Web Map from ' + itemType, '', 'error');
        			}
        		},
        		error: function(error){
        			console.log('Error extracting Web Map from Web Mapping Application: ', arguments);
        			nisis.ui.displayMessage('App could not extract Web Map from ' + itemType, '', 'error');
        		}
        	});
        },
        extractLayers: function(itemDataUrl, itemType){
        	var $this = this;
        	console.log('itemDataUrl: ', itemDataUrl);
        	if(!itemDataUrl){
        		return;
        	}

        	$.ajax({
        		url: itemDataUrl,
        		dataType: 'json',
        		success: function(data, status, xhr){
        			//console.log(arguments);
        			var layers = null, len;
        			if(data.operationalLayers){
        				len = data.operationalLayers.length;
        				if(len > 0){
        					layers = data.operationalLayers;
        					$.each(layers,function(i,lyr){
        						if(lyr.url){
        							$this.processAgoLayer(lyr);
        						} else {
        							$this.processAgoFeatures(lyr);
        						}
        					});
        				} else {
        					console.log('There is not layers in this Web App: ', arguments);
        					nisis.ui.displayMessage('There is not layers in this Web App', '', 'error');
        				}
        			}
        		},
        		error: function(error){
        			console.log('Error extracting layers from Web App: ', arguments);
        			nisis.ui.displayMessage('App could not extract layer from ' + itemType, '', 'error');
        		}
        	});
        },
        processAgoLayer: function(lyr){
        	//console.log('Processing AGO Layer: ', lyr);
        	var layer = null;
        	if(lyr.url && lyr.url.indexOf('/MapServer') !== -1){
        		if (lyr.visibleLayers && lyr.visibleLayers.length === 1) {
        			var url = lyr.url + "/" + lyr.visibleLayers[0];
        			layer = new FeatureLayer(url, {
	        			id: lyr.id,
	        			name: lyr.title,
	        			outFields: ['*'],
	        			visible: true
	        		});
        			layer.type = 'Feature Layer';
        		} else {
	        		layer = new ArcGISDynamicMapServiceLayer(lyr.url, {
	        			id: lyr.id,
	        			name: lyr.title,
	        			outFields: ['*'],
	        			visible: true
	        		});
	        	}
        	} else if (lyr.url && lyr.url.indexOf('/FeatureServer/') !== -1){
        		layer = new FeatureLayer(lyr.url, {
        			id: lyr.id,
        			name: lyr.title,
        			outFields: ['*'],
        			visible: true
        		});
        		layer.type = 'Feature Layer';
        	} else if (lyr.url && lyr.url.indexOf('/ImageServer') !== -1) {
        		layer = new ArcGISImageServiceLayer(lyr.url, {
        			id: lyr.id,
        			name: lyr.title,
        			visible: true
        		});
        	} else {
        		console.log('Unknown layer type: ', lyr.type, lyr);
        	}

        	if(layer){
        		layer.group = 'ago';
        		nisis.ui.mapview.map.addLayer(layer);
        	}
        },
        processAgoFeatures: function(lyr){
        	//console.log('Processing AGO Features: ', lyr);
        	var layers = null,
        		len = 0;

        	if(!lyr){
        		return;
        	}

        	if(lyr.featureCollection){
        		layers = lyr.featureCollection.layers;
        		len = layers.length;
        		if(layers.length > 0){
        			$.each(layers,function(i,featLayer){
        				var layer = new FeatureLayer(featLayer, {
		        			id: lyr.id,
		        			title: lyr.title,
		        			mode: FeatureLayer.MODE_SNAPSHOT,
		        			visible: true
		        		});
        				layer.group = 'ago';
        				nisis.ui.mapview.map.addLayer(layer);
        			});
        		}
        	}
        },
        isValidUrl: function(url){
        	var def = new $.Deferred();
        	$.ajax({
        		url: url,
				success: function(data, status, xhr){
					//console.log(arguments);
					var idx = 0, l = 0;
					if(status === 'success'){
						def.resolve(true);
					} else {
						def.resolve(false);
					}
				},
				error: function(e){
					//console.log(arguments);
					def.reject(false);
				}
				//,dataType: 'json'
        	});

        	return def.promise();
        }
	});
	//public objects
	return layers;
});
