/**
 * app configurations
 */

define(["app/appUtils","app/dashboard","app/windows/tableview","app/historyNav","app/windows/shapesManager"]
    ,function(appUtils,dashboard,tableview,historyNav,shapesMgr){

    var self = kendo.observable({
        self: this,
        app: null,
        tfrMode: false,
        shapeLayerQuery: "",
        header: {
            logo: {
                id: 'app-faa-image',
                image: 'FAA-logo.png',
                alt: 'APP Logo'
            },
            title: {
                id: 'title',
                text: 'NISIS'
            },
            menubar: [
                {
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'basemaps',
                    title: 'Basemaps',
                    target: 'basemaps-div',
                    icon: 'images/menu/basemaps.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'layers',
                    title: 'Map Layers',
                    target: 'layers-div',
                    icon: 'images/menu/map_layers.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },
                {
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'legend',
                    title: 'Map Legend',
                    target: 'legend-div',
                    icon: 'images/menu/map_legend.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },
                {
                    add: true,
                    nisis: true,
                    tfr: false,
                    id: 'features',
                    title: 'Feature Management',
                    target: 'features-div',
                    icon: 'images/menu/features.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },
                {
                    add: true,
                    nisis: true,
                    tfr: false,
                    id: 'drawing',
                    title: 'Shape Tools',
                    target: 'drawing-div',
                    icon: 'images/menu/objects.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: false,
                    nisis: true,
                    tfr: false,
                    id: 'shape-mgr',
                    title: 'Shape Management Tool',
                    target: 'shapes-manager-div',
                    icon: 'images/menu/shapesMgmt.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'measure',
                    title: 'Measurements',
                    target: 'measure-div',
                    icon: 'images/menu/tape_measure.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: false,
                    nisis: true,
                    tfr: false,
                    id: 'filter',
                    title: 'Filter Tools',
                    target: 'filter-div',
                    icon: 'images/menu/funnel.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'directions',
                    title: 'Locations &amp; Directions',
                    target: 'directions-div',
                    icon: 'images/menu/map_route.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },
                {
                    add: true,
                    nisis: true,
                    tfr: false,
                    id: 'view-options',
                    title: 'View Options',
                    target: 'view-options-div',
                    icon: 'images/menu/eye_view.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },
                {
                    add: true,
                    nisis: true,
                    tfr: false,
                    id: 'tableview',
                    title: 'Table View',
                    target: 'tableview-div',
                    icon: 'images/menu/table_selection_cell.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: false,
                    id: 'dashboard',
                    title: 'Dashboard',
                    target: 'dashboard-div',
                    icon: 'images/menu/chart_bar.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: false,
                    nisis: true,
                    tfr: false,
                    id: 'reports',
                    title: 'Reports',
                    target: 'reports-div',
                    icon: 'images/menu/document_chart.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: false,
                    id: 'alerts',
                    title: 'Alerts',
                    target: 'alerts-div',
                    icon: 'images/menu/alerts.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add:true, //TO-DO This needs to check if the user is an admin. At this point in the code, the nisis.user.isAdmin variable is not set
                    nisis: true,
                    tfr: false,
                    id: 'history',
                    title: 'History',
                    target: 'history-div',
                    icon: 'images/menu/history.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: false,
                    nisis: true,
                    tfr: true,
                    id: 'print',
                    title: 'Print Map',
                    target: 'print-div',
                    icon: 'images/menu/printer.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'help',
                    title: 'Help',
                    target: 'help-div',
                    icon: 'images/menu/question.png',
                    events: {
                        click: 'openHelpPage',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                },{
                    add: true,
                    nisis: true,
                    tfr: true,
                    id: 'tfr',
                    title: 'TFR Builder',
                    target: 'tfr-div',
                    icon: 'images/menu/tfr_builder.png',
                    events: {
                        click: 'toggleWindow',
                        mouseover: 'showTooltip',
                        mouseout: 'hideTooltip'
                    }
                }
            ],
            usercontrols: [
                {
                    id: 'user-name',
                    title: '',
                    target: '',
                    icon: '',
                    events: {
                        click: ''
                    }
                },{
                    id: 'search-btn',
                    title: 'Search',
                    target: '',
                    icon: 'fa fa-search',
                    events: {
                        click: 'toggleSearchBox'
                    }
                },{
                    id: 'full-screen',
                    title: 'Full Screen',
                    target: '',
                    icon: 'fa fa-arrows-alt',
                    events: {
                        click: 'toggleFullScreen'
                    }
                }, {
                    id: 'collapse-header',
                    title: 'Collapse Header',
                    target: '',
                    icon: 'fa fa-arrow-circle-left',
                    events: {
                        click: 'clpseMenu'
                    }
                },  {
                    id: 'logout',
                    title: 'Sign Out',
                    target: '',
                    icon: 'fa fa-power-off',
                    events: {
                        click: 'userLogOut'
                    }
                }
            ]
        },
        mapHeader: {
            basemaps: {id:'basemaps', class:'menu-item', title:'Basemaps', top:'200px', left:'100px'},
            layers: {id:'search-box'}
        },
        mapContainer: [
            {id: 'map', class:'views ui-widget-content'}
        ],
        footer: [
            {id:'numScale', class:'footer-section'},
            {id:'mouseLatLon', class:'footer-section'}
        ],
        showSearch: false,
        toggleSearchBox: function(e){
            var visible = this.get('showSearch');

            if(visible){
                this.set('showSearch',false);
            } else {
                this.set('showSearch',true);
                $('#search-box').focus();
            }
        },
        expandSearch: function(e){
            $('#search').animate({
                width: 300
            },1000);
        },
        restoreSearch: function(e){
            $('#search').animate({
                width: 150
            },1000);
        },
        toggleMapHeader: function(e){
            //console.log(e);
            $('#header').toggle(500,function(evt){
                var icon = $(e.currentTarget).find('i');
                if($(icon).hasClass('fa-toggle-right')){
                    //update icon
                    $(icon).removeClass('fa-toggle-right');
                    $(icon).addClass('fa-toggle-left');
                    //slide the navigation up
                    $('#nav-div').css('top','10px');
                    //toggle footer
                    $('#footer').hide('slow');
                    $('#overviewmap-div').css('bottom','5px');
                    $('.esriControlsBR').css('bottom','5px');
                } else {
                    $(icon).removeClass('fa-toggle-left');
                    $(icon).addClass('fa-toggle-right');
                    $('#nav-div').css('top','50px');
                    $('#footer').show('slow');
                    $('#overviewmap-div').css('bottom','25px');
                    $('.esriControlsBR').css('bottom','25px');
                }
            });
        },
        showTooltip: function(e){
            var el = $(e.currentTarget),
                content = el.data('title');
            //build tooltip only once
            if(el.data('kendoTooltip')){
                $(e.sender).show();
            } else {
                //custom tooltip
                el.kendoTooltip({
                    content: content,
                    position: 'bottom'
                }).data('kendoTooltip').show();
            }
        },
        hideTooltip: function(e){
             $(e.sender).hide();
        },
        updateMapLayer: function(e){
            //hide the current tooltip
            var tbl = $('#tableview')
            tbl.data('kendoTooltip').hide();
            //check if the tableview has been polutated
            if(tableview.tblReady){
                tableview.updateLayer();
            } else {
                nisis.ui.displayMessage('The table view has now been populated.','','warn');
                tbl.trigger('click');
            }
        },
        clearMapLayerUpdates: function(e){
            //hide the current tooltip
            $('#tableview').data('kendoTooltip').hide();
            //clear map updates
            tableview.clearUpdate();
        },
        showTblViewFilter: function(e){
            var action = e.currentTarget.dataset.action;

            if($('#table').data('kendoGrid')){
                tableview.showTblViewFilter(action);
            } else {
                nisis.ui.displayMessage('The table view has not been populated.','','error');
                $('#tableview-div').data('kendoWindow').center().open();
                $('#tableview-content').data('kendoSplitter').resize();
            }
        },
        showIwlFeatures: function(e){
            var action = e.currentTarget.dataset.action;

            tableview.showIwlFeatures(action);
        },
        resetDefaultView: function(e){
            tableview.resetDefaultView(e);
        },
        openFAAPage: function() {
            var faa = "http://www.faa.gov/ato/";
            window.open(faa, '_blank');
        },
        openDOTPage: function() {
            var dot = "http://www.dot.gov";
            window.open(dot, '_blank');
        },
        updateMainMenu: function(e) {
            var $this = this,
                tfr = $this.get('tfrMode'),
                mItems = $this.header.menubar;

            $.each(mItems, function(i, item) {
                if (item.add) {
                    var dialog,
                        target = $('#'+item.target);

                    if (target) {
                        dialog = target.data('kendoWindow');
                    }

                    if (tfr) {

                        if (item.tfr) {
                            $('#m-' + item.id).show();
                        } else {
                            $('#m-' + item.id).hide();
                            if (dialog) {
                                dialog.close();
                            }
                        }

                    } else {
                        if (item.nisis) {
                            $('#m-' + item.id).show();
                        } else {
                            $('#m-' + item.id).hide();
                            if (dialog) {
                                dialog.close();
                            }
                        }
                    }
                }else{
                    if(item.id.toLowerCase()=="history"&&nisis.user.isAdmin){
                        if($("#m-history").length){
                            $('#m-' + item.id).show();
                        }else{
                            var h = $("<div/>",{
                                "id": "m-"+item.id,
                                "class": "menu-item",
                                "data-title": item.title,
                                "data-target": item.target
                            });
                            var img = $("<img width='26' height='26' />");
                            img.attr('id', item.id);
                            img.attr('src', item.icon);
                            h.append(img);
                            h.insertBefore($("#m-help"));
                            h.click($this.toggleWindow);
                            h.mouseover($this.showTooltip);
                            h.mouseout($this.hideTooltip);
                        }
                    }
                }
            });
        },
        toggleWindow: function(evt){
            // console.log("---------------in toggleWindow, evt: ", evt);
            var $this = this,
                target = $('#' + $(evt.currentTarget).data('target')),
                id = $(target).attr('id'),
                apps = nisis.user.apps;

            if(target.length > 0){
                var kendoW = target.data('kendoWindow');
                if(target.is(':visible')){
                    kendoW.close();
                } else {
                    if ( id === 'tfr-div') {

                        if ($this.tfrMode) {
                            kendoW.restore();
                            kendoW.open();

                        } else {
                            var msg = "";

                            if ( apps.length === 1 && apps[0].toLowerCase() === 'tfr' ) {
                                kendoW.restore();
                                kendoW.open();
                                return;
                            }

                            var options = {
                                title: 'TFR Mode',
                                message: 'Are you sure you want to enter TFR Mode?',
                                buttons: ['YES', 'NO']
                            };

                            nisis.ui.displayConfirmation(options)
                            .then(function(resp){
                                if(resp == options.buttons[0]) {
                                    $this.set('tfrMode', true);
                                    $this.set('app', 'tfr');
                                    $('#app').trigger('module-change');
                                    //update shapetools
                                    require(['app/windows/shapetools','app/windows/layers'], function(shapetools, layers) {
                                        shapetools.set('nisisShapes', false);
                                        shapetools.set('app', 'tfr');
                                        layers.set('isNisisActive', false);
                                    });
                                    kendoW.restore();
                                    kendoW.open();
                                    
                                    //set Shapes Polygon layer to display no shapes. FM window will be closed already. 
                                    var shapeLayer = nisis.ui.mapview.map.getLayer('Polygon_Features');       
                                    if (shapeLayer) {
                                        var shapeLayerQuery = shapeLayer.getDefinitionExpression();
                                        $this.set('shapeLayerDefQuery', shapeLayerQuery);
                                        query = "OBJECTID = -1";        
                                        shapeLayer.setDefinitionExpression(query);
                                    }
     
                                    //NISIS - 1981: CC - After to display and open the TFR window, we are going straight to the TFR list to avoid the blank (white) screen at the beginning
                                    require(["app/windows/tfrBuilder"], function(tfrBuilder){
                                        tfrBuilder.listTfrs();
                                    });
                                }
                            },
                            function(error){
                                msg = 'App could not capture your answer. Please try again.',
                                nisis.ui.displayMessage(msg, 'error');
                            });
                        }

                    } else if ( id === 'history-div'){
                        var historyGrid = $("#historyTable").data("kendoGrid");
                        if (historyGrid){
                            historyGrid.dataSource.read();
                        }
                        historyNav.openMasterHistory();
                    } else if ( id === 'drawing-div'){
                        if (!$this.checkIfHandlerExists(kendoW, "close", "onShapeToolsClosed")){
                            kendoW.bind("close", $this.onShapeToolsClosed);
                        }

                        kendoW.title("Shape Tools");
                        $this.showHideStylesFeatureTab("hide");

                        //CC - Selecting the first tab
                        // $('#draw-options-div').data('kendoTabStrip').select(0); 
                        $('#draw-options-div').data('kendoTabStrip').activateTab($('#pushpins')); 
                        kendoW.restore();
                        kendoW.open();
                    } else{
                        //console.log("kendoW", kendoW);
                        kendoW.restore();
                        kendoW.open();
                    }

                    if(id === 'tableview-div'){
                        //window content need to be refreshed only once
                        if($('#tableview-content').data('reset') === true){
                            $('#tableview-content').data('kendoSplitter').toggle('#tbl-panel-left');
                            $('#tableview-content').data('reset',false);
                        }
                        //window will the centered on once
                        if(target.data('center') === true){
                            kendoW.center();
                            target.data('center',false);
                        }

                    } else if ( id === 'admin-panel-div') {
                        //admin panel configured differently so we can load from the external url
                        kendoW.refresh({
                            iframe: true,
                            url: "admin/index.php"
                        });
                        kendoW.center();
                    } else if ( id === 'tfr-admin-panel-div') {
                        //tfr admin panel configured differently so we can load from the external url
                        kendoW.refresh({
                            iframe: true,
                            url: "tfradmin/index.php"
                        });
                        kendoW.center();
                    } else if ( id === 'dashboard-div') {
                        dashboard.createDashboardWidget();
                    } else if ( id === 'alerts-div') {
                        kendoW.center();
                        var alertsGrid = $("#alertTable").data('kendoGrid');
                        var dataSource = alertsGrid.dataSource;
                        nisis.alerts.oid = '';
                        nisis.alerts.facId = '';
                        nisis.alerts.facType = '';
                        nisis.alerts.statusType = '';
                        dataSource.filter({});
                        dataSource.cancelChanges();
                        $('.k-grid-toolbar').hide();
                        $("#readOnlyGrid").removeClass("hideGrid");
                        $("#alertTable").addClass("hideGrid");
                    }
                    else if ( id === 'shapes-manager-div') {
                        shapesMgr.activateShapesManagementTool();
                    }
                }
            }
        },
        showHideStylesFeatureTab: function(action){
            var tabStrip = $("#draw-options-div").data("kendoTabStrip");
            var tabStripItems = tabStrip.items();

            $.each(tabStripItems,function(index, tab){
                if(tab.id === "styles"){
                    if(action === "show"){
                        //CC - Hiding the TabStrip ul elements because they may be displayed when the window was called from "Format Feature" option (Right click over the shape menu) and showing the "Styles" tab in drawing window for editing the feature.
                        $("#draw-options-div.k-tabstrip>ul").hide();
                        $(tabStrip.items()[index]).show();
                    }else{
                        $("#draw-options-div.k-tabstrip>ul").show();
                        $(tabStrip.items()[index]).hide();
                    } 
                    return false;
                }
            });
        },
		checkIfHandlerExists: function(widget, eventName, handlerName) {
			if (widget._events.hasOwnProperty(eventName)) {
				var handlersList = widget._events[eventName];
				var found = false;
				$.each(handlersList, function(i, handler){
					if (handler.name === handlerName){
						found = true;
						return false;
					}
				});
				// return widget._events[eventName].length;
				return found;
			}
			return false;
		},
		onShapeToolsClosed: function(e){
		    if (e.userTriggered){
				var selectedTab = $("#draw-options-div").data("kendoTabStrip").select();
				if (selectedTab.attr("id") === "styles" && selectedTab.index() === 5){
					require(['app/windows/shapetools'], function(shapetools) {
						shapetools.updateFeatureStyles (null, true);
						shapetools.isFirstTime = true;
					});
				}
			}
		},
        openHelpPage: function(e){
            //console.log("Help document:", e);
            //var help = "/nisis/docs/?type=help";
            var help = "/nisis/docs/nisis_user_guide_118.pdf";

            window.open(help, '_blank');
        },
        clpseMenu: function(e){
            var $header = $('#header');
            var $openBtn = $('#openBtn');
            $header.fadeOut();
            $openBtn.delay(200).fadeIn();

        },
        tblSplitterReady: function(e){
            //console.log("tblSplitterReady");
            $(e.sender).toggle('#tbl-panel-left');
        },
        dashSplitterReady: function(e){
            //console.log(e);
            $(e.sender).toggle('#dash-panel-left');
        },
        adjustWindowPosition: function(e){
            //console.log(e);
        },
        getUserSubMenu: function(){
            //console.log(e);
            return $('#user-menu').html();
        },
        userLogOut: function(e){
            nisis.user.logout();
        },
        searchKey: "",
        searchSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "api/auto_complete.php"
                    ,type: 'GET'
                    ,dataType: 'json'
                }
                ,parameterMap: function(data,type){
                    return {term: data.filter.filters[0].value}
                }
            },
            serverFiltering: true,
            error: function(e){
                //console.log(e);
                nisis.ui.displayMessage(e.status, '', 'error');
            }
        }),
        autoCompleteItemSelected: false,
        searchFeatures: function(e){
            var $this = this,
                app = $this.get('app');

                //To prevent to execute keyUp event when an item from the autocomplete (quicksearch) has been selected
                $this.set("autoCompleteItemSelected", true);

            var label = e.item.text(),
                data = e.sender.dataSource.data(),
                id = null,
                value = null;

            //Toggle layers
            // NISIS-2754 - KOFI HONU
            var toggleLayers = function(table){
                if (app && app != 'nisis') {
                    return;
                }

                var layers = [],
                    lyr = table.replace("nisis_", "");

                if ( lyr === 'airports' ) {
                    layers.push('airports_pu');
                    layers.push('airports_pr');
                    layers.push('airports_others');
                    layers.push('airports_military');
                }
                else if ( lyr === 'ans' ) {
                    layers.push('ans1');
                    layers.push('ans2');
                } else if (lyr === 'all_objects') {
                    layers.push('airports_pu');
                    layers.push('airports_pr');
                    layers.push('airports_others');
                    layers.push('airports_military');
                    layers.push('atm');
                    layers.push('ans1');
                    layers.push('ans2');
                    layers.push('osf');
                    //layers.push('teams');
                    layers.push('tof');
                } 
                // else if (lyr === 'resp'){
                //     lyr = 'teams';
                //     layers.push(lyr);
                // } 
                else if (lyr === 'tof'){
                    lyr = 'tof';
                    layers.push(lyr);
                } 
                else  {
                    layers.push(lyr);
                }

                //see if we need to load the layer
                var layerTree = $('#overlays-toggle').data('kendoTreeView');
                 
                $.each(layers, function(i, id){
                    layer = nisis.ui.mapview.layers[id];

                    if (layer) {
                        //find it in Map Layers Tree View
                       
                        var node = layerTree.dataSource.get(id);
                        if(node == null){
                            console.log('toggleLayers: unable to query from Map Layer Tree View');                            
                            return;
                        }

                        //check if tree node exist already
                        if(node.checked == true){                            
                            //we are good, layer has already been loaded
                            return;
                        }
                        else {
                            //select the node from the tree and load the layer
                            layerTree.findByUid(node.uid);
                            var selectitem = layerTree.findByUid(node.uid);
                            selectitem.checked = true;
                            layerTree.select(selectitem);
                            require(["app/windows/layers"], function(layers){
                               layers.updateTreeNode(node.id,true);
                            });                                                 
                        }                                                
                    } 
                });              

            };

            //turn layer on
            var showLayer = function(id){
                if(!id){
                    return;
                }
                var layer = nisis.ui.mapview.map.getLayer(id);
                if( !layer ){
                    var msg = 'App could not identify layer on the map.';
                    nisis.ui.displayMessage(msg,'','error');
                    return;
                }

                if(!layer.visible){
                    layer.setVisibility(true);
                }
            };

            $.each(data,function(idx, d){
                if(d.label === label){
                    id = d.oid;
                    value = d.value;
                    table = d.category;

                    nisis.ui.displayMessage("Searching for " + value);

                    nisis.ui.mapview.util.searchFeatures(id,true,['LOOKUPID'],table)
                    .then(function(data){

                            var features = data,
                                len = features.length;
                            //get the zoom level
                            var z,
                            zoom = nisis.ui.mapview.map.getZoom();
                            if ( zoom > 12 ) {
                                z = zoom ? zoom : 12;
                            }

                            if(len === 0){
                                nisis.ui.displayMessage("App couldn't find the selected features.", "error");
                            } else if (len === 1){
                                nisis.ui.displayMessage("The map will get you to the feature.","1 Result!",'info');
                                var target = features[0].feature.geometry;
                                if (target.type === 'polyline'){
                                    var indexPoint = Math.ceil(target.paths[0].length/2) - 1;
                                    var point = target.getPoint(0,indexPoint);
                                    nisis.ui.mapview.map.centerAndZoom(point,15);
                                    nisis.ui.mapview.util.flashAtLocation(point);
                                }else{
                                    nisis.ui.mapview.map.centerAndZoom(target,z);
                                    nisis.ui.mapview.util.flashAtLocation(target);
                                }
                                toggleLayers(table);
                            } else {
                                nisis.ui.displayMessage("There are " + len + " features found.",len +" Results!",'info');
                                var target1 = features[0].feature.geometry;
                                if (target1.type === 'polyline'){
                                    var indexPoint = Math.ceil(target1.paths[0].length/2) - 1;
                                    var point = target1.getPoint(0,indexPoint);
                                    nisis.ui.mapview.map.centerAndZoom(point,15);
                                    nisis.ui.mapview.util.flashAtLocation(point);
                                }else{
                                    nisis.ui.mapview.map.centerAndZoom(target1,z);
                                    nisis.ui.mapview.util.flashAtLocation(target1);
                                }
                                toggleLayers(table);
                            }

                            //Clearing the quicksearch after it is done
                            $('#search-box').data("kendoAutoComplete").value('');
                        },function(error){
                            nisis.ui.displayMessage(error.message, "error");
                        },function(p){
                            nisis.ui.displayMessage(p, "info");
                        }
                    );
                    return false;
                }
            });
        },
        locateFeature: function(e){
            //console.log(e);
        },
        geocodeAddress: function(e){
            var key = this.get('searchKey'),
                locDir = "Locations & Directions",
                msg = "Geocoding addresses can be done from ";
                msg += "'" + locDir.toUpperCase() + "' widget. Would you like to ";
                msg += "proceed with your input: '" + key.toUpperCase() + "'?";

            if ($.trim(key) == "") {
                return;
            }

            var opt = {
                title: "Geocode Addresses!",
                message: msg,
                buttons: ["YES", "NO"]
            };

            nisis.ui.displayConfirmation(opt)
            .then(function(resp) {
                if (!resp || resp.toUpperCase() !== opt.buttons[0]) {
                    return;
                }
                //get a ref of the Locations & Directions div
                var locDialog = $('#directions-div');

                if (!locDialog.length) {
                    nisis.ui.displayMessage("The app could not open the widget for you.", "error");
                    return;
                }
                //get a ref of the Locations & Directions widget
                var locWin = locDialog.data('kendoWindow');

                if (!locWin) { return; }
                //open the widget if not already
                if (!locDialog.is(':visible')) {
                    locWin.center().open();
                }
                //select the first tab
                $("#dir-tabs").data('kendoTabStrip').select(0);
                //set the seach value
                require(["app/windows/locations"], function(locations) {
                    locations.set('streetAddress', key);
                });

            },function(error) {
                nisis.ui.displayMessage("The app could not open the widget.", "error");
            });
        },
        toggleFullScreen: function(evt){
            if (!document.fullscreenElement &&    // alternative standard method
                !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
                if (document.documentElement.requestFullscreen) {
                    document.documentElement.requestFullscreen();
                } else if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullscreen) {
                    document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
                $(evt.currentTarget).find('.fa').removeClass('fa-arrows-alt')
                .addClass('fa-times-circle-o');
            } else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
                $(evt.currentTarget).find('.fa').removeClass('fa-times-circle-o')
                .addClass('fa-arrows-alt');
            }
        },
        scaleUnit: 'mi',
        scaleLabel: 'Miles',
        scaleValue: "",
        scaleNumValue: 0,
        scaleBarWidth: 'Scale Units: ',
        getScaleUnits: function(){
            return $("#footer")
            .kendoTooltip({
                autoHide: true,
                showOn: 'click',
                width: 'auto',
                height: 'auto',
                position: "top",
                content: $('#num-scale-units ul')
            });
        },
        showNumScaleUnits: function(evt){
            var tooltip = $(evt.target).data("kendoTooltip");
            if(!tooltip){
                $(evt.target).kendoTooltip({
                    autoHide: true,
                    showOn: 'click',
                    width: 'auto',
                    height: 'auto',
                    position: "top",
                    content: $('#num-scale-units ul')
                }).data("kendoTooltip").show();
            } else {
                tooltip.show();
            }
        },
        setNumScaleUnit: function(e) {
            var $this = this;
            console.log(e)
            $('#numScaleUnits').data('kendoTooltip').hide();
            $this.set('scaleLabel', e.currentTarget.innerText);
            $this.set('scaleUnit', e.currentTarget.dataset.unit);
            e.data.updateMapScale();
        },
        updateMapScale: function(evt) {
            var $this = this;
            var len = $('.esriScalebarRuler').css('width');
            len = parseInt(len);

            var label = $('.esriScalebarSecondNumber').text();
            //extract the value from the label
            value = label.match(/[0-9\.]/g).join('');
            //value = Math.round(parseInt(value));
            value = parseFloat(value, 10);
            var nValue = Math.round(parseInt(value) / len / window.innerWidth);
            $this.set('scaleValue',value);

            var unit = label.match(/[^0-9\.]/g).join('');

            var userUnit = $this.get('scaleUnit');

            var v = nisis.ui.mapview.util.convertUnits(value, unit, userUnit);
            v = v.toFixed(2);

            $this.set('scaleValue', v);
            $this.set('scaleNumValue', nValue);
            $('.esriScalebarFirstNumber').hide();
            $('.esriScalebarSecondNumber').text(v+userUnit);

            if(unit !== $this.get('scaleUnit')) {
               /* $this.set('scaleValue', 'unkown');
                $('.esriScalebarSecondNumber').text('Unknown')*/;
            }

        },
        updateNumericScale: function(evt){
            //console.log('Updating the numeric scale ...');
            var scale = (evt && evt.lod) ? evt.lod.scale : nisis.ui.mapview.map.getScale();
            var sUnit = esri.Units[$('#numScaleUnits').data('units')];
            scale = Math.round(nisis.ui.mapview.util.convertLengthUnits(scale,sUnit));
            $('#numScaleVal').html(Math.round(scale));

        },
        mouseLatLonUnits: "DD",
        mouseLatLon: true,
        mouseLatitude: "0.000",
        mouseLongitude: "0.000",
        mouseUTM: false,
        mouseUTMCoords: "",
        mouseGARS: false,
        mouseGARSCoords: "",
        showMouseCoordUnits: function(evt){
            var tooltip = $(evt.target).data("kendoTooltip");
            if(!tooltip){
                tooltip = $(evt.target).kendoTooltip({
                    autoHide: true,
                    showOn: 'click',
                    width: 'auto',
                    height: 'auto',
                    position: "top",
                    content: $('#mouse-pos-units ul')
                }).data("kendoTooltip");
            }
            tooltip.show();
        },
        setMouseCoordUnits: function(e){
            //console.log(e);
            $('#mCoordsUnit').data('kendoTooltip').hide();

            var units = e.currentTarget.dataset.unit;
            self.set('mouseLatLonUnits', units);

            if(units == 'UTM'){
                self.set('mouseUTM', true);
                self.set('mouseLatLon', false);
                self.set('mouseGARS', false);
            } else if (units == 'MGRS' || units == 'GARS'){
                self.set('mouseUTM', false);
                self.set('mouseLatLon', false);
                self.set('mouseGARS', true);
            } else {
                self.set('mouseUTM', false);
                self.set('mouseLatLon', true);
                self.set('mouseGARS', false);
            }
        },
        updateMousePosition: function(evt){
            //console.log(evt);
            if(!evt){
                return;
            }

            var units = self.get('mouseLatLonUnits'),
            mousePos = {
                x: 0,
                y: 0
            }, lat, lon;

            if(evt && evt.mapPoint){
                mousePos = esri.geometry.webMercatorToGeographic(evt.mapPoint);
            }

            if (units === "DD") {
                lat = mousePos.y.toFixed(5);
                lon = mousePos.x.toFixed(5);
            } else if (units === "DMS") {
                require(['app/geopoint'],function(GeoPoint){
                    var pt = new GeoPoint(mousePos.x, mousePos.y);
                    lat = pt.getLatDeg();
                    lon = pt.getLonDeg();
                });

            } else if (units === "DDM") {
                lat = nisis.ui.mapview.util.convertDDtoDDM(mousePos.y) + (lat < 0 ? 'S' : 'N');
                lon = nisis.ui.mapview.util.convertDDtoDDM(mousePos.x) + (lon < 0 ? 'W' : 'E');

            } else if (units === "UTM") {
                //use the geoconverter
                require(["app/geoConverter"], function(geoConverter){
                    var utm = geoConverter.latLonToUTMXYZone(mousePos.y, mousePos.x);
                    utm = utm.zone + (mousePos.y < 0 ? 'S' : 'N') + ' ' + utm.x.toFixed(0) + ' ' + utm.y.toFixed(0);

                    self.set('mouseUTMCoords', utm);
                });
            } else {
                lat = mousePos.y;
                lon = mousePos.x;
                var gars = nisis.ui.mapview.util.convertDDtoGARS(lon,lat);
                self.set('mouseGARSCoords', gars);
            }

            self.set('mouseLatitude', lat);
            self.set('mouseLongitude', lon);
        },
        deactivateMeasure: function(e){
            console.log(e);
        },
        appClock: "",
        appClockErrors: 0,
        updateClock: function() {
            var $this = this;
            //update clock every second
            var check = setInterval(function() {
                $.ajax({
                    url: 'api/lookup.php',
                    type: 'GET',
                    data: {
                        target: 'clock',
                        filter: ''
                    },
                    dataType: 'json',
                    success: function(data, status, xhr) {
                        if (data && data.date) {
                            $this.appClockErrors = 0;
                            var dt = data.date,
                                dtg = kendo.toString(dt, "yyyy/MM/dd HH:mm:ss");

                            dtg = dtg.replace(/-/g, '/').split('.')[0];
                            dtg += " (UTC)";

                            $this.set('appClock', dtg);

                        } else {

                        }
                    },
                    error: function(xhr, status, err) {
                        $this.appClockErrors++;

                        $this.set('appClock', "");
                        //cancel clock after 5min of errors
                        if ( $this.appClockErrors > 300 ) {
                            clearInterval(check);
                        }
                    }
                });
            }, 1000);
        },
        hideNisisObjects: function(e){
            $("body").spin(true);

            var $this = this,
                layers = nisis.ui.mapview.layers;
            var activeLayers = [];
            var count = 0;

            for (var key in layers) {
                if (layers[key].visible != false) {
                    activeLayers[count] = key;
                    count++;
                    layers[key].setVisibility(false);
                }
            }

            var tfrFeatures = overlays.dataSource.data().splice(0,1);
            var nisisMyFeatures = overlays.dataSource.data().splice(0,1);
            var nisisGroupFeatures = overlays.dataSource.data().splice(0,1);
            var nisisTrackedObjects = overlays.dataSource.data().splice(0,1);

            self.activeLayers = activeLayers;
            self.nisisMyFeatures = nisisMyFeatures;
            self.nisisGroupFeatures = nisisGroupFeatures;
            self.nisisTrackedObjects = nisisTrackedObjects;

            $("body").spin(false);
        },
        unhideNisisObjects: function(e){
            $("body").spin(true);

            var layersToShow = self.activeLayers;
            var showMyFeatures = self.nisisMyFeatures;
            var showGroupFeatures = self.nisisGroupFeatures;
            var showTrackedObjects = self.nisisTrackedObjects;
            var overlays = $("#overlays-toggle").data("kendoTreeView");

            overlays.dataSource.data().splice(0,0, showMyFeatures[0]);
            overlays.dataSource.data().splice(1,0, showGroupFeatures[0]);
            overlays.dataSource.data().splice(2,0, showTrackedObjects[0]);

            var layers = nisis.ui.mapview.layers;
            for (i = 0; i < layersToShow.length; i++){
                layers[layersToShow[i]].setVisibility(true);
            }

            $("body").spin(false);
        }
    });

    return self;
});
