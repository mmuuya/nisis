/**
 * map configurations
 */
define([], function(){
	return kendo.observable({
		container: "map",
		center: [-77.03,38.89],
		center2: {
			lon: -77.03,
			lat: 38.89
		},
		zoom: function() {
            var z = 9;

            //TODAY => Set up user setting table and ui
            
            return z;
        },
		extent: [],
        //Default basemap - Try one of these: streets,satellite,hybrid,topo,gray,oceans,national-geographic,osm
        basemap: function() {
            var bm = "streets";

            //TODO => Set up user setting table and ui
            if( nisis.user.username === 'baboyma') {
                bm = 'streets';
            }

            return bm;
        },
        mapScaleUnits: function(){
            var units = 'mi';

            return units;
        },
        mousePositionUnits: function(){
            var units = 'DD';

            return units;
        },
		//map will be reset when the map loads
		map: null,
        mapReady: false,
        navActiveTool: 'pan', //track nav tool state
        isMapLoaded: function(isLoaded){
        	this.set('mapReady',isLoaded);
        },
        showMapNav: function(e){
            e.stopPropagation();
            $(e.target).fadeTo(300, 1);
        },
        hideMapNav: function(e){
            e.stopPropagation();
            $(e.target).fadeTo(300, .3);
        },
        panUp: function(e){
        	//console.log(this.get('map'),e);
            this.get('map').panUp();
        },
        panUpperRight: function(e){
            this.get('map').panUpperRight();
        },
        panRight: function(e){
            this.get('map').panRight();
        },
        panLowerRight: function(e){
            this.get('map').panLowerRight();
        },
        panDown: function(e){
            this.get('map').panDown();
        },
        panLowerLeft: function(e){
            this.get('map').panDown();
        },
        panLeft: function(e){
            this.get('map').panLeft();
        },
        panUpperLeft: function(e){
            this.get('map').panUpperLeft();
        },
        fullExtent: function(e){
        	this.get('map').setLevel(3);
        },
        navTools: null, //nisis.ui.mapview.navTools,
        previousExtent: function(e){
        	this.get('navTools').zoomToPrevExtent();
        },
        nextExtent: function(e){
        	this.get('navTools').zoomToNextExtent();
        },
        updateNavHistory: function(e){
        	//update nav history buttons
        	var nav = this.get('navTools');
        	//extent history for previous
        	if(nav){
	        	if(nav.isFirstExtent()){
	        		$('#nav-previous').css('color','#666');
	        	} else {
	        		$('#nav-previous').css('color','green');
	        	}
	        	//extent history for next
	        	if(nav.isLastExtent()){
	        		$('#nav-next').css('color','#666');
	        	} else {
	        		$('#nav-next').css('color','green');
	        	}
	        }
        },
        zoomInBox: function(e){
        	this.get('navTools').activate(esri.toolbars.Navigation.ZOOM_IN);
        	this.updateNavTools(e,'nav-zoominbox','zoom-in');
            var nTool = e.currentTarget.id.split('-')[1];
            this.set('navActiveTool', nTool);
        },
        zoomOutBox: function(e){
        	this.get('navTools').activate(esri.toolbars.Navigation.ZOOM_OUT);
        	this.updateNavTools(e,'nav-zoomoutbox','zoom-out');
            var nTool = e.currentTarget.id.split('-')[1];
            this.set('navActiveTool', nTool);
        },
        pantool: function(e){
        	this.get('navTools').activate(esri.toolbars.Navigation.PAN);
        	this.updateNavTools(e,'nav-pan','default');
            var nTool = e.currentTarget.id.split('-')[1];
            this.set('navActiveTool', nTool);
        },
        updateNavTools: function(e,tool,text){
            if(tool){
            	$('#'+tool).css('color','green')
            	.siblings().css('color','#000');

            } else {
                if(e === 'nav-pan'){
                    $('#'+e).css('color','green')
                    .siblings().css('color','#000');
                } else {
                    $('#navToggleTools').children().css('color','#000');
                }
            }
        }
	});
});
