/*
 * mapview object
 */
require(["dojo/parser","dojo/dom","dojo/query","dojo/on","dojo/keys","dojo/has","dojo/_base/connect",
	"dojo/dom-construct","esri/config","esri/kernel","esri/arcgis/Portal",
	"esri/layers/ArcGISTiledMapServiceLayer","esri/graphic","esri/geometry/Circle","esri/geometry/Polyline",
	"esri/tasks/query","esri/tasks/QueryTask","esri/InfoTemplate",
	"esri/layers/ArcGISDynamicMapServiceLayer","esri/layers/WebTiledLayer",
	"dojo/cookie", "dojo/json", "dojo/_base/unload","esri/IdentityManager",
	"esri/layers/WMSLayer","esri/layers/WMSLayerInfo","esri/dijit/Basemap","esri/dijit/BasemapLayer","esri/SpatialReference",
	"esri/utils","esri/units","esri/map","esri/toolbars/navigation","esri/toolbars/draw","esri/toolbars/edit",
	"esri/dijit/BasemapGallery","esri/dijit/OverviewMap","esri/dijit/Legend","esri/dijit/Scalebar",
	"esri/dijit/Print","esri/dijit/Measurement","esri/dijit/editing/Editor",
	"esri/dijit/Geocoder","esri/dijit/Directions","esri/tasks/servicearea",
	"esri/dijit/editing/TemplatePicker","esri/geometry/Extent","esri/tasks/GeometryService","esri/tasks/locator",
	"esri/dijit/Popup",
	"esri/dijit/PopupTemplate","dijit/form/VerticalRule","dijit/form/VerticalSlider",
	"dijit/form/HorizontalSlider","dijit/form/Button","dijit/form/CheckBox","dijit/Toolbar",
	"app/basemaps/ShaddedReliefLight",
	"libs/latlong",
	'app/appConfig',
	"app/mapConfig",
	"app/layersConfig",
	"app/windows/layers",
	"app/iconsConfig",
	"app/renderers",
	"app/windows/shapetools",
	"app/windows/tfrBuilder",
	"app/featuresTemplates",
	"app/fieldsInfos",
	"app/mapview/mapUtils",
	"app/dotObjectHeaders",
	"app/dotObjectProfile",
	"app/nisisProfile",
	'/nisis/settings/settings.js',
	"esri/geometry/mathUtils"
], function(parser,dom,query,on,keys,has,connect,
    	domConstruct,esriConfig,kernel,esriPortal,
    	ArcGISTiledMapServiceLayer,Graphic,Circle,Polyline,
    	Query,QueryTask,InfoTemplate,
    	ArcGISDynamicMapServiceLayer,WebTiledLayer,
    	cookie,JSON,baseUnload,IdentityManager,
    	WMSLayer,WMSLayerInfo,Basemap,BasemapLayer,SpatialReference,
    	esriUtils,Units,Map,Navigation,Draw,Edit,
    	BasemapGallery,OverviewMap,Legend,Scalebar,
    	Print,Measurement,Editor,
    	Geocoder,Directions,servicearea,
    	TemplatePicker,Extent,GeometryService,Locator,
    	Popup,
    	PopupTemplate,VerticalRule,VerticalSlider,
    	HorizontalSlider,Button,CheckBox,Toolbar
    	,ShaddedReliefLight
    	,LatLon
    	,appConfig
    	,mapConfig
    	,layersConfig
    	,layers
    	,icons
    	,renderers
    	,shapetools
		,tfrBuilder
    	,templates
    	,fieldsInfos
    	,mapUtils
    	,dotObjectHeaders
    	,dotObjectProfile
    	,nisisProfile
    	,settings
    	,mathUtils
    ){
nisis.ui.mapview = (function(){
	parser.parse();
	//geometry service url
	var gsUrl = nisis.url.arcgis.services + 'Utilities/Geometry/GeometryServer';
	/*
	 * gnewman - The new url (tasks.arcgisonline.com) seems to break other shape tools.
	 * Getting a proxy related error, perhaps the new url needs to be added to the proxy page?
	 * Reverting this change for now.
	 */
	//var gsUrl = "http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer";
	//setup default map elements
	var mapElements = {};
	mapElements.src = layersConfig;
	mapElements.showInfo = false;
	mapElements.loaded = false;
	mapElements.icons = icons;
	mapElements.draw = {};
	mapElements.edit = {};
	mapElements.overlays = [];
	//array of layers to be used in the feature template picker
	mapElements.templateLayers = [];
	mapElements.templateLayerInfos = [];
	mapElements.activeTemplate = null;
	mapElements.EditorLayerInfos = [];
	mapElements.legendLayerInfos = [];
	mapElements.snapLayerInfos = [];
	mapElements.snappingMgr = null;
	mapElements.alwaysSnap = false;
	mapElements.transLayer = null;
	mapElements.tableLayers = [];
	//mapElements.src = nisis.config.layers;
	mapElements.util = {};
	//setup proxy
	esri.config.defaults.io.proxyUrl = "proxy/";
    esri.config.defaults.io.alwaysUseProxy = false;
	esri.config.defaults.io.useCors = true;

	//instantiate a map instance
	mapElements.initMap = function() {
		//credentials will the save when page unloads
		baseUnload.addOnUnload(nisis.user.storeCredentials);
		//loading saved credentials
		nisis.user.loadCredentials();
		//irena
	    var popup = new esri.dijit.Popup({
						},   dojo.create('div', {
							'id': 'popupWindow'})
		);
		popup.showClosestFirst=function(b) {
			console.log("show closest first....")
		    //esri\geometry\mathUtils.js is a ./geometry/mathUtils
		    //make this only for polygon features
			var c = this.features;
            if (c && c.length) {
                if (1 < c.length) {
                    var e, f = Infinity,
                        g = -1,
                        m, h = mathUtils.getLength, //dont know what a is... maybe a symbol.?
                        k, n = b.spatialReference,
                        l, r;
                    b = b.normalize();
                    var layerId = c[0]._layer.id;
                    var layer =  c[0]._layer;
                    if (layerId == 'Polygon_Features') {
                    	//instead of closest first, rearrange the order (if that was not already rearranged) and do the last (or first) in order first
                    	this.select(0);
                    }
                    else {
                    // native jsapi.js call
                    for (e = c.length - 1; 0 <= e; e--)
                        if (m = c[e].geometry) {
                            l = m.spatialReference;
                            k = 0;
                            try {
                                r = "point" === m.type ? m : m.getExtent().getCenter(), r = r.normalize(), n && (l && !n.equals(l) && n._canProject(l)) && (r = n.isWebMercator() ? d.geographicToWebMercator(r) : d.webMercatorToGeographic(r)), k = h(b, r)
                            } catch (p) {}
                            0 < k && k < f && (f = k, g = e)
                        }
                    0 < g && (c.splice(0, 0, c.splice(g, 1)[0]), this.select(0))
                	}
                }
            } else this.deferreds && (this._marked = b)
		};

		//here is the map instance
		mapElements.map = new Map(mapConfig.container, {
			basemap: mapConfig.basemap(),
			center: [mapConfig.center[0], mapConfig.center[1]], //kendo won't let you access the array directly
			zoom: mapConfig.zoom(),
			minZoom: 3,
			//maxZoom: 20,
			slider: false,
			sliderStyle: 'large',
			autoResize: true
		});

		//create a portal
		mapElements.agsonline = new esriPortal.Portal(nisis.url.arcgis.portal);
		//map zoom slider rules
		mapElements.zoomSliderRules = new VerticalRule({
			count: 16,
            style: "width:5px;"
		}, 'zoomRules');
		//map zoom slider
		mapElements.zoomSlider = new VerticalSlider({
			name: 'vertical',
			value: 9,
			minimum: 3,
			maximum: 20,
			discreteValues: 18,
			intermediateChanges: true,
			showButtons: true
			,onChange: function(evt){
				mapElements.map.setZoom(parseInt(evt));
			}
		}, 'zoomSlider');
		mapElements.zoomSlider.startup();
		//define geocoding object
		mapElements.locator = new Locator(nisisProtocol + '://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer');
		//attach complete event to search field
        //mapElements.locator.on('address-to-locations-complete', mapElements.util.showFindLocResults);
        //mapElements.locator.on('error', mapElements.util.displayProcessesErrorMessage);
		//define a geometry service
		mapElements.geometryService = new GeometryService(gsUrl);
		//set default geometry service
		esri.config.defaults.geometryService = mapElements.geometryService;
		//Create a scale bar
        mapElements.scalebar = new Scalebar({
            map: mapElements.map,
            attachTo: 'bottom-left'
            //,scalebarUnit: 'dual'
        });
		//create an overview map
		mapElements.overview = new OverviewMap({
				map: mapElements.map,
				visible: false,
				attachTo: "bottom-right",
				id: 'overviewmap-div',
				height: 200,
				width: 200
			});
		mapElements.overview.startup();

		//measurement tools
		mapElements.measure = new Measurement({
			map: mapElements.map,
			defaultAreaUnit: esri.Units.SQUARE_MILES,
			defaultLengthUnit: esri.Units.MILES
			,advancedLocationUnits: false //disabling this bc it adds more unsupported units to Points measurements
		}, dojo.byId('measurements'));
		mapElements.measure.startup();
		//routing widgets
		mapElements.directions = new Directions({
			map: mapElements.map,
			routeTaskUrl: nisis.url.arcgis.route,
			showClearButton: true,
			directionsLengthUnits: esri.Units.MILES,
		}, 'directions-content');
		mapElements.directions.startup();
        //create the map navigation toolbar
        mapElements.navTools = new Navigation(mapElements.map);
        mapElements.navTools.on('extent-history-change', function(){
			mapConfig.updateNavHistory();
		});
        //create drawing tools
		mapElements.draw.activeTool = null;
		mapElements.draw.geomCenter = null;
        mapElements.draw.tools = new Draw(mapElements.map, {
            showTooltips: true
        });
        //call back
        mapElements.draw.tools.on('draw-end', function(evt) {
        	console.log('VERTICES: ', evt);
			//get app
			require(['app/windows/shapetools'], function(shapetools) {
                var app = shapetools.get('app'),
                	opsActive = shapetools.get('featureOpsActive'),
                	opsTool = shapetools.get('featureOpsTool');

                console.log("Drawing options: ", app, opsActive, opsTool);

                //associate the window with the geometry
                var dotObjectProfileInstance = null;
	            var dotObjectProfileWindow = shapetools.get('dotObjectId');
	            //find the profile window and add evt.geometry in there
	            if (dotObjectProfileWindow) {
	              	//find the window
	              	dotObjectProfileInstance = DOTObjectProfile.prototype.findProfile(dotObjectProfileWindow);
	            }

				if (mapElements.draw.geomCenter) {
					mapElements.util.addGraphicToMap(mapElements.util.createPolygon(mapElements.draw.activeTool, mapElements.draw.geomCenter));
				}

				if(shapetools.addToUserFeatures) {
					if (dotObjectProfileInstance==null) {
						nisis.ui.mapview.util.addGraphicToUserLayers(evt.geometry, app);
					}
					else {
						var dotProfileWindowId =  "dotObject_" + dotObjectProfileInstance.observable_.tableName + dotObjectProfileInstance.graphicId_.attributes.OBJECTID;
						var opts = {
							reference: dotProfileWindowId
						};
						nisis.ui.mapview.util.addGraphicToUserLayers(evt.geometry, app, opts);
					}

				}
				// NISIS-2754- KOFI HONU
				// else if(shapetools.addToResponseTeams) {
				// 	mapElements.util.addFeatureToRespTeamsLayer(evt.geometry);

				// }
				else {
					//check for shapetools operations
					if ( opsActive && (opsTool === 'cut' || opsTool === 'extract') ) {
						mapElements.util.addGraphicToMap(evt.geometry, opsTool);

					} else {
						mapElements.util.addGraphicToMap(evt.geometry);
					}
				}
			});
		});
        //create editing toolbar
        mapElements.edit.tools = new Edit(mapElements.map);
        //add event to map
		mapElements.map.on("load", mapLoadHandler);
        //deactivate editing tools when user click outside of the graphic
        mapElements.map.on('click', mapClickHandler);
		//contextual menu on the map
		mapElements.map.on('mouse-up', mapMouseUpHandler);
        //reposition map when container resizes
        dojo.connect(dijit.byId('map'), 'resize', mapElements.map, 'resize');
		//response to map extent change
		mapElements.map.on('extent-change', mapExtentChangeHandler);
		//on layers added event handler ==> used to build layertree, legend, template picker, editor widget
        mapElements.map.on('layers-add-result', setupFeaturesFunctionalities);
    	// handle drop event on map container
		/*dojo.connect(dojo.byId('map'), 'dragenter' ,function(evt){evt.stopPropagation();evt.preventDefault();});
		dojo.connect(dojo.byId('map'), 'dragover' ,function(evt){evt.stopPropagation();evt.preventDefault();});
		dojo.connect(dojo.byId('map'), 'drop' ,function(evt){
			evt.stopPropagation();
			evt.preventDefault();
		});*/
	};

	var clearShapeSelection = function(){
		var attrInsp = mapElements.featureEditor.attributeInspector;

		var feat = attrInsp._currentFeature;
		if(feat._graphicsLayer){
			feat._graphicsLayer.clearSelection();
		}

		//Deactivate create/edit tools
		mapElements.featureEditor.drawingToolbar.editToolbar.deactivate();
		mapElements.featureEditor.drawingToolbar.drawToolbar.deactivate();
	};

	//clean up infowindow
	var setMapInfoWindowCleanup = function() {
		mapElements.map.infoWindow.on('show', updatePopupWindow);

		//this is needed to be bound so we call deactivate only if popup window gets hidden from the "close" button
		//if they started to move/drag shapes on the map and the popup window is up, esri closes it, causing hide event.
		//we dont need to deactiveate the editToolbar in that case, since the shape is getting edited (being moved)
		$(".esriPopup .titleButton.close").click(function() {
		    nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.deactivate();
		});

		mapElements.map.infoWindow.on('hide',function(evt){
			$(mapElements.map.infoWindow.domNode).addClass("infoWindowHidden");

		    var feat = nisis.ui.mapview.featureEditor._currentGraphic;
			domConstruct.destroy(query('.shapeMeasure', mapElements.map.infoWindow.domNode)[0]);
			//hide status
			$('.dotProfileDirtyForm.statusBar').hide();

			//hide all extra buttons
			$('.profileButton').hide();
			$('.copyButton').hide();
			
			//hide feature nav buttons
			$('.titleButton.next').hide();
			$('.titleButton.prev').hide();

		    // KOFI
			// avoid the "layer.refresh()"
			// Map behaves more smoothly if we do it.  No refresh!
			// NISIS-2400 KOFI: Added the three other user layer types to this logic.
			// When you click on an item, and close the infowindow, now the symbol reverts back to original correct one...
			// Not some jackass RED DOT which only changes after you pan or refresh the map.
			if (feat) {
				var layer = feat._graphicsLayer;
				if (layer && (layer.id=="Polygon_Features" || layer.id=="Point_Features" || layer.id=="Line_Features" || layer.id=="text_features") ) {
					var graphics = layer.graphics
			    	if( graphics && renderers[layer.id]) {
			    		if( renderers[layer.id].styles ){
			    			$.each(graphics,function(i,g){
			                    var oid = g.attributes.OBJECTID,
			                        found = false,
									layeritemtype = features.getItemTypeByLayerId(layer.id);
								var itemtype = features.getItemByObjectId(oid, layeritemtype).ITEMTYPE;
			    				$.each(renderers[layer.id].styles, function(j, style){
			    					if( oid == style.oid && itemtype == layeritemtype) {
			                            found = true;
				    					g.setSymbol(style.symbol);
				    					return false;
				    				}
			    				});
			    			});
			    		}
			    	}
			    	require(['app/windows/shapesManager'],function(shapesManager){
						shapesManager.renderShapesInOrder();
					});
				}
			}
		});
	};

    var setPopupWindowMaximizedPosition = function(evt) {
    	$(mapElements.map.infoWindow.domNode).css('top', '60px');
    	var $restoreBtn = $(mapElements.map.infoWindow.domNode).find(".restore");
    	$restoreBtn.css('top', '14px');
    	$restoreBtn.css('right', '30px');
    }
	var updatePopupWindow = function(evt) {
		console.log("updatePopupWindow..");


		var feats =nisis.ui.mapview.map.infoWindow.features;
		if (feats== undefined || feats.length==0) {
			//when we just add a shape, sometimes Popup Window features are underfined, causing Popup Window have no information in it.
			//It happens (intermittenly) when the show event gets called before the graphic is created (see shapesManager.createShape). The latter will
			//set the newly added feature to be selected in the popup window
			console.log("updatePopupWindow.. - Popup Window features are underfined, setting up the attribute Inspector current feature");
			return;
		}

		//infoWindowHidden used to toggle visibility of paper-input-wrapper divs
		$(mapElements.map.infoWindow.domNode).removeClass("infoWindowHidden");

        //hide datablocks
	    clearTimeout(nisis.ui.datablockTimeOut);
	    nisis.ui.hideDataBlocks();

        var count = nisis.ui.mapview.map.infoWindow.count;
        //do this better and include readonly logic
		if (feats.length>1) {
			//draw the buttons if more then one feature selected
			$(".esriPopup .next").show();
			$(".esriPopup .prev").show();
		}

		var layer = feats[0]._layer;
		var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature();
        var row, template, data, template2, data2, val;

        //if there is no layer.apprenderer, use 'fieldName': "DESCRIPT",
		//	'label': "DESCRIPTION",
		//	'isEditable': true from the layer

	        console.log("updatePopupWindow.. layer.apprenderer", layer);
	        var fields = [];
		   	var observableFields = {};
			var viewModel = {};


            var layerEditable = (layer.id == 'Polygon_Features' || layer.id == 'Point_Features' || layer.id == 'Line_Features' || layer.id == 'text_features'
            	                 || layer.id == 'tfr_polygon_features' || layer.id == 'tfr_point_features' || layer.id == 'tfr_line_features' );

            var showProfile = (layer.id == 'Polygon_Features' || layer.id == 'Point_Features' || layer.id == 'Line_Features' || layer.id == 'text_features'
            	                 || layer.id == 'tfr_polygon_features' || layer.id == 'tfr_point_features' || layer.id == 'tfr_line_features'
            	                 || layer.id == 'atm' || layer.id == 'ans1' || layer.id == 'ans2' || layer.id == 'osf' || layer.id == 'teams'
            	                 );

            //show delete button and gotoProfile only when appropriate (dot layer profile only?)
            var deleteBtn = (layerEditable)?'<button class="delete-btn" data-bind="click: deleteGraphic"></button>':'';
            var gotoProfileBtn = (showProfile)?'<i class="fa fa-th-list" data-bind="click: gotoProfile"></i>':'';
            var saveBtn = (layerEditable)?'<button class="save-btn" data-bind="click: saveGraphicsAttributes"></button>':'';
			var statusBar = '<div class="dotProfileDirtyForm statusBar"><i class="fa fa-exclamation-circle"></i> Unsaved changes</div>';
			var actionBar = '<div class="info-action-bar"> '+ gotoProfileBtn + deleteBtn + saveBtn + '</div>';

			var content= '<div id="summary-form">' + statusBar + actionBar;
	     	fieldInfo = layer.apprenderer ? layer.apprenderer : null;

	     		console.log("layer fieldInfo...", fieldInfo);
	     		if (fieldsInfos!=null && fieldInfo!=null) {
	     			fields = fieldsInfos[fieldInfo];
	     		}
	     		else {
	     			for (var ndx=0; ndx<layer.fields.length; ndx++) {
			        	var f = layer.fields[ndx];
			        	var field = {};
			        	field.label = f.alias;
			     		field.fieldName = f.name;
			     		field.isEditable = false;
			     		fields.push(field);
			        }
			        if (layer.group && layer.group === 'rivers') {
			        	//search for url and add on more html to the label
			        	for (var ndx=0; ndx<fields.length; ndx++) {
			        	var field = fields[ndx];
			        		if (field.label.toUpperCase() == "Url".toUpperCase()) {
			        			field.label = '<p class="river_url" >URL<a target="_blank" href="'+selFeat.attributes.url+'"><span title="Link to Hydrograph" class="rivers-hydrograph"><i class="fa fa fa-external-link"></i></span></a></p>';
			        		}
			        	}
			        }
	     		}

		    	if (fields != undefined) {
			        console.log("fields...", fields);
			        for (var ndx=0; ndx<fields.length; ndx++) {
			        	var field = fields[ndx];

					    template = kendo.template("<label>#= label #</label>");
					    var fName = field.label.toLowerCase();
					    var input = '<input type="text" data-bind="value: #=name #" class="paper-input paper-input--floating paper-input--touched" />';

					    //handles toggling the pretty stuff on the inputs
					    //$('input').toggleClass('paper-input--touched', $('input').val() !== '');

					    function capitalizeFirstLetter(string) {
						    return string[0].toUpperCase() + string.slice(1);
						};
						fName = capitalizeFirstLetter(fName);

					    data = {label: fName}; //Data with HTML tags

					    if (layerEditable && field.isEditable) {
					    	 observableFields[field.fieldName] = selFeat.attributes[field.fieldName];
					    	 templateEditBox = kendo.template(input);
						     data2 = {name: field.fieldName }; //Data with HTML tags
						     row = '<div class="paper-input__wrapper">' + templateEditBox(data2) + template(data) + '</div>';
					    }
					    else {
					    	 templateText = kendo.template("<span class='uneditable-fields'>#= value #</span>");
					    	 val = selFeat.attributes[field.fieldName]!=null? selFeat.attributes[field.fieldName]:"";
					         data2 = {value: val }; //Data with HTML tags
					         row = '<div class="paper-input__wrapper">' + template(data) + templateText(data2) + '</div>';
					    }
		                content = content + row; //Pass the data to the compiled template
			        }
				}


	     	//append the end...
	        if (content != "") {
	        	content = content + '</div>';
	        }

	       	//add on measure wrapper
	       	content = content + '<div class="shapeMeasureWrapper"></div>';

	        nisis.ui.mapview.map.infoWindow.setContent(content);

	        //bind kendo after content has been added
	        observableFields['gotoProfile'] = function(e) {
	        	var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature(),
				oid = selFeat.attributes.OBJECTID,
				layer = selFeat._graphicsLayer;
				var feature = {
					oid: oid,
					geom: selFeat.geometry,
					layer: layer
				};
				if (layer.group === 'dot' || layer.group ==='pipelines')  {
					//bring up the DOT profile window
	                $.ajax({
	                        url:"api/",
	                            type:"POST",
	                            data: {
	                            action: "get_profiles_dot",
	                            layerid: layer.id,
	                            objectid: oid,
	                            userid: Number(nisis.user.userid)
	                        },
	                        success: function(data, status, req) {
	                            var resp = JSON.parse(data);
	                            var profileData = JSON.parse(resp["profile"][0]["PROFILE"])["PROFILE"];
	                            if(profileData["METADATA"].length>0){
	                                var evt = [];
	                                evt.graphic = feat;
	                                //convert from screen point to map point
	                                if (feat.geometry.type=="polyline") {
	                                    evt.mapPoint =  nisis.ui.mapview.util.getGeometryMapPoint(feat.geometry.getPoint(0,0));
	                                }
	                                else if (feat.geometry.type=="point") {
	                                    evt.mapPoint = nisis.ui.mapview.util.getGeometryMapPoint(feat.geometry);
	                                }
	                                evt.screenPoint = esri.geometry.toScreenPoint(nisis.ui.mapview.map.extent,
	                                    	          nisis.ui.mapview.map.width, nisis.ui.mapview.map.height, evt.mapPoint);
	                                var infobox = new DOTObjectProfile(profileData, evt, oid);
	                                }else{
	                                	nisis.ui.displayMessage(profileData["MSG"], 'error');
	                                }
	                            },
	                        error: function(req, status, err) {
	                            console.error("ERROR in ajax call to get DOT map objects");
	                            }
	                        });
				}
				else {
					//bring up regular profile window
					console.log("bring up regular profile window");
					nisis.ui.mapview.util.buildFeatureProfile(feature);
				}

				nisis.ui.mapview.map.infoWindow.hide();
				selFeat._graphicsLayer.clearSelection();

	        };
	        observableFields['deleteGraphic'] = function(e) {
	            var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature();
	        	nisis.ui.mapview.util.removeFeatureFromUserLayers(selFeat, false);
	        };
	        observableFields['saveGraphicsAttributes'] = function(e) {
	        	//see if we have warnings on the form
	        	if ($("#summary-form").find(".k-warning").length > 0) {
	        		console.log("errors length: ", $("#summary-form").find(".k-warning").length);
	        		console.log("summary-form got warnings on the form, validation failed, no Save");
	        		return;
	        	}

				$("#summary-form").find('input[type="text"]').eq(0).focus();


		        var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature();
		        var nameAttrNewValue = null;
		        if (selFeat != undefined) {
		        	//go thru and see what changed
		        	$.each(e.data.toJSON(), function(i){
		        		var graphicsAttribute = i;
		        		$.each(e.data.modifiedFields, function(j) {
		        			var modifiedField = j;
			        		if (graphicsAttribute === modifiedField) {
			        			//copy the new value over from the form to the selected Feature
			        			selFeat.attributes[graphicsAttribute] = e.data[graphicsAttribute];
			        		}
		        		});
					});

			        //save the value with applyEdits. //see if edits were successfull, then ...
			        layer.applyEdits(null, [selFeat], null).then(function(d){
						//special case if the name for Feature Management items or Shape name changed:
				        var lyrID = layer.id;
				        if (e.data.modifiedFields.hasOwnProperty('NAME')) {
						    if(appConfig.get('tfrMode') && lyrID == 'tfr_polygon_features') {
								nisis.ui.mapview.util.updateCurrentTFRShapeAttribute(selFeat.attributes.OBJECTID, 'NAME', e.data['NAME']);
							}else if(appConfig.get('tfrMode')==false && (lyrID == 'Polygon_Features' || lyrID == 'Point_Features' || lyrID == 'Line_Features' || lyrID == 'text_features')) {
								nisis.ui.mapview.util.updateFeatureManagerShapeName(selFeat.attributes.OBJECTID, features.getItemTypeByLayerId(lyrID), 'NAME', e.data['NAME']);
							}
						};
						//reset e.data.modifiedFields from the viewModel
						if ($statusBar.length>0){
							$statusBar.removeClass('show');
						}
						e.data.modifiedFields = {};

					},function(e){
						console.log("applyEdits did not work");
					});
		        }
		        else {
		        	console.log("unable to get selected feature from popup Window: its undefined");
		        }
		    };

		    //add on to the observable
			viewModel = kendo.observable(observableFields);
			viewModel.modifiedFields={};
			viewModel.bind("set", function(e) {
                var observable = e.sender;
                observable.modifiedFields[e.field]=e.value;
			});
			//attach
    	    kendo.bind($("#summary-form"), viewModel);
    	    // attach a validator to the container
    	    $("#summary-form").kendoValidator({
		        messages: {
		             // defines a message for the custom validation rule
		             custom: "Please enter valid value for your shape name",
		        },
		        rules: {
		            custom: function(input) {
		           	  console.log("valid....... Check if this input is NAME and its Polygon Feature");
		           	  var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature();
		           	  var lyrID=selFeat._layer.id;
		           	  var viewModel = input.get(0).kendoBindingTarget.source;
		           	  var warnings = $("#summary-form").find(".k-warning").length;

					//   console.log('changed fields:', Object.keys(viewModel.modifiedFields).length)

					  $statusBar = $('#summary-form  > .dotProfileDirtyForm');
 					  Object.keys(viewModel.modifiedFields).length > 0 ? $statusBar.addClass('show') : $statusBar.removeClass('show');

		              if ((lyrID == 'Polygon_Features' || lyrID == 'Point_Features' || lyrID == 'Line_Features' || lyrID == 'text_features')
		                                        && viewModel.modifiedFields.hasOwnProperty('NAME') ) {
		                var toMatch = /~|`|!|@|#|\$|%|\^|&|\*|\(|\)|_|\+|\[|\]|\|;|'|,|\/|<|>|\?|:|"|{|}|\||\\/,
			                toReturn = input.val().match(toMatch) === null;

			            //add a check for empty string
			            if ($(input).val().trim() == "") {
						    // it's empty
						    toReturn = false;
						}


						// if length > 0, then toReturn has to be false
						console.log("errors count:",  $("#summary-form").find(".k-warning").length);
						console.log("toReturn:", toReturn);

						if(warnings === 1 && toReturn === true){
							$("#summary-form").find("*").detach(".k-warning");
							console.log("removing warning")
						}
						return toReturn;

		             }
					 return true;
		            }
		        }
	        });


		    //show profile and share buttons
			if(layer && layer.group){
				if(layer.group === 'nisisto' || layer.group === 'dot' || layer.group === 'pipelines') {
					$('.info-action-bar .fa-th-list').show();
					//NISIS-2754 - KOFI HONU
					// if( layer.id === 'teams' ) {
					// 	$('.info-action-bar .fa-th-list').hide();
					// }
				} else if (layer.group === 'user') {
					$('.info-action-bar .fa-th-list').hide();
				}

				if (layer.isEditable()) {
					$('.info-action-bar .save-btn').show();
				} else {
					$('.info-action-bar .save-btn').hide();
				}
			}

	        //add on shape measure for polylines, polygons and points
	        createShapeMeasure(selFeat);

	};

	var createShapeMeasure = function(feat) {
		//destroy units div
			domConstruct.destroy(query('.shapeMeasure', mapElements.map.infoWindow.domNode)[0]);
			//check if the measurement tool update
			if (!nisis.ui.mapview.measure) {
				return;
			}
			//check geom type
			if (feat.geometry && feat.geometry.type == "polygon") {
				//console.log('Calculationg feature area/perimeter: ');
				if($('.shapeMeasure').length === 0){
					var shapeMeasure = domConstruct.create('div', {
						'class': "shapeMeasure",
						'innerHTML': "<p>AREA: <span id='shape-area'>processing ...<span></p>"
							+ "<p>LENGTH: <span id='shape-length'>processing ...<span></p>"
					}, query('.shapeMeasureWrapper', mapElements.map.infoWindow.domNode)[0]);
				}
				//get area measurements
				nisis.ui.mapview.measure.setTool('area', true);
				var area = mapElements.measure.on("measure-end", function(evt){
					var tool = mapElements.measure.getTool(),
						unit = " Sq Miles"; //evt.unitName;

					$('#shape-area').text(evt.values.toFixed(2) + unit);
					mapElements.measure.clearResult();
					nisis.ui.mapview.measure.setTool('area', false);
					dojo.disconnect(area);
				});
				nisis.ui.mapview.measure.measure(feat.geometry);

				//create a line feature from polygon
				var line = new Polyline(mapElements.map.spatialReference),
					rings = feat.geometry.rings;

				$.each(rings, function(i, ring) {
					line.addPath(ring);
				});

				//get line measurements
				nisis.ui.mapview.measure.setTool('distance', true);
				var len = mapElements.measure.on("measure-end", function(evt){
					var tool = mapElements.measure.getTool(),
						unit = " Miles"; //evt.unitName;

					$('#shape-length').text(evt.values.toFixed(2) + unit);
					mapElements.measure.clearResult();
					nisis.ui.mapview.measure.setTool('distance', false);
					dojo.disconnect(len);
				});
				nisis.ui.mapview.measure.measure(line);

			} else if (feat.geometry && feat.geometry.type == "polyline") {

				if ( $('.shapeMeasure').length === 0 ) {
					var shapeMeasure = domConstruct.create('div', {
						'class': "shapeMeasure",
						'innerHTML': "<p>LENGTH: <span id='shape-length'>processing ...<span></p>"
					}, query('.shapeMeasureWrapper', mapElements.map.infoWindow.domNode)[0]);
				}
				//get line measurements
				nisis.ui.mapview.measure.setTool('distance', true);
				var p = mapElements.measure.on("measure-end", function(evt) {
					var tool = mapElements.measure.getTool(),
						unit = " Miles"; //evt.unitName;

					$('#shape-length').text(evt.values.toFixed(2) + unit);
					mapElements.measure.clearResult();
					nisis.ui.mapview.measure.setTool('distance', false);
					dojo.disconnect(p);
				});
				nisis.ui.mapview.measure.measure(feat.geometry);

			} else if ( feat.geometry && feat.geometry.type == "point" ) {

				if($('.shapeMeasure').length === 0){
					var shapeMeasure = domConstruct.create('div', {
						'class': "shapeMeasure",
						'innerHTML': "<p>Lat/Lon (DD): <span id='shape-ll'>processing ...<span></p>"
						+ "<p>Lat/Lon (DMS): <span id='shape-dms'>processing ...<span></p>"
					}, query('.shapeMeasureWrapper', mapElements.map.infoWindow.domNode)[0]);
				}

				var point = esri.geometry.webMercatorToGeographic(feat.geometry),
					lat = Number( point.y.toFixed(5) ),
					lon = Number( point.x.toFixed(5) ),
					dms = new LatLon(lat, lon).toString('dms', 2);

				$('#shape-ll').text(lat + " / " + lon);
				$('#shape-dms').text(dms);
			}
	};
	//NOT USED ANYMORE
	var updateInfoWindowNOTUSEDANYMORELEFTJUSTFORREFERENCE = function(evt) {
		//NOT BEING USED. updatePopupWindow is similar but better!

        if (mapElements.featureEditor.attributeInspector && appConfig.get('tfrMode') == false) {
			//do we have TFR Builder form open?
			 //there is infobox for nisis too (airports), so making sure I am hooking up to a TFR infobox, not the NISIS one.
	       	mapElements.featureEditor.attributeInspector.on("attribute-change", function(evt) {
				if(appConfig.get('tfrMode')) {
					nisis.ui.mapview.util.updateCurrentTFRShapeAttribute(evt.feature.attributes.OBJECTID, evt.fieldName, evt.fieldValue);
				}else{
					nisis.ui.mapview.util.updateFeatureManagerShapeName(evt.feature.attributes.OBJECTID, 'NEEDTYPEHERE', evt.fieldName, evt.fieldValue);
				}
	  		});
        }

		//hide data blocks
		nisis.ui.hideDataBlocks();
		// //get feature from attribute inspector
		// var attrInsp = mapElements.featureEditor.attributeInspector,
		// 	feat = attrInsp._currentFeature,
		// 	len = attrInsp._selection.length;

	    //irena - we are not getting from attribute inspector, we are getting selected feature from Popup Window
	    // var attrInsp = mapElements.featureEditor.attributeInspector,
		var feat = nisis.ui.mapview.map.infoWindow.getSelectedFeature(),
		 	len = nisis.ui.mapview.map.infoWindow.count;


		if (feat) {
			// console.log('Info Window feature: ',attrInsp._currentFeature);
			 //irena - we are not getting from attribute inspector, we are getting selected feature from Popup Window
			// var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature(),;
			// var featLayer = selFeat._graphicsLayer;
			// var lyrID = selFeat._graphicsLayer.id;

			var selFeat = nisis.ui.mapview.map.infoWindow.getSelectedFeature();
			var featLayer = selFeat._graphicsLayer;
			var lyrID = selFeat._graphicsLayer.id;

            var showNextPrevButtons = true;
		    //special actions for Feature Layers
		    //remove prev and next arrows and refresh layer to reset the Symbol to original from the Highlighted if the selection includes shared/read only shape to prevent shared shape to be moved by the read-only users
		    //restrict special characters
		    //delete for Polygon Layers - will also remove a shape from FM
		    if(appConfig.get('tfrMode') == false && (lyrID == 'Polygon_Features' || lyrID == 'Point_Features' || lyrID == 'Line_Features' || lyrID == 'text_features') ) {


		    	//go thru attrInsp._selection and deselect all read only features.
		    	//if at least one read only feature in the cluster, remove the Next and Prev buttons and refresh the layer to get rid of highlight over the shared shape
		    	if (len && len > 1) {
					// for (var i=0; i<=attrInsp._selection.length; i++) {
					// 	if (attrInsp && attrInsp._selection[i] && attrInsp._selection[i].hasOwnProperty("attributes") && nisis.ui.isReadOnlyFeature(attrInsp._selection[i].attributes.OBJECTID, lyrID) == true) {
					// 	    var sharedObjectId = attrInsp._selection[i].attributes.OBJECTID;
					// 		showNextPrevButtons = false;
					// 		break;
					// 	}
					// }
					var arrayFeatures = nisis.ui.mapview.map.infoWindow.features;
					for (var i=0; i<len; i++) {
						if (arrayFeatures[i].hasOwnProperty("attributes") && nisis.ui.isReadOnlyFeature(arrayFeatures[i].attributes.OBJECTID, lyrID) == true) {
						    var sharedObjectId = arrayFeatures[i].attributes.OBJECTID;
							showNextPrevButtons = false;
							break;
						}
					}
					if (showNextPrevButtons==false) {
						selFeat._graphicsLayer.refresh();
					}
				}

				//Restrict special characters for polygon name
				var $td = $(".esriAttributeInspector table td").filter(function() {
				    return $(this).text() == 'NAME';
				});
				var $tr = $td.parent();
				var $inpt = $tr.find("input");

                var specialChars = [62,63,33,34,36,64,35,37,92,94,38,42,40,41,  126];

				$inpt.bind("keypress", function(event) {
				// prevent if in array
				   if($.inArray(event.which,specialChars) != -1) {
					   console.log('keypress prevent')
				       event.preventDefault();
				   }
				});

				mapElements.featureEditor.attributeInspector.on("delete", function(evt) {
					//delete is getting called multiple times from the dojo Info Window. Check the selected feature to delete the graphic only once
					var feat = evt.feature;
					//var feat = nisis.ui.mapview.featureEditor.attributeInspector._currentFeature;
					if (feat && nisis.ui.mapview.featureEditor.attributeInspector._currentFeature) {
						nisis.ui.mapview.util.removeFeatureFromUserLayers(feat, true);
					}
	            }); //irena - need to redo
			}

			//special thing for rivers layers - add url to display the gauge graph
			if(featLayer && featLayer.group && featLayer.group === 'rivers' ) {				
				//add an button that takes to url
				;(function() {
				    jQuery.expr[':'].containsNC =  function(elem, index, match) {
				        return (elem.textContent || elem.innerText || jQuery(elem).text() || '').toLowerCase().indexOf((match[3] || '').toLowerCase()) >= 0;
				    }
				}(jQuery));

				var dmInfoWindow = $(mapElements.map.infoWindow.domNode), 
				tdUrl = dmInfoWindow.find("td.atiLabel:containsNC('url')");									
				tdUrl.append('<a target="_blank" href="'+feat.attributes.url+'"><span title="Link to Hydrograph" class="rivers-hydrograph"><i class="fa fa fa-external-link"></i></span></a>');				
			}


			//show feature nav buttons
			if (len && len > 1 && showNextPrevButtons) {
				$('.titleButton.next').show();
				$('.titleButton.prev').show();
			} else {
				$('.titleButton.next').hide();
				$('.titleButton.prev').hide();
			}
			//show profile and share buttons
			if(featLayer && featLayer.group){
				//console.log(featLayer.group);
				if(featLayer.group === 'nisisto' || featLayer.group === 'dot' || featLayer.group === 'pipelines') {
					$('.copyButton').hide();					
					//CC - Hiding the buttons per NISIS-2148.
					$('.zoomToButton').hide();
					$('.profileButton').show();
					if( featLayer.id === 'teams' ) {
						$('.profileButton').hide();
					}
				} else if (featLayer.group === 'user') {
					//CC - Hiding the buttons per NISIS-2148
					// $('.copyButton').show();
					// $('.shareButton').show();
					$('.copyButton').hide();					
					$('.zoomToButton').hide();
					$('.profileButton').hide();
				} else {
					// $('.copyButton').show();
					//CC - Hiding the buttons per NISIS-2148
					$('.copyButton').hide();
					$('.profileButton').hide();					
					$('.zoomToButton').hide();
				}

				if (featLayer.isEditable()) {
					$('.saveButton').show();
				} else {
					$('.saveButton').hide();
				}
			}
			//destroy units div
			domConstruct.destroy(query('.shapeMeasure', mapElements.map.infoWindow.domNode)[0]);
			//check if the measurement tool update
			if (!nisis.ui.mapview.measure) {
				return;
			}
			//check geom type
			if (feat.geometry && feat.geometry.type == "polygon") {
				//console.log('Calculationg feature area/perimeter: ');
				if($('.shapeMeasure').length === 0){
					var shapeMeasure = domConstruct.create('div', {
						'class': "shapeMeasure",
						'innerHTML': "<p>AREA: <span id='shape-area'>processing ...<span></p>"
							+ "<p>LENGTH: <span id='shape-length'>processing ...<span></p>"
					}, query('.atiAttributes', mapElements.map.infoWindow.domNode)[0]);
				}
				//get area measurements
				nisis.ui.mapview.measure.setTool('area', true);
				var area = mapElements.measure.on("measure-end", function(evt){
					var tool = mapElements.measure.getTool(),
						unit = " Sq Miles"; //evt.unitName;

					$('#shape-area').text(evt.values.toFixed(2) + unit);
					mapElements.measure.clearResult();
					nisis.ui.mapview.measure.setTool('area', false);
					dojo.disconnect(area);
				});
				nisis.ui.mapview.measure.measure(feat.geometry);

				//create a line feature from polygon
				var line = new Polyline(mapElements.map.spatialReference),
					rings = feat.geometry.rings;

				$.each(rings, function(i, ring) {
					line.addPath(ring);
				});

				//get line measurements
				nisis.ui.mapview.measure.setTool('distance', true);
				var len = mapElements.measure.on("measure-end", function(evt){
					var tool = mapElements.measure.getTool(),
						unit = " Miles"; //evt.unitName;

					$('#shape-length').text(evt.values.toFixed(2) + unit);
					mapElements.measure.clearResult();
					nisis.ui.mapview.measure.setTool('distance', false);
					dojo.disconnect(len);
				});
				nisis.ui.mapview.measure.measure(line);

			} else if (feat.geometry && feat.geometry.type == "polyline") {

				if ( $('.shapeMeasure').length === 0 ) {
					var shapeMeasure = domConstruct.create('div', {
						'class': "shapeMeasure",
						'innerHTML': "<p>LENGTH: <span id='shape-length'>processing ...<span></p>"
					}, query('.atiAttributes', mapElements.map.infoWindow.domNode)[0]);
				}
				//get line measurements
				nisis.ui.mapview.measure.setTool('distance', true);
				var p = mapElements.measure.on("measure-end", function(evt) {
					var tool = mapElements.measure.getTool(),
						unit = " Miles"; //evt.unitName;

					$('#shape-length').text(evt.values.toFixed(2) + unit);
					mapElements.measure.clearResult();
					nisis.ui.mapview.measure.setTool('distance', false);
					dojo.disconnect(p);
				});
				nisis.ui.mapview.measure.measure(feat.geometry);

			} else if ( feat.geometry && feat.geometry.type == "point" ) {

				if($('.shapeMeasure').length === 0){
					var shapeMeasure = domConstruct.create('div', {
						'class': "shapeMeasure",
						'innerHTML': "<p>Lat/Lon (DD): <span id='shape-ll'>processing ...<span></p>"
						+ "<p>Lat/Lon (DMS): <span id='shape-dms'>processing ...<span></p>"
					}, query('.atiAttributes', mapElements.map.infoWindow.domNode)[0]);
				}

				var point = esri.geometry.webMercatorToGeographic(feat.geometry),
					lat = Number( point.y.toFixed(5) ),
					lon = Number( point.x.toFixed(5) ),
					dms = new LatLon(lat, lon).toString('dms', 2);

				$('#shape-ll').text(lat + " / " + lon);
				$('#shape-dms').text(dms);
			}
			//hide styles buttons
			$('.styleButton').hide();
		}
	}
	//create feature editing link on info window
	var createEditLink = function() {
		var editLink = dojo.create('a', {
			'class': 'action',
			'innerHTML': 'Edit',
			'href': 'javascript:void(0);'
		}, dojo.query('.actionList', mapElements.map.infoWindow.domNode)[0]);
		dojo.connect(editLink, 'click', function(){
			var graphic = mapElements.map.infoWindow.getSelectedFeature();
			//console.log(graphic);
			//mapElements.featureEditor.drawingToolbar.editToolbar.activate(graphic.geometry);
		});
	};
	var setNumericScale = function() {
        //console.log('Setting up the numeric scale ...');
        var scale = mapElements.map.getScale();
        scale = Math.round(scale);
        $('#numScaleVal').text(scale);
    };
    var updateNumericScale = function(evt) {
        //console.log('Updating the numeric scale ...');
        var scale = (evt && evt.lod) ? evt.lod.scale : mapElements.map.getScale();
        var sUnit = esri.Units[$('#numScaleUnits').data('units')];
        scale = Math.round(nisis.ui.mapview.util.convertLengthUnits(scale,sUnit));
        $('#numScaleVal').html(Math.round(scale));
    };
    //update map position coordinates
    var updateMousePosition = function(evt){
        var unit = "esri.Units." + $('#mCoordsUnit').data('units'),
        mousePos, lat, lon;
        if (unit === "esri.Units.DECIMAL_DEGREES") {
            mousePos = esri.geometry.webMercatorToGeographic(evt.mapPoint);
            lat = mousePos.y.toFixed(5);
            lon = mousePos.x.toFixed(5);
        } else {
            mousePos = evt.mapPoint;
            lat = nisis.ui.mapview.util.convertLengthUnits(mousePos.y,unit).toFixed(2);
            lon = nisis.ui.mapview.util.convertLengthUnits(mousePos.x,unit).toFixed(2);
        }
        //update the lat/lon on the screen
        $('#mouseLat').html(lat);
        $('#mouseLon').html(lon);
    };
	//map loaded handler
	var mapLoadHandler = function(evt){
		//random bug. Force map to correct size
    	$('.map').css({top:0, left:0, right:0, bottom:0});
    	//display nav-bar
		$('#nav-div').removeClass('hide')
    	//another hack to bring the dd up
    	$('.esriToggleButton .dijitButtonNode').click(function(evt){
    		if($('.dijitPopup').is(':visible')){
    			$('.dijitPopup').css('z-index', 11000);
    		}
    	})
    	//resize map according to its container
        mapElements.map.resize();
        mapElements.map.reposition();
        //resize info window
        //mapElements.map.infoWindow.resize('auto', 'auto');
		//set numeric scale
		setNumericScale();
		//display nav
		mapConfig.isMapLoaded(mapElements.map.loaded);
		//reset map value in mapConfig
		mapConfig.map = evt.map;
		//reset navTools value in mapConfig
		mapConfig.navTools = mapElements.navTools;
		//activate the pan tool
		mapConfig.updateNavTools('nav-pan');
		//update mouse position lat/lon
        mapElements.map.on('mouse-move', appConfig.updateMousePosition);
        mapElements.map.on('click', appConfig.updateMousePosition);
		//activate editing tools when user click on map graphics
        //mapElements.map.graphics.on('dbl-click', nisis.ui.mapview.util.activateEditTools);
		//create a sub menu on feature graphic as well
		mapElements.map.graphics.on('mouse-up', nisis.ui.createFeatureSubMenu);
		//clean up info window
        setMapInfoWindowCleanup();
        //build basemap galery
        buildBasemapGalery();
        //build map layers
        // KOFI before calling the following, call the getUserDefaultVisibleShapes() to set definition expressions.s
        nisis.ui.mapview.buildMapLayers();
	};
	var buildOverviewMap = function(){
		mapElements.overview = new OverviewMap({
				map: mapElements.map,
				visible: false,
				attachTo: "bottom-right",
				id: 'overviewmap-div',
				height: 200,
				width: 200
			});
			mapElements.overview.startup();
	}
	//build basemap galery
	var buildBasemapGalery = function(){
		nisis.url.geoserver.services = nisisProtocol + "://192.168.19.193:8080/geoserver/adapt/wms";
		var wkid = 4326;
        var srs = new SpatialReference({wkid:wkid});

        var srlExt = new Extent(-180.0,-90.0,180.0,90.0,srs),
        srLight = new WMSLayer(nisis.url.geoserver.services, {
        	resourceInfo: {
	            extent: srlExt,
	            layerInfos: [new WMSLayerInfo({name:"adapt:base_sr_light",title:"Shaded Relief (Light)"})]
	        },
	        visibleLayers: ["base_sr_light"]
        });
        srLight.spatialReferences[0] = wkid;

        var srdExt = new Extent(-180.0,-90.0,180.0,90.0,srs),
        srDark = new WMSLayer(nisis.url.geoserver.services, {
        	resourceInfo: {
	            extent: srdExt,
	            layerInfos: [new WMSLayerInfo({name:"adapt:base_sr_dark",title:"Shaded Relief (Dark)"})]
	        },
	        visibleLayers: ["base_sr_dark"]
        });
        srDark.spatialReferences[0] = wkid;

        var bdExt = new Extent(-180.0,-90.0,180.0,90.0,srs),
        bDark = new WMSLayer(nisis.url.geoserver.services, {
        	resourceInfo: {
	            extent: bdExt,
	            layerInfos: [new WMSLayerInfo({name:"base_dark",title:"Dark Canvas"})]
	        },
	        visibleLayers: ["base_dark"],
	        format: "png",
	        transparent: false
        });
        bDark.spatialReferences[0] = wkid;
        //var srl = new ShaddedReliefLight();
        //arcgis online map services
        var esriSRL = new ArcGISTiledMapServiceLayer(nisisProtocol + "://services.arcgisonline.com/arcgis/rest/services/World_Shaded_Relief/MapServer");
        var esriWPM = new ArcGISTiledMapServiceLayer(nisisProtocol + "://services.arcgisonline.com/arcgis/rest/services/World_Physical_Map/MapServer");
        //var esriUSTP = new ArcGISTiledMapServiceLayer("https://services.arcgisonline.com/arcgis/rest/services/NGS_Topo_US_2D/MapServer");
        var esriUSTP = new ArcGISTiledMapServiceLayer(nisisProtocol + "://services.arcgisonline.com/arcgis/rest/services/USA_Topo_Maps/MapServer");
        var esriDeLorme = new ArcGISTiledMapServiceLayer(nisisProtocol + "://services.arcgisonline.com/arcgis/rest/services/Specialty/DeLorme_World_Base_Map/MapServer");
        var esriDCM = new ArcGISTiledMapServiceLayer(nisisProtocol + "://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer");

        var esriWorldNavigationChart = new ArcGISTiledMapServiceLayer(nisisProtocol + "://services.arcgisonline.com/arcgis/rest/services/Specialty/World_Navigation_Charts/MapServer");

        //var srl2 = new ArcGISDynamicMapServiceLayer(nisis.url.arcgis.services + 'hurricanes/hurr_basemap/MapServer');

        var mb = nisisProtocol + "://${subDomain}.tile.opencyclemap.org/cycle/${level}/${col}/${row}.png",
        mb2 = nisisProtocol + "://${subDomain}.tiles.mapbox.com/v3/baboyma.ijjb8oca/${level}/${col}/${row}.png",
        mb3 = nisisProtocol + "://${subDomain}.tiles.mapbox.com/v3/baboyma.map-lnn282sl/${level}/${col}/${row}.png",
        mb4 = nisisProtocol + "://a.tile.opencyclemap.org/cycle/9/15/12.png",
        mb5 = nisisProtocol + "://www.arcgis.com/sharing/rest/content/users/NisisDev/items/8379d6c780304fe49acd9f58d7584193";

        var mapbox = new WebTiledLayer(mb3, {
        	id: "dark_canvas",
            "copyright": "Open Cycle Map",
            "id": "Open Cycle Map",
            "subDomains": ["a", "b", "c"]
            ,visible: false
        });
        //mapElements.map.addLayer(mapbox);
        //console.log('Custom basemap: ', srl, srl2);

        var bmLayer1 = new BasemapLayer({
        	type: mb2,
            "copyright": "Open Cycle Map",
            "id": "Open Cycle Map",
            "subDomains": ["a", "b", "c"]
        }),
        basemap1 = new Basemap({
        	id: "shaded_relief_light",
        	thumbnailUrl: "images/basemaps/ShadedReliefLight.png",
        	title: 'World Shaded Relief',//'Shaded Relief (Light)',
        	layers: [esriSRL]
        });

        var bmLayer2 = new BasemapLayer({
        	type: mb2,
            "copyright": "Open Cycle Map",
            "id": "Open Cycle Map",
            "subDomains": ["a", "b", "c"]
        }),
        basemap2 = new Basemap({
        	id: "shaded_relief_dark",
        	thumbnailUrl: "images/basemaps/world_physical.png",
        	title: 'World Physical Map',//'Shaded Relief (Dark)',
        	layers: [esriWPM]//[srDark]
        });

/*		// NISIS-3023
		// FB: duplicate US Topo Map
        var bmLayer3 = new BasemapLayer({
        	type: mb2,
            "copyright": "Open Cycle Map",
            "id": "Open Cycle Map",
            "subDomains": ["a", "b", "c"]
        }),
        basemap3 = new Basemap({
        	id: "dark_canvas",
            thumbnailUrl: "images/basemaps/us_topographic.png",
        	title: 'US Topo Map', //'Dark Canvas'
        	layers: [esriUSTP] //[esriDCM]
        });
*/
     
        basemap3 = new Basemap({
        	id: "world_nav_charts",
            thumbnailUrl: "images/basemaps/world_nav_charts.png",
        	title: 'World Navigation Charts', 
        	layers: [esriWorldNavigationChart] 
        });


        var basemap33 = new Basemap({
        	id: "open_street_map",
            thumbnailUrl: "images/basemaps/us_topographic.png",
        	title: 'Open Street Map',
        	layers: [ new BasemapLayer({
        		type: 'OpenStreetMap'
        	})]
        });

        mapElements.basemaps = new BasemapGallery({
            showArcGISBasemap: true,
			basemaps: [basemap1,basemap2,basemap3],
            map: mapElements.map
        },'basemaps-content');

        mapElements.basemaps.on("error", function(error) {
	        //console.log('Basemap error: ', error);
	    });

		var refLayerOpacity = 1,
			refLayerID = null,
			isRefLayer = false,
			$rtl = $("#refLayerToggler"),
			$checkbox = $rtl.find('input');

	    mapElements.basemaps.on('selection-change', function() {
	    	var bm = mapElements.basemaps.getSelected();

			if (bm.layers[1] && bm.layers[1].isReference){
				$rtl.removeClass('disabled');
				$checkbox.prop('disabled', false);
				bm.layers[1].opacity = refLayerOpacity;
				refLayerID = bm.id;
				isRefLayer = true;
			}else{
				// console.log('not ref')
		        $rtl.addClass('disabled');
				$checkbox.prop('disabled', true);
				refLayerID = null;
				isRefLayer = false;
			}

			mapElements.overview.destroy();
	    	buildOverviewMap();
			//mapbox.show();
	    });

	    //mapElements.basemaps.add(basemap);

		$rtl.on("click","input", function(e){
			console.log('clicked')
			if($rtl.hasClass('.disabled')){
				console.log('disabled');
				e.preventDefault();
				return;
			}
			// respect disabled
			var $this = $(e.currentTarget);
			// $this.text($this.text() === "Hide Labels" ?  "Show Labels" : "Hide Labels");
			refLayerOpacity = refLayerOpacity === 1 ? 0 : 1;
			if(isRefLayer){
				var bmElementID = '#galleryNode_' + refLayerID;
				$(bmElementID).find('a')[0].click();
			}
		})

		mapElements.basemaps.startup();

	};

	//map click event handler: For drawing go to: mapElements.draw.tools.on('draw-end')
	var mapClickHandler = function(evt){
		 console.log('-----------------mapClickHandler 1: ', evt);
		if (mapElements.edit.tools.activate) {
			mapElements.edit.tools.deactivate();
		}
		if (mapElements.map.infoWindow.isShowing){
			mapElements.map.infoWindow.hide();
		}
		//get the mapPoint of click evt
		//console.log("Active Tool: ", mapElements.draw.activeTool);
		if (mapElements.draw.activeTool == 'rectangle' || mapElements.draw.activeTool == 'triangle'
			|| mapElements.draw.activeTool == 'circle'/*  || mapElements.draw.activeTool == 'ellipse'*/) {
			mapElements.draw.geomCenter = evt.mapPoint;
		}
		//clear selected feature style
		if(evt.which === 1){
			//get shape viewmodel
            //require(["app/windows/shapetools"],function(shape){
                //shape.set('styleFeature', null);
                //shape.setStylesForm();
            //});
            //show cluster ==> is done on right click
            //nisis.ui.createMapSubMenu(evt);
		}

		//force a refresh to restyle features
		var layers = [],
			featEditor = nisis.ui.mapview.featureEditor,
			feats = null;

		if (featEditor) {
			if (featEditor.attributeInspector == null) {
				console.log('Map click event: featEditor.attributeInspector is null');
				return;
			}
			else {
				feats = featEditor.attributeInspector._selection;
		 		console.log('-----------------mapClickHandler feats : ', feats);
		 		//try there: feats = nisis.ui.mapview.featureEditor._currentGraphic;
		 		if (feats == null) {
		 			//feats = [nisis.ui.mapview.featureEditor._currentGraphic];
		 			if (evt.graphic!= undefined) {
			 			feats = [evt.graphic];
		 			}
				}
			}
		}

		if (!feats) {
			return;
		}

		$.each(feats, function(i, feat){
			var layer = feat.getLayer();
			console.log("Layer: ", layer);
			if ($.inArray(layers, layer.id) == -1 && !nisis.ui.mapview.map.updating) {
		 		console.log('-----------------mapClickHandler LAYER REFRESH : ', evt);

				layer.refresh();
				layers.push(layer.id);
			}
		});
    };
	//map mouse up event handler
	var mapMouseUpHandler = function(evt) {
		if (mapElements.draw.activeTool === null && evt.which === 3) {
			nisis.ui.createMapSubMenu(evt);
		}
	};
	//extent change handler
	var mapExtentChangeHandler = function(evt){
		//console.log(evt);
		//update numeric scale info
		if (evt.levelChange) {
			mapElements.zoomSlider.set("value", mapElements.map.getZoom());
			//update map rendering
			renderers.updateFeatureSymbols(evt);
		}
		//update Numeric Scale;
		appConfig.updateMapScale(evt);
	};
	//handler for future layer addition
	var layerLoadHandler = function(lyr) {
	    // console.log("A layer has been loaded: ", lyr);
	    //exclude basemap changes
	    if(lyr.layer._basemapGalleryLayerType === 'basemap' || lyr.layer._basemapGalleryLayerType === 'reference'){
	    	return;
	    }
	    //remove layers with errors
	    if(lyr.error){
	    	nisis.ui.displayMessage(lyr.error.message ? lyr.error.message : 'Error occured while loading layer: ' + lyr.layer.id, '', 'error');
	    	nisis.ui.displayMessage('A layer will be removed: ' + lyr.layer.id, '', 'info');
	    	nisis.ui.mapview.map.removeLayer(lyr.layer);
	    	return;
	    }
	    //get layer
		var layer = lyr.layer,
			type = layer.type;

		//get tree node if already built
		var	layerTree = $('#overlays-toggle').data('kendoTreeView'),
    		node = layerTree.dataSource.get(layer.id),
    		layerName = node && node.text ? node.text : layer.name;

		//add layer to legend
		mapElements.legendLayerInfos.push({
			layer: layer
			,title: layerName
		});
		// console.log(mapElements.legendLayerInfos)
//		console.log('layer.....', layer);
		console.log('refreshing map legend after loading layer.....', layer);
		nisis.ui.mapview.util.refreshMapLegend();

		//add layer to attr inspector
		if (type && type === 'Feature Layer' && !node) {
			console.log('Adding Feature layer to Attr. Inspector: ', lyr.layer.name);

			var options = {
				featureLayer: lyr.layer,
				showObjectID: false,
				showGlobalID: false,
				isEditable: false
				,fieldInfos: false
			};
			//add the layer to attributes inspector
			/*mapElements.featureEditor.settings.layers.push(lyr.layer);
			mapElements.featureEditor.settings.layerInfos.push(options);
			mapElements.featureEditor.attributeInspector.refresh();*/


             var infoTemp = new PopupTemplate();
			//var infoTemp = new InfoTemplate();
			infoTemp.setTitle(lyr.layer.name);
			infoTemp.setContent(lyr.layer.name);
			//lyr.layer.setInfoTemplate(infoTemp);
			lyr.layer.on('click',function(evt){
				//console.log('AGO Layer Click: ', arguments);
				evt.preventDefault();
				evt.stopPropagation();
				console.log('lyr layer click prevent')

				var selSymbols = {
					point: nisis.ui.mapview.map.getLayer('Point_Features').getSelectionSymbol(),
					polyline: nisis.ui.mapview.map.getLayer('Line_Features').getSelectionSymbol(),
					polygon: nisis.ui.mapview.map.getLayer('Polygon_Features').getSelectionSymbol()
				}

				var query = new Query();
				query.geometry = evt.graphic.geometry;

				var symbol = layer.renderer.getSymbol(evt.graphic);
				evt.graphic.setSymbol(selSymbols[evt.graphic.geometry.type]);
				nisis.ui.displayAgoAttributes(lyr.layer, evt, symbol);
			});

			//add new layer to layers tree
			layers.addTreeNode(lyr);
			//check if layer already exist
			if (!mapElements.layers[layer.id]) {
				mapElements.layers[layer.id] = layer;
			}
			//zoom to layer extent;
			//nisis.ui.mapview.zoomToLayerExtent(lyr.layer);
			//add right click event
			layer.on('mouse-up', nisis.ui.createFeatureSubMenu);
			//add double click event
			//layer.on('dbl-click', nisis.ui.mapview.util.activateEditTools);
		}
		else if (type && type === 'Feature Layer' && node) {
			//add new feature layer to feature editor
			mapElements.editorLayerInfos.push({
				featureLayer: layer
				,showObjectID: false
				,showGlobalID: false
				,isEditable: false
				,fieldInfos: false
				,showDeleteButton: false
			});

			//add snapping for build Feature Editor
			mapElements.snapLayerInfos.push({
					layer: layer,
					snapToPoint: true,
					snapToVertix: true,
					snapToEdge: true
			});

			//build feature editor
			buildFeatureEditor();

			//add visibility change
			layer.on('visibility-change', function(evt){
				var checked = evt.visible,
					id = evt.target.id;

				layers.updateTreeNode(id,checked);
			});

			//add right click event
			layer.on('mouse-up', nisis.ui.createFeatureSubMenu);

			//bind mouse-over/out of feature

			// NISIS-2945 unconstrain from just nisisto
			// if (layer.group == "nisisto") {
			layer.on('mouse-over', function(evt) {
				var lyr = evt.graphic.getLayer(),
					attr = evt.graphic.attributes;


                if (lyr && lyr.group !== 'dot' && lyr.group !== 'pipelines'){
				    if(!lyr || (lyr && lyr.group !== 'nisisto') ){
				    	if(lyr.id.toLowerCase() !== 'tracks') {
					    	return;
					    }
				    }
				}

				nisis.ui.datablockTimeOut = setTimeout(function(){
					nisis.ui.displayDataBlocks(lyr, attr, evt);
				}, 500);
			});

			layer.on('mouse-out', function(evt) {
				clearTimeout(nisis.ui.datablockTimeOut);
				nisis.ui.hideDataBlocks();
			});
			// }
		}

		//special click-on-layer for DOT and Pipelines
		if (layer.group && (layer.group === 'dot'||layer.group === 'pipelines')) {

                //NISIS-2945
                layer.on('mouse-over', function(evt){
					var lyr = evt.graphic.getLayer(),
						attr = evt.graphic.attributes;

					// NISIS-2945 what's this doing here?
     				// if (lyr && lyr.group !== 'dot' && lyr.group !== 'pipelines'){
					//     if(!lyr || (lyr && lyr.group !== 'nisisto') ){
					//     	if(lyr.id.toLowerCase() !== 'tracks') {
					// 	    	return;
					// 	    }
					//     }
					// }

					nisis.ui.datablockTimeOut = setTimeout(function(){
						nisis.ui.displayDataBlocks(lyr, attr, evt);
					}, 500);

                });

				layer.on('mouse-out', function(evt) {
					clearTimeout(nisis.ui.datablockTimeOut);
					nisis.ui.hideDataBlocks();
				});

                // console.log('Events and ntad_amtrak layer: adding special onclick event for a ntad_amtrak');
                //irena - make sure we disable click propogation on tfr layer
                layer.on('click',function(evt){
                    //where does it get propogated to esri to do the call
                    evt.stopPropagation();
                    //set screenpoint
                    var screenPoint = evt.screenPoint;
                    $.ajax({
                        url:"api/",
                        type:"POST",
                        data: {
                            action: "get_profiles_dot",
                            layerid: layer.id,
                            objectid: evt.graphic.attributes.OBJECTID,
                            userid: Number(nisis.user.userid)
                        },
                        success: function(data, status, req) {
                            var resp = JSON.parse(data);
                            //console.log(resp);
                            var profileData = JSON.parse(resp["profile"][0]["PROFILE"])["PROFILE"];
                            //console.log(JSON.stringify(profileData));
                            if(profileData["METADATA"].length>0){
                            	var infobox = new DOTObjectProfile(profileData, evt);
                            }else{
                            	nisis.ui.displayMessage(profileData["MSG"], 'error');
                            }
                        },
                        error: function(req, status, err) {
                            console.error("ERROR in ajax call to get DOT map objects");
                        }
                    });
                });
        }
        else if (layer.id && (layer.id === 'airports_pr') || (layer.id === 'airports_others') || (layer.id === 'airports_pu') || (layer.id === 'airports_military') || (layer.id === 'atm') || (layer.id === 'ans1') || (layer.id === 'ans2') || (layer.id === 'osf') || (layer.id === 'tof')) {
                trackedObjectsLayerClickHandlerHookup(layer);
        }

	};
	 //special click-on-layer for Airports Private, Public, etc.... AND ATM, ANS, TOF, OSF
	var trackedObjectsLayerClickHandlerHookup = function(layer){
		// console.log('Events and ntad_amtrak layer: adding special onclick event for a airports layer');
        layer.on('click',function(evt){
            //where does it get propogated to esri to do the call
            evt.stopPropagation();
            //Bring Up NISIS Tracked Object Profile
            nisis.ui.mapview.util.bringUpNISISTOProfile(evt);
        });
	};

	//handler for layer update //==> Should take care to renderers updates
	var layerUpdateHandler = function(layers){
		$.each(layers,function(i,layer){
			if (layer.success && layer.layer.type === "Feature Layer" &&
				(layer.layer.id != "Polygon_Features" && layer.layer.id != "Line_Features" && layer.layer.id != "Point_Features" && layer.layer.id != "text_features")
				) {
				layer.layer.on('update-end', function(evt){
				    // console.log("+++++++++++++++ SHALL WE REDRAW LAYER? "+ layer.layer.id + " :"+layer.layer.redraw);
				    // KOFI.  Investigate later what the hell this does for other non-FM layers.
				    if(layer.layer.redraw){
				       layer.layer.redraw();
				    }
				});
			}
		});
	};
	//layer add result call back function
	var setupFeaturesFunctionalities = function(results) {
	  //  console.log('Layer loaded: ',results);
		var layers = null
			target = results.target;

		if(results.length){
			layers = results;
		} else {
			layers = results.layers;
		}

       // console.log('setupFeaturesFunctionalities: Layer loaded - layers: ',layers);
		//nisis.log('Initial Layers loaded: ', layers.length);
		//on layer added event ==> this event should be register after the map is loaded.
		mapElements.map.on('layer-add-result', layerLoadHandler);
		//register events to feature layers
		try {
			addEventsToFeatureLayers(layers);
		} catch (err){
			console.log("Layers events settings failed: ", err);
		}
		//setup layers refresh interval
		try {
			setRefreshIntervals(layers);
		} catch (err){
			console.log("Layers refresh interval settings failed: ", err);
		}
		//redraw feature layers at the end of content update
		try {
			layerUpdateHandler(layers);
		} catch (err){
			console.log("Layers update handler settings failed: ", err);
		}
		//create map legend
		try {
			createMapLegend(layers);
		} catch (err){
			console.log("Map Legend failed: ", err);
		}
		//create feature template picker
		try {
			createFeaturesTemplatePicker(layers);
		} catch (err){
			console.log("Feature Templates failed: ", err);
		}
		//create features editor ==> this is blocking the rest of the function
		try {
			createFeatureEditor(layers);
		} catch (err){
			console.log("Layers editor settings failed: ", err);
		}
		//enable snapping on map
		mapElements.snappingMgr = mapElements.map.enableSnapping({
			tolerance: 15,
			alwaysSnap: mapElements.alwaysSnap
		});

        //find layers with layer.id =  === 'airports_pu'
        //find airports_pu layer - its getting loaded by default
        //and hook up special handler
        $.each(layers,function(idx,lyr){
			if (lyr.layer.id === 'airports_pu') {
             	trackedObjectsLayerClickHandlerHookup(lyr.layer);
        	}
		});



        //irena - add custom popup to InfoWindow
        nisis.ui.mapview.map.infoWindow.showClosestFirst=function(b) {
			console.log("show closest first - irena ....")
		    //esri\geometry\mathUtils.js is a ./geometry/mathUtils
		    //make this only for polygon features
			var c = this.features;
                            if (c && c.length) {
                                if (1 < c.length) {
                                    var e, f = Infinity,
                                        g = -1,
                                        m, h = mathUtils.getLength, //dont know what a is... maybe a symbol.?
                                        k, n = b.spatialReference,
                                        l, r;
                                    b = b.normalize();
                                    //irena
                                    console.log("showClosestFirst: ....h is ...", h);
                                    var layerId = c[0]._layer.id;
                                    var layer =  c[0]._layer;
                                    if (layerId == 'Polygon_Features') {


                                    	//clear current selection if its not already on the correct element that is high ordered
          //                           	var feat = nisis.ui.mapview.featureEditor._currentGraphic;
										// if (feat) {
										// 	if (feat._graphicsLayer) {
										// 		feat._graphicsLayer.clearSelection();
										// 	}
										// 	nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.deactivate();
										// }
										//done clearing current selection
                                    	//instead of closest first, rearrange the order (if that was not already rearranged) and do the last (or first) in order first
                                    	this.select(0);

                                    	/// should not need all of these
                                    	var setcurrFeat = c[0];

                                    	//toolbar is not synced in. Thats default esri behaviour
                                    	//setcurrFeat._graphicsLayer.clearSelection();

                                    	// nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.deactivate();

                                    	// // nisis.ui.mapview.featureEditor.attributeInspector._currentFeature = setcurrFeat;
                                    	// // nisis.ui.mapview.featureEditor._currentGraphic = setcurrFeat;

                                    	// nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.activate(Edit.MOVE | Edit.SCALE, setcurrFeat);

                                    	//  var currEditState = nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.getCurrentState();
                                    	// nisis.ui.mapview.featureEditor.drawingToolbar.editToolbar.refresh();


                                    	//select that feature at index 0.
                                    	//nisis.ui.mapview.featureEditor._selectionHelper.selectFeatures
                                    	//nisis.ui.mapview.featureEditor.

                                    	//layer.selectFeatures([graphic]);

                                    	// var selFeat = this.features[0];
                                    	// mapElements.featureEditor.drawingToolbar.editToolbar.activate(selFeat.geometry);
                                    	// var layer = c[0]._layer;//nisis.ui.mapview.map.getLayer('Polygon_Features')

                                    	//nisis.ui.mapview.featureEditor._currentGraphic=features[0];
                                    	//also put the editor to the selected feature
                                    }
                                    else { //irena - need to call native jsapi showClosestFirst.... h = a.getLength,
                                    // native jsapi.js call
                                    for (e = c.length - 1; 0 <= e; e--)
                                        if (m = c[e].geometry) {
                                            l = m.spatialReference;
                                            k = 0;
                                            try {
                                                r = "point" === m.type ? m : m.getExtent().getCenter(), r = r.normalize(), n && (l && !n.equals(l) && n._canProject(l)) && (r = n.isWebMercator() ? d.geographicToWebMercator(r) : d.webMercatorToGeographic(r)), k = h(b, r)
                                            } catch (p) {}
                                            0 < k && k < f && (f = k, g = e)
                                        }
                                    0 < g && (c.splice(0, 0, c.splice(g, 1)[0]), this.select(0))
                                	}
                                }
                            } else this.deferreds && (this._marked = b)

		};

		//hide app spinner
		setTimeout(function() {
			nisis.ui.stopProcess();
		}, 1500);

		//check layers credentials every 30min ==> 1000 * 60 * 30 = 1800000
		setInterval(function() {
			nisis.user.checkCredentials();
		}, 1800000);
	};

	//register events to feature layers
	var addEventsToFeatureLayers = function(results){
		//console.log('addEventsToFeatureLayers... ');
		$.each(results, function(index, layer){
			//console.log(layer.layer.type);
            if (layer.layer.group && (layer.layer.group === 'dot'||layer.layer.group === 'pipelines')) {
                // console.log('Events and ntad_amtrak layer: adding special onclick event for a ntad_amtrak');
                //irena - make sure we disable click propogation on tfr layer
                layer.layer.on('click',function(evt){
                    //where does it get propogated to esri to do the call
                    evt.stopPropagation();
                    //set screenpoint
                    var screenPoint = evt.screenPoint;
                    $.ajax({
                        url:"api/",
                        type:"POST",
                        data: {
                            action: "get_profiles_dot",
                            layerid: layer.layer.id,
                            objectid: evt.graphic.attributes.OBJECTID,
                            userid: Number(nisis.user.userid)
                        },
                        success: function(data, status, req) {
                            var resp = JSON.parse(data);
                            //console.log(resp);
                            var profileData = JSON.parse(resp["profile"][0]["PROFILE"])["PROFILE"];
                            //console.log(JSON.stringify(profileData));
                            if(profileData["METADATA"].length>0){
                            	var infobox = new DOTObjectProfile(profileData, evt);
                            }else{
                            	nisis.ui.displayMessage(profileData["MSG"], 'error');
                            }
                        },
                        error: function(req, status, err) {
                            console.error("ERROR in ajax call to get DOT map objects");
                        }
                    });
                });
            }

			if (layer.layer.type == 'Feature Layer') {
				//array of layers to be used in snapping manager
				mapElements.snapLayerInfos.push({
					layer: layer.layer,
					snapToPoint: true,
					snapToVertix: true,
					snapToEdge: true
				});
				//add click event on feature layer
				layer.layer.on('mouse-up', function(evt) {
					if (evt.which === 3) {
						nisis.ui.createFeatureSubMenu(evt);
					}
				});
				//add mouse over/out event
				layer.layer.on('mouse-over', function(evt) {

				    var lyr = evt.graphic.getLayer();
				    // console.log(lyr);

				    if (lyr && lyr.group !== 'dot' && lyr.group !== 'pipelines'){
					    if(!lyr || (lyr && lyr.group !== 'nisisto') ){
					    	if(lyr.id.toLowerCase() !== 'tracks') {
						    	return;
						    }
					    }
					}

				    //target attributes
				    var attr = evt.graphic.attributes,
				    	sym = lyr.renderer.getSymbol(evt.graphic),
				    	size = 15;

				    if (sym && sym.width) {
				    	size = sym.width;

				    } else if (sym && sym.size) {
				    	size = sym.size;
				    }

				    //get feature count ==> this slow down the data block
				    /*if ( !$('#cluster-div').is(':visible') ) {
					    //count the number of feature at that location
					    var q = new Query(),
					    	qt = new QueryTask(lyr.url),
					    	w = $('.esriScalebarRuler').width(),
					    	r = appConfig.scaleValue * size / w,
					    	su = appConfig.scaleUnit,
					    	radius = nisis.ui.mapview.util.convertUnits(r, su, 'mi'),

					    	circle = new Circle(evt.mapPoint, {
					    		geodesic: true,
					    		radius: radius,
					    		radiusUnit: esri.Units.MILES
					    	});

					    q.geometry = circle;
					    lyr.queryCount(q)
					    .then(function(res){
					    	//console.log('Data block count: ', arguments);
					    	nisis.ui.displayDataBlocks(lyr, attr, evt, res);
					    },function(err){
					    	//console.log('Data block count error: ', arguments);
					    	nisis.ui.displayDataBlocks(lyr, attr, evt);
					    });
					} else {
					    //build data blocks
					    nisis.ui.displayDataBlocks(lyr, attr, evt);
					}*/

					// For now using the new function to display the datablocks for DOT objects, we will keep only one call here whatever works better
					nisis.ui.datablockTimeOut = setTimeout(function(){
	 				// 	if (lyr && (lyr.group === 'dot' || lyr.group === 'pipelines')){
	 				// 		var layerid = lyr.id;
	 				// 		var pkfield = nisis.layersPks[layerid].table_pk;
	 				// 		// console.log("attr", attr, pkfield);
	 				// 		var pkid = attr[pkfield];
					// 	nisis.ui.displayDOTDataBlocks(layerid, pkid, evt);
					// }else{
						nisis.ui.displayDataBlocks(lyr, attr, evt);
					// }
					}, 500);
				});

				layer.layer.on('mouse-out', function(evt) {
					clearTimeout(nisis.ui.datablockTimeOut);
					nisis.ui.hideDataBlocks();
				});


			}
			//add visibility change
			layer.layer.on('visibility-change', function(evt){
				//console.log(evt);
				var checked = evt.visible,
				id = evt.target.id;
				//nisis.ui.changeLayerVisibility(layer.layer.id, checked);
				layers.updateTreeNode(id,checked);
			});
		});
	};
	//set refresh intervals
	var setRefreshIntervals = function(layers){
	    var refreshLayers = ['Tracks'];
		$.each(layers,function(idx,lyr){
			if(lyr.success && $.inArray(lyr.layer.id, refreshLayers) !== -1 && lyr.layer.success){
				//console.log(lyr.layer);
				setInterval(function(){
					//refresh only when layer is visible
					if(nisis.ui.mapview.layers[lyr.layer.id].visible){
						nisis.ui.mapview.layers[lyr.layer.id].refresh();
					}
				},5000);
			}
		});
	};
	//create map legend
	var createMapLegend = function(results) {
		//array of layers to be used in lagend and TOC
		mapElements.legendLayerInfos = [];
		$.each(results, function(index,layer){
			//console.log('Layer: ',index, layer);
			if(layer.success && layer.layer.id !== 'text_features'){
				mapElements.legendLayerInfos.push({
					layer: layer.layer,
					title: layer.layer.name
				});
			}
			//console.log('layer.....', layer);
		});

		//map legend
		mapElements.legend = new Legend({
            map: mapElements.map
            ,layerInfos: mapElements.legendLayerInfos
            ,respectCurrentMapScale: true
            ,autoUpdate: true,
        }, "legend-content");

        // var layerInfosTitle = mapElements.legendLayerInfos;
        // for(var i in layerInfosTitle){
        // 	if(layerInfosTitle[i].title === "ANS Systems"){
        // 		console.log("ans", layerInfosTitle[i]);
        // 	}
        // }

		try {
			mapElements.legend.startup();
    	} catch(err) {
    		console.log('Legend error: ',err);
    		//nisis.ui.displayMessage("Legend error: " + err.message, "", "error");
    	}

    	//also do something when map legend refreshes, preserve the collapses!
    	$(document).on('click','.esriLegendServiceLabel',function(){
		    $header = $(this);

			//getting the next element
			$service = $header.closest(".esriLegendService");
			$content = $service.find("div:first");

			//put id into array
			//open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
			$content.slideToggle(500, function () {
				$header.toggleClass('collapsed');
			});
		});




	};
	//create features layers template picker
	var createFeaturesTemplatePicker = function(results) {
		//console.log("createFeaturesTemplatePicker?");
		//template layers
		mapElements.templateLayers = [];
		$.each(results.reverse(), function(index, layer){
			//console.log('Templates Layer #'+index, layer);
			if (layer.layer && layer.success) {
				if (layer.layer.type == 'Feature Layer'){
					var isEditable = false;
					if(layer.layer.capabilities.indexOf('Editing') != -1) {
						isEditable = true;
					}
					//only user features can be created
					if (isEditable && layer.layer.group === 'user') {
						mapElements.templateLayers.push(layer.layer);
					}
					//Response Type
					//NISIS-2754 KOFI HONU
					// if(layer.layer.id === 'teams'){
					//     mapElements.templateLayers.push(layer.layer);
					// }
				}
			} else {
				console.log('Templates Layer #'+index + ' - ' + ' is not valid.');
			}
		});
		//feature layer template picker
		mapElements.templatePicker = new TemplatePicker({
			featureLayers: mapElements.templateLayers,
			//items: templates,
			grouping: true,
			columns: 10,
			//rows: 3,
			showTooltip: true,
			emptyMessage: 'Feature layers have not been loaded.'
			//,useLegend: true
			,style: {width:'auto', height:'250px', color:'inherit'}
		}, 'featTemplates');
		mapElements.templatePicker.startup();
		//register events
		mapElements.templatePicker.on('selection-change',function(){
			console.log("selection-change, may be reverse selection here?");
			if (mapElements.templatePicker.getSelected()) {
				mapElements.activeTemplate = mapElements.templatePicker.getSelected();
				//disable navigation
				mapElements.navTools.deactivate();
				mapElements.map.disablePan();
				$('#app').trigger('nav-change');
			} else {
				mapElements.navTools.activate('pan');
				$('#app').trigger('nav-change','nav-pan');
			}
		});
	};
	//create feature editor
	var createFeatureEditor = function(results) {
		//editor layers infos
		mapElements.editorLayerInfos = [];
		//reg a reference of the treeview
		var treeview = $("#overlays-toggle").data("kendoTreeView");
		//mapElements.editorLayerInfos = dojo.map(results, function(layer,index){
		$.each(results, function(index, layer){

			if (layer && layer.success) {

				if (layer.layer && layer.layer.type && layer.layer.type == 'Feature Layer') {
					var group = layer.layer.group,
						id = layer.layer.id,
						fieldInfo = layer.layer.apprenderer ? layer.layer.apprenderer : null;

					var options = {
						featureLayer: layer.layer
						,showObjectID: false
						,showGlobalID: false
						,isEditable: false
						,fieldInfos: false
						//TODO - gnewman remove delete buttons! but this probably affects other stuff 5-29-2015
						//,showDeleteButton: false
					};

					//Validation
					var validName = new dijit.form.ValidationTextBox({
			          	regExp : /~|`|!|@|#|\$|%|\^|&|\*|\(|\)|_|\+|\[|\]|\|;|'|,|\.|,|\/|<|>|\?|:|"|{|}|\|/,
			          	required : false,
			          	promptMessage: "Enter a shape name with valid characters",
			          	invalidMessage : "Your input contains illegal characters"
				  	});

					//costum fields
					if (fieldInfo && fieldsInfos[fieldInfo]) {
						options.fieldInfos = fieldsInfos[fieldInfo];
						if (layer.layer.id === "Polygon_Features"){
							console.log('Fields: ', options.fieldInfos);
						}
					}


					//only user features will be editable
					if ( layer.layer.group && layer.layer.group === 'user' ) {
						//disable editing for other group features
						options.isEditable = true;
						options.showDeleteButton = true;

					} else if ( layer.layer.group && layer.layer.group.indexOf('group_') === 0 ) {
						//owner or admin can edit
						if ( group === 'user' || group === 'group_0' ) {
							//enable attributes editing
							options.isEditable = true;
							options.showDeleteButton = true;
						} else {
							//disable editing for other group features
							options.isEditable = false;
							options.showDeleteButton = false;
						}

					} else if (layer.layer.group && layer.layer.group === 'tfr') {

						options.isEditable = true;
						options.showDeleteButton = true;
						//CC - Removing the delete button from the feature editor for the tfr polygons layer. Shapes will be deleted by the button located in the shapes grid.
						if (id === 'tfr_polygon_features'){
							options.showDeleteButton = false;
						}

					} else if ( layer.layer.group && layer.layer.group === 'nisisto' ) {

						options.showDeleteButton = false;

						//NISIS-2754 KOFI HONU
						// if( layer.layer.id === 'teams' && nisis.user.isAdmin) {
						// 	options.isEditable = true;
						// 	options.showDeleteButton = true;
						// }
					}
					//
					mapElements.editorLayerInfos.push(options);
				}

			} else {
				console.log('Editor Layer #'+index + ' - ' + layer.layer.id + ': has some errors.');

				var lNode = treeview.dataSource.get(layer.layer.id),
					tNode = null;

				if (lNode) {
					tNode = treeview.findByUid(lNode.uid);
				}
				//desable layer node
				if (tNode) {
					treeview.enable(tNode, false);
				}
			}
		});
		//build feature editor
		buildFeatureEditor();
	};
	//update feature editor
	var buildFeatureEditor = function() {
		//destroy any existing editor
		if (mapElements.featureEditor) {
			mapElements.featureEditor.destroy();

			$("#fEditor").append($("<div/>", {
				id: "featEditor",
				class: "marg clear of-h"
			}));
		}

		//feature editor widget
		mapElements.featureEditor = new Editor({
			settings: {
				map: mapElements.map,
				templatePicker: mapElements.templatePicker,
				geometryService: mapElements.geometryService,
				layerInfos: mapElements.editorLayerInfos,
				toolbarVisible: true,
				enableUndoRedo: true,
				createOptions: {
					polylineDrawTools: [
						Editor.CREATE_TOOL_POLYLINE,
						Editor.CREATE_TOOL_FREEHAND_POLYLINE
					],
					polygonDrawTools: [
						Editor.CREATE_TOOL_CIRCLE,
						Editor.CREATE_TOOL_ELLIPSE,
						Editor.CREATE_TOOL_TRIANGLE,
						Editor.CREATE_TOOL_RECTANGLE,
						Editor.CREATE_TOOL_POLYGON,
						Editor.CREATE_TOOL_AUTOCOMPLETE,
						Editor.CREATE_TOOL_FREEHAND_POLYGON
					]
				},
				toolbarOptions: {
					mergeVisible: true,
					cutVisible: true,
					reshapeVisible: true
				}
			}
		}, 'featEditor');

		//start the widget
		try {
			mapElements.featureEditor.startup();
			//not using attribute Inspector for the Popup window. Wire infoWindow behaviour
			if (mapElements.map.infoWindow) {
				//mapElements.map.infoWindow.on('next', updatePopupWindow);

				mapElements.map.infoWindow.on('maximize', setPopupWindowMaximizedPosition);

				mapElements.map.infoWindow.on('selection-change', updatePopupWindow);	//same this as wire the next - but better

				//on set-features, reorder the features so the window will page thru in correct order 2328
				mapElements.map.infoWindow.on("set-features", function(evt) {

					console.log("OrderInfoWindow! - set-features event for ..",nisis.ui.mapview.map.infoWindow.features);

					if (nisis.ui.mapview.map.infoWindow.features && nisis.ui.mapview.map.infoWindow.features.length>1) {
						//this is only applicable to Polygon_Features features
					    var layerId = nisis.ui.mapview.map.infoWindow.features[0]._layer.id;
					    if (layerId == 'Polygon_Features') {
					 		console.log("features before sort", nisis.ui.mapview.map.infoWindow.features);
					 		for (var i = 0; i < nisis.ui.mapview.map.infoWindow.features.length; i++) {
							    console.log(nisis.ui.mapview.map.infoWindow.features[i].attributes.NAME);
							    //Do something
							}

						    //reorder the array based on graphics[i].attributes.DISPLAY_ORDER
				            function compare(a,b) {
				            	console.log("features compare");
				                var aaObj = features.getItemByObjectId(a.attributes.OBJECTID, 'polygon');
				                var bbObj = features.getItemByObjectId(b.attributes.OBJECTID, 'polygon');

				                if (aaObj==null || bbObj==null) {
				                	return 0;
				                }

				                var aa = aaObj.STYLE.DISPLAY_ORDER;
				                var bb = bbObj.STYLE.DISPLAY_ORDER;

				                var ax = (aa == "" ? 0 : parseInt(aa));
				                var bx = (bb == "" ? 0 : parseInt(bb));

				                //might flip the sorting

				                if (ax < bx)
				                    return 1;
				                if (ax > bx)
				                    return -1;
				                return 0;
				            }

				            nisis.ui.mapview.map.infoWindow.features.sort(compare);

				            console.log("features after sort", nisis.ui.mapview.map.infoWindow.features);
				            for (var i = 0; i < nisis.ui.mapview.map.infoWindow.features.length; i++) {
							    console.log(nisis.ui.mapview.map.infoWindow.features[i].attributes.NAME);
							}
				        }
				    }
			  	});
			}
		} catch (err) {
			console.log(err);
		}
		//add intersection tool and snapping to editor toolbar
		try {
			extendEditorToolbar();
		} catch (err) {
			console.log(err);
		}

	};

	//add intersection tool and snapping to editor toolbar
	var extendEditorToolbar = function(){
		if(!mapElements.featureEditor){
			return;
		}
		var snap = new CheckBox({
			id: 'lockSnapping',
			name: 'lockSnapping',
			label: 'Lock Snapping',
			title: "Check to 'Lock Snapping' or hold CTRL KEY to 'Enable snapping'",
			checked: false,
			showLabel: true,
			onChange: function(evt){
				//console.log(evt);
				if(evt){
					mapElements.alwaysSnap = true;
					mapElements.map.disableSnapping();
					mapElements.snappingMgr = mapElements.map.enableSnapping({
						tolerance: 15,
						alwaysSnap: mapElements.alwaysSnap
					});
					mapElements.snappingMgr.setLayerInfos(mapElements.snapLayerInfos);
				} else {
					mapElements.alwaysSnap = false;
					mapElements.map.disableSnapping();
					mapElements.map.enableSnapping({
						tolerance: 15,
						alwaysSnap: mapElements.alwaysSnap
					});
				}
			}
		});
		//snap.startup();
		var snapLabel = domConstruct.create('label',{
			'for':'lockSnapping',
			'innerHTML': ' Lock Snapping'
		});
		var btn = new Button({
			id: 'btnSnappingLabel',
			label: 'Lock Snapping',
			showLabel: true,
			disabled: false,
			style: "margin-left:0,cursor:default"
		});
		var editor = dojo.query('.esriDrawingToolbar',mapElements.featureEditor.domNode)[0];
		ftEditor = dijit.byId(editor.id);
		ftEditor.addChild(new dijit.ToolbarSeparator());
		ftEditor.addChild(snap);
		var cb = dijit.byId('lockSnapping');
		dojo.place(btn.domNode, cb.domNode, "after");
		//polygon cut
		var btnPolygonCut = new Button({
			id: 'btnPolygonCut',
			iconClass: 'btnPolygonCut',
			label: 'Cut with polygon',
			title: 'Cut out features',
			showLabel: false,
			style: "margin-left:5px;",
			onClick: function(evt) {
				//console.log('Cut button: ', evt);
				var active = dojo.hasClass(this, 'active'),
					extActive = dojo.hasClass("btnFeaturesExtract", 'active');

				if (active) {
					shapetools.set('featureOpsTool', "");
					shapetools.set('featureOpsActive', false);
				} else {
					shapetools.set('featureOpsTool', 'cut');
					shapetools.set('featureOpsActive', true);
				}
			}
		});
		var btnCut = dijit.byId('btnFeatureCut');
		dojo.place(btnPolygonCut.domNode, btnCut.domNode, "after");
		//intersect tool
		var btnIntersect = new Button({
			id: 'btnFeaturesIntersection',
			iconClass: 'btnFeatsIntersect',
			label: 'Intersection',
			title: 'Intersection',
			showLabel: false,
			style: "margin-left:5px;",
			onClick: function(evt){
				//console.log(evt);
				var selFeats = mapElements.featureEditor.drawingToolbar._selectedFeatures, app = appConfig.app;
				if (selFeats == null) {
					return;
				}

				var	len = selFeats.length;

				if(len && len > 1){
					//exclude point shapes
					var feats = [];
					$.each(selFeats,function(i,f){
						var g = f.getLayer().group;
						if (f.geometry.type != 'point') {
							feats.push(f);
						}
					});

					if (feats.length < 1) {
						//console.log('Intersection features: ', feats);
						var msg = 'The intersection tool requires at least 2 valid features (Lines and/or Polygons).';
						nisis.ui.displayMessage(msg, '', 'error');
						return;
					}

					var layer = feats[0].getLayer(),
						symbol = feats[0].symbol,
						geoms = [], geom = null;

					$.each(feats,function(i,feat){
						var group = feat.getLayer().group;

						if (i===0) {
							geom = feat.geometry;
						} else {
							geoms.push(feat.geometry);
						}
					});
					//run the intersection
					var intersect = nisis.ui.mapview.util.intersectGeometries(geoms,geom);
					intersect.then(
					function(resp) {
						//console.log(resp);
						var geom;
						if (resp && resp.length > 0) {
							geom = resp[0];

                            //check if we got valid response - are their rings on the resulting intersecting geometry?
                            if (!(geom && geom.rings && geom.rings.length>0)) {
                            	var msg = 'The shapes do not intersect';
								nisis.ui.displayMessage(msg, 'error');
								//clear the selection
								$('#btnClearSelection').trigger('click');
								return;
                            }

							//reset user group to none
							var attr = {
								NAME: 'Intersection Result',
								REF_NUMBER: "",
								DESCRIPTION: 'Feature copied from ',
								SUP_NOTES: '',
								SYMBOL: 0,
								USER_GROUP: 9999
							};

							if(geom.type == 'polygon'){
								attr.FS_DESIGN = 1;
								attr.IWA_DESIGN = 0
							}
							//new graphic
							var graphic = new Graphic(geom,symbol,attr),
								graphic2 = new Graphic(geom,symbol,attr);


							//use this function for adding features
							mapElements.util.addFeatureToUserLayers(graphic2, layer, 'Intersection')
							.then(function(newFeature){
								console.log("addFeatureToUserLayers - intersection");
								if (!( newFeature.length > 0 && newFeature[0].success )) {
									var msg2 = 'The intersection operation failed. Please try again.';
									nisis.ui.displayMessage(msg2, 'error');
								}
							},function(e){
								var error = e.message ? e.message : e;
								nisis.ui.displayMessage(error, 'error');
								$('.dijitProgressBar.dijitProgressBarEmpty.dijitProgressBarIndeterminate.progressBar').hide();
							});

						} else {
							var msg = 'The intersection operation was not executed successfully.';
							nisis.ui.displayMessage(msg, 'error');
						}
					},
					function(error){
						nisis.ui.displayMessage(error, 'error');
					});
				} else {
					var msg = "Intersection requires at least 2 features.";
					nisis.ui.displayMessage(msg, '','error');
				}
			}
		});
		//union
		var btnUnion = dijit.byId('btnFeatureUnion');
		dojo.place(btnIntersect.domNode, btnUnion.domNode, "after");
		//intersect tool
		var btnExtract = new Button({
			id: 'btnFeaturesExtract',
			iconClass: 'btnFeatsExtract',
			label: 'Extract',
			title: 'Extract Features',
			showLabel: false,
			style: "margin-left:5px;",
			onClick: function(evt){
				var active = dojo.hasClass(this, 'active'),
					cutActive = dojo.hasClass("btnPolygonCut", 'active');

				if (active) {
					dojo.removeClass(this, 'active');
					shapetools.set('featureOpsTool', null);
					shapetools.set('featureOpsActive', false);
				} else {
					dojo.addClass(this, 'active');
					shapetools.set('featureOpsTool', 'extract');
					shapetools.set('featureOpsActive', true);
				}

				if (cutActive) {
					dojo.removeClass("btnPolygonCut", 'active');
				}
			}
		});

		dojo.place(btnExtract.domNode, btnUnion.domNode, "after");

		//New Delete Button. We need new delete button instead of dorky ESRI btnDelete2 cuz we need to call our remove feature function
		//that would take care of the priveleges while deleting. Some users might not have priveleges to delete certain features. It will warn if
		//feature has been shared and deletion will unshare the feature with others
		var btnDelete = new Button({
			id: 'btnFeaturesDelete',
			iconClass: 'toolbarIcon deleteFeatureIcon',
			label: 'Delete',
			title: 'Delete',
			showLabel: false,
			style: "margin-left:5px;",
			onClick: function(evt){
				//who is selected
				var selFeats = mapElements.featureEditor.drawingToolbar._selectedFeatures;
				//loop thru and delete selected objects
				$.each(selFeats, function(i, selFeat) {
					nisis.ui.mapview.util.removeFeatureFromUserLayers(selFeat, true);
				});
			}
		});
		var btnIrenaDelete = dijit.byId('btnFeaturesDelete');
		var btnOriginalDelete = dijit.byId('btnDelete2');
		dojo.place(btnIrenaDelete.domNode, btnOriginalDelete.domNode, "after");
		dojo.destroy(btnOriginalDelete.domNode);
	};

	//create a connection to arcgis online
	var createAgsOnlineLink = function(){
		//console.log(esriPortal);
		//create a portal
		mapElements.agsonline = new esriPortal.Portal(nisis.url.arcgis.portal);

		if(mapElements.agsonline) {
			mapElements.agsonline.on('load',function(loaded){
				console.log('ArcGIS Online link has been established: ', loaded);
			});
		} else {
			console.log('ArcGIS Online link has not been established');
		}
	};

	//expose only public prop/func
	return mapElements;
})();
});
