$('.esriPopupWrapper').ready(function() {

$input = $('.paper-input');

$input.on('keydown keyup focus', floatLabel);

 //float labels
        function floatLabel() {
            $(this).toggleClass('paper-input--touched', $(this).val() !== '');
        }

        //clear input
        function clearInput(){
            return;
        }

        //edit input
        function edit(event) {
            var valState = function(_, val){
                return !val;
            };
            $('.paper-input').prop("disabled", valState);
        }
    });