NISIS Style Guide
=================

by the Adapt Team
(c) 2013, Concept Solutions
Version 0.2.0b


Table of Contents:
------------------

  1. Code Style:
    a. HTML
    b. JavaScript
    c. CSS
    d. PHP
    e. Other
  2. Code Conventions
    a. HTML
    b. JavaScript
    c. CSS
    d. PHP
    e. Other
  3. Code Structure
    a. 
  4. Libraries
    q. JavaScript
    b. CSS
    c. PHP
    d. Other
  5. Versioning
  6. Documentation


1. Code Style
-------------
### HTML

NISIS is modern web-application intended to be run on a standards-compliant
browser (specifically, it's being developed against FireFox 23 or later). As
such, it should be developed using HTML5 wherever possible.  Any questions
regarding HTML5 standards-compliance for HTML5 should be answered in the
[Mozilla Developer Network documentation](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5/)

Even more specifically, a flexible web design that can be parsed as XML is
desirable, and so we should aim to develop using the stricter [Polyglot XHTML5
standards](http://www.w3.org/TR/html-polyglot/)

### Javascript

Because of Javascript's decentralized nature, No "official" style guide can be
pointed to as more canonical than any given company's in-house Javascript
style guide.  As such, we'll rely upon the [Google Style Guide](http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml),
but only loosely.

### CSS

Similar to Javascript, Conventions about the appropriate style of CSS are not
well-defined.  As such, we'll again refer to [Google's in-house guide for CSS
best practices]<http://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml>,
with one conspicuous difference: indent with tab characters, not sequences of
spaces.

Write it clearly with as much whitespace is needed.  We'll minify it for
production.

### PHP

All PHP Code should adhere to [PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md)
and [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md).

### Other


2. Code Conventions
-------------------

### HTML

Fewer Tags are better tags.  We want our HTML to be as compact as possible,
while still mitigating the need write HTML structures using javascript
builders.

### Javascript

You only need one Global Object.  It's called "nisis".  See section 3 for an
outline of the structure of the client-side nisis object.  Don't create
additional global objects when it can be avoided (which is always).

Where rendering is concerned, use classes in static HTML and then apply the JS
transformations only when you need to (e.g., when event handlers are required).

### CSS



### PHP

Don't close a PHP tag if you don't need to.

### Other


3. STRUCTURE
------------

### File Structure

admin/

api/

db/

html/ - Contains (mostly) static HTML

scripts/
:app.js - only loaded when the user is logged in and visiting the main app.  Contains a lot of DOM initialization, doesn't contribute much to the global nisis object.
:base.js - provides a base structure for the nisis object, including a skeleton for the object so that objects requiring interaction with nisis may be able to call each of those things without failing.  Sets the full contents of nisis.user, and also includes the jquery plugin that creates the .spin() method.
:config.js - contains the complete contents of nisis.config.
:login.js - only runs on the login screen--handles the dom manipulations and events for the login form.
:mapview.js - contains all the code to initialize the map, which is stored in the nisis.ui.mapview object.
:register.js - deprecated, no longer to be used.  Will be removed after Character rolling is implemented.
:ui.js - provides a variety of methods which interact with the DOM.  Contents available in nisis.ui

setup/

styles/ - Contains static CSS files

Tests/


### Global Data Structures

#### Client Side

    nisis = {
     	ui : {
        	content : convenience link to DOM Element ($('#content')),
    		dataview : convenience link to the Tabular view,
    		mapview : the complete mapview object,
    	},
    	user : {
    		login : function accepting username and password
    		logout : function accepting no arguments
    	},
    	util : {
    		reporter : {
    			report : the AJAX object for the most recent report request,
    			getReport : linearly recursive function which gets status changes from the server
    		}
    	},
    	config : object containing an extensive array of data sources and meta data thereabout
    }

#### Server Side

The Server's implementation doesn't currently contain any meaningfully organized global data structure.


4. Libraries
------------

ESRI ArcGIS Client
jQuery
jQueryUI
PrimeUI
Alertify
ColorPicker
TableSorter
LoDash
Normalize.css

5. Versioning
-------------

All versioning should emply the Semantic Versioning scheme of the type:

    Major.Minor.Release

as is described at [SemVer](http://semver.org/).  In short, Major version is
incremented only when backwards-incomptible changes are introduced into the
production environment.  NISIS should be delivered at the conclusion of this
contract as a version 1.0.0 product (though some small bugfixes may push that
value past 1.0.1).

The concluding version bump at the end of a sprint should increment the Minor,
setting the release counter back to zero.

6. Documentation
----------------

### User Documentation

Out of scope.  Don't document anything.

### Developer Documentation

All developer documentation is written in Markdown (see this file or README.md)
or using code comments for clarity.  In general, the better our Agile
practices, the less documentation is required.
