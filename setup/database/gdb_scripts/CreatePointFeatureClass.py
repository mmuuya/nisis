# Name: CreatePointFeatureClass.py
# Description: Create a feature class to store the gnatcatcher habitat zones

# Import system modules
import arcpy
from arcpy import env

# Set workspace
env.workspace = "C:/ArcGIS_WorkSpace/"

# Set local variables
out_path = "C:/ArcGIS_WorkSpace/output/nisis.gdb"
out_name = "nisis_point_features.shp"
geometry_type = "POINT"
##template = "study_quads.shp"
has_m = "DISABLED"
has_z = "DISABLED"

# Use Describe to get a SpatialReference object
spatial_reference = arcpy.Describe("C:/ArcGIS_WorkSpace/refs/point_features.shp").spatialReference

# Execute CreateFeatureclass
arcpy.CreateFeatureclass_management(out_path, out_name, geometry_type, template, has_m, has_z, spatial_reference)