# Name: AddField_Example2.py
# Description: Add a pair of new fields to a table
 
# Import system modules
import arcpy
from arcpy import env
 
# Set environment settings
env.workspace = "C:/ArcGIS_WorkSpace/output"
 
# INPUT FEATURE
inFeatures = "nisis_point_features.shp"
# 1 NAME
field1_name = "NAME"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "NAME"
field1_isNullable = "NON_NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 2 REF_NUMBER
field1_name = "REF_NUMBER"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "SHAPE REFERENCE NUMBER"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 3 DESCRIPTION
field1_name = "DESCRIPTION"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 2048
field1_alias = "DESCRIPTION"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 4 SUP_NOTES
field1_name = "SUP_NOTES"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 2048
field1_alias = "SUPPLEMENTAL NOTES"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 5 MIN_A_AGL
field1_name = "MIN_A_AGL"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "MINIMUM ALTITUDE (AGL)"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 6 MAX_A_MSL
field1_name = "MIN_A_MSL"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "MINIMUM ALTITUDE (MSL)"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 7 MAX_A_AGL
field1_name = "MAX_A_AGL"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "MAXIMUM ALTITUDE (AGL)"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 8 MAX_A_MSL
field1_name = "MAX_A_MSL"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "MINIMUM ALTITUDE (MSL)"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 9 VERTICES
field1_name = "VERTICES"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "VERTICES"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 10 SYMBOL
field1_name = "SYMBOL"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "SYMBOL"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)

# 11 USER_GROUP
field1_name = "USER_GROUP"
field1_type = "TEXT"
field1_precision = ""
field1_scale = ""
field1_length = 255
field1_alias = "USER ACCESS GROUP"
field1_isNullable = "NULLABLE"
# CREATE FIELD
arcpy.AddField_management(inFeatures, field1_name, field1_type, field1_precision, field1_scale, field1_length, field1_alias, field1_isNullable)
