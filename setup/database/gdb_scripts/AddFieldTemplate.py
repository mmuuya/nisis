# Name: AddField_Example2.py
# Description: Add a pair of new fields to a table
 
# Import system modules
import arcpy
from arcpy import env
 
# Set environment settings
env.workspace = "C:/data/airport.gdb"
 
# Set local variables
inFeatures = "schools"
fieldName1 = "ref_ID"
fieldPrecision = 9
fieldAlias = "refcode"
fieldName2 = "status"
fieldLength = 10
 
# Execute AddField twice for two new fields
arcpy.AddField_management(inFeatures, fieldName1, "LONG", fieldPrecision, "", "",fieldAlias, "NULLABLE")
arcpy.AddField_management(inFeatures, fieldName2, "TEXT", "", "", fieldLength)