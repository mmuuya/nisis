<?php

require "../handler.php";

?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Install NISIS</title>
		<link rel="stylesheet" href="../deps/normalize.css" />
		<link rel="stylesheet" href="../styles/base.css" />
		<!--[if lt IE 9]>
		<script src="../deps/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<div id="title">
				<span id="h1">NISIS</span><br />
				<span id="version">Version <?php echo $version; ?></span>
			</div>
		</header>
		<div id="content">
			<?php
		if(file_exists('../settings.php')){
			echo '<h2>Looks like this NISIS is Already Set Up!</h2>';
			include '../html/redirect.php';
		} else {
			include '../html/setup.html';
		}
			?>
		</div>
		<footer>
			Copyright 2013-<?php echo date("Y"); ?>,
			<a href="http://www.faa.gov/about/office_org/headquarters_offices/ato/">FAA - Air Traffic Organization</a>
		</footer>
	</body>
</html>

<?php
include ("../handler.php");
if (isset($_REQUEST['go'])) {
	$file_content = file($_REQUEST['dbtype'] . '.sql');
	foreach ($file_content as $sql_line) {
		if (trim($sql_line) != "" && strpos($sql_line, "--") === false) {
			pg_query($sql_line);
		}
	}
}
