<?php
	//The output from this file is going to start getting very large
	exit(); //this file not in use right now
?>

<script type="application/javascript">
$(document).ready(function() {
	var table = $("#auditTable").dataTable({
		 "bJQueryUI" : true,
		 "iDisplayLength" : 50,
	});
	table.fnSort([[0,'desc']]);
});
</script>

<p><a href="index.php">Return to the admin panel</a></p>

<?php

$audit_q = 'SELECT to_char(aud.time, :qformat), use.username, aud.success FROM NISIS_ADMIN.AUDIT_LOGIN aud LEFT JOIN APPUSER.USERS use ON aud.userid = use.userid';

$parsed_audit_q = oci_parse($db, $audit_q);
oci_bind_by_name($parsed_audit_q, ':qformat', $ora_dt_format);

if(!oci_execute($parsed_audit_q)) {
	kill('Error getting the login audit data.');
}
oci_fetch_all($parsed_audit_q, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

?>

<table id="auditTable">
<thead><tr><th>Access Time</th><th>Username</th><th>Successful?</th></tr></thead>
<tbody>
<?php

foreach ($results as $entry) {
	echo '<tr>';
	foreach ($entry as $col_val) {
		echo '<td>';
		switch($col_val) { //some values could look better
			case "":
				echo "[Unknown]";
				break;
			case "Y":
				echo "Yes";
				break;
			case "N":
				echo "No";
				break;
			default:
				echo $col_val;
		}
		echo '</td>';
	}
	echo '</tr>' . PHP_EOL; //adding the newline makes the source HTML pretty
}

?>
</tbody>
</table>

