$(document).ready(function() {
	var tblHigh = 300; //pixel height for the kendo grids

	//for debugging
	var debug = false; //log some stuff in the console?
	var hideID = true; //hide id columns?

	//When a user clicks a row in a "main" table, these need to get updated. The kendo data source for the "members" table sends these in the POST when read() is called. On page load, these are sent as "-1", and the response from PHP is the default message.
	var selectedGrpID = "-1";
	var selectedTFRGrpID = "-1";
	var selectedTableID = "-1";
	var selectedFieldID = "-1";
	var selectedReadWritePerms = "r";
	var groupAdminUserID;

	//Validators for the Forms
	var usrValidator = $("#userForm").kendoValidator().data("kendoValidator");
	var grpValidator = $("#groupForm").kendoValidator().data("kendoValidator");
	var tfrGrpValidator = $("#tfrGroupForm").kendoValidator().data("kendoValidator");

	//Getting the statuses divs
	var statusUser = $("#statusUser"),
	statusGroup = $("#statusGroup"),
	statusAssign = $("#statusAssign");
	tfrStatusGroup = $("#tfrStatusGroup"),
	tfrStatusAssign = $("#tfrStatusAssign");

	//-------------------
	//    USERS
	//-------------------
	var usrTableDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action" : "acl",
						"function" : "getUsers",
					};
				}
			},
		},
		schema: {
			data: "users",
		},
		sort: {
			field: "usrname",
			dir: "asc"
		},
		change: function(){
			if (debug) {
				console.log("users main table data changed");
			}
		},
	});

	$("#usersTable").kendoGrid({
		dataSource: usrTableDataSource,
		columns: [
			{field: "usrid", title:"ID", hidden: hideID },
			{field: "usrname", title:"Username", width: 200},
			{field: "email", title:"Email", width: 400},
			{field: "fname", title:"First"},
			{field: "lname", title:"Last"},
			{field: "apps", title:"Apps", template: appsTemplate},
		],
		sortable: { allowUnsort: false },
		autoBind: false,
		selectable:true,
		height: tblHigh,
		change: function(){
			if (debug) {
				console.log("Users grid has changed");
			}
			var selectedUsrData = this.dataItem(this.select());

			if (selectedUsrData !== null) {
				//set selected in form
				usrFormViewModel.set("selected", selectedUsrData);
				//needs to set the value for the multiselect
				appsMultiselect.value(selectedUsrData.apps);
			}
		},
	});
	var usrFormWindow = $("#usrMod").kendoWindow({
		visible: false,
		modal: true,
		open: function() {
			usrValidator.hideMessages();
			this.center();
		},
		close: function() {
			clearSelections();
			usrValidator.hideMessages();
		}
	});

	var usrFormViewModel = kendo.observable({
		selected: {},
		enableUsername: true,
		enableSaveButton: false,
		checkDirtyForm: function(e) {
			if(this.selected.fname !== $('#fname').val()){
				this.set("enableSaveButton", true);
			}else if(this.selected.lname !== $('#lname').val()){
				this.set("enableSaveButton", true);
			}else if(this.selected.email !== $('#email').val()){
				this.set("enableSaveButton", true);
			}else if(this.selected.usrname !== $('#username').val()){
				this.set("enableSaveButton", true);
			}else if(this.selected.password !== $('#password').val()){
				this.set("enableSaveButton", true);
			}else if(this.selected.usrid !== "-1"){
				if($(this.selected.apps).not(appsMultiselect.value()).length === 0 && $(appsMultiselect.value()).not(this.selected.apps).length === 0){
					this.set("enableSaveButton", true);
				}else{
					this.set("enableSaveButton", false);
				}
			}
		},
		saveUsr: function(e) {
			e.preventDefault();
			$this = this;
            if (usrValidator.validate()) {
            	//Since appsMultiSelect cannot be bound to a value like 'selected.apps' directly from
            	// MVVM, so the value has to be set manually as following
            	var appsValue = appsMultiselect.value();

            	$.ajax({
					url: "api/",
					type: "POST",
					data: {
						"action": "acl",
						"function": "modUser",
						"username": $this.selected.usrname,
						"first": $this.selected.fname,
						"last": $this.selected.lname,
						"email": $this.selected.email,
						"password": $this.selected.password,
						"id": $this.selected.usrid,
						"apps": appsValue,
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						var msg = ($this.selected.usrid === "-1") ? "created" : "modified";
						//NISIS-2977 - KOFI HONU - rewrote the if statement to correctly handle errors.
						if ( parseInt(resp.result) > 0) {
						//if (aclSuccess(resp)) {
							//true success
							usrTableDataSource.read();
							statusUser.text("The user was "+msg+" successfully!")
								.removeClass("invalid")
                                .addClass("valid");
							usrFormWindow.data("kendoWindow").close();
						} else {
							var errMsg = ($this.selected.usrid === "-1") ? "create" : "modify";
							statusGroup.text("There is an error trying to "+errMsg+" the user. " + resp.result)
								.removeClass("valid")
                                .addClass("invalid");
							displayErr("There is an error trying to "+errMsg+" the user. " + resp.result);
						}
					},
					error: function(req, status, err) {
						console.error("ERROR in ajax call to modify TFR user");
					},
				});
            } else {
            	displayErr("There is invalid data in the User Form.");
                statusUser.text("There is invalid data in the User Form.")
                	.removeClass("valid")
                    .addClass("invalid");
            }
		}
	});
	kendo.bind($("#usrMod"), usrFormViewModel);

	var appsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action" : "acl",
						"function" : "getAvailableApps"
					};
				}
			},
		},
		schema: {
		    model: { id: "APPID" },
			data: "apps",
		},
		change: function(){
			if (debug) {
				console.log("Apps dataSource data changed to: " , this.data());
			}
		}
	});

	var appsMultiselect = $("#apps").kendoMultiSelect({
		dataSource: appsDataSource,
		dataTextField: "APPNAME",
		dataValueField: "APPID"
	}).data("kendoMultiSelect");

	function appsTemplate(data) {
		//Checking if the value is string because when the value is
		// coming from db is a string: "0,1". When the value is changed
		//by the user the value will be an array of strings: ["0", "1"]
		var names = [], ids = [], dataArray = [];
		var dataIsString = false;

		if (data.apps instanceof Array){
			dataArray = data.apps;
		}else if (typeof(data.apps) === "string"){
			dataArray = data.apps.split(',');
			dataIsString = true;				
		}

		$.each(dataArray, function (idx, elem) {
			var temp = appsDataSource.get(elem);
			if ( temp ) {
				if (dataIsString){
					ids.push(elem);
				}
				names.push(temp.APPNAME);
			}
		});

		//Change the value in the datasource only if ids is not an empty array
		//e.g: From a value like "0,1" to ["0", "1"] instead
		if(ids.length !== 0 ){
			data.apps = ids;
		}
		
		return names.join(", ");
	}

	//-------------------
	//    NISIS GROUPS
	//-------------------
	var grpMembersDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action": "acl",
						"function": "getUsersFromNISISGroup",
						"groupId": selectedGrpID,
					};
				}
			},
		},
		schema: {
			data: "members",
			model: {
				id: "id",
				fields: {
					id: {editable: false},
					usrname: {editable: false},
					usrrole: {}
				}
			}
		},
		sort: {
			field: "usrname",
			dir: "asc"
		},
		change: function() {
			if (debug) {
				console.log("group members data changed");
			}
		},
	});


	$("#grpMembersTable").kendoGrid({
		dataSource: grpMembersDataSource,
		columns: [
			{field: "id", title: "ID", hidden:hideID },
			{field: "usrname", title: "Group Members"},
			{field: "usrrole", title: "Group Role", editor: usrRoleEditor , template: usrRoleTemplate},
		],
		autoBind: false,
		selectable:true,
		editable: true,
		height: tblHigh,
		change: function() {
				selectedItem = this.dataItem(this.select());

			if(selectedItem !== null){
				groupAdminUserID = selectedItem.id;
			}
		}
	});

	function usrRoleEditor(container, options){
		$('<input id="usrRoleEditor" data-bind="value:' + options.field + '" />')
		.appendTo(container)
		.kendoDropDownList({
			dataTextField: 'text',
			dataValueField: 'value',
			dataSource: [{ value: "1", text: "Read Only" }, { value: "2", text: "Read/Write" }, { value: "3", text: "Group Admin"}],
			change: function (e){
				$.ajax({
					url: "api/",
					type: "POST",
					data: {
						action: "acl",
						function: "modUserRoleInNISISGroup",
						groupID: selectedGrpID,
						userID: groupAdminUserID,
						userRole: this.value() //"this" is refering to the kendo dropdownlist,
					},
					success: function(result) {
						// console.log(result);
						grpMembersDataSource.read();
					},
					error: function(result) {
						console.error(result);
					}
				});
			}
		});
	}

	function usrRoleTemplate(data) {
		var datasource = {"1": "Read Only", "2": "Read/Write", "3": "Group Admin"};
		if (data.usrrole === "") {
			return "Select User Role...";
		} else {
			var text = datasource[data.usrrole];
			return text;
		}
	}

	//-----------------
	//Layers Tree view
	//-----------------
	/*var grpLayersDataSource = new kendo.data.HierarchicalDataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {

					console.log("selectedGrpID", selectedGrpID);
					return {
						"action": "acl",
						"function": "getLayersForNISISGroup",
						"groupId": selectedGrpID,
					};
				}
			},
		},
		// transport: {
		//     read: function(options) {
		//       // var id = options.data.EmployeeId;

		//       // [additional processing here]

		//       $.ajax({
		//         url: "api/",
		//         action: "acl",
		// 		function: "getLayersForNISISGroup",
		// 		groupId: selectedGrpID,
		//         // type: "POST",
		//         dataType: "jsonp", // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
		//         //data: { LAYERID: id },
		//         success: function(result) {
		//           // notify the data source that the request succeeded
		//           options.success(result);
		//         },
		//         error: function(result) {
		//           // notify the data source that the request failed
		//           options.error(result);
		//         }
		//       });
		//     }
		// },
		schema: {
			data: "layers",
			model: {
            	id: "LAYERID",
            	// hasChildren: "hasChildren"
            }
		},
		change: function(){
			if (debug) {
				console.log("NISIS layers datasource data changed");
			}
		}
	});

	$("#grpLayers").kendoTreeView({
            checkboxes: {
                checkChildren: true
            },
            // check: onCheck,
			// columns: [
			// 	{field: "grpid", title: "ID", hidden:hideID },
			// 	{field: "grpname", title: "Group Layers"},
			// ],
            dataSource: grpLayersDataSource,
            dataTextField: "LONGNAME",
    });*/

	var grpTableDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {"action" : "acl", 
							"function" : "getGroupsForLoggedUser"
					};
				}
			},
		},
		schema: {
			data: "groups",
		},
		sort: {
			field: "grpname",
			dir: "asc"
		},
		change: function(){
			if (debug) {
				console.log("NISIS group main data changed");
			}
		}
	});
	$("#grpsTable").kendoGrid({
		dataSource: grpTableDataSource,
		columns: [
			{field: "grpid", title: "ID", hidden:hideID },
			{field: "grpname", title: "Group Name"},
		],
		autoBind: false,
		selectable: true,
		height: tblHigh,
		change: function() {
			if (debug) {
				console.log("NISIS groups table grid changed");
			}
			//row clicked, so set selected in form, populate members table
			var selectedGrpData = this.dataItem(this.select());
			if (selectedGrpData !== null) {
				//set selected for form
				grpFormViewModel.set("selected", $.extend({}, selectedGrpData));

				//set selected for members data source
				selectedGrpID = selectedGrpData.grpid;

				//CC - If the group selected is "Administrators" (0), then hide the usrrole column because all the users in grp 0 should be Group Admin
				if (selectedGrpID === "0"){
					$("#grpMembersTable").data('kendoGrid').hideColumn("usrrole");
				}else{
					$("#grpMembersTable").data('kendoGrid').showColumn("usrrole");
				}
				
				grpMembersDataSource.read();
				// grpLayersDataSource.read();
			}
		}
	});
	var grpFormWindow = $("#grpMod").kendoWindow({
		visible: false,
		modal: true,
		open: function() {
			grpValidator.hideMessages();
			this.center();
		},
		close: function() {
			clearSelections();
			grpValidator.hideMessages();
		}
	});
	var grpFormViewModel = kendo.observable({
		validName: true, //for now always true
		selected: {},
		saveGrp: function(e) {
			e.preventDefault();
			$this = this;
			if (grpValidator.validate()) {
				$.ajax({
					url: "api/",
					type: "POST",
					data: {
						"action": "acl",
						"function": "modGroup",
						"groupId": $this.selected.grpid,
						"groupName": $this.selected.grpname,
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						var msg = ($this.selected.grpid === "-1") ? "created" : "modified";
						if (aclSuccess(resp)) {
							grpTableDataSource.read(); //we changed the data, so refresh
							statusGroup.text("The NISIS group was "+msg+" successfully!")
								.removeClass("invalid")
                                .addClass("valid");
							grpFormWindow.data("kendoWindow").close();
						} else {
							var errMsg = ($this.selected.grpid === "-1") ? "create" : "modify";
							statusGroup.text("There is an error trying to "+errMsg+" the NISIS group. " + resp.result)
								.removeClass("valid")
                                .addClass("invalid");
							displayErr("There is an error trying to "+errMsg+" the NISIS group. " + resp.result);
						}
					},
					error: function(req, status, err) {
						console.error("ERROR in ajax call to save NISIS group");
					},
				});
			}else{
				displayErr("The group name is not valid.");
                statusGroup.text("The group name is not valid.")
                	.removeClass("valid")
                    .addClass("invalid");
			}
		}
	});
	kendo.bind($("#grpMod"), grpFormViewModel);

	//-------------------
	//    TFR GROUPS
	//-------------------
	var tfrGrpMembersDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action": "acl",
						"function": "getUsersFromTFRGroup",
						"groupId": selectedTFRGrpID,
					};
				}
			},
		},
		schema: {
			data: "tfrMembers",
		},
		sort: {
			field: "usrname",
			dir: "asc"
		},
		change: function() {
			if (debug) {
				console.log("TFR group members data changed");
			}
		},
	});
	$("#tfrGrpMembersTable").kendoGrid({
		dataSource: tfrGrpMembersDataSource,
		columns: [
			{field: "id", title: "ID", hidden:hideID },
			{field: "usrname", title: "Group Members"},
		],
		autoBind: false,
		selectable:true,
		height: tblHigh,
		change: function() {
			if (debug) {
				console.log("TFR group members grid changed");
			}
		},
	});
	var tfrGrpTableDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return { "action" : "acl", "function" : "getAllTFRGroups", };
				}
			},
		},
		schema: {
			data: "groups",
		},
		sort: {
			field: "grpname",
			dir: "asc"
		},
		change: function(){
			if (debug) {
				console.log("TFR Group main data changed");
			}
		}
	});
	$("#tfrGroupsTable").kendoGrid({
		dataSource: tfrGrpTableDataSource,
		columns: [
			{field: "grpid", title: "ID", hidden:hideID },
			{field: "grpname", title: "Group Name"},
		],
		autoBind: false,
		selectable: true,
		height: tblHigh,
		change: function() {
			if (debug) {
				console.log("TFR Groups table grid changed");
			}
			//row clicked, so set selected in form, populate members table
			var selectedTfrGrpData = this.dataItem(this.select());
			if (selectedTfrGrpData !== null) {
				//set selected for form
				tfrGrpFormViewModel.set("selected", $.extend({}, selectedTfrGrpData));

				//set selected for members data source
				selectedTFRGrpID = selectedTfrGrpData.grpid;
				tfrGrpMembersDataSource.read();
			}
		}
	});
	var tfrGrpFormWindow = $("#tfrgrpMod").kendoWindow({
		visible: false,
		modal: true,
		open: function() {
			tfrGrpValidator.hideMessages();
			this.center();
		},
		close: function() {
			clearSelections();
			tfrGrpValidator.hideMessages();
		}
	});
	var tfrGrpFormViewModel = kendo.observable({
		validName: true, //for now always true
		selected: {},
		saveGrp: function(e) {
			e.preventDefault();
			$this = this;
            if (tfrGrpValidator.validate()) {
				$.ajax({
					url: "api/",
					type: "POST",
					data: {
						"action": "acl",
						"function": "modTFRGroup",
						"groupId": $this.selected.grpid,
						"groupName": $this.selected.grpname,
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						var msg = ($this.selected.grpid === "-1") ? "created" : "modified";
						if (aclSuccess(resp)) {
							tfrGrpTableDataSource.read(); //we changed the data, so refresh
							tfrStatusGroup.text("The TFR group was "+msg+" successfully!")
								.removeClass("invalid")
                                .addClass("valid");
							tfrGrpFormWindow.data("kendoWindow").close();
						} else {
							var errMsg = ($this.selected.grpid === "-1") ? "create" : "modify";
							tfrStatusGroup.text("There is an error trying to "+errMsg+" the TFR group. " + resp.result)
								.removeClass("valid")
                                .addClass("invalid");
							displayErr("There is an error trying to "+errMsg+" the TFR group. " + resp.result);
						}
					},
					error: function(req, status, err) {
						console.error("ERROR in ajax call to modify TFR group");
					},
				});
			}else{
				displayErr("The group name is not valid.");
                tfrStatusGroup.text("The group name is not valid.")
                	.removeClass("valid")
                    .addClass("invalid");
			}
		}
	});
	kendo.bind($("#tfrgrpMod"), tfrGrpFormViewModel);

	//-------------------
	//    NISIS ASSIGNMENT MULTISELECT
	//-------------------
	var assignFormWindow = $("#assignmentMod").kendoWindow({
		visible:false,
		modal:true,
		width: "75%",
		open: function(){
			this.center();
			availItemsDataSource.read();
			assignSelect.value([]);
		},
		close: function(){
			clearSelections();
			if(debug) {
				console.log("NISIS assignment form closed");
			}
		}
	}).data("kendoWindow");

	var assignFormViewModel = kendo.observable({
		type: "", //"usrtogrp" or "grptofldr" or "grptofldw" - set by button click
		id: "", //id of item to assign members to - set by button click
		saveAssignment: function(e) {
			e.preventDefault();
			$this = this;
			if(debug) {
				console.log("let's assign " + $this.get("type") + " " + $this.get("id") + " selected vals: " , assignSelect.value());
			}

			if(assignSelect.value().length === 0) {
				displayErr("Please select the user or users to assign");
				console.log("NISIS Assignment form empty, nothing to assign");
				// assignFormWindow.close();
				// return;
			}else{
				$.ajax({
					url:"api/",
					type:"POST",
					data: {
						"action":"acl",
						"function":"assign",
						"idassignto": $this.get("id"),
						"type": $this.get("type"),
						"items": assignSelect.value(),
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						if (aclSuccess(resp)) {
							statusAssign.text("The NISIS user(s) was(were) assigned successfully!")
								.removeClass("invalid")
	                        	.addClass("valid");
	                        assignFormWindow.close();
	                        //The code to update the layers tree when an user(s) is assign to a group needs to be here
	                 		//require(["app/windows/layers"], function(layers){
  		            		//	layers.updateLayersTree();
  		            		//});

						} else {
							statusAssign.text("There is an error trying to assign the NISIS item(s).")
								.removeClass("valid")
	                        	.addClass("invalid");;
							displayErr("There is an error trying to assign the NISIS item(s). " + resp.result);
						}
					},
					error: function(req, status, err) {
						console.error("ERROR in ajax call to assign NISIS items");
					}
				});
			}
		}
	});
	kendo.bind($("#assignmentMod"), assignFormViewModel);

	var availItemsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action" : "acl",
						"function" : "getavail",
						"type" : assignFormViewModel.get("type"),
						"mainid": assignFormViewModel.get("id"),
					};
				}
			},
		},
		schema: {
			data: "available",
		},
		change: function(){
			if (debug) {
				console.log("available items data changed to: " , this.data());
			}
		}
	});

	var assignSelect = $("#itemsassigned").kendoMultiSelect({
		dataSource: availItemsDataSource,
		dataTextField: "itemlabel",
		dataValueField: "itemid",
	}).data("kendoMultiSelect");

	//-------------------
	//    TFR ASSIGNMENT MULTISELECT
	//-------------------
	var tfrAssignFormWindow = $("#tfrassignmentMod").kendoWindow({
		visible:false,
		modal:true,
		width: "75%",
		open: function(){
			this.center();
			tfrAvailItemsDataSource.read();
			tfrAssignSelect.value([]);
		},
		close: function(){
			clearSelections();
			if(debug) {
				console.log("TFR assignment form closed");
			}
		}
	}).data("kendoWindow");

	var tfrAssignFormViewModel = kendo.observable({
		type: "", //"usrtotfrgrp" set by button click
		id: "", //id of item to assign members to - set by button click
		saveTFRAssignment: function(e) {
			e.preventDefault();
			$this = this;
			if(debug) {
				console.log("let's assign " + $this.get("type") + " " + $this.get("id") + " selected vals: " , tfrAssignSelect.value());
			}

			if(tfrAssignSelect.value().length === 0) {
				displayErr("Please select the user or users to assign");
				console.log("TFR Assignment form empty, nothing to assign");
			}else{
				$.ajax({
					url:"api/",
					type:"POST",
					data: {
						"action":"acl",
						"function":"assign",
						"idassignto": $this.get("id"),
						"type": $this.get("type"),
						"items": tfrAssignSelect.value(),
					},
					success: function(data, status, req) {
						var resp = JSON.parse(data);
						if (aclSuccess(resp)) {
							tfrStatusAssign.text("The user(s) was(were) assigned successfully!")
								.removeClass("invalid")
	                        	.addClass("valid");
	                        tfrAssignFormWindow.close();
						} else {
							tfrStatusAssign.text("There is an error trying to assign the user(s).")
								.removeClass("valid")
	                        	.addClass("invalid");
							displayErr("There is an error trying to assign the user. " + resp.result);
						}
					},
					error: function(req, status, err) {
						console.error("ERROR in ajax call to assign TFR items");
					},
				});
			}
		}
	});
	kendo.bind($("#tfrassignmentMod"), tfrAssignFormViewModel);

	var tfrAvailItemsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action" : "acl",
						"function" : "getavail",
						"type" : tfrAssignFormViewModel.get("type"),
						"mainid": tfrAssignFormViewModel.get("id"),
					};
				}
			},
		},
		schema: {
			data: "available",
		},
		change: function(){
			if (debug) {
				console.log("available items data changed to: " , this.data());
			}
		}
	});

	var tfrAssignSelect = $("#tfrItemsAssigned").kendoMultiSelect({
		dataSource: tfrAvailItemsDataSource,
		dataTextField: "itemlabel",
		dataValueField: "itemid",
	}).data("kendoMultiSelect");

	//-------------------
	//    ACCESS CONTROL (SHARED)
	//-------------------
	var tablesDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action": "acl",
						"function": "getalltables",
					};
				}
			},
		},
		schema: {
			data: "tables",
		},
		change: function() {
			if(debug){ console.log("tables data changed"); }
		},
	});

	$("#tablesTableW, #tablesTableR").kendoGrid({
		dataSource: tablesDataSource,
		columns: [
			{field: "tableid", title: "ID", hidden: hideID},
			{field: "table", title: "DB Name", hidden: hideID},
			{field: "label", title: "Profile / Table"},
		],
		autoBind: false,
		selectable:true,
		height: tblHigh,
		change: function(){
			if (debug) {
				console.log("tables grid changed");
			}

			var selectedData = this.dataItem(this.select());
			if (selectedData !== null) {
				//set selected in form
				tableFormViewModel.set("selected", $.extend({}, selectedData));

				//set selected to populate next grid
				selectedTableID = selectedData.tableid;
				fieldsDataSource.read();

				//clear the allowed edit/read groups grid
				selectedFieldID = "-1";
				editGrpsDataSource.read();
				readGrpsDataSource.read();
			}
		},
	});

	var tableFormWindow = $("#tblMod").kendoWindow({
		visible: false,
		modal: true,
		open: function() {
			this.center();
			//clearSelections();
		},
	}).data("kendoWindow");

	var tableFormViewModel = kendo.observable({
		selected: {},
		valid: true,
		saveTableLabel: function() {
			$.ajax({
				url: "api/",
				type: "POST",
				data: {
					"action": "acl",
					"function": "modtable",
					"label": this.selected.label,
					"tableid": this.selected.tableid,
				},
				success: function(data, status, req) {
					var resp = JSON.parse(data);
					if (aclSuccess(resp)) {
						//true success
						console.log("table label saved okay");
						tablesDataSource.read();
						tableFormWindow.close();
					} else {
						//error sent from php
						displayErr(resp.result);
					}
				},
				error: function(req, status, err) {
					console.log("ERROR in ajax call to modify editable table");
				},
			});
		},
	});
	kendo.bind($("#tblMod"), tableFormViewModel);

	var fieldsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === 'read') {
					return {
						"action": "acl",
						"function": "gettablefields",
						"readwrite": selectedReadWritePerms,
						"ident": selectedTableID,
					};
				}
			},
		},
		schema: {
			data: "fields",
		},
		change: function() {
			if(debug) {
				console.log("fields data changed");
			}
		},
	});

	$("#fieldsTableW, #fieldsTableR").kendoGrid({
		dataSource: fieldsDataSource,
		columns: [
			{field: "fieldid", title: "ID", hidden: hideID},
			{field: "field", title: "Oracle Column", hidden: hideID},
			{field: "label", title: "Field"},
		],
		autoBind: false,
		selectable:true,
		height: tblHigh,
		change: function(e){
			if (debug) {
				console.log("fields grid changed");
			}

			var selectedData = this.dataItem(this.select());
			if (selectedData !== null){
				//set select for form
				fieldFormViewModel.set("selected", $.extend({}, selectedData));

				//set selected to populate next grid
				selectedFieldID = selectedData.fieldid;

				var clicked = e.sender.element[0].id;
				if(clicked === "fieldsTableR"){
					readGrpsDataSource.read();
				} else if (clicked === "fieldsTableW"){
					editGrpsDataSource.read();
				}
			}
		},
	});
	var fieldsFormWindow = $("#fldMod").kendoWindow({
		visible: false,
		modal: true,
		open: function() {
			console.log("we're opening the window, selected should be set, it is: " , fieldFormViewModel.selected);
			this.center();
		},
	}).data("kendoWindow");
	var fieldFormViewModel = kendo.observable({
		selected: {},
		saveFldLabel: function() {
			$.ajax({
				url: "api/",
				type: "POST",
				data: {
					"action": "acl",
					"function": "modfield",
					"label": this.selected.label,
					"fieldid": this.selected.fieldid,
				},
				success: function(data, status, req) {
					var resp = JSON.parse(data);
					if(aclSuccess(resp)) {
						//true success
						console.log("field label saved okay");
						fieldsDataSource.read();
						fieldsFormWindow.close();

					} else {
						//error sent from php
						displayErr(resp.result);
					}
				},
				error: function(req, status, err) {
					console.log("ERROR in ajax call to modify editable field");
				},
			});
		},
	});
	kendo.bind($("#fldMod"), fieldFormViewModel);

	//-------------------
	//    ACCESS CONTROL (READ)
	//-------------------
	var readGrpsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === "read") {
					return {
						"action": "acl",
						"function": "getfieldrgroups",
						"ident": selectedFieldID,
					};
				}
			},
		},
		schema: {
			data: "groups",
		},
		change: function(){
			if(debug) {
				console.log("readable groups data has changed");
			}
		}
	});
	var readersGrid = $("#readersTable").kendoGrid({
		dataSource: readGrpsDataSource,
		columns: [
			{field: "grpid", title: "ID", hidden: hideID},
			{field: "grpname", title: "Groups Allowed to Read"}
		],
		autoBind: false,
		selectable: true,
		height: tblHigh,
		change: function(){
			if(debug) {
				console.log("readable groups grid was changed");
			}
		},
	});

	//-------------------
	//    ACCESS CONTROL (WRITE)
	//-------------------
	var editGrpsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "api/",
				type: "POST",
				dataType: "json",
			},
			parameterMap: function(data, type) {
				if (type === "read") {
					return {
						"action": "acl",
						"function": "getfieldwgroups",
						"ident": selectedFieldID,
					};
				}
			},
		},
		schema: {
			data: "groups",
		},
		change: function() {
			if(debug) {
				console.log("editable groups data has changed");
			}
		}
	});
	var editorsGrid = $("#editorsTable").kendoGrid({
		dataSource: editGrpsDataSource,
		columns: [
			{field: "grpid", title: "ID", hidden: hideID},
			{field: "grpname", title: "Groups Allowed to Edit"},
		],
		autoBind: false,
		selectable:true,
		height: tblHigh,
		change: function(){
			if(debug) {
				console.log("editable groups grid was changed");
			}
		},
	}).data("kendoGrid");

	//-------------------
	//    MAIN PANELS
	//-------------------
	$("#adminTables").kendoPanelBar({
		//it's important this setting be "single" - the rest of the code is implemented in a way that assumes the user will only be operating in one section at a time
		expandMode: "single",
		expand: function(e) {
			if(debug) {
				console.log("panel expanding... ");
			}

			clearSelections();
			cleanMessages();

			var panelID = $(e.item).attr("id");
			if (panelID === "userPanel") {
				usrTableDataSource.read();
			} else if (panelID === "groupsPanel") {
				grpTableDataSource.read();
				grpMembersDataSource.read();
			} else if (panelID === "tfrGroupsPanel") {
				tfrGrpTableDataSource.read();
				tfrGrpMembersDataSource.read();
			} else if (panelID === "aclWPanel") {
				selectedReadWritePerms = "w";
				tablesDataSource.read();
				//fieldsDataSource.read();
				//editGrpsDataSource.read();
			} else if (panelID === "aclRPanel") {
				selectedReadWritePerms = "r";
				tablesDataSource.read();
				//fieldsDataSource.read();
				//readGrpsDataSource.read();
			} else {
				console.error("expand ID is bad: ", panelID);
			}
		},
	});

	//CC - Expand the groupsPanel if the user is only a Group Admin and not an Administrator because it will have only 1 panel
	if ($("#adminTables").data("kendoPanelBar").element.children().length === 1)
		$("#adminTables").data("kendoPanelBar").expand($("#groupsPanel"));

	//-------------------
	//    FUNCTIONS
	//-------------------

	//onclicks for edit, plus, minus buttons
	$("a.k-button").click(function() {
		var buttonargs = this.id.split('-');
		var action = buttonargs[0]; //edit, new, del, assign, unassign
		var type = buttonargs[1]; //usr, grp, tfrgrp, cat, usrtogrp, usrtotfrgrp, grptocat

		//Cleaning any possible validation messages
		cleanMessages();

		if(debug){
			console.log("button click: action=" + action + " | type=" + type);
		}

		switch (action) {
			case "new":
				addNew(type);
			break;
			case "del":
				remove(type);
			break;
			case "edit":
				modify(type);
			break;
			case "assign":
				assign(type);
			break;
			case "unassign":
				//setup the tables to get ids from
				var mainTable, memberTable, dataSrcToUpdate;
				if (type === "usrtogrp") {
					mainTable = "#grpsTable";
					memberTable = "#grpMembersTable";
					dataSrcToUpdate = grpMembersDataSource;
				} else if (type === "usrtotfrgrp") {
					mainTable = "#tfrGroupsTable";
					memberTable = "#tfrGrpMembersTable";
					dataSrcToUpdate = tfrGrpMembersDataSource;
				} else if (type === "grptofldw") {
					mainTable = "#fieldsTableW";
					memberTable = "#editorsTable";
					dataSrcToUpdate = editGrpsDataSource;
				} else if (type === "grptofldr") {
					mainTable = "#fieldsTableR";
					memberTable = "#readersTable";
					dataSrcToUpdate = readGrpsDataSource;
				} else {
					console.log("BUG - bad call to unassign case");
				}

				//get the selected row of "main" and "member" grids
				var mainGrid = $(mainTable).data("kendoGrid");
				var mainRow = mainGrid.dataItem(mainGrid.select());
				if (mainRow === null){
					displayErr("Please select an item to unassign from.");
					return;
				}

				var memberGrid = $(memberTable).data("kendoGrid");
				var memberRow = memberGrid.dataItem(memberGrid.select());
				if (memberRow === null) {
					displayErr("Please select a member to be unassigned.");
					return;
				}

				//now we can get the ids which specify which member
				//should be unassigned from which item
				var mainID, memberID;
				if (type === "usrtogrp" || type === "usrtotfrgrp") {
					mainID = mainRow.grpid;
					memberID = memberRow.id;
				} else if (type === "grptofldw" || type === "grptofldr") {
					mainID = mainRow.fieldid;
					memberID = memberRow.grpid;
				}

				unassign(type, mainID, memberID);
				dataSrcToUpdate.read();

				if(debug){
					console.log("main row: " , mainRow);
					console.log("member row: " , memberRow);
				}
			break;
			default:
				displayErr("BUG: Button not well defined.");
		}
	});

	//Cleaning the messages
	function cleanMessages() {
		statusUser.text("");
		statusGroup.text("");
		tfrStatusGroup.text("");
		statusAssign.text("");
		tfrStatusAssign.text("");
	}

	//Clear user selections from kendo grids and other forms
	function clearSelections() {
		if (debug) {
			console.log("[clearing selections]");
		}

		//remove selected rows from all kendo grids
		var gridsToClear = [
			"#usersTable",
			"#grpsTable",
			"#grpMembersTable",
			"#tfrGroupsTable",
			"#tfrGrpMembersTable",
			"#tablesTableW",
			"#fieldsTableW",
			"#editorsTable"
		];
		for (var ii=0; ii<gridsToClear.length; ii++){
			if($(gridsToClear[ii]).data("kendoGrid") !== null){
				$(gridsToClear[ii]).data("kendoGrid").clearSelection();
			}
		}

		//empty selected objects in the various view models
		usrFormViewModel.set("selected", {});
		//The multiselect needs to be cleared manually because it is set manually
		appsMultiselect.value([]);
		grpFormViewModel.set("selected", {});
		tfrGrpFormViewModel.set("selected", {});
		fieldFormViewModel.set("selected", {});
		tableFormViewModel.set("selected", {});

		//make selected ID's the default
		selectedGrpID = selectedTFRGrpID = selectedCatID = selectedTableID = selectedFieldID = "-1";

		//read some members tables to blank them out
		tfrGrpMembersDataSource.read();
		grpMembersDataSource.read();
		fieldsDataSource.read();
		editGrpsDataSource.read();
		readGrpsDataSource.read();
	}

	//Pop up the dialog to add a new usr/grp/cat
	//
	//type is one of: "usr", "grp", "tfrgrp", "cat"
	function addNew(type) {
		var selector = "#" + type + "Mod";
		var kendoW = $(selector).data("kendoWindow");

		//set title for window, and clear values in appropriate ViewModel
		switch (type){
			case "usr":
				kendoW.title("New User");
				usrFormViewModel.set("selected", {"usrid":"-1"});
				usrFormViewModel.set("enableUsername", true);
				usrFormViewModel.set("enableSaveButton", false);
				appsMultiselect.value([]);
			break;
			case "grp":
				kendoW.title("New NISIS Group");
				grpFormViewModel.set("selected", {"grpid":"-1"});
			break;
			case "tfrgrp":
				kendoW.title("New TFR Group");
				tfrGrpFormViewModel.set("selected", {"grpid":"-1"});
			break;
			default:
				displayErr("BUG - error trying to add new '" + type + "'");
			break;
		}

		kendoW.open();
	}

	//Pop up the dialog to modify usr/grp/cat
	//
	//type is one of: "usr", "grp", "tfrgrp", "tbl", "tblr", "fld", "fldr"
	//    - "tblr" will be treated as "tbl" & "fldr" as "fld"
	function modify(type) {
		if (type === "tblr") {
			type = "tbl";
		} else if (type === "fldr") {
			type = "fld";
		}

		var kendoW = $("#" + type + "Mod").data("kendoWindow");

		switch (type) {
			case "usr":
				if(!usrFormViewModel.get("selected").usrid){
					displayErr("Please select a user");
					return;
				}
				kendoW.title("Edit User");
				usrFormViewModel.set("enableUsername", false);
				usrFormViewModel.set("enableSaveButton", false);
			break;
			case "grp":
				if(!grpFormViewModel.get("selected").grpid){
					displayErr("Please select a group");
					return;
				}
				kendoW.title("Edit NISIS Group");
			break;
			case "tfrgrp":
				if(!tfrGrpFormViewModel.get("selected").grpid){
					displayErr("Please select a group");
					return;
				}
				kendoW.title("Edit TFR Group");
			break;
			case "tbl":
				if(!tableFormViewModel.get("selected").tableid){
					displayErr("Please select a table");
					return;
				}
				kendoW.title("Edit Table");
			break;
			case "fld":
				if(!tableFormViewModel.get("selected").tableid || !fieldFormViewModel.get("selected").fieldid){
					displayErr("Please select a table and field");
					return;
				}
				kendoW.title("Edit Field");
			break;
			default:
				kendoW.title("UNKNOWN MODIFY");
			break;
		}

		kendoW.open();
	}

	//Confirm deletion of usr/grp/cat
	//
	//type is one of: "usr", "grp", "cat"
	function remove(type) {
		var id, dataSourceToRefresh, membersDataSourceToRefresh, grid;
		switch (type) {
			case "usr":
				if(!usrFormViewModel.get("selected").usrid){
					displayErr("Please select the user to be deleted");
					return;
				}else{
					if(!confirm("Are you sure you want to delete this user?")) {
						return;
					}else{
						grid = $("#usersTable").data("kendoGrid");
						dataSourceToRefresh = usrTableDataSource;
						id = grid.dataItem(grid.select()).usrid;
						gridToRefresh = grid;
					}
				}
			break;
			case "grp":
				if(!grpFormViewModel.get("selected").grpid){
					displayErr("Please select the NISIS group to be deleted");
					return;
				}else{
					if(!confirm("Are you sure you want to delete this NISIS group?")) {
						return;
					}else{
						grid = $("#grpsTable").data("kendoGrid");
						dataSourceToRefresh = grpTableDataSource;
						membersDataSourceToRefresh = grpMembersDataSource;
						id = grid.dataItem(grid.select()).grpid;
						gridToRefresh = grid;
					}
				}
			break;
			case "tfrgrp":
				if(!tfrGrpFormViewModel.get("selected").grpid){
					displayErr("Please select the TFR group to be deleted");
					return;
				}else{
					if(!confirm("Are you sure you want to delete this TFR group?")) {
						return;
					}else{
						grid = $("#tfrGroupsTable").data("kendoGrid");
						dataSourceToRefresh = tfrGrpTableDataSource;
						membersDataSourceToRefresh = tfrGrpMembersDataSource;
						id = grid.dataItem(grid.select()).grpid;
						gridToRefresh = grid;
					}
				}
			break;
		}

		$.ajax({
			url: "api/",
			type: "POST",
			data: {
				"action": "acl",
				"function" : "deleteItem",
				"type": type,
				"id" : id,
			},
			success: function(data, status, req) {
				var resp = JSON.parse(data),
				successMsg="", errMsg="", tp="", sts="";
				switch (type) {
					case "usr":
						tp="user";
						sts=statusUser;
					break;
					case "grp":
						tp="NISIS group";
						sts=statusGroup;
					break;
					case "tfrgrp":
						tp="TFR group";
						sts=tfrStatusGroup;
					break;
					}
				successMsg = "The "+tp+" was deleted successfully!";
				errorMsg = "There is an error trying to delete the "+tp+". " + resp.result;

				if (aclSuccess(resp)) {
					//true success
					dataSourceToRefresh.read();
					if (type === "grp" || type === "tfrgrp"){
						selectedGrpID = selectedTFRGrpID = "-1";
						membersDataSourceToRefresh.read();
					}
					sts.text(successMsg)
						.removeClass("invalid")
                        .addClass("valid");
				} else {
					//some error was sent
					sts.text(errorMsg)
						.removeClass("valid")
                        .addClass("invalid");
					displayErr(errMsg);
				}
				//Clearing the selected object
				usrFormViewModel.set("selected", {});
				grpFormViewModel.set("selected", {});
				tfrGrpFormViewModel.set("selected", {});
			},
			error: function(req, status, err) {
				console.log("ERROR in AJAX request trying to delete item.");
			},
		});
	}

	//Show dialog to assign user into group, or group into category
	//
	//type is one of: "usrtogrp", "grptofldw", "grptotblw", "grptofldr", "grptotblr"
	function assign(type) {
		if(type === "usrtotfrgrp"){
			tfrAssignFormViewModel.set("type", type);
		}else{
			assignFormViewModel.set("type", type);
		}

		//get selected id and name from edit form (when a user clicks a kendo grid's row, we set it there - better way to do this?)
		var selData;
		var titleStr = "BUG - DEFAULT TITLE";
		switch(type){
			case("usrtogrp"):
				selData = grpFormViewModel.get("selected");
				if(!selData.grpid) {
					displayErr("Please, first select a group");
					return;
				}else{
					assignFormViewModel.set("id", selData.grpid);
					titleStr = "Assign user(s) to NISIS group: " + selData.grpname;
				}
			break;
			case("usrtotfrgrp"):
				selData = tfrGrpFormViewModel.get("selected");
				if(!selData.grpid) {
					displayErr("Please, first select a group");
					return;
				}else{
					tfrAssignFormViewModel.set("id", selData.grpid);
					titleStr = "Assign user(s) to TFR group: " + selData.grpname;
				}
			break;
			case("grptofldr"):
				//fall through to grptofldw
			case("grptofldw"):
				selData = fieldFormViewModel.get("selected");
				if(!selData.fieldid) {
					displayErr("Please select a field");
					return;
				}
				assignFormViewModel.set("id", selData.fieldid);
				titleStr = "Assign groups to ";
				if (type === "grptofldr") {
					titleStr += "read: ";
				} else if (type === "grptofldw") {
					titleStr += "edit: ";
				}
				titleStr += selData.label;
			break;
			case("grpuntblr"): //all fall through to "grptotblw"
			case("grpuntblw"):
			case("grptotblr"):
			case("grptotblw"):
				selData = tableFormViewModel.get("selected");
				if(!selData.tableid) {
					displayErr("Please select a table");
					return;
				}
				assignFormViewModel.set("id", selData.tableid);

				if (type === "grpuntblr") {
					titleStr = "Remove groups from every field's READ permission in ";
				} else if (type === "grpuntblw") {
					//we're using the assignment form, but removing each group's access to all fields
					titleStr = "Remove groups from every field's WRITE permission in ";
				} else if(type === "grptotblr"){
					titleStr = "Assign groups to every field's READ permission in ";
				} else if(type === "grptotblw"){
					titleStr = "Assign groups to every field's WRITE permission in ";
				}
				titleStr += selData.label;
			break;
			default:
				console.error("bad call to assign() type is: ", type);
				return;
			break;
		}

		if(type === "usrtotfrgrp"){
			tfrAssignFormWindow.title(titleStr);
			tfrAssignFormWindow.open();
		}else{
			assignFormWindow.title(titleStr);
			assignFormWindow.open();
		}
	}

	//Show confirmation to unassign user from group, or group from category
	//
	//type is one of: "usrtogrp", "grptocat"
	//id is the database id of the group/cat from which the member will be unassigned
	//memberID is the database id of the user/group to be unassigned
	function unassign(type, id, memberID) {
		if (debug) {
			console.log("In unassign function, type: " + type + " | id: " + id + " | memberID: " + memberID);
		}

		var errMsg = "BUG - BAD CALL TO UNASSIGN";
		if (type === 'usrtogrp'){
			errMsg = "Are you sure you want to remove this user from the NISIS group?";
		} else if (type === 'usrtotfrgrp'){
			errMsg = "Are you sure you want to remove this user from the TFR group?";
		} else if (type === 'grptofldr') {
			errMsg = "Are you sure you want to remove this group from the field's READ permission?";
		} else if (type === 'grptofldw') {
			errMsg = "Are you sure you want to remove this group from the field's WRITE permission?";
		}

		if(!confirm(errMsg)) {
			return;
		}else{
			$.ajax({
				url: "api/",
				type: "POST",
				data: {
					"action": "acl",
					"function" : "unassign",
					"type": type,
					"id" : id,
					"memberid" : memberID,
				},
				success: function(data, status, req) {
					var resp = JSON.parse(data);
					switch(type){
						case("usrtogrp"):
							if (aclSuccess(resp)) {
								statusAssign.text("The NISIS user was unassigned successfully!")
									.removeClass("invalid")
		                        	.addClass("valid");
		                        //The code to update the layers tree when the user is unassign to a group needs to be here
		                		//require(["app/windows/layers"], function(layers){
	  		            		//	layers.updateLayersTree();
	  		            		//});
							} else {
								statusAssign.text("There is an error trying to unassign the NISIS item.")
									.removeClass("valid")
		                        	.addClass("invalid");
								displayErr("There is an error trying to unassign the NISIS item. " + resp.result);
							}
						break;
						case("usrtotfrgrp"):
							if (aclSuccess(resp)) {
								tfrStatusAssign.text("The TFR user was unassigned successfully!")
									.removeClass("invalid")
		                        	.addClass("valid");
							} else {
								tfrStatusAssign.text("There is an error trying to unassign the TFR user.")
									.removeClass("valid")
		                        	.addClass("invalid");
								displayErr("There is an error trying to unassign the TFR user. " + resp.result);
							}
						break;
					}
				},
				error: function(req, status, err) {
					console.error("ERROR in ajax performing the item unassignment");
				}
			});
		}
	}

	// check for successful message from the ajax call to api/acl.php
	// returns true if "result":"Success" is found in the JSON object
	function aclSuccess(jsonData) {
		var successKey = "result";
		var successVal = "Success";

		return (jsonData[successKey] === successVal);
	}

	//This will eventually be a wrapper for nisis.ui.displayMessage
	//once the link to ui.js is connected properly
	function displayErr(messageStr) {
		alert(messageStr);
	}

});
