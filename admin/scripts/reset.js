//Handle password reset windows/forms.

$(document).ready(function() {

// var formPanels = $("#resetFormPanels").kendoPanelBar({
//     expandMode: "single",
// }).data("kendoPanelBar").expand($("#reqTknPnl"));

kendo.init($("#resetForms"));
var getInitWindow = $("#resetForms").data("kendoWindow").center();

//which form is written out by PHP and exists in DOM
if($("#getNameEmailForm").length){
    var reqTknVM = kendo.observable({
        username: "",
        email: "",
        requestToken: function(){
            $.ajax({
                url: "admin/api/",
                type: "POST",
                data: {
                    action: "reset_pw",
                    username: this.username,
                    email: this.email
                },
                success: function(data, status, req){
                    var resp = JSON.parse(data);
                    if(resp.result === "Success"){
                        nisis.ui.displayMessage(resp.message);
                    } else {
                        nisis.ui.displayMessage(resp.result, 'error');
                    }
                },
                error: function(req, status, err){
                    console.error("ERROR when requesting token for pw reset");
                    console.error("req: ", req);
                    console.error("status: ", status);
                    console.error("err: ", err);
                }
            });
        },
    });
    kendo.bind($("#getNameEmailForm"), reqTknVM);
} else if($("#newPwForm").length){
    var nuPassVM = kendo.observable({
        nupass1: "",
        nupass2: "",
        submitNuPass: function(){
            // console.log("token: " + $("#inputToken").text());
            $.ajax({
                url: "admin/api/",
                type: "POST",
                data: {
                    action: "change_pw",
                    passOne: this.nupass1,
                    passTwo: this.nupass2,
                    token: $("#inputToken").text(),
                },
                success: function(data, status, req){
                    var resp = JSON.parse(data);
                    console.log("pw's submitted: ", resp);
                    //if something's wrong, display the message server returned
                    if(resp.result === 'Error'){
                        $("#pwErrArea").text("* " + resp.message);
                        nisis.ui.displayMessage("Password could not be changed!", 'error');
                        return;
                    } else if(resp.result === 'Success' || resp.result === 'Redirect'){
                        window.location.replace(resp.redirect);
                    } else {
                        nisis.ui.displayMessage("Something went wrong, password not changed");
                    }
                },
                error: function(req, status, err){
                    console.error("ERROR when submitting new passwords for reset");
                    console.error("req: ", req);
                    console.error("status: ", status);
                    console.error("err: ", err);
                }
            });
        }
    });
    kendo.bind($("#newPwForm"), nuPassVM);
    getInitWindow.open();
}

//display a message if PHP needs it
if($("#resetPwMsg").length){
    nisis.ui.displayMessage($("#resetPwMsg").text());
}

//user clicked "reset password" link under the login form
$("#newpass").click(function(){
    getInitWindow.open();
});

//clear err message by clicking on it!
$("#pwErrArea").click(function(){
    $("#pwErrArea").html("&nbsp;");
});

}); //end document.ready