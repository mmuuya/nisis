<?php
require ('../handler.php');

//check for a valid session
$loggedin = isset($_SESSION['username']);

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>NAS Integrated Status Insight System</title>
		<link rel="stylesheet" type="text/css" href="../deps/kendoui/styles/kendo.common.min.css" /> <!-- common kendo ui style -->
		<link rel="stylesheet" type="text/css" href="../deps/kendoui/styles/kendo.default.min.css" /> <!-- default kendo web theme -->
		<link rel="stylesheet" type="text/css" href="../styles/base.css" />
		<link rel="stylesheet" type="text/css" href="../styles/app.css" />		
		<!-- <link rel="stylesheet" type="text/css" href="style.css" /> -->
		<!--[if lt IE 9]>
		<script src="deps/html5shiv.js"></script>
		<![endif]-->
		<script type="text/javascript" src="../deps/kendoui/js/jquery.min.js"></script> <!-- kendo's included jquery lib -->
		<script type="text/javascript" src="../deps/kendoui/js/kendo.web.min.js"></script> <!-- kendo ui web -->
	</head>
	<body class="tundra">
        <!-- alert messages-->
		<!-- <header>
			<div id="title-box">
				<span id="title">NISIS</span>&nbsp;
				<span id="version">V. <?php echo $version; ?></span>
			</div>
		</header> -->
		<div id="content">
			<?php
			if ($loggedin) {
				if ($_SESSION['isAdmin'] || $_SESSION['isGroupAdmin']) {
					if (array_key_exists('action', $_REQUEST)) {
						include ('html/' . $_REQUEST['action'] . '.php');
					} else {
						include ('html/admin_gui.html');
					}		
				} else {
					include ('../html/privileges.html');
				}
			} else {
				include ('../html/redirect.php');
			}
			?>
		</div>
        <div id="messages"></div>
	</body>
</html>
