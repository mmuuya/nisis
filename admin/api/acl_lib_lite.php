<?php
//Commonly used ACL functions.

//Get all roles in system, for ArcGIS.
//
//Returns 2d array, with keys:
//    "role" => "role name"
//    "description" => "description (but actually just the role name)"
function getAllRoles() {
    $query = 'SELECT name as "role", name as "description" FROM NISIS_ADMIN.GROUPS';

    $parsed = parse($query);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving all the roles(groups) in getAllRoles() function - acl_lib_lite.php");
    }
    fetch_all_rows($parsed, $results);
    return $results;
}

//Get all usernames in system, simple array.
//
//If $long is specified as TRUE, the long version as an associative array
//is returned with the following keys:
//    "usrid" => "17"
//    "usrname" => "User Name"
//    "fname" => "First"
//    "lname" => "Last"
//    "email" => "email@address"
//    "apps" => "NISIS,TFR"
function getAllUsers($long=FALSE) {
    $query = 'SELECT userid as "usrid", username as "usrname", '
            . 'fname as "fname", lname as "lname", email as "email", apps as "apps"
                FROM APPUSER.USER_APPS_VW';

    $parsed = parse($query);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving all the users in getAllUsers() function - acl_lib_lite.php");
    }
    fetch_all_rows($parsed, $results);

    if($long) {
        return $results;
    }
    return flattenAss($results, "usrname");
}

//Get all users in specified role.
//
//Param $group can be an int to specify the group id.
//Otherwise, $group is assumed to be a string and specifying the group name.
//
//Returns a simple array containing each user as a string if
//the group name is specified.
//OR
//Returns an associative array if the group id is specified. Keys:
//    "id" => "17"
//    "usrname" => "User Name"
function getUsersInRole($group) {
    $isID = is_int($group);

    $query = 'SELECT a.userid as "id", a.username as "usrname", b.priv as "usrrole" '
           . 'FROM APPUSER.USERS a '
           . 'JOIN NISIS_ADMIN.USER_GROUP b ON a.userid = b.userid '
           . 'JOIN NISIS_ADMIN.GROUPS c ON b.groupid = c.groupid ';

    if($isID) {
        $query .= 'WHERE c.groupid=:thegrp';
    } else {
        $query .= 'WHERE c.name=:thegrp';
    }

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":thegrp", $group);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving all the users in the role(group): $group in getUsersInRole() function - acl_lib_lite.php");
    }
    fetch_all_rows($parsed, $results);

    if($isID) {
        return $results;
    }

    return flattenAss($results, "usrname");
}

//Get all the roles a user belongs to, simple array.
function getUserRoles($username) {
    $query = 'SELECT a.name as "role" from NISIS_ADMIN.GROUPS a '
           . 'JOIN NISIS_ADMIN.USER_GROUP b ON a.groupid=b.groupid '
           . 'JOIN APPUSER.USERS c ON c.userid=b.userid '
           . 'WHERE c.username=:usename';

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":usename", $username);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving all the roles(groups) for the user: $username in getUserRoles() function - acl_lib_lite.php");
    }
    fetch_all_rows($parsed, $results);

    return flattenAss($results, "role");
}

// Get all the group id's belonging to the user specified
// NOTE - DO NOT USE THIS FUNCTION! The user's group id's are saved in the session on login.php
function getUserGroupIDs($userid) {
    $query = 'SELECT GROUPID FROM NISIS_ADMIN.USER_GROUP
                WHERE USERID=:usrid';

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":usrid", $userid);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving all the roles(groups) in an array with IDs for the user: $userid in getUserGroupIDs() function - acl_lib_lite.php");
    }
    fetch_all_rows($parsed, $results);

    return flattenAss($results, "GROUPID");
}

//Get information about a user (currently just need userid as a string)
function getUserInfo($username) {
    // $query = 'SELECT username as "user", userid as "usrid" '
    //        . 'FROM APPUSER.USERS WHERE username=:usename';

    $query = 'SELECT userid as "user" FROM APPUSER.USERS '
           . 'WHERE username=:usename';

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":usename", $username);
     if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving the user information for the user: $username in getUserInfo() function - acl_lib_lite.php");
    }
    $res = oci_fetch_assoc($parsed);
    return $res["user"];
}

//Flatten a multi dimensional associative array into a much simpler
//array with int indices. Loops over the outer array, pushing the value
//of the key specified to the resulting array.
//
//$assArray is the associative array to flatten
//$key specifies which item on each row will get added to the array
function flattenAss($assArray, $key) {
    $result = array();

    foreach ($assArray as $item) {
        array_push($result, $item[$key]);
    }

    return $result;
}

//wrap oci_fetch_all with flags to fetch by row and get an associative array
function fetch_all_rows($statementID, &$output) {
    oci_fetch_all($statementID, $output, 0, -1, OCI_FETCHSTATEMENT_BY_ROW+OCI_ASSOC);
}

//wrap oci_parse() - don't need to reference $db everywhere
function parse($queryStr) {
    global $db;
    return oci_parse($db, $queryStr);
}

//Lookup username from id
function usernameLookup($userid){
    $query = 'SELECT USERNAME FROM APPUSER.USERS '
            . 'WHERE userid=:userid';

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":userid", $userid);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving the username for the user: $userid in usernameLookup() function - acl_lib_lite.php");
    }

    $res = oci_fetch_assoc($parsed);
    return $res["USERNAME"];
}

?>
