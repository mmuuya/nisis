<?php
//Actions for ACL stuff

require "acl_lib.php";

//ACTIONS
$function = $_POST['function'];
if (isset($function)) {
	$result = array("result" => "Success");

	switch($function) {
		case "getUsers":
			$result['users'] = getAllUsers(TRUE);
		break;
		case "getAvailableApps":
			$result['apps'] = getAvailableApps();
		break;
		case "getAllNISISGroups":
			$result['groups'] = getAllNISISGroups();
		break;
		case "getusergroups":
			$id = $_POST['id'];
			if($id){
				$result['items'] = getUserGroups($id);
			} else {
				kill($result, FALSE);
			}
		break;
		case "getGroupsForLoggedUser":
			$id = $_SESSION['userid'];
			if($id){
				$result['groups'] = getGroupsForLoggedUser($id);
			} else {
				kill($result, FALSE);
			}
		break;
		case "getalltables":
			$result['tables'] = getAllTables();
		break;
		case "getUsersFromNISISGroup":
			$id = $_POST['groupId'];
			$result['members'] = getUsersFromNISISGroup($id);
		break;
		case "getUsersVisibleToUser":
            $id = $_POST['id'];
            $result['items'] = getUsersVisibleToUser($id);
        break;
		case "getAllFeaturesVisibleToUser":
            $id = $_POST['id'];
            $itemtype = $_POST['itemtype'];
            $result['items'] = getAllFeaturesVisibleToUser($id,$itemtype);
        break;
        case "modUserRoleInNISISGroup":
			$userID = $_POST['userID'];
			$userRole = $_POST['userRole'];
			$userGroupID = $_POST['groupID'];
			modUserGroupAdminRole($userID, $userRole, $userGroupID);
		break;
		case "getLayersForNISISGroup":
			$id = $_POST['groupId'];
			$result['layers'] = getLayersForNISISGroup($id);
			error_log(var_export($result['layers'], true));
		break;
		case "getAllTFRGroups":
			$result['groups'] = getAllTFRGroups();
		break;
		case "getUsersFromTFRGroup":
			$id = $_POST['groupId'];
			$result['tfrMembers'] = getUsersFromTFRGroup($id);
		break;
		case "gettablefields":
			$readwrite = $_POST['readwrite'];
			$tableid = $_POST['ident'];
			$result['fields'] = getFields($tableid,$readwrite);
		break;
		case "getfieldrgroups":
			$fieldid = $_POST['ident'];
			$result['groups'] = getReadGroups($fieldid);
		break;
		case "getfieldwgroups":
			$fieldid = $_POST['ident'];
			$result['groups'] = getEditGroups($fieldid);
		break;
		case "modUser":
			$usrname = $_POST['username'];
			$first = $_POST['first'];
			$last = $_POST['last'];
			$email = $_POST['email'];
			$usrid = $_POST['id'];
			$apps = $_POST['apps'];
			if(isset($_POST['password'])){
				$pass = trim($_POST['password']);
			} else {
				$pass = "";
			}
			modUser($usrname, $first, $last, $email, $pass, $apps, $usrid);
		break;
		case "chPwd":
			$userid = $_SESSION['userid'];
			// $userid = $_POST['id'];
			$pass = trim($_POST['password']);
			chPwd($userid, $pass);
		break;
		case "modGroup":
			$groupName = $_POST['groupName'];
			$groupId = $_POST['groupId'];
			modGroup($groupName, $groupId);
		break;
		case "modTFRGroup":
			$groupName = $_POST['groupName'];
			$groupId = $_POST['groupId'];
			modTFRGroup($groupName, $groupId);
			break;
		case "modtable":
			$label = $_POST['label'];
			$tableID = $_POST['tableid'];
			modTable($tableID, $label);
		break;
		case "modfield":
			$label = $_POST['label'];
			$fieldID = $_POST['fieldid'];
			modField($fieldID, $label);
		break;
		case "assign":
			$idAssignTo = $_POST['idassignto'];
			$type = $_POST['type'];
			$items = $_POST['items'];
			foreach($items as $curItemID) {
				if ($type === "usrtogrp") {
					userToNISISGroup($curItemID, $idAssignTo);
				} else if ($type === "usrtotfrgrp") {
					userToTFRGroup($curItemID, $idAssignTo);
				} else if ($type === "grptofldr") {
					groupToRField($curItemID, $idAssignTo);
				} else if ($type === "grptofldw") {
					groupToWField($curItemID, $idAssignTo);
				} else if ($type === "grptotblr") {
					groupToRTable($curItemID, $idAssignTo);
				} else if ($type === "grptotblw") {
					groupToWTable($curItemID, $idAssignTo);
				} else if ($type === "grpuntblw") {
					//this one's weird, we used the assignment form, but we're removing each group's access from entire tables
					groupUnWTable($curItemID, $idAssignTo);
				} else if ($type === "grpuntblr") {
					groupUnRTable($curItemID, $idAssignTo);
				} else {
					kill("BAD CALL to Assign case in acl.php [$type]");
				}
			}
		break;
		case "unassign":
			$type = $_POST['type'];
			$mainID = $_POST['id'];
			$memberID = $_POST['memberid'];
			if ($type === "usrtogrp") {
				unassignUserFromNISISGroup($memberID, $mainID);
			} else if ($type === "usrtotfrgrp") {
				unassignUserFromTFRGroup($memberID, $mainID);
			} else if ($type === "grptofldr") {
				groupUnRField($memberID, $mainID);
			} else if ($type === "grptofldw") {
				groupUnWField($memberID, $mainID);
			} else {
				kill("ERROR - bad call to Unassign case in acl.php [$type]");
			}
		break;
		case "getusr":
			$userid = $_POST['id'];
			$result['item'] = getUser($userid);
		break;
		case "getgrp":
			$grpid = $_POST['id'];
			$result['item'] = getGroup($grpid);
		break;
		case "getavail":
			$type = $_POST['type']; //either 'usrtogrp' 'usrtotfrgrp' or 'grptocat'
			$mainID = $_POST['mainid'];
			if ($type === '') {
				// initial value on page load, no need to return results
				exit();
			}
			$result['available'] = getAvailable($type, $mainID);
		break;
		case "deleteItem":
			$type = $_POST['type'];
			$id = $_POST['id'];
			deleteItem($type, $id);
		break;
		default:
			$result['result'] = "ERROR - ACL could not determine function";
		break;
	}

	//remember, kill args:
	//    0: array or string to encode as json
	//    1: specify whether to write it to the error log (TRUE by default)
	kill($result, FALSE);
}

kill("Bad call to ACL.");
?>
