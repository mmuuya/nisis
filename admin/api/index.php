<?php 
//Get our configurations and global stuff, or fail if we can't.
if ((include '../../handler.php') === FALSE) {
	exit(json_encode(array('return' => 'Misconfigured Server')));
}

//Check to make sure this request has an action
if(!array_key_exists('action', $_REQUEST)){
	kill('Admin API Malformed Request: No Action Specified');
}

$loggedin = isset($_SESSION['username']);
if(!$loggedin && $_REQUEST['action']!=='report' && $_REQUEST['action']!=='login' && $_REQUEST['action']!=='reset_pw' && $_REQUEST['action']!=='change_pw'){
	kill('Admin API Security Violation.  Please Log In.');
}

//Check to make sure this request makes sense--i.e., we have an API point for it, and call it if we do.
if((include $_REQUEST['action'].'.php') === FALSE){
	kill('Admin API Malformed Request: No API Endpoint (' . $_REQUEST['action'] . ')');
}
?>