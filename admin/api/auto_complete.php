<?php
require ('../handler.php');

if ($db === FALSE) {
	kill('No Database Connection');
}

//Get user-entered search term
$term = strtoupper($_GET["term"]);

//Set up a counter, and maxnum variable to return a maximum of 5 results
$count = 1;
$maxresult = 5;

//Initialize return result JSON object
$json = array();

//Generate queries and result sets based off of term
$icaochecksql		= oci_parse($db, "SELECT * FROM (SELECT OBJECTID, LOCID, FACNAME FROM SDE.map_nfdc_airports WHERE UPPER(locid) LIKE '%'|| :term ||'%'  OR 'K' || UPPER(locid) LIKE :term ||'%') WHERE ROWNUM <=5");
$citychecksql 		= oci_parse($db, "SELECT * FROM (SELECT DISTINCT OBJECTID, CITY, STATE FROM SDE.map_nfdc_airports WHERE upper(city) LIKE :term || '%') WHERE ROWNUM <= 5 ");
$statechecksql 		= oci_parse($db, "SELECT * FROM (SELECT DISTINCT OBJECTID, STATENAME FROM SDE.map_nfdc_airports WHERE upper(state) LIKE :term || '%' OR UPPER(statename) LIKE :term || '%')  WHERE ROWNUM <= 5");
$airportchecksql	= oci_parse($db, "SELECT * FROM (SELECT OBJECTID, LOCID, FACNAME, STATE, CITY FROM SDE.map_nfdc_airports WHERE UPPER(facname) LIKE '%'|| :term ||'%'  OR UPPER(state) LIKE '%'|| :term ||'%' OR UPPER(city) LIKE '%'|| :term ||'%'  ORDER BY FACNAME) WHERE ROWNUM <= 5 ");

//Associate search term with variable(s)
oci_bind_by_name($icaochecksql, ':term', $term);
oci_bind_by_name($citychecksql, ':term', $term);
oci_bind_by_name($statechecksql, ':term', $term);
oci_bind_by_name($airportchecksql, ':term', $term);

//Check at ICAO level: Priority 1
oci_execute($icaochecksql);
while($icao = oci_fetch_array($icaochecksql)){
	if ($icao["LOCID"] != null && $count <= $maxresult){
		$json[] = array("category" => "ICAO",
						"label" => ucwords($icao["LOCID"]) . " - " . ucwords(strtolower($icao["FACNAME"])),
						"value" => ucwords($icao["LOCID"]),
						"id" => $icao["OBJECTID"]);
		$count++;
	}	
}

//Check at State level: Priority 2
oci_execute($statechecksql);
while($state = oci_fetch_array($statechecksql)){
	if ($state["STATENAME"] != null && $count <= $maxresult){
		$json[] = array("category" => "States",
						"label" => ucwords(strtolower($state["STATENAME"])),
						"id" => $state["OBJECTID"]);
		$count++;
	}	
}

//Check at City level: Priority 3
oci_execute($citychecksql);
while($city = oci_fetch_array($citychecksql)){
	if ($city["CITY"] != null && $count <= $maxresult){
		$json[] = array("category" => "City",
						"label" => ucwords(strtolower($city["CITY"])) . ", " . $city["STATE"],
						"id" => $city["OBJECTID"]);
		$count++;
	}	
}

//Check at Airportname level: Priority 4
oci_execute($airportchecksql);
while ($airport = oci_fetch_array($airportchecksql)) {
	// $json[] = ucwords($airport["FACNAME"] . ", " . $airport["CITY"] . " " . $airport["STATE"]);
	if ($count <= $maxresult){
		$json[] = array("category" => "Airports",
						"label" => $airport["LOCID"] . " - " . ucwords(strtolower($airport["FACNAME"] . ", " . $airport["CITY"])) . ", " . $airport["STATE"], 
						"value" => ucwords(strtolower($airport["FACNAME"])),
						"id" => $airport["OBJECTID"]);
		$count++;
	}
}
echo json_encode($json);
