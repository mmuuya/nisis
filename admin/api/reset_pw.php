<?php
if ($db == FALSE) {
	kill('No Database Connection');
}

//response is error by default, updated to success if all's well
$resp = array();
set_resp('Error', 'Bad call to reset_pw');

if(array_key_exists('username', $_REQUEST) && array_key_exists('email', $_REQUEST)) {
    #### REQUEST TOKEN - user entered username / email, or admin initiated password reset
    global $ora_dt_format, $php_dt_format;
    
    $username = trim($_REQUEST['username']);
    $email = trim($_REQUEST['email']);
    
    if ($username == '' || $email == '') {
        kill('Empty username or email.');
    }
    
    $email_query = 'SELECT EMAIL, USERID FROM APPUSER.USERS WHERE USERNAME = :uname';
    $parsedsql = oci_parse($db, $email_query);
    oci_bind_by_name($parsedsql, ':uname', $username);
    oci_execute($parsedsql);
    
    $num_rows = oci_fetch_all($parsedsql, $results);
    $badUser = "User/email not found.";
    if($num_rows > 1){
        kill('DB Error - Multiple emails found for user. Cannot reset password.');
    } else if($num_rows < 1){
        error_log("user not found in database, cannot reset password");
        kill($badUser, FALSE);
    }
    
    $result_userid = $results['USERID'][0];
    $result_email = $results['EMAIL'][0];
    if($email !== $result_email){
        error_log("user's database email does not match supplied email, cannot reset password");
        kill($badUser, FALSE);
    }
    
    //random bytes creates some messy, nonstandard characters which may be bad for the db
    $token = bin2hex(openssl_random_pseudo_bytes(15));
    
    $add_token_query = "INSERT INTO NISIS_ADMIN.PWTOKEN (TOKEN, USERID, CREATEDATE) values (:tok, :userid, to_date(:dt, :qformat))";
    
    $parsedsql = oci_parse($db, $add_token_query);
    oci_bind_by_name($parsedsql, ':tok', $token);
    oci_bind_by_name($parsedsql, ':userid', $result_userid);
    oci_bind_by_name($parsedsql, ':qformat', $ora_dt_format);

    //generate current time in PHP, in case the DB is in different time zone
    $cur_time = date($php_dt_format);
    oci_bind_by_name($parsedsql, ':dt', $cur_time);

    // Suppressed warnings are generally bad, but there's not a straightforward way to
    // try/catch a warning. Initialize a false status in case we miss the warning.
    $query_status = false;
    $query_status = @oci_execute($parsedsql);
    
    if(!$query_status) {
        //php_errormsg holds prev err thrown by php (oci_execute in our case)
        //BUT php_errormsg can only be used if 'track_errors' is on in php.ini
        error_log($php_errormsg);
        kill('Database error when creating token.');
    } else {
        $url = $root . '/?token=' . $token;
        
        // $body = PHP_EOL;
        // $body .= 'A request has been made to reset your NISIS password.' . PHP_EOL;
        // $body .= 'Your unique token follows. Copy without the leading and trailing hyphens.' . PHP_EOL;
        // $body .= '-----------------' . PHP_EOL;
        // $body .= $token . PHP_EOL;
        // $body .= '-----------------' . PHP_EOL;
        // $body .= 'Follow this url and enter your token into the form: ' . $url . PHP_EOL;

        $body = "A password reset has been initiated for your NISIS account. Please follow this unique URL to complete the password reset process (it will expire in 30 minutes): " . PHP_EOL . $url;
        
        // error_log('Attempting to email. Token will be sent to $email');
        if (mail($email, "NISIS Password Reset", $body, "From: nisis_administration")){
            set_resp('Success', 'A reset token has been sent to the email on file.', $url);
        } else {
            error_log("The following email failed to send - is mail server running?");
            error_log($body);
            set_resp('Email could not be sent.', 'Could not send email. Is the mail server running?');
        }
    }
    
    #### END REQUEST TOKEN
}

//if things are good, response was updated. otherwise, default response is err
kill($resp, FALSE);

//utility function to update the response
//result: should a "Success" or "Error"
//message: a message string to include
//redirect: [optional] add a full url the page should redirect to
function set_resp($rslt, $msg, $redirect = '') {
    global $resp;
    $resp['result'] = $rslt;
    $resp['message'] = $msg;
    if($redirect !== ''){
        $resp['redirect'] = $redirect;
    }
}

?>
