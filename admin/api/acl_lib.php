<?php
// Access Control database query functions. All the queries you can handle
// for users, groups, and access categories.
//
// An INSERT command will generally return TRUE OR FALSE as returned from
// oci_execute.
//
// A SELECT command will generally return a 2d associative array. The outer
// array has integer indices, with each row having the keys as described.
///////////////////////////

require "acl_lib_lite.php"; //should already be included by handler


//Get all the groups that exist. Keys:
//    "grpid" => "10"
//    "grpname" => "Group Name"
function getAllNISISGroups() {
	$query = 'SELECT groupid as "grpid", name as "grpname"
				FROM NISIS_ADMIN.GROUPS';

	$parsed = parse($query);
	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR retrieving all the NISIS groups in getAllNISISGroups() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);
	return $results;
}

//Get user groups : input ==> userid
//    "grpid" => "10"
//    "grpname" => "Group Name"
function getUserGroups($userid) {

	if(!isset($_SESSION['userGroups'])){
	
		$query = 'SELECT g.groupid, g.name as "GNAME", ug.priv FROM NISIS_ADMIN.GROUPS g
				JOIN NISIS_ADMIN.USER_GROUP ug ON g.groupid = ug.groupid
				JOIN APPUSER.USERS u ON u.userid = ug.userid
				WHERE u.userid = ' . $userid;

		$parsed = parse($query);
		if(!oci_execute($parsed)) {
			kill("DATABASE ERROR retrieving all user groups in getUserGroups() function - acl_lib.php");
		}
		//oci_bind_by_name($parsed, ":userid", $userid);
		fetch_all_rows($parsed, $results);

		//CC - NISIS-2202: Adding the user groups to a session variable to use it to display user admin panels.
		$_SESSION['userGroups'] = $results;
		
		return $results;
	}else{
		return $_SESSION['userGroups'];
	}
}

//Get the NISIS Groups for the logged in user depending if the user is a NISIS Admin, a Group Admin or a normal user.
function getGroupsForLoggedUser($userid){
	if ($_SESSION['isAdmin']){
		$results = getAllNISISGroups();
		return $results;
	}else {
		if($_SESSION['isGroupAdmin']){
			$userGroups = getUserGroups($userid);
			$results = array();
			for ($i = 0; $i<count($userGroups); $i++){
				if($userGroups[$i]['PRIV'] === "3"){
					array_push($results, array('grpid' => $userGroups[$i]['GROUPID'], 'grpname' => $userGroups[$i]['GNAME']));
				}
			}
			return $results;
		}else{
			return array();
		}
	}
}

function getUsersVisibleToUser($userid) {
	$query = 'SELECT DISTINCT a.userid as "USERID", a.username as "USRNAME"'
		   . 'FROM APPUSER.USERS a '
		   . 'JOIN NISIS_ADMIN.USER_GROUP b ON a.userid = b.userid '
		   . 'JOIN NISIS_ADMIN.GROUPS c ON b.groupid = c.groupid '
		   . 'WHERE c.groupid IN (SELECT groupid FROM nisis_admin.user_group WHERE userid = :userid) AND b.userid <> :userid ORDER BY LOWER(a.username)';

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":userid", $userid);
	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR retrieving all the users visible to user: $userid in getUsersVisibleToUser() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);

	return $results;
}
function getAllFeaturesVisibleToUser($userid, $itemtype) {

    $query = 'SELECT NISIS_ADMIN.fm_pkg.get_user_features_all(:userid,:itemtype) as "clobresult" FROM DUAL';

    $parsed = parse($query);
    oci_bind_by_name($parsed, ":userid", $userid);
    oci_bind_by_name($parsed, ":itemtype", $itemtype);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR retrieving all the users visible to user: $userid in getAllFeaturesVisibleToUser() function - acl_lib.php");
    }
    $result = oci_execute($parsed);
     if($result !== false){
        while($row = oci_fetch_assoc($parsed)){
            $res = $row['clobresult'];
            //echo $res->load();
            return $res->load();
        }
    }
}

// Get all the tables that are editable. Keys:
//    "tableid" => "3"
//    "table" => "NISIS_APTS"
//    "label" => "Airports"
function getAllTables() {
	$query = 'SELECT TABLEID as "tableid", TABLE_NM as "table", DISPLAY_NM as "label"
		FROM NISIS_ADMIN.ACL_TABLES
		ORDER BY TABLE_NM ASC';

	$parsed = parse($query);
	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR retrieving all the tables in getAllTables() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);

	return $results;
}

// Get available users or groups which can be assigned
// to an item depending on the type provided. Does not return items
// which are already part of the specified id.
//
// Returns keys:
//    "itemid" => "7"
//    "itemlabel" => "Item Label"
function getAvailable($type, $mainID) {
	$needsBind = TRUE;
	if ($type === 'usrtogrp') {
		$query = 'SELECT C.USERID as "itemid", (C.FNAME || \' \' || C.LNAME || \' (\' || C.USERNAME || \')\') as "itemlabel"
					FROM APPUSER.USERS C
					FULL JOIN
					(
						SELECT A.* FROM APPUSER.USERS A
						JOIN NISIS_ADMIN.USER_GROUP B
						ON A.USERID = B.USERID WHERE B.GROUPID=:mainid
					) D
					ON D.USERID = C.USERID
					WHERE D.USERNAME IS NULL';
	} else if ($type === 'usrtotfrgrp') {
		$query = 'SELECT C.USERID as "itemid", (C.FNAME || \' \' || C.LNAME || \' (\' || C.USERNAME || \')\') as "itemlabel"
					FROM APPUSER.USERS C
					FULL JOIN
					(
						SELECT A.* FROM APPUSER.USERS A
						JOIN TFRUSER.USER_GROUP B
						ON A.USERID = B.USERID WHERE B.GROUPID=:mainid
					) D
					ON D.USERID = C.USERID
					WHERE D.USERNAME IS NULL';
	} else if ($type === 'grptofldr' || $type === 'grptofldw') {
		//utterly ridiculous...
		$query =
			'SELECT XX.GROUPID as "itemid", XX.NAME as "itemlabel"
			FROM NISIS_ADMIN.GROUPS XX
			FULL JOIN
			(
				SELECT A.GROUPID, A.NAME, B.ACLID, C.FIELDID, C.PERM FROM NISIS_ADMIN.GROUPS A
				JOIN NISIS_ADMIN.ACL_GROUPS B
				ON A.GROUPID=B.GROUPID
				JOIN NISIS_ADMIN.ACL_PERMS C
				ON B.ACLID=C.ACLID ';
				if ($type === 'grptofldw') {
					$query .= 'WHERE C.PERM=\'w\' ';
				} else {
					$query .= 'WHERE C.PERM=\'r\' ';
				}
		$query .= 'AND C.FIELDID=:mainid
			) YY
			ON XX.GROUPID=YY.GROUPID
			WHERE YY.NAME IS NULL';
	} else if ($type === 'grptotblw' || $type === 'grpuntblw' || $type === 'grptotblr' || $type === 'grpuntblr') {
		//just get all the groups, no need to filter with mainid
		$query = 'SELECT groupid as "itemid", name as "itemlabel"
					FROM NISIS_ADMIN.GROUPS';
		$needsBind = FALSE;
	} else {
		kill("BAD CALL to getAvailable() function in acl_lib.php [$type]");
	}

	$parsed = parse($query);
	if ($needsBind) {
		oci_bind_by_name($parsed, ":mainid", $mainID);
	}
	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR retrieving all available items, type: $type, mainID: $mainID in getAvailable() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);

	return $results;
}

// Get the users belonging to a group. Keys:
//    "id" => "17"
//    "usrname" => "User Name"
function getUsersFromNISISGroup($groupid) {
	if($groupid === "-1") {
		$defaultMsg = "Click a group to see its users";
		return array(array(
			"usrname" => $defaultMsg,
			"id" => "-1",
			"usrrole" => ""
		));
	}

	$id_int = intval($groupid);
	return getUsersInRole($id_int);
}

// Modify the selected user's Group Admin role
function modUserGroupAdminRole($userID, $userRole, $userGroupID){

	$query = "UPDATE NISIS_ADMIN.USER_GROUP
				SET PRIV=:userRole
				WHERE USERID=:userID
				AND GROUPID=:userGroupID";

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":userRole", $userRole);
	oci_bind_by_name($parsed, ":userID", $userID);
	oci_bind_by_name($parsed, ":userGroupID", $userGroupID);

	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR modifying the acl table: $userRole in modUserGroupAdminRole() function - acl_lib.php");
	}
}

// Get the layers permissions tree for a group. Keys:
//      "layerid": "703",
//      "layername": "DOTMap Tracked Objects",
//      "parentid": "0",
//      "isfolder": "1",
//      "longname": "DOTMap Tracked Objects",
//      "children": Array of Items[],
function getLayersForNISISGroup($groupid) {
	if($groupid === "-1") {
		$defaultMsg = "Click a group to get the layers tree";
		return array(array(
			"layerid" => "0",
			"layername" => "root",
			"parentid" => "0",
			"isfolder" => "1",
			"hasChildren" => false,
			"longname" => $defaultMsg
		));
	 }else{
		$query = "BEGIN
					:tree := nisis.get_layer_tree_json(0);
					END;";

		$parsed = parse($query);
		oci_bind_by_name($parsed, ":tree", $tree, 20000);

		if(!oci_execute($parsed)) {
			kill("DATABASE ERROR when trying to get the layers tree for the group: $group in getLayersForNISISGroup() function - acl_lib.php");
		}

		error_log("treeee: " . $tree);
		return array(json_decode($tree));
	}
}

//Get all the TFR groups that exist. Keys:
//    "grpid" => "10"
//    "grpname" => "Group Name"
function getAllTFRGroups() {
	$query = 'SELECT groupid as "grpid", name as "grpname"
				FROM TFRUSER.GROUPS';

	$parsed = parse($query);
	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR retrieving all the TFR groups in getAllTFRGroups() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);
	return $results;
}

//Get all users from a TFR Group.
//Returns an associative array if the group id is specified. Keys:
//    "id" => "17"
//    "usrname" => "User Name"
function getUsersFromTFRGroup($id) {
	if($id === "-1") {
		$defaultMsg = "Click a TFR group to see its users";
		return array(array(
			"usrname" => $defaultMsg,
			"id" => "-1"
		));
	}else{
		$query = 'SELECT a.username as "usrname", a.userid as "id"
			FROM APPUSER.USERS a
			JOIN TFRUSER.USER_GROUP b ON a.userid = b.userid
			JOIN TFRUSER.GROUPS c ON b.groupid = c.groupid
			WHERE c.groupid=:groupid';

		$parsed = parse($query);
		oci_bind_by_name($parsed, ":groupid", $id);

		if(!oci_execute($parsed)) {
			kill("DATABASE ERROR retrieving the users for the TFR group: " . $id . " in getUsersFromTFRGroup() function - acl_lib.php");
		}

		fetch_all_rows($parsed, $results);

		return $results;
	}
}

// Get the fields belonging to a table. Keys:
//    "fieldid" => "ID"
//    "field" => "ORA_COL_NM"
//    "label" => "Display Name"
function getFields($tableid, $readwrite) {
	if($tableid === "-1") {
		$defaultMsg = "Click a table to see its fields";
		return array(array(
			"fieldid" => "-1",
			"field" => "DEFAULT_MSG",
			"label" => $defaultMsg,
		));
	}
    if ($tableid == 4 || $tableid == 5 || $tableid == 8 || $tableid == 9 || $tableid == 24 ){
		$query = "SELECT FIELDID as \"fieldid\", COL_NM as \"field\", UPPER (display_nm) ||' ('||LOWER(col_nm)||')' AS \"label\"
			FROM NISIS_ADMIN.ACL_FIELDS WHERE TABLEID=:tableid
			AND FIELDID IN (SELECT FIELDID FROM NISIS_ADMIN.ACL_PERMS WHERE PERM = :readwrite)  AND COL_NM IN ( SELECT col_nm FROM NISIS_ADMIN.PROFILE_FIELDS WHERE tableid = :tableid)
			ORDER BY UPPER(DISPLAY_NM) ASC";
    } else {
    	$query = "SELECT FIELDID as \"fieldid\", COL_NM as \"field\", UPPER (display_nm) ||' ('||LOWER(col_nm)||')' AS \"label\"
			FROM NISIS_ADMIN.ACL_FIELDS WHERE TABLEID=:tableid
			AND FIELDID IN (SELECT FIELDID FROM NISIS_ADMIN.ACL_PERMS WHERE PERM = :readwrite)
			ORDER BY UPPER(DISPLAY_NM) ASC";
    }

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":tableid", $tableid);
	oci_bind_by_name($parsed, ":readwrite", $readwrite);
	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR retrieving " . $readwrite . " fields for a table: " . $tableid . " in getFields() function - acl_lib.php");
	}

	fetch_all_rows($parsed, $results);

	return $results;
}

// Get the groups which are allowed to read the specified field.
// Keys:
//    "grpid" => "15"
//    "grpname" => "Group Name"
function getReadGroups($fieldid) {
	return getFieldGroups(FALSE, $fieldid);
}

// Get the groups which are allowed to write to the specified field.
// Keys:
//    "grpid" => "15"
//    "grpname" => "Group Name"
function getEditGroups($fieldid) {
	return getFieldGroups(TRUE, $fieldid);
}

// Query the groups associated with the field.
// $writable can be TRUE or FALSE to get the read fields
// Keys:
//    "grpid" => "15"
//    "grpname" => "Group Name"
function getFieldGroups($writable, $fieldid) {
	if($fieldid === "-1") {
		$default = array(array(
			"grpid" => "-1"
		));

		if($writable){
			$default[0]["grpname"] = "Click a field to see which groups can edit";
		} else {
			$default[0]["grpname"] = "Click a field to see which groups can read";
		}
		return $default;
	}

	$query = 'SELECT a.GROUPID as "grpid", a.NAME as "grpname"
				FROM NISIS_ADMIN.GROUPS a
				JOIN NISIS_ADMIN.ACL_GROUPS b ON a.GROUPID=b.GROUPID
				JOIN NISIS_ADMIN.ACL_PERMS c ON b.ACLID=c.ACLID ';
	if ($writable) {
		$query .= 'WHERE PERM=\'w\' ';
	} else {
		$query .= 'WHERE PERM=\'r\' ';
	}
	$query .= 'AND FIELDID=:fieldid';

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":fieldid", $fieldid);

	if(!oci_execute($parsed)) {
		kill("DB ERROR retrieving groups for a field: $fieldid where writable=$writable in getFieldGroups() function - acl_lib.php");
	}

	fetch_all_rows($parsed, $results);
	return $results;
}

// //Modify (or create) a user with the information supplied.
// //
// //If usrid is greater than or equal to 0, that user will be modified.
// //By default, the userid will be -1 which specifies a new user will be created.
// function modUser($usrname, $first, $last, $email, $pass, $apps, $usrid="-1", $active=1) {
// 	$result = "";
// 	if ($usrid === "-1") {
// 		if($pass === "") {
// 			kill("ERROR - password cannot be empty string for new user in modUser() function - acl_lib.php.");
// 		}else if($apps === null){
// 			kill("ERROR - Apps cannot be an empty array for new user in modUser() function - acl_lib.php.");
// 		}

// 		/*for new user this function returns the userId assigned in the database for the user or an error as these:
// 		err_no_app       NUMBER := -1;
// 		err_no_userid    NUMBER := -2;
// 		err_no_username  NUMBER := -3;
// 		err_dup_usename  NUMBER := -4;
// 		err_empty_field  NUMBER := -5;
// 		err_other        NUMBER := -6; */
// 		$query = "BEGIN :userId := APPUSER.USER_ACCT_PG.CREATE_USER (
// 					:username,
// 					:fname,
// 					:lname,
// 					:email,
// 					:pwhash,
// 					:salt,
// 					:apps);
// 					END;";

// 		$parsed = parse($query);
// 		oci_bind_by_name($parsed, ":userId", $usrid, 2000);
// 		oci_bind_by_name($parsed, ":username", $usrname);
// 		oci_bind_by_name($parsed, ":fname", $first);
// 		oci_bind_by_name($parsed, ":lname", $last);
// 		oci_bind_by_name($parsed, ":email", $email);
// 		oci_bind_array_by_name($parsed, ":apps", $apps, 5, -1, SQLT_CHR);

// 	} else {
// 		if($apps === null){
// 			kill("ERROR - Apps cannot be an empty array for new user in modUser() function - acl_lib.php.");
// 		}else if($pass === "") {
// 			$salt = "";
// 			$hashedpw = "";
// 		}

// 		/*for modifying user the return is 1 if the user was updated successfully or an error as these:
// 		err_no_app       NUMBER := -1;
// 		err_no_userid    NUMBER := -2;
// 		err_no_username  NUMBER := -3;
// 		err_dup_usename  NUMBER := -4;
// 		err_empty_field  NUMBER := -5;
// 		err_other        NUMBER := -6; */
// 		$query = "BEGIN :result := APPUSER.USER_ACCT_PG.UPDATE_USER (
// 					:userId,
// 					:fname,
// 					:lname,
// 					:email,
// 					:pwhash,
// 					:salt,
// 					:apps,
// 					:active);
// 					END;";

// 		$parsed = parse($query);

// 		oci_bind_by_name($parsed, ":result", $result);
// 		oci_bind_by_name($parsed, ":userId", $usrid);
// 		oci_bind_by_name($parsed, ":fname", $first);
// 		oci_bind_by_name($parsed, ":lname", $last);
// 		oci_bind_by_name($parsed, ":email", $email);
// 		oci_bind_array_by_name($parsed, ":apps", $apps, 5, -1, SQLT_CHR);
// 		oci_bind_by_name($parsed, ":active", $active);
// 	}

// 	if ($pass !== ""){
// 		$salt = uniqid();
// 		$hashedpw = hash('sha512', $salt . $pass);
// 	}

// 	oci_bind_by_name($parsed, ":pwhash", $hashedpw);
// 	oci_bind_by_name($parsed, ":salt", $salt);

// 	if(!oci_execute($parsed)) {
// 		kill("DATABASE ERROR Modifying the user in modUser() function - acl_lib.php.");
// 	}
// }


// NISIS-2977 KOFI HONU 
// Error handling not done right for when stored proc fails.
// ----------------------------------------
//Modify (or create) a user with the information supplied.
//
//If usrid is greater than or equal to 0, that user will be modified.
//By default, the userid will be -1 which specifies a new user will be created.
function modUser($usrname, $first, $last, $email, $pass, $apps, $usrid="-1", $active=1) {
	$result = "";
	if ($usrid === "-1") {
		if($pass === "") {
			kill("ERROR - password cannot be empty string for new user in modUser() function - acl_lib.php.");
		}else if($apps === null){
			kill("ERROR - Apps cannot be an empty array for new user in modUser() function - acl_lib.php.");
		}

		/*for new user this function returns the userId assigned in the database for the user or an error as these:
		err_no_app       NUMBER := -1;
		err_no_userid    NUMBER := -2;
		err_no_username  NUMBER := -3;
		err_dup_usename  NUMBER := -4;
		err_empty_field  NUMBER := -5;
		err_other        NUMBER := -6; */
		$query = "BEGIN :result := APPUSER.USER_ACCT_PG.CREATE_USER (
					:username,
					:fname,
					:lname,
					:email,
					:pwhash,
					:salt,
					:apps);
					END;";

		$parsed = parse($query);
		oci_bind_by_name($parsed, ":result", $result, 2000);
		oci_bind_by_name($parsed, ":username", $usrname);
		oci_bind_by_name($parsed, ":fname", $first);
		oci_bind_by_name($parsed, ":lname", $last);
		oci_bind_by_name($parsed, ":email", $email);
		oci_bind_array_by_name($parsed, ":apps", $apps, 5, -1, SQLT_CHR);

	} else {
		if($apps === null){
			kill("ERROR - Apps cannot be an empty array for new user in modUser() function - acl_lib.php.");
		}else if($pass === "") {
			$salt = "";
			$hashedpw = "";
		}

		/*for modifying user the return is 1 if the user was updated successfully or an error as these:
		err_no_app       NUMBER := -1;
		err_no_userid    NUMBER := -2;
		err_no_username  NUMBER := -3;
		err_dup_usename  NUMBER := -4;
		err_empty_field  NUMBER := -5;
		err_other        NUMBER := -6; */
		$query = "BEGIN :result := APPUSER.USER_ACCT_PG.UPDATE_USER (
					:userId,
					:fname,
					:lname,
					:email,
					:pwhash,
					:salt,
					:apps,
					:active);
					END;";

		$parsed = parse($query);

		oci_bind_by_name($parsed, ":result", $result);
		oci_bind_by_name($parsed, ":userId", $usrid);
		oci_bind_by_name($parsed, ":fname", $first);
		oci_bind_by_name($parsed, ":lname", $last);
		oci_bind_by_name($parsed, ":email", $email);
		oci_bind_array_by_name($parsed, ":apps", $apps, 5, -1, SQLT_CHR);
		oci_bind_by_name($parsed, ":active", $active);
	}

	if ($pass !== ""){
		$salt = uniqid();
		$hashedpw = hash('sha512', $salt . $pass);
	}

	oci_bind_by_name($parsed, ":pwhash", $hashedpw);
	oci_bind_by_name($parsed, ":salt", $salt);

	if(!oci_execute($parsed)){
	    $err = oci_error($parsed);
	    $errStr = $err['message'];
	    kill(array('result' => 'DATABASE ERROR Modifying the user in modUser() function - acl_lib.php.', 'error' => $errStr));
	}
	else {    
	    kill(array('result' => $result), FALSE);    
	}

}


/* 
	FUNCTION RETURNS:
	----------------------
	-2 = userid is null
	-5 = password or salt is null
	0 = password not changed - probably invalid userid and couldn't find the userid in DB.
	1 = SUCCESS
	*/
	
function chPwd($userid, $pass) {
    $result = "";
    if ($userid === "" || $pass === "") {
        kill("ERROR - Userid and password cannot be empty in chPwd() function - acl_lib.php.");
    }

    $query = "BEGIN :result := APPUSER.USER_ACCT_PG.CHANGE_PWD (:userid, :pwhash, :salt); END;";

    $parsed = parse($query);

    $salt = uniqid();
    $hashedpw = hash('sha512', $salt . $pass);

    oci_bind_by_name($parsed, ":result", $result);
    oci_bind_by_name($parsed, ":userid", $userid);
    oci_bind_by_name($parsed, ":pwhash", $hashedpw);
    oci_bind_by_name($parsed, ":salt", $salt);

    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR Changeing pwd for the user in chPwd() function - acl_lib.php.");
    }
    return $result;
}

//Modify or create a NISIS group.
//
//If groupid is >= 0, that group gets modified
function modGroup($grpname, $grpid="-1") {
	if ($grpid === "-1") {
		$parsed = parse('INSERT INTO NISIS_ADMIN.GROUPS (groupid, name, datecreated) VALUES (NISIS_ADMIN.SEQ_GROUP_ID.nextVal, :name, SYSDATE)');
	} else {
		$parsed = parse('UPDATE NISIS_ADMIN.GROUPS SET name=:name WHERE groupid=:grpid');
		oci_bind_by_name($parsed, ":grpid", $grpid);
	}

	oci_bind_by_name($parsed, ":name", $grpname);

	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR modifying the NISIS group in modGroup() function - acl_lib.php");
	}
}

//Modify or create a TFR group.
//
//If groupid is >= 0, that group gets modified
function modTFRGroup($grpname, $grpid="-1") {
	if ($grpid === "-1") {
		$parsed = parse('INSERT INTO TFRUSER.GROUPS (groupid, name, datecreated) VALUES (TFRUSER.SEQ_GROUP_ID.nextVal, :name, SYSDATE)');
	} else {
		$parsed = parse('UPDATE TFRUSER.GROUPS SET name=:name WHERE groupid=:grpid');
		oci_bind_by_name($parsed, ":grpid", $grpid);
	}

	oci_bind_by_name($parsed, ":name", $grpname);

	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR modifying the TFR group in modTFRGroup() function - acl_lib.php");
	}
}

//Modify the label for an acl table.
function modTable($tableid, $label) {
	$query = "UPDATE NISIS_ADMIN.ACL_TABLES
				SET DISPLAY_NM=:label
				WHERE TABLEID=:tableid";

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":tableid", $tableid);
	oci_bind_by_name($parsed, ":label", $label);

	if(!oci_execute($parsed)) {
		kill("DATABASE ERROR modifying the acl table: $tableid in modTable() function - acl_lib.php");
	}
}

//Modify the label for an acl field
function modField($fieldid, $label) {
	$query = "UPDATE NISIS_ADMIN.ACL_FIELDS
				SET DISPLAY_NM=:label
				WHERE FIELDID=:fieldid";

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":label", $label);
	oci_bind_by_name($parsed, ":fieldid", $fieldid);

	if(!oci_execute($parsed)) {
		kill("DB ERROR modifying the acl field: $fieldid in modField() function - acl_lib.php");
	}
}

//Get user info to populate the form in the admin panel using the
//user id specified.
//
//Keys:
//    "fname" => "First"
//    "lname" => "Last"
//    "email" => "email@address"
//    "username" => "user.name"
function getUser($userid) {
	$query = 'SELECT username as "username", email as "email", fname as "fname", lname as "lname" FROM APPUSER.USERS WHERE userid=:userid';

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":userid", $userid);
	if(!oci_execute($parsed)) {
		kill("DB ERROR retrieving the user: $userid in getUser() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);

	return $results;
}

//Get the apps to populate the multiselect in the user form in the admin panel
//Returns keys:
//    "APPID" => "0"
//    "APPNAME" => "Nisis"
function getAvailableApps() {
	$query = 'SELECT APPID, APPNAME FROM APPUSER.APP';

	$parsed = parse($query);
	if(!oci_execute($parsed)) {
		kill("DB ERROR retrieving the apps in getAvailableApps() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);

	return $results;
}

//Get group info to populate the form in the admin panel.
//
//Keys:
//    "name" => "Group Name"
function getGroup($groupid) {
	$query = 'SELECT name as "name" FROM NISIS_ADMIN.GROUPS WHERE groupid=:groupid';

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":groupid", $groupid);
	if(!oci_execute($parsed)) {
		kill("DB ERROR retrieving the group: $groupid in getGroup() function - acl_lib.php");
	}
	fetch_all_rows($parsed, $results);

	return $results;
}

//Delete a user or group.
//Also removes any entries related to it in corresponding mapping tables.
//
//$type must be one of: "usr", "grp", "tfrgrp"
//$id must be the id of the item to be deleted
function deleteItem($type, $id) {
	global $db;
	switch ($type) {
		case "usr":
			$queries = array(
				'DELETE FROM APPUSER.USERS_APP WHERE userid=:id',
				'DELETE FROM NISIS_ADMIN.USER_GROUP WHERE userid=:id',
				'DELETE FROM TFRUSER.USER_GROUP WHERE userid=:id',
				'DELETE FROM APPUSER.USERS WHERE userid=:id',
				);
		break;
		case "grp":
			if($id === "0") {
				kill("ERROR - Administrators NISIS Group cannot be deleted.");
			}
			$queries = array(
				'DELETE FROM NISIS_ADMIN.USER_GROUP WHERE groupid=:id',
				'DELETE FROM NISIS_ADMIN.GROUPS WHERE groupid=:id',
				);
		break;
		case "tfrgrp":
			if($id === "0" || $id === "1") {
				kill("ERROR - TFR Group with ID: $id cannot be deleted.");
			}
			$queries = array(
				'DELETE FROM TFRUSER.USER_GROUP WHERE groupid=:id',
				'DELETE FROM TFRUSER.GROUPS WHERE groupid=:id',
				);
		break;
	}

	foreach ($queries as $statement) {
		$parsed = parse($statement);
		oci_bind_by_name($parsed, ":id", $id);

		if(!oci_execute($parsed, OCI_NO_AUTO_COMMIT)) {
			$e = oci_error($parsed);
			oci_rollback($db);  // rollback changes because all the queries were not executed successfully
			// trigger_error(htmlentities($e['message']), E_USER_ERROR);
			kill("DB ERROR trying to delete an item in deleteItem() function, type: " . $type . ", id:" . $id . " [" . var_export($queries, TRUE) . "]");
		}
	}

	// Commit the changes for all the queries
	if (!oci_commit($db)) {
		$e = oci_error($db);
		oci_rollback($db);  // rollback changes because all the queries were not executed successfully
		// trigger_error(htmlentities($e['message']), E_USER_ERROR);
		kill("DB ERROR trying to Commit the changes in deleteItem() function: " . $e['message']);
	}
}

//Assign a user to the specified NISIS group.
function userToNISISGroup($userid, $groupid) {
	if ($groupid === "0"){
		$query = "INSERT INTO NISIS_ADMIN.USER_GROUP (userid, groupid, priv) VALUES (:useid, :grpid, 3)";
	} else {
		$query = "INSERT INTO NISIS_ADMIN.USER_GROUP (userid, groupid) VALUES (:useid, :grpid)";
	}

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":useid", $userid);
	oci_bind_by_name($parsed, ":grpid", $groupid);

	return executeIgnoreUnique($parsed);
}

//Assign a user to the specified TFR group.
function userToTFRGroup($userid, $groupid) {
	$query = "INSERT INTO TFRUSER.USER_GROUP (userid, groupid) VALUES (:useid, :grpid)";

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":useid", $userid);
	oci_bind_by_name($parsed, ":grpid", $groupid);

	return executeIgnoreUnique($parsed);
}

//Assign a group to the specified field's read permission.
function groupToRField($groupid, $fieldid) {
	return groupToField(FALSE, $groupid, $fieldid);
}

//Assign a group to the specified field's write permission.
function groupToWField($groupid, $fieldid) {
	return groupToField(TRUE, $groupid, $fieldid);
}

//Assign a group to the specified field's write or read permission.
//
//$writable can be TRUE (or FALSE for read fields)
function groupToField($writable, $groupid, $fieldid) {
	$query = 'INSERT INTO NISIS_ADMIN.ACL_GROUPS (ACLID, GROUPID)
				VALUES (
					(SELECT ACLID FROM NISIS_ADMIN.ACL_PERMS WHERE FIELDID=:fldid ';
						if ($writable) {
							$query .= 'AND PERM=\'w\'),';
						} else {
							$query .= 'AND PERM=\'r\'),';
						}
				$query .= ':grpid
				)';

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":fldid", $fieldid);
	oci_bind_by_name($parsed, ":grpid", $groupid);

	return executeIgnoreUnique($parsed);
}

//Assign a group to EVERY field's READ permission in a table.
function groupToRTable($groupid, $tableid){
	groupToTable(FALSE, $groupid, $tableid);
}

//Assign a group to EVERY field's WRITE permission in a table.
function groupToWTable($groupid, $tableid){
	groupToTable(TRUE, $groupid, $tableid);
}

//Assign a group to EVERY field's write or read permission in a table.
//
//$writable must be TRUE or FALSE for assigning to the read permission
function groupToTable($writable, $groupid, $tableid){
	//use the pl/sql function in the database
	if($writable){
		$query = 'BEGIN :rowcnt := NISIS_ADMIN.MAP_GROUP_TO_TABLE(:grpid, :tblid); END;';
	} else {
		$query = 'BEGIN :rowcnt := NISIS_ADMIN.MAP_GROUP_TO_R_TABLE(:grpid, :tblid); END;';
	}

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":grpid", $groupid);
	oci_bind_by_name($parsed, ":tblid", $tableid);

	//:rowcnt must be bound even if we never use $rows_updated. Seems
	//to only get NULL which may be a bug in the pl/sql function.
	//Specify a maxlength to prevent "string buffer too small" oracle error.
	oci_bind_by_name($parsed, ":rowcnt", $rows_updated, 2000);

	if(!oci_execute($parsed)) {
		kill("DB ERROR assigning a group to edit all fields in a table in groupToTable() function - acl_lib.php");
	}
}

//Unassign a group from EVERY field's READ permission on a table
function groupUnRTable($groupid, $tableid) {
	groupUnTable(FALSE, $groupid, $tableid);
}

//Unassign a group from EVERY field's WRITE permission on a table.
function groupUnWTable($groupid, $tableid) {
	groupUnTable(TRUE, $groupid, $tableid);
}

//Unassign a group from EVERY field's write or read permission on a table
//
//$writable must be TRUE or false for READ permission
function groupUnTable($writable, $groupid, $tableid){
	$query = 'DELETE FROM NISIS_ADMIN.ACL_GROUPS
				WHERE ACLID IN
					(
					SELECT ACLID FROM NISIS_ADMIN.GRP_FIELDS_COMP_VIEW
					WHERE TABLEID=:tblid ';
	if($writable){
					$query .= 'AND PERM=\'w\'';
	} else {
					$query .= 'AND PERM=\'r\'';
	}
					$query .= ')
				AND GROUPID=:grpid';

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":tblid", $tableid);
	oci_bind_by_name($parsed, ":grpid", $groupid);

	if(!oci_execute($parsed)) {
		kill("DB ERROR removing a group from all fields in a table in groupUnTable() function - acl_lib.php");
	}
}

//Remove a user from a NISIS group.
function unassignUserFromNISISGroup($userid, $groupid) {
	$query = "DELETE FROM NISIS_ADMIN.USER_GROUP WHERE userid=:useid AND groupid=:grpid";
	$parsed = parse($query);
	oci_bind_by_name($parsed, ":useid", $userid);
	oci_bind_by_name($parsed, ":grpid", $groupid);
	//return oci_execute($parsed);
	if(!oci_execute($parsed)) {
		kill("DB ERROR removing the user: $userid from the NISIS group: $groupid in userUnGroup() function - acl_lib.php");
	}
}

//Remove a user from a TFR group.
function unassignUserFromTFRGroup($userid, $groupid) {
	$query = "DELETE FROM TFRUSER.USER_GROUP WHERE userid=:useid AND groupid=:grpid";
	$parsed = parse($query);
	oci_bind_by_name($parsed, ":useid", $userid);
	oci_bind_by_name($parsed, ":grpid", $groupid);
	// return oci_execute($parsed);
	if(!oci_execute($parsed)) {
		kill("DB ERROR removing the user: $userid from the TFR group: $groupid in userUnGroup() function - acl_lib.php");
	}
}

//Remove a group from being able to read a field.
function groupUnRField($groupid, $fieldid) {
	groupUnField(FALSE, $groupid, $fieldid);
}

//Remove a group from being able to write to a field.
function groupUnWField($groupid, $fieldid) {
	groupUnField(TRUE, $groupid, $fieldid);
}

//Remove a group from being associated with a write or read permission
//on a field.
//
//$writable can be TRUE (or FALSE for read fields)
function groupUnField($writable, $groupid, $fieldid) {
	error_log("writable: $writable | grpid: $groupid | fieldid: $fieldid");
	$query = "DELETE FROM NISIS_ADMIN.ACL_GROUPS
				WHERE GROUPID=:grpid
				AND ACLID=(
					SELECT ACLID FROM NISIS_ADMIN.ACL_PERMS
					WHERE FIELDID=:fldid ";
					if($writable) {
						$query .= "AND PERM='w')";
					} else {
						$query .= "AND PERM='r')";
					}

	$parsed = parse($query);
	oci_bind_by_name($parsed, ":grpid", $groupid);
	oci_bind_by_name($parsed, ":fldid", $fieldid);

	if(!oci_execute($parsed)) {
		kill("DB ERROR removing the NISIS group: $groupid from the field: $fieldid in groupUnField() function - acl_lib.php");
	}
}

//Call oci_execute and ignore unique constraint violations. If any
//other oracle error is thrown, kill() is called.
//
//$statement must be a statement handle as returned from oci_parse. Any
//oracle variables must be bound by oci_bind_by_name
function executeIgnoreUnique($statement) {
	if(!@oci_execute($statement)) {
		$err = oci_error($statement);
		if($err['code'] !== 1) { //code 1 is 'violated unique constraint'
			kill("DB ERROR in executeIgnoreUnique() function - acl_lib.php: "
				. $err['message']
				. " | "
				. $err['sqltext']
				);
		}
	}
	return TRUE;
}

?>
