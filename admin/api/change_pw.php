<?php
require "../../lib/check_pw_token.php";

if ($db == FALSE) {
    kill('No Database Connection');
}

//Initialize response to error
$resp = array();
set_resp('Error', 'Bad call to change_pw');

$token = trim($_POST['token']);
$new_pass1 = trim($_POST['passOne']);
$new_pass2 = trim($_POST['passTwo']);
unset($_POST['pw1']);
unset($_POST['pw2']);

if($new_pass1 === "" || $new_pass2 === ""){
    set_resp('Error', 'Both fields are required');
    kill($resp, FALSE);
}

if($new_pass1 !== $new_pass2) {
    set_resp('Error', 'Passwords are not the same.');
    kill($resp, FALSE);
}

if(!checkPwRules($new_pass1)) {
    unset($new_pass1);
    unset($new_pass2);
    set_resp('Error', 'Password does not conform to the password rules.');
    kill($resp, FALSE);
}

//let's get the userid and be sure the token is still good
$userid = checkPwToken($token, TRUE);
if(!$userid) {
    setBadTokenMsg();
    set_resp("Redirect", "$msgStr (before password could be changed) - '$token'", $root);
    kill($resp);
}

$salt = bin2hex(openssl_random_pseudo_bytes(10));
$hashedpw = hash('sha512', $salt . $new_pass1);
unset($new_pass1);
unset($new_pass2);

//change password for user on database
$chpw_query = 'UPDATE APPUSER.USERS SET password=:passwd, salt=:slt WHERE userid=:useid';
$parsedsql = oci_parse($db, $chpw_query);
oci_bind_by_name($parsedsql, ':passwd', $hashedpw);
oci_bind_by_name($parsedsql, ':slt', $salt);
oci_bind_by_name($parsedsql, ':useid', $userid);
if(!oci_execute($parsedsql)) {
    set_resp('Error', 'Error updating passwords.');
    kill($resp);
}

$parsed = oci_parse($db, 'DELETE FROM NISIS_ADMIN.PWTOKEN WHERE USERID=:usrid');
oci_bind_by_name($parsed, ":usrid", $userid);
oci_execute($parsed);

$_SESSION['pwResetMsg'] = "Your password has been reset.";
set_resp('Success', 'Password has been reset.', $root);
kill($resp, FALSE);

//utility function to update the response
//result: should a "Success" or "Error" or "Redirect"
//message: a message string to include
//redirect: [optional] add a full url the page should redirect to
function set_resp($rslt, $msg, $redirect = '') {
    global $resp;
    $resp['result'] = $rslt;
    $resp['message'] = $msg;
    if($redirect !== '') $resp['redirect'] = $redirect;
}

// Check the specified string conforms to the password rules.
//
// pw rules: 8 chars, 1 up, 1 low, 1 num, 1 special
function checkPwRules($str) {
    if (strlen($str) < 8) return FALSE;

    $specialChars = "!#@$%&+=?:^~{}[]<>()";
    $upFound = $lowFound = $numFound = $specFound = FALSE;

    for ($ii = 0; $ii < strlen($str); $ii++) {
        if ($upFound && $lowFound && $numFound && $specFound) return TRUE;

        $curChar = substr($str, $ii, 1);

        if (is_numeric($curChar)) $numFound = TRUE;
        //strpos is a stupid, awful, horrible function that returns an INT of the index if found, or FALSE if
        //it's not found. So you HAVE to check for 'not false' which is ridiculous
        else if (strpos($specialChars, $curChar) !== FALSE) $specFound = TRUE;
        else if (ctype_upper($curChar)) $upFound = TRUE;
        else if (ctype_lower($curChar)) $lowFound = TRUE;

        //use these for debugging
        //error_log('cur char: ' . $curChar );
        //error_log('-- up: ' . var_export($upFound, true) . ' - low: ' . var_export($lowFound, true) . ' - num: ' . var_export($numFound, true) . ' - spec: ' . var_export($specFound, true) . PHP_EOL);
    }

    if ($upFound && $lowFound && $numFound && $specFound) return TRUE;
    else return FALSE;
}
?>