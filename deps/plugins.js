(function(){
    //spin.min
    
    //processing spinner
    $.fn.spin = function(opts) {
        if (Spinner) {
            if (opts !== false) {
                return this.each(function() {
                    var $this = $(this), data = $this.data();
                    if (!data.spinner) {
                        data.spinner = new Spinner();
                    }
                    data.spinner.spin(this);
                });
            } else {
                return this.each(function() {
                    var $this = $(this), data = $this.data();
                    if (data.spinner) {
                        data.spinner.stop();
                    }
                });
            }
        } else {
            throw ("Spinner Class not Available!");
        }
    };
    //autocomplete
    /*$.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu : function(ul, items) {
            var that = this, currentCategory = "";
            $.each(items, function(index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                that._renderItemData(ul, item);
            });
        }
    });*/
}());
