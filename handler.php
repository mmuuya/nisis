<?php
function report($msg, $type){
	$_SESSION['messages'][] = array("message"=>$msg, "type"=>$type);
}

if((include 'settings/settings.php') === FALSE) {
	report('Could not find settings.php.  Has this app been installed?', 'error');
}

$version = '1.1.0';

//no trailing forward slash please, some links depend on it
$root = "$protocol://$domain/$path";

//increase the max execution time, this helps while on bad connection
if ($domain == "localhost") {
	ini_set("max_execution_time", 300);
}

//set up the user guide, warn in the log if it doesn't exist
$userGuidePath = "docs/NISIS-User-and-Training-Guide.pdf";
if(file_exists($userGuidePath)){
	$userGuide = $root . "/" . $userGuidePath;
} else {
	// error_log("WARNING - The user guide is missing or it cannot be read: " . $userGuidePath);
	$userGuide = $root . "/html/missing_help.php";
}

// //Creating a Redis client
// $redis = null;
// //Making sure the Redis class is define before instantiating
// if ( class_exists("Redis") ) {
// 	$redis = new Redis();
// 	//Connecting the Redis client to the Redis server
// 	if ($redis->connect($redisip, $redisport) == false){
// 		//error_log("The Redis connection has failed.");
// 	}
// }

if(isset($db)){
	if(!$db){
		$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
	}
} else {
	$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
}

if (!$db) {
    $e = oci_error();
    report(($e['message']), "error");
    error_log("DB Error:" . $e['message']);
}


//Time formats are a pain. These can be used when changing between
//PHP and the database. In general, try to create timestamps in PHP
//instead of using sysdate on the db (in case of different time zones)
//This format is FAA DTG with separator characters
$ora_dt_format = 'YYYY-MM-DD HH24:MI:SS';
$php_dt_format = 'Y-m-d H:i:s';

$loggedin = isset($_SESSION['username']);

if (session_id() == '') {session_start();}

if(isset($refresh)){
	$refresh = min($refresh, ini_get("max_execution_time"));
} else {
	$refresh = ini_get("max_execution_time");
}

//Exit with the $msg specified. The $msg will first be encoded as JSON.
//Parameter $log specifies whether to write JSONified $msg to the error_log
function kill($msg, $log=TRUE) {
    $status = '';
	if(is_string($msg)){
		$status = json_encode(array('result' => $msg));
	}
	if(is_array($msg)){
		if(!isset($msg['result'])){
			$msg['result'] = 'Unknown Error.';
		}
		$status = json_encode($msg);
	}
	if ($log) {
    	error_log("Page killed: " . $status . PHP_EOL);
	}
    exit($status);
}

function userIsAdmin(){
	global $db;

	if(!isset($_SESSION['userid'])){
		return false;
	}

	if(!isset($_SESSION['isAdmin'])){
		//Administrators group should always be id 0
		$adminCheckQuery = oci_parse($db, "SELECT userid, groupid FROM NISIS_ADMIN.USER_GROUP WHERE userid=:userID AND groupid=0");
		oci_bind_by_name($adminCheckQuery, ':userID', $_SESSION['userid']);
		oci_execute($adminCheckQuery);

		$_SESSION['isAdmin'] = FALSE;
		$result = oci_fetch_all($adminCheckQuery, $res);
		if($result !== FALSE && $result > 0){
			$_SESSION['isAdmin'] = TRUE;
		}
	}
	
	return $_SESSION['isAdmin'];
}

function userIsGroupAdmin(){
	global $db;

	if(!isset($_SESSION['userid'])){
		return false;
	}

	if(!isset($_SESSION['isGroupAdmin'])){
		$_SESSION['isGroupAdmin'] = FALSE;

		$query = oci_parse($db, "SELECT nisis_admin.user_is_a_grp_admin (:userID) as NUM_GROUPS from dual");
		oci_bind_by_name($query, ':userID', $_SESSION['userid']);
		oci_execute($query);

		oci_fetch_all($query, $result);

		if($result !== FALSE && $result['NUM_GROUPS'][0] > 0 ){
			$_SESSION['isGroupAdmin'] = TRUE;
		}
	}
	return $_SESSION['isGroupAdmin'];
}

function userIsTFRAdmin(){
	global $db;

	if(!isset($_SESSION['userid'])){
		return false;
	}

	if(!isset($_SESSION['isTFRAdmin'])){
		//TFR Administrators group should always be id 0
		$tfrAdminQuery = oci_parse($db, "SELECT userid, groupid FROM TFRUSER.USER_GROUP WHERE userid=:userID AND groupid=0");
		oci_bind_by_name($tfrAdminQuery, ':userID', $_SESSION['userid']);
		oci_execute($tfrAdminQuery);

		$_SESSION['isTFRAdmin'] = FALSE;
		$nrows = oci_fetch_all($tfrAdminQuery, $result);
		if($nrows !== FALSE && $nrows > 0){
			$_SESSION['isTFRAdmin'] = TRUE;
		}
	}
	
	return $_SESSION['isTFRAdmin'];
}
