<?php
require ('handler.php');

//check for a valid session
$loggedin = isset($_SESSION['username']);

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Loading...</title>
		<link rel="stylesheet" type="text/css" href="styles/base.css"/>
		<script type="text/javascript" src="settings/settings.js"></script>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
		<!-- Feature Manager -->
		<link rel="stylesheet" href="styles/featureManager.css" media="screen" charset="utf-8">
		<!-- Feature Manager -->
		<link rel="stylesheet" href="styles/popupWindow.css" media="screen" charset="utf-8">

	    <!-- html/profiles_fr23/css/reset.css messes up the whole NISIS ui -->
	    <link rel="stylesheet" type="text/css" href="html/profiles_fr23/css/grid.css" />
		<link rel="stylesheet" type="text/css" href="html/profiles_fr23/css/style.css" />
        <link rel="stylesheet" type="text/css" href="./styles/changePassword.css">

		<!--[if lt IE 9]>
		<script src="deps/html5shiv.js"></script>
		<![endif]-->
		<!-- Libraries -->
		<script src="deps/d3.v3.min.js"></script>
		<script type="text/javascript" src="deps/spin.min.js"></script>
		<script type="text/javascript" src="deps/kendoui/js/jquery.min.js"></script>
		<script type="text/javascript" src="deps/kendoui/js/kendo.all.min.js"></script>
		<script type="text/javascript" src="deps/kendoui/js/jszip.min.js"></script>
		<script type="text/javascript" src="deps/esri/library/3.11/jsapi/init.js"></script>
		<script type="text/javascript" src="deps/jquery.parse-0.5.6.js"></script>
		<script type="text/javascript" src="deps/plugins.js"></script>
		<script type="text/javascript" src="scripts/base.js"></script>
		<script type="text/javascript" src="scripts/ui.js"></script>
		<script type="text/javascript" src="scripts/paper-input.js"></script>


    <script type="text/x-kendo-template" id="profile_forms">
        # switch(type){

        case "h3":#
        <h3 class="#: columnClasses #">
            <hr class="med-blue2"/>
            <div class="white med-dark-blue2 paddingR10px">#: name #</div>
        </h3> #; break;

        case "profileImages":#
        <div class="#: columnClasses #">

        </div> #; break;

        case "spacer":#
        <div class="#: columnClasses #" />
         #; break;

        case "input[type='text']":#
        <div class="#: columnClasses #">
            <div class="content">
                <div class="paper-input__wrapper">
                    #
                    // TODO: clean up all instances
                        var limits = {
                            "col-1-1": 200,
                            "col-1-2": 33,
                            "col-1-3": 22
                        };

                        var title = (function(){
                                return (name.length < limits[columnClasses]) ? " " : name
                        })();


                    #

                    #if (typeof dbcol !== 'undefined') {
                        if (typeof numeric !== 'undefined' && typeof format !== 'undefined' && typeof decimals !== 'undefined') {
                            #

                               <input name="#: name #" id="#: name.replace(/ /gi,'_') #" class="paper-input paper-input--floating paper-input--touched numeric"  format="#: format #" decimals="#: decimals #" type="text" data-bind="value:fields.#: dbcol #"  title="#: name #" maxlength="#: maxChar #" #: disabled #/>

                            #
                        }
                        else {
                    #
                            <input name="#: name #" id="#: name.replace(/ /gi,'_') #" class="paper-input paper-input--floating paper-input--touched"  type="text" data-bind="value:fields.#: dbcol #"  title="#: name #" maxlength="#: maxChar #" #: disabled #/>
                    #
                        }#

                    #}
                    
                    else  {
                    	console.log("Error in loading Airport Profile: dbcol is underfined for the input box ", name);
                    }
                    #

                    #if (typeof disabled !== 'undefined') {#
                        <label for "#: name.replace(/ /gi,'_') #" title="#: title #" #: disabled # >#: name #</label>
                    #}
                    else  {
                        console.log("Error in loading Airport Profile: disabled is underfined for the input box ", name);
                    }
                    #

                </div>
            </div>
        </div>#; break;

        case "link":#
        <div class="#: columnClasses #">
            <div class="content">
                <div class="paper-input__wrapper divider">
                    #
                    // TODO: clean up all instances
                        var limits = {
                            "col-1-1": 200,
                            "col-1-2": 33,
                            "col-1-3": 22
                        };

                        var title = (function(){
                                return (name.length < limits[columnClasses]) ? " " : name
                        })();
                    #
                    
                    <div class="link-wrapper">
                        <label for  class="link-label" "#: name.replace(/ /gi,'_') #" title="#: name #"   > #: name #  </label>
                         <a href="#: linkURL #" target="_blank" name="#: linkName #" title="#: name #"><i class="fa fa-external-link-square"></i>#: linkName #</a>
                    </div>
                </div>
            </div>
        </div>#; break;

        case "textarea":#
        <div class="#: columnClasses #">
            <div class="content">

                <div class="paper-input__wrapper">


                    #
                    // TODO: clean up all instances
                        var limits = {
                            "col-1-1": 200,
                            "col-1-2": 33,
                            "col-1-3": 22
                        };

                        var title = (function(){
                                return (name.length < limits[columnClasses]) ? " " : name
                        })();

                    #


                    #if (typeof dbcol !== 'undefined') {#
                    	<textarea name="#: name #" id="#: name.replace(/ /gi,'_') #" data-bind="value:fields.#: dbcol #" class="paper-input paper-input--floating paper-input--touched" #: disabled # scrollable title="#: name #" maxlength="#: maxChar #">#: value #   data-bind="value:fields.#: dbcol #" </textarea>
                    #}
                    else  {
                    	console.log("Error in loading Airport Profile: dbcol is underfined for the textarea ", name);
                    }
                    #

                    #if (typeof notes !== 'undefined') {#
                        <label for "#: name.replace(/ /gi,'_') #" title="#: title #" #: disabled # >#: name #
                            <i class="fa fa-clock-o fa-external-link-history notesHistoryButton"></i>
                        </label>
                    # } else  {#
                        <label for "#: name.replace(/ /gi,'_') #" title="#: title #" #: disabled # >#: name #
                        </label>
                    # } #

                </div>
            </div>
        </div>
        #; break;

        case "select":#
        <div class="#: columnClasses #">
            <div class="content">
                <div class="paper-input__wrapper">
                    #if (typeof dbcol !== 'undefined') {#
	                    <select name="#: name #" id="#: name.replace(/ /gi,'_') #" class="paper-input paper-input--floating paper-input--touched "  data-bind="value:fields.#: dbcol #" title="#: name #">
							<option value="" disabled>Choose an option</option>
	                        #
	                        if(typeof selectOptions !== "undefined"){
	                            $.each(selectOptions, function(index, item){
	                                var selected =  item.option == value ? "selected" : "";
									# <option class="status-${item.label}" value="${item.option}" ${selected}>${item.label}</option> #
	                            });
	                        }
	                        #
	                    data-bind="value:fields.#: dbcol #"</select>
                    #}
                    else  {
                    	console.log("Error in loading Airport Profile: dbcol is underfined for the select ", name);
                    }
                    #
                    <label for "#: name.replace(/ /gi,'_') #" title="#: title #" #: disabled # >
                    #: name #
                    #
                    if(typeof buttons !== "undefined"){
                        $.each(buttons, function(index, item){
                            # <button title="${item.label}"><i class="${item.iconClass}"></i></button> #
                        });
                    }
                    #
                    </label>
                </div>
            </div>
        </div>
        #; break;


        case "multiselect":#
        <div class="#: columnClasses #">
            <div class="content">
                <div class="paper-input__wrapper">
                    <div class="title_#: name.replace(/ /gi,'_') #    "  #: disabled #>#: name #  </div>
                    <select class="multiselect #: name.replace(/ /gi,'_') #" bind-to="#: dbcol #" title="#: name #" #: disabled # ></select>
                </div>
            </div>
        </div>
        #; break;

        case "button":#
        <div class="#: columnClasses #">
            <div class="content">
                <div class="paper-input__wrapper">                  

                   <button class="k-button" title="#: linkName #" #: disabled #  action="#: action #" role="button"><strong>#: name #</strong></button>

                </div>
            </div>
        </div>
        #; break;

        case "grid":#
        <div class="#: columnClasses #">
            <div class="content">
                <label class="grid-label" for "#: name.replace(/ /gi,'_') #" title="#: name #"   >
                    #: name #
                </label>
                #if ( assoc_type !== '') {#
                    <div class="toHookUp section-grid #:datatableClass#" assoc-type="#: assoc_type #" scrollable></div>
                # } else  {#
                    <div class="toHookUp section-grid #:datatableClass#" scrollable></div>
                # } #

            </div>


        </div>#; break;

		<!-- case "datatable-ans":#
	     <div class="#: columnClasses #">
	         <div class="content">
	             <label class="grid-label" for "#: name.replace(/ /gi,'_') #" title="#: name #"   >
	                 #: name #
	             </label>
	                 <div class="section-grid assoc-atm-grid" scrollable></div>
	         </div>
	     </div>#; break; -->

<!--         case "lastupdated":#
        <div class="#: columnClasses #">
            <div class="last-updated-wrap">
                <div id="lastUpdated">#: name #<span>: <strong>#: value #</strong></span></div>
            </div>
        </div> #; break; -->

        }#
    </div>
    </script>
    <script type="text/x-kendo-template" id="subGridData">
        <div class="sub-grid-data">
            <div>
                <div class="sub-data"></div>
            </div>
        </div>
    </script>
    <script type="text/x-kendo-template" id="button_row_list">
        #
        $.each(buttons, function(index, item){
            # <i class="$(item.iconClass)"></i> #-->
        });
        #
    </script>
    <script type="text/x-kendo-template" id="quick_status_list">
        <menuitem class="qsr-container med-blue" data-target-form="#: targetForm #" data-target-offset="#: targetOffset #">
            <div class="qsr-status status-#: status_text.replace(/ |\/|,/gi,'_') #"  dbcol="#: dbcol #"></div>
            <div class="qsr-mini-toolbar">
                <!-- <i class="fa fa-external-link-square"></i> -->
                <i class="fa fa-clock-o fa-external-link-history statusHistoryButton" title="History"></i>
                <i class="fa fa-bell fa-external-link-alerts alertsButton" title="Alerts"></i>
            </div>
            <div class="qsr-label">#: label #</div>
			<div class="qsr-status-text" >#: status_text #</div>
        </menuitem>
    </script>
    <script type="text/x-kendo-template" id="profiles_list">
        <menuitem class="profiles-container">
            <a href="#: target #" data-is-table="#: isDatatable #">#: label  #</a>
        </menuitem>
    </script>


        <script src="html/profiles_fr23/js/airport_profile_constants.js"></script>
        <script src="html/profiles_fr23/js/airport_json_no_data_values.js"></script>
	    <script src="html/profiles_fr23/js/atm_profile_constants.js"></script>
        <script src="html/profiles_fr23/js/atm_json_no_data_values.js"></script>
        <script src="html/profiles_fr23/js/ans_profile_constants.js"></script>
        <script src="html/profiles_fr23/js/ans_json_no_data_values.js"></script>
        <script src="html/profiles_fr23/js/osf_profile_constants.js"></script>
        <script src="html/profiles_fr23/js/osf_json_no_data_values.js"></script>
        <script src="html/profiles_fr23/js/tof_profile_constants.js"></script>
        <script src="html/profiles_fr23/js/tof_json_no_data_values.js"></script>

       <!--  NISIS-2723 #ready #comment forced https redirect on all nisis URLs to prevent truncating of HTTPS and broken arcgis layers. This will force an update for dev environments to listen on https and we will be finally consistent accross all environments. -->
        <script type="text/javascript">        
        //nisis.faa.gov doing this only for PROD. we dont have our servers set for https      
        if ((window.location.hostname =="nisis.faa.gov" && window.location.protocol != "https:" ) 
            || ((window.location.hostname =="ndpuat.faa.gov" && window.location.protocol != "https:"))) {
            window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);
        }
        </script>

		<script>
			//quick and dirty way to change page title
			var checkURL = function(){
				var url = window.location.hostname;
				if(url == "dotmap.dot.gov"){
				//add new page title if DOTMAP
					document.title = "USDOTs Geographic Information System for Emergency Operations";

				}else{
				//add new page title if NISIS
					document.title = "NAS Integrated Status Insight System";
				}
			};

			//check to see if URL
			checkURL();
		</script>
	</head>
	<body class="soria">
		<div id="content">
			<?php

			//Check if user has correct browser
			if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') === FALSE) AND (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') === FALSE)) {
				include 'html/ie.html';
			}

			//Check if user is logged in
			if($loggedin){
					include 'html/app.php';
			}else{
				$url = $_SERVER["SERVER_NAME"];
				//check url
				if (isset($url)){
					if($url == 'dotmap.dot.gov'){
						include 'html/dotmap-login.php';
					}else{
						include 'html/login.php';
					}
				}
			}
			?>
		</div>
        <div id="messages"></div>
	</body>
</html>
