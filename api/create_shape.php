<?php
//
$userid = $_POST["userid"];
$shapeid = $_POST["shapeid"];
$parentid = $_POST["parentid"];
$itemtype = $_POST["itemtype"];
$symbolvalue = $_POST["symbolvalue"];
$result ="";

// NISIS-2400 KOFI: added param symbolvalue for pushpin issue.  FM_PKG.create_shape takes care of inserting into symbol_points table.
$query = "begin :result := NISIS_ADMIN.fm_pkg.create_shape(:userid, :shapeid, :parentid, :itemtype, :symbolvalue); end;";

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":userid", $userid);
oci_bind_by_name($parsed, ":shapeid", $shapeid);
oci_bind_by_name($parsed, ":parentid", $parentid);
oci_bind_by_name($parsed, ":itemtype", $itemtype);
oci_bind_by_name($parsed, ":symbolvalue", $symbolvalue);
oci_bind_by_name($parsed, ":result", $result, 2000);


error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in create_shape api', 'error' => $errStr));
}
else {
            
    	kill(array('result' => $result), FALSE);    
}

?>