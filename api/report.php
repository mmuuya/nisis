<?php

$breaks = floor((ini_get('max_execution_time'))-1 / $refresh);

while (empty($_SESSION['messages']) && $breaks > 0) {
	sleep($refresh);
	$breaks--;
}

if (empty($_SESSION['messages'])) {
	exit(json_encode(array('message' => '', 'type' => 'none')));
} else {
	$msg = array_pop($_SESSION['messages']);
	exit(json_encode(array('message' => date('D, M j, g:ia') . ': ' . $msg['message'], 'type' => $msg['type'])));
}