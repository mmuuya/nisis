<?php
//we can't use query.php as its currently structured
//because it doesn't allow us to group/order the values
	require("../handler.php"); 
	$query = "SELECT VALUE, LABEL FROM NISIS.PICKLIST_ITEMS WHERE PICKLIST = 3 ORDER BY VALUE ASC";
	$parsedquery = oci_parse($db, $query); 
	oci_execute($parsedquery); 
	oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
	exit(json_encode($results)); 
?>