<?php

//Get our configurations and global stuff, or fail if we can't.
if ((include '../handler.php') === FALSE) {
	exit(json_encode(array('return' => 'Misconfigured Server')));
}

//intercept special arcgis actions
if (isset($_REQUEST['action'])) {
    function isArc() {
        global $arcIP;
        $isArc = ($arcIP === $_SERVER['REMOTE_ADDR']);

        //for debugging
        // error_log("Check IP address for ArcGIS: " . $_SERVER['REMOTE_ADDR']);
        // error_log($isArc ? "SUCCESS: is ArcGIS" : "FAIL: not ArcGIS");

        //return $isArc;
        return true;

    }

    function notArc() {
        kill("Action '" . $_REQUEST['action'] . "' was attempted, but client was not ArcGIS.");
    }

    // require "admin/api/acl_lib_lite.php";
    $acl_lite = "../admin/api/acl_lib_lite.php";
    switch ($_REQUEST['action']) {
        case "agslogin":
            //TODO this needs to be the same code as login.php, but built so as to avoid infinite loop
            if (isArc()) {
                require $acl_lite;
                // error_log("ArcGIS Checking login");
                //special login action if arcgis -> want to avoid infinite loop
                $username = $_REQUEST['username'];
                $password = $_REQUEST['password'];
                $query = "SELECT U.USERNAME, U.PASSWORD, U.SALT, A.ACTIVE
                            FROM APPUSER.USERS U
                            JOIN APPUSER.USERS_APP A ON U.USERID = A.USERID
                            WHERE U.USERNAME = :uname
                            AND A.ACTIVE = 1
                            AND ROWNUM = 1";

                $parsed = parse($query);
                oci_bind_by_name($parsed, ':uname', $username);
                if(!oci_execute($parsed)){
                    kill("Bad login query");
                }
                $row = oci_fetch_assoc($parsed);
                if (!$row) {
                    kill("Bad username or password.");
                }
                $pass = hash('sha512', $row['SALT'] . $_REQUEST['password']);
                unset($_REQUEST['password']);
                if (strcmp($row['PASSWORD'], $pass) == 0) {
                    if($row['ACTIVE']==1) {
                        //exit(json_encode(array('return'=>'Success', 'message'=>'Successully Logged in.', 'userid'=>$row['USERID'], 'username'=>$username, 'token'=>$token, 'newhtml'=>$app)));
                        kill(array('return'=>'Success', 'message'=>'Successfully Logged in.'));
                    } else {
                        kill(array('return' => 'Failure', 'message' => 'Your Account is currently inactive.  Please Contact your Supervisor.'));
                    }
                } else {
                    kill("Bad username / password");
                }
            }
            break;
        case "roles":
            //all roles
            require $acl_lite;
            isArc() ? kill(array("result" => "All roles in the database.", "roles" => getAllRoles())) : notArc();
            break;
        case "allusers":
            //all users
            require $acl_lite;
            isArc() ? kill(array("result" => "All users in the database.", "users" => getAllUsers())) : notArc();
            break;
        case "users":
            //users in specified role
            require $acl_lite;
            $groupName = $_REQUEST['role'];
            isArc() ? kill(array("result" => "Users in role '" . $groupName . "'", "users" => getUsersInRole($groupName)), FALSE) : notArc();
            break;
        case "userroles":
            //roles a specified user belongs to
            require $acl_lite;
            $userName = $_REQUEST['userName'];
            isArc() ? kill (array("result" => "Roles for user '" . $userName . "'", "roles" => getUserRoles($userName)), FALSE) : notArc();
            break;
        case "user":
            //specified user info
            require $acl_lite;
            $userName = $_REQUEST['userName'];
            isArc() ? kill(array("result" => "Info for user '" . $userName . "'", "user" => getUserInfo($userName)), FALSE) : notArc();
            break;
    }
}

//Check to make sure this request has an action
if(!array_key_exists('action', $_REQUEST)){
	kill('Root API Malformed Request: No Action Specified');
}

$loggedin = isset($_SESSION['username']);
if(!$loggedin && $_REQUEST['action']!=='report' && $_REQUEST['action']!=='login' && $_REQUEST['action']!=='reset_pw' && $_REQUEST['action']!=='create_iwl'){
	kill('Root API Security Violation.  Please Log In.');
}

//Check to make sure this request makes sense--i.e., we have an API point for it, and call it if we do.
if((include $_REQUEST['action'].'.php') === FALSE){
	kill('Root API Malformed Request: No API Endpoint (Register)');
}
