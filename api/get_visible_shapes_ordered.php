<?php
//
$userid = $_POST["userid"];
$shapes = $_POST["shapes"];
$result ="";

$query = "begin :result := NISIS_ADMIN.fm_pkg.get_visible_shapes_ordered(:userid, :p1); end;";

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_array_by_name($parsed, ":p1", $shapes, count($shapes), -1,  SQLT_INT); 
oci_bind_by_name($parsed, ":userid", $userid);

oci_bind_by_name($parsed, ":result", $result, 2000);


error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get_visible_shapes_ordered api', 'error' => $errStr));
}
else {
            
    	kill(array('result' => $result), FALSE);    
}

?>