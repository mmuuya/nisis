<?php
//KH and JS Get number of times item was shared with other users/groups AND WAS MOVED INTO THEIR "MY FEATURES" / "GROUP FEATURES" folder.

$itemid = $_POST['itemid'];


$parsed = oci_parse($db, 'select NISIS_ADMIN.fm_pkg.ITEM_SHARE_CNT(:itemid) AS ITEMSHARECNT from dual');

oci_bind_by_name($parsed, ":itemid", $itemid);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query', 'error' => $errStr));
}

oci_fetch_all($parsed, $results);
$itemsharecnt = $results['ITEMSHARECNT'];


kill(array('result' => 'Success', "itemsharecnt" => $itemsharecnt), FALSE);

?>