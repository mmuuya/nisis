<?php
    require "clean_profile_sesh.php";
    
    $facType = $_REQUEST['type'];
    $facId = $_REQUEST['facId'];

    ob_start();
    require '../html/profiles/' . $facType . '_profile.php';
    $htm = ob_get_clean();

    exit(json_encode(array(
            'return' => 'success',
            'newhtml' => $htm
        )));
?>