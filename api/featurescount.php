<?php
   //Make Sure the DB is wired up correctly
	if ($db === FALSE) {
		kill('No Database Connection');
	}
	
	//Make sure we've got all the data we need
	if (!array_key_exists('table', $_REQUEST)) {
		kill('Malformed Request in featurescount');
	}
	
	$table = $_REQUEST['table'];
	if($table == 'NisisTO_Airports'){
		$table = 'SDE.MAP_NFDC_AIRPORTS';
	}
	$values = 'OBJECTID';
	
	if(array_key_exists('values', $_REQUEST)){	
		$values = $_REQUEST['values'];
	}
	
	if(is_array($values)){
		$values = implode(',', $values);
		$table = implode(',', $table);
	}
	
	$query = 'SELECT :values FROM :table WHERE $constraints;';
	$parsedquery = oci_parse($db, $query);
	oci_bind_by_name($parsedquery, ':values', $values);
	oci_bind_by_name($parsedquery, ':table', $table);
	print_r($parsedquery);
	
	//If this fails, somebody wrote something that broke the query when it was escaped.
	if(!oci_execute($parsedquery)){
		kill('Malformed Query');
		error_log("Possible SQL Injection Attack!  Submitted Username: '$username'");
	}
	
	oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
	exit(json_encode($results));
?>
