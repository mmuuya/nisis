<?php
require ('../handler.php');

if ($db === FALSE) {
    kill('No Database Connection');
}

//check if term param exist
if (!array_key_exists('term', $_REQUEST)) {
    kill("Missing correct required argument: 'term'");
}

//Get user-entered search term
$term = strtoupper($_GET["term"]);
//Stop here if the key search is less then 2 char
if(strlen($term) < 1){
    $term = 'A';
    //kill("Missing keyword");
}

//Set up a counter, and maxnum variable to return a maximum of 10 results
$count = 1;
$maxresult = 40;

//Initialize return result JSON object
$json = array();
 
// NISIS-2742 - KOFI HONU . decommission nisis_resp
// NISIS-2754 - KOFI HONU Add Nisis_tof
$icaoselectsql = "SELECT * FROM (SELECT a.*, ROWNUM AS rnum  FROM (  SELECT LAYERNM,
                           FACTYPE,
                           OBJECTID,
                           FACINFO,
                           FACID,
                           CASE
                              WHEN facid = :term THEN 0
                              WHEN facid LIKE :term ||'%' THEN 1
                              WHEN facid LIKE '%'|| :term ||'%' THEN 2
                              ELSE 3
                           END
                              AS matchcol,
                           CASE
                              WHEN layernm = 'nisis_airports' THEN 1
                              WHEN layernm = 'nisis_atm' THEN 2
                              WHEN layernm = 'nisis_osf' THEN 3
                              WHEN layernm = 'nisis_tof' THEN 4
                              WHEN layernm = 'nisis_ans' THEN 5
                              ELSE 6
                           END
                              AS ord
                      FROM nisis_geodata.quicksearch
                     WHERE facid LIKE '%'|| :term ||'%' OR facnm LIKE '%'|| :term ||'%'
                  ORDER BY matchcol) a 
           WHERE ROWNUM < ($maxresult + 1)) ORDER BY ord, facid ";

//Generate queries and result sets based off of term
$icaochecksql = oci_parse($db, $icaoselectsql);

//Associate search term with variable(s)
oci_bind_by_name($icaochecksql, ':term', $term);

//Execute the sql
oci_execute($icaochecksql);
while($icao = oci_fetch_array($icaochecksql)){  
    $labeldisplay = $icao["FACINFO"];
    $json[] = array("category" => strtolower($icao["LAYERNM"]),
            "label" => $labeldisplay,
            "value" => $icao["LAYERNM"] . ":" . $icao["OBJECTID"],
            "id" => $icao["FACID"],
            "oid" => $icao["OBJECTID"]);
}

//Return the JSON 
echo json_encode($json);
?>