<?php
//
// NISIS-HOTFIX, KOFI HONU - Added this file for persisting profile pic selections.
$objectid = $_POST["objectid"];
$tableName = $_POST["tableName"];
$pics = $_POST["pics"];

$result ="";

$query = "begin :result := NISIS.profile_util_pkg.set_profile_pics(:objectid, :tableName, :pics); end;";

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":objectid", $objectid);
oci_bind_by_name($parsed, ":tableName", $tableName);
oci_bind_array_by_name($parsed, ":pics", $pics, 5, -1, SQLT_INT);
oci_bind_by_name($parsed, ":result", $result, 2000);

error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in set_profile_pics api', 'error' => $errStr));
}
else {            
    kill(array('result' => $result), FALSE);    
}

?>



