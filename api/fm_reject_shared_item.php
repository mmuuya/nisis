<?php
//KH - Reject a shared item in feature management.
 // FUNCTION reject_shared_item (in_userid     IN NUMBER,
 //                                in_itemid     IN NUMBER,
 //                                in_usertype   IN VARCHAR2)
 //      RETURN NUMBER


$result = "";

$userid = $_SESSION['userid'];
$itemid = $_POST["itemid"];
$userorgroupmode = $_POST["userorgroupmode"];
$groupid = $_POST["groupid"];

if ($userorgroupmode == "group"){
	$userid = $groupid;
}

$query = 'BEGIN :result := NISIS_ADMIN.fm_pkg.reject_shared_item (:userid, :itemid, :userorgroupmode); END;';

$parsed = oci_parse($db, $query);

oci_bind_by_name($parsed, ":userid", $userid);
oci_bind_by_name($parsed, ":itemid", $itemid);
oci_bind_by_name($parsed, ":userorgroupmode", $userorgroupmode);
oci_bind_by_name($parsed, ":result", $result, 2000);

error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in fm_reject_shared_item api', 'error' => $errStr));
}
else {    
    kill(array('result' => $result), FALSE);    
}

?>

