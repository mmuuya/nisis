<?php
//Save profile
$objId = $_POST["id"];  //like DCA for airports or SBY-FCT for ATM 
$changes = $_POST["changes"];  //like '[{"lg_name":"DULLES AIRPORT 999922","icao_id":"XXXX","elevation":200}]'
$action = $_POST["save_what"]; 
// NISIS-2812 - KOFI HONU
// added userid parameter to stored proc call
$userid = $_SESSION['userid'];

$result ="";
$query ="";

switch ($action) {           
    case 'save_profiles_airports': 
        $query = "begin :result :=  nisis.profile_apt.update_profile(:objId, :userid, :p1); end;";
        break;
    case 'save_profiles_atm': 
        $query = "begin :result :=  nisis.profile_atm.update_profile(:objId, :userid, :p1); end;";
        break;
    case 'save_profiles_ans': 
        $query = "begin :result :=  nisis.profile_ans.update_profile(:objId, :userid, :p1); end;";
        break;
    case 'save_profiles_osf': 
        $query = "begin :result :=  nisis.profile_osf.update_profile(:objId, :userid, :p1); end;";
        break;
    case 'save_profiles_tof': 
        $query = "begin :result :=  nisis.profile_tof.update_profile(:objId, :userid, :p1); end;";
        break;
    
    default:  
    	error_log("invalid action in save profiles.php");      
        break;
};


$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":objId", $objId);
oci_bind_by_name($parsed, ":userid", $userid);
oci_bind_by_name($parsed, ":p1", $changes);
oci_bind_by_name($parsed, ":result", $result, 2000);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in update nisis profile api', 'error' => $errStr));
}
else {
    error_log("Save profile executed successfully ...");       
    kill(array('result' => $result), FALSE);    
}

?>