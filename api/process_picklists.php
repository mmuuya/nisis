<?php
//Save every picklist mapping (number value -> text) to a session variable. Also save
//every picklist image.
//
//If the picklist session variable is already set, no action is taken.
//
//Note that since the variable is saved to $_SESSION['profile'] it will be
//unset by clean_profile_sesh.php (usually when the profile window is closed)
//
//The formats are:
// $_SESSION['profile']['picklist'][<field name>][<# value>] => "Text label"
// $_SESSION['profile']['picklist']['image'][<Text label>] = <path/to/img>

if(isset($_SESSION['profile']['picklist'])) {
    //error_log("INFO, skipping query: picklist mapping already setup");
    return;
}

 //error_log("INFO, querying: picklist mapping not setup");

$query = 'SELECT A.COL_NM, P.VALUE, P.LABEL, I.PATH 
            FROM NISIS_ADMIN.ACL_FIELDS A
            JOIN NISIS.PICKLIST_ITEMS P ON A.PICKLIST=P.PICKLIST
            JOIN NISIS.PICKLIST_IMAGES I ON P.IMGID=I.IMGID
            ORDER BY P.DISPLAY_ORDER ASC';

$parsed = oci_parse($db, $query);
if(!oci_execute($parsed)) {
    kill("DATABASE ERROR while processing the picklists");
}

while(oci_fetch($parsed)) {
    $fld = oci_result($parsed, "COL_NM");
    $val = oci_result($parsed, "VALUE");
    $txt = oci_result($parsed, "LABEL");
    $img = oci_result($parsed, "PATH");
    
    $_SESSION['profile']['picklist'][$fld][$val] = $txt;
    $_SESSION['profile']['picklist']['image'][$txt] = $img;
}

?>