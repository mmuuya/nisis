
<?php 
	//Get our configurations and global stuff, or fail if we can't.
	if ((include '../../handler.php') === FALSE) {
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}

	//
	//	Process TFR data: input ==> JSON String
	//
	//session_start();

	$session = isset($_SESSION);

	if (!$session) {
		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'You are not logged in. Try again.',
			'output' => session_id()
		)));
	}

	$username = $_SESSION['username'];

	if (!isset($username)) {

		error_log("Unidentified user trying to access NISIS");

		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'Unidentified user trying to access NISIS/TFR.'
		)));
	}

	$input = $_REQUEST['notamId'];
	$action = $_REQUEST['action'];
	
	if (!isset($input)) {

		error_log($username . ': Invalid input for AIXM file process.');

		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'Invalid input for NOTAM submission.'			
		)));
	}

    // Set Enviromental Variable
    //$JAVA_HOME = "C:\\Program Files\\Java\\jdk1.8.0_25";
    //$PATH = "$JAVA_HOME" . "\\bin";
    if (!isset($javapath)) {

		error_log('Javapath is not set.');

		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'Invalid Javapath for NOTAMs submission to NES.'
		)));
	}
    
    
    //setup javapath
    //$PATH = $javapath;    
    //putenv("PATH=$PATH");
    putenv("PATH=$javapath");

    error_log("Passing arguments to nes.jar .... ");
    error_log("java -jar ../../nes/dist/nes.jar " . $input." ".$action);

    //execute java program
	$exec = shell_exec("java -jar ../../nes/dist/nes.jar " . $input." ".$action);

	//error_log($username . ': Notam Generation Execution Result: ' . $exec);
	
	if ($exec) {

		exit(json_encode(array(
			'return' => 'Success',			
			'message' => $exec,
			'output' => $exec,
			'username' => $username
		)));

	} else {

		error_log($javapath . ': Failed to submit .');
		error_log($input . ': Failed to submit the input.');
		error_log(': Failed to submit ... was trying to '.$action );

		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'Failed to submit NOTAM.',
			'output' => $exec,
			'username' => $username
		)));
	}
?>
