
<?php 
	//Get our configurations and global stuff, or fail if we can't.
	if ((include '../../handler.php') === FALSE) {
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}


	$session = isset($_SESSION);

	if (!$session) {
		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'You are not logged in. Try again.',
			'output' => session_id()
		)));
	}

	
    // Set Enviromental Variable
    //$JAVA_HOME = "C:\\Program Files\\Java\\jdk1.8.0_25";
    //$PATH = "$JAVA_HOME" . "\\bin";
    if (!isset($javapath)) {

		error_log('Javapath is not set.');

		exit(json_encode(array(
			'return' => 'Failure',
			'message' => 'Invalid Javapath for NOTAMs submission to NES.'
		)));
	}
    
    
    //setup javapath
    //$PATH = $javapath;    
    //putenv("PATH=$PATH");
    putenv("PATH=$javapath");

    //execute java program. Dont pass any parameters
	$exec = shell_exec("java -jar ../../nes/dist/nes.jar ");
	
	
	if ($exec) {

		exit(json_encode(array(
			'return' => 'Success',			
			'statuses' => $exec
		)));

	} else {

		error_log($javapath . ': Failed to submit .');		

		exit(json_encode(array(
			'return' => 'Failure',
			'error' => 'Failed to retrive status from NES.'
		)));
	}
?>
