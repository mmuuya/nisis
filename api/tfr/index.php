<?php
	require_once '../../cache.class.php';
	//Get our configurations and global stuff, or fail if we can't.
	if ((include '../../handler.php') === FALSE) {
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}

	$action = $_REQUEST['action'];
	$filter = null;
	$type = null;

	if ($_REQUEST['filter']) {
		$filter = $_REQUEST['filter'];
	}

	if ( isset($_REQUEST['type']) ) {
		$type = $_REQUEST['type'];
	}

	//validate session
	if ( !isset($_SESSION) ) { 
		exit(json_encode(array('return' => 'Failure', 'message' => 'Permission denied.')));
	}
	
	if (!isset($action) || $action == "") { 
		exit(json_encode(array('return' => 'Failure', 'message' => 'Missing/Invalid ACTION argument.')));
	}
	
	if (!isset($javapath) || $javapath == "") { 
		exit(json_encode(array('return' => 'Failure', 'message' => 'Javapath is not set, unable to submit NOTAMs to NES.')));
	}

	//states
	function getStates() {
		global $db;
		//see if we have the results of this query cached. The cache is stored in nisis/cache. 
		$c = new Cache(array(
		  'name'      => 'nisis',
		  'path'      => '../../cache/',
		  'extension' => '.cache'
		));
		$cachedResults = $c->retrieve('STATES');
		if (!isset($cachedResults)) {

		    $query = 'SELECT ID, NAME, ABBR, LAT, LON FROM NISIS.LOOKUP_STATES';

		    $parsed = oci_parse($db, $query);

		    if (!oci_execute($parsed)) {
		    	$e = oci_error($parsed);
		    	error_log('Error retreiving states: '. $e['message']);
		    	kill(array("return"=> "Failure", "result"=> $e['message']));
		    };

		    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
		    if (isset($results)) {
			    //cache the results so we dont have to go to the database again
	            $c->store('STATES', $results);
        	}
	    }
		else {
			error_log("Already have cached results for STATES query");
			$results = $cachedResults;				
		}
	    //error_log('States: ' . count($results));
	    //return $results;
	    exit(json_encode($results));
	};

	//cities
	function getCities($state) {
		global $db;
		
	    $query = 'SELECT ID, NAME, STATE, LAT, LON FROM NISIS.LOOKUP_CITIES'
	    		. ' WHERE STATE IN (SELECT DISTINCT ABBR FROM NISIS.LOOKUP_STATES)';

	    if ($state){
	    	$query .= " AND STATE = '" . $state . "'";
	    } else {
	    	$query .= " AND STATE = ''";
	    }
	    $query .= " ORDER BY NAME ASC";

	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving states: '. $e['message']);
	    	kill(array("return"=> "Failure", "result"=> $e['message']));
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit(json_encode($results));
	};

	//tfr time zones
	function getTfrTimeZones() {
		global $db;

		//see if we have the results of this query cached. The cache is stored in nisis/cache. 
		$c = new Cache(array(
		  'name'      => 'nisis',
		  'path'      => '../../cache/',
		  'extension' => '.cache'
		));
		$cachedResults = $c->retrieve('TFR_TIMEZONES');
		if (!isset($cachedResults)) {
		    $query = 'SELECT TZID AS ID, TZ_NAME AS NAME, UTC_OFFSET AS OFFSET FROM TFRUSER.TFR_TIMEZONES';

		    $parsed = oci_parse($db, $query);

		    if (!oci_execute($parsed)) {
		    	$e = oci_error($parsed);
		    	error_log('Error retreiving tfr time zones: '. $e['message']);
		    	kill(array("return"=> "Failure", "result"=> $e['message']));
		    };

		    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
		    if (isset($results)) {
			    //cache the results so we dont have to go to the database again
	            $c->store('TFR_TIMEZONES', $results);
        	}
		}
		else {
			error_log("Already have cached results for TFR_TIMEZONES query");
			$results = $cachedResults;				
		}
	    //error_log('TIME Zones: ' . count($results));
	    //return $results;
	    exit(json_encode($results));
	};

	//tfr types
	function getTfrTypes() {
		global $db;

        //see if we have the results of this query cached. The cache is stored in nisis/cache. 
		$c = new Cache(array(
		  'name'      => 'nisis',
		  'path'      => '../../cache/',
		  'extension' => '.cache'
		));
		$cachedResults = $c->retrieve('TFR_TYPES');
		if (!isset($cachedResults)) {
		    //$query = "select typeid as id, tfrtype as type, description, '' as ref, '' as value, '' as details from tfruser.tfr_types";
			//$query = "select id, type, ref, name, details, description, template from tfruser.lookup_tfr_types";

			$query = "select id, REF || ' ' || name  || case when details is null THEN  '' else ' (' || details || ')' end as type, ref, name, details, description, template from tfruser.lookup_tfr_types order by id asc";

		    $parsed = oci_parse($db, $query);

		    if (!oci_execute($parsed)) {
		    	$e = oci_error($parsed);
		    	error_log('Error retreiving tfr time zones: '. $e['message']);
		    	kill(array("return"=> "Failure", "result"=> $e['message']), true);
		    };

		    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
		    if (isset($results)) {
	            //cache the results so we dont have to go to the database again
	            $c->store('TFR_TYPES', $results);
        	}
		}
		else {
			error_log("Already have cached results for TFR_TYPES query");
			$results = $cachedResults;				
		}

		//return the encoded results
		exit(json_encode($results));
	};

	//get tfr templates
	function getTfrTemplate($filter) {

	};

	//tfr list
	function getTfrList($filter, $type) {
		global $db;

		if ( $type == null ) {
			//exit(json_encode(Array()));
		}

		$username = $_SESSION['userid'];
		$fStr = strtolower($filter);

	    $query = "SELECT DISTINCT
			    N.NOTAM_ID AS ID,
			    N.NOTAM_TYPE AS TYPE,
			    N.NOTAM_NAME AS NAME
			FROM TFRUSER.NOTAM_BODY N
			LEFT JOIN TFRUSER.NOTAM_GROUPS G
			ON N.NOTAM_ID = G.NOTAM_ID
			WHERE G.GROUP_ID IN (SELECT U.GROUPID FROM TFRUSER.USER_GROUP U WHERE U.USERID = '" . $username . "')";
		
		if ($type && $type != "") {
			$query .= " AND N.NOTAM_TYPE = '" . $type . "'";
		}

		if ($filter && $filter != "") {
			$query .= " AND LOWER(N.NOTAM_NAME) LIKE '%" . $fStr . "%'";
		}
		
		$query .= " AND ROWNUM < 100 ORDER BY N.NOTAM_NAME ASC";

	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving tfr time zones: '. $e['message']);
	    	kill(array("return"=> "Failure", "result"=> $e['message']), true);
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit( json_encode($results) );
	};

	//match new tfr types
	function getNewTfrId($oldId) {
		$newId = 1;

		if ($oldId > 0 && $oldId <= 3) {
			$newId = $oldId;
		} else if ( $oldId > 3 && $oldId < 8) {
	    		$newId = 4;
    	} else if ( $oldId == 8 ) {
    		$newId = 5;
    	} else if ($oldId > 8 && $oldId < 12) {
    		$newId = 6;
    	} else if ($oldId == 12) {
    		$newId = 7;
    	}

    	return $newId;
	}

	//tfr instructions
	function getTfrInstructions($typeId) {
		global $db;

	    $query = "select i.id, i.name, i.value from tfruser.lookup_instructions i join tfruser.lookup_type_insts t on i.id = t.instruction";

	    if (isset($typeId) && $typeId != 0) {
	    	//get the new id
	    	$typeId = getNewTfrId($typeId);

	    	$query .= " where t.tfr_type = ";
	    	$query .= $typeId;
	    }

	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving tfr instructions: ' . $e['message']);
	    	kill(array("return"=> "Failure", "result"=> $e['message']));
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit(json_encode($results));
	};

	//tfr instructions options
	// function getTfrInstOptions($tfr, $inst, $shape) {
	function getTfrInstOptions($tfr, $inst) {
		global $db;

	    $query = "select o.id, o.name, ti.id order_id from tfruser.lookup_inst_options o ";
	    $query .= "join tfruser.lookup_types_inst_options ti ";
	    $query .= "on o.id = ti.inst_option ";

	    if (isset($tfr) && $tfr != 0 && isset($inst) && $inst != 0) {
	    	$tfr = getNewTfrId($tfr);

	    	$query .= " where ti.tfr_type = ";
	    	$query .= $tfr;
	    	$query .= " and ti.instruction = ";
	    	$query .= $inst;
	    }

	    $query .= " order by order_id asc";

	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving tfr instructions: ' . $e['message']);
	    	kill(array("return"=> "Failure", "result"=> $e['message']));
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit(json_encode($results));
	};

	//tfr time zones
	function getArtccs() {
		global $db;
		$c = new Cache(array(
		  'name'      => 'nisis',
		  'path'      => '../../cache/',
		  'extension' => '.cache'
		));
		$cachedResults = $c->retrieve('ARTCCS');
		if (!isset($cachedResults)) {

		    $query = "select id, artcc_id as artcc, artcc_name as name, lat, lon from nisis.lookup_artccs";

		    $parsed = oci_parse($db, $query);

		    if (!oci_execute($parsed)) {
		    	$e = oci_error($parsed);
		    	error_log('Error retreiving tfr time zones: '. $e['message']);
		    	kill(array("return"=> "Failure", "result"=> $e['message']));
		    };

		    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
		    if (isset($results)) {
	            //cache the results so we dont have to go to the database again
	            $c->store('ARTCCS', $results);
        	}
	    }
		else {
			error_log("Already have cached results for ARTCCS query");
			$results = $cachedResults;				
		}
	    //return $results;
	    exit(json_encode($results));
	};

	//tfr time zones
	function getFacTypes() {
		global $db;
		$c = new Cache(array(
		  'name'      => 'nisis',
		  'path'      => '../../cache/',
		  'extension' => '.cache'
		));
		$cachedResults = $c->retrieve('FAC_TYPES');
		if (!isset($cachedResults)) {
		    $query = "select typeid as id, type_name as name from tfruser.factypes order by type_name asc";

		    $parsed = oci_parse($db, $query);

		    if (!oci_execute($parsed)) {
		    	$e = oci_error($parsed);
		    	error_log('Error retreiving tfr fac types: '. $e['message']);
		    	kill(array("return"=> "Failure", "result"=> $e['message']));
		    };

		    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
	        if (isset($results)) {
	            //cache the results so we dont have to go to the database again
	            $c->store('FAC_TYPES', $results);
        	}
	    }
		else {
			error_log("Already have cached results for FAC_TYPES query");
			$results = $cachedResults;				
		}
	    //return $results;
	    exit(json_encode($results));
	};

	//tfr shapes
	function getNotamAreas($filter) {
		global $db;

	    $query = "select objectid, tfr_id, tfr_shape, 
	    	(name || ' (' || tfr_shape || ')') as NAME, 
	    	floor, fl_type, ceiling, ceil_type, geom_type, 
	    	user_group, created_user 
			from nisis_geodata.tfr_polygons 
			where tfr_shape in ('base', 'add', 'subtract') ";

		if ($filter != null) {
			$query .= "and lower(name) like '%" . strtolower($filter) . "%' ";
		}

		$query .= "and rownum < 100 ";
		$query .= "order by name asc";

	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving tfr areas: '. $e['message']);
	    	kill(array("return"=> "Failure", "message"=> $e['message']));
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //error_log('Fac Types: ' . count($results));
	    //return $results;
	    exit(json_encode($results));
	};

	switch ($action) {
		case 'states':
				getStates();
			break;

		case 'cities':
				getCities($filter);
			break;

		case 'timezones':
				getTfrTimeZones();
			break;

		case 'tfrtypes':
				getTfrTypes();
			break;

		case 'tfr-list': 
				getTfrList($filter, $type);
			break;

		case 'instructions':
				getTfrInstructions($filter);
			break;

		case 'ints-options':
				$tfr = $filter['tfr'];
				$inst = $filter['inst'];
				// $shape = $filter['shape'];

				// getTfrInstOptions($tfr, $inst, $shape);
				getTfrInstOptions($tfr, $inst);
			break;

		case 'tfr-template':
				getTfrTemplate($filter);
			break;

		case 'artccs':
				getArtccs();
			break;
		
		case 'fac-type':
				getFacTypes();
			break;

		case 'notam-areas':
				getNotamAreas($filter);
			break;

		default:
			# code...
			break;
	}

?>