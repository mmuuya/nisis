<?php
//Get all the apps to which this user has access 
$query = 'SELECT APPNAME
FROM APPUSER.USERS_APP U, APPUSER.APP A 
WHERE U.APPID = A.APPID 
AND USERID=:usrid';

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":usrid", $_SESSION['userid']);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get_app_access api', 'error' => $errStr));
}

oci_fetch_all($parsed, $results);
$apps = $results['APPNAME'];

if(count($apps) === 0){
    $apps = array();
}

//Setting the userapps array as session variable in case that somebody needs to use it.
$_SESSION['userapps'] = $apps;

kill(array('result' => 'Success', "userapps" => $apps), FALSE);

?>