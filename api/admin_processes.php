<?php
	//update field 
	$task = $_REQUEST['task'];

	if(!$task){
		return;
	} else {
		switch ($task) {
			case 'update_fields_alias':
				$fields = $_REQUEST['fields'];
				$lyr = $_REQUEST['layer'];
				if($fields && $lyr){
					updateFieldsAlias($fields, $lyr);
				} else {
					exit(json_encode(array('result'=>'Failure','message'=>'Invalid arguments')));
				}

				break;
			
			default:
				# code...
				break;
		}
	}

	//update fields
	function updateFieldsAlias($fields, $lyr){
		global $db;
		$len = count($fields);
		$p = 0;
		

		$table = array(
			'airports'=> 4,
			'atm'=> 5,
			'osf'=> 8,
			'ans'=> 9,
			//NISIS-2754 - KOFI HONU
			'tof'=> 24
			//'teams'=> 7
		);

		if(!($len > 0)){
			exit(json_encode(array('result'=>'Failure','message'=>'Fields is empty')));
		}

		$report = array(
			'success'=> 0,
			'failure'=> 0,
			'errors'=> []
		);

		for($i=0; $i<$len; $i++){
			$p++;

			//$query = '';
			$query = "UPDATE NISIS_ADMIN.ACL_FIELDS SET DISPLAY_NM = '";
			$query .= $fields[$i]['alias'] . "' ";
			$query .= "WHERE COL_NM = '";
			$query .= $fields[$i]['name'] . "' ";
			$query .= "AND TABLEID = '";
			$query .= $table[$lyr] . "'";

			error_log($query);

			$parsedquery = oci_parse($db, $query);

			//If this fails, somebody wrote something that broke the query when it was escaped.
			if(!oci_execute($parsedquery)){
				$e = oci_error($parsedquery);
				error_log('ACL Fields update error: ' . $e['message']);
				$report['failure']++;
				array_push($report['errors'], array('field'=> $fields[$i]['name'], 'message'=> $e['message']));
			}

			$report['success']++;

			if($p === $len){
				exit(json_encode(array('result'=>'Done','report'=>$report)));
			}
		}

	}
?>