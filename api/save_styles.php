<?php

$user = $_SESSION['username'];
$task = $_REQUEST['task'];
$layer = $_REQUEST['layer'];
$objid = $_REQUEST['objid'];
$symType = $_REQUEST['symType'];
$symSize = null;
$symStyle = null;
$symColor = null;
$symTrans = null;
$sql = null;

error_log('Custom styles task: ' . $task);

if( ($task != 'create') || ($task != 'update') ) {
	//kill(array('return' => 'Failure', 'message' => 'Missing or invalid task argument'));
	$task = 'create';
}

if ( !$layer || !$objid ) {
	kill(array('return' => 'Failure', 'message' => 'Missing required inputs'));
}

if ( !$symType ) {
	kill(array('return' => 'Failure', 'message' => 'App could not identify the feature type'));
}

if( isset($_REQUEST['symSize']) ) {
	$symSize = $_REQUEST['symSize'];
}

if( isset($_REQUEST['symStyle']) ){
	$symStyle = $_REQUEST['symStyle'];
}

if( isset($_REQUEST['symColor']) ) {
	$symColor = $_REQUEST['symColor'];
}
if( isset($_REQUEST['symTrans']) ){
	$symTrans = $_REQUEST['symTrans'];
}

if( isset($_REQUEST['outStyle']) ){
	$outStyle = $_REQUEST['outStyle'];
}

if( isset($_REQUEST['outWidth']) ){
	$outWidth = $_REQUEST['outWidth'];
}

if( isset($_REQUEST['outColor']) ) {
	$outColor = $_REQUEST['outColor'];
}
if( isset($_REQUEST['outTrans']) ){
	$outTrans = $_REQUEST['outTrans'];
}

if($symType == 'point'){

	$sql = "MERGE INTO NISIS.SYMBOL_POINTS P
	USING (SELECT '$layer' AS PT_LAYER, '$objid' AS PT_OID, '$symSize' AS PT_SIZE,
		'$symColor' AS PT_COLOR, '$symTrans' AS PT_TRANS, '$outStyle' AS PT_OUT_TYPE,
		'$outWidth' AS PT_OUT_WIDTH, '$outColor' AS PT_OUT_COLOR, '$user' AS CREATED_USER FROM DUAL) N
	ON (P.PT_LAYER = N.PT_LAYER AND P.PT_OID = N.PT_OID AND P.CREATED_USER = N.CREATED_USER)
	WHEN MATCHED THEN
		UPDATE SET
			P.PT_SIZE = N.PT_SIZE,
			P.PT_COLOR = N.PT_COLOR,
			P.PT_TRANS = N.PT_TRANS,
			P.PT_OUT_TYPE = N.PT_OUT_TYPE,
			P.PT_OUT_WIDTH = N.PT_OUT_WIDTH,
			P.PT_OUT_COLOR = N.PT_OUT_COLOR,
			P.LAST_UPDATED_USER = '$user',
			P.LAST_UPDATED_DATE = SYSDATE
	WHEN NOT MATCHED THEN
		INSERT (PT_LAYER, PT_OID, PT_SIZE, PT_COLOR, PT_TRANS, PT_OUT_TYPE, PT_OUT_WIDTH, PT_OUT_COLOR, CREATED_USER)
		VALUES (N.PT_LAYER, N.PT_OID, N.PT_SIZE, N.PT_COLOR, N.PT_TRANS, N.PT_OUT_TYPE, N.PT_OUT_WIDTH, N.PT_OUT_COLOR, '$user')";
}
else if( $symType == 'polyline' ){

	$sql = "MERGE INTO NISIS.SYMBOL_POLYLINES P
	USING (SELECT '$layer' AS PL_LAYER, '$objid' AS PL_OID, '$symStyle' AS PL_TYPE, '$outWidth' AS PL_WIDTH,
		'$outColor' AS PL_COLOR, '$outTrans' AS PL_TRANS, '$user' AS CREATED_USER FROM DUAL) N
	ON (P.PL_LAYER = N.PL_LAYER AND P.PL_OID = N.PL_OID AND P.CREATED_USER = N.CREATED_USER)
	WHEN MATCHED THEN
		UPDATE SET P.PL_TYPE = N.PL_TYPE,
			P.PL_WIDTH = N.PL_WIDTH,
			P.PL_COLOR = N.PL_COLOR,
			P.PL_TRANS = N.PL_TRANS,
			P.LAST_UPDATED_USER = '$user',
			P.LAST_UPDATED_DATE = SYSDATE
	WHEN NOT MATCHED THEN
		INSERT (PL_LAYER, PL_OID, PL_TYPE, PL_WIDTH, PL_COLOR, PL_TRANS, CREATED_USER)
		VALUES (N.PL_LAYER, N.PL_OID, N.PL_TYPE, N.PL_WIDTH, N.PL_COLOR, N.PL_TRANS, '$user')";

}
else if( $symType == 'polygon' ){

	$sql = "MERGE INTO NISIS.SYMBOL_POLYGONS P
	USING (SELECT '$layer' AS PG_LAYER, '$objid' AS PG_OID, '$symStyle' AS PG_FILL_TYPE,
		'$symColor' AS PG_FILL_COLOR, '$symTrans' AS PG_FILL_TRANS, '$outStyle' AS PG_OUT_TYPE,
		'$outWidth' AS PG_OUT_WIDTH, '$outColor' AS PG_OUT_COLOR, '$user' AS CREATED_USER FROM DUAL) N
	ON (P.PG_LAYER = N.PG_LAYER AND P.PG_OID = N.PG_OID AND P.CREATED_USER = N.CREATED_USER)
	WHEN MATCHED THEN
		UPDATE SET P.PG_FILL_TYPE = N.PG_FILL_TYPE,
			P.PG_FILL_COLOR = N.PG_FILL_COLOR,
			P.PG_FILL_TRANS = N.PG_FILL_TRANS,
			P.PG_OUT_TYPE = N.PG_OUT_TYPE,
			P.PG_OUT_WIDTH = N.PG_OUT_WIDTH,
			P.PG_OUT_COLOR = N.PG_OUT_COLOR,
			P.LAST_UPDATED_USER = '$user',
			P.LAST_UPDATED_DATE = SYSDATE
	WHEN NOT MATCHED THEN
		INSERT (PG_LAYER, PG_OID, PG_FILL_TYPE, PG_FILL_COLOR, PG_FILL_TRANS, PG_OUT_TYPE, PG_OUT_WIDTH, PG_OUT_COLOR, CREATED_USER)
		VALUES (N.PG_LAYER, N.PG_OID, N.PG_FILL_TYPE, N.PG_FILL_COLOR, N.PG_FILL_TRANS, N.PG_OUT_TYPE, N.PG_OUT_WIDTH, N.PG_OUT_COLOR, '$user')";
}

$parsed_sql = oci_parse($db, $sql);

error_log('parsed query=' . $sql);
//exit();

if( !oci_execute($parsed_sql) ) {
	$e = oci_error($parsed_sql);

	error_log('Custom styles error: ' . $e['message']);
	error_log('Custom styles sql error: ' . $sql);

	kill(array('return' => 'Failure', 'message' => $e['message']));
}

exit(json_encode(array('return' => 'Success', 'results' => TRUE)));

?>
