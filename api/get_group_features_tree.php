<?php
//CC - This file was created to get the Group Features tree from the DB.

if (isset($_POST["grpId"])) {
	$grpId = $_POST["grpId"];
}else{
	error_log("Group ID - grpId is not defined in get_group_features_tree.php");
	kill(array('result' => 'Group ID - grpId is not defined in get_group_features_tree.php'));
}

$query = "SELECT NISIS_ADMIN.fm_pkg.get_group_features (:grpId, :userId) as TREE FROM DUAL";

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":grpId", $grpId);
oci_bind_by_name($parsed, ":userId", $_SESSION['userid']);

if(!oci_execute($parsed)) {
	kill("DATABASE ERROR when trying to get Group Features tree from the DB in get_group_features_tree.php");
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

$tree = json_decode($results[0]['TREE']);
// error_log(var_export($tree, true));

switch (json_last_error()) {
	case JSON_ERROR_NONE:
		error_log(' - No errors decoding JSON in get_group_features_tree.php');
	break;
	case JSON_ERROR_DEPTH:
		error_log(' - Maximum stack depth exceeded decoding JSON in get_group_features_tree.php');
	break;
	case JSON_ERROR_STATE_MISMATCH:
		error_log(' - Underflow or the modes mismatch decoding JSON in get_group_features_tree.php');
	break;
	case JSON_ERROR_CTRL_CHAR:
		error_log(' - Unexpected control character found decoding JSON in get_group_features_tree.php');
	break;
	case JSON_ERROR_SYNTAX:
		error_log(' - Syntax error, malformed JSON in get_group_features_tree.php');
	break;
	case JSON_ERROR_UTF8:
	   error_log(' - Malformed UTF-8 characters, possibly incorrectly encoded JSON in get_group_features_tree.php');
	break;
	default:
		error_log(' - Unknown error decoding JSON in get_group_features_tree.php');
	break;
}

kill(array("result" => "Success", "tree" => $tree), FALSE);

?>