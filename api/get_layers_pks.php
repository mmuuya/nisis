<?php
if(!isset($_SESSION['layers_pks'])) {
    $query = 'SELECT LAYER AS LAYERID, TABLE_NM, TABLE_PK
                FROM NISIS_ADMIN.DATABLOCK_LAYERS_VW';

    $parsed = oci_parse($db, $query);
    if(!oci_execute($parsed)) {
        kill("DATABASE ERROR while trying to get the layers ids and the primary keys from DB in get_layers_pks.php");
    }

    // oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_COLUMN+OCI_ASSOC);

    while(oci_fetch($parsed)) {
        $layerid = oci_result($parsed, "LAYERID");
        $table_nm = oci_result($parsed, "TABLE_NM");
        $table_pk = oci_result($parsed, "TABLE_PK");
        
        $_SESSION['layers_pks'][$layerid] = array ("table_nm" => $table_nm, "table_pk" => $table_pk);
    }
    //error_log(var_export($_SESSION['layers_pks'], true));
}

kill(array("result" => "Success", "layersPks" => $_SESSION['layers_pks']), FALSE);

?>