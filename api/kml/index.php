<?php
//Get our configurations and global stuff, or fail if we can't.
if ((include '../../handler.php') === FALSE) {
	exit(json_encode(array('return' => 'failure', 'message' => 'Misconfigured Server')));
}

//check request params
if ( !isset($_REQUEST['action']) || !isset($_REQUEST['features'])) { 
    exit(json_encode(array('return'=>'failure', 'message'=>'Missing or invalid arguments.')));
}

switch ($_REQUEST['action']) {
    
    case "generate-kml":
        include 'generate-kml.php';
    break;

    default:
        echo 'Unknown/Invalid action';
        exit(json_encode(array('return'=>'failure', 'message'=>'Unknown/Invalid action.')));

}