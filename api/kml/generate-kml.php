<?php
/**
  * Example File
  *
  * @package kmlInstaller
  */

/**
  * KML definition file inclusion
  */
include_once('kml.class.php');

$kml = new KML('NISIS OUTPUT');

$document = new KMLDocument('nisis_output', 'NISIS OUTPUT');

$features = json_decode( $_REQUEST['features'] );

$date = date("m-d-Y-Gi", time());


/**
  * Style definitions
  */
$point = new KMLStyle('pointsStyle');
$point->setIconStyle('images/world.png', 'ff00ff00', 'normal', 0.2);
$document->addStyle($point);

$lines = new KMLStyle('linesStyle');
$lines->setLineStyle('ffffff', 'normal', 3);
$document->addStyle($lines);

$polygon = new KMLStyle('polygonsStyle');
$polygon->setPolyStyle('660000');
$document->addStyle($polygon);

/**
  * File adds
  */
$kml->addFile('images/world.png', $domain . '/nisis/images/nav/world1.png');

//Folders
$points = new KMLFolder('', 'Points');
$polylines = new KMLFolder('', 'Polylines');
$polygons = new KMLFolder('', 'Polygons');

//count shapes
$pts = 0;
$lns = 0;
$poly = 0;

/**
  * Geometries
  * Placemark ==>   new KMLPlaceMark(id, name, description, visible);
  * Style ==>       new KMLStyle(id)
  * Point ==>       new KMLPoint(-1, 2, 0)
  * Polyline ==>    new KMLPolygon( Array (array ( 2, 0,0), array (-4, 0,0), array (-5, 5,100),  array ( 1, 5,0), array ( 2, 0,0)), true, '', true))
  * Polygon ==>     new KMLPolygon( Array (array ( 2, 0,0), array ( 1, 5,0), array ( 2, 0,0)), true, '', true)
  *
  */

$n = count($features);

for ($i=0; $i < $n; $i++) {

  if ( isset( $features[$i]->geometry ) ) {
    $geom = $features[$i]->geometry;
    $gType = $features[$i]->type;

    $placemark = new KMLPlaceMark('', $gType, '', true);

    switch ($gType) {

        case 'point':
            
            $kmlPt = new KMLPoint($geom[0], $geom[1], 0 );
            $placemark->setGeometry($kmlPt);
            $placemark->setStyleUrl('#pointsStyle');
            $points->addFeature($placemark);

            $pts++;

        break;

        case 'polyline':

            $vertices = Array();

            foreach ($geom as $pos => $path) {

                foreach ($path as $i => $pt) {
                    array_push( $vertices, Array( $pt[0], $pt[1], 0) );
                }
            }

            $kmlPl = new KMLLineString( $vertices );
            $placemark->setGeometry($kmlPl);
            $placemark->setStyleUrl('#linesStyle');
            $polylines->addFeature($placemark);

            $lns++;

        break;

        case 'polygon':

            $vertices = Array();

            foreach ($geom as $pos => $ring) {

                foreach ($ring as $i => $pt) {
                    array_push( $vertices, Array( $pt[0], $pt[1], 0) );
                }
            }

            $kmlPg = new KMLPolygon( $vertices );
            $placemark->setGeometry($kmlPg);
            $placemark->setStyleUrl('#polygonsStyle');
            $polygons->addFeature($placemark);

            $poly++;

        break;

        default:

        break;
    }

  } else {
    //echo "No000 Geometries";
  }
}

//check folders counts
if ( ($pts + $lns + $poly) < 1) {
    exit(json_encode(array('return'=>'failure', 'message'=>'Nossss valid shape found.')));
}

//add folders to document
if ($pts > 0) {
    $document->addFeature($points);
}

if ($lns > 0) {
    $document->addFeature($polylines);
}

if ($poly > 0) {
    $document->addFeature($polygons);
}
//set document
$kml->setFeature($document);

$user;
if ( isset($_SESSION) && isset($_SESSION['username'])) {
  $user = $_SESSION['username'];
} else {
  $user = "annonymous";
}

$name = $user . "_output_" . $date;

$location = '/nisis/api/kml/output/';

$url = $location . $name . ".kml";

$kml->output('F', 'output/' . $name . '.kml');
//$kml->output('Z', 'output/' . $name . '.kmz');

//header("Location: $url");

exit(json_encode(array('return'=>'success', 
    'message'=>'Done processing kml file.', 
    'file'=>$kml, 
    'path'=>$url)));

?>
