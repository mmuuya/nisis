<?php
require_once ('../handler.php');

//clean up if they clicked on cancel mid-upload - make sure we clean up the database
$i_ref_objid = $_POST["objectid"];
$i_file_name =$_POST["filename"];
$i_table_name =$_POST["facTable"];


$query = "DELETE FROM NISIS.snapshot_uploads where ref_objid = :i_ref_objid and ref_table = :i_table_name and file_nm = :i_file_name";

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":i_ref_objid", $i_ref_objid);
oci_bind_by_name($parsed, ":i_file_name", $i_file_name);
oci_bind_by_name($parsed, ":i_table_name", $i_table_name);

if(!oci_execute($parsed)) {
    kill("DATABASE ERROR when trying to delete an image");
}


kill(array('return' => 'Success', 'results' => TRUE), FALSE);
?>