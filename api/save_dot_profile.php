<?php
$objectid = $_POST["objectid"];
$tablenm = $_POST["tablenm"];
$changes = json_decode($_POST['changes']);
$array;
$tablenm = 'NISIS_GEODATA.'.$tablenm;
$array = $changes;

if (count($array) > 0) {
    foreach ($array as $key => $value) {
        error_log("IRENA - key, value | $key $value");
        $value = str_replace("'", "''",$value);
        $updates[] = "$key = '$value'";
    }
}
$set_clause = implode(', ', $updates);
$query = "UPDATE {$tablenm} SET {$set_clause} WHERE OBJECTID=:objectid";
$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":objectid", $objectid);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in save_dot_profile api', 'error' => $errStr));
}
else {
    kill(array('result' => 'Success'), FALSE);
}

?>