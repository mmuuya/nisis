<?php
//Get an array containing the objectids for the IWL specified by its ID.

$iwl_id = $_POST['iwl_id'];

$timeout = 5 * 60; //how much time (in seconds) an IWL's oid's will be "cached"

$cacheOK = isset($_SESSION['iwlOids'][$iwl_id]) && ((time() - $_SESSION['iwlOids'][$iwl_id . '-qtime']) < $timeout);
if(!$cacheOK){
	//the oids for this IWL are not in the session (or they are, but they were queried too long ago)
	require "../admin/api/acl_lib_lite.php";

	$usergroups = $_SESSION['groupids'];
	$len = count($usergroups);

	$query = "SELECT OBJECTID FROM NISIS.NISIS_IWL_OBJECTS WHERE IWL_ID=:iwlid";

	$query .= " ORDER BY OBJECTID";

	$parsedquery = oci_parse($db, $query);
	oci_bind_by_name($parsedquery, ":iwlid", $iwl_id);

	//If this fails, somebody wrote something that broke the query when it was escaped.
	if(!oci_execute($parsedquery)){
		kill(array('return' => 'Failure', 'result' => 'Malformed Query', 
			'error' => $query));
		error_log('Possible SQL Injection Attack!');
	}

	oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_COLUMN);
	
	$_SESSION['iwlOids'][$iwl_id] = $results['OBJECTID'];
	$_SESSION['iwlOids'][$iwl_id . '-qtime'] = time();

}

kill(array('return' => 'Success', 'results' => $_SESSION['iwlOids'][$iwl_id]), FALSE);

?>
