<?php
//Get the groups that an IWL is associated with. Returns a simple array.

$iwl_id = $_POST['iwl_id'];

$parsed = oci_parse($db, 'SELECT GROUP_ID FROM NISIS.NISIS_IWL_GROUP WHERE IWL_ID=:iwl_id');
oci_bind_by_name($parsed, ":iwl_id", $iwl_id);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query', 'error' => $errStr));
}

oci_fetch_all($parsed, $results);
$ids = $results['GROUP_ID'];

if(count($ids) === 0){
    $ids = array('0');
}

kill(array('result' => 'Success', "groups" => $ids), FALSE);

?>