<?php
	require ('../handler.php');

	if ($db === FALSE) {
		kill('No Database Connection');
	}

	//check if term param exist
	if (!array_key_exists('target', $_REQUEST) || !array_key_exists('filter', $_REQUEST)) {
		kill("Missing correct required argument: 'target and/or filter'");
	}

	$target = null;
	$filter = null;

	if ($_REQUEST['filter']) {
		$filter = $_REQUEST['filter'];
	}

	if ( $_REQUEST['target'] ) {
		$target = $_REQUEST['target'];
	}

	function getAirports($arpid) {
		global $db;
		global $filter;

		if ($arpid == "") {
			$arpid = 'ABC';
			//$arpid = 'I';
		}

		error_log('Airports filter: ' . $arpid);
		error_log('Airports key: ' . $filter);

	    $query = "SELECT
	    	faa,
       		icao,
       		tp,
       		name,
       		latitude,
       		longitude,
       		CASE icaox
          		WHEN '[]' THEN faa || ' - ' || name || ''
          		ELSE faa || ' (' || icao || ') - ' || name
       		END AS label
  		FROM (
  			SELECT
  				CAST(faa_id AS varchar2(10)) AS faa,
               	CAST (icao_id AS VARCHAR2 (10)) AS icao,
               	'[' || CAST (icao_id AS VARCHAR2 (10)) || ']' AS icaox,
               	arp_type AS tp,
               	CAST (lg_name AS VARCHAR2 (100)) AS name,
               	latitude,
               	longitude
          	FROM nisis_geodata.nisis_airports)
 		WHERE faa like '%" . strtoupper($arpid) . "%'
 			OR icao like '%" . strtoupper($arpid) . "%'
 			OR name like '%" . strtoupper($arpid) . "%'
 		ORDER BY faa";

	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving states: '. $e['message']);
	    	kill(array());
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit(json_encode($results));
	};

	function getVors($vorid) {
		global $db;

		if ($vorid == "") {
			$vorid = "A";
		}

		//OR basic_tp LIKE '%" . strtoupper($vorid) . "%'
   		//OR basic_cat LIKE '%" . strtoupper($vorid) . "%'

	    /*$query = "SELECT DISTINCT ans_id AS ans,
				basic_tp AS tp,
				basic_cat AS cat,
				descript,
				latitude,
				longitude,
				ans_id || ' (' || basic_tp || ') - ' || descript AS label
			FROM nisis_geodata.nisis_ans ";
   		$query .= "WHERE (ans_id LIKE '%" . strtoupper($vorid) . "%'
   				OR descript LIKE '%" . strtoupper($vorid) . "%')
				AND basic_tp = 'NAVIGATION'
                AND basic_cat IN ('VOR', 'TACR')
   				AND ROWNUM <= 20
			ORDER BY ans_id";*/

	    $query = "select
			ans, tp, cat, descript,
			latitude, longitude, label,
			magnetic_var, magnetic_var_n as mag
			from nisis.nisis_vor_vw";

		if ($vorid != "") {
			$query .= " where label like '%";
			$query .= strtoupper($vorid);
			$query .= "%'";

		} else {
			$query .= " where rownum < 100";
		}

		$query .= " order by ans asc";
		//error_log('Retreiving vors: '. $query);
	    $parsed = oci_parse($db, $query);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	//error_log('Error retreiving vor: '. $e['message']);
	    	kill(array());
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit(json_encode($results));
	};
	//GET CLOSEST VOR
	function getClosestVors($lat, $lon, $num) {
		global $db;
		// error_log('lat: '. $lat . ' long: '. $lon . ' num: '. $num);
		//try SDE ==> ~6s
	    /*$query = "SELECT * FROM
				(SELECT ANS_ID, LATITUDE, LONGITUDE, SDE.ST_DISTANCE(
				    SDE.ST_POINT(LONGITUDE, LATITUDE, 4326),
				    SDE.ST_POINT(" . $lon . ", " . $lat . ", 4326)) AS DISTANCE
				FROM NISIS_GEODATA.NISIS_ANS
				WHERE GDB_TO_DATE > SYSDATE
				AND BASIC_TP = 'NAVIGATION'
				AND BASIC_CAT IN ('VOR', 'TACR')
				ORDER BY DISTANCE ASC) WHERE ROWNUM <= " . $num;*/

		//try SDO ==> ~2s
		$range = 1; //1 degree (approx 120 nautical miles) by default, expanded if no results found.
		$i = 0;
		do {
			// $query = "SELECT * FROM
			// 		(SELECT ANS_STATUS, ANS_ID || ' (' || DESCRIPT || ')' AS LABEL,
			// 			LATITUDE, LONGITUDE,
			// 			SDO_GEOM.SDO_DISTANCE(
			// 		    MDSYS.SDO_GEOMETRY('POINT(' || LONGITUDE || ' ' || LATITUDE || ')'),
			// 		    MDSYS.SDO_GEOMETRY('POINT('||:lon||' '||:lat||')'),
			// 		    0.005) AS DISTANCE
			// 		FROM NISIS_GEODATA.NISIS_ANS
			// 		WHERE BASIC_TP = 'NAVIGATION'
			// 		AND BASIC_CAT IN ('VOR', 'TACR')
			// 		AND LONGITUDE BETWEEN (:lon - :range) AND (:lon + :range)
			// 		AND LATITUDE BETWEEN (:lat - :range) AND (:lat + :range)
			// 		ORDER BY DISTANCE ASC)
			// 		WHERE ROWNUM <= :num";

			$query = "SELECT *
					FROM (  SELECT ans_status,
					                 ans || ' (' || descript || ')' AS label,
					                 latitude,
					                 longitude,
					                 SDO_GEOM.sdo_distance (
					                    mdsys.sdo_geometry (
					                       'POINT(' || longitude || ' ' || latitude || ')'),
					                    mdsys.sdo_geometry ('POINT(' || :lon || ' ' || :lat || ')'),
					                    0.005)
					                    AS distance,
					                 magnetic_var_n
					            FROM nisis.nisis_vor_vw x
					           WHERE    cat IN ('VOR', 'TACR')
					                 OR     (    cat = 'DME'
					                         AND (   fac_ident IN (SELECT fac_ident
					                                                 FROM nisis.nisis_vor_vw
					                                                WHERE cat = 'VOR')
					                              OR navaid_fac_id IN (SELECT navaid_fac_id
					                                                     FROM nisis.nisis_vor_vw
					                                                    WHERE cat = 'VOR')))
					                    AND longitude BETWEEN ( :lon - :range) AND ( :lon + :range)
					                    AND latitude BETWEEN ( :lat - :range) AND ( :lat + :range)
					        ORDER BY distance ASC)
					 WHERE ROWNUM <= :num";

			// error_log ($query);

		    $parsed = oci_parse($db, $query);
		    oci_bind_by_name($parsed, ":lon", $lon);
		    oci_bind_by_name($parsed, ":lat", $lat);
		    oci_bind_by_name($parsed, ":num", $num);
		    oci_bind_by_name($parsed, ":range", $range);

		    if (!oci_execute($parsed)) {
		    	$e = oci_error($parsed);
		    	error_log('Error retreiving closest vor: '. $e['message']);
		    	kill(array());
		    };

		    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

		    if(count($results) == 0){
		    	$range++;
		    } else {
		    	$i++;
		    }
		} while ($i == 0);

	    //return $results;
	    exit(json_encode($results));
	};
	//GET VOR BY ID
	function getVorByID($vorId) {
		global $db;

		$query = "SELECT ANS_ID, DESCRIPT, LATITUDE, LONGITUDE
				FROM NISIS_GEODATA.NISIS_ANS
				WHERE BASIC_TP = 'NAVIGATION'
				AND BASIC_CAT IN ('VOR', 'TACR')
				AND ANS_ID = :id
				AND ROWNUM = 1";

	    $parsed = oci_parse($db, $query);
	    oci_bind_by_name($parsed, ":id", $vorId);

	    if (!oci_execute($parsed)) {
	    	$e = oci_error($parsed);
	    	error_log('Error retreiving closest vor: '. $e['message']);
	    	kill(array());
	    };

	    oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	    //return $results;
	    exit(json_encode($results));
	};

	switch ($target) {

		case 'airports':
				getAirports($filter);
			break;

		case 'vor':
				getVors($filter);
			break;

		case 'closest-vor':

				$lat = $_REQUEST['lat'];
				$lon = $_REQUEST['lon'];
				$num = $_REQUEST['num'];

				getClosestVors($lat, $lon, $num);

			break;

		case 'vor-info':
				getVors($filter);

			break;

		case 'clock':
				exit(json_encode(new DateTime('NOW', new DateTimeZone("UTC") )));
				//exit(json_encode( gmmktime() ));

			break;

		default:

			break;
	}
?>
