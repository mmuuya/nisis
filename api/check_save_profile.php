<?php
//Verify the user is allowed to edit the modified fields.

$TABLE = $_POST['table'];
$fieldsChanged = $_POST['changes'];
require "../lib/user_field_perms.php";

$isANS = FALSE;
if($TABLE === "NISIS_ANS") {
    $isANS = TRUE;
}

foreach($fieldsChanged as $curField){
    //need to also check for an ANS Status change
    if($isANS && $curField === "ANS_STATUS") {
        // error_log("MARKING ANS STATUS CHANGE AS HUMAN-MADE");
    }
    //now do the actual permission verification
    if(getPermission($curField) !== $WRITABLE){
        kill("false", FALSE);
    }
}

//got through all fields, proceed with saving
kill("true", FALSE);

?>
