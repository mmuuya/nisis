<?php
	require("../handler.php"); 
	
	// NISIS-2767 KOFI HONU 
	// CHANGED USE OF NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VW TO NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW
	// IT'S MORE LIVE.
	$queryType 	= null;
	$iwlFilter 	= null;
	$analysis	= null;
	if(isset($_POST['queryType'])){
		$queryType = $_POST['queryType'];
	}

	if(isset($_POST['filterIWL'])){
		$iwlFilter = $_POST['filterIWL'];
	}

	if(isset($_POST['analysis'])){
		$analysis = $_POST['analysis'];
	}
	// error_log('Value passed to ovr_status is: ' . $queryType);

	$query;

	/*
		The following query builder took what used to be a simple "select all that meet this criteria" statement
		and added a secondary layer of complexity on top of it to account for IWL filtering. Although it is hard
		to read, it seems to work nicely. Basically, any query assignment not encased in the if(isset(...)) command
		creates a basic search query. If the user has selected an IWL, the original query needs to be updated
		to include ObjectID information, and then that entire query needs to be wrapped in another select statement
		with it's own group by definition to ensure the database doesn't return values to
		the web interface individualy.

		A third layer of complexity was introduced with readiness level (which only applies to airports, atm and osf).
		This changes the data that is returned, but does not change the alias of the columns, so it should plug directly
		into the front-end implementation. For this I have inserted a short-hand if statement that looks akin to the 
		following: ".(isset($analysis) ? 'CRT_RL' : 'OVR_STATUS')."
		This will basically swap out the queried columns depending on whether Readiness Level is selected or not.

		Original: 
		SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
                WHERE TABLE_NM = 'NISIS_AIRPORTS' AND COL_NM = 'OVR_STATUS' 
                AND N.OVR_STS = VALUE) OVR_STATUS_TEXT, SOURCETABLE, OVR_STS OVR_STATUS, COUNT(OVR_STS) COUNT FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_AIRPORTS' 
                AND OVR_STS IS NOT NULL GROUP BY OVR_STS, SOURCETABLE ORDER BY OVR_STATUS ASC;

        Updated with IWL filter (in lower case):
		select ovr_status_text, sourcetable, ovr_status, count(*) count from (SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
                WHERE TABLE_NM = 'NISIS_AIRPORTS' AND COL_NM = 'OVR_STATUS' 
                AND N.OVR_STS = VALUE) OVR_STATUS_TEXT, SOURCETABLE, OVR_STS OVR_STATUS, COUNT(OVR_STS) COUNT,
                objectid
                FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_AIRPORTS' 
                AND OVR_STS IS NOT NULL GROUP BY OVR_STS, SOURCETABLE, objectid ORDER BY OVR_STATUS ASC) a 
                join nisis.nisis_iwl_objects b on a.objectid = b.objectid and b.iwl_id = :iwlFilter
                group by ovr_status_text, ovr_status, sourcetable order by ovr_status asc;

		Updated with readiness level toggle (basic):
		SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
                WHERE TABLE_NM = 'NISIS_AIRPORTS' AND lower(COL_NM) = 'crt_rl' 
                AND N.readiness_level = VALUE) OVR_STATUS_TEXT, SOURCETABLE, readiness_level OVR_STATUS, COUNT(readiness_level) COUNT
                FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_AIRPORTS' 
                AND readiness_level IS NOT NULL GROUP BY readiness_level, SOURCETABLE
                ORDER BY OVR_STATUS ASC;

		Updated with readiness level toggle (with IWL filter):
		SELECT OVR_STATUS_TEXT, SOURCETABLE, OVR_STATUS, COUNT(*) COUNT FROM (SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
                WHERE TABLE_NM = 'NISIS_AIRPORTS' AND COL_NM = 'crt_rl' 
                AND N.readiness_level = VALUE) OVR_STATUS_TEXT, SOURCETABLE, readiness_level OVR_STATUS, COUNT(readiness_level) COUNT,
                OBJECTID
                FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_AIRPORTS' 
                AND readiness_level IS NOT NULL GROUP BY readiness_level, SOURCETABLE, OBJECTID ORDER BY OVR_STATUS ASC) A 
                JOIN NISIS.NISIS_IWL_OBJECTS B ON A.OBJECTID = B.OBJECTID AND B.IWL_ID = :IWLFILTER
                GROUP BY OVR_STATUS_TEXT, OVR_STATUS, SOURCETABLE ORDER BY OVR_STATUS ASC;

	*/

	switch ($queryType){
		case "dash_airports":			
			// error_log("first case entered");
			$query = "SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
				WHERE TABLE_NM = 'NISIS_AIRPORTS' AND COL_NM = '".(isset($analysis) ? 'CRT_RL' : 'OVR_STATUS')."'  
                AND N.".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')." 
                = VALUE) OVR_STATUS_TEXT, SOURCETABLE, ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')." 
				OVR_STATUS, COUNT(".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS').") COUNT";
			if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_AIRPORTS' 
                AND ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')."  IS NOT NULL GROUP BY 
                ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS').", SOURCETABLE";
            if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " ORDER BY OVR_STATUS ASC";
			break;

		case "dash_airports_pub":			
			// error_log("first case entered");
			$query = "SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
				WHERE TABLE_NM = 'NISIS_AIRPORTS' AND COL_NM = '".(isset($analysis) ? 'CRT_RL' : 'OVR_STATUS')."'  
                AND N.".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')." 
                = VALUE) OVR_STATUS_TEXT, SOURCETABLE, ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')." 
				OVR_STATUS, COUNT(".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS').") COUNT";
			if (isset($iwlFilter)){
				$query .= ", N.OBJECTID";
			}
            $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N
                JOIN NISIS_GEODATA.NISIS_AIRPORTS B ON N.FAA_ID = B.FAA_ID
                WHERE UPPER(N.SOURCETABLE) LIKE 'NISIS_AIRPORTS' 
                AND N.".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')."  IS NOT NULL 
                AND B.BASIC_USE = 'PU'
                GROUP BY N.".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS').", SOURCETABLE";
            if (isset($iwlFilter)){
				$query .= ", N.OBJECTID";
			}
            $query .= " ORDER BY OVR_STATUS ASC";
			break;

		case "dash_atm":
			$query = "SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS
				WHERE TABLE_NM = 'NISIS_ATM' AND COL_NM = '".(isset($analysis) ? 'CRT_RL' : 'OPS_STATUS')."' 
                AND N.".(isset($analysis) ? 'READINESS_LEVEL' : 'ATM_OS')." 
                = VALUE) OVR_STATUS_TEXT, SOURCETABLE, ".(isset($analysis) ? 'READINESS_LEVEL' : 'ATM_OS')." 
				OVR_STATUS,  COUNT(".(isset($analysis) ? 'READINESS_LEVEL' : 'ATM_OS').") COUNT";
			if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_ATM' 
                AND ".(isset($analysis) ? 'READINESS_LEVEL' : 'ATM_OS')." IS NOT NULL GROUP BY 
                ".(isset($analysis) ? 'READINESS_LEVEL' : 'ATM_OS').", SOURCETABLE";
            if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " ORDER BY OVR_STATUS ASC";
			break;

		case "dash_ans":
			$query = "SELECT  (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
				WHERE TABLE_NM = 'NISIS_ANS' AND COL_NM = 'ANS_STATUS' 
                AND N.ANS_STS = VALUE) OVR_STATUS_TEXT, ANS_STS OVR_STATUS, SOURCETABLE, COUNT(ANS_STS) COUNT";
			if (isset($iwlFilter)){
				$query .= ", N.OBJECTID";
			}
            $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N 
            JOIN NISIS_GEODATA.NISIS_ANS B ON N.FAA_ID = B.ANS_ID
            WHERE UPPER(SOURCETABLE) LIKE 'NISIS_ANS' AND ANS_STS 
            IS NOT NULL AND B.NAPRS_REPORTABLE = 'Y'
            GROUP BY ANS_STS, SOURCETABLE";
            if (isset($iwlFilter)){
				$query .= ", N.OBJECTID";
			}
            $query .= " ORDER BY OVR_STATUS ASC";
			break;

		case "dash_osf":
			$query = "SELECT  (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
                WHERE TABLE_NM = 'NISIS_OSF' AND COL_NM = '".(isset($analysis) ? 'CRT_RL' : 'BOPS_STS')."' 
                AND N.".(isset($analysis) ? 'READINESS_LEVEL' : 'BO_STS')." 
                = VALUE) OVR_STATUS_TEXT, SOURCETABLE, ".(isset($analysis) ? 'READINESS_LEVEL' : 'BO_STS')." OVR_STATUS,  
				COUNT(".(isset($analysis) ? 'READINESS_LEVEL' : 'BO_STS').") COUNT";
			if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_OSF' 
            	AND ".(isset($analysis) ? 'READINESS_LEVEL' : 'BO_STS')." IS NOT NULL GROUP BY 
            	".(isset($analysis) ? 'READINESS_LEVEL' : 'BO_STS').", SOURCETABLE";
            if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= "  ORDER BY OVR_STATUS ASC";
			break;


		case "dash_tof":
			$query = "SELECT (SELECT LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
				WHERE TABLE_NM = 'NISIS_TOF' AND COL_NM = '".(isset($analysis) ? 'CRT_RL' : 'OPS_STATUS')."'  
                AND N.".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')." 
                = VALUE) OVR_STATUS_TEXT, SOURCETABLE, ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')." 
				OVR_STATUS, COUNT(".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS').") COUNT";
			if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_TOF' 
                AND ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS')."  IS NOT NULL GROUP BY 
                ".(isset($analysis) ? 'READINESS_LEVEL' : 'OVR_STS').", SOURCETABLE";
            if (isset($iwlFilter)){
				$query .= ", OBJECTID";
			}
            $query .= " ORDER BY OVR_STATUS ASC";
			break;

	  // NISIS-2742 - KOFI HONU 
		// case "dash_resp":
		// 	$query = "SELECT  (SELECT FIELD_DISPLAY_NM || ':' || LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
  //               WHERE TABLE_NM = 'NISIS_RESP' AND COL_NM = 'FS_INTER'          
  //               AND N.RES_STS_S = VALUE) OVR_STATUS_TEXT, RES_STS_S OVR_STATUS, SOURCETABLE, COUNT(RES_STS_S) COUNT";
		// 	if (isset($iwlFilter)){
		// 		$query .= ", OBJECTID";
		// 	}
  //           $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_RESP' AND RES_STS_S 
  //               IS NOT NULL GROUP BY RES_STS_S, SOURCETABLE";
		// 	if (isset($iwlFilter)){
		// 		$query .= ", OBJECTID";
		// 	}
  //           $query .= " UNION ALL
		// 	SELECT  (SELECT FIELD_DISPLAY_NM || ':' || LABEL FROM NISIS.ALL_FIELD_PICKLISTS 
  //               WHERE TABLE_NM = 'NISIS_RESP' AND COL_NM = 'IWL_DSG'
  //               AND N.RES_STS_S = VALUE) OVR_STATUS_TEXT, RES_STS_S OVR_STATUS, SOURCETABLE, COUNT(RES_STS_S) COUNT";
		// 	if (isset($iwlFilter)){
		// 		$query .= ", OBJECTID";
		// 	}
  //           $query .= " FROM NISIS_GEODATA.MASTER_OBJECTS_SHAPELESS_VIEW N WHERE UPPER(SOURCETABLE) LIKE 'NISIS_RESP' AND RES_STS_S 
  //               IS NOT NULL GROUP BY RES_STS_S, SOURCETABLE";
		// 	if (isset($iwlFilter)){
		// 		$query .= ", OBJECTID";
		// 	}
  //           $query .= " ORDER BY OVR_STATUS ASC";
		// 	break;
		default:
			//error_log("default case entered");
			$query = "SELECT 'none' SOURCETABLE, null OVR_STATUS_TEXT, null OVR_STATUS, 'undefined' COUNT 
				FROM DUAL CONNECT BY ROWNUM <= 6";
	}

	$parsedquery = oci_parse($db, $query); 

	if (isset($iwlFilter) && $queryType != ''){
		$query = "SELECT OVR_STATUS_TEXT, SOURCETABLE, OVR_STATUS, COUNT(*) COUNT FROM (" . $query . 
			") A JOIN NISIS.NISIS_IWL_OBJECTS B ON A.OBJECTID = B.OBJECTID AND B.IWL_ID =:iwlFilter
        GROUP BY OVR_STATUS_TEXT, OVR_STATUS, SOURCETABLE ORDER BY OVR_STATUS ASC";     
		$parsedquery = oci_parse($db, $query); 
		oci_bind_by_name($parsedquery, ":iwlFilter", $iwlFilter);
	}

	// error_log($query);
	oci_execute($parsedquery); 
	oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

	// error_log($queryType);
	exit(json_encode($results)); 
?>