<?php 

// Define the Default Sort Order if nothing is sent from the grid
if ( isset( $_REQUEST['sort'][0]['field'] ) ) {
    $sortColDir = $_REQUEST['sort'][0]['field'];
}else{
    $sortColDir = "status_date"; 
}

if ( isset( $_REQUEST['sort'][0]['dir'] ) ) {
    $sortColDir .= " " . $_REQUEST['sort'][0]['dir'];
}else{
    $sortColDir .= " desc"; 
}


//error_log(var_export($_REQUEST, true));

$where="";

if(isset($_REQUEST['facId'])&&isset($_REQUEST['fieldType'])){
  $where = "fac_id='".$_REQUEST['facId']."' AND col_nm='".$_REQUEST['fieldType']."'";
}else if(isset($_REQUEST['facId'])){
  $where = "fac_id='".$_REQUEST['facId']."'";
}

if(isset($_REQUEST['quicksearch'])){
  $likeqs="LIKE LOWER('%".$_REQUEST['quicksearch']."%')";
  if($where!=""){
    $where=$where." AND ";
  }
  $where = $where."(LOWER(FAC_ID) ".$likeqs." OR LOWER(OBJECTID) ".$likeqs." OR LOWER(FIELD_DISPLAY_NM) ".$likeqs." OR LOWER(TO_CHAR(STATUS_DATE, 'YYYY-MM-DD HH24:MI:SS')) ".$likeqs." OR LOWER(STATUS_DISPLAY) ".$likeqs." OR LOWER(EXPLANATION) ".$likeqs." OR LOWER(USERID) ".$likeqs." OR LOWER(RTG_CODE) ".$likeqs." OR LOWER(IM_ROLE) ".$likeqs.")";
}


if(isset($_REQUEST['filter'])){
  $filters = parseDateFilters( $_REQUEST['filter'] );
  if ($filters!=""){
    if($where!=""){
      $where=$where." AND ";
    }
    $where .= "(".$filters.")";
  }
}

if(isset($_REQUEST['columnfilt'])){
  $filters = parseColumnFilters( $_REQUEST['columnfilt'] );
  if ($filters!=""){
    if($where!=""){
      $where=$where." AND ";
    }
    $where .= "(".$filters.")";
  }
}


//get a list of all the facilities from iwls
if(isset($_REQUEST['pickedIWLs'])){
  $iwls = $_REQUEST['pickedIWLs'];
  $iwlstring="";
  
  for($x=0;$x<count($iwls);$x++){
    if(isset($iwls[$x+1])){
      $iwlstring.="IWL_ID=".$iwls[$x]." OR ";
    }else{
      $iwlstring.="IWL_ID=".$iwls[$x];
    }
  }

  //error_log("iwls: ".$iwlstring);

  $query = "SELECT OBJECTID FROM NISIS.NISIS_IWL_OBJECTS WHERE ".$iwlstring;
  //error_log($query);
  $parsedquery = oci_parse($db, $query);

  //If this fails, somebody wrote something that broke the query when it was escaped.
  if(!oci_execute($parsedquery)){
    kill(array('return' => 'Failure', 'result' => 'Malformed Query', 
      'error' => $query));
    error_log('Possible SQL Injection Attack!');
  }

  oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_COLUMN);
  $facilities=$results["OBJECTID"];
}


if(isset($_REQUEST['pickedShapeFilters'])){
  $shapeFilters = $_REQUEST['pickedShapeFilters'];
  $sfstring=implode(",",$shapeFilters);

//NISIS-2742 KOFI HONU - decommission nisis_resp
  // $query =  "SELECT a.ans_id
  //             FROM nisis_geodata.nisis_ans a, nisis_geodata.nisis_polygons p
  //             WHERE p.objectid = any(".$sfstring.")
  //             AND TRUNC (p.gdb_to_date) = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
  //             AND sde.st_within (a.shape, p.shape) = 1
  //             ORDER BY 1";


  // $query = "SELECT a.objectid, a.ans_id AS faa_id, 'nisis_ans' AS source_table
  //            FROM nisis_geodata.nisis_ans a, nisis_geodata.nisis_polygons p
  //           WHERE     p.objectid = any(".$sfstring.")
  //                 AND sde.st_within (a.shape, p.shape) = 1
  //                 AND TRUNC (p.gdb_to_date) = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
  //           UNION
  //           SELECT a.objectid, a.atm_id AS faa_id, 'nisis_atm' AS source_table
  //            FROM nisis_geodata.nisis_atm a, nisis_geodata.nisis_polygons p
  //           WHERE     p.objectid = any(".$sfstring.")
  //                 AND sde.st_within (a.shape, p.shape) = 1
  //                 AND TRUNC (p.gdb_to_date) = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
  //           UNION
  //           SELECT a.objectid, a.faa_id AS faa_id, 'nisis_airports' AS source_table
  //            FROM nisis_geodata.nisis_airports a, nisis_geodata.nisis_polygons p
  //           WHERE     p.objectid = any(".$sfstring.")
  //                 AND sde.st_within (a.shape, p.shape) = 1
  //                 AND TRUNC (p.gdb_to_date) = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
  //           UNION
  //           SELECT a.objectid, a.osf_id AS faa_id, 'nisis_osf' AS source_table
  //            FROM nisis_geodata.nisis_osf a, nisis_geodata.nisis_polygons p
  //           WHERE     p.objectid = any(".$sfstring.")
  //                 AND sde.st_within (a.shape, p.shape) = 1
  //                 AND TRUNC (p.gdb_to_date) = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
  //           UNION
  //           SELECT a.objectid, a.res_id AS faa_id, 'nisis_resp' AS source_table
  //            FROM nisis_geodata.nisis_resp a, nisis_geodata.nisis_polygons p
  //           WHERE     p.objectid = any(".$sfstring.")
  //                 AND sde.st_within (a.shape, p.shape) = 1
  //                 AND TRUNC (p.gdb_to_date) = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')";

  // NISIS-2754 - KOFI HONU - add nisis_tof
  $query = "SELECT a.objectid, a.ans_id AS faa_id, 'nisis_ans' AS source_table
             FROM nisis_geodata.nisis_ans a, nisis_geodata.nisis_polygons p
            WHERE     p.objectid = any(".$sfstring.")
                  AND sde.st_within (a.shape, p.shape) = 1                  
            UNION
            SELECT a.objectid, a.atm_id AS faa_id, 'nisis_atm' AS source_table
             FROM nisis_geodata.nisis_atm a, nisis_geodata.nisis_polygons p
            WHERE     p.objectid = any(".$sfstring.")
                  AND sde.st_within (a.shape, p.shape) = 1                 
            UNION
            SELECT a.objectid, a.faa_id AS faa_id, 'nisis_airports' AS source_table
             FROM nisis_geodata.nisis_airports a, nisis_geodata.nisis_polygons p
            WHERE     p.objectid = any(".$sfstring.")
                  AND sde.st_within (a.shape, p.shape) = 1                  
            UNION
            SELECT a.objectid, a.osf_id AS faa_id, 'nisis_osf' AS source_table
             FROM nisis_geodata.nisis_osf a, nisis_geodata.nisis_polygons p
            WHERE     p.objectid = any(".$sfstring.")
                  AND sde.st_within (a.shape, p.shape) = 1                  
            UNION
            SELECT a.objectid, a.tof_id AS faa_id, 'nisis_tof' AS source_table
             FROM nisis_geodata.nisis_tof a, nisis_geodata.nisis_polygons p
            WHERE     p.objectid = any(".$sfstring.")
                  AND sde.st_within (a.shape, p.shape) = 1";
  //error_log($query);
  $parsedquery = oci_parse($db, $query);

  //If this fails, somebody wrote something that broke the query when it was escaped.
  if(!oci_execute($parsedquery)){
    kill(array('return' => 'Failure', 'result' => 'Malformed Query in get_history api', 
      'error' => $query));
    error_log('Malformed Query in get_history api');
  }

  oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_COLUMN);
  if(isset($facilities)){
    $facilities=array_intersect($facilities, $results["OBJECTID"]);
  }else{
    $facilities=$results["OBJECTID"];
  }
}


if(isset($facilities)){
  if($where!=""){
    $where=$where." AND ";
  }
  if($facilities==""){
    $where=$where."0=1";
  }else{
    $where .= "OBJECTID=any('".implode("','",$facilities)."')";
  }
}

if($where!=""){
  $where=" WHERE ".$where;
}

//error_log($where);


// Initial count of the returned rows
$total = 0;

switch ( $_REQUEST['actionType'] ) {
    case 'ReadGrid':
        $page = $_REQUEST['page'] - 1;
        $pageSize = $_REQUEST['pageSize'];
        
        $sql = "SELECT rnum,
                       FAC_ID,
                       OBJECTID,
                       SOURCE_TABLE,
                       COL_NM,
                       STATUS_DISPLAY,
                       USERID,
                       STATUS,
                       TO_CHAR(STATUS_DATE, 'YYYY-MM-DD HH24:MI:SS') AS STATUS_DATE,
                       RTG_CODE,
                       IM_ROLE,
                       EXPLANATION,
                       FIELD_DISPLAY_NM,
                       TOTAL_CNT
                FROM (SELECT a.*, ROWNUM AS rnum
                        FROM (SELECT x.*, COUNT(*) OVER() AS total_cnt
                                FROM NISIS.MASTER_OBJ_HIST_V x
                                $where 
                                ORDER BY $sortColDir) a
                        WHERE ROWNUM <= (:page * :pageSize + :pageSize))
                WHERE rnum >= (:page * :pageSize + 1)";
        $parsed = oci_parse($db, $sql);

        //error_log($sql);

        oci_bind_by_name($parsed, ":page", $page);
        oci_bind_by_name($parsed, ":pageSize", $pageSize);

        if(!oci_execute($parsed)){
            $err = oci_error($parsed);
            $errStr = $err['message'];
            kill(array('result' => 'Malformed query in get_history.php api with actionType: '. $_REQUEST['actionType'], 'error' => $errStr));
        }

        oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

         //error_log(var_export($results, true));
         //error_log("$sql  ---- $sortColDir -----$page ---- $pageSize");

        if(count($results) == 0){
            $total=0;
            $results=array();
        }else{
          $total=$results[0]['TOTAL_CNT'];
        }

        kill(array('result' => 'Success', "history" => $results, "total" => $total), FALSE);

    case 'ExportToCsv':
        $sql = "SELECT FAC_ID,
                       OBJECTID,                       
                       FIELD_DISPLAY_NM,
                       STATUS_DISPLAY,
                       EXPLANATION,
                       USERID,
                       TO_CHAR(STATUS_DATE, 'YYYY-MM-DD HH24:MI:SS') AS STATUS_DATE,
                       RTG_CODE,
                       IM_ROLE                       
                       FROM NISIS.MASTER_OBJ_HIST_V
                       $where 
                       ORDER BY $sortColDir";
        $parsed = oci_parse($db, $sql);

        if(!oci_execute($parsed)){
            $err = oci_error($parsed);
            $errStr = $err['message'];
            kill(array('result' => 'Malformed query in get_history api.php api with actionType: '. $_REQUEST['actionType'], 'error' => $errStr));
        }

        oci_fetch_all($parsed, $allResults, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

        if(count($allResults) == 0){
            $csvData="";
            kill(array('result' => 'Success', "csvData" => $csvData), FALSE);
        }else{
            ob_start();
        
            $output = fopen('php://output','w');
            fputcsv($output, explode(',',"FAC. ID,OBJECT ID,STATUS FIELD,STATUS CHANGE,STATUS EXPLANATION/SUPPLEMENTAL NOTES,USER ID,DATE,ROUTING CODE,IM ROLE"));

            // fputcsv($output, array_keys($allResults[0]));

            foreach($allResults as $values){
                fputcsv($output, $values);
            }        

            fclose($output);
            $csvData = ob_get_clean();
            kill(array('result' => 'Success', "csvData" => $csvData), FALSE);
        }
        break;
    case 'get_unique_field':
        $filteredField = $_REQUEST['filteredField'];
        $sql = "SELECT DISTINCT $filteredField FROM NISIS.MASTER_OBJ_HIST_V $where ORDER BY $filteredField";
        //error_log($sql);
        $parsed = oci_parse($db, $sql);
        if(!oci_execute($parsed)){
            $err = oci_error($parsed);
            $errStr = $err['message'];
            kill(array('result' => 'Malformed query in get_history.php api with actionType: '. $_REQUEST['actionType'], 'error' => $errStr));
        }
        oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
        kill(array('result' => 'Success', $filteredField => $results), FALSE);
        break;
    case 'get_unique_facID':
        if(!isset($_REQUEST['filter'])){
          kill(array('result' => 'Success'), FALSE);
        }else{
          $filter=$_REQUEST['filter'];
          $filteredField = $_REQUEST['filteredField'];
          
          $sql = "SELECT DISTINCT $filteredField FROM NISIS.MASTER_OBJ_HIST_V WHERE LOWER(".$filteredField.") LIKE LOWER('".$filter['filters'][0]['value']."%') ORDER BY $filteredField";
          //error_log($sql);
          $parsed = oci_parse($db, $sql);
          if(!oci_execute($parsed)){
              $err = oci_error($parsed);
              $errStr = $err['message'];
              kill(array('result' => 'Malformed query in get_history.php api with actionType: '. $_REQUEST['actionType'], 'error' => $errStr));
          }
          oci_fetch_all($parsed, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
          kill(array('result' => 'Success', $filteredField => $results), FALSE);
        }
        break;
    default:
        break;
}

function parseDateFilters( $filters , $count = 0) {  
    $where = "";    
    $intcount = 0;
    $noend= false;
    $nobegin = false;

    if ( isset( $filters['filters'] ) ) {
        $itemcount = count( $filters['filters'] );
        if ( $itemcount == 0 ) {
            $noend= true;
            $nobegin = true;
        } elseif ( $itemcount == 1 ) {
            $noend= true;
            $nobegin = true;           
        } elseif ( $itemcount > 1 ) {
            $noend= false;
            $nobegin = false;           
        }
        foreach ( $filters['filters'] as $key => $filter ) {
            if ( isset($filter['field'])) {
                $field = 'LOWER('.$filter['field'].")";
                if($filter['field']=="STATUS_DATE"){
                    switch ( $filter['operator'] ) {
                        case 'eq':
                            $compare = " = ";
                              $field = "TO_CHAR(".$filter['field'].",'YYYYMMDD')";
                              $value = "'".$filter['value']."'";
                            break;
                        case 'neq':
                            $compare = " <> ";
                              $field = "TO_CHAR(".$filter['field'].",'YYYYMMDD')";
                              $value = "'".$filter['value']."'";
                            break;
                        case 'gt':
                            $compare = " > ";
                            $field = "TO_CHAR(".$filter['field'].",'YYYYMMDD')";
                            $value = "'".$filter['value']."'";
                            break;
                        case 'lt':
                            $compare = " < ";
                            $field = "TO_CHAR(".$filter['field'].",'YYYYMMDD')";
                            $value = "'".$filter['value']."'";
                            break;
                        case 'gte':
                            $compare = " >= ";
                            $field = "TO_CHAR(".$filter['field'].",'YYYYMMDD')";
                            $value = "'".$filter['value']."'";
                            break;
                        case 'lte':
                            $compare = " <= ";
                            $field = "TO_CHAR(".$filter['field'].",'YYYYMMDD')";
                            $value = $filter['value'];
                            break;
                    }
                    if ( $count == 0 && $intcount == 0 ) { 
                        $before = ""; 
                        $end = " " . $filters['logic'] . " ";
                    } elseif ( $count > 0 && $intcount == 0 ) { 
                        $before = ""; 
                        $end = " " . $filters['logic'] . " ";
                    } else {
                        $before = " " . $filters['logic'] . " ";
                        $end = ""; 
                    }        
                    $where .= ( $nobegin ? "" : $before ) . $field . $compare . $value . ( $noend ? "" : $end );
                    $count ++;
                    $intcount ++;
                } else {
                  $where .= " ( " . parseDateFilters( $filter , $count ) . " )" ;        
                }      
                $where = str_replace( " or  or " , " or " , $where );
                $where = str_replace( " and  and " , " and " , $where );
            }  
        }
    }
    return $where;
}

function parseColumnFilters( $filters , $count = 0) {  
    $where = "";    
    $intcount = 0;
    $noend= false;
    $nobegin = false;
 
        $itemcount = count( $filters["filters"]);
        if ( $itemcount == 0 ) {
            $noend= true;
            $nobegin = true;
        } elseif ( $itemcount == 1 ) {
            $noend= true;
            $nobegin = true;           
        } elseif ( $itemcount > 1 ) {
            $noend= false;
            $nobegin = false;           
        }
        foreach ( $filters['filters'] as $key => $filter ) {
            if ( isset($filter['field'])) {
                $field = 'LOWER('.$filter['field'].")";

                $compare = " = ";
                $values = array();
                for($x=0;$x<count($filter['value']);$x++){
                  array_push($values,"LOWER('" . $filter['value'][$x] . "')");
                }
                $value = "any(".implode(",",$values).")";


                if ( $count == 0 && $intcount == 0 ) { 
                    $before = ""; 
                    $end = " " . $filters['logic'] . " ";
                } elseif ( $count > 0 && $intcount == 0 ) { 
                    $before = ""; 
                    $end = " " . $filters['logic'] . " ";
                } else {
                    $before = " " . $filters['logic'] . " ";
                    $end = ""; 
                }        
                $where .= ( $nobegin ? "" : $before ) . $field . $compare . $value . ( $noend ? "" : $end );
                $count ++;
                $intcount ++;
            }     
            $where = str_replace( " or  or " , " or " , $where );
            $where = str_replace( " and  and " , " and " , $where );
        }  
    return $where;
}

function oneline( $sql ) {
    $str = $sql;    
    $str = str_replace("\n\r", '<br />', $str );
    $str = str_replace("\r\n", '<br />', $str );
    $str = preg_replace("/[ ]+/", ' ', $str );
    $str = str_replace("\t", '', $str );
    return trim($str);
}

?>