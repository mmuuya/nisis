<?php
//KH - Move feature management item from one folder to another.

// (in_userid             IN NUMBER,
//                             in_itemid             IN NUMBER,
//                             in_frm_parentid       IN NUMBER,
//                             in_to_parentid        IN NUMBER,
//                             in_isshared           IN VARCHAR2,
//                             in_user_or_grp_mode   IN VARCHAR2,
//                             in_groupid            IN NUMBER)

$result = "";

$userid = $_SESSION['userid'];
$usertype = $_POST["usertype"];
$selectedgroup = $_POST["selectedgroup"];
$itemid = $_POST["itemid"];
$frmparentid = $_POST["frmparentid"];
$toparentid = $_POST["toparentid"];
$isshared = $_POST["isshared"];


// error_log("userid:" . $userid);
// error_log("usertype:" . $usertype);
// error_log("selectedgroup:" . $selectedgroup);
// error_log("itemid:" . $itemid);
// error_log("frmparentid:" . $frmparentid);
// error_log("toparentid:" . $toparentid);
// error_log("isshared:" . $isshared);

$query = 'BEGIN :result := NISIS_ADMIN.FM_PKG.move_to_folder (:userid, :itemid, :frmparentid, :toparentid, :isshared, :usertype, :selectedgroup); END;';


$parsed = oci_parse($db, $query);

oci_bind_by_name($parsed, ":userid", $userid);
oci_bind_by_name($parsed, ":usertype", $usertype);
oci_bind_by_name($parsed, ":selectedgroup", $selectedgroup);
oci_bind_by_name($parsed, ":itemid", $itemid);
oci_bind_by_name($parsed, ":frmparentid", $frmparentid);
oci_bind_by_name($parsed, ":toparentid", $toparentid);
oci_bind_by_name($parsed, ":isshared", $isshared);
oci_bind_by_name($parsed, ":result", $result, 2000);

error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in fm_move_item_to_folder api', 'error' => $errStr));
}
else {    
    kill(array('result' => $result), FALSE);    
}

?>

