<?php
$arpt_id = $_POST["id"];  //like DCA
$type = $_POST['assoc_type'];  //HELIPORT or AIRPORT
$table_name = "nisis_arpt_runway_line";

if ($type === "HELIPORT") { 
  $table_name = "nisis_arpt_runway_point"; 
}
else { 
  $table_name = "nisis_arpt_runway_line"; 
};

$query = "SELECT RWY_ID,
      RWY_LENGTH,
      rwy_width,
      be_elevation,
      re_elevation,
      rwy_surface_type,
      rwy_surface_treatment,
      pavement_class,
      single_wheel_weight,
      dual_wheel_weight,
      two_dual_wheel_weight,
      two_dual_tandem_wheel_weight,
      nisis.profile_apt.get_runway_key_value (rwy_leih, 'RWY_LEIH') AS rwy_leih,
      nisis.profile_apt.get_runway_key_value (be_vgsi, 'BE_VGSI') AS be_vgsi,
      nisis.profile_apt.get_runway_key_value (be_alsaf, 'BE_ALSAF') AS be_alsaf,
      nisis.profile_apt.get_runway_key_value (be_reil, 'BE_REIL') AS be_reil,
      nisis.profile_apt.get_runway_key_value (be_cl_lights, 'BE_CL_LIGHTS') AS be_cl_lights,
      nisis.profile_apt.get_runway_key_value (be_touchdown_lights, 'BE_TOUCHDOWN_LIGHTS') AS be_touchdown_lights,
      nisis.profile_apt.get_runway_key_value (re_asi, 'RE_ASI') AS re_asi,
      nisis.profile_apt.get_runway_key_value (re_alsaf, 'RE_ALSAF') AS re_alsaf,
      nisis.profile_apt.get_runway_key_value (re_reil, 'RE_REIL') AS re_reil,
      nisis.profile_apt.get_runway_key_value (re_cl_lights, 'RE_CL_LIGHTS') AS re_cl_lights,
      nisis.profile_apt.get_runway_key_value (re_touchdown_lights, 'RE_TOUCHDOWN_LIGHTS') AS re_touchdown_lights
 FROM $table_name 
WHERE faa_id = :arpt_id";


$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":arpt_id", $arpt_id);

if(!oci_execute($parsed)){
    error_log('Get_runways.php: Got Error Executing query: ' . $query);
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get runways', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

kill(array('result' => 'Success', "data" => $results), FALSE);


?>