<?php
//Get the airport profile

$id = $_POST["id"];  //like DCA for airports or SBY-FCT for ATM 
$action = $_POST["get_what"]; 
$result ="";
$query ="";

error_log($action);
switch ($action) {           
    case 'get_qsr_airports':      
        $query = "SELECT nisis.profile_apt.get_all_statuses(:id) as statuses from dual";
        break;
    case 'get_qsr_atm': 
        $query = "SELECT nisis.profile_atm.get_all_statuses(:id) as statuses FROM DUAL";
        break;
    case 'get_qsr_ans': 
        $query = "SELECT nisis.profile_ans.get_all_statuses(:id) as statuses FROM DUAL";
        break;
    case 'get_qsr_osf': 
        $query = "SELECT nisis.profile_osf.get_all_statuses(:id) as statuses FROM DUAL";
        break;
    case 'get_qsr_tof': 
        $query = "SELECT nisis.profile_tof.get_all_statuses(:id) as statuses FROM DUAL";
        break;
    
    default:  
    	error_log("invalid action in get qsr statuses.php");      
        break;
};

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":id", $id);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get_qsr_statuses api', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

kill(array('result' => 'Success', 'qsr' => $results));
?>

