<?php

require_once ('../handler.php');


if ($_POST["objectid"]) {
    $objectid = $_POST["objectid"];
}
if ($_POST["facTable"]) {
    $facTable = $_POST["facTable"];
}

    $uploadCheck = "select count(*) as total from nisis.snapshot_uploads where ref_objid = :i_obj_id and 
                    ref_table = :i_table_name ";
    $sqlUpdate = "update nisis.snapshot_uploads set 
                    file_nm = :i_file_nm,
                    file_contents = EMPTY_BLOB(), 
                    upload_by = :i_user_id, 
                    upload_dt = systimestamp 
                where ref_objid = :i_obj_id and ref_table = :i_table_name
                returning file_contents into :l_blob";
    $sqlInsert = "insert into nisis.snapshot_uploads s (s.file_nm, s.file_contents, s.ref_table, s.ref_objid, s.comments, s.upload_by, s.upload_dt)
                  values (:i_file_nm, EMPTY_BLOB(), :i_table_name, :i_obj_id, null, :i_user_id, systimestamp)
                  returning s.file_contents, s.fileid into :l_blob,:l_fileid";    
  
$files = $_FILES['files'];
//only one file at a time ??
$filename =  $files['name'];

    // Save the uploaded files  
    for ($index = 0; $index < count($files['name']); $index++) {
        $file = $files['tmp_name'][$index];
        if (is_uploaded_file($file)) {
            move_uploaded_file($file, './' . $files['tmp_name'][$index]);
        }
    }

    $temp = explode(".", $_FILES["files"]["tmp_name"]);

        if ($_FILES["files"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
        } else {
            
            $recordCount = oci_parse($db, $uploadCheck);  
            oci_bind_by_name($recordCount, ":i_obj_id", $objectid);
            oci_bind_by_name($recordCount, ":i_table_name", $facTable);
            oci_execute($recordCount);
            oci_fetch($recordCount);

     
            $count = oci_result($recordCount, "TOTAL");  
            error_log("Upload - count is ...............");     
            error_log($count);  
            if ($count == 3){
                //cleanup
                unlink($_FILES["files"]["tmp_name"]);
                kill(array('result' => 'Error', 'message' =>  "Only up to 3 files can be uploaded"));
            } 
               

            if ($count <3000000){
                $updateParsed = oci_parse($db, $sqlInsert);  
            } else {          
                //$updateParsed = oci_parse($db, $sqlUpdate);
            }
            $dlob = oci_new_descriptor($db, OCI_DTYPE_LOB);    
            oci_bind_by_name($updateParsed, ":i_file_nm", $filename);
            oci_bind_by_name($updateParsed, ":l_blob", $dlob, -1, OCI_B_BLOB);
            oci_bind_by_name($updateParsed, ":i_user_id", $_SESSION["userid"]);
            oci_bind_by_name($updateParsed, ":i_obj_id", $objectid);
            oci_bind_by_name($updateParsed, ":i_table_name", $facTable);           
            oci_bind_by_name($updateParsed, ":l_fileid", $fileId);
              
            $blob = (fread(fopen($_FILES["files"]["tmp_name"], "r"), filesize($_FILES["files"]["tmp_name"])));

            //  error_log($blob);

            oci_execute($updateParsed, OCI_NO_AUTO_COMMIT);
            if ($dlob->save($blob)){
                oci_commit($db);
            }
            else {
               echo "Unable to save file blob";
            }
            //cleanup
            unlink($_FILES["files"]["tmp_name"]);

           
        }
   
    kill(array('result' => 'Success', 'fileId' =>  $fileId));

?>