<?php
require_once ('../handler.php');

// NISIS-HOTFIX, KOFI HONU - Added FILEID so that deletes are done by primary key.  Avoids potential bugs later.
// $i_ref_objid = $_POST["objectid"];
// $i_file_name =$_POST["filename"];
// $i_table_name =$_POST["facTable"];
$i_fileid =$_POST["fileid"];

// $query = "DELETE FROM NISIS.snapshot_uploads where ref_objid = :i_ref_objid and ref_table = :i_table_name and file_nm = :i_file_name";
$query = "DELETE FROM NISIS.snapshot_uploads where fileid = :i_fileid";

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":i_fileid", $i_fileid);
// oci_bind_by_name($parsed, ":i_table_name", $i_table_name);
// oci_bind_by_name($parsed, ":i_file_name", $i_file_name);


if(!oci_execute($parsed)) {
    kill("DATABASE ERROR when trying to delete an image");
}


   
    kill(array('result' => 'Success', 'fileId' =>  $i_fileid));

//kill(array('return' => 'Success', 'results' => TRUE), FALSE);
?>