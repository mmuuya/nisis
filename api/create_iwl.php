<?php
//Create the overall IWL, return the IWL ID

if ($db === FALSE) {
	kill('No Database Connection');
}

$table = 'NISIS.NISIS_IWL';
$name = $_POST['iwl_name'];
$desc = $_POST['iwl_desc'];
$layer = $_POST['iwl_layer'];
$username = $_POST['username'];

switch($layer)
{
	case "airports_pu":
	case "airports_pr":
	case "airports_others":
	case "airports_military":
	case "airports":
		$layer = "airports";
	break;
	case "atm":
		$layer = "atm";
	break;
	case "ans1":
	case "ans2":
	case "ans":
		$layer = "ans";
	break;
	case "osf":
		$layer = "osf";
	break;
	//NISIS-2754 KOFI HONU
	// case "teams":
	// 	$layer = "teams";
	// break;	
	case "tof":
		$layer = "tof";
	break;	
	default: 
		//must be a dot layer, pass it through.
		
}

$iwlQuery = "INSERT INTO NISIS.NISIS_IWL
			(name, layer, description, created_user)
			VALUES
			(:name, :layer, :descr, :usrnm)";

$qIwlQuery = oci_parse($db, $iwlQuery);
oci_bind_by_name($qIwlQuery, ":name", $name);
oci_bind_by_name($qIwlQuery, ":layer", $layer);
oci_bind_by_name($qIwlQuery, ":descr", $desc);
oci_bind_by_name($qIwlQuery, ":usrnm", $username);

if(!oci_execute($qIwlQuery, OCI_DEFAULT)){
	$e = oci_error($qIwlQuery);
	$errStr = $e['message'];
	oci_rollback($db);
	if(substr($errStr, 0, 9) === "ORA-00001"){
		$errStr = "An IWL with this name already exists.";
	}
	error_log($username . ' could not create an iwl: name (' . $name . '), layer (' . $layer . '), description (' . $desc . ').');
	kill(array('error'=> $errStr));
} else {
	oci_commit($db);

	//get IWL ID from IWL we just created - NAME and CREATED_USER make up unique contstraint
	$query = oci_parse($db, 'SELECT ID FROM NISIS.NISIS_IWL WHERE NAME=:name AND CREATED_USER=:username');
	oci_bind_by_name($query, ":name", $name);
	oci_bind_by_name($query, ":username", $username);
	if(!oci_execute($query)){
		$err = oci_error($query);
		error_log("Could not get the IWL ID from recently created IWL");
		kill(array('error' => $err['message']));
	}
	oci_fetch_all($query, $iwlIdResult);

	kill(array('return' => 'Success', 'iwlid' => $iwlIdResult['ID'][0]), FALSE);
}

?>
