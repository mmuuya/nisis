<?php
//
$userid = $_POST["userid"];
$shapeid = $_POST["shapeid"];
$itemtype = $_POST["itemtype"];
$result ="";

$query = "begin :result := NISIS_ADMIN.fm_pkg.delete_shape(:userid, :shapeid, :itemtype); end;";

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":userid", $userid);
oci_bind_by_name($parsed, ":shapeid", $shapeid);
oci_bind_by_name($parsed, ":itemtype", $itemtype);
oci_bind_by_name($parsed, ":result", $result, 2000);


error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in delete_shape api', 'error' => $errStr));
}
else {
            
    	kill(array('result' => $result), FALSE);    
}

?>