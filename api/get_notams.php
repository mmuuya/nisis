<?php
$arpt_id = $_POST["id"];  //like DCA
error_log("Executing get_notams query..........................");

$query = "SELECT notam_id, notam_text||' - '||notam_report as notam_name FROM aidap_notams WHERE cns_location_id = :arpt_id AND notam_cancel_dtg IS NULL AND notam_delete_dtg IS NULL ORDER BY notam_effective_dtg DESC";

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":arpt_id", $arpt_id);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get notams', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

// $resultsPrint = print_r($results, true);
// error_log($resultsPrint);

kill(array('result' => 'Success', "data" => $results), FALSE);


?>