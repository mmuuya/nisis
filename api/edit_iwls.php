<?php

$iwl_id = $_REQUEST['iwl_id'];
switch($_REQUEST['task']){
	case "update":
		$name = $_REQUEST['name'];
		$desc = $_REQUEST['desc'];
		$grps = $_REQUEST['groups'];

		$query = "UPDATE NISIS.NISIS_IWL
					SET NAME=:name,
					DESCRIPTION=:descript,
					LAST_UPDATED_USER=:updtUsr,
					LAST_UPDATED_DATE=SYSDATE
					WHERE ID=:iwl_id";
		$parsedquery = oci_parse($db, $query);
		oci_bind_by_name($parsedquery, ":name", $name);
		oci_bind_by_name($parsedquery, ":descript", $desc);
		// oci_bind_by_name($parsedquery, ":grps", $grps);
		oci_bind_by_name($parsedquery, ":updtUsr", $_SESSION['username']);

		//remove the groups from the iwl, so that only what's left in multiselect is what's assigned
		$parsedClrGrps = oci_parse($db, "DELETE FROM NISIS.NISIS_IWL_GROUP WHERE IWL_ID=:iwl_id");
		oci_bind_by_name($parsedClrGrps, ":iwl_id", $iwl_id);
		oci_execute($parsedClrGrps);

		foreach (explode(",", $grps) as $grpid) {
			$parsedGrpQuery = oci_parse($db , "INSERT INTO NISIS.NISIS_IWL_GROUP (GROUP_ID, IWL_ID) VALUES (:grpid, :iwl_id)");
			oci_bind_by_name($parsedGrpQuery, ":grpid", $grpid);
			oci_bind_by_name($parsedGrpQuery, ":iwl_id", $iwl_id);
			if(!@oci_execute($parsedGrpQuery)) {
		        $err = oci_error($parsedGrpQuery);
		        if($err['code'] !== 1) { //code 1 is 'violated unique constraint'
		            kill("DB ERROR - executeIgnoreUnique: " 
		                . $err['message'] 
		                . " | " 
		                . $err['sqltext']
		                );
		        } else {
		        	error_log("[unique constraint ignored when assigning groups to IWL]");
		        }
		    }
		}

	break;
	case "delete":
		$query = "DELETE FROM NISIS.NISIS_IWL WHERE ID=:iwl_id";
		$parsedquery = oci_parse($db, $query);

		//also need to delete the objects
		$parseddel = oci_parse($db, "DELETE FROM NISIS.NISIS_IWL_OBJECTS WHERE IWL_ID=:iwl_id");
		oci_bind_by_name($parseddel, ":iwl_id", $iwl_id);
		if(oci_execute($parseddel)){
			//If deleting objects succeeds, delete the iwl's groups. If it fails, don't care
			$parsedgrpdel = oci_parse($db, "DELETE FROM NISIS.NISIS_IWL_GROUP WHERE IWL_ID=:iwl_id");
			oci_bind_by_name($parsedgrpdel, ":iwl_id", $iwl_id);
			oci_execute($parsedgrpdel);
		} else {
			//if iwl's objects can't be deleted, halt before the main iwl is deleted
			$e = oci_error($parseddel);
			error_log('Deleting IWL Objects error message: ' . $e['message']);
			kill(array('result' => 'Unable to delete the IWL objects from the IWL. IWL was not deleted.'));
		}
	break;
	case "remove":
		require "clean_iwl_oids_sesh.php";
		
		$oids = $_REQUEST['oids'];

		$query = "DELETE FROM NISIS.NISIS_IWL_OBJECTS WHERE IWL_ID=:iwl_id
					AND OBJECTID IN (" . implode(",", $oids) . ")";
		$parsedquery = oci_parse($db, $query);
	break;
}

oci_bind_by_name($parsedquery, ":iwl_id", $iwl_id);

//If this fails, somebody wrote something that broke the query when it was escaped.
if(!oci_execute($parsedquery)){
	error_log('IWL Editing error: ' . var_export(oci_error($parsedquery), TRUE));
	kill(array('return' => 'Failure', 'message' => $e['message']));
}

kill(array('return' => 'Success', 'results' => TRUE), FALSE);

?>