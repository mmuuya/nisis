<?php
if ($db === FALSE) {
	kill('No Database Connection');
}

require "../admin/api/acl_lib_lite.php";

//Make sure we've got all the data we need
if (!array_key_exists('password', $_REQUEST) || !array_key_exists('username', $_REQUEST)) {
	kill('Login Malformed Request');
}

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
unset($_REQUEST['password']); //Clean up behind ourselves so no one's secret is leaked


// NISIS-3021 KOFI HONU - 
// Check user's username case indifferent.
// Retrieve actual username spelling (upper/lowercase) for use in the rest of the app.
// If we use the one supplied at login, it might not be same case as stored in ArcGIS / browser credentials / etc.
$sql = "SELECT U.USERNAME, U.PASSWORD, U.SALT, U.FNAME, U.LNAME, U.USERID, U.EMAIL, A.ACTIVE 
		FROM APPUSER.USERS U 
        JOIN APPUSER.USERS_APP A ON U.USERID = A.USERID
        WHERE UPPER(U.USERNAME) = UPPER(:uname)
        AND A.ACTIVE = 1
        AND ROWNUM = 1";
$parsedsql = oci_parse($db, $sql);
oci_bind_by_name($parsedsql, ':uname', $username);

//If this fails, somebody wrote something that broke the query when it was escaped.
if(!oci_execute($parsedsql)){
	kill('Malformed Query');
	error_log("Possible SQL Injection Attack!  Submitted Username: '$username'");
}

//set up the audit query
$audit_query = 'INSERT INTO APPUSER.AUDIT_LOGIN (userid, time, success) VALUES (:useid, to_date(:stamp, :qformat), :good)';
$audit_parsed = oci_parse($db, $audit_query);
$cur_time = date($php_dt_format);
oci_bind_by_name($audit_parsed, ':stamp', $cur_time);
oci_bind_by_name($audit_parsed, ':qformat', $ora_dt_format);
//we'll bind :good when we execute

//Let's get our User Data!
$incorrect_str = 'Incorrect username or password.';
$row = oci_fetch_assoc($parsedsql);
if ($row === FALSE) {
	//audit unsuccessful login for unknown user
	$non_user = -1;
	$success = 'N';
	oci_bind_by_name($audit_parsed, ':useid', $non_user);
	oci_bind_by_name($audit_parsed, ':good', $success);
	oci_execute($audit_parsed);

	kill($incorrect_str);
} else {
	$pass = hash('sha512', $row['SALT'] . $password);
	unset($password); //Clean up behind ourselves so no one's secret is leaked

	//bind the username to the query now that we have it
	oci_bind_by_name($audit_parsed, ':useid', $row['USERID']);

	if (strcmp($row['PASSWORD'], $pass) == 0) {
		if($row['ACTIVE']==1){
			//$agsTokenUrl = "https://192.168.19.211:6443/arcgis/tokens/generateToken?username=" . $username . "&password=" . $password . "&client=requestip&expiration=720&f=json";
			//$curlHandle = curl_init($agsTokenUrl);
			//curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, TRUE);
			//curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, FALSE); //don't verify cert (consider CURLOPT_CAINFO)
			//$response = curl_exec($curlHandle);
			//curl_close($curlHandle);
			//$token = json_decode($response, TRUE);

			$urlParsed = oci_parse($db, 'SELECT LINK_NAME, URL FROM NISIS.NISIS_EXTERNAL_LINKS');
			if(!oci_execute($urlParsed)){ kill("DB ERROR getting urls from database"); }
			while(($urlRow = oci_fetch_assoc($urlParsed)) != false) {
				$_SESSION['urls'][$urlRow['LINK_NAME']] = $urlRow['URL'];
			}
			// error_log(var_export($_SESSION['urls'], TRUE));
			// NISIS-3021 KOFI HONU - Retrieve actual username spelling (upper/lowercase) for use in the rest of the app.
			// If we use the one supplied at login, it might not be same case as stored in ArcGIS / browser credentials / etc.
			$_SESSION['username'] = $row['USERNAME'];	
			$_SESSION['userid']	= $row['USERID'];
			$_SESSION['name'] = $row['FNAME'] . ' ' . $row['LNAME'];
			$_SESSION['email'] = $row['EMAIL'];
			$_SESSION['groupids'] = getUserGroupIDs($_SESSION['userid']);
			
			$loggedin = TRUE; //this flag sets off the logic in index.php to load the application
			//audit successful login activity
			$success = 'Y';
			oci_bind_by_name($audit_parsed, ':good', $success);
			oci_execute($audit_parsed);

			exit(json_encode(
				//array('return'=>'Success', 'message'=>'Successully Logged in.', 'userid'=>$row['USERID'], 'username'=>$username, 'token'=>$token, 'newhtml'=>$app)));
				array('return'=>'Success', 'message'=>'Successully Logged in.', 'userid'=>$row['USERID'], 'username'=>$row['USERNAME'])));
		} else {
			exit(json_encode(array('return' => 'Failure',
				 'message' => 'Your Account is currently inactive.  Please Contact your Supervisor.')));
		}
	} else {
		//audit unsuccessful login activity then kill
		$success = 'N';
		oci_bind_by_name($audit_parsed, ':good', $success);
		oci_execute($audit_parsed);
		kill($incorrect_str);
	}
}
?>
