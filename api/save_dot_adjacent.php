<?php
//
$objectid = $_POST["objectid"];
$tablenm = $_POST["tablenm"];
$status = $_POST["status"];
$statuscolumn = $_POST["statuscolumn"];
$changes = $_POST['changes'];
$result ="";

$array = $changes;

$query = "begin :result := geo_query_pkg.set_adjacent_status(:objectid, :status, :tablenm, :statuscolumn, :p1); end;";

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_array_by_name($parsed, ":p1", $array, count($array), -1, SQLT_INT); 
oci_bind_by_name($parsed, ":objectid", $objectid);
oci_bind_by_name($parsed, ":status", $status);
oci_bind_by_name($parsed, ":tablenm", $tablenm);
oci_bind_by_name($parsed, ":statuscolumn", $statuscolumn);

oci_bind_by_name($parsed, ":result", $result, 2000);

error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in save_dot_adjacent api', 'error' => $errStr));
}
else {
            
    	kill(array('result' => $result), FALSE);    
}

?>