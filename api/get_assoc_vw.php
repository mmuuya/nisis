<?php
//Returns results for associated views. For use in the profile.

$type = $_POST['assoc_type'];
$mainId = $_POST['id'];

//$zeroResults is returned to the kendo gui if there are no rows found, so it should use the same column names as the original query
switch($type) {

	case "arpt-ans":
		$columns = "
			BASIC_CAT, 
			ANS_ID, 
			LG_NAME, 
			BASIC_TP,
			" . textSubQuery("ANS_STS", 9, "ANS_STATUS") . "ANS_STS,
			DESCRIPT
			";
		$zeroResults = array(array(
			"BASIC_CAT" => "", //"[No associated ANS]", 
			"ANS_ID" => "", 
			"LG_NAME" => "", 
			"BASIC_TP" => "", 
			"ANS_STS" => "",
			"DESCRIPT" => ""
			));
		$query = "SELECT " . $columns . " FROM NISIS.NISIS_AIRPORT_ANS_VW A WHERE ARPT_ID=:main";
		
		error_log($query);
	break;

	case "arpt-atm":
		$columns = "			
			ATM_ID,
			LG_NAME,
			" . textSubQuery("OPS_STATUS",5) . "OPS_STATUS";
		$zeroResults = array(array(			
			"ATM_ID" => "",
			"LG_NAME" => "",
			"OPS_STATUS" => ""
			));
		$query = "SELECT " . $columns . " FROM NISIS.NISIS_AIRPORT_ATM_VW A WHERE ARPT_ID=:main";
	break;

	case "arpt-rmls-open":
		// $zeroResults = array(array(
		// 	"RMLS" => "[No Open RMLS Tickets for this Airport]"
		// ));	
		$columns = "ANS_ID, LOG_ID, FAC_IDENT, LOG_STATUS, CODE_CATEGORY, 
			TO_CHAR(START_DATETIME, 'DD-MON-YYYY HH24:MI') AS START_DATETIME, 
			TO_CHAR(END_DATETIME, 'DD-MON-YYYY HH24:MI') AS END_DATETIME,
			TO_CHAR(MODIFIED_DATETIME, 'DD-MON-YYYY HH24:MI') AS MODIFIED_DATETIME,
			EQUIPMENT_ID, EVENT_TYPE_CODE, 
			ERROR_CODE, INTERRUPT_CONDITION, LOG_SUMMARY, SUPPLEMENTAL_CODE, NOTAM_NUMBER";
		$query = "SELECT " . $columns . " FROM NISIS.AIRPORT_RMLS_OPEN_VIEW WHERE FAC_IDENT=:main ORDER BY START_DATETIME DESC";
	break;

	case "arpt-rmls-void":
		// $zeroResults = array(array(
		// 	"RMLS" => "[No Closed/Void RMLS Tickets for this Airport]"
		// ));
		$columns = "ANS_ID, LOG_ID, FAC_IDENT, LOG_STATUS, CODE_CATEGORY, 
			TO_CHAR(START_DATETIME, 'DD-MON-YYYY HH24:MI') AS START_DATETIME, 
			TO_CHAR(END_DATETIME, 'DD-MON-YYYY HH24:MI') AS END_DATETIME,
			TO_CHAR(MODIFIED_DATETIME, 'DD-MON-YYYY HH24:MI') AS MODIFIED_DATETIME,
			EQUIPMENT_ID, EVENT_TYPE_CODE, 
			ERROR_CODE, INTERRUPT_CONDITION, LOG_SUMMARY, SUPPLEMENTAL_CODE, NOTAM_NUMBER";

		$query = "SELECT " . $columns . " FROM NISIS.AIRPORT_RMLS_VOID_VIEW WHERE FAC_IDENT=:main ORDER BY START_DATETIME DESC";
	break;

	case "arpt-gbpa-com":
		$zeroResults = array(array(
			"GBPA" => "[None found]"
		));
		
		/*$query = "SELECT ";
		$query .= "RUNWAY, TYPE, CATEGORY, ANS_ID, BASIC_TP, ANS_STATUS ";
		$query .= "FROM NISIS.ARPT_GBPA_HEADER WHERE ARPT_ID=:main ";
		$query .= "ORDER BY RUNWAY ASC";*/

		$query = "SELECT ";
		$query .= "DISTINCT RUNWAY, TYPE, CATEGORY, ";
		$query .= "LISTAGG(CAST(ANS_ID AS VARCHAR2(20)) || ':' || ANS_STATUS, ', ') WITHIN GROUP (ORDER BY ARPT_ID, ANS_ID) ";
		$query .= "OVER (PARTITION BY RUNWAY) AS ANS_STATUS ";
		$query .= "FROM NISIS.ARPT_GBPA_HEADER WHERE ARPT_ID =:main ";
		$query .= "ORDER BY RUNWAY ASC";

	break;

	case "arpt-gbpa":
		$zeroResults = array(array(
			"GBPA" => "[None found]"
		));
		
		/*$query = "SELECT ";
		$query .= "RUNWAY, TYPE, CATEGORY, ANS_ID, BASIC_TP, ANS_STATUS ";
		$query .= "FROM NISIS.ARPT_GBPA WHERE ARPT_ID=:main ";
		$query .= "ORDER BY RUNWAY ASC";*/

		$query = "SELECT ";
		$query .= "DISTINCT RUNWAY, TYPE, CATEGORY, ";
		$query .= "LISTAGG(CAST(ANS_ID AS VARCHAR2(20)) || ':' || ANS_STATUS, ', ') WITHIN GROUP (ORDER BY ARPT_ID, ANS_ID) ";
		$query .= "OVER (PARTITION BY RUNWAY) AS ANS_STATUS ";
		$query .= "FROM NISIS.ARPT_GBPA WHERE ARPT_ID =:main ";
		$query .= "ORDER BY RUNWAY ASC";

	break;

	case "arpt-arriv-rates":
		$zeroResults = array(array(
			"APCFGTIME" => "[None found]"
		));
		$columns = "
			TO_CHAR(APCFGTIME, 'DD-MON-YYYY HH24:MI') AS APCFGTIME,
			APPROACH,
			ARRRWY,
			DEPRWY,
			AAR,
			ADR,
			WEATHER,
			STRATAAR,
			AARADJUST,
			AAR-STRATAAR AS VAR_CURR_BL
			";
		$query = "SELECT " . $columns . " FROM NTML_CURR_ARRIV_RATES WHERE AIRPORT=:main ORDER BY APCFGTIME DESC";
	break;

	case "arpt-eng-rates":
		$zeroResults = array(array(
				"ARRRWY" => "[None found]"
			));
		$columns = "
			ARRRWY,
			DEPRWY,
			VMC,
			LOW_VMC,
			IMC,
			LOW_IMC
			";
		$query = "SELECT " . $columns . " FROM NTML_ENG_ARRIV_RATES WHERE AIRPORT=:main";
	break;

	case "ans-rmls-open":
		$zeroResults = array(array(
			"RMLS" => "[No Open RMLS Tickets for this ANS]"
		));
		$columns = "ANS_ID, LOG_ID, FAC_IDENT, LOG_STATUS, CODE_CATEGORY, 
			TO_CHAR(START_DATETIME, 'DD-MON-YYYY HH24:MI') AS START_DATETIME, 
			TO_CHAR(END_DATETIME, 'DD-MON-YYYY HH24:MI') AS END_DATETIME,
			TO_CHAR(MODIFIED_DATETIME, 'DD-MON-YYYY HH24:MI') AS MODIFIED_DATETIME,
			EQUIPMENT_ID, EVENT_TYPE_CODE, 
			ERROR_CODE, INTERRUPT_CONDITION, LOG_SUMMARY, SUPPLEMENTAL_CODE, NOTAM_NUMBER";
		$query = "SELECT " . $columns . " FROM NISIS.ANS_RMLS_OPEN_VIEW WHERE ANS_ID=:main ORDER BY START_DATETIME DESC";
	break;

	case "ans-rmls-void":
		$zeroResults = array(array(
			"RMLS" => "[No Closed/Void RMLS Tickets for this ANS]"
		));
		$query = "SELECT ANS_ID, LOG_ID, FAC_IDENT, LOG_STATUS, CODE_CATEGORY, 
			TO_CHAR(START_DATETIME,'MM/DD/YYYY HH24:MI') AS START_DATETIME, 
			TO_CHAR(END_DATETIME, 'MM/DD/YYYY HH24:MI') AS END_DATETIME,
			TO_CHAR(MODIFIED_DATETIME, 'MM/DD/YYYY HH:MI AM') AS MODIFIED_DATETIME,
			EQUIPMENT_ID, EVENT_TYPE_CODE, 
			ERROR_CODE, INTERRUPT_CONDITION, LOG_SUMMARY, SUPPLEMENTAL_CODE, NOTAM_NUMBER 
			FROM NISIS.ANS_RMLS_VOID_VIEW 
			WHERE ANS_ID=:main 
			ORDER BY START_DATETIME DESC";
	break;

	case "arpt-runways":
		//base end & reciprocal end
		$columns = '
			RWY_ID,
			RWY_LENGTH,
			RWY_WIDTH,
			BE_ELEVATION,
			RE_ELEVATION,
			RWY_SURFACE_TYPE,
			RWY_SURFACE_TREATMENT
		';
		$zeroResults = array(array(
			"RWY_ID" => "[No runways found]",
			"RWY_LENGTH" => "",
			"RWY_WIDTH" => "",
			"BE_ELEVATION" => "",
			"RE_ELEVATION" => "",
			"RWY_SURFACE_TYPE" => "",
			"RWY_SURFACE_TREATMENT" => ""
			));
		$query = "SELECT " . $columns . " FROM NISIS.NISIS_ARPT_RUNWAY_LINE WHERE FAA_ID=:main";
	break;

	case "atm-arpts":
		//id, arpt name, status, ??
		$columns = "
			A.FAA_ID,
			A.LG_NAME,
			" . textSubQuery("OVR_STATUS", 5) . "OVR_STATUS
		";
		$zeroResults = array(array(
			"FAA_ID" => "[No airports found]",
			"LG_NAME" => "",
			"OVR_STATUS" => ""
			));
		$query = "SELECT " . $columns . " FROM NISIS.NISIS_AIRPORT_ATM_VW X
					JOIN NISIS_GEODATA.NISIS_AIRPORTS A ON X.ARPT_ID=A.FAA_ID
					WHERE X.ATM_ID=:main";
					//AND A.IS_CURR_REC=1";
	break;

	default:
		kill("Type not in a good format, could not get associated systems: $type | $mainId");
}

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":main", $mainId);

if(!oci_execute($parsed)) {
	error_log('Get_Assoc_VW.php: Got Error Executing query: ' . $query);
	kill("DB ERROR - could not get associated systems");
}

if(oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW) === 0) {
	//$results = $zeroResults;
	error_log("No results");
}


// error_log($query);
// error_log($results);
kill(array('result' => 'Success', 'data' => $results, 'count'=> count($results)), FALSE);

//Returns a string which is the subquery to get the text value from PICKLIST_ITEMS
//Make sure the "main" view or table you're selecting from uses alias "A"
//
//$colNm is the column name to use.
//$fieldsCol can be provided if the ACL_FIELDS column name is different
function textSubQuery($colNm,  $tableID, $fieldsCol=NULL){
	if(is_null($fieldsCol)){
		$fieldsCol = $colNm;
	}

	return " (SELECT LABEL FROM NISIS.PICKLIST_ITEMS B WHERE PICKLIST = (SELECT UNIQUE PICKLIST FROM NISIS_ADMIN.ACL_FIELDS WHERE COL_NM='" . $fieldsCol . "'  AND TABLEID=" . $tableID . ") AND A." . $colNm . "=B.VALUE) ";
}

?>