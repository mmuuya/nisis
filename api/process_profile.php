<?php

//Get the history of this entire facility, all fields (except shape), and put the results in $_SESSION['profile']['history'][<fac><oid>]
//<fac> would be replaced by "airports", "ans", "atm", "osf", or "resp"
//<oid> would be the facility's objectid
//
//The zeroth entry - $_SESSION['profile']['history'][<fac><oid>][0] - contains the most-recent record (at the time of the query)
//
//No action is taken if the $_SESSION variable for this facility is already set, so a call should be made to clean_profile_sesh.php when any profile window is closed.
//
//Setting the history in the session like this prevents the history from being queried every time the user clicks on a history icon. It also allows for multiple profiles to be open at a time. When any profile is closed, though, any subsequent history icon clicks shoot off another query regardless.
//
//Note, though, that if the user never closes any profile windows, new history might not be reflected in the history log.

//$facType and $facId MUST BE SET by the code which runs this file
if(!isset($facType) || !isset($facId)) {
    kill("FATAL ERROR: variable facType or facId is not set [process_fac_history.php]");
}

if(isset($_SESSION['profile']['history'][$facType.$facId])) {
    // error_log("INFO, skipping query: history already setup");
    return;
}

//We want every field in the profile's history in the session, so we can quickly access it when a user is clicking around in a profile window. We have to explicitly specify every column because we need to exclude SHAPE. We exclude SHAPE because PHP breaks when fetching its value, presumably because it's a non-standard data type that it is unable to convert to a string.
//TODO make this a view, ie a "history" view, that way we can query all columns
    $cols = "ILLEGALCOLNM";
    $table = "ILLEGALTBLNM";
    $last_edit_dt_format = "TO_CHAR(LAST_EDITED_DATE, 'YYYY-MM-DD HH:MI:SS AM') AS LAST_EDITED_DATE";
    switch($facType) {
        case "airports":
            $cols = "ADDRESS, " . ddstr("AFA_STATUS") . ", AFA_STS_EX, AFD_INFO, ANS_ID, " . ddstr("ANS_STATUS") . ", ANS_STS_AD, " . ddstr("ANS_STS_AL") . ", ANS_STS_EX, ANS_STS_SN, " . ddstr("AOO_STATUS") . ", AOO_STS_AD, AOO_STS_AL, AOO_STS_EX, AOO_STS_SN, APPR_CTRL, ARFF_SVC, ARP_CLASS, ARP_NOTES, ARP_TYPE, ASSOC_CITY, ASSOC_ST, ATO_SA, ATO_TD, ATTENDANCE, AVG_OPS_N, AV_OP_PLAN, " . ddstr("A_CTRL_STS") . ", BASIC_USE, BL_AAR, BOUNDARY, C30_STATUS, CARGO_SVC, CBP_STATUS, CBP_STS_AD, CBP_STS_AL, CBP_STS_EX, CBP_STS_SN, CDC_Q_LEAD, CDC_Q_ST, COUNTY, CREATED_DATE, CREATED_USER, CRT_AAR, CRT_AAR_EX, CRT_AAR_SN, CRT_M_TAFS, CRT_NOTAMS, " . ddstr("CRT_RL") . ", DIAGRAM, ELEVATION, ENRTE_ATC, " . ddstr("ER_ATC_STS") . ", EST_CO_CRT, FAA_ID, FAA_REG, FBO, FEMA_REG, FLIGHT_SVC, " . ddstr("FS_INTER") . ", FS_MBER, FT_AVAIL, GGLE_MAP, HUB_TYPE, H_AREA, IATA_ID, ICAO_ID, INT_SVC, " . ddstr("IWL_DSG") . ", IWL_MBER, $last_edit_dt_format, LAST_EDITED_USER, LAST_UPDT, LATITUDE, LG_NAME, LONGITUDE, MANAGER, NCI_OB_CAT, NCI_OB_REL, NFDC_LINK, OBJECTID, OEP35_STS, " . ddstr("OOO_STATUS") . ", OOO_STS_AD, " . ddstr("OOO_STS_AL") . ", OOO_STS_EX, OOO_STS_SN, OPS45_STS, OP_CTR_INF, OP_EG_INF," . ddstr("OVR_STATUS") . ", OVR_STS_AD, OVR_STS_AL, OVR_STS_EX, OVR_STS_SN, OWNER, PROJ_OPS, REL_STATUS, RES_DSG, RES_DSG_EX, RSP_CBPPOC, RSP_TSAFSD, RUNWAYS, SECT_CHART, SUP_AG_COM, SUP_ANS_O, SUP_ASR, SUP_NAVAID, SUP_PROCS, SYMBOLOGY, TFM_CMTS, TIME_ZONE, TOWER, TOWER_ID, " . ddstr("TOWER_STS") . ", TOWER_TP, " . ddstr("TSA_STATUS") . ", TSA_STS_AD, TSA_STS_AL, TSA_STS_EX, TSA_STS_SN, T_OPS_PDAY, T_SCH_OPS, VAR_AO_TO, VAR_AVG_PD, VAR_BC_AAR";
            $table = "NISIS_GEODATA.NISIS_AIRPORTS";
        break;
        case "ans":
            $cols = "ANS_ID, ANS_NOTES, " . ddstr("ANS_STATUS") . ", ANS_STS_AD, ANS_STS_AL, ANS_STS_EX, ANS_STS_SN, ASSOC_A_RW, ASSOC_CITY, ASSOC_CNTY, ASSOC_ST, ATO_SA, BASIC_CAT, BASIC_TP, " . ddstr("BPWR_STS") . ", BPWR_S_AL, BPWR_S_EX, BPWR_S_SN, BPWR_TYPE, BPWR_USTS, CREATED_DATE, CREATED_USER, DESCRIPT, EG_UPS_SUP, FAA_REG, FEMA_REG, FSEP_ID, " . ddstr("FS_INTER") . ", FS_MBER, IWL_DSGN, IWL_MBER, $last_edit_dt_format, LAST_EDITED_USER, LAST_UPDT, LATITUDE, LONGITUDE, MTNCE_RESP, NAPRS_REPORTABLE, NCI_OB_CAT, NCI_OB_REL, OBJECTID, OWNERSHIP, PWR_CD, REL_ANSS, RESP_SU, RESTO_REQ, RMLS_REP, RTOF_FAC, SUP_ARPS, SUP_ATMS, SUP_INSTP, SUP_RWYS, SYMBOLOGY, TIME_ZONE, T_OPS_DIST, USE_STATUS";
            $table = "NISIS_GEODATA.NISIS_ANS";
        break;
        case "atm":
            $cols = "ADDRESS, ALT_PH, ANS_ID, ANS_ID_STS, APPR_CTRL, ARP_ID, ASSOC_CITY, ASSOC_CNTY, ASSOC_ST, ASS_P_BEX, ASS_P_TTL, ATC_LEVEL, ATM_ID, ATM_NOTES, ATO_OSL, ATO_PAS_EX, ATO_PAT, " . ddstr("ATO_PA_STS") . ", ATO_PA_TTL, " . ddstr("ATO_PSI") . ", ATO_PSI_EX, ATO_PSI_SN, ATO_SA, ATO_TD, BASIC_TP, CATC_VD, CREATED_DATE, CREATED_USER, " . ddstr("CRT_RL") . ", CRT_RL_AL, CRT_RL_EX, CRT_RL_SN, " . ddstr("CRT_SL") . ", CRT_SL_AL, CRT_SL_EX, CRT_SL_SN, EG_SUP, ENRTE_ATC, FAA_REG, FEMA_REG, FLIGHT_SVC, " . ddstr("FS_INTER") . ", FS_MBER, " . ddstr("IWL_DSG") . ", IWL_MBER, $last_edit_dt_format, LAST_EDITED_USER, LAST_UPDT, LATITUDE, LG_NAME, LONGITUDE, MANAGER, NCI_CI_REL, NCI_OB_CAT, OBJECTID, OPS_HRS, " . ddstr("OPS_SL") . ", OPS_SL_EX, " . ddstr("OPS_STATUS") . ", OPS_STS_AD, OPS_STS_AL, OPS_STS_EX, OPS_STS_SN, PHOTO, PRIM_PH, " . ddstr("PWR_STS") . ", PWR_STS_EX, PWR_STS_SN, RESP_SU, SAT_PH,STOF_FAC, SYMBOLOGY, TD_MGR, TIME_ZONE, TOD_MGR, TOWER_TYPE, TO_DIST";
            $table = "NISIS_GEODATA.NISIS_ATM";
        break;
        case "osf":
            $cols = "ADDRESS, " . ddstr("AOPA_4S") . ", AOPA_4S_EX, AOPA_4S_SN, AOPS_IMP, AOPS_IP_EX, AOPS_IP_SN, " . ddstr("AOS_LVL") . ", AOS_L_EX, AOS_L_SN, ASSOC_CITY, ASSOC_CNTY, ASSOC_ST, ATO_REG, BASIC_TP, BOPS_AD, BOPS_EX, BOPS_SN, " . ddstr("BOPS_STS") . ", BPWR_EX, BPWR_SN, " . ddstr("BPWR_STS") . ", BPWR_TP, BPWR_USTS, CREATED_DATE, CREATED_USER, CRT_RL_AL, CRT_RL_EX, CRT_RL_SN, " . ddstr("CRT_RL") . ", CRT_SL_AL, CRT_SL_EX, CRT_SL_SN, " . ddstr("CRT_S_LVL") . ", EG_UPS_SUP, ELEVATION, FAA_REG, FAC_MGR, FEMA_REG, " . ddstr("FS_INTER") . ", FS_MBER, HRS_OPS, " . ddstr("IWL_DSG") . ", IWL_MBER, $last_edit_dt_format, LAST_EDITED_USER, LAST_UPDT, LATITUDE, LG_NAME, LONGITUDE, MAIN_TEL, NCI_OB_CAT, NCI_OB_REL, OBJECTID, OSF_ID, OSF_NOTES, PA_ALL, PA_ATO, PA_NOTES, PHOTO, PPL_UORG, SOPS_AL, SYMBOLOGY, TERM_DIST, TIME_ZONE, TOPS_DIST";
            $table = "NISIS_GEODATA.NISIS_OSF";
        break;
        default:
            kill("FATAL ERROR - $facType table not recognized");
        break;
    }
    

    $query = "SELECT * FROM 
                (   
                    SELECT $cols FROM $table A
                    WHERE OBJECTID=:facid
                    --AND GDB_TO_DATE < SYSDATE
                    --ORDER BY GDB_TO_DATE DESC
                )
            WHERE ROWNUM <= 1";   
//error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":facid", $facId);

if ( !oci_execute($parsed) ) {
    error_log('Possible SQL Injection Attack!');
    kill(array('return' => 'failure', 'result'=>'Malformed Query', 'query' => $query));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW+OCI_ASSOC);
$_SESSION['profile']['history'][$facType.$facId] = $results;

//DEBUG for current values
// error_log("CURRENT: " . var_export($results[0], TRUE));

//ddstr stands for drop down string. Prepends the column name with some extra subquery stuff so you can get the text value of a column instead of the number. Should only be used on columns which are dropdowns, which have an entry in ACL_FIELDS with non-null value in the PICKLIST column
function ddstr($column_name){
    return "(SELECT LABEL FROM NISIS.PICKLIST_ITEMS B WHERE PICKLIST = (SELECT unique PICKLIST FROM NISIS_ADMIN.ACL_FIELDS WHERE COL_NM = '" . $column_name . "') AND A." . $column_name . " = B.VALUE) " . $column_name;
}

function getProfileField($column_name){
    global $db,$facId,$TABLE;

    $query = "SELECT EXPLANATION, USERID, TO_CHAR( STATUS_DATE, 'YYYY/MM/DD HH24:MI:SS' ) AS STATUS_DATE
              FROM NISIS.MASTER_OBJ_STATUS_V
              WHERE OBJECTID=:objectid and source_table =:source_table  and col_nm=:col_nm";
            
    $parsed = oci_parse($db, $query);
    oci_bind_by_name($parsed, ":objectid", $facId);
    oci_bind_by_name($parsed, ":source_table", $TABLE);
    oci_bind_by_name($parsed, ":col_nm", $column_name);

    if(!oci_execute($parsed)){
        $err = oci_error($parsed);
        $errStr = $err['message'];
        kill(array('result' => 'Malformed query in get_app_access api', 'error' => $errStr));
    }

    $nrows=oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

    //check if 0 rows returned
    if($nrows==0) {
        return "";
    }

    $row=$results[0];
    $explanation=$row['EXPLANATION'];
    $username=$row['USERID'];
    $dtg=$row['STATUS_DATE'];

    //Adding Z at the end because this is getting the UTC (Zulu) time from the database already
    $statusExplanation =  $explanation.'<br /><div class="signature"> Entered by <b>'. $username . '</b> on <i>' . $dtg . 'Z</i></div>';        
    return $statusExplanation;
}


?>