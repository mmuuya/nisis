<?php
//Retrieve all of the current NOTAMs for a facility's profile using the FAA ID. Returns the HTML string on the 'notams' key in the returned JSON.

$faaId = $_POST["faaId"];

$query = 'SELECT NOTAM_TEXT FROM AIDAP_NOTAMS
            WHERE CNS_LOCATION_ID=:facility
            AND NOTAM_CANCEL_DTG IS NULL
            AND NOTAM_DELETE_DTG IS NULL
            ORDER BY NOTAM_EFFECTIVE_DTG desc';

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":facility", $faaId);

if(!oci_execute($parsed)) {
    kill("DB ERROR getting NOTAMs for the facility");
}

$ntmHtml = "";
$ntmCtr = 0;
$sep = "<br />";
while(oci_fetch($parsed)) {
    $ntmHtml .= oci_result($parsed, "NOTAM_TEXT") . $sep;
    $ntmCtr ++;
}

if ($ntmHtml !== "") {
    //got results, but don't need the final separator
    $ntmHtml = substr($ntmHtml, 0, -(strlen($sep)));
}

//add timestamp (zulu time) to the beginning
//"As of 2014/27/01 – 1420Z"
$ntmHtml = "<i>" . $ntmCtr . " NOTAMs as of " . gmdate("Y-m-d - Hi") . "Z</i><br />" . $ntmHtml;

kill(array('result' => 'Success', 'notams' => $ntmHtml), FALSE);

?>