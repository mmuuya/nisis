<?php

// NISIS-2660 - K.Honu - 
// Moved this code to trigger level to reduce network roundtrips on application and simplify inserts.

//Inserting a notification in the db

// $result = "Success";

// $notId = "";
// $fac_type = $_POST["fac_type"];
// $fac_id = $_POST["fac_id"];
// $column_nm = $_POST["column_nm"];
// $old_value = $_POST["old_value"];
// $new_value = $_POST["new_value"];
// $userId = $_SESSION['userid'];
// // $group = $_SESSION['group'];

// $query = 'BEGIN :notId := INSERT_NOTIFICATION (
//                     :fac_type,
//                     :fac_id,
//                     :column_nm,
//                     :old_value,
//                     :new_value,
//                     :userId
//                     );
//                     END;';

// $parsed = oci_parse($db, $query);

// oci_bind_by_name($parsed, ":notId", $notId, 2000);
// oci_bind_by_name($parsed, ":fac_type", $fac_type);
// oci_bind_by_name($parsed, ":fac_id", $fac_id);
// oci_bind_by_name($parsed, ":column_nm", $column_nm);
// oci_bind_by_name($parsed, ":old_value", $old_value);
// oci_bind_by_name($parsed, ":new_value", $new_value);
// oci_bind_by_name($parsed, ':userId', $userId);

// if(!oci_execute($parsed)) {
// 	$result = "Error";
//     kill("DATABASE ERROR while Inserting a notification for FAC_ID: ".$fac_id." and COLUMN_NM: ".$column_nm);
// }

// //Inserting record in NOTIFICATION_GROUP table
// $id = $notId;
// $groups = $_SESSION['groupids'];
// $j=0;
// if (count($groups) > 0){
//     foreach ($groups as $gr){
// 		$queryGroups = 'INSERT INTO NOTIFICATION_GROUP (NOTID, GROUPID) VALUES(:id, :groupId)';
		                
// 		$parsedGroups = oci_parse($db, $queryGroups);

// 		oci_bind_by_name($parsedGroups, ":id", $id);
// 		oci_bind_by_name($parsedGroups, ":groupId", $gr);

// 		if(!oci_execute($parsedGroups)) {
// 			$result = "Error";
// 		    kill("DATABASE ERROR Inserting NOTIFICATION_GROUP record in insert_notification.php file with the record number: ". $j . ".");
// 		}
// 		$j++;
// 	}
// }

// kill(array(
//     "result" => $result
//     ), FALSE);

?>