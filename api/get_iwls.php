<?php
//Get each IWL associated with the user's groups or created by the user.
//If user is admin, every IWL is returned. No objectid's are returned.
//
//Note that you can set a filter on the facility type. IWLs will be matched on the LAYER column using the filter string. If no filter variable is passed, no matching on the facility type is done. If the filter variable is NONE, no query is performed, and an empty result set will be returned.

require "../admin/api/acl_lib_lite.php";

require "clean_iwl_oids_sesh.php";

$usergroups = $_SESSION['groupids'];
$username = $_SESSION['username'];
if(isset($_POST['filter'])) {
	$facFilterStr = $_POST['filter'];

	if ($facFilterStr === "NONE") {
		kill(array('return' => 'Success', 'results' => array()), FALSE);
	}
	
	$doFilterFac = true;	
} else {
	$doFilterFac = false;
}

//base query
$query = "SELECT DISTINCT A.ID, A.NAME, A.LAYER, A.DESCRIPTION, A.CREATED_USER, A.CREATED_DATE, A.LAST_UPDATED_USER, A.LAST_UPDATED_DATE FROM NISIS.NISIS_IWL A LEFT JOIN NISIS.NISIS_IWL_GROUP B ON A.ID=B.IWL_ID";
$filterStr = "";

//if not an admin - filter to show only IWL's created by user or shared with user's groups
if(!in_array('0', $usergroups)) {
	$filterStr .= " (A.CREATED_USER = '" . $username . "'";
	for ($i=0; $i<count($usergroups); $i++) {
		$filterStr .= " OR B.GROUP_ID=" . $usergroups[$i];
	}
	$filterStr .= ")";
}

//add filter for facility if it was specified
if($doFilterFac) {
	if($filterStr !== ""){
		//we need this if non-admin filter added to query
		$filterStr .= " AND";
	}
	$filterStr .= " A.LAYER=:facFilter";
}

//add in the full filter string if it's there
if ($filterStr !== "") {
	$query = $query . " WHERE " . $filterStr;
}

$parsedquery = oci_parse($db, $query);

if($doFilterFac) {
	oci_bind_by_name($parsedquery, ":facFilter", $facFilterStr);
}

//If this fails, somebody wrote something that broke the query when it was escaped.
if(!oci_execute($parsedquery)) {
	kill(array('result'=>'Malformed Query'));
}

oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
kill(array('return' => 'Success', 'results' => $results), FALSE);

?>