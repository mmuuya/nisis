<?php
//Get the airport profile

$id = $_POST["id"];  //like DCA for airports or SBY-FCT for ATM 
$userid = $_SESSION['userid'];

$action = $_POST["get_what"]; 
$result ="";
$query ="";

//error_log($action);
switch ($action) {           
    case 'get_profiles_airports': 
        $query = "SELECT nisis.profile_apt.get_profile(:id, :userid) as profile FROM DUAL";
        break;
    case 'get_profiles_atm': 
        $query = "SELECT nisis.profile_atm.get_profile(:id, :userid) as profile FROM DUAL";
        break;
    case 'get_profiles_ans': 
        $query = "SELECT nisis.profile_ans.get_profile(:id, :userid) as profile FROM DUAL";
        break;
    case 'get_profiles_osf': 
        $query = "SELECT nisis.profile_osf.get_profile(:id, :userid) as profile FROM DUAL";
        break;
    case 'get_profiles_tof': 
        $query = "SELECT nisis.profile_tof.get_profile(:id, :userid) as profile FROM DUAL";
        break;
    
    default:  
    	error_log("invalid action in get profiles.php");      
        break;
};

//error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":id", $id);
oci_bind_by_name($parsed, ":userid", $userid);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get_profiles api', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

kill(array('result' => 'Success', 'profile' => $results));
?>