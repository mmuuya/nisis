<?php
//Getting all the TFR groups which the user has access
$queryGrps = 'SELECT U.GROUPID FROM TFRUSER.USER_GROUP U JOIN TFRUSER.GROUPS G ON U.GROUPID = G.GROUPID WHERE USERID=:usrid';

$parsedGrps = oci_parse($db, $queryGrps);
oci_bind_by_name($parsedGrps, ":usrid", $_SESSION['userid']);

if(!oci_execute($parsedGrps)){
    $errGrps = oci_error($parsedGrps);
    $errStrGrps = $errGrps['message'];
    kill(array('result' => 'Malformed query in get_tfr_groups_access api', 'error' => $errStrGrps));
}

oci_fetch_all($parsedGrps, $resultsGrps);
$tfrGroups = $resultsGrps['GROUPID'];

if(count($tfrGroups) === 0){
    $tfrGroups = array();
}
//Setting the tfrGroups array as session variable in case that somebody needs to use it.
$_SESSION['tfrgroups'] = $tfrGroups;

kill(array('result' => 'Success', "tfrgroups" => $tfrGroups), FALSE);

?>