<?php
//Add multiple objects to an existing IWL

//instead of looping over each insert, this can be optimized by using pl/sql collections

require "clean_iwl_oids_sesh.php";

$iwlId = $_POST['iwl_id'];
$layer = $_POST['iwl_layer'];
foreach($_POST['objects'] as $objid){
    $query = "INSERT INTO NISIS.NISIS_IWL_OBJECTS (objectid, layer, iwl_id) VALUES (:objid, :layer, :iwl)";
    $parsed = oci_parse($db, $query);
    oci_bind_by_name($parsed, ":objid", $objid);
    oci_bind_by_name($parsed, ":layer", $layer);
    oci_bind_by_name($parsed, ":iwl", $iwlId);
    if(!oci_execute($parsed)){ //need to finish this up
        $err = oci_error($parsed);
        $errStr = $err['message'];
        error_log("DATABASE ERROR inserting IWL objects! PHP execution about to halt. Note that not all objects may have been added to this IWL (id " . $iwlId . ")");
        kill(array('error' => $errStr));
    }
}

kill(array("done", "result" => "Success, items added to the IWL"), FALSE);

?>