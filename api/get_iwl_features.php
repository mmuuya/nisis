<?php
//avoid error reporting
// error_reporting(0);

$iwl_id = $_REQUEST['iwl_id'];
// $name = $_REQUEST['NAME'];
$layer = $_REQUEST['layer'];
//$username = $_REQUEST['username'];

$query = "";
$qclause = "JOIN NISIS.NISIS_IWL_OBJECTS O ";
$qclause .= "ON F.OBJECTID = O.OBJECTID ";
$qclause .= "JOIN NISIS.NISIS_IWL I ";
$qclause .= "ON O.IWL_ID = I.ID ";
$qclause .= "WHERE I.ID = " . $iwl_id;

switch ($layer) {
	case 'airports':
		$query .= "SELECT O.OBJECTID, F.FAA_ID AS FAC_ID, F.ARP_TYPE AS TYPE, F.LG_NAME AS NAME, F.ASSOC_CITY AS CITY, F.COUNTY AS COUNTY, F.ASSOC_ST AS STATE ";
		$query .= "FROM NISIS_GEODATA.NISIS_AIRPORTS F ";
		$query .= $qclause;
		$query .= " ORDER BY F.FAA_ID ASC";
		break;

	case 'atm':
		$query .= "SELECT O.OBJECTID, F.ATM_ID AS FAC_ID, F.BASIC_TP AS TYPE, F.LG_NAME AS NAME, F.ASSOC_CITY AS CITY, F.ASSOC_CNTY AS COUNTY, F.ASSOC_ST AS STATE ";
		$query .= "FROM NISIS_GEODATA.NISIS_ATM F ";
		$query .= $qclause;
		$query .= " ORDER BY F.ATM_ID ASC";
		break;

	case 'ans':
		$query .= "SELECT O.OBJECTID, F.ANS_ID AS FAC_ID, F.BASIC_TP AS TYPE, F.DESCRIPT AS NAME, F.ASSOC_CITY AS CITY, F.ASSOC_CNTY AS COUNTY, F.ASSOC_ST AS STATE ";
		$query .= "FROM NISIS_GEODATA.NISIS_ANS F ";
		$query .= $qclause;
		$query .= " ORDER BY F.ANS_ID ASC";
		break;

	case 'osf':
		$query .= "SELECT O.OBJECTID, F.OSF_ID AS FAC_ID, F.BASIC_TP AS TYPE, F.LG_NAME AS NAME, F.ASSOC_CITY AS CITY, F.ASSOC_CNTY AS COUNTY, F.ASSOC_ST AS STATE ";
		$query .= "FROM NISIS_GEODATA.NISIS_OSF F ";
		$query .= $qclause;
		$query .= " ORDER BY F.OSF_ID ASC";
		break;
//NISIS 2754 - KOFI HONU - add nisis_tof
	case 'tof':
		$query .= "SELECT O.OBJECTID, F.TOF_ID AS FAC_ID, F.BASIC_TYPE AS TYPE, F.LONG_NAME AS NAME, F.ASSOC_CITY AS CITY, F.ASSOC_COUNTY AS COUNTY, F.ASSOC_STATE AS STATE ";
		$query .= "FROM NISIS_GEODATA.NISIS_TOF F ";
		$query .= $qclause;
		$query .= " ORDER BY F.TOF_ID ASC";
		break;

// NISIS-2742 - KOFI HONU
	// case 'teams':
	// 	$query .= "SELECT O.OBJECTID, F.RES_ID AS FAC_ID, F.RES_TYPE AS TYPE, F.LG_NAME AS NAME, F.LLOC_ADDR AS CITY, '' AS COUNTY, '' AS STATE ";
	// 	$query .= "FROM NISIS_GEODATA.NISIS_RESP F ";
	// 	$query .= $qclause;
	// 	$query .= " ORDER BY F.RES_ID ASC";
	// 	break;

	case 'amtrak':
		$query .= "SELECT O.OBJECTID, F.FRAARCID AS FAC_ID, F.COUNTY AS COUNTY, F.STATEAB AS STATE ";
		$query .= "FROM NISIS_GEODATA.NTAD_AMTRAK F ";
		$query .= $qclause;
		$query .= " ORDER BY F.FRAARCID ASC";
		break;

	default:
		kill(array('return' => 'Failure', 'message' => 'Unknown Layer Name in get_iwl_features: ' . $layer ,'error' => $query));
		break;
}

$parsedquery = oci_parse($db, $query);

//If this fails, somebody wrote something that broke the query when it was escaped.
if(!oci_execute($parsedquery)){
	kill(array('return' => 'Failure', 'message' => 'Malformed Query', 'error' => $query));
	//echo 'Error retreiving IWL features';
	error_log('Possible SQL Injection Attack!');
}

oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

exit(json_encode(array('return' => 'Success', 'results' => $results)));

?>
