<?php
//KH Get the users or groups that an item has been shared with. Returns a simple array.

$itemid = $_POST['itemid'];
$userorgroup = $_POST['userorgroup'];


if ($userorgroup === 'group'){
	$parsed = oci_parse($db, 'SELECT TARGET_GROUPID AS SHAREE FROM NISIS_ADMIN.SHARED_W_GROUP WHERE ITEMID=:itemid');
} else if ($userorgroup === 'user'){
	$parsed = oci_parse($db, 'SELECT TARGET_USERID AS SHAREE FROM NISIS_ADMIN.SHARED_W_USER WHERE ITEMID=:itemid');
}	

oci_bind_by_name($parsed, ":itemid", $itemid);


if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query', 'error' => $errStr));
}

oci_fetch_all($parsed, $results);
$ids = $results['SHAREE'];

// if(count($ids) === 0){
//     $ids = array('0');
// }

kill(array('result' => 'Success', "sharees" => $ids), FALSE);

?>