<?php
//avoid error reporting
error_reporting(0);
//error_reporting(E_ALL);

$task = $_REQUEST['task'];

$query = "SELECT F.COL_NM, P.VALUE, P.LABEL 
		FROM NISIS_ADMIN.ACL_FIELDS F 
		JOIN NISIS.PICKLIST_ITEMS P 
		ON F.PICKLIST = P.PICKLIST 
		WHERE F.PICKLIST IS NOT NULL 
		ORDER BY F.COL_NM, P.VALUE";

$parsedquery = oci_parse($db, $query);

//If this fails, somebody wrote something that broke the query when it was escaped.
if(!oci_execute($parsedquery)){
	$e = oci_error($parsedquery);
	error_log('App could not retreive settings: ' . $e['message']);
	kill(array('return' => 'failure', 'message' => $e['message']));
}

oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
exit(json_encode(array('return' => 'Success', 'results' => $results)));

?>