<?php
//avoid error reporting
error_reporting(0);
//error_reporting(E_ALL);

//Make Sure the DB is wired up correctly
if ($db === FALSE) {
	kill('No Database Connection');
}

//$tables = array('SDE.MAP_NFDC_AIRPORTS');
if(array_key_exists('tables', $_REQUEST)){
	$tables = $_REQUEST['tables'];
}
if(is_array($tables)){
	$tables = implode(', ', $tables);
}

//$values = array('FACNAME','OBJECTID');
if(array_key_exists('values', $_REQUEST)){
	$values = $_REQUEST['values'];
}
if(is_array($values)){
	$values = implode(', ', $values);
}

$constraints = '';
if(array_key_exists('constraints', $_REQUEST)){
	$constraints = $_REQUEST['constraints'];
	if(is_string($constraints)){
		$constraints = "WHERE $constraints";
	}
}
if(is_array($constraints)){
	$constraints = 'WHERE ' . implode(' AND ', $constraints);
}

$query = "SELECT $values FROM $tables $constraints";
$parsedquery = oci_parse($db, $query);

//If this fails, somebody wrote something that broke the query when it was escaped.
if(!oci_execute($parsedquery)){
	kill(array('result'=>'Malformed Query'));
	error_log('Possible SQL Injection Attack!');
}

oci_fetch_all($parsedquery, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
exit(json_encode(array('return' => 'Success', 'results' => $results)));

?>
