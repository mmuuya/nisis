<?php
//KH - create folders or update folder and shape names in feature management

// 
// FUNCTION create_folder (in_userid IN NUMBER,
//   						in_ownertype    IN VARCHAR2,
//                         in_ownerid      IN NUMBER,
//                         in_foldername   IN VARCHAR2,
//                         in_parentid     IN NUMBER := 0)
//    RETURN NUMBER

 // FUNCTION edit_item_name (in_userid             IN NUMBER,
 //                            in_itemid             IN NUMBER,
 //                            in_itemname           IN VARCHAR2,
 //                            in_itemtype           IN VARCHAR2,
 //                            in_user_or_grp_mode   IN VARCHAR2)
 //      RETURN NUMBER
	$result = "";

	$actiontype = $_POST["actiontype"];
						
    if ($actiontype == 'create_folder'){
    	$userid = $_SESSION['userid'];
		$ownertype = $_POST["ownertype"];
		$ownerid = $_POST['ownerid'];
		$foldername = $_POST["foldername"];
		$parentid = $_POST["parentid"];
		$query = 'BEGIN :result := NISIS_ADMIN.fm_pkg.create_folder (:userid, :ownertype, :ownerid, :foldername, :parentid); END;';
		$parsed = oci_parse($db, $query);

		oci_bind_by_name($parsed, ":userid", $userid);
		oci_bind_by_name($parsed, ":ownertype", $ownertype);
		oci_bind_by_name($parsed, ":ownerid", $ownerid);
		oci_bind_by_name($parsed, ":foldername", $foldername);
		oci_bind_by_name($parsed, ":parentid", $parentid);
		oci_bind_by_name($parsed, ":result", $result, 2000);
	}
	else if ($actiontype == 'edit_item_name'){
		$userid = $_SESSION['userid'];
		$itemid = $_POST['itemid'];
		$itemname = $_POST["itemname"];
		$itemtype = $_POST["itemtype"];
		$userorgroupmode = $_POST["userorgroupmode"];

		$query = 'BEGIN :result := NISIS_ADMIN.fm_pkg.edit_item_name (:userid, :itemid, :itemname, :itemtype, :userorgroupmode); END;';
		$parsed = oci_parse($db, $query);

		oci_bind_by_name($parsed, ":userid", $userid);
		oci_bind_by_name($parsed, ":itemid", $itemid);
		oci_bind_by_name($parsed, ":itemname", $itemname);
		oci_bind_by_name($parsed, ":itemtype", $itemtype);
		oci_bind_by_name($parsed, ":userorgroupmode", $userorgroupmode);
		oci_bind_by_name($parsed, ":result", $result, 2000);
	}
	error_log("executing...");

	if(!oci_execute($parsed)){
	    $err = oci_error($parsed);
	    $errStr = $err['message'];
	    kill(array('result' => 'Malformed query in fm_create_update_item api', 'error' => $errStr));
	}
	else {    
	    kill(array('result' => $result), FALSE);    
	}

?>

