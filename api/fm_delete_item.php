<?php
//KH - Delete shape or folder in feature management.

// delete_folder()
// in_userid             IN NUMBER,
// in_itemid             IN NUMBER,
// in_parentid           IN NUMBER,
// in_user_or_grp_mode   IN VARCHAR2,
// in_groupid            IN NUMBER
//
// delete_shape()
//(in_userid IN NUMBER, in_shapeid IN NUMBER)


$result = "";

$itemtype = $_POST["itemtype"];
$userid = $_SESSION['userid'];
$userorgroupmode = $_POST["userorgroupmode"];
$groupid = $_POST["groupid"];

// if ($userorgroupmode == "group"){
// 	$userid = $groupid;
// }


if ($itemtype == 'folder'){
	$itemid = $_POST["itemid"];
	$parentid = $_POST["parentid"];
	$query = 'BEGIN :result := NISIS_ADMIN.fm_pkg.delete_folder (:userid, :itemid, :parentid, :userorgroupmode, :groupid); END;';

	$parsed = oci_parse($db, $query);

	oci_bind_by_name($parsed, ":userid", $userid);
	oci_bind_by_name($parsed, ":itemid", $itemid);
	oci_bind_by_name($parsed, ":parentid", $parentid);
	oci_bind_by_name($parsed, ":userorgroupmode", $userorgroupmode);
	oci_bind_by_name($parsed, ":groupid", $groupid);
	oci_bind_by_name($parsed, ":result", $result, 2000);

}else {
	$shapeid = $_POST['shapeid'];
	$query = 'BEGIN :result := NISIS_ADMIN.fm_pkg.delete_shape (:userid, :shapeid, :itemtype); END;';

	$parsed = oci_parse($db, $query);

	oci_bind_by_name($parsed, ":userid", $userid);
	oci_bind_by_name($parsed, ":shapeid", $shapeid);
	oci_bind_by_name($parsed, ":itemtype", $itemtype);
	oci_bind_by_name($parsed, ":result", $result, 2000);

}
error_log("executing....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in fm_delete_item api', 'error' => $errStr));
}
else {    
    kill(array('result' => $result), FALSE);    
}

?>

