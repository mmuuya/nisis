<?php
$facType = $_POST['assoc_type'];
$oid = $_POST['id'];

    $query = "SELECT DISTINCT I.NAME AS NAME FROM NISIS.NISIS_IWL I
                JOIN NISIS.NISIS_IWL_OBJECTS O ON I.ID=O.IWL_ID
                LEFT JOIN NISIS.NISIS_IWL_GROUP G ON I.ID=G.IWL_ID
                WHERE O.OBJECTID=:objid
                AND I.LAYER=:layer";
                
    if(!userIsAdmin()){
        $query .= ' AND G.GROUP_ID IN (SELECT X.GROUPID FROM NISIS_ADMIN.USER_GROUP X WHERE USERID=:userid)';
    }

    $parsed = oci_parse($db, $query);

    if(!userIsAdmin()){
        oci_bind_by_name($parsed, ":userid", $_SESSION['userid']);
    }
    oci_bind_by_name($parsed, ":objid", $oid);
    oci_bind_by_name($parsed, ":layer", $facType);

error_log('Executing IWL Names query');
error_log($query);
error_log($facType);
error_log($oid);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get iwl names', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

$resultsPrint = print_r($results, true);
error_log($resultsPrint);

kill(array('result' => 'Success', "data" => $results), FALSE);


?>