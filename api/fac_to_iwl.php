<?php
//Handle IWL functions when user is in a facility profile. List IWL's associated with the profile, add facilities to IWL's from the profile, etc

$function = $_POST["function"];

switch ($function){    
    case "get_avail_iwls":
        $facType = $_POST["facType"];
        $objectid = $_POST["objid"];
        getAvailIWLs($facType, $objectid);
    break;
    case "assign_iwls":
        $iwlsToAdd = $_POST["iwls_to_add"];
        $facType = $_POST["fac_type"];
        $facId = $_POST["fac_id"];
        assignFacToIWLs($iwlsToAdd, $facId, $facType);
    break;
    default:
        kill("Bad call to fac_to_iwl.php");
    break;
}


//Get the available IWLs for the multiselect. Returns any IWLs that exist
//for any of the user's groups, and not already part of the facility.
function getAvailIWLs($facType, $facId){
    //TODO - the available iwl's may need to be only if you created, or if you're admin. This is because the rules for "allowed to edit" an IWL would be limited to creator or administrators

    $query = 'SELECT DISTINCT A.ID, A.NAME FROM NISIS.NISIS_IWL A
                LEFT JOIN NISIS.NISIS_IWL_GROUP G ON A.ID=G.IWL_ID
                WHERE LAYER=:ftype
                AND A.ID NOT IN
                (SELECT IWL_ID FROM NISIS.NISIS_IWL_OBJECTS WHERE OBJECTID=:facid)';

    if(!userIsAdmin()){
        $query .= ' AND G.GROUP_ID IN (SELECT GROUPID FROM NISIS_ADMIN.USER_GROUP WHERE USERID=:userid)';
    }

    $parsed = parse($query);

    if(!userIsAdmin()){
        oci_bind_by_name($parsed, ":userid", $_SESSION['userid']);
    }
    oci_bind_by_name($parsed, ":ftype", $facType);
    oci_bind_by_name($parsed, ":facid", $facId);

    oci_execute($parsed);
    fetch_all_rows($parsed, $iwlResults);

    if(count($iwlResults) === 0) {
        $iwlResults = array(array('ID' => '-1', 'NAME' => 'No IWLs Available'));
    }

    kill(array('result' => 'Success', 'iwls' => $iwlResults), FALSE);
}

//Assign the facility to the existing IWLs
function assignFacToIWLs($iwlsToAdd, $facID, $facType){
    require "clean_iwl_oids_sesh.php";
    
    foreach ($iwlsToAdd as $iwlID) {
        if($iwlID === "-1"){
            kill("ERROR - an IWL was attempted to be saved with an id of -1 (some IWL's may have been assigned before this IWL was reached)");
        }

        //assign to nisis_iwl_objects table
        $query = "INSERT INTO NISIS.NISIS_IWL_OBJECTS 
                    (objectid, iwl_id, layer)
                    VALUES (:objid, :iwlid, :factype)";
        $parsed = parse($query);
        oci_bind_by_name($parsed, ":objid", $facID);
        oci_bind_by_name($parsed, ":iwlid", $iwlID);
        oci_bind_by_name($parsed, ":factype", $facType);
        if(!oci_execute($parsed)) {
            kill("DB ERROR adding facility to nisis_iwlobjects");
        }

        //update last_updated_user and last_updated_date
        $query = "UPDATE NISIS.NISIS_IWL
                    SET last_updated_user=:updtuser,
                    last_updated_date=SYSDATE
                    WHERE id=:iwlid";
        $parsed = parse($query);
        oci_bind_by_name($parsed, ":updtuser", $_SESSION['username']);
        oci_bind_by_name($parsed, ":iwlid", $iwlID);
        if(!oci_execute($parsed)) {
            kill("DB ERROR updating the 'last update' fields after assigning the facility to the IWL", FALSE);
        }
    }
    kill(array("result" => "Success", "message" => "Successfully added to IWL"), FALSE);    
}

// a wrapper to oci_parse to avoid 'global $db' elsewhere
function parse($queryStr){ global $db; return oci_parse($db, $queryStr); }

//wrap oci_fetch_all with flags to fetch by row and get an associative array
function fetch_all_rows($statementID, &$output) { oci_fetch_all($statementID, $output, 0, -1, OCI_FETCHSTATEMENT_BY_ROW+OCI_ASSOC); }

?>