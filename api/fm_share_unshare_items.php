<?php
//KH - Share and unshare items with groups or users

// share_items (
// in_userid            IN NUMBER,
// in_user_or_grp_mode   IN VARCHAR2,
// in_itemid_list       IN VARCHAR2,
// in_sharee_userid_list     IN VARCHAR2,
// in_sharee_usertype   IN VARCHAR2)
	$result = "";

	$userid = $_SESSION['userid'];
	$userorgroupmode = $_POST["userorgroupmode"];
	$itemidlist = $_POST["itemidlist"];
	$shareeuseridlist = $_POST["shareeuseridlist"];
	$shareeusertype = $_POST["shareeusertype"];

	$query = 'BEGIN :result := NISIS_ADMIN.fm_pkg.share_items (:userid, :userorgroupmode, :itemidlist, :shareeuseridlist, :shareeusertype); END;';

	$parsed = oci_parse($db, $query);

	oci_bind_by_name($parsed, ":userid", $userid);
	oci_bind_by_name($parsed, ":userorgroupmode", $userorgroupmode);
	oci_bind_by_name($parsed, ":itemidlist", $itemidlist);
	oci_bind_by_name($parsed, ":shareeuseridlist", $shareeuseridlist);
	oci_bind_by_name($parsed, ":shareeusertype", $shareeusertype);
	oci_bind_by_name($parsed, ":result", $result, 2000);
	// error_log("executing....");

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in fm_share_items api', 'error' => $errStr));
}
else {    
    kill(array('result' => $result), FALSE);    
}

?>

