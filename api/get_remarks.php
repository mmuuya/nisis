<?php
error_log("Executing get_remarks query..........................");
$arpt_id = $_POST["id"];  //like DCA

$query = "SELECT rec_id as remark_id, rmk as remark_name FROM nfdc_arpt_rmks WHERE arpt_id = :arpt_id";

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":arpt_id", $arpt_id);

if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get remarks', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

// $resultsPrint = print_r($results, true);
// error_log($resultsPrint);

 kill(array('result' => 'Success', "data" => $results), FALSE);
?>