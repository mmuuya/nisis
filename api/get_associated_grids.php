<?php
$id = $_POST["id"];  //like 'HEF-FAAT'
$type = $_POST['assoc_type'];

switch($type) {

    case "atm-supported-airports":
        $query = "SELECT x.arpt_id, x.arpt_lg_name as lg_name, x.arpt_ovr_status as status, x.arpt_ovr_status_label as status_label FROM nisis.nisis_airport_atm_vw x WHERE x.atm_id = :id";
    break;
    
    case "ans-supported-airports":
        $query = "SELECT x.arpt_id, x.arpt_lg_name as lg_name, x.arpt_ovr_status as status, x.arpt_ovr_status_label as status_label FROM nisis.nisis_airport_ans_vw x WHERE x.ans_id = :id";
    break;
    
    case "atm-supported-ans-infrastructure":
        $query = "SELECT ans_id,
        descript,
        y.ans_sts as status,   
        get_status_label ('NISIS_ANS', 'ANS_STATUS', Y.ANS_STS) AS status_label
        FROM nisis.nisis_airport_ans_vw y
        WHERE y.arpt_id IN (SELECT x.arpt_id
                       FROM nisis.nisis_airport_atm_vw x
                       WHERE x.atm_id = :id)";      
    break;

    case "atm-supported-tof-facilities":
        $query = "SELECT a.tof_id,
        a.long_name,
        a.ops_status,
        get_status_label ('NISIS_TOF', 'OPS_STATUS', a.ops_status) AS ops_status_label
        FROM nisis_geodata.nisis_tof a, nisis_geodata.nisis_atm b
        WHERE a.assoc_atm_facilities = b.atm_id AND atm_id = :id";
    break;

    case "ans-supported-atm-facilities":
        $query = "SELECT d.atm_id,
        d.lg_name,
        d.ops_status,
        nisis.get_status_label ('NISIS_ATM', 'OPS_STATUS', d.ops_status) AS ops_status_label        
        FROM nisis_geodata.nisis_arpt_ans a,
        nisis_geodata.nisis_ans b,
        nisis_geodata.nisis_arpt_atm c,
        nisis_geodata.nisis_atm d
        WHERE a.ans_id = b.ans_id AND a.arpt_id = c.arpt_id AND c.atm_id = d.atm_id AND a.ans_id = :id";    
    break;

    case "tof-associated-atm-facilities":
        $query = " SELECT b.atm_id,
        b.lg_name,
        b.ops_status,
        get_status_label ('NISIS_ATM', 'OPS_STATUS', b.ops_status) AS ops_status_label
        FROM nisis_geodata.nisis_tof a, nisis_geodata.nisis_atm b
        WHERE a.assoc_atm_facilities = b.atm_id AND a.tof_id = :id";
    break;


    default:
        kill("ATM Grids: Could not get systems associated with ATMs: $type | $id");
}

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":id", $id);

if(!oci_execute($parsed)){
    error_log('Get_associated_grids.php: Got Error Executing query: ' . $query);
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in get atm associated grids', 'error' => $errStr));
}

oci_fetch_all($parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
error_log("results are");


kill(array('result' => 'Success', "data" => $results), FALSE);


?>