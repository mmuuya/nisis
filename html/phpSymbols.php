<?php
require '../settings/settings.php';
require_once '../cache.class.php';

$d = $_SERVER['HTTP_HOST'];
//increase the max execution time, this helps while on bad connection
if ($domain == "localhost") {
	ini_set("max_execution_time", 300);
}

if(isset($db)){
	if(!$db){
		$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
	}
} else {
	$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
}

if (!$db) {
    $e = oci_error();
    report(($e['message']), "error");
}

$results = '';

//see if we have the results of this query cached. The cache is stored in nisis/cache. 
$c = new Cache(array(
  'name'      => 'nisis',
  'path'      => '../cache/',
  'extension' => '.cache'
));
$cachedResults = $c->retrieve('PHP_SYMBOLS');

if (!isset($cachedResults)) {
	$sql = 'SELECT value,label,path FROM DYNAMIC_SYMBOLS ORDER BY value';

	$parsed_sql = oci_parse($db, $sql);
	if(!$parsed_sql){
		error_log("DB Parse Error. App Could not parse SQL statement: (" . $sql . ")");
		echo "<h1>ERROR - Could not parse SQL statement. </h1>";
		exit;
	}

	oci_execute($parsed_sql);

	oci_fetch_all($parsed_sql, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
	
	if (isset($results)) {
		error_log("Storing results PHP_SYMBOLS query in a local cache file");		
		$c->store('PHP_SYMBOLS', $results);
	}
}
else {
	error_log("Already have cached resuts for PHP_SYMBOLS query");
	$results = $cachedResults;	
}

foreach($results as $entry) {
	foreach ($entry as $col_val) {
		echo $col_val;
		echo '*';
	}
}


?>