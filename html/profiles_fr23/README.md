# FR23 Prototype
prototype for airport profiles

## first, install
- if you don't have nodejs, install nodejs
- if you don't have npm, install npm
- if you have npm, do: `npm i`

## then, run it
- do: `gulp serve` from the root folder
- updating and save any files inside the root folder, watch the page reload
- for safety's sake reboot the server when adding new files or changing names
