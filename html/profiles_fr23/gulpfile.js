var gulp = require('gulp');
var server = require('gulp-server-livereload');

gulp.task('serve', function() {
  gulp.src('./')
    .pipe(server({
      livereload:{
          enable: true,
          filter: function(filePath, cb){
              cb(!(/.git/.test(filePath)));
          }
      },
      open: true,
      port: 8
    }));
});
