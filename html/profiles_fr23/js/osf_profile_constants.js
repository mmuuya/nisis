var osfProfileConstants = {
datatables : {
  
   
},

profileMenu: [
    {"label": "Basic Information", "target": "#form-basic_info", "isDatatable": false},
    {"label": "Status Update", "target": "#form-status_update", "isDatatable": false},
    {"label": "Contact Info", "target": "#form-contact_info", "isDatatable": false},
    {"label": "Staffing", "target": "#form-staffing", "isDatatable": false},
    {"label": "Incident Management Information", "target": "#form-incident_info", "isDatatable": false}
],

// QSR
statuses: [
    {"label": "OSF Status", "dbcol": "bops_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 773},
    {"label": "OSF Power Status", "dbcol": "osf_pwr_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 955},
    {"label": "Ops Staffing Status", "dbcol": "ops_sl_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1145},
    {"label": "ATO Personnel Accounting Status", "dbcol": "ato_pa_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1380},
    {"label": "Current Readiness Level ", "dbcol": "crt_rl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575},
    {"label": "Current SECON Level ", "dbcol": "crt_s_lvl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575}
]

}

