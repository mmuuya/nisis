
// TODO - all string literal args && key vals ==> vars
// TODO - add init for opening layout

function closeModule(){
    $module.hide();
}

// TODO: rename to match QSR function name
function toggleModuleCollapse() {
    $module.toggleClass('moduleCollapsed');
    if($module.is('centered'))
         $module.removeClass('centered');
    // toggleQSR()
}

function toggleModuleWidth(isDataTable){
    isDataTable ? $module.addClass('datatable') : $module.removeClass('datatable');
}

function toggleMenu(e){
    var btns = Object.keys(toggleList),
        btn_class = e.currentTarget.className.match(/btn-[a-z]*/)[0],
        //  DRYer than ...
        $btn_container = function(_className){
            return $("i." + _className).parents('li');
        };

        $btn_container(btn_class).toggleClass(toggleActiveClass);
        toggleList[btn_class].toggleClass('menu-on');

    btns.forEach(function(el){
        if(el !== btn_class){
            $btn_container(el).removeClass(toggleActiveClass);
            toggleList[el].removeClass('menu-on');
        }
    });
    adjustForm();
}


adjustForm = function(){
    var openMenusCount = $('.nav-container').find('.menu-on').length;
    openMenusCount <= 0 ?
    ($forms.removeClass('with-menu'), $datatables.removeClass('with-menu')) :
    ($forms.addClass('with-menu'), $datatables.addClass('with-menu') );

};

function handleSelect(e){
    console.log('triggered select')

    var vals = {},
        opts = $(e.currentTarget),
        nextExplanationField = $(opts[0]).parents(".content").parent("div").next("div").find("textarea");

    console.log("next field:", nextExplanationField);

    $.each(opts.find('option'), function(i, v){
        if($(v).prop('selected')){
            new_status_class = v.className;
        }
    });

    var current_status_class = opts[0].className.match(/status-[a-z]*/)[0];
    new_className = opts[0].className.replace(current_status_class, new_status_class);
    opts[0].className = new_className;
    // TODO: note element ID, current value

    vals = {
        textarea: nextExplanationField,
        value: nextExplanationField.val(),
        top: nextExplanationField.offset().top,
        left: nextExplanationField.offset().left
    };
    makeModal(vals);
}

//clear input
function clearInput(){
    return;
}

//edit input
function edit(event) {
    var valState = function(_, val){
        return !val;
    };
    $('.paper-input').prop("disabled", valState);
}

// function init(){}
// init();
