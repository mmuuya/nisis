var ansProfileConstants = {
datatables : {
    "supported-airports-runways-procedures": {
        "name": "Supported Airports/Runways/Procedures",
        "type": "datatable",
        "action": "json", //no real action to php, the data is json provided in the Profile
        //this.original_data_["supp_apt_rwy_proc"]["formDataTable"]["0"]["supp_apt_rwy_proc_list"]  
        "assoc_type": "supp_apt_rwy_proc.formDataTable.0.supp_apt_rwy_proc_list",
         "columns": [
            {
                template: "#= arpt_id #",
                field: "arpt_id",
                title: "Airport ID"
            },
            {
                template: "#= runway #",
                field: "runway",
                title: "Runway"
            },
            {
                template: "#= rwy_type #",
                field: "rwy_type",
                title: "Runway Type"
            },            
            {
                template: "#= rwy_proc #",
                field: "rwy_proc",
                title: "Runway Procedures"
            }
        ],
        "fields": [{                    
                    arpt_id: {type : "string"},
                    runway: {type : "string"},
                    rwy_type: {type : "string"},
                    rwy_proc: {type : "string"}
                }
        ]
    },
    "supported-atms": {
        "name": "Supported ATMs",
        "type": "datatable",
        "action": "get_associated_grids",
        "assoc_type": "ans-supported-atm-facilities",
         "columns": [
            {
                template: "#= ATM_ID #",
                field: "ATM_ID",
                title: "ATM ID"
            },
            {
                template: "#= LG_NAME #",
                field: "LG_NAME",
                title: "Long Name"
            },
            {
                template: "#= OPS_STATUS_LABEL #",
                field: "OPS_STATUS_LABEL",
                title: "Status"
            }
        ],
        "fields": [{                    
                    ATM_ID: {type : "string"},
                    LG_NAME: {type : "string"},
                    OPS_STATUS_LABEL: {type : "string"}
                }
        ]
    },
    "supported-airports": {
        "name": "Supported Airports",
        "type": "datatable",
        "action": "get_associated_grids",
        "assoc_type": "ans-supported-airports",
            "columns": [
            {
                template: "#= ARPT_ID #",
                field: "ARPT_ID",
                title: "Airport ID"
            },
            {
                template: "#= LG_NAME #",
                field: "LG_NAME",
                title: "Airport Long Name"
            },
            {
                template: "#= STATUS #",
                field: "STATUS",
                title: "Status",
                hidden: true
            },
            {
                template: "#= STATUS_LABEL #",
                field: "STATUS_LABEL",
                title: "Status",
                hidden: false
            }
        ],
        "fields": [{
                    ARPT_ID: {type : "string"},
                    LG_NAME: {type : "string"},
                    STATUS: {type : "string"},
                    STATUS_LABEL: {type : "string"}
                }
        ]

    }
   
},

profileMenu: [
    {"label": "Basic Information", "target": "#form-basic_info", "isDatatable": false},
    {"label": "Status Update", "target": "#form-status_update", "isDatatable": false},   
    {"label": "Supported Airports/Runways/Procedures", "target": "#datatable-supported-airports-runways-procedures", "isDatatable": true},
    {"label": "Supported ATM Facilities", "target": "#datatable-supported-atms", "isDatatable": true},
    {"label": "Supported Airports", "target": "#datatable-supported-airports", "isDatatable": true},
    {"label": "Incident Management Information", "target": "#form-incident_info", "isDatatable": false}
],

// QSR
statuses: [
    {"label": "ANS System Status", "dbcol": "ans_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 773},
    {"label": "Backup Power Use Status", "dbcol": "bpwr_usts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 955},
    {"label": "Backup Power System Status", "dbcol": "bpwr_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1145},
   
   
]

}

