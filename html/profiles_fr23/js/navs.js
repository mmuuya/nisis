// TODO - all string literal args && key vals ==> vars

// TODO: reveal after fixing showProfile error
// window.onhashchange = function(){
//     var targetPart = location.hash.replace("#",""),
//         fullTarget = "#form-" + targetPart;
//     $('.grid').removeClass('showProfile');
//     $('.grid' + fullTarget).addClass('showProfile');
// };


// profiles menu clicks
// TODO - follow other patterns
$('menu.profiles').find('menuitem a').on('click',function(e){
    var isDataTable =  JSON.parse($(e.currentTarget)[0].attributes[1].value),
        $target = $(e.currentTarget.hash);

    e.preventDefault();
    toggleModuleWidth(isDataTable);
    $forms.find('.showProfile').removeClass('showProfile');
    $datatables.find('.showProfile').removeClass('showProfile');
    $target.addClass('showProfile');

    //scroll to the top of the section
    $target.animate({ scrollTop: (0) }, 'fast');
});

// TODO - add hilight behavior to qsr arrow icon
// FIXME - replace removeHilite's cleartimeout w/ animation end listener
function jump2status(e){
    adjustForm();
    var $form = $('#form-airport_snapshot'),
        moduleOffset = $module.offset().top,
        $targetField = $("#" + e.currentTarget.children[2].textContent.replace(/ /gi,"_")),
        targetOffset = Number(e.currentTarget.getAttribute('data-target-offset')),
        offsetPadding = 85, //header plus 1.5em padding
        finalOffset = targetOffset - offsetPadding;

    $form.addClass('showProfile');
    $form.animate({scrollTop: finalOffset});
    $targetField.addClass('hiliteField');

    // TODO trigger class removal at animation end
    var removeHilite = setTimeout(function(){
        $targetField.removeClass('hiliteField');
    },6000);
}
