var forms = {
    // TODO: list final vs demo-only data and seperate
    "basic_info": {
        "name": "Basic Information",
        "type": "form",
        "formElements": [
            {"name":"Basic Information", "type": "h3", "columnClasses": "col-1-1"},
            {"name":"Airport Long Name", "type":"input[type='text']", "columnClasses": "col-1-1", "value": "WASHINGTON DULLES INTL"},
            {"name":"Street Address", "type": "input[type='text']", "columnClasses": "col-1-1", "value": "1 Saarinen Cir"},
            {"name":"City", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "Dulles"},
            {"name":"County", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "Loudoun/Fairfax"},
            {"name":"State", "type": "select", "columnClasses": "col-1-3", "value": "VA", "selectOptions": [{"option":"AL"},{"option":"AK"},{"option":"AZ"},{"option":"AR"},{"option":"CA"},{"option":"CO"},{"option":"CT"},{"option":"DE"},{"option":"DC"},{"option":"FL"},{"option":"GA"},{"option":"HI"},{"option":"ID"},{"option":"IL"},{"option":"IN"},{"option":"IA"},{"option":"KS"},{"option":"KY"},{"option":"LA"},{"option":"ME"},{"option":"MD"},{"option":"MA"},{"option":"MI"},{"option":"MN"},{"option":"MS"},{"option":"MO"},{"option":"MT"},{"option":"NE"},{"option":"NV"},{"option":"NH"},{"option":"NJ"},{"option":"NM"},{"option":"NY"},{"option":"NC"},{"option":"ND"},{"option":"OH"},{"option":"OK"},{"option":"OR"},{"option":"PA"},{"option":"RI"},{"option":"SC"},{"option":"SD"},{"option":"TN"},{"option":"TX"},{"option":"UT"},{"option":"VT"},{"option":"VA"},{"option":"WA"},{"option":"WV"},{"option":"WI"},{"option":"WY"},]},
            {"name":"Reference Point Coordinates", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "38.94744 / -77.45994"},
            {"name":"Latitude", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "38.9531"},
            {"name":"Longitude", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "77.4565"},
            {"name":"Elevation", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "313"},
            {"name":"Airport Boundary (GIS)", "type": "input[type='text']", "columnClasses": "col-1-2", "value": ""}, //ask evans
            {"name":"Airport ID (FAA)", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "IAD"},
            {"name":"Airport ID (IATA)", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "IAD"},
            {"name":"Airport ID (ICAO)", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "KIAD"},
            {"name":"Timezone", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "America/New_York (GMT -4:00) a"},
            {"name":"ATO Terminal District", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "a"},
            {"name":"ATO Service Area", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "a"},
            {"name":"FAA Region", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "AEA a"},
            {"name":"FEMA Region", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "a"},
            {"name":"Attendance", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "N/A"},
            {"name":"Airport Classification", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "PU"},
            {"name":"Basic Use", "type": "select", "columnClasses": "col-1-2", "value": "RL-Alpha", "selectOptions": [{"option": "Public"}, {"option": "Private"}, {"option": "Military"}]}, //select - public, private, military
            {"name":"Hub Type", "type": "select", "columnClasses": "col-1-2", "value": "RL-Alpha", "selectOptions": [{"option": "RL-Delta"}, {"option": "RL-Charlie"}, {"option": "RL-Bravo"}, {"option": "RL-Alpha"}, {"option": "Unknown"}, {"option": "N/A"}]}, //select
            {"name":"Cargo Service Type", "type": "select", "columnClasses": "col-1-2", "value": "RL-Alpha", "selectOptions": [{"option": "RL-Delta"}, {"option": "RL-Charlie"}, {"option": "RL-Bravo"}, {"option": "RL-Alpha"}, {"option": "Unknown"}, {"option": "N/A"}]},
            {"name":"Reliever", "type": "select", "columnClasses": "col-1-2", "value": "RL-Alpha", "selectOptions": [{"option": "Yes"}, {"option": "No"}]},
            {"name":"GA Airport", "type": "select", "columnClasses": "col-1-2", "value": "RL-Alpha", "selectOptions": [{"option": "RL-Delta"}, {"option": "RL-Charlie"}, {"option": "RL-Bravo"}, {"option": "RL-Alpha"}, {"option": "Unknown"}, {"option": "N/A"}]},
            {"name":"Airport Manager", "type": "textarea", "columnClasses": "col-1-2", "value": "CHRISTOPHER BROWNE, 1 SAARINEN CIRCLE, SAARINEN CENTER MA-210, DULLES, VA 20166, 703-572-2730", "disabled": ""},
            {"name":"Airport Owner", "type": "textarea", "columnClasses": "col-1-2", "value": "METRO WASH ARPT AUTHORITY, 1 AVIATION CIRCLE, WASHINGTON, DC 20001-6000, 703-417-8600", "disabled":""},
            {"name":"POE Status", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "n/a"},
            {"name":"FBO", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "n/a"},
            // {"name":"Airport Diagram", "type": "input[type='text']", "columnClasses": "col-1-1", "value": "n/a"}, //ask evans, link?
            // {"name":"Overlying Sectional", "type": "input[type='text']", "columnClasses": "col-1-1", "value": "n/a"}, //ask evans
            // {"name":"Airport A/FD Information", "type": "input[type='text']", "columnClasses": "col-1-1", "value": "n/a"}, //ask evans, link?
            // {"name":"Map Using", "type": "input[type='text']", "columnClasses": "col-1-3", "value": ""},
            // {"name":"Link to NFDC", "type": "input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Airport Tower Present", "type": "input[type='radio']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Airport Tower Type", "type": "select", "columnClasses": "col-1-3", "selectOptions": [{"option": "Operational", "color": "green"}, {"option": "Degraded", "color": "yellow"}, {"option": "Unavailable", "color": "red"}], "value": "Operational"},
            {"name":"ARFF Services", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "I E S 05/1973"},
            {"name":"Fuel Types", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "100LLA"},
            {"name":"Profile Last Updated", "type": "input[type='text']", "columnClasses": "col-1-1", "disabled": "readonly", "value": "10/01/16 by John Evans"},
        ]
    },
    "status_update": {
        "name": "Status Update",
        "type": "form",
        "formElements": [
            {"name": "Status Update", "type": "h3", "columnClasses": "col-1-1"},
            // needs radio buttons
            {"name":"ARFF Services", "type": "input[type='text']", "columnClasses": "col-1-1", "value": "Operational"},
            {"name":"ARFF Services Status", "type": "select", "selectOptions": [{"option": "Operational", "color": "green"}, {"option": "Degraded", "color": "yellow"}, {"option": "Unavailable", "color": "red"}], "columnClasses": "col-1-1", "value": "Operational"},
            {"name":"ARFF Services Status Explanation", "type": "textarea", "disabled": "", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Fuel Types", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Fuel Availability Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Fuel Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Fuel Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"ANS Infrastructure Overall Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"ANS Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport Overall Status",  "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Airport Overall Status Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport Power Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "", "selectOptions": [{"option":"Normal Main Power"},{"option":"Main Power Degraded"},{"option":"Main Power Disrupted"},{"option":"On Egs", "color":"yellow"},{"option":"No Power", "color":"red"}]},
            {"name":"Airport Power Status Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport Power Status Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Aviation Operator Ops Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Normal Main Power"},{"option":"Main Power Degraded"},{"option":"Main Power Disrupted/On EGs"},{"option":"No Power"}]},
            {"name":"Aviation Operator Ops Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Aviation Operator Ops Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport TSA Ops Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Airport TSA Ops Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport TSA Ops Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport CBP Ops Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Airport CBP Ops Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport CBP Ops Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Last Update to Airport Profile", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-1","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
        ]
    },
    "contact_info": {
        "name": "Contact Info",
        "type": "form",
        "formElements": [
            {"name": "Contact Info", "type": "h3", "columnClasses": "col-1-1", "value": ""},
            {"name":"Airport Manager", "type": "textarea", "columnClasses": "col-1-2", "value": "CHRISTOPHER BROWNE, 1 SAARINEN CIRCLE, SAARINEN CENTER MA-210, DULLES, VA 20166, 703-572-2730", "disabled": ""},
            {"name":"Airport Owner", "type": "textarea", "columnClasses": "col-1-2", "value": "METRO WASH ARPT AUTHORITY, 1 AVIATION CIRCLE, WASHINGTON, DC 20001-6000, 703-417-8600", "disabled":""},
            {"name":"TSA Responsible FSD", "type": "textarea", "disabled": "", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"CBP Responsible Lead", "type": "textarea", "disabled": "", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"CDC Q-Station Present", "type": "textarea", "disabled": "", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"CDC Responsible QMO", "type": "textarea", "disabled": "", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"FBO", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"Airport Operator Emergency Contact Info", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"Airport Operations Center Contact Info", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans
            {"name":"Contingency Ramp Main Tel", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "703-417-8600"}, //ask evans
        ]
    },
    "airport_ops": {
        "name": "Airport Ops and Infrastructure",
        "type": "form",
        "formElements": [
            {"name": "Airport Ops and Infrastructure", "type": "h3", "columnClasses": "col-1-1", "value": ""},
            {"name":"Airport Manager", "type": "textarea", "columnClasses": "col-1-2", "value": "CHRISTOPHER BROWNE, 1 SAARINEN CIRCLE, SAARINEN CENTER MA-210, DULLES, VA 20166, 703-572-2730", "disabled": ""},
            {"name":"Airport Owner", "type": "textarea", "columnClasses": "col-1-2", "value": "METRO WASH ARPT AUTHORITY, 1 AVIATION CIRCLE, WASHINGTON, DC 20001-6000, 703-417-8600", "disabled":""},
            {"name":"FBO", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "n/a"}, //ask evans
            {"name":"Airport Operator Emergency Contact Info", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "n/a"},
            {"name":"Airport Operations Center Contact Info", "type": "input[type='text']", "columnClasses": "col-1-3", "value": "n/a"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"ARFF Services", "type": "select", "selectOptions": [{"option": "A"}, {"option": "B"}, {"option": "C"}, {"option": "D"}, {"option": "E"}], "columnClasses": "col-1-2", "value": "B"},
            {"name":"ARFF Services Status", "type": "select", "selectOptions": [{"option": "Arf"}, {"option": "Woof"}, {"option": "Bow-Wow"}], "columnClasses": "col-1-2", "value": "Arf"},
            {"name":"ARFF Services Status Explanation", "type": "textarea", "disabled": "", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"ARFF Notes", "type": "textarea", "disabled": "", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Fuel Types", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-1","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Fuel Availability Status", "type": "select", "selectOptions": [{"option": "Arf"}, {"option": "Woof"}, {"option": "Bow-Wow"}], "columnClasses": "col-1-1", "value": "Arf"},
            {"name":"Fuel Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Fuel Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport Overall Status",  "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Airport Overall Status Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport Overall Status Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport Power Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Normal Main Power"},{"option":"Main Power Degraded"},{"option":"Main Power Disrupted/On EGs"},{"option":"No Power"}]},
            {"name":"Airport Power Status Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport Power Status Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Aviation Operator Ops Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Normal Main Power"},{"option":"Main Power Degraded"},{"option":"Main Power Disrupted/On EGs"},{"option":"No Power"}]},
            {"name":"Aviation Operator Ops Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Aviation Operator Ops Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"}
        ]
    },
    "current_notams": {
        "name": "Current NOTAMs",
        "type": "form",
        "formElements": [
            {"name":"Current NOTAMs", "type":"h3", "columnClasses": "col-1-1"},
            {"name":"Airport Current NOTAMs", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-1","value": "!FDC 1/4735 IAD FI/T STAR WASHINGTON DULLES INTL, WASHINGTON, D.C.,"},
            {"name":"Airport Current METARs and TAFs", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-1","value": "!FDC 4/2005 IAD IAP WASHINGTON DULLES INTL, WASHINGTON, DC."},
            {"name":"Airport Current NOTAMs", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-1","value": "!FDC 4/2823 IAD STAR WASHINGTON DULLES INTERNATIONAL, WASHINGTON,"},
            {"name":"Airport Remarks", "type":"input[type='text']", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-1","value": "!FDC 4/5589 IAD STAR WASHINGTON DULLES INTERNATIONAL, WASHINGTON,"},
        ]
    },
    "ans_ops": {
        "name": "ANS Ops",
        "type": "form",
        "formElements": [
            {"name":"ANS Ops", "type":"h3", "columnClasses": "col-1-1"},
            {"name":"Core 30 Membership", "type":"input[type='text']", "columnClasses": "col-1-3", "value": "Yes"}, //checkbox
            {"name":"OEP 35 Membership", "type":"input[type='text']", "columnClasses": "col-1-3", "value": "Yes"}, //checkbox
            {"name":"OPSNET 45 Membership", "type":"input[type='text']", "columnClasses": "col-1-3", "value": "Yes"}, //checkbox
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport Tower Present", "type":"input[type='text']", "columnClasses": "col-1-2", "value": "Yes"}, //checkbox
            {"name":"Airport Tower Type", "type": "select", "selectOptions": [{"option": "Operational"}, {"option": "Degraded"}, {"option": "Unavailable"}], "columnClasses": "col-1-2", "value": "Operational"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Tower ID", "type":"input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans about this one.
            {"name":"Tower Service Status", "type": "select", "selectOptions": [{"option": "A"}, {"option": "B"}, {"option": "C"}, {"option": "D"}, {"option": "E"}], "columnClasses": "col-1-2", "value": "B"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Approach Control", "type":"input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //display ATM OPs Status ask evans what this looks like
            {"name":"Approach Control Status", "type": "select", "selectOptions": [{"option": "A"}, {"option": "B"}, {"option": "C"}, {"option": "D"}, {"option": "E"}], "columnClasses": "col-1-2", "value": "B"}, //ask evans about data in this field
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"En Route ATC", "type":"input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //ask evans is this a datatable
            {"name":"En Route ATC Status", "type": "select", "selectOptions": [{"option": "Operational"}, {"option": "Degraded"}, {"option": "Unavailable"}], "columnClasses": "col-1-2", "value": "Operational"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Flight Services", "type":"input[type='text']", "columnClasses": "col-1-2", "value": "n/a"},
            {"name":"Flight Services Status", "type": "select", "selectOptions": [{"option": "A"}, {"option": "B"}, {"option": "C"}, {"option": "D"}, {"option": "E"}], "columnClasses": "col-1-2", "value": "B"},
            {"name":"Baseline AAR", "type":"input[type='text']", "columnClasses": "col-1-1", "value": "n/a"},
            {"name":"Current AAR", "type":"input[type='text']", "columnClasses": "col-1-1", "value": "THIS IS A DATATABLE"}, //DATATABLE
            {"name":"Current AAR Explanation", "type":"textarea", "columnClasses": "col-1-2", "value": "", "disabled": ""}, //textarea
            {"name":"Current AAR Notes", "type":"textarea", "columnClasses": "col-1-2", "value": "", "disabled": ""}, //textarea
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Variance Between Baseline AAR and Current AAR", "type":"input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Average Daily Operations", "type":"input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Total Operations Previous Day", "type":"input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Variance Between Average Operations Number and Total Operations Previous Day", "type":"input[type='text']", "columnClasses": "col-1-1", "value": ""},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Total Estimated Completed Operations for Current Day", "type":"input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Total Scheduled Operations for Current Day", "type":"input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"Total Projected Operations for Current Day", "type":"input[type='text']", "columnClasses": "col-1-3", "value": ""},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Variance Between Average Operations Number and Total Operations Previous Day", "type":"input[type='text']", "columnClasses": "col-1-2", "value": ""},
            {"name":"Applied TFM Measures", "type":"input[type='text']", "columnClasses": "col-1-2", "value": ""}
        ]
    },
    "ans_inf": {
        "name": "ANS Infrastructure",
        "type": "form",
        "formElements": [
            {"name":"ANS Infrastructure", "type":"h3", "columnClasses": "col-1-1"},
            {"name":"ANS Infrastructure Overall Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"ANS Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"ANS Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"ANS System ID", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "ID1979"},
            {"name":"ANS Systems Status", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "Status"}
        ]
    },
    "other_ops": {
        "name": "Other Agency Ops",
        "type": "form",
        "formElements": [
            {"name":"Other Agency Ops", "type":"h3", "columnClasses": "col-1-1"}, //select
            {"name":"POE Status", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"},
            {"name":"TSA Responsible FSD", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"},
            {"name":"CBP Responsible Lead", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"},
            {"name":"CDC Q-Station Present", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"}, //yes or no? ask evans
            {"name":"CDC Responsible QMO", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"},
            {"name":"CBP Responsible Lead", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport TSA Ops Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Airport TSA Ops Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport TSA Ops Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Airport CBP Ops Status", "type": "select", "perms": ["admin"], "icons": ["status"], "columnClasses": "col-1-1","value": "Open With Limitations", "selectOptions": [{"option":"Open", "color": "green"},{"option":"Open With Limitations", "color": "yellow"},{"option":"Closed", "color": "red"}]},
            {"name":"Airport CBP Ops Explanation", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Airport CBP Ops Notes", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
        ]
    },
    "incident_info": {
        "name": "Incident Management Information",
        "type": "form",
        "formElements": [
            {"name":"Incident Management Information", "type":"h3", "columnClasses": "col-1-1"},
            {"name":"ATO Terminal District", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"ATO Service Area", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"FAA Region", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "AEA", "disabled": ""},
            {"name":"FEMA Region", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"GQP Intersection", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"GQP Membership", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"IWL Designation", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"IWL Membership", "type": "input[type='text']", "columnClasses": "col-1-2", "value": "n/a", "disabled": ""},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Response Airport Designation Explanation", "type": "select", "selectOptions": [{"option": "FSA"}, {"option": "ISB"}, {"option": "APOE/D"}, {"option": "JRSOI"}], "columnClasses": "col-1-1", "value": ""},
            {"name":"Contingency Ramp Presence", "type": "textarea", "disabled": "", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Contingency Ramp Main Tel", "type": "textarea", "disabled": "", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"", "type": "spacer", "disabled": "", "columnClasses": "col-1-1 spacer","value": ""},
            {"name":"Notes", "type": "textarea", "disabled": "", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
            {"name":"Last Update to Airport Profile", "type": "textarea", "disabled": "readonly", "columnClasses": "col-1-2","value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et gravida nunc rutrum! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique urna at laoreet bibendum.\n\rEntered by John.Evans on 2015/09/04 18:21:50Z"},
        ]
    },
    "useful_links": {
        "name": "Useful Links",
        "type": "form",
        "formElements": [
            {"name":"Useful Links", "type":"h3", "columnClasses": "col-1-1"}, //select
            {"name":"Supporting Instrument Procedures", "type": "link", "columnClasses": "col-1-2", "id": "1", "linkName": "AirNav", "linkURL": "http://airnav.com/airport/19487#ifr" },
            {"name":"Airport Diagram", "type": "link", "columnClasses": "col-1-2", "id": "2", "linkName": "AeroNav", "linkURL": "http://aeronav.faa.gov/digital_tpp_search.asp?fldIdent=19487&fld_ident_type=FAA&ST=&txtAirportName=&ver=1401&fldShowADOnly=Y&eff=01-09-2000&end=02-06-2099" },
            {"name":"Overlying Sectional", "type": "link", "columnClasses": "col-1-2", "id": "3", "linkName": "NFDC", "linkURL": "https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=19487" },
            {"name":"A/FD Information", "type": "link", "columnClasses": "col-1-2", "id": "4", "linkName": "AeroNav", "linkURL": "http://aeronav.faa.gov/new_afd.asp?effDate=29MAY2014&eff=05-29-2014&end=07-24-2014&search=19487&fld_ident_type=FAA&stateSearch=&citySearch=&volSearch=&chartSearch=&select=&navaidSearch=&submit1=Search#results" },
            {"name":"Google Maps", "type": "link", "columnClasses": "col-1-2", "id": "5", "linkName": "Google", "linkURL": "https://www.google.com/maps?q=LEESBURG+EXECUTIVE&z=18" },
            {"name":"NFDC", "type": "link", "columnClasses": "col-1-2", "id": "6", "linkName": "NFDC", "linkURL": "https://nfdc.faa.gov/nfdcApps/services/airportLookup/airportDisplay.jsp?airportId=19487" }
        ]
    },
 };