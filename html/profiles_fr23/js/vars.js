// $(document).ready(function(){

        var lastToggled = "",
            $module = $('.nisis_profile'),
            $header = $('#header'),
            $edit = $('.btn-edit'),
            $save = $('.btn-save'),
            // $close = $('.btn-close'),
            $closeModule = $('.btn-close-module'),
            $resizeModule = $('.btn-resize-module'),
            $closeFormsNav = $('.btn-close-nav-forms'),
            $toggleQSR = $('.btn-qsr'),
            $toggleProfiles = $('.btn-profiles'),
            toggleActiveClass = 'dark-dark-blue',
            $upload = $('.btn-upload'),
            $input = $('.paper-input'),
            $clear = $('#clear'),
            $ul = $('#form-nav-elements'),
            $nav = $('nav.forms'),
            $label = $('label'),
            $forms = $('.forms-wrap'),
            $datatables = $('.datatables-wrap'),
            $qsr_menu = $('.qsr-menu-container'),
            $profiles_menu = $('.profiles-menu-container'),
            $link2status = $('menu.qsr > menuitem'),
            $select = $('select'),
            toggleList = {
                "btn-qsr": $qsr_menu,
                "btn-profiles": $profiles_menu
            };

        //bind events
        $toggleQSR.on('click',toggleMenu);
        $toggleProfiles.on('click', toggleMenu);
        $resizeModule.on('click', toggleModuleCollapse);
        $closeModule.on('click', closeModule);
        $closeFormsNav.on('click', toggleMenu);
        $edit.on('click', edit);
        // $input.on('keydown keyup focus', floatLabel);
        $clear.on('click', clearInput);
        $link2status.on('click', jump2status);
        $select.on('change',handleSelect);

// });
