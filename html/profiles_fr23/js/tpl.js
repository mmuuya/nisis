// template
var tpls,
    formsTpl,
    formsCount = 0,
    classname,
    formElements,
    formRender,
    qrsTpl,
    datatables,
    datatableRender,
    datatablesTpl,
    datatablesCount = 0;
//  and whatever in the inner loop

// forms
formsTpl = kendo.template($("#profile_forms").html());

for(var i in forms){
    if(forms[i].type == "form"){
    // TODO generate containers here
        formElements = forms[i].formElements;
        formRender = kendo.render(formsTpl, formElements);
        classname = Object.keys(forms)[formsCount];
        $("#form-" + classname).html(formRender);
        formsCount ++;
    }
}

//datatables
for(var i in datatables){
    // TODO generate containers here
        var data = datatables[i].data,
            name = datatables[i].name,
            classname = Object.keys(datatables)[datatablesCount];

        var dataSource = new kendo.data.DataSource({data: datatables[classname].rows});
        var $datatable = $("#datatable-" + classname);
        // console.log($datatable, classname, datatablesCount);
        var grid = $datatable.kendoGrid({
            dataSource: dataSource,
                sortable: false,
                columns: datatables[classname].columns
        });
        $("#datatable-" + classname).prepend('<h3 class="#: columnClasses #"><hr class="med-blue2"/><div class="white med-dark-blue2 paddingR10px">' + datatables[i].name + '</div></h3>');
        datatablesCount ++;
}



//qsr menu
qsrMenuTpl =  kendo.template($("#quick_status_list").html());
$("menu.qsr").html(kendo.render(qsrMenuTpl, statuses));

//profiles menu
profilesMenuTpl =  kendo.template($("#profiles_list").html());
$("menu.profiles").html(kendo.render(profilesMenuTpl, profileMenu));


// TODO:  containers for forms vs. datatables
