var airportProfileConstants = {
datatables : {
    "runways": {
        "name": "Runways",
        "type": "datatable",
        "action": "get_runways",
        "assoc_type": "",
        "columns": [{
                template: "#= RWY_ID #",
                field: "RWY_ID",
                title: "Runway ID",
                hidden: false
            },{
                template: "#= RWY_LENGTH #",
                field: "RWY_LENGTH",
                title: "Runway Length",
                hidden: false
            }
            ,{
                template: "#= RWY_WIDTH #",
                field: "RWY_WIDTH",
                title: "Runway Width",
                hidden: false
            }
            ,{
                template: "#= BE_ELEVATION #",
                field: "BE_ELEVATION",
                title: "BE Elevation",
                hidden: true
            }
            ,{
                template: "#= RE_ELEVATION #",
                field: "RE_ELEVATION",
                title: "RE Elevation",
                hidden: true
            },{
                template: "#= RWY_SURFACE_TYPE #",
                field: "RWY_SURFACE_TYPE",
                title: "Runway Surface Type",
                hidden: true
            },{
                template: "#= RWY_SURFACE_TREATMENT #",
                field: "RWY_SURFACE_TREATMENT",
                title: "Runway Surface Treatment",
                hidden: true
            },{
                template: "#= PAVEMENT_CLASS #",
                field: "PAVEMENT_CLASS",
                title: "Pavement Class",
                hidden: true
            },{
                template: "#= SINGLE_WHEEL_WEIGHT #",
                field: "SINGLE_WHEEL_WEIGHT",
                title: "Single Wheel Weight",
                hidden: true
            },{
                template: "#= DUAL_WHEEL_WEIGHT #",
                field: "DUAL_WHEEL_WEIGHT",
                title: "Dual Wheel Weight",
                hidden: true
            },{
                template: "#= TWO_DUAL_WHEEL_WEIGHT #",
                field: "TWO_DUAL_WHEEL_WEIGHT",
                title: "Two Dual Wheel Weight",
                hidden: true
            },{
                template: "#= TWO_DUAL_TANDEM_WHEEL_WEIGHT #",
                field: "TWO_DUAL_TANDEM_WHEEL_WEIGHT",
                title: "Two Dual Tandem Wheel Weight",
                hidden: true
            },{
                template: "#= RWY_LEIH #",
                field: "RWY_LEIH",
                title: "Runway Edge Intensity Lights",
                hidden: true
            },{
                template: "#= BE_VGSI #",
                field: "BE_VGSI",
                title: "BE VGSI",
                hidden: true
            },{
                template: "#= BE_ALSAF #",
                field: "BE_ALSAF",
                title: "BE ALSAF",
                hidden: true
            },{
                template: "#= BE_REIL #",
                field: "BE_REIL",
                title: "BE REIL",
                hidden: true
            },{
                template: "#= BE_CL_LIGHTS #",
                field: "BE_CL_LIGHTS",
                title: "BE CL Lights",
                hidden: true
            },{
                template: "#= BE_TOUCHDOWN_LIGHTS #",
                field: "BE_TOUCHDOWN_LIGHTS",
                title: "BE Touchdown Lights",
                hidden: true
            },{
                template: "#= RE_ASI #",
                field: "RE_ASI",
                title: "RE ASI",
                hidden: true
            },{
                template: "#= RE_ALSAF #",
                field: "RE_ALSAF",
                title: "RE ALSAF",
                hidden: true
            },{
                template: "#= RE_REIL #",
                field: "RE_REIL",
                title: "RE REIL",
                hidden: true
            },{
                template: "#= RE_CL_LIGHTS #",
                field: "RE_CL_LIGHTS",
                title: "RE CL LIGHTS",
                hidden: true
            },{
                template: "#= RE_TOUCHDOWN_LIGHTS #",
                field: "RE_TOUCHDOWN_LIGHTS",
                title: "Runway RE touchdown lights",
                hidden: true
            }
        ],
        "fields": [{
                    RWY_ID: {type : "string"},
                    RWY_LENGTH: {type : "number"},
                    RWY_WIDTH: {type : "number"},
                    BE_ELEVATION: {type : "string"},
                    RE_ELEVATION: {type : "string"},
                    RWY_SURFACE_TYPE: {type : "string"},
                    RWY_SURFACE_TREATMENT: {type : "string"},
                    PAVEMENT_CLASS: {type: "string"},
                    SINGLE_WHEEL_WEIGHT: {type: "number"},
                    DUAL_WHEEL_WEIGHT: {type: "number"},
                    TWO_DUAL_WHEEL_WEIGHT: {type: "number"},
                    TWO_DUAL_TANDEM_WHEEL_WEIGHT: {type: "number"},
                    RWY_LEIH: {type: "string"},
                    BE_VGSI: {type: "string"},
                    BE_ALSAF: {type: "string"},
                    BE_REIL: {type: "string"},
                    BE_CL_LIGHTS: {type: "string"},
                    BE_TOUCHDOWN_LIGHTS: {type: "string"},
                    RE_ASI: {type: "string"},
                    RE_ALSAF: {type: "string"},
                    RE_REIL: {type: "string"},
                    RE_CL_LIGHTS: {type: "string"},
                    RE_TOUCHDOWN_LIGHTS: {type: "string"}
                }
        ]

    },
    "associated_ans": {
        "name": "Associated ANS",
        "type": "datatable",
        "action": "get_assoc_vw",
        "assoc_type": "arpt-ans",
         "columns": [{
                template: "#= BASIC_CAT #",
                field: "BASIC_CAT",
                title: "Basic Category"
            },
            {
                template: "#= ANS_ID #",
                field: "ANS_ID",
                title: "ANS ID"
            },
            {
                template: "#= ANS_STS #",
                field: "ANS_STS",
                title: "Status"
            }
        ],
        "fields": [{
                    BASIC_CAT: {type : "string"},
                    ANS_ID: {type : "string"},
                    ANS_STS: {type : "string"}
                }
        ]
    },
    "associated_atm": {
        "name": "Associated ATMs",
        "type": "datatable",
        "action": "get_assoc_vw",
        "assoc_type": "arpt-atm",
         "columns": [
            {
                template: "#= ATM_ID #",
                field: "ATM_ID",
                title: "ATM ID"
            },
            {
                template: "#= LG_NAME #",
                field: "LG_NAME",
                title: "Long Name"
            },
            {
                template: "#= OPS_STATUS #",
                field: "OPS_STATUS",
                title: "Status"
            }
        ],
        "fields": [{                    
                    ATM_ID: {type : "string"},
                    LG_NAME: {type : "string"},
                    OPS_STATUS: {type : "string"}
                }
        ]
    }
},

profileMenu: [
    {"label": "Basic Information", "target": "#form-basic_info", "isDatatable": false},
    {"label": "Status Update", "target": "#form-status_update", "isDatatable": false},
    {"label": "Contact Info", "target": "#form-contact_info", "isDatatable": false},
    {"label": "Airport Ops and Infrastructure", "target": "#form-airport_ops", "isDatatable": false},
    {"label": "Runways", "target": "#datatable-runways", "isDatatable": true},
    {"label": "Current NOTAMs", "target": "#form-current_notams", "isDatatable": false},
    {"label": "Associated ANS", "target": "#datatable-associated_ans", "isDatatable": true},
    {"label": "Associated ATMs", "target": "#datatable-associated_atm", "isDatatable": true},
   // {"label": "Associated ATMs", "target": "#form-assoc_atms", "isDatatable": false},
    {"label": "ANS Ops (ATC, TFM, etc.)", "target": "#form-ans_ops", "isDatatable": false},
    {"label": "ANS Infrastructure", "target": "#form-ans_inf", "isDatatable": false},
    {"label": "Other Agency Ops", "target": "#form-other_ops", "isDatatable": false},
    {"label": "Incident Management Information", "target": "#form-incident_info", "isDatatable": false},
    {"label": "Useful Links", "target": "#form-useful_links", "isDatatable": false}
],

// TODO: denormalize from formFields)
statuses: [
    {"label": "Airport Overall Status", "dbcol": "ovr_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 773},
    {"label": "Airport Power Status", "dbcol": "apt_pwr_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 955},
    {"label": "Aviation Operator Ops Status", "dbcol": "aoo_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1145},
    {"label": "Airport TSA Ops Status", "dbcol": "tsa_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1380},
    {"label": "Airport CBP Ops Status", "dbcol": "cbp_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575},
    {"label": "ARFF Services Status", "dbcol": "arff_svc_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 195.5},
    {"label": "Fuel Availability Status", "dbcol": "ft_avail_sts", "value": "#","status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 430.5},
    {"label": "ANS Infrastructure Overall Status", "dbcol": "ans_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 590},
    {"label": "Flight Services Status", "dbcol": "flight_svc_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 195.5},
   //NISIS-2795 - KOFI HONU 
    {"label": "Airport Tower Status", "dbcol": "tower_ops_status", "value": "#", "status": "", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 185},
    {"label": "Approach Control Status", "dbcol": "appr_ctrl_ops_status", "value": "#", "status": "", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 725.09375},
    {"label": "Enroute ATC Status", "dbcol": "enrte_atc_ops_status", "value": "#", "status": "", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 725.09375}
],

// //Global readyonly: overwrites user perms. For example, Airport Profile shows Tower properties but thats another NISIS tracking object
readonlyFields: [
     {"label": "Airport Tower Present ", "dbcol": "tower"},
     {"label": "Airport Tower Type", "dbcol": "tower_type"},
     {"label": "Airport Type", "dbcol": "arp_type"}
]


}

