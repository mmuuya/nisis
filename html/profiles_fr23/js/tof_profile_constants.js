var tofProfileConstants = {
datatables : {
    "associated_atm": {
        "name": "Associated ATMs",
        "type": "datatable",
        "action": "get_associated_grids",
        "assoc_type": "tof-associated-atm-facilities",
         "columns": [
            {
                template: "#= ATM_ID #",
                field: "ATM_ID",
                title: "ATM ID"
            },
            {
                template: "#= LG_NAME #",
                field: "LG_NAME",
                title: "Long Name"
            },
            {
                template: "#= OPS_STATUS_LABEL #",
                field: "OPS_STATUS_LABEL",
                title: "Status"
            }
        ],
        "fields": [{                    
                    ATM_ID: {type : "string"},
                    LG_NAME: {type : "string"},
                    OPS_STATUS_LABEL: {type : "string"}
                }
        ]
    }
},

profileMenu: [
    {"label": "Basic Information", "target": "#form-basic_info", "isDatatable": false},
    {"label": "Status Update", "target": "#form-status_update", "isDatatable": false},
    {"label": "Contact Info", "target": "#form-contact_info", "isDatatable": false},
    {"label": "Staffing", "target": "#form-staffing", "isDatatable": false},
    {"label": "Associated ATMs", "target": "#datatable-associated_atm", "isDatatable": true},
    {"label": "Incident Management Information", "target": "#form-incident_info", "isDatatable": false}
],

// QSR
statuses: [
    {"label": "TOF Status", "dbcol": "ops_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 773},
    {"label": "TOF Power Status", "dbcol": "bpwr_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 955},
    {"label": "Ops Staffing Status", "dbcol": "ops_staffing_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1145},
    {"label": "ATO Personnel Accounting Status", "dbcol": "ato_pers_acctg_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1380},
    {"label": "Current Readiness Level ", "dbcol": "crt_rl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575},
    {"label": "Current SECON Level ", "dbcol": "crt_secon_lvl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575},
    // {"label": "Satellite Phone Status", "dbcol": "sat_ph_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575}
]

}
