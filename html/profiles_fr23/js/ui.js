// $("input:checkbox").on('change', function() {
//     $("input:checkbox").not(this).prop('checked', false);
// });

// kendo Modal
// TODO cleanup

makeModal = function(currentValues){

    currentValues.container.append('<div id="window"></div>');

    var $myWindow = $("#window"),
        currentValues = Object.assign({}, currentValues),
        updatedTextareaValue = null,
        $nextExplanationField = $("#" + currentValues.textarea[0].id),
        selectID = currentValues.select,
        textareaValue = currentValues.value,
        textareaTop = currentValues.top + 8,
        textareaLeft = currentValues.left,
        widthIndex = currentValues.widthIndex,
        $update = $('button[type="submit"]'),
        $reset = $('button[type="reset"]'),
        $editField = $("#editField"),
        $windowContainerPos = {top: textareaTop, left: textareaLeft },
        $windowWidth = $(".forms-wrap").hasClass("with-menu") ? "281" : "417",
        signature = [sigIntro, capitalize(nisis.user.name),"\r\non", new Date().toGMTString()].join(" ").replace(/ GMT/,"Z");


    positionWindow = function(posObj){
        $myWindow.parent('.k-widget.k-window').css(posObj);
    };

    var config = {
        width: $windowWidth,
        height: "auto",
        iframe: false,
        // FIXME open animation origin if possible
        // animation: {open:{duration: 250}},
        animation: false,
        title: false,
        modal: true,
        visible: false,
        draggable: false,
        resize: positionWindow($windowContainerPos),
        deactivate: function(){
            this.destroy();
        }
    };


    $myWindow
    .css("width", $windowWidth)
    .kendoWindow(config)
    .data("kendoWindow")
    // TODO use content template
    // TODO add validation for entry - alert if bad
    .content('<div class="paper-input__wrapper"><textarea id="editField" class="paper-input paper-input--touched">' + textareaValue + '</textarea><button type="reset" value="reset" title="ClearReset"><i class="btn-clear fa fa-ban marginRhalfem"></i><strong>Clear</strong></button><button type="submit" title="Update Explanation" disabled="disabled"><i class="btn-save fa fa-pencil marginRhalfem"></i><strong>Update</strong></button></div>')
    .open();
    // TODO - fix content editable
    // - add slide up

    positionWindow($windowContainerPos);

    $(editField).focus();

    if($("#editField").val().length >= 6){
        $('button[type="submit"]').prop('disabled', false);
    }

    $myWindow.on('click', 'button[type="reset"]', function(e){
        $myWindow.data("kendoWindow").close();
        //FIXME: seperate function
        
        var valIndex = $(selectID + " option[value=" + previousVal + "]").index();
        $(selectID).data("kendoDropDownList").select(valIndex)
        $(selectID).data("kendoDropDownList").trigger('change');

        // console.log("previousVal:", previousVal)
    });

    $(editField).on('focus change keyup', function(){
        if($("#editField").val().length >= 6){
            console.log('you have plenty of characters');
            //alert("Please enter at least 6 characters");
            $('button[type="submit"]').prop('disabled', false);
        }else{
            console.log('you need more buddy...');
            $('button[type="submit"]').prop('disabled', true);
        }
    });

    $myWindow.on('click', 'button[type="submit"]', function(e){
        e.preventDefault();
        // FIXME: FB 12/12 total PITA, will come back to this
        // $nextExplanationField.val(addSig($("#editField").val(), signature)).change().addClass('paper-input--touched');
        setTimeout(function(){
            // FIXME: FB - 12/21- commenting out for now
            // $nextExplanationField.val(addSig($("#editField").val(), signature)).change().addClass('paper-input--touched');
            $nextExplanationField.val($("#editField").val()).change().addClass('paper-input--touched');
            $myWindow.data("kendoWindow").close();
        },100);
    });
};
