var atmProfileConstants = {
datatables : {
    "supported-airports": {
        "name": "Supported Airports",
        "type": "datatable",
        "action": "get_associated_grids",
        "assoc_type": "atm-supported-airports",
            "columns": [
            {
                template: "#= ARPT_ID #",
                field: "ARPT_ID",
                title: "Airport ID"
            },
            {
                template: "#= LG_NAME #",
                field: "LG_NAME",
                title: "Airport Long Name"
            },
            {
                template: "#= STATUS #",
                field: "STATUS",
                title: "Status",
                hidden: true
            },
            {
                template: "#= STATUS_LABEL #",
                field: "STATUS_LABEL",
                title: "Status",
                hidden: false
            }
        ],
        "fields": [{
                    ARPT_ID: {type : "string"},
                    LG_NAME: {type : "string"},
                    STATUS: {type : "string"},
                    STATUS_LABEL: {type : "string"}
                }
        ]

    },
    "tech-ops-staffed-facilities": {
        "name": "Associated Tech Ops Staffed Facilities",
        "type": "datatable",
        "action": "get_associated_grids",
        "assoc_type": "atm-supported-tof-facilities",
            "columns": [
            {
                template: "#= TOF_ID #",
                field: "TOF_ID",
                title: "TOF ID"
            },
            {
                template: "#= LONG_NAME #",
                field: "LONG_NAME",
                title: "TOF Long Name"
            },
            {
                template: "#= OPS_STATUS #",
                field: "OPS_STATUS",
                title: "Status",
                hidden: true
            },
            {
                template: "#= OPS_STATUS_LABEL #",
                field: "OPS_STATUS_LABEL",
                title: "Status",
                hidden: false
            }
        ],
        "fields": [{
                    TOF_ID: {type : "string"},
                    LONG_NAME: {type : "string"},
                    STATUS: {type : "string"},
                    STATUS_LABEL: {type : "string"}
                }
        ]

    },
    "ans-infrastructure": {
        "name": "ANS Infrastructure (Navaids, etc)",
        "type": "datatable",
        "action": "get_associated_grids",
        "assoc_type": "atm-supported-ans-infrastructure",
         "columns": [
            {
                template: "#= ANS_ID #",
                field: "ANS_ID",
                title: "ANS ID",
                hidden: false
            },
            {
                template: "#= DESCRIPT #",
                field: "DESCRIPT",
                title: "Description",
                hidden: false
            },
            {
                template: "#= STATUS #",
                field: "STATUS",
                title: "Status",
                hidden: true
            },
            {
                template: "#= STATUS_LABEL #",
                field: "STATUS_LABEL",
                title: "Status",
                hidden: false
            }
        ],
        "fields": [{
                    ANS_ID: {type : "string"},
                    DESCRIPT: {type : "string"},
                    STATUS: {type : "string"},
                    STATUS_LABEL: {type : "string"}
                }
        ]
    }
},

profileMenu: [
    {"label": "Basic Information", "target": "#form-basic_info", "isDatatable": false},
    {"label": "Status Update", "target": "#form-status_update", "isDatatable": false},
    {"label": "Contact Info", "target": "#form-contact_info", "isDatatable": false},
    {"label": "Staffing", "target": "#form-staffing", "isDatatable": false},
    {"label": "Supported Airports", "target": "#datatable-supported-airports", "isDatatable": true},
    {"label": "Associated Tech Ops Staffed Facilities", "target": "#datatable-tech-ops-staffed-facilities", "isDatatable": true},
    {"label": "Associated ATM Facilities", "target": "#form-associated_atm_facilities", "isDatatable": false},
    {"label": "ANS Infrastructure (Navaids, etc)", "target": "#datatable-ans-infrastructure", "isDatatable": true},
    {"label": "Incident Management Information", "target": "#form-incident_info", "isDatatable": false}
],

// QSR
statuses: [
    {"label": "ATM Operations Status", "dbcol": "ops_status", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 773},
    {"label": "ATM Facility Power Status", "dbcol": "pwr_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 955},
    {"label": "Ops Staffing Status", "dbcol": "ops_sl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1145},
    {"label": "ATO Personnel Accounting Status", "dbcol": "ato_pa_sts", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1380},
    {"label": "Current Readiness Level ", "dbcol": "crt_rl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575},
    {"label": "Current SECON Level ", "dbcol": "crt_sl", "value": "#", "status_text": "Unknown", "targetForm": "#form-status_update", "targetOffset": 1575}
],


//Global readyonly: overwrites user perms. For example, Airport Profile shows Tower properties but thats another NISIS tracking object
readonlyFields: [{
    "label": "Personnel Accounted for Percentage",
    // "dbcol": "tower"
}]

}
