<?php
	require "lib/check_pw_token.php";
?>
<script type="text/javascript" src="scripts/login.js"></script>
<script type="text/javascript" src="admin/scripts/reset.js"></script>


<!-- Modal window for intro warning -->
<div class="lgn-ol"></div>
<div class="lgn-mdl">
	<div class="lgn-mdl-hdr">
		Welcome to DOTMap
	</div>
	<div class="lgn-mdl-bdy">
		<?php include 'DOTMap_dsclmr.html'; ?>
	</div>
	<div class="lgn-mdl-ftr">
		<a class="lgn-mdl-btn" id="continue">Continue</a><a class="lgn-mdl-btn" id="cancel">Cancel</a>
	</div>
</div>
<!-- end of Modal -->

<!-- Main content -->
<div id="welcome" class=""></div>
<div id="lgn-wrpr" class="dotmap">
	<div class="lgn-cont m-cntr">
		<div class="lgn-box flt-lf m-hide bg-wht">
			<div class="lgn-wrng-txt">
				<?php include "DOTMap_warning.html"; ?>
			</div>
		</div>

		<div class="lgn-box flt-rt">
			<div id="lgn-frm">
				<div class="lgn-frm-hdr">
					<h1>DOTMap</h1>
					<h3>USDOT’s Geographic Information System for Emergency Operations</h3>
				</div>
				
				<div class="lgn-frm-cnt">
					<input type="text" id="username" name="username" class="form-input lgn-input" placeholder="&#xf007;  username" required="required">
					<input type="password" id="password" name="password" class="form-input lgn-input" placeholder="&#xf023;  password" required="required">
				
					<a id="reset" class="lgn-btn clr">Clear</a>
					<a id="submit" class="lgn-btn sbmt">Submit</a>
				</div>

				<div class="lgn-frm-ftr">
					Forgot your password? <a id="newpass">Reset here</a>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Footer -->
<footer class="lgn-ftr">
	<div class="lgn-cont m-cntr">
		<div class="lgn-ftr-brnd"><img src="images/logos/DOT_logo.png" alt="">Department of Transportation</div>
	</div>
</footer>



<!-- Reset Password Forms -->
<div id="resetForms" class="dialog"
	data-role="window"
	data-title="Reset Password"
	data-visible="false"
	data-modal="true"
	data-width="50%">

<?php
	$showDefaultForm = TRUE;
	if (array_key_exists('token', $_GET)){
		if(checkPwToken($_GET['token'])){
			$showDefaultForm = FALSE;
?>
			<div id="newPwForm">
				Your new password must:
				<ul>
					<li>Be longer than 8 characters</li>
					<li>Contain at least one upper-case character</li>
					<li>Contain at least one lower-case character</li>
					<li>Contain at least one number</li>
					<li>Contain at least one special character: !#@$%&+=?:^~{}[]<>()</li>
				</ul>
				<span id="inputToken" style="display:none"><?php echo $_GET['token'] ?></span>
				<input type="password" class="k-textbox lgn-input" placeholder="Enter New Password" data-bind="value:nupass1"/><br/>
				<input type="password" class="k-textbox lgn-input" placeholder="Confirm New Password" data-bind="value:nupass2"/><br/>
				<button data-bind="click:submitNuPass" class="k-button">Change Password</button><br/>
				<div id="pwErrArea" style="color:red;font-size:small;padding-top:5px">&nbsp;</div>
			</div>
<?php
		} else {
			echo '<span id="badToken" style="display:none"></span>';
		}
	}
	if($showDefaultForm){
?>
		<div id="getNameEmailForm">
			<input type="text" class="form-input lgn-input" placeholder="&#xf007;  Enter username" data-bind="value: username" required="required"/>
			<input type="email" class="form-input lgn-input" placeholder="&#xf003;  Enter Email" data-bind="value:email" required="required"/>
			<span><button data-bind="click:requestToken" class="lgn-btn email">Send Email</button></span>
		</div>
<?php
	}

	//write out a hidden message which reset.js will display if it exists in DOM
	if(isset($_SESSION['pwResetMsg'])){
		echo '<span id="resetPwMsg" style="display:none">' . $_SESSION['pwResetMsg'] . '</span>';
		unset($_SESSION['pwResetMsg']);
	}
?>

</div>