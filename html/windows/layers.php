
<div id='overlays-content' class='margBottom clear'>
	
  <div id="map_layers_tabs" data-role="tabstrip" >
	<ul>
		<li class="f-tab k-state-active">NISIS/DOT</li>
		<li class="f-tab">Threat Layers</li>	
		<li class="f-tab">Arcgis Online</li>	
		<li class="f-tab">Search ArcGIS Online</li>
		<li class="f-tab">GII Data</li>
		<li class="f-tab">My Content</li>
	</ul>
	
	<div >
		<div class="layers-button-menu">
	
			<div id='layer-options' class='of-h clear'>
				<span id='zLayerExtent' class='layerOption' 
					title='Zoom to layer full extent'
					data-role='button'
					data-bind="click:zoomToLayerExtent,enabled:zoomEnabled">
					<i class="fa fa-arrows-alt"></i> </span>
					
				<span id='rLayer' class='layerOption' 
					title='Refresh Layer'
					data-role='button'
					data-bind="click:refreshLayer,enabled:refreshEnabled">
					<i class="fa fa-refresh"></i> </span>

				<span id='labelLayer' class='layerOption' 
					title='Label selected layer'
					data-role='button'
					data-bind="click:labelLayer,enabled:hasFeatures">
					<i class="fa fa-font"></i> </span>

				<span id='oLayerAttr' class='layerOption' 
					title="Open layer's Attributes"
					data-role='button'
					data-bind="visible:isNisisActive,click:openLayerAttributes,enabled:hasAttributes">
					<i class="fa fa-table"></i> </span>
					
				<span id='rLayer' class='layerOption' 
					title='Remove features selected layer'
					data-role='button'
					data-bind="click:removeFeatures,enabled:removeTempGraphics">
					<i class="fa fa-times"></i> </span> 
			
			</div> <!-- layer-options end -->

		</div> <!-- layers-button-menu end -->

		
		<!-- Data -->
		<div id='overlays-toggle' class='overlays'
		data-role="treeview"
		data-drag-and-drop="true"
		data-text-field="text"
		data-value-field="id"
		data-spritecssclass-field="type"
		data-load-on-demand="false"
		data-checkboxes="{checkChildren:true}"
		data-bind="source:treeNodes('overlays-toggle'),
			events:{select:selectLayer,
			dragstart:handleDragStart,
			dragend:handleDragEnd,
			drop:reorderLayers}"
		data-autosync="true">		
	    </div>

	</div>

	<div >
		<!-- Menu -->
		<div class="layers-button-menu">
				<div id='layer-options' class='of-h clear'>
			<span id='zLayerExtent' class='layerOption' 
				title='Zoom to layer full extent'
				data-role='button'
				data-bind="click:zoomToLayerExtent,enabled:zoomEnabled">
				<i class="fa fa-arrows-alt"></i> </span>
				
			<span id='rLayer' class='layerOption' 
				title='Refresh Layer'
				data-role='button'
				data-bind="click:refreshLayer,enabled:refreshEnabled">
				<i class="fa fa-refresh"></i> </span>



			<span id='labelLayer' class='layerOption' 
				title='Label selected layer'
				data-role='button'
				data-bind="click:labelLayer,enabled:hasFeatures">
				<i class="fa fa-font"></i> </span>

			<span id='oLayerAttr' class='layerOption' 
				title="Open layer's Attributes"
				data-role='button'
				data-bind="visible:isNisisActive,click:openLayerAttributes,enabled:hasAttributes">
				<i class="fa fa-table"></i> </span>
				
			

			<span id='rLayer' class='layerOption' 
				title='Remove features selected layer'
				data-role='button'
				data-bind="click:removeFeatures,enabled:removeTempGraphics">
				<i class="fa fa-times"></i> </span> 

			
		</div>

		
		</div>

	
	 	
		<div id='threat-layers' class='overlays'
		data-role="treeview"
		data-drag-and-drop="true"
		data-text-field="text"
		data-value-field="id"
		data-spritecssclass-field="type"
		data-load-on-demand="false"
		data-checkboxes="{checkChildren:true}"
		data-bind="source:treeNodes('threat-layers'),  
			events:{select:selectLayer,
			dragstart:handleDragStart,
			dragend:handleDragEnd,
			drop:reorderLayers}"
		data-autosync="true">		
	    </div>
	
	</div>
	
<div >
		<div class="layers-button-menu">
	
			<div id='layer-options' class='of-h clear'>
				<span id='zLayerExtent' class='layerOption' 
					title='Zoom to layer full extent'
					data-role='button'
					data-bind="click:zoomToLayerExtent,enabled:zoomEnabled">
					<i class="fa fa-arrows-alt"></i> </span>
					
				<span id='rLayer' class='layerOption' 
					title='Refresh Layer'
					data-role='button'
					data-bind="click:refreshLayer,enabled:refreshEnabled">
					<i class="fa fa-refresh"></i> </span>

				<span id='labelLayer' class='layerOption' 
					title='Label selected layer'
					data-role='button'
					data-bind="click:labelLayer,enabled:hasFeatures">
					<i class="fa fa-font"></i> </span>

				<span id='oLayerAttr' class='layerOption' 
					title="Open layer's Attributes"
					data-role='button'
					data-bind="visible:isNisisActive,click:openLayerAttributes,enabled:hasAttributes">
					<i class="fa fa-table"></i> </span>
					
				<span id='rLayer' class='layerOption' 
					title='Remove features selected layer'
					data-role='button'
					data-bind="click:removeFeatures,enabled:removeTempGraphics">
					<i class="fa fa-times"></i> </span> 
			
			</div> <!-- layer-options end -->

		</div> <!-- layers-button-menu end -->

		
		<!-- Data -->
		<div id='arcgis-online' class='overlays'
        data-role="treeview"
        data-drag-and-drop="true"
        data-text-field="text"
        data-value-field="id"
        data-spritecssclass-field="type"
        data-load-on-demand="false"
        data-checkboxes="{checkChildren:true}"
        data-bind="source:treeNodes('arcgis-online'),  
            events:{select:selectLayer,
            dragstart:handleDragStart,
            dragend:handleDragEnd,
            drop:reorderLayers}"
        data-autosync="true">        
        </div>

	</div>





	<div >
		

		

		<div class="ago-tabs">
			<h1>Search ArcGIS Online</h1>
				<div class="margBottom clear of-h">
					<input type="text" class="w-fit k-textbox" 
						placeholder="Search content ..." 
						data-value-update="keyup"
						data-bind="value:agoKeyword,events:{keyup:searchAgoContent}"/>
				</div>
				<div id="ago-results" class="clear">
					<div class="margBottom clear">
						<span data-bind="text:agoSearchMsg"></span>
					</div>
					<div class="margTop clear" data-bind="html:agoSearchContent">
						
					</div>
				</div>
			</div>
	</div>


	<div >
		<!-- Menu -->
		<div class="ago-tabs">
			<h1>GII Data</h1>
			<div class="layers-button-menu">
			
	 		
			</div>
		</div>
		
		
	</div>


	<div >
		
	

		<div class="ago-tabs">
				<div class="margBottom clear of-h"
					data-bind="visible:agoLogin">
					<span><span class="pointer ft-b td-u"
						data-bind="click:agoGetCredentials">Sign in</span> to access your content</span>
				</div>
				
				<div class="margBottom clear of-h"
					data-bind="visible:agoLogout">
					<span data-bind="text:agoFullName"></span>
					(<span class="pointer ft-b td-u"
						data-bind="click:agoSignOut">Sign Out</span>)
				</div>

				<div class="margTop clear">
					<span data-bind="text:agoUserMsg"></span>
					<div class="margTop clear" data-bind="html:agoUserContent">
						
					</div>
				</div>
			</div>

	</div>


</div> <!-- map_layers_tabs end-->

</div> <!-- overlays-content end -->

<div id="layer-trans" class='marg clear of-h'>
	<!-- <div id='layer-transparency'></div> -->
	<input id="slider" class="balSlider" title="slider" data-bind="enabled:zoomEnabled,value:layerOpacity,events:{change:changeLayerOpacity}"/>
</div>

<script>
	var $menuBar = $('#menu-bar');

	$menuBar.on('click', function(e){
		var target = e.target;
		if( target.id === "layers"){
			var $el = $('#layers-div');

			$el.parent().addClass('layers');

			var slider = $("#slider").kendoSlider({
                increaseButtonTitle: "Increase",
                decreaseButtonTitle: "Descrease",
                min: 0,
                max: 100,
                smallStep: 5,
                largeStep: 25
            }).data("kendoSlider");
		}
	})
</script>