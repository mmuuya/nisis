<div id="iwls" class="clear of-h">
	<div class="marg clear">
		<span id="load-iwls" class="k-button"
			data-bind="enabled:canLoadIWL,text:loadLabel,click:loadIWLs"></span>
		<span id="create-iwl" class="k-button"
			data-bind="enabled:canCreateIWL,text:createLabel,click:createIWL"></span>
		<button class=""
			data-role="button"
			data-bind="enabled:canEdit,click:editIWLs">Edit</button>
		<!-- <button class=""
			data-role="button"
			data-bind="enabled:canEdit,click:loadIWLFeatures">List Features</button> -->
		<button id="show-iwl-features" class=""
			data-display=""
			data-role="button"
			data-bind="enabled:iwlSelected,click:updateLayers"
			>Update Map View (<span data-bind="text:numIWLObjects"></span>)</button>
		<button class=""
			data-role="button"
			data-bind="enabled:iwlSelected,click:clearIWLFeatures"
			>Reset Map View</button>
	</div>
	<div class="marg clear of-h"
		data-bind="visible:isCreateMode">
		<div class="margBottom clear">
			<span class="msg"
				data-bind="text:iwlMsg,style:{color:iwlMsgCol}"></span>
		</div>
		<div class="w300 margBottom left clear">
			<input type="text" class="k-textbox tbl-input" 
				placeholder="Enter IWL Name"
				data-value-update="keyup"
				data-bind="value:iwlName"/>
		</div>
		<div class="w300 margBottom left clear">
			<input type="text" class="k-textbox tbl-input" 
				placeholder="Enter IWL Description"
				data-value-update="keyup"
				data-bind="value:iwlDescription"/>
		</div>
		<div class="margBottom left clear">
			<span class="k-button"
				data-bind="enabled:isIWLCorrect,text:saveLabel,click:saveIWL"></span>
			<button class=""
				data-role="button"
				data-bind="click:cancelIWL">Cancel</button>
		</div>
	</div>
	<div class="marg clear of-h">
		<div id="iwl-table" class=""></div>
	</div>
	<div style="display:none">
		<?php //include "iwlEditor.php" ?>
	</div>
</div>