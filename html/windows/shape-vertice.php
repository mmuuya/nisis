
<div class='dt-vertice pad clear of-h'>
	<!-- Vertices inputs -->
	<div class="left of-h">
		<span class='dt-vertice-count'>1- </span>
		<!-- Decimal Degree -->
		<span class="dt-input dt-dd hide">Latitude: </span>
		<span class="dt-input dt-dd hide">
			<input type='text' class='dt-lat dt-ll k-textbox'
				placeholder=''
				data-bind="events:{keyup:ddInput}" />
		</span>
		<span class="dt-input dt-dd hide">Longitude: </span>
		<span class="dt-input dt-dd hide">
			<input type='text' class='dt-lon dt-ll k-textbox'
				placeholder=''
				data-bind="events:{keyup:ddInput}"/>
		</span>
		<!-- GARS -->
		<span class="dt-input dt-input dt-gars hide">Coordinates (GARS): </span>
		<span class="dt-input dt-gars hide">
			<input type='text' class='dt-gars dt-ll k-textbox'
				placeholder=''
				data-bind="click:enableVerticeInput"/>
		</span>
		<!-- UTM -->
		<span class="dt-input dt-input dt-utm hide">Coordinates (UTM): </span>
		<span class="dt-input dt-utm hide">
			<input type='text' class='dt-utm dt-ll k-textbox'
				placeholder=''
				data-bind="click:enableVerticeInput"/>
		</span>
		<!-- DMS -->
		<span class="dt-input dt-input dt-dms">Latitude: </span>
		<span class="dt-input dt-dms">
			<input type='text' class='dt-lat1 dt-ll dt-lls k-textbox'
				maxlength="2"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
			<input type='text' class='dt-lat2 dt-ll dt-lls k-textbox'
				maxlength="2"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
			<input type='text' class='dt-lat3 dt-ll dt-llm k-textbox'
				maxlength="5"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
			<input type='text' class='dt-lat4 dt-ll dt-lls k-textbox'
				maxlength="1"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
		</span>
		<span class="dt-input dt-input dt-dms">Longitude: </span>
		<span class="dt-input dt-dms">
			<input type='text' class='dt-lon1 dt-ll dt-llm k-textbox'
				maxlength="3"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
			<input type='text' class='dt-lon2 dt-ll dt-lls k-textbox'
				maxlength="2"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
			<input type='text' class='dt-lon3 dt-ll dt-llm k-textbox'
				maxlength="5"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
			<input type='text' class='dt-lon4 dt-ll dt-lls k-textbox'
				maxlength="1"
				placeholder=''
				data-bind="events: { keyup: dmsInput }" />
		</span>
		<!-- APT -->
		<span class="dt-input dt-apt hide">Airport ID: </span>
		<span class="dt-input dt-apt hide">
			<input class="w-400 dt-apt dt-ll"
				data-role="dropdownlist"
				data-option-label="Select an Airport"
				data-auto-bind="false"
				data-text-field="LABEL"
				data-value-field="FAA"
				data-min-length="2"
				data-filter="contains"
				data-serverFiltering="true"
				data-bind="source:airportVertices" />
		</span>
		<!-- VOR -->
		<span class="dt-input dt-vor hide">VOR ID: </span>
		<span class="dt-input dt-vor hide">
			<input id="" class="w-400 dt-vor dt-ll"
				data-role="dropdownlist"
				data-option-label="Select a VOR"
				data-auto-bind="false"
				data-text-field="LABEL"
				data-value-field="ANS"
				data-min-length="2"
				data-filter="contains"
				data-serverFiltering="true"
				data-bind="source:vorVertices" />
		</span>
		<!-- FRD -->
		<span class="dt-input dt-input dt-frd hide">Fix </span>
		<span class="dt-input dt-frd hide">
			<input class="w-200 dt-frd dt-ll"
				data-role="dropdownlist"
				data-option-label="Select a VOR"
				data-auto-bind="false"
				data-text-field="LABEL"
				data-value-field="ANS"
				data-min-length="2"
				data-filter="contains"
				data-serverFiltering="true"
				data-bind="source:vorVertices,events:{select:moveToRadialInput}" />
		</span>
		<span class="dt-input dt-input dt-frd hide">Radial (&deg;) </span>
		<span class="dt-input dt-input dt-frd hide">
			<input type='number' class='dt-frd dt-frd-radial dt-ll dt-llm k-textbox'
				maxlength="3"
				min="0"
				max="360"
				placeholder=''
				data-bind="events:{keyup:frdRadialInput}" />
		</span>
		<span class="dt-input dt-input dt-frd hide">Distance (nm) </span>
		<span class="dt-input dt-input dt-frd hide">
			<input type='number' class='dt-frd dt-frd-dist dt-ll dt-llm k-textbox'
				maxlength="3"
				min="1"
				placeholder=''
				data-bind="events:{keyup:frdDistInput}" />
		</span>
	</div>
	<!-- Options to add/remove vertices -->
	<div class='dt-vertice-options right of-h'
		data-bind="visible:showVertOpts">
		<span class='dt-add-vertice pointer'
			data-bind="click:addVertice">
			<i class="fa fa-plus" title="Add another vertice"></i></span>

		<span class='dt-remove-vertice hide margLeft pointer'
			data-bind="click:removeVertice">
			<i class="fa fa-times" title="Remove this vertice"></i></span>

		<!-- <span class='margLeft'>
			<i class="dt-v-handler pointer fa fa-arrows" title="Reorder this vertice"></i></span> -->

		<span class='dt-options hide margLeft pointer'
			data-bind="click:toggleVerticeOption">
			<i class="fa fa-cog" title="Vertice options"></i></span>
	</div>
	<!-- Options for pie -->
	<div class="dt-pie-options clr of-h" data-bind="visible:isShapePie">
		<hr/>
		<span class="dt-input dt-pie dt-pie-brg1">Bearing A </span>
		<span class="dt-input dt-pie dt-pie-brg1">
			<input type='number' class='dt-pie dt-pie-brg1 dt-ll dt-llm k-textbox'
				maxlength="3"
				min="0"
				max="360"
				placeholder='' />
		</span>
		<span class="dt-input dt-pie dt-pie-brg2">Bearing B </span>
		<span class="dt-input dt-pie dt-pie-brg2">
			<input type='number' class='dt-pie dt-pie-brg2 dt-ll dt-llm k-textbox'
				maxlength="3"
				min="0"
				max="360"
				placeholder='' />
		</span>
		<span class="dt-input dt-pie dt-pie-dist">Radius (nm) </span>
		<span class="dt-input dt-pie dt-pie-dist">
			<input type='number' class='dt-pie dt-pie-dist dt-ll dt-llm k-textbox'
				maxlength="3"
				min="1"
				placeholder='' />
		</span>
	</div>
	<!-- Options for Semi circle -->
	<div class="dt-arc-options hide clr of-h">
		<hr/>
		<label>
			<input type="checkbox" class="dt-curve-line"
			data-bind="events:{change:toggleCurveFields}" /> Draw an arc from this vertice</label>&nbsp;&nbsp;&nbsp;&nbsp;
	</div>
</div>
