<?php
	require  "../../handler.php";
	require  "../../admin/api/acl_lib.php";
?>

<div class="clear of-h">
	<div class="margBottom left clear">
		<span class="iwl-save k-button">Save</span>
		<span class="iwl-delete k-button">Delete IWL</span>
		<span class="iwl-remove k-button">Remove Feature(s)</span>
		<span class="iwl-cancel k-button">Cancel</span>
	</div>
	<div class="left clear">
		<div class="m-l m-t clr">
			<label>Name *</label>
		</div>
		<div class="left margTop">
			<input type="text" class="iwl-name k-textbox w300"/>
		</div>
	</div>
	<div class="left clear">
		<div class="m-l m-t clr">
			<label>Description</label>
		</div>
		<div class="left margTop">
			<input type="text" class="iwl-desc k-textbox w300"/>
		</div>
	</div>
	<div class="margTop margBottom left clear">
		<div class="m-l m-t clr">
			<label>Access Groups</label>
		</div>
		<div class="margTop left">
			<select class="iwl-groups w300">
				<?php 
					$grps = getAllNISISGroups();
					foreach ($grps as $gp) {
						echo "<option value='" . $gp['grpid'] . "'>" . $gp['grpname'] . "</option>";
					}
				?>
			</select>
		</div>
	</div>
	<div id="iwl-feats" class="margTop clear of-h">
		<p class="margTop margBottom">Feature(s) in the IWL</p>
		<div class="iwl-feats-list">
			
		</div>
	</div>
</div>