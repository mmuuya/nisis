<?php
//increase the max execution time, this helps while on bad connection
if ($domain == "localhost") {
	ini_set("max_execution_time", 300);
}

if(isset($db)){
	if(!$db){
		$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
	}
} else {
	$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
}
if (!$db) {
    $e = oci_error();
    report(($e['message']), "error");
}
$sql = 'SELECT value,label,path,symbolname,fontletter FROM DYNAMIC_SYMBOLS ORDER BY value';
$parsed_sql = oci_parse($db, $sql);
if(!$parsed_sql){
	echo "<h1>ERROR - Could not parse SQL statement. </h1>";
	exit;
}
oci_execute($parsed_sql);
oci_fetch_all($parsed_sql, $shapesData, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW); //return data as $shapesData

//error_log("Shapes array: " . var_export($shapesData, TRUE)); //writes a string to the log
?>

<!-- Wrapper for drawing options -->
<div id="draw-options-div" class="dialog-options-div" data-role="tabstrip">
	<ul>
		<!-- <li id="draw" class="k-state-active" title="Draw Options">Draw</li> -->
		<li id="pushpins" class="k-state-active" title="Pushpins"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-map-marker"></i></li>
		<li id="draw-shapes" title="Draw Shapes"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-pencil"></i></li>
		<li id="enter-vertices" title="Draw Shapes using vertices"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-sitemap"></i></li>
		<li id="upload-list" title="Draw Shapes using list of features"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-list-alt"></i></li>
		<li id="upload-file" title="Upload Files (.zip, .kml, .csv)"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-file-zip-o"></i></li>
		<!-- <li id="upload">Upload</li> -->
		<li id="styles" title="Format Shapes"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-picture-o"></i></li>
		<!-- <li id="styles">Format</li> -->
		<li id="edit-feats" title="Editing tools"
			data-bind="events:{mouseover:showDescription,mouseout:hideDescription}">
			<i class="fa fa-pencil-square-o"></i></li>
	</ul>
	<!-- draw grphics with shape tools -->
	<!--
	<div id="drawtools" class="draw-type clear">

		<div id='shape-entry' class='marg of-h k-button-group'>
			<label data-role="button" class="draw-entry"
				title="Use existing templates to draw your feature"
				data-target="templates-div"
				data-bind="click:selectDrawOption">
				<input type='radio' name='shape-entry' id='shape-entry-templates' value="templates"/><i class="fa fa-pencil"></i> Templates</label>
			<label data-role="button" class="draw-entry"
				data-target="tools-div"
				data-bind="click:selectDrawOption">
				<input type='radio' name='shape-entry' id='shape-entry-draw' value="tools"/>Tools</label>
			<label data-role="button" class="draw-entry"
				title="Enter vertices info of your feature"
				data-target="vertices-div"
				data-bind="click:selectDrawOption">
				<input type='radio' name='shape-entry' id='shape-entry-manual' value="manual"/><i class="fa fa-map-marker"></i> Vertices</label>
			<label data-role="button" class="draw-entry"
				title="Enter a list features"
				data-target="list-div"
				data-bind="click:selectDrawOption">
				<input type='radio' name='shape-entry' id='shape-entry-list' value="list"/><i class="fa fa-list"></i> List</label>
		</div>

		<div class="clear of-h">
			<div id="templates-div" class="draw-type clear of-h">
				<div id="featTemplates" class=" clear of-h"> </div>
			</div>
			<div id='tools-div' class='draw-type marg clear of-h'>
				<div class="marg clear">
					<input id="shapes" class="w350"
						data-role="dropdownlist"
						data-bind="source:shapes,value:selTool,events:{change:activateDrawTool}"
						data-text-field="label"
						data-value-field="value" />
				</div>
				<div class="marg clear">
					<span class="" data-bind='text:shapeInputMsg'></span>
				</div>
				<div class="marg clear">
					<input type="text" id="circleRadius" class="w100"
						placeholder="Radius"
						data-role="numerictextbox"
						data-bind="value:circleRadius,visible:circleInput" />

					<input type="text" id="ellipseWidth" class="w100"
						placeholder="Weight"
						data-role="numerictextbox"
						data-bind="value:ellipseWidth,visible:ellipseInput" />

					<input type="text" id="ellipseHeight" class="w100"
						placeholder="Height"
						data-role="numerictextbox"
						data-bind="value:ellipseHeight,visible:ellipseInput" />

					<input id="dt-units" class="w100"
						data-role="dropdownlist"
						data-bind="source:shapeUnits,value:shapeUnit,visible:unitInput,events:{change:setShapeUnit}"
						data-text-field="name"
						data-value-field="value" />

					<input type="text" id="labelText" class="w250 k-textbox"
						data-role="maskedtextbox"
						placeholder="Enter your label here ..."
						data-bind="value:labelText,visible:labelInput" />

					<button id="labelStyles" class=""
						data-role="button"
						data-bind="visible:labelInput,enabled:labelValid,events{click:activateLabelTool}">Styles</button>
				</div>

			</div>
		</div>
	</div>
	-->
	<!-- draw using push button-->
	<div id="pushpin-div" class="draw-type">
		<div class="k-block">
			<div class="k-header">
				<span>Basic Shapes Pushpins</span>
			</div>
			<div id="dt-pushpins" class="clear of-h">
				<?php
				$x=0;
				//echo sizeof($shapesData);

				while($x<sizeof($shapesData)&&$shapesData[$x]['VALUE']<2001)
				{
					echo '<div class="nisis-item"  title="'.$shapesData[$x]["LABEL"].'"';
					echo ' data-tool="point"';
					echo ' data-value="'.$shapesData[$x]["VALUE"].'"';
					echo ' data-symbol="'.$shapesData[$x]["SYMBOLNAME"].'"';
					echo ' data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">';
					echo '<div class="item-symbol">';
					echo '<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">';
					echo '<path d="'.$shapesData[$x]["PATH"].'" stroke="black" stroke-width="1" fill="black" ';
					if($x==0 || $x==2) echo ' transform="scale(.7)"';
					echo '/>';
					echo '</svg>';
					echo '</div>';
					echo '</div>';
					$x++;
				}
				?>
			</div>
		</div>

<!-- 		<?php
		if ($_SESSION['isAdmin']) {
		echo
		'<div class="k-block" data-bind="visible:nisisShapes">
			<div class="k-header">
				<span>Response Teams</span>
			</div>
			<div id="dt-pushpins" class="clear of-h">
				<div class="nisis-item" title="New Team"
					data-tool="point"
					data-value="13"
					data-symbol="teams"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 18 L10 2 L18 18 Z" style="fill:white;stroke:gray;stroke-width:3"/>
                        </svg>
					</div>
				</div>
			</div>
		</div>';
		}
		?> -->

		<div class="k-block" data-bind="visible:nisisShapes">
			<div class="k-header">
				<span>Homeland Security Symbols (Natural Events)</span>
			</div>
			<div id="dt-hss-nat" class="clear of-h">
			<?php
			while($x<sizeof($shapesData)&&$shapesData[$x]['VALUE']>2000&&$shapesData[$x]['VALUE']<3001)
			{
				echo '<div class="nisis-item"  title="'.$shapesData[$x]["LABEL"].'"';
				echo ' data-tool="point"';
				echo ' data-value="'.$shapesData[$x]["VALUE"].'"';
				echo ' data-symbol="nat'.$shapesData[$x]["FONTLETTER"].'"';
				echo ' data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">';
				echo '<div class="item-symbol">';
				echo '<span class="ers-nat-font">'.$shapesData[$x]["FONTLETTER"].'</span>';
				echo '</div>';
				echo '</div>';
				$x++;
			}

			?>
			</div>
		</div>
		<div class="k-block"  data-bind="visible:nisisShapes">
			<div class="k-header">
				<span>Homeland Security Symbols (Incidents)</span>
			</div>
			<div id="dt-hss-inc" class="clear of-h">
				<?php
				while($x<sizeof($shapesData)&&$shapesData[$x]['VALUE']>3000)
					{
						echo '<div class="nisis-item"  title="'.$shapesData[$x]["LABEL"].'"';
						echo ' data-tool="point"';
						echo ' data-value="'.$shapesData[$x]["VALUE"].'"';
						echo ' data-symbol="inc'.$shapesData[$x]["FONTLETTER"].'"';
						echo 'data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">';
						echo '<div class="item-symbol">';
						echo '<span class="ers-inc-font">'.$shapesData[$x]["FONTLETTER"].'</span>';
						echo '</div>';
						echo '</div>';
						$x++;
					}
				?>
			</div>
		</div>
		<!-- <div class="k-block">
			<div class="k-header">
				<span>Homeland Security Symbols (Operations)</span>
			</div>
			<div id="dt-hss-inc" class="clear of-h">

			</div>
		</div>
		<div class="k-block">
			<div class="k-header">
				<span>Homeland Security Symbols (Infrastructures)</span>
			</div>
			<div id="dt-hss-inc" class="clear of-h">

			</div>
		</div> -->
	</div>
	<!-- draw line and polygon -->
	<div id="line-polygon-div" class="draw-type clear">
		<div class="k-block">
			<div class="k-header">
				<span>Lines</span>
			</div>
			<div id="dt-lines" class="clear of-h">
				<div class="nisis-item" title="Straight Line"
					data-tool="line"
					data-value="1"
					data-symbol="line"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="40" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                        <line x1="0" y1="10" x2="40" y2="10" style="stroke:rgb(0,0,0);stroke-width:2" />
	                    </svg>
					</div>
				</div>
				<div class="nisis-item" title="Polyline"
					data-tool="polyline"
					data-value="1"
					data-symbol="line"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="40" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                        <polyline points="0,20 12.5,0 25,20 40,0" style="fill:none;stroke:black;stroke-width:2" />
	                    </svg>
					</div>
				</div>
				<div class="nisis-item" title="Freehand Line"
					data-tool="freehandpolyline"
					data-value="1"
					data-symbol="line"
					data-bind="visible:nisisShapes,events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="40" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                        <path d="M0 20 Q 10 -10 20 10 30 30 40 0" stroke="black" stroke-width="2" fill="transparent"/>
	                    </svg>
					</div>
				</div>
			</div>
		</div>
		<div class="k-block">
			<div class="k-header">
				<span>Basic Shapes</span>
			</div>
			<div id="dt-basic-shapes" class="clear of-h">
				<div class="nisis-item" title="Circle"
					data-tool="circle"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="10" cy="10" r="9" stroke="black" stroke-width="2" fill="white" />
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Ellipse"
					data-tool="ellipse"
					data-value="1"
					data-symbol="polygon"
					data-bind="visible:nisisShapes,events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="30" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <ellipse cx="15" cy="10" rx="13" ry="8" style="fill:white;stroke:black;stroke-width:2" />
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Square"
					data-tool="rectangle"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <!-- <rect width="16" height="16" style="fill:rgb(255,255,255);stroke-width:2;stroke:rgb(0,0,0)" /> -->
                            <path d="M2 2 L18 2 L18 18 L2 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Rectangle"
					data-tool="rectangle"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="30" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <!-- <rect width="26" height="16" style="fill:rgb(255,255,255);stroke-width:2;stroke:rgb(0,0,0)" /> -->
                            <path d="M2 2 L28 2 L28 18 L2 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Triangle"
					data-tool="triangle"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 18 L10 2 L18 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Polygon"
					data-tool="polygon"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="30" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 2 L10 5 L28 2 L28 18 L2 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Freehand Polygon"
					data-tool="freehandpolygon"
					data-value="1"
					data-symbol="polygon"
					data-bind="visible:nisisShapes,events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="40" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 20 Q 10 -10 20 10 30 30 40 0 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
			</div>
		</div>
		<div class="k-block" data-bind="visible:nisisShapes">
			<div class="k-header">
				<span>Block Arrows</span>
			</div>
			<div id="dt-block-arrows" class="clear of-h">
				<div class="nisis-item" title="Right Arrow"
					data-tool="rightarrow"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 5 L10 5 L10 0 L20 10 L10 20 L10 15 L1 15 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Left Arrow"
					data-tool="leftarrow"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 10 L10 0 L10 5 L19 5 L19 15 L10 15 L10 20 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Up Arrow"
					data-tool="uparrow"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5 19 L5 10 L0 10 L10 0 L20 10 L15 10 L15 19 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
				<div class="nisis-item" title="Down Arrow"
					data-tool="downarrow"
					data-value="1"
					data-symbol="polygon"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5 1 L15 1 L15 10 L20 10 L10 20 L0 10 L5 10 Z" style="fill:white;stroke:black;stroke-width:2"/>
                        </svg>
					</div>
				</div>
			</div>
		</div>
		<div class="k-block" data-bind="visible:nisisShapes">
			<div class="k-header">
				<span>Callouts</span>
			</div>
			<div id="dt-text-labels" class="clear of-h">
				<div id="text-label" class="nisis-item" title="Text"
					data-tool="point"
					data-value="8888"
					data-symbol="text"
					data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
					<div class="item-symbol">
						<svg width="35" height="30" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                        <path d="M16,5.333c-7.732,0-14,4.701-14,10.5c0,1.982,0.741,3.833,2.016,5.414L2,25.667l5.613-1.441c2.339,1.317,5.237,2.107,8.387,2.107c7.732,0,14-4.701,14-10.5C30,10.034,23.732,5.333,16,5.333z" style="fill:white;stroke:black;stroke-width:2"/>
	                    </svg>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- enter vertices -->
	<div id='vertices-div' class='draw-type clear of-h'>
		<div class='marg clear of-h'>
			<div id='dt-types' class='clear'>
				<div class="dt-types-gp ui-buttonset" data-role="buttons">
					<label class="dt-type d-ib w-60 k-state-active"
						data-role="button"
						data-bind="click:selectShapeType">
						<input type='radio' id='dt-circle' class="hide" name='dt-type' value='circle' checked='checked'/>Circle</label>
					<label class="dt-type d-ib w-60"
						data-role="button"
						data-bind="click:selectShapeType">
						<input type='radio' id='dt-polyline' class="hide" name='dt-type' value='polyline'/>Polyline</label>
					<label class="dt-type d-ib w-60"
						data-role="button"
						data-bind="click:selectShapeType">
						<input type='radio' id='dt-polygon' class="hide" name='dt-type' value='polygon'/>Polygon</label>
					<label class="dt-type d-ib w-60"
						data-role="button"
						data-bind="click:selectShapeType">
						<input type='radio' id='dt-point' class="hide" name='dt-type' value='point'/>Points</label>
					<label class="dt-type d-ib w-60"
						data-role="button"
						data-bind="click:selectShapeType">
						<input type='radio' id='dt-arc' class="hide" name='dt-type' value='pie'/>Arc</label>
				</div>
			</div>
			<div class='marg  m-t15 m-l0 clear'>
				<span>Select Coordinates Units</span>
			</div>
			<div class='vertice-option left'>
				<input id="dt-vertices-units" class="w-400"
					data-role="dropdownlist"
					data-text-field="name"
					data-value-field="value"
					data-bind="value:verticeUnit,source:verticeUnits,events:{change:resetVerticesValue}" />
			</div>
			<div class='left' data-bind="visible:isShapeCircle">
				<div class="left margLeft">
					<input type='number' id='dt-radius' class='w100'
						data-role="numerictextbox"
						data-format="#.00"
						data-value-update="keyup"
						data-bind="value:vCircleRadius"/> <!-- data-format="n" data-min="1" -->
				</div>
				<div class="left margLeft">
					<input id="dt-radius-units" class=""
						data-role="dropdownlist"
						data-text-field="name"
						data-value-field="value"
						data-bind="value:vCircleRaduisUnit,source:vCircleRadiusUnits,events:{change:resetCircleRadius}" />
				</div>
			</div>
			<div class="clr of-h" data-bind="visible:showBufferOption">
				<div class="marg clr">
					<label><input type="checkbox" checked="false"
					data-bind="checked:bufferShape,events:{change:showBufferFields}" /> Create a buffer from this feature</label>
				</div>
				<div class="marg clr" data-bind="visible:showKeepBothOption">
					<label><input type="checkbox"
					data-bind="checked:keepBothShapes" /> Keep both shapes</label>
				</div>
			</div>
		</div>
		<div class="hide">
			<input id="dt-copy-coords" type="text" data-bind="value:copiedCoordinates">
		</div>
		<div class='dt-vertices sort marg clear of-h'>
			<?php include "shape-vertice.php"; ?>
		</div>
		<div class="marg m-l0, clr">
			<span data-bind="text:verticesMsg,attr:{class:verticesMsgType}"></span>
		</div>
		<div class='clear of-h'>
			<div class='marg clear'>
				<button id='dt-create-shape' class=''
					data-role="button"
					data-bind="enabled:verticesReady,events:{click:createVerticesShape}">Draw Graphic</button>
				<button id='dt-clear-fields' class=''
					data-role="button"
					data-bind="enabled:verticesReset,events:{click:clearVerticeFields}">Reset Fields</button>
			</div>
		</div>
	</div>
	<!-- draw graphics based on a list -->
	<div id="list-div" class="draw-type clear">
		<div class="margBottom clear">
			<input id="lt-layers" class="w300 k-block"
				data-role="dropdownlist"
				data-bind="source:fListSources,value:fListLyr,events:{change:setFListLayer}"
				data-text-field="name"
				data-value-field="value" />

		</div>
		<div class="margBottom clear">
			<textarea id="lt-featList" class="w-fit k-textbox"
				placeholder="Enter a list of features ..."
				data-bind="value:fList"></textarea>
		</div>
		<div class="clear">
			<button id="lt-submit" class="w100"
				data-role="button"
				data-bind="click:submitFeatList,enabled:fListReady">Submit</button>
			<button id="lt-clear" class="w100"
				data-role="button"
				data-bind="click:clearFeatList">Clear</button>
		</div>
		<p id="lt-processMsg" class=""
			data-bind="text:fListMsg"> </p>
	</div>
	<!-- draw graphics baseed on file upload-->
	<div id="filetools" class="draw-type clear">
		<form enctype="multipart/form-data" method="post" id="ft-uploadForm">
			<p>Upload zipped shapefile, kml or csv files</p>
			<div class="marg clear">
				<input type="file" name="file" id="ft-uploadFile"/>
			</div>
		</form>

		<div class="marg clear">
			<button id="ft-submitFile" class="k-button"
				data-bind="click:clrearFile">Display</button>
			<button id="ft-clearFile" class="k-button"
				data-bind="click:clrearFile">Clear</button>
		</div>
		<p id="ft-processMsg" data-bind="text:fileMsg"> </p>
	</div>
	<!-- style features-->
	<div id="styletools" class="draw-type clear">
		<p id='style-guide'
			data-bind="text:stylesMsg">There is no feature selected.</p>
		<div id="featureStyle" class='clear'
			data-bind="visible:stylesReady">
			<div id='selFeatStyle' class='clear'>
				<div class='marg left'>
					<select id='feat-type' class='w200'
						data-role="dropdownlist"
						data-bind="value:styleFeatType,enabled:editFeatType">
						<option value=''>Select Feature Type</option>
						<option value='point'>Point Feature</option>
						<option value='polyline'>Polyline Feature</option>
						<option value='polygon'>Polygon Feature</option>
					</select>
				</div>
				<div class='marg left'>
					<select id='style-type' class='w200'
						data-role="dropdownlist"
						data-bind="value:stylesType,enabled:editStylesType">
						<option value=''>Select Style Type</option>
						<option value='marker'>Marker Styles</option>
						<option value='picture'>Picture Styles</option>
					</select>
				</div>
			</div>
			<div id='styles-settings' class='marg clear of-h'>
				<!-- Point Features -->
				<div id='point-styles' class='marg clear'
					data-bind="visible:pointFeature">
					<div class="left">
						<!--<div class="marg clear of-h">
							 <div class="w100 left">
								<label>Marker Type</label>
							</div>
							<div class="left">
								<select id='style-type' class='w150'
									data-role="dropdownlist"
									data-bind="source:ptMarkerTypes,value:ptMarkerType"
									data-text-field="name"
									data-value-field="value">
								</select>
							</div>
						</div> -->
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Marker Size</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="n"
									data-min="1"
									data-bind="value:ptSize"/>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Marker Color</label>
							</div>
							<div class="left">
								<input class="w100"
									data-role="colorpicker"
									data-bind="value:ptColor">
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Transparency</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="p"
									data-min="0"
									data-max="1"
									data-step="0.1"
									data-bind="value:ptTrans">
							</div>
						</div>
					</div>
					<div class="left">
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Outline Type</label>
							</div>
							<div class="left">
								<select id='style-type' class='w150'
									data-role="dropdownlist"
									data-bind="source:plTypes,value:ptOutlineType"
									data-text-field="name"
									data-value-field="value">
								</select>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Outline Width</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="n"
									data-min="1"
									data-bind="value:ptOutlineWidth"/>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Outline Color</label>
							</div>
							<div class="left">
								<input class="w100"
									data-role="colorpicker"
									data-bind="value:ptOutlineColor">
							</div>
						</div>
					</div>
				</div>
				<!-- Polyline Features -->
				<div id='polyline-styles' class='marg clear'
					data-bind="visible:polylineFeature">
					<div class="left">
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Line Type</label>
							</div>
							<div class="left">
								<select id='style-type' class='w150'
									data-role="dropdownlist"
									data-bind="source:plTypes,value:plType"
									data-text-field="name"
									data-value-field="value">
								</select>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Line Width</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="n"
									data-min="1"
									data-bind="value:plWidth"/>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Line Color</label>
							</div>
							<div class="left">
								<input class="w100"
									data-role="colorpicker"
									data-bind="value:plColor">
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Transparencyz</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="p"
									data-min="0"
									data-max="1"
									data-step="0.1"
									data-bind="value:plTrans">
							</div>
						</div>
					</div>
				</div>
				<!-- Polygon Features -->
				<div id='polygon-styles' class='clear'
					data-bind="visible:polygonFeature">
					<div class="left">
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Fill Type</label>
							</div>
							<div class="left">
								<!-- ,events:{change:updatePgStyleOptions} -->
								<select id='style-type' class='w150'
									data-role="dropdownlist"
									data-bind="source:pgFillTypes,value:pgFillType"
									data-text-field="name"
									data-value-field="value">
								</select>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Fill Color</label>
							</div>
							<div class="left">
								<input class="w100"
									data-role="colorpicker"
									data-bind="value:pgFillColor,enabled:pgFillColActive">
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Transparency</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="p"
									data-min="0"
									data-max="1"
									data-step="0.1"
									data-bind="value:pgFillTrans,enabled:pgFillTransActive"/>
							</div>
						</div>
					</div>
					<div class="left">
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Outline Type</label>
							</div>
							<div class="left">
								<select id='style-type' class='w150'
									data-role="dropdownlist"
									data-bind="source:plTypes,value:pgOutlineType"
									data-text-field="name"
									data-value-field="value">
								</select>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Outline Width</label>
							</div>
							<div class="left">
								<input type='number' class='w100'
									data-role="numerictextbox"
									data-format="n"
									data-min="1"
									data-bind="value:pgOutlineWidth"/>
							</div>
						</div>
						<div class="marg clear of-h">
							<div class="w100 left">
								<label>Outline Color</label>
							</div>
							<div class="left">
								<input class="w100"
									data-role="colorpicker"
									data-bind="value:pgOutlineColor">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='marg clear'
				data-bind="visible:stylesReady">
				<button id='' class=''
					data-role="button"
					data-bind="click:updateFeatureStyles,enabled:styleFeatSelected">Apply</button>
				<button id='' class=''
					data-role="button"
					data-bind="click:saveFeatureStyles,enabled:styleFeatSelected">Apply &amp; Save Format</button>
			</div>
		</div>
	</div>
	<!-- Feature editor -->
	<div class="marg clear">
		<div id="fEditor" class="marg clear">
			<div id="featEditor" class="marg clear of-h"> </div>
		</div>

		<div id="cut-download" class="m-t15 clr"
			data-bind="visible:featureOpsActive">

			<div class="k-block">
				<div class="k-header">
					<span>Use tools below to <span
						data-bind="text:featureOpsTool"></span> features</span>
				</div>

				<div id="dt-cut-extract" class="clear of-h">
					<div class="nisis-item" title="Circle"
						data-tool="circle"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <circle cx="10" cy="10" r="9" stroke="black" stroke-width="2" fill="white" />
	                        </svg>
						</div>
					</div>

					<div class="nisis-item" title="Ellipse"
						data-tool="ellipse"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="30" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <ellipse cx="15" cy="10" rx="13" ry="8" style="fill:white;stroke:black;stroke-width:2" />
	                        </svg>
						</div>
					</div>

					<div class="nisis-item" title="Square"
						data-tool="rectangle"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <path d="M2 2 L18 2 L18 18 L2 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
	                        </svg>
						</div>
					</div>

					<div class="nisis-item" title="Rectangle"
						data-tool="rectangle"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="30" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <path d="M2 2 L28 2 L28 18 L2 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
	                        </svg>
						</div>
					</div>

					<div class="nisis-item" title="Triangle"
						data-tool="triangle"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <path d="M2 18 L10 2 L18 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
	                        </svg>
						</div>
					</div>

					<div class="nisis-item" title="Polygon"
						data-tool="polygon"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="30" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <path d="M2 2 L10 5 L28 2 L28 18 L2 18 Z" style="fill:white;stroke:black;stroke-width:2"/>
	                        </svg>
						</div>
					</div>

					<div class="nisis-item" title="Freehand Polygon"
						data-tool="freehandpolygon"
						data-ops="cut-extract"
						data-symbol="cut-extract"
						data-bind="visible:nisisShapes,events:{click:activateDrawing,mouseover:showDescription,mouseout:hideDescription}">
						<div class="item-symbol">
							<svg width="40" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg">
	                            <path d="M0 20 Q 10 -10 20 10 30 30 40 0 Z" style="fill:white;stroke:black;stroke-width:2"/>
	                        </svg>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>
