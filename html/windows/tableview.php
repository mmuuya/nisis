<div id="tbl-panel-left" class="">
	<div class="k-header of-h clear">
		<div class="left">
			<span>Filter Management Panel</span>
		</div>
		<div class="right">
			<span id="tbl-review-iwl" class="pointer" title="Create / Manage IWL(s)"
				data-bind="click:manageIWLs">
				<i class="fa fa-tasks"></i>
			</span>
			<span>&nbsp;</span>
			<span class="pointer" title="Select View Option"
				data-bind="events:{mouseover:showViewOptions}">
				<i class="fa fa-eye"></i>
			</span>
			<span>&nbsp;</span>
			<span class="pointer" title="Apply Filter"
				data-bind="click:populateTable,enabled:lyrReady">
				<i class="fa fa-filter"></i>
			</span>
		</div>
	</div>
	<!-- view options for IWLs-->
	<div id="tbl-view-options-menu" style='display:none;'>
		<div id="tbl-view-options">
			<span class='user-action'>View Options</span>
			<ul data-role='menu' data-orientation='vertical'>
				<li class='user-action' data-action='hide'
					data-bind="click:setViewOption">
					<span><input type="radio" value="hide" name="tblViewOption"
						data-bind="checked:viewOption,events:{change:applyViewOption}"/> Hide</span></li>
				<li class='user-action' data-action='gray'
					data-bind="click:setViewOption">
					<span><input type="radio" value="gray" name="tblViewOption"
						data-bind="checked:viewOption,events:{change:applyViewOption}"/> De-emphasize</span></li>
			</ul>
		</div>
	</div>
	<!-- view options ==> Same as the one in the main menu -->
	<div id="view-options-menu" class="" style='display:none;'>
		<div id="view-sub-menu" class="">
			<span class='user-action'>View Options</span>
			<ul data-role='menu' data-orientation='vertical'>
				<li class='user-action' data-action='hide'
					data-bind="click:showTblViewFilter">
					<span>Filtered Objects (Hide)</span></li>
				<li class='user-action' data-action='gray'
					data-bind="click:showTblViewFilter">
					<span><span>Filtered Objects (De-emphasize)</span></li>
				<li class='user-action' data-action='hide'
					data-bind="click:showIwlFeatures">
					<span >IWL(s) Objects (Hide)</span></li>
				<li class='user-action' data-action='gray'
					data-bind="click:showIwlFeatures">
					<span><span>IWL(s) Objects (De-emphasize)</span></li>
				<li class='user-action' 
					data-action='clear'
					data-bind="click:resetDefaultView">
					<span>Reset to default View</span></li>
			</ul>
		</div>
	</div>
	<!-- layers selections -->
	<div id="tbl-panel-filter" class="of-h">
		<div class="marg clear">
			<span>Filter by Layer:</span>
		</div>
		<div id="tbl-lyrs-wrap" class="clear marg">
			<input id="tbl-lyrs" class="tbl-input" 
				data-role="dropdownlist"
				data-auto-bind="true"
				data-bind="source:layers,value:selLayer,events:{change:populateTable,close:updateLayers}"
				data-option-label="Select a layer"
				data-text-field="name"
				data-value-field="value"/>
		</div>
		<div class="marg clear">
			<p id="tbl-layers-msg" class=""></p>
		</div>
		<div class="marg clear">
			<span>Filter by Area:</span>
		</div>
		<div id="tbl-lyrs-wrap" class="clear marg">
			<input id="tbl-sq-areas" class="tbl-input" 
				data-role="dropdownlist"
				data-bind="source:sqExtents,value:sqExtent,events:{change:enableFS}"
				data-text-field="name"
				data-value-field="value"/>
		</div>
		
		<!-- <div id="tbl-filter-areas" class="marg clear">
			<label class="marg">
				<input type="radio" id="tbl-map-extent" class="marg" 
					name="tbl-map-extent" 
					title="Result will only include features in current map extent" 
					value="mapview" 
					data-bind="checked:sqExtent,events:{change:enableFS}" /> Map View 
					
				<input type="radio" id="tbl-full-extent" class="marg" 
					name="tbl-map-extent" 
					title="Result will include all features in the layer"
					value="fullextent"
					data-bind="checked:sqExtent,events:{change:enableFS}" /> Full Extent
					
				<input type="radio" id="tbl-filter-shapes" class="marg" 
					name="tbl-map-extent" 
					title="Result will only include features in selected filter shapes"
					value="fs"
					data-bind="checked:sqExtent,events:{change:enableFS}" /> FS(s)
			</label>
		</div> -->
		<div class="marg clear of-h"
			data-bind="visible:fsActive">
			<div class="left padTop padBottom">
				<span>Selected Filter Shapes: </span>
			</div>
			<div class="right">
				<button id="tbl-get-fs" title="Refresh Filter Shapes"
					data-role="button"
					data-bind="click:refreshFS,enabled:fsActive">
					<i class="fa fa-refresh"></i></button>
				<button id="tbl-fs-zoom" title="Locate Selected Filter Shapes"
					data-role="button"
					data-bind="click:locateFS,enabled:fsActive">
					<i class="fa fa-crosshairs"></i></button>
				<button id="tbl-fs-clear" title="Clear selected Filter Shapes"
					data-role="button"
					data-bind="click:clearFS,enabled:fsActive">
					<i class="fa fa-times"></i></button>
			</div>
		</div>
		<div id="tbl-fs-list-wrap" class="marg clear"
			data-bind="visible:fsActive">
			<select id="tbl-fs-list" class="tbl-input" 
				data-role='multiselect' 
				data-placeholder='Select Filter Shapes'
				data-bind="source:filterShapes,value:selFS,enabled:fsActive"
				data-text-field="name"
				data-value-field="id"
				data-geom-field="geom">
				
			</select>
		</div>
		<!-- <div id="" class="marg clear">
			<button id="tbl-get-fs" class="k-button tbl-input3"
				data-role="button"
				data-bind="click:refreshFS,enabled:fsActive">
				<i class="fa fa-refresh"></i></button>
			<button id="tbl-fs-zoom" class="k-button tbl-input3"
				data-role="button"
				data-bind="click:locateFS,enabled:fsActive">
				<i class="fa fa-crosshairs"></i></button>
			<button id="tbl-fs-clear" class="k-button tbl-input3"
				data-role="button"
				data-bind="click:clearFS,enabled:fsActive">
				<i class="fa fa-times"></i></button>
		</div> -->
		<div class="marg clear"
			data-bind="visible:fsActive">
			<span>Area Filter Logic:</span>
		</div>
		<div class="tbl-box marg clear of-h"
			data-bind="visible:fsActive">
			<div class="marg clear">
				<div class="marg left w160">
					<span>Relationship to Filter Shapes:</span>
				</div>	
				<div class="marg left"
				data-bind="visible:fsActive">
					<!-- <input id="tbl-sp-rel" class="tbl-input" 
						data-role='dropdownlist'
						data-bind="source:queryRel,value:queryRelVal,events:{change:showQueryRelVal}"
						data-text-field="name"
						data-value-field="value" /> -->
					<label> 
						<input type="radio" class="marg" 
							name="tbl-fs-rel" 
							value="within" 
							data-bind="checked:queryRelVal"/> Within 
					</label>
					<br>
					<!-- <label> 
						<input type="radio" class="marg" 
							name="tbl-fs-rel" 
							value="intersect" 
							data-bind="checked:queryRelVal"/> Intersect 
					</label>
					<br> -->
					<label> 
						<input type="radio" class="marg" 
							name="tbl-fs-rel" 
							value="outside" 
							data-bind="checked:queryRelVal"/> Outside
					</label>
				</div>
			</div>
		
			<div class="marg clear"
				data-bind="visible:queryRelInput">
				<span>Relationship options:</span>
			</div>

			<div id="" class="marg clear"
				data-bind="visible:queryRelInput">
				<input type='number' id='tbl-sp-rel-val' 
					name='tbl-sp-rel-val' 
					class='k-textbox tbl-input3 left' 
					placeholder='10'
					data-bind="visible:queryRelInput,value:sqValue" />
					
				<input id="tbl-fs-unit" class="tbl-input2 right" 
					data-role='dropdownlist'
					data-bind="source:sqUnits,value:sqUnit,visible:queryRelInput,events:{change:showQueryRelVal}"
					data-text-field="name"
					data-value-field="value" />
			</div>

			<div class="marg clear">
				<div class="marg left w160">
					<span>Multiple Filter Shapes Logic:</span>
				</div>
				<div class="marg left">
					<label> 
						<input type="radio" id="tbl-interGQPs" class="marg" 
							name="tbl-gqps-rel" 
							value="AND" 
							data-bind="checked:fsLogic,enabled:fsActive"/> AND 
					</label>
					<br>
					<label>
						<input type="radio" id="tbl-unionGQPs" class="marg" 
							name="tbl-gqps-rel" 
							value="OR" 
							data-bind="checked:fsLogic,enabled:fsActive"/> OR 
					</label>
				</div>
			</div>
		</div>
		
		<!-- <div class="marg clear">
			<span>Load layer attributes</span>
		</div>
		<div class="marg clear">
			<button id="tbl-fs-filter" class="k-button tbl-input" 
				data-role="button" 
				data-bind="click:populateTable,enabled:lyrReady"><i class="fa fa-filter"></i> Display / Update Table</button>
		</div> -->
		<!-- <div class="marg clear">
			<span>Sync table content with layer</span>
		</div>
		<div class="marg clear">
			<button id="tbl-update-map" class="k-button tbl-input2"
				data-role="button"
				data-bind="click:updateLayer,enabled:tblReady"><i class="fa fa-filter"></i> Update Layer</button>
			<button id="tbl-clear-filter" class="k-button tbl-input2"
				data-role="button" 
				data-bind="click:clearUpdate,enabled:queryDef"><i class="fa fa-times"></i></span> Clear Updates</button>
		</div> -->
		<!-- <div class="marg clear">
			<button id="tbl-table-filter" class="k-button tbl-input"
				data-role="button" 
				data-bind="click:clearTableFilter,enabled:tblReady"><i class="fa fa-table"></i></span> Clear </button>
		</div> -->
		<!-- <div class="marg text-center clear">
			<label class="text-center">
				<input type="radio" id="tbl-feat-hide" name="tbl-feat-display" value="hide" checked="checked"/> Hide features
				<input type="radio" id="tbl-feat-gray" name="tbl-feat-display" value="gray"/> De-emphasize
			</label>
		</div> -->

		<!-- <div class="marg clear">
			<span>Incident Watch Lists</span>
		</div>
		<div class="marg clear">
			<button id="tbl-create-iwl" class="tbl-input2" 
				data-role="button"
				data-bind="click:createIWL,enabled:tblReady"><i class="fa fa-list"></i> Create</button>
			<button id="tbl-review-iwl" class="tbl-input2" 
				data-role="button"
				data-bind="click:manageIWLs"><i class="fa fa-th-list"></i> Manage</button>
		</div>
		<div class="marg clear">
			<button id="tbl-save-iwl" class="tbl-input2" 
				data-role="button"
				data-bind="click:saveIWL,enabled:iwl"><i class="fa fa-save"></i> Save</button>
			<button id="tbl-cancel-iwl" class="tbl-input2" 
				data-role="button"
				data-bind="click:cancelIWL,enabled:iwl"><i class="fa fa-times"></i> Cancel</button>
		</div>
		<div class="marg clear">
			<input type="text" class="k-textbox tbl-input" 
				placeholder="Enter IWL Name"
				data-bind="visible:iwl,value:iwlName"/>
		</div>
		<div class="marg clear">
			<input type="text" class="k-textbox tbl-input" 
				placeholder="Enter IWL Description"
				data-bind="visible:iwl,value:iwlDescription"/>
		</div>
		<div class="marg clear">
			<span id="" class="msg"
				data-bind="text:iwlMsg,style:{color:iwlMsgCol}"></span>
		</div> -->

		<div class="marg clear">
			<p id="tbl-area-msg" class=""></p>
		</div>
		<div class="marg clear of-h"
			data-bind="visible:tblReady">
			<div class="marg clear">
				<span>Filter by Attributes:</span>
			</div>
			<div class="marg clear">
				<button id="tbl-save-iwl" class="tbl-input" 
					data-role="button"
					data-bind="click:showAttrFilterOptions,enabled:enableAttrFilter">Show Filtering Options</button>
			</div>
			<div id='filters-div' class="marg clear" data-bind="visible:isAppliedFiltersVisible"><span>Applied Filters:</span></div>
			<div class="marg clear" data-bind="visible:isAttrFilterVisible">
				<span>Fields Filtering:</span>
				<input id="tbl-fields" class="tbl-input" 
					data-role="dropdownlist"
					data-bind="source:attrFields,value:selAttrField,events:{change:changeDropdownFields}"
					data-text-field="title"
					data-value-field="field"/>
			</div>
			<div id='tbl-filter-options' class="marg clear" data-bind="visible:isAttrFilterVisible">
			</div>
			<div id="string-filter-div" class="marg clear" data-bind="visible:isStringFilterGUIVisible">
				<div class="margBottom clear">
					<select id="string-multiselect"
						data-role="multiselect"
						data-bind="source:stringMultiselectValues, events:{change: changeStringMultiselect}"
						data-text-field="title"
						data-value-field="value">
					</select>
				</div>
			</div>
			<div id="number-filter-div" class="marg clear" data-bind="visible:isNumberFilterGUIVisible">
				<div class="margBottom clear">
					<select id="num-select-ops1"
						data-role="dropdownlist"
						data-bind="source:numOptions"
						data-text-field="title"
						data-value-field="value">
					</select>
					<input id="num-input1" class="w100 margLeft" 
						data-role="numerictextbox"/>
				</div>
                <div class="margBottom clear">
                	<select id="num-select-and"
                		data-role="dropdownlist"
						data-bind="source:andOrOptions,events:{change:changeNumberSelectAnd}"
						data-text-field="title"
						data-value-field="value">
                	</select>
                </div>
                <div class="margBottom clear" data-bind="visible:isNumberSecondFilterVisible">
                	<select id="num-select-ops2"
						data-role="dropdownlist"
						data-bind="source:numOptions"
						data-text-field="title"
						data-value-field="value">
					</select>
					<input id="num-input2" class="w100 margLeft"
					data-role="numerictextbox"/>
                </div>
                <div class="margBottom clear">
                	<button id="num-button-apply" data-role="button" data-bind="events:{click:clickNumButtonApply}">Apply</button>
                </div>
                <div id="num-dialogOp1" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please select an option for the filter</p>
					<button id="num-okOp1" data-role="button">OK</button>
				</div>
				<div id="num-dialogOp2" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please select an option for the filter</p>
					<button id="num-okOp2" data-role="button">OK</button>
				</div>
				<div id="num-dialogIn1" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please set a value for the filter</p>
					<button id="num-okIn1" data-role="button">OK</button>
				</div>
				<div id="num-dialogIn2" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please set a value for the filter</p>
					<button id="num-okIn2" data-role="button">OK</button>
				</div>
			</div>
			<div id="date-filter-div" class="marg clear" data-bind="visible:isDateFilterGUIVisible">
				<div class="margBottom clear">
					<select id="date-select-ops1" class="w130" 
						data-role="dropdownlist"
						data-bind="source:dateOptions"
						data-text-field="title"
						data-value-field="value">
					</select>
					<input id="date-input1" class="margLeft w180" 
						data-role="datetimepicker"
						data-format="{0:yyyy/MM/dd HH:mm}"/>
				</div>
                <div class="margBottom clear">
                	<select id="date-select-and" class="w130" 
                		data-role="dropdownlist"
						data-bind="source:andOrOptions,events:{change:changeDateSelectAnd}"
						data-text-field="title"
						data-value-field="value">
                	</select>
                </div>
                <div class="margBottom clear" data-bind="visible:isDateSecondFilterVisible">
                	<select id="date-select-ops2" class="w130" 
						data-role="dropdownlist"
						data-bind="source:dateOptions"
						data-text-field="title"
						data-value-field="value">
					</select>
					<input id="date-input2" class="margLeft w180"
						data-role="datetimepicker"
						data-format="{0:yyyy/MM/dd HH:mm}"/>
                </div>
                <div class="margBottom clear">
                	<button id="date-button-apply" data-role="button" data-bind="events:{click:clickDateButtonApply}">Apply</button>
                </div>
                <div id="date-dialogOp1" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please select an option for the filter</p>
					<button id="date-okOp1" data-role="button">OK</button>
				</div>
				<div id="date-dialogOp2" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please select an option for the filter</p>
					<button id="date-okOp2" data-role="button">OK</button>
				</div>
				<div id="date-dialogIn1" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please set a value for the filter</p>
					<button id="date-okIn1" data-role="button">OK</button>
				</div>
				<div id="date-dialogIn2" class="txtCenter"
					data-role="window"
					data-title="Message"
					data-modal="true"
					data-minWidth="250px"
					data-resizable="false"
					data-visible="false">
					<p>Please set a value for the filter</p>
					<button id="date-okIn2" data-role="button">OK</button>
				</div>
			</div>

		</div>
		<!-- filter by Filter -->
		<!-- <div class="marg clear"
			data-bind="visible:tblReady">
			<span>Apply Saved Filter</span>
		</div>
		<div class="marg clear"
			data-bind="visible:tblReady">
			<input id="tbl-sq-areas" class="tbl-input" 
				data-role="dropdownlist"
				data-bind="source:tblFilters,value:selTblFilter,events:{change:applySavedFilter}"
				data-text-field="name"
				data-value-field="value"/>
		</div> -->
		<div class="marg clear"
			data-bind="visible:tblReady">
			<p id="tbl-filters-msg" class=""></p>
		</div>
		<!-- filter by IWL -->
		<div class="marg clear"
			data-bind="visible:tblReady">
			<span>Apply Saved IWL</span>
		</div>
		<div class="marg clear"
			data-bind="visible:tblReady">
			<input id="tbl-saved-iwl" class="tbl-input" 
				data-role="dropdownlist"
				data-bind="source:iwls,value:selIWL,events:{change:applyIWLFilter}"
				data-text-field="name"
				data-value-field="value"/>
		</div>
		<div class="marg clear"
			data-bind="visible:tblReady">
			<p id="tbl-iwl-msg" class=""></p>
		</div>
	</div>
</div>
<!--table container -->		
<div id="tablewrapper" class="of-h">
	<div class="k-header of-h clear">
		<div class="left">
			<button id="tbl-export" title="Download Table View Content" 
				data-role="button" 
				data-bind="click:downloadTable,enabled:tblReady">
				<span class=""><i class="fa fa-download"></i> </span> 
				<span id="tbl-name" class="" 
					data-bind="text:selLayerName"></span>
			</button>
			<button id="tbl-open-profile" title="Open selected feature's profile"
				data-role="button"
				data-bind="enabled:isFeatSelected,click:openFeatureProfile">
				<span><i class="fa fa-file-powerpoint-o"></i></span>
			</button>
			<button id="tbl-fly-to-feature" title="Fly to selected feature's location"
				data-role="button"
				data-bind="enabled:isFeatSelected,click:flyToFeature">
				<span><i class="fa fa-crosshairs"></i></span>
			</button>
			<button id="tbl-process" 
				data-role="button"
				data-bind="visible:tblLoading">
				<span><i class="fa fa-spinner fa-spin"></i></span>
			</button>
			<button id="tbl-process-msg" 
				data-role="button">
				<span data-bind="text:tblMsg"></span>
			</button>
		</div>
		<div class="right" >
			<span class="k-textbox tbl-search">
				<input type="text" id="tbl-search" 
					placeholder="Search" 
					data-bind="value:tblSearch,enabled:tblReady,events:{keyup:keyUpSearchTable}" />
			</span>
			<button id="tbl-search-button" title="Search"
				data-role="button"
				data-bind="click:searchTable,enabled:tblReady">
				<span><i class="k-icon k-i-search"></i></span>
			</button>
		</div>
	</div>
	<div id="table-wrap" class="of-h">
		<div id="table"> </div>
	</div>
</div>
