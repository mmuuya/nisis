<?php
/* PHP code for retrieving a NOTAM by ID from the database*/

	//Retrieve the confirgurations and global stuff, or fail if we can't.
	if ((include '../../../handler.php') === FALSE){
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}

	if(!isset($_SESSION['userid'])){
		return false;
	}

	$userid = $_SESSION['userid'];
	$notamid = $_REQUEST['notam_id'];
	$action = $_REQUEST['action'];
	// $user_group =
	
	$result = array();
	//TODO: Add a filter to ensure returned NOTAM information already belongs 
	//to user's assigned group to prevent access fraud.
	

	function getBody($notamid, $return) {
		global $db;
		// COMMMENTED OUT BY KOFI BECAUSE OF REFERENCE_VOR COLUMN OBSOLETE
		// $notam_record_sql = 'select NOTAM_ID, NOTAM_TYPE, NOTAM_NAME, STATUS, USER_ID, 
		// 	ARTCC_ID, ARTCC_NAME, EFFECTIVE_DATE_UTC, EXPIRATION_DATE_UTC, 
		// 	CUSTOM_TEXT, NOTAM_NAME || \' - \' || (select type from tfruser.lookup_tfr_types where id = NOTAM_TYPE) DISPLAY_NAME,
		// 	REFERENCE_VOR
  		//from tfruser.notam_body where notam_id = :notamid ';

        $notam_record_sql = 'select NOTAM_ID, NOTAM_TYPE, NOTAM_NAME, STATUS, USER_ID, 
			ARTCC_ID, ARTCC_NAME, EFFECTIVE_DATE_UTC, INCLUDE_FRD, INCLUDE_LOCAL_TIME, EXPIRATION_DATE_UTC, CUSTOM_TEXT, NOTAM_NAME || \' - \' || (select type from tfruser.lookup_tfr_types where id = NOTAM_TYPE) DISPLAY_NAME
            from tfruser.notam_body where notam_id = :notamid ';

		//and user_group = :usergroup';


		// error_log($notam_record_sql);

		$notam_record_parsed = oci_parse($db, $notam_record_sql);
		oci_bind_by_name($notam_record_parsed, ":notamid", $notamid);
		//oci_bind_by_name($notam_record_parsed, ":usergroup", $user_group);

		oci_execute($notam_record_parsed);
		oci_fetch_all($notam_record_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

		// error_log(var_export($result, true));

		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
	};

	function getLocations($notamid, $return) {
		global $db;

		$notam_locations_sql = 'select STATE_ABBREV, STATE_TERRITORY, CITY, INSERT_ORDER
			from tfruser.notam_locations where notam_id = :notamid order by INSERT_ORDER asc';

		$notam_locations_parsed = oci_parse($db, $notam_locations_sql);
		oci_bind_by_name($notam_locations_parsed, ":notamid", $notamid);

		oci_execute($notam_locations_parsed);
		oci_fetch_all($notam_locations_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

		if ( isset($return) && $return == true ) {
			return $result;
		} else {
			exit(json_encode($result));
		}
	};

	function getOperatingInstructions($notamid, $return) {
		global $db;

		$notam_inst_sql = 'select NOTAM_ID, INSTRUCTION_ID, INSTRUCTION_TEXT from tfruser.notam_instruction where notam_id = :notamid order by INSTRUCTION_ID asc';

		$notam_inst_parsed = oci_parse($db, $notam_inst_sql);
		oci_bind_by_name($notam_inst_parsed, ":notamid", $notamid);

		oci_execute($notam_inst_parsed);
		oci_fetch_all($notam_inst_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
	
		if ( isset($return) && $return == true ) {
			return $result;
		} else {
			exit(json_encode(Array('oprInsts' => $result)));
		}
	};

	function getArea($notamid, $return) {
		global $db;

		$notam_areas_sql = 'select SHAPE_ID, AREA_ID, INST_ID, AREA_NAME, DEFINITION_OR_NAME,
			ALLOW_AUTHORIZATION, UTC_TIME_ZONE, SCHEDULED, MONDAY, TUESDAY, WEDNESDAY,
			THURSDAY, FRIDAY, SATURDAY, SUNDAY from tfruser.notam_area where notam_id = :notamid order by 
			AREA_ID asc';

		$notam_areas_parsed = oci_parse($db, $notam_areas_sql);
		oci_bind_by_name($notam_areas_parsed, ":notamid", $notamid);

		oci_execute($notam_areas_parsed);
		oci_fetch_all($notam_areas_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
		// print_r(json_encode($result));

		$dates = getAreaDateTimes($notamid, true);
		$shapes = getAreaShapes($notamid, true);
		//$instructions = getAreaInstructions($notamid, true);

		if (isset($return) && $return == true) {
			return $result;
		} else {
			//exit(json_encode($result));
			//exit(json_encode(Array('areas' => $result, 'dates' => $dates, 'shapes' => $shapes, 'inst' => $instructions)));
			exit(json_encode(Array('areas' => $result, 'dates' => $dates, 'shapes' => $shapes)));
		}	
	};

	function getAreaDateTimes($notamid, $return){
		global $db;		

		// $notam_dt_sql = 'select NOTAM_ID, AREA_ID, DATE_TIME_ID, 
		// 	to_char(EFFECTIVE_DATE_TIME, \'MM/DD/YYYY HH:MI:SS AM\') EFFECTIVE_DATE_TIME, 
		// 	to_char(EXPIRATION_DATE_TIME, \'MM/DD/YYYY HH:MI:SS AM\') EXPIRATION_DATE_TIME
		// 	from tfruser.notam_tfr_date_time where notam_id = :notamid order by NOTAM_ID, AREA_ID, EFFECTIVE_DATE_TIME, 
		// 	EXPIRATION_DATE_TIME asc';
		$notam_dt_sql = 'select NOTAM_ID, AREA_ID, DATE_TIME_ID, 
			EFFECTIVE_DATE_TIME, EXPIRATION_DATE_TIME
			from tfruser.notam_tfr_date_time where notam_id = :notamid order by NOTAM_ID, AREA_ID, EFFECTIVE_DATE_TIME, 
			EXPIRATION_DATE_TIME asc';

		$notam_dt_parsed = oci_parse($db, $notam_dt_sql);
		oci_bind_by_name($notam_dt_parsed, ":notamid", $notamid);

		oci_execute($notam_dt_parsed);
		oci_fetch_all($notam_dt_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
			
		// print_r($result);
		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
	}
	
	function getAreaShapes($notamid, $return){
		global $db;	

		$notam_dt_sql = 'select a.NOTAM_ID, a.AREA_ID, a.SHAPE_ID, a.REFERENCE_VOR, b.NAME
			from tfruser.notam_area_shapes a, NISIS_GEODATA.TFR_POLYGONS b where 
			notam_id = :notamid and a.SHAPE_ID=b.OBJECTID order by NOTAM_ID, AREA_ID asc';

		$notam_dt_parsed = oci_parse($db, $notam_dt_sql);
		oci_bind_by_name($notam_dt_parsed, ":notamid", $notamid);

		oci_execute($notam_dt_parsed);
		oci_fetch_all($notam_dt_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
			
		// print_r($result);
		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
	}
	
	// function getAreaInstructions($notamid, $return){
	// 	global $db;	

	// 	$notam_inst_sql = 'select NOTAM_ID, AREA_ID, INSTRUCTION_ID, INSTRUCTION_TEXT
	// 		from tfruser.notam_area_inst where 
	// 		notam_id = :notamid order by NOTAM_ID, AREA_ID, INSTRUCTION_ID asc';

	// 	$notam_inst_parsed = oci_parse($db, $notam_inst_sql);
	// 	oci_bind_by_name($notam_inst_parsed, ":notamid", $notamid);

	// 	oci_execute($notam_inst_parsed);
	// 	oci_fetch_all($notam_inst_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
			
	// 	// print_r($result);
	// 	if (isset($return) && $return == true) {
	// 		return $result;
	// 	} else {
	// 		exit(json_encode($result));
	// 	}
	// }

	function getExtra($notamid, $return){
		global $db;

		//first; retrieve notam type from NOTAMID

		$notam_type_sql = 'select a.NOTAM_TYPE, b.NAME NAME from tfruser.notam_body a join tfruser.lookup_tfr_types b
			on b.id = a.notam_type where notam_id = :notamid';

		$notam_type_parsed = oci_parse($db, $notam_type_sql);
		oci_bind_by_name($notam_type_parsed, ":notamid", $notamid);

		oci_execute($notam_type_parsed);
		oci_fetch($notam_type_parsed);

		$notam_type = oci_result($notam_type_parsed, 'NAME');
		switch(strtoupper($notam_type)) {
			case 'HAZARDS':
			$notam_extra_sql = 'select NOTAM_ID, POINT_OF_CONTACT_NAME, POC_ORGANIZATION, POC_PHONES, 
				POC_FREQUENCIES, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, REASON, MODIFY_ID, 
				MODIFY_REASON, 1 SHOW_POC, 1 SHOW_COORD, 0 SHOW_CNTRL from tfruser.notam_type_hazards where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		case 'VIP':
			$notam_extra_sql = 'select NOTAM_ID, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, MODIFY_ID, MODIFY_REASON, 0 SHOW_POC, 1 SHOW_COORD, 0 SHOW_CNTRL 
				from tfruser.notam_type_vip where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		case 'AIRSHOW':
			$notam_extra_sql = 'select NOTAM_ID, POINT_OF_CONTACT_NAME, POC_ORGANIZATION, POC_PHONES, 
				POC_FREQUENCIES, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, CONTROLLING_FACILITY_NAME,
				CNTRL_FAC_TYPE, CNTRL_FAC_FREQ, REASON, MODIFY_ID, 
				MODIFY_REASON, 1 SHOW_POC, 1 SHOW_COORD, 1 SHOW_CNTRL from tfruser.notam_type_airshow where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		case 'SECURITY':
			$notam_extra_sql = 'select NOTAM_ID, POINT_OF_CONTACT_NAME, POC_ORGANIZATION, POC_PHONES, 
				POC_FREQUENCIES, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, REASON, MODIFY_ID, 
				MODIFY_REASON, 1 SHOW_POC, 1 SHOW_COORD, 0 SHOW_CNTRL from tfruser.notam_type_security where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		case 'SPACE OPERATIONS':
			$notam_extra_sql = 'select NOTAM_ID, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, REASON, MODIFY_ID, 
				MODIFY_REASON, 0 SHOW_POC, 1 SHOW_COORD, 0 SHOW_CNTRL from tfruser.notam_type_space_operations where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		case 'HAWAII':
			$notam_extra_sql = 'select NOTAM_ID, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, MODIFY_ID, MODIFY_REASON, 0 SHOW_POC, 0 SHOW_COORD, 0 SHOW_CNTRL 
				from tfruser.notam_type_hawaii where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		case 'EMERGENCY':
			$notam_extra_sql = 'select NOTAM_ID, COORDINATING_FACILITY_ID, COORD_FAC_NAME, COORD_FAC_TYPE, 
				COORD_FAC_TYPE_ID, COORD_FAC_PHONES, COORD_FAC_FREQUENCIES, MODIFY_ID, MODIFY_REASON, 0 SHOW_POC, 0 SHOW_COORD, 0 SHOW_CNTRL
				from tfruser.notam_type_emergency_traffic where notam_id = :notamid';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);
			break;
		default:
			error_log("NOTAM Type not found: " . strtoupper($type_name) . "| It may need to be added to tfr-load.php");
			$notam_extra_sql = 'select null from dual';
			$notam_extra_parsed = oci_parse($db, $notam_extra_sql);

		}
		oci_bind_by_name($notam_extra_parsed, ':notamid', $notamid);

		oci_execute($notam_extra_parsed);
		oci_fetch_all($notam_extra_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
	};

	function getCustomText($notamid, $return){
		global $db;
		$notam_text_sql = 'select TEXT from tfruser.notam_text where notam_id = :notamid';

		$notam_text_parsed = oci_parse($db, $notam_text_sql);
		oci_bind_by_name($notam_text_parsed, ":notamid", $notamid);

		oci_execute($notam_text_parsed);
		oci_fetch_all($notam_text_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
		
	};
	function getIncludeLocalTime($notamid, $return){
		global $db;
		$notam_LOCAL_TIME_sql = 'select INCLUDE_LOCAL_TIME from tfruser.notam_body where notam_id = :notamid';

		$notam_LOCAL_TIME_parsed = oci_parse($db, $notam_LOCAL_TIME_sql);
		oci_bind_by_name($notam_LOCAL_TIME_parsed, ":notamid", $notamid);

		oci_execute($notam_LOCAL_TIME_parsed);
		oci_fetch_all($notam_LOCAL_TIME_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
		
	};


	function getIncludeFRD($notamid, $return){
		global $db;
		$notam_FRD_sql = 'select INCLUDE_FRD from tfruser.notam_body where notam_id = :notamid';

		$notam_FRD_parsed = oci_parse($db, $notam_FRD_sql);
		oci_bind_by_name($notam_FRD_parsed, ":notamid", $notamid);

		oci_execute($notam_FRD_parsed);
		oci_fetch_all($notam_FRD_parsed, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

		if (isset($return) && $return == true) {
			return $result;
		} else {
			exit(json_encode($result));
		}
		
	};


	//retreive all tfr data with 1 request
	function getNotamContent($notamid) {

		$body = getBody($notamid, true);
		$locations = getLocations($notamid, true);
		$operatingInstructions = getOperatingInstructions($notamid, true);
		$areaDT = getAreaDateTimes($notamid, true);
		$area = getArea($notamid, true);
		$extra = getExtra($notamid, true);
		$customText = getCustomText($notamid, true);
		$content = getNotamContent($notamid, true);
		$includeLocalTime = getIncludeLocalTime($notamid, true);
		$includeFRD = getIncludeFRD($notamid, true);


		exit(json_encode(Array( 
			'body' => $body,
			'locations' => $locations,
			'operatingInstructions' => $operatingInstructions,
			'areaDT' => $areaDT,
			'area' => $area,
			'extra' => $extra,
			'customText' => $customText,
            'content' => $content,
			'includeLocalTime' => $includeLocalTime,			
			'includeFRD' => $includeFRD
		)));
	};

	switch($action){
		case 'loadBody':
			getBody($notamid, false);
			break;
		case 'loadLocations':
			getLocations($notamid, false);
			break;
		case 'loadOprInsts':
			getOperatingInstructions($notamid, false);
			break;
		case 'loadAreaDateTimes':
			getAreaDateTimes($notamid, false);
			break;
		case 'loadAreas':
			getArea($notamid, false);
			break;
		case 'loadExtra':
			getExtra($notamid, false);
			break;
		case 'loadText':
			getCustomText($notamid, false);
			break;
		//FB - JIC	
		case 'includeFRD':
			getIncludeFRD($notamid, true);
			break;
		case 'includeLocalTime':
			getIncludeLocalTime($notamid, true);
			break;
		//END JIC						
		case 'all':
			getNotamContent($notamid, true);
			break;
		default:
			error_log("Invalid action '". $action . "'' passed by " . $userid);
			exit(json_encode(Array('return'=> 'failure', 
				'message' => 'Invalid request. App could not identify.')));
			break;
	}

	//getBody($notamid);

?>



