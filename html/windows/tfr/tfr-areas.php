<div class="tfr-area of-h" data-tab="">
	<div class="left of-h">
		<div class="w-305 clr of-h">
			<div class="clr of-h">
				<div class="marg clr">Area Name</div>
				<div class="marg clr">
					<input type="text" class="area-name w-250 k-textbox" 						
						placeholder="Enter area name" 
						data-value-update="keyup" 						
						data-bind="events:{keyup:updateAreaName}" 
						value="" 
						/>						
				</div>
			</div>
			<div class="clr of-h">
				<!-- Shapes -->
				<div class="marg clr">Shape (s)</div>
				<!-- Shapes grid -->
				<div class="marg clr shapesGrid"></div>
				<!-- Shapes manipulation -->
				<div class="marg clr shapesButtons">
					<span data-bind="events:{click:addNewShape}" class="w-100 k-button addnewshape">Add</span>
					<span data-bind="events:{click:deleteShape}" class="w-100 k-button">Delete</span>
				</div>				
						
			</div>
		</div>
	</div>
	<div class="left of-h">
		<div class="marg clr of-h">
			<div class="marg clr">Time Zone</div>
			<div class="marg clr">
				<label><input type="radio" class="eff-dtz" 
					value="utc"
					name="eff-dtz-x" 
					data-bind="events:{change:setAreaDTOption}" /> UTC</label>

				<label class="m-l15"><input type="radio" class="eff-dtz" 
					checked="true" 
					value="local"
					name="eff-dtz-x" 
					data-bind="events:{change:setAreaDTOption}" /> Local (Affected location)</label>
			</div>
		</div>
		<div class="marg clr of-h sched-block">
			<div class="clr of-h">
				<div class="marg clr">
					<label><input type="radio" class="eff-dtg" checked="true" name="eff-dtg-x"
						value="non-schedule" 
						data-option="non-schedule"
						data-bind="events:{change:updateEffDTG}" /> Non-Scheduled TFR (YYYY/MM/DD HHMM)</label>
				</div>
				<div class="tfr-dates non-sch marg m-t10 clr">
					<div class="marg clr">
						<div class="txt-b w-200 m-b left">Effective Date/Time</div>
						<div class="txt-b w-200 m-b left">Expiration Date/Time</div>
					</div>
					<div class="area-dtg-input marg clr of-h"
						data-dtgroup="dt1">
						<input class="area-dtg area-eff-dtg"
							data-schedule="non-schedule"
							data-dtg="eff" 
							data-role="datetimepicker"
							data-format="yyyy/MM/dd HH:mm"
							data-time-format="HH:mm"
							data-enabled="true" 
							data-bind="events:{change:setAreaDates}" />
						<input class="area-dtg area-exp-dtg" 
							data-schedule="non-schedule"
							data-dtg="exp"
							data-role="datetimepicker"
							data-format="yyyy/MM/dd HH:mm"
							data-time-format="HH:mm"
							data-enabled="true"
							data-bind="events:{change:setAreaDates}" />
						<i class="pointer marg fa fa-plus"
							data-schedule="non-schedule"
							title="Add"
							data-bind="click:addDateTimeSet"></i>
						<i class="pointer marg fa fa-times" 
							data-schedule="non-schedule"
							title="Remove"
							data-bind="click:removeDateTimeSet"></i>
					</div>
				</div>
			</div>
			<div class="m-t15 clr of-h">
				<div class="marg clr">
					<label><input type="radio" class="eff-dtg" name="eff-dtg-x"
						value="schedule"  
						data-option="schedule"
						data-bind="events:{change:updateEffDTG}" /> Scheduled TFR (YYYY/MM/DD HHMM)</label>
				</div>
				<div class="marg m-t10 clr of-h day-checks">
					<div class="clr">
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day sch-daily" 
								data-bind="events:{change:setDailySchedule}" /> Daily</label>
						</div>
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="monday"
								data-bind="events:{change:setScheduledDays}" /> Monday</label>
						</div>
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="tuesday"
								data-bind="events:{change:setScheduledDays}" /> Tuesday</label>
						</div>
						<div class="w-85 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="wednesday"
								data-bind="events:{change:setScheduledDays}" /> Wednesday</label>
						</div>
					</div>
					<div class="clr">
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="thursday"
								data-bind="events:{change:setScheduledDays}" /> Thursday</label>
						</div>
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="friday"
								data-bind="events:{change:setScheduledDays}" /> Friday</label>
						</div>
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="saturday"
								data-bind="events:{change:setScheduledDays}" /> Saturday</label>
						</div>
						<div class="w-80 marg left">
							<label><input type="checkbox" disabled="true" class="sch-day" 
								value="sunday"
								data-bind="events:{change:setScheduledDays}" /> Sunday</label>
						</div>
					</div>
				</div>
				<div class="tfr-dates sch marg m-t10 clr">
					<div class="marg clr">
						<div class="txt-b w-200 m-b left">Effective Date/Time</div>
						<div class="txt-b w-200 m-b left">Expiration Date/Time</div>
					</div>
					<!--data-time-format="HH:mm"
						data-format="MM/dd/yyyy HH:mm"
						data-parse-formats="['MMMM yyyy', 'HH:mm']"
						['MM/dd/yyyy hh:mmtt', 'MM/dd/yyyy HH:mm', 'MM/dd/yyyy', 'HH:mm']
						data-bind="events:{change:setAreaDates}" -->
					<div class="area-dtg-input marg clr of-h" data-dtgroup="dt1">
						<input class="area-dtg area-eff-dtg" 
							data-schedule="schedule"
							data-dtg="eff" 
							data-enable="false"
							data-role="datetimepicker"
							data-format="yyyy/MM/dd HH:mm"
							data-time-format="HH:mm"
							data-bind="events:{change:setAreaDates}" />

						<input class="area-dtg area-exp-dtg" 
							data-schedule="schedule"
							data-dtg="exp" 
							data-enable="false"
							data-role="datetimepicker"
							data-format="yyyy/MM/dd HH:mm"
							data-time-format="HH:mm"
							data-bind="events:{change:setAreaDates}" />

						<i class="pointer marg fa fa-plus"
							data-schedule="schedule"
							title="Add"
							data-bind="click:addDateTimeSet"></i>
						<i class="pointer marg fa fa-times" 
							data-schedule="schedule"
							title="Remove"
							data-bind="click:removeDateTimeSet"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>