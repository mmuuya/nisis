<?php
//
//Retrieve the confirgurations and global stuff, or fail if we can't.
	if ((include '../../../handler.php') === FALSE){
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}

	if(!isset($_SESSION['userid'])){
		return false;
	}


global $db;

$notamid = $_REQUEST["notamid"];
$userid = $_REQUEST["userid"];
$result ="";

error_log($notamid);
error_log($userid );
						
$query = "begin :result := NISIS_GEODATA.pkg_tfr_util.copy_tfr(:notamid, :userid); end;";

error_log($query);

$parsed = oci_parse($db, $query);
oci_bind_by_name($parsed, ":notamid", $notamid);
oci_bind_by_name($parsed, ":userid", $userid);

oci_bind_by_name($parsed, ":result", $result, 100);

error_log("executing!!!!....");
if(!oci_execute($parsed)){
    $err = oci_error($parsed);
    $errStr = $err['message'];
    kill(array('result' => 'Malformed query in copy_tfr api', 'error' => $errStr));
}
else {            
	error_log($result);
    kill(array('result' => $result), FALSE);    
}

?>