<!--<form id="tfr-data-form" class="tfr-data-form">-->

<div id="tfr-form" class="tfr-form"
	data-role="tabstrip"
	data-bind="visible:tfrShowForm,events:{activate:updateTfrTab}">
	<ul>
		<li class="k-state-active">Quick Entry</li>
		<li>Notam Text</li>
		<li>Airports</li>
	</ul>
	<div class="tfr-entry">
		<!-- name && type -->
		<div class="clr of-h">
			<div class="left of-h">
				<div class="txt-b w-150 marg clr">Name</div>
				<div class="marg m-l15 left">
					<input type="text" class="w-300 k-textbox"
						data-value-update="keyup"
						data-bind="value:tfrName,events:{keyup:updateTfrTitle}" />
				</div>
			</div>
			<div class="left of-h">
				<div class="txt-b w-150 marg clr">Type</div>
				<div class="marg m-l15 left">
					<select class="w-300 typeDropDown"
						id="typeDropDown"
						data-role="dropdownlist"
						data-text-field="TYPE"
	       				data-value-field="ID"
						data-bind="source:tfrTypes,value:tfrTypeId,events:{change:updateTfrType}">
					</select>
				</div>
			</div>
		</div>
		<!-- location -->
		<div class="m-t15 clr of-h">
			<div class="txt-b marg clr">Affected Location</div>
			<div class="marg clr">
				<!-- ARTCCs -->
				<div class="left of-h">
					<div class="marg left">
						<div class="w-150 marg clr">ARTCC</div>
						<div class="marg clr">
							<select class="tfr-artcc-id w-300"
								data-artcc="tfr-artcc-name"
								data-role="dropdownlist"
								data-option-label="Select ARTCC ID"
								data-text-field="ARTCC"
                   				data-value-field="ARTCC"
								data-bind="source:tfrArtccs,value:artccId,events:{change:updateArtccName}">
							</select>
						</div>
					</div>
					<div class="marg left">
						<div class="w-150 marg clr">Common Name</div>
						<div class="marg clr">
							<select class="tfr-artcc-name w-300"
								data-artcc="tfr-artcc-id"
								data-role="dropdownlist"
								data-option-label="Select ARTCC Name"
								data-text-field="NAME"
                   				data-value-field="NAME"
								data-bind="source:tfrArtccs,value:artccName,events:{change:updateArtccId}">

							</select>
						</div>
					</div>
				</div>
				<!-- Time zones -->
				<div class="left of-h">
					<div class="marg left">
						<div class="w-150 marg clr">Effective Date (Time Zone)</div>
						<div class="marg clr">
							<select class="tfr-eff-tz w-300" id="effectiveTimeZone"
								data-timezone="tfr-exp-tz"
								data-role="dropdownlist"
								data-text-field="NAME"
                   				data-value-field="ID"
								data-bind="source:tfrTimeZones,value:effTimeZone,events: {change:effTimeZoneOnChange}">
							</select>
						</div>
					</div>
					<div class="marg left">
						<div class="w-150 marg clr">Expiration Date (Time Zone)</div>
						<div class="marg clr">
							<select class="tfr-exp-tz w-300" id="expirationTimeZone"
								data-timezone="tfr-eff-tz"
								data-role="dropdownlist"
								data-text-field="NAME"
                   				data-value-field="ID"
								data-bind="source:tfrTimeZones,value:expTimeZone,events: {change:expTimeZoneOnChange}">
							</select>
						</div>
					</div>
				</div>
				<!-- City & States -->
				<div class="tfr-locs left of-h">
					<div class="marg clr of-h">
						<div class="w-300 marg m-r15 left">State / Territory</div>
						<div class="w-300 marg left">City</div>
					</div>
					<div class="tfr-loc marg clr of-h" data-location="loc0">
						<!-- states options -->
						<select class="tfr-loc-state w-300 marg m-r10"
							data-location="loc0"
							data-role="dropdownlist"
							data-auto-bind="true"
							data-filter="contains"
							data-option-label="Select a state"
							data-text-field="NAME"
               				data-value-field="ABBR"
							data-bind="source:tfrStates,value:locations.loc0.state,events:{change:updateCities}">

						</select>
						<!-- city options -->
						<select class="tfr-loc-city w-300 marg"
							data-location="loc0"
							data-role="combobox"
							data-auto-bind="true"
							data-filter="contains"
							data-option-label="Select a city"
							data-text-field="NAME"
               				data-value-field="NAME"
							data-bind="source:tfrCities,enabled:locations.loc0.cityEnabled,events:{change:updateLocData}">

						</select>
						<!-- remove location -->
						<i class="pointer marg fa fa-times"
							title="Remove"
							data-location="loc0"
							data-bind="click:removeNewLocation,visible:rmLocVisible"></i>
					</div>
				</div>
				<!-- Add new location: City & States -->
				<div class="marg clr">
					<button class="marg" title="Add a location"
						data-role="button"
						data-bind="click:addNewLocation">
							<i class="fa fa-plus"></i> Add</button>
				</div>
			</div>
		</div>
		<!-- Instructions & Representation -->
		<div id="oprInst" class="m-t15 clr of-h">
			<div class="marg clr">
				<!-- Operating Instructions -->
				<div class="left of-h">
					<div class="txt-b marg clr">Operating Instructions</div>
					<div class="clr of-h">
						<div class="marg clr">
							<!--<div class="marg m-t10 clr">
								<label><input type="radio" class="use-inst ops-inst" name="ops-inst-x"
									data-action="use"
									data-bind="enabled:useInstEnabled,events:{change:updateOpsInst}" />
										Use the same instruction as:</label>
							</div>

							<div class="marg clr">
								<select class="use-area-insts w-300"
									data-role="dropdownlist"
									data-option-label="Select source area"
									data-auto-bind="false"
									data-enabled="false"
									data-text-field="name"
		               				data-value-field="value"
									data-bind="source:tfrAreas, events:{change:applyCopiedInsts}">

								</select>
							</div>-->

							<div class="marg m-t10 clr">
								<label><input type="radio" class="add-inst ops-inst" name="ops-inst-x" checked="true"
									data-action="add"
									data-bind="events:{change:updateOpsInst}" /> Create and edit area instructions:</label>
							</div>

							<!-- <div class="marg clr">
								<label>Default: </label>
							</div> -->

							<div class="marg clr">
								<select class="area-insts w-250"
									data-role="dropdownlist"
									data-text-field="NAME"
		               				data-value-field="ID"
									data-bind="source:areaInstructions">
								</select>
								<button class="area-apply-insts w-50"
									data-role="button"
									data-bind="click:applyAreaInsts">Apply</button>
							</div>
							<div class="marg clr">
								<button class="area-inst-add area-inst w-60"
									data-role="button"
									data-bind="click:addAreaInst">Add</button>
								<button class="area-inst-del area-inst w-60"
									data-role="button"
									data-enable="false"
									data-bind="click:delAreaInst">Delete</button>
								<button class="area-inst-up area-inst w-60"
									data-role="button"
									data-enable="false"
									data-bind="click:upAreaInst">Up</button>
								<button class="area-inst-down area-inst w-60"
									data-role="button"
									data-enable="false"
									data-bind="click:downAreaInst">Down</button>
							</div>
							<div class="area-insts-list b-sq w-300 h-150 marg clr of-a tfrInstructions">
								<!-- <div class="area-inst clr marg pad"> Instructions goes here </div> -->
							</div>
						</div>
					</div>
				</div>
				<!-- Area Representation -->
				<div class="left of-h">
					<div class="txt-b marg clr">Area Representation</div>
					<div class="clr of-h">
						<div class="marg clr">
							<div class="marg m-t10 clr">
							<div class="w-150 m-b5 clr">Select Component Definition</div>
								<select class="w-250 m-b5 area-definition"
									id="area-definition"
									data-role="dropdownlist"
									data-bind="events:{change:setFRDOption}">
									<option value="component">Component definition</option>
									<option value="name">Area Name</option>
								</select>
							</div>
							<div class="marg clr">
								<label><input type="checkbox" class="area-frdCheck"
										data-bind="events:{change:setFRDInclusion}" checked="checked"/> Include FRDs in shape definitions</label>
							</div>
							<div class="marg clr">
								<label><input type="checkbox" class="area-locTimeCheck"
										data-bind="events:{change:setLocTimeInclusion}" checked="checked"/> Include Local Time</label>
							</div>
							<!--<div class="marg clr">
								<label><input type="checkbox" class="area-allowAuthCheck"
										data-bind="enabled:allowFlights,events:{change:setAllowFlightAuth}" /> Allow authorization of flight operations</label>
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- areas -->
		<div class="m-t15 clr of-h">
			<div class="marg clr">
				<span class="txt-b">TFR Area(s)</span>
			</div>
			<!-- Areas btns -->
			<div class="marg m-t10 clr of-h">
				<div class="left">
					<button class="w-100"
						data-role="button"
						data-bind="click:addBlankArea">Add Area</button>
					<button class="w-100"
						data-role="button"
						data-bind="click:removeCrtArea">Remove Area</button>
					<!-- TODO: Move tabs around
					<button class="w-100"
						data-role="button"
						data-bind="click:moveCrtAreaLeft">&lt;&lt; Move Left</button>
					<button class="w-100"
						data-role="button"
						data-bind="click:moveCrtAreaRight">Move Right &gt;&gt;</button> -->
				</div>
				<!-- VOR LIST
				<div class="m-l15 left">
					<input class="tfr-vors w-300"
						data-role="dropdownlist"
						data-option-label="Select Closest VOR"
						data-text-field="LABEL"
               			data-value-field="ANS_ID"
						data-bind="value:vor,source:vors,events:{change:setVor}" />
				</div>-->
			</div>

			<!-- areas -->
			<div id="tfr-areas" class="marg m-t10 clr"
				data-role="tabstrip" data-bind="events:{select:updateActiveArea}">
				<ul>
					<li class="k-state-active">Area A</li>
				</ul>
				<div class="of-h">
					<?php include "tfr-areas.php"; ?>
				</div>
			</div>
		</div>
		<!-- Group & Coord -->
		<div class="m-t15 clr of-h">
			<!-- group or point of contact -->
			<div class="left of-h" data-bind="visible:gOpsVisible">
				<div class="txt-b marg clr"
					data-bind="text:gOpsLabel">Group in charge of the operation</div>
				<div class="marg clr of-h">
					<div class="marg clr of-h">
						<div class="w-150 marg left">Name</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								value="Area A"
								data-value-update="keyup"
								data-bind="value:gOpsName" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Organization</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:gOpsOrg" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Telephone Numbers(s)</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:gOpsTels" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Frequency(s)</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:gOpsFreqs" />
						</div>
					</div>
				</div>
			</div>
			<!-- coordination ==> show this only for Space Ops-->
			<div class="left of-h" data-bind="visible:crdFacVisible">
				<div class="txt-b marg clr">Coordinating facility</div>
				<div class="marg clr">
					<div class="marg clr of-h">
						<div class="w-150 marg left">ID</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:crdFacId" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Name</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:crdFacName" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Type</div>
						<div class="marg left">
							<input class=""
								data-role="dropdownlist"
								data-option-label="Select facility Type"
								data-text-field="NAME"
                   				data-value-field="NAME"
								data-bind="source:crdFacTypes,value:crdFacType" />

						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Telephone Numbers(s)</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:crdFacTels" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Frequency(s)</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:crdFacFreqs" />
						</div>
					</div>
				</div>
			</div>
			<!-- controlling ==> only for Airshows -->
			<div class="left of-h" data-bind="visible:ctlFacVisible">
				<div class="txt-b marg clr">Controlling facility</div>
				<div class="marg clr">
					<div class="marg clr of-h">
						<div class="w-150 marg left">Name</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:ctlFacName" />
						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Type</div>
						<div class="marg left">
							<input class=""
								data-role="dropdownlist"
								data-option-label="Select facility Type"
								data-text-field="NAME"
                   				data-value-field="NAME"
								data-bind="source:crdFacTypes,value:ctlFacType" />

						</div>
					</div>
					<div class="marg clr of-h">
						<div class="w-150 marg left">Frequency(s)</div>
						<div class="marg left">
							<input type="text" class="k-textbox"
								data-value-update="keyup"
								data-bind="value:ctlFacFreqs" />
						</div>
					</div>
				</div>
			</div>
			<!-- reason & modification ==> hidden for lack of req clarification NISIS-783 -->
			<div class="left of-h">
				<!-- reason ==> hide for VIPs -->
				<div class="clr" data-bind="visible:tfrRsVisible">
					<div class="txt-b marg clr">Reason for the TFR</div>
					<!-- for hazards -->
					<div class="marg clr" data-bind="visible:tfrTxtRsVisible">
						<textarea class="w-300 h-50 b-rnd b-shd"
							data-bind="value:tfrReason" ></textarea>
					</div>
					<!-- only for security -->
					<div class="marg clr" data-bind="visible:tfrSelRsVisible">
						<div class="clr">
							<select  class="w-300"
								data-role="dropdownlist"
								data-option-label="- None -"
								data-text-field="name"
                   				data-value-field="value"
								data-bind="source:tfrReasons,value:tfrReason">

							</select>
						</div>
					</div>
				</div>
				<!-- modify previous -->
				<div class="hide m-t15 clr">
					<div class="txt-b marg clr">Modify existing TFR</div>
					<div class="marg clr of-h">
						<div class="clr of-h">
							<div class="w-100 marg left">NOTAM Number</div>
							<div class="marg left">
								<input type="text" class="w-200 k-textbox"
									data-value-update="keyup"
									data-bind="value:modTfrNum" />
							</div>
						</div>
						<div class="marg clr of-h">Reason</div>
						<div class="marg clr">
							<textarea class="w-300 h-100 b-rnd b-shd"
								data-value-update="keyup"
								data-bind="value:modTfrReason"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Notam Text-->
	<div class="tfr-notam-text">
		<div class="">
			<div class="marg left">
				<!-- hidden until requirement is finalized
				<div class="marg clr">
					<label><input type="checkbox"
						data-bind="events:{change:updateTfrNotamText}" /> Split into parts</label>
				</div>
				<div class="marg clr">
					<label><input type="checkbox"
						data-bind="events:{change:updateTfrNotamText}" /> Display NOTAM Text without Formatting</label>
				</div> -->
				<div class="marg clr">
					<label><input type="checkbox" id="custom_text_checkbox" class="do-not-track"
						 data-bind="value:custom_text,events:{change:enableTfrEdit}" /> Freeform NOTAM Text</label>
				</div>
			</div>
			<div class="marg right">
				<span class="pointer"
					data-bind="click:copyNotamText"><i class="fa fa-copy"></i></span>
				<span class="txt-b"
					data-bind="text:notamTextLen">0</span>
				<span class="txt-b"> character(s)</span>
			</div>
		</div>
		<div data-role="editor" id="notamEditor" class="c-txt w-fit-m h-500 marg pad b-rnd b-shd clr of-a"
			data-bind="value:tfrNotamText,events:{focus:hideTfrToolbar}"  data-tools=""></div>
	</div>
	<!-- airports -->
	<div class="trf-airports">
		<div class="m-t clr of-h">
			<button data-role="button"
				data-bind="click:getArpList">
				<i class="fa fa-calculator"></i> Calculate</button>
			<!-- <button data-role="button">
			<i class="fa fa-file-excel-o"></i> Export</button> -->
		</div>

		<div class="m-t15 clr of-h">
			<div id="tfr-arps" class="tfr-airports"
				data-role="grid"
				data-scrolable="true"
				data-editable="false"
				data-sortable="true"
				data-filterable="true"
				data-pageable="{pageSize:10}"
				data-columns='[{"field":"id","title":"ID","width":50},
					{"field":"area","title":"Area","width":80},
					{"field":"arp_id","title":"Airport ID","width":60},
					{"field":"arp_name","title":"Airport Name","width":150},
					{"field":"location","title":"Location","width":200}]'
				data-bind="source:arpList"></div>
		</div>
	</div>
</div>

<!-- </form> -->
