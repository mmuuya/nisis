<!-- 
	Add this item to the TFR Menu
	<li>Web Text</li> 
	
	Add this div to TFR Body
	<div class="tfr-web-text">
		<?php //include "tfr-webtext.php" ?>
	</div>
-->

<!-- Notam -->
<div id="tfr-web-notam" class="clr of-h">
	<div class="tfr-web-header clr of-h">
		<div class="w-300 left"><span>NOTAM Number: </span></div>
		<div class="left"><span class="">FDC</span> <span class="">Y/NNNN</span></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Location:</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Begining Date and Time:</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Ending Date and Time:</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Reason for NOTAM:</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Type:</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Replaced NOTAM(s):</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="txt-r txt-b w-300 marg left">Jump To:</div>
		<div class="marg left">
			<span class="txt-link" data-target="tfr-web-area" data-bind="click:moveToSection">Affected Areas</span><br/>
			<span class="txt-link" data-target="tfr-web-ops" data-bind="click:moveToSection">Operating Restrictions and Requirements</span><br/>
			<span class="txt-link" data-target="tfr-web-info" data-bind="click:moveToSection">Other Information</span>
		</div>
	</div>
</div>

<!-- affected area -->
<div id="tfr-web-area" class="clr of-h">
	<div class="tfr-web-header clr of-h">
		<div class="left"><span>Affected Area(s)</span></div>
		<div class="right"><span class="pointer txt-u" data-bind="click:moveTopPage">Top</span></div>
	</div>
	<div class="tfr-web-area clr of-h">
		<div class="marg left">Airspace Definition:</div>
	</div>
</div>

<!-- Rest. & Req. -->
<div id="tfr-web-ops" class="clr of-h">
	<div class="tfr-web-header clr of-h">
		<div class="left"><span>Operating Restrictions and Requirements</span></div>
		<div class="right"><span class="pointer txt-u" data-bind="click:moveTopPage">Top</span></div>
	</div>
	<div class="tfr-web-ops clr of-h">
		<div class="marg left">No pilots may operate an aircraft in the areas covered by this NOTAM (except as described).</div>
	</div>
</div>

<!-- Other Info -->
<div id="tfr-web-info" class="clr of-h">
	<div class="tfr-web-header clr of-h">
		<div class="left"><span>Other Information</span></div>
		<div class="right"><span class="pointer txt-u" data-bind="click:moveTopPage">Top</span></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">ARTCC:</div>
		<div class="marg left"></div>
	</div>
	<div class="m-b clr of-h">
		<div class="w-300 marg left">Authority:</div>
		<div class="marg left"></div>
	</div>
</div>