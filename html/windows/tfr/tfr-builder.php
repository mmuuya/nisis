<!-- TFR Builder main page -->
<div id="tfr-dialog" class="clr">
	<div id="tfr-menu-div" class="clr">
				<ul id="tfr-menu" data-role="menu">
					<li data-bind="click:createTfr"><i class="fa fa-square-o"></i> &nbsp;New</li>
					<li data-bind="click:openTfr"><i class="fa fa-folder-open-o"></i> Open</li>
					<!-- <li data-bind="click:editTfr"><i class="fa fa-edit"></i> Edit</li>
					<li data-bind="click:reviewTfr"><i class="fa fa-check-square-o"></i> Review</li> -->

					<li class="tfr-publish"
						data-bind="click:publishTfr"><i class="fa fa-share-square-o"></i> &nbsp;Publish</li>

						<li class="tfr-help" data-bind="click:openTfrHelp"><i class="fa fa-question-circle"></i> &nbsp;Help</li>

					<!-- <li class="k-separator"></li>
					<li class="tfr-save-xls" data-bind="click:saveXlsTfr"><i class="fa fa-save"></i> Save TFR to Workbook</li> -->
					<!--<li class="k-separator"></li>
					<li class="tfr-print-web" data-bind="click:prinWebText"><i class="fa fa-print"></i> Print Web Text</li>
					<li class="tfr-print-text" data-bind="click:printNotamText"><i class="fa fa-print"></i> Print NOTAM Text</li>
					<li data-bind="click:hideNisisObjects"><i class="fa fa-print"></i> Testing</li>-->

					<li class="tfr-exit" data-bind="visible:exitTfrMenu,events:{click:exitTfrMode}"><i class="fa fa-sign-out"></i> Exit</li>
		</ul>
	</div>
	<div id="tfr-body" class="clr of-h">
		<div id="tfr-infos" class="m-t clr of-h">
			<div class="txt-b m-t left of-h">
				<span data-bind="text:tfrTitle"></span>&nbsp;&nbsp;&nbsp;
				<!-- <span class="txt-b tc-r" data-bind="visible:tfrModified">Modified</span> -->

				<div id="saveMenu">
					<span class="tfr-save" data-bind="click:saveTfr"><i class="fa fa-save" title="Save TFR"></i></span>
					<span class="tfr-cancel" data-bind="click:cancelTfr"><i class="fa fa-trash-o" title="Cancel TFR"></i></span>
				</div>
			</div>
			<div class="txt-r m-t right of-h">
				<span data-bind="text:tfrMsg"></span>
			</div>
		</div>
	</div>

	<div id="tfr-content" class="m-t clr of-a">
		<?php include "tfr-template.php" ?>
	</div>
</div>
