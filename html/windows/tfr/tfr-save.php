<?php
/* PHP code for committing NOTAM values to the database 
	To add save definitions for new NOTAM types, update the "extraneous information" section at the bottom.
*/

	//Retrieve the confirgurations and global stuff, or fail if we can't.
	if ((include '../../../handler.php') === FALSE){
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}

	if(!isset($_SESSION['userid'])){
		return false;
	}

	$notam_type = $_REQUEST['notam_type'];

	if (!isset($notam_type) || $notam_type == ""){
		exit(json_encode(array('return' => 'Failure', 'message' => 'Missing/Invalid NOTAM_TYPE argument.')));
	}

	global $db;

	//NOTAM Parameters/fields to save to NOTAM_BODY table
	$notam_id = $_REQUEST['notam_id'];
	$notam_name = $_REQUEST['notam_name'];
	$status = $_REQUEST['status'];
	$user_id = $_SESSION['userid'];
	$artcc_id = $_REQUEST['artcc_id'];
	$artcc_name = $_REQUEST['artcc_name'];
	$effective_date_utc = $_REQUEST['effective_date_utc'];
	$expiration_date_utc = $_REQUEST['expiration_date_utc'];
	$locations_array = json_decode($_REQUEST['locations_array'], true);
	$area_array = json_decode($_REQUEST['area_array'], true);
	// error_log($area_array[1]);
	$custom_text = $_REQUEST['custom_text'];
	$operatingInstructions = json_decode($_REQUEST['operatingInstructions'], true);
	$include_local_time = $_REQUEST['include_local_time'];
	$include_frd = $_REQUEST['include_frd'];	


	// COMMENTED OUT BY KOFI $selected_vor = $_REQUEST['selected_vor'];

	/*check for read-only access*/
	$read_only_sql = "select * from tfruser.user_group where groupid in (select groupid 
			from tfruser.groups where lower(name) like '%read%only%') and userid = :user_id";
	$read_only_parsed = oci_parse($db, $read_only_sql);
	oci_bind_by_name($read_only_parsed, ':user_id', $user_id);
	oci_execute($read_only_parsed);
	oci_fetch_all($read_only_parsed, $read_only_results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
	$read_only = count($read_only_results);
	//error_log('num returned: ' .$read_only);
	if($read_only > 0){
		// return(json_encode(array('return' => 'error', 'status' => 'Current user has read only permissions.')));
		$data = array();
		$data['error']= true;
		$data['success'] = false;
		$data['errormessage'] = "Current user has read-only permissions.";
		echo json_encode($data);
		exit(false);
	}


	/*if notam id is not passed in, assume new notam is being saved. Fetch new notam id
		and save notam to groups that user belongs to*/
	if(!isset($notam_id) || $notam_id == "" || $notam_id == null){
		$id_query = 'select  tfruser.seq_notam_id.nextval from dual';
		$id_parsed = oci_parse($db, $id_query);
		oci_execute($id_parsed);
		oci_fetch($id_parsed);
		$notam_id = oci_result($id_parsed, 'NEXTVAL');
		oci_free_statement($id_parsed);

		$user_group_sql = 'select userid, groupid from tfruser.user_group where userid = :user_id';
		$user_group_parsed = oci_parse($db, $user_group_sql);
		oci_bind_by_name($user_group_parsed, ':user_id', $user_id);
		oci_execute($user_group_parsed);
		oci_fetch_all($user_group_parsed, $all_groups, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
		$num_groups = count($all_groups);

		for ($i = 0; $i < $num_groups; $i++){
			$notam_group_sql = 'insert into tfruser.notam_groups (notam_id, group_id) 
				values (:notam_id, :group_id)';
			$notam_group_parsed = oci_parse($db, $notam_group_sql);
			oci_bind_by_name($notam_group_parsed, ":notam_id", $notam_id);
			oci_bind_by_name($notam_group_parsed, ":group_id", $all_groups[$i]['GROUPID']);
			oci_execute($notam_group_parsed);
		}		
	}

	// print_r($_REQUEST);
	//main notam body insertion
	// COMMENTED OUT BY KOFI BCOS OF REFERENCE_VOR COLUMN BEING OBSOLETE
	// $notam_body_sql = 'merge into tfruser.notam_body n
 //    using (select :notam_id "x.notam_id" from dual) 
 //    on (n.notam_id = "x.notam_id")
 //    when matched then update set n.notam_type = :notam_type, n.notam_name = :notam_name,
 //        n.status = :status, n.updated_by = :user_id, 
 //        n.update_date = sysdate, n.artcc_id = :artcc_id, n.artcc_name = :artcc_name,
 //        n.effective_date_utc = :effective_date_utc, n.expiration_date_utc = :expiration_date_utc,
 //        n.custom_text = :custom_text, n.reference_vor = :selected_vor
 //    when not matched then insert (n.NOTAM_ID, n.NOTAM_TYPE, n.NOTAM_NAME, n.STATUS, n.USER_ID, 
 //        n.LOAD_DATE, n.ARTCC_ID, n.ARTCC_NAME, n.EFFECTIVE_DATE_UTC, 
 //        n.EXPIRATION_DATE_UTC, n.CUSTOM_TEXT, n.REFERENCE_VOR) 
 //        values (:notam_id, :notam_type, :notam_name, :status, :user_id,  
 //        sysdate, :artcc_id, :artcc_name, :effective_date_utc, :expiration_date_utc, 
 //        :custom_text, :selected_vor)';


	$notam_body_sql = 'merge into tfruser.notam_body n
    using (select :notam_id "x.notam_id" from dual) 
    on (n.notam_id = "x.notam_id")
    when matched then update set n.notam_type = :notam_type, n.notam_name = :notam_name,
        n.status = :status, n.updated_by = :user_id, 
        n.update_date = sysdate, n.artcc_id = :artcc_id, n.artcc_name = :artcc_name,
        n.effective_date_utc = :effective_date_utc, n.expiration_date_utc = :expiration_date_utc,
        n.custom_text = :custom_text, 
        n.include_local_time = :include_local_time, 
        n.include_frd = :include_frd 
    when not matched then insert (n.NOTAM_ID, n.NOTAM_TYPE, n.NOTAM_NAME, n.STATUS, n.USER_ID, 
        n.LOAD_DATE, n.ARTCC_ID, n.ARTCC_NAME, n.EFFECTIVE_DATE_UTC, 
        n.EXPIRATION_DATE_UTC, n.CUSTOM_TEXT, n.INCLUDE_LOCAL_TIME, 
        n.INCLUDE_FRD)
     values (:notam_id, :notam_type, :notam_name, :status, :user_id,  
        sysdate, :artcc_id, :artcc_name, :effective_date_utc, :expiration_date_utc, 
        :custom_text, :include_local_time, :include_frd)';

	$body_parsed = oci_parse($db, $notam_body_sql);

	oci_bind_by_name($body_parsed, ":notam_id", $notam_id);
	oci_bind_by_name($body_parsed, ":notam_type", $notam_type);
	oci_bind_by_name($body_parsed, ":notam_name", $notam_name);
	oci_bind_by_name($body_parsed, ":status", $status);
	oci_bind_by_name($body_parsed, ":user_id", $user_id);
	oci_bind_by_name($body_parsed, ":artcc_id", $artcc_id);
	oci_bind_by_name($body_parsed, ":artcc_name", $artcc_name);
	oci_bind_by_name($body_parsed, ":effective_date_utc", $effective_date_utc);
	oci_bind_by_name($body_parsed, ":expiration_date_utc", $expiration_date_utc);
	oci_bind_by_name($body_parsed, ":custom_text", $custom_text);
	oci_bind_by_name($body_parsed, ":include_local_time", $include_local_time);
	oci_bind_by_name($body_parsed, ":include_frd", $include_frd);		

	oci_execute($body_parsed);

	//NOTAM OPERATING INSTRUCTIONS TABLE INSERTION.
	//Deleting all the notam instructions for the specific notam first
	$opr_inst_delete = 'delete from tfruser.notam_instruction where notam_id = :notam_id';
	$opr_inst_delete_parsed = oci_parse($db, $opr_inst_delete);
	oci_bind_by_name($opr_inst_delete_parsed, ":notam_id", $notam_id);
	oci_execute($opr_inst_delete_parsed);

	//Instruction_id ("instId") will be the instruction_id and also the display order, so "display_order" column will be removed.
	//Inserting the new notam instructions after the old have been deleted
	for ($i=0; $i<count($operatingInstructions); $i++){

		$instruction_text = $operatingInstructions[$i]['text'];
		$instruction_id = $operatingInstructions[$i]['instId'];

		$opr_inst_insert = 'insert into tfruser.notam_instruction n (n.notam_id, n.instruction_text, n.instruction_id) values
		(:notam_id, :instruction_text, :instruction_id)';
		$opr_inst_insert_parsed = oci_parse($db, $opr_inst_insert);
		oci_bind_by_name($opr_inst_insert_parsed, ":notam_id", $notam_id);
		oci_bind_by_name($opr_inst_insert_parsed, ":instruction_text", $instruction_text);
		oci_bind_by_name($opr_inst_insert_parsed, ":instruction_id", $instruction_id);
		
		oci_execute($opr_inst_insert_parsed);
	}

	//notam location table insertion
	$notam_locations_delete = 'delete from tfruser.notam_locations where notam_id like :notam_id';
	$notam_locations_delete_parsed = oci_parse($db, $notam_locations_delete);
	oci_bind_by_name($notam_locations_delete_parsed, ":notam_id", $notam_id);
	oci_execute($notam_locations_delete_parsed);
	$num_records = count($locations_array);
	$insert_order = 0;

	for ($i = 0; $i < $num_records; $i++){
		$state_territory = $locations_array['loc'.$i]['stName'];
		$city = $locations_array['loc'.$i]['city'];
		$state_abbrev = $locations_array['loc'.$i]['state'];
		$state_territory = ($state_territory != "Select a state" ? $state_territory : null);
		$city = ($city != "Select a city" ? $city : null);
		$state_abbrev = ($state_territory != "Select a state" ? $state_abbrev : null);

		//If state information is null, drop entry from insert.
		if ($state_abbrev){			
			$notam_locations_insert = 'insert into tfruser.notam_locations n (n.notam_id, n.state_abbrev, n.state_territory,
			n.city, n.insert_order) values (:notam_id, :state_abbrev, :state_territory, :city, :insert_order)';
			$notam_locations_insert_parsed = oci_parse($db, $notam_locations_insert);
			oci_bind_by_name($notam_locations_insert_parsed, ":notam_id", $notam_id);
			oci_bind_by_name($notam_locations_insert_parsed, ":state_abbrev", $state_abbrev);
			oci_bind_by_name($notam_locations_insert_parsed, ":state_territory", $state_territory);
			oci_bind_by_name($notam_locations_insert_parsed, ":city", $city);
			oci_bind_by_name($notam_locations_insert_parsed, ":insert_order", $insert_order);

			oci_execute($notam_locations_insert_parsed);
			$insert_order ++;
		}	
	}

	//notam text table insertion
	$notam_text_delete = 'delete from tfruser.notam_text where notam_id like :notam_id';
	$notam_text_delete_parsed = oci_parse($db, $notam_text_delete);
	oci_bind_by_name($notam_text_delete_parsed, ":notam_id", $notam_id);
	oci_execute($notam_text_delete_parsed);

	//notam custom text insertion & update 
	$notam_text	= $_REQUEST['save_text'];
	$notam_text_insert = 'insert into tfruser.notam_text n (n.notam_id, n.text) values
		(:notam_id, :notam_text)';
	$notam_text_insert_parsed = oci_parse($db, $notam_text_insert);
	oci_bind_by_name($notam_text_insert_parsed, ":notam_id", $notam_id);
	oci_bind_by_name($notam_text_insert_parsed, ":notam_text", $notam_text);

	oci_execute($notam_text_insert_parsed);

	//NOTAM TFR area and date/time information (one NOTAM to many Areas, to many Date/Times)
	$num_records = count($area_array);

	//drop pre-existing records and overwrite with new (updated) area information
	$notam_areas_delete_sql = 'delete from tfruser.notam_area where notam_id = :notam_id';
	$notam_areas_delete_parsed = oci_parse($db, $notam_areas_delete_sql);
	oci_bind_by_name($notam_areas_delete_parsed, ":notam_id", $notam_id);
	oci_execute($notam_areas_delete_parsed);

	//Drop existing Date/time information and overwrite with new (updated) information
	$notam_tfr_date_time_delete = 'delete from tfruser.notam_tfr_date_time where notam_id = :notam_id';
	$notam_tfr_date_time_delete_parsed = oci_parse($db, $notam_tfr_date_time_delete);
	oci_bind_by_name($notam_tfr_date_time_delete_parsed, ":notam_id", $notam_id);
	oci_execute($notam_tfr_date_time_delete_parsed);

	//Drop existing Shape information and overwrite with new (updated) information
	$notam_area_shape_delete = 'delete from tfruser.notam_area_shapes where notam_id = :notam_id';
	$notam_area_shape_delete_parsed = oci_parse($db, $notam_area_shape_delete);
	oci_bind_by_name($notam_area_shape_delete_parsed, ":notam_id", $notam_id);
	oci_execute($notam_area_shape_delete_parsed);

	//Drop existing Instruction information and overwrite with new (updated) information
	// $notam_inst_delete = 'delete from tfruser.notam_area_inst where notam_id = :notam_id';
	// $notam_inst_delete_parsed = oci_parse($db, $notam_inst_delete);
	// oci_bind_by_name($notam_inst_delete_parsed, ":notam_id", $notam_id);
	// oci_execute($notam_inst_delete_parsed);

	//loop through all Area definitions, and (while still in area loop) loop through time definitions.
	for ($i = 1; $i <= $num_records; $i++){
		$area_temp = $area_array[$i];

		// $debug = $notam_id . ', ';
		// $debug = $debug . $i . ', ';
		// $debug = $debug . $area_temp["areaName"] . ', ';
		// $debug = $debug . $area_temp["includeFRD"] . ', ';
		// $debug = $debug . $area_temp["frdOption"] . ', ';
		// $debug = $debug . $area_temp["includeLocTime"] . ', ';
		// $debug = $debug . $area_temp["allowFlightAuth"] . ', ';
		// $debug = $debug . $area_temp["areaDTOption"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchOption"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["monday"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["tuesday"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["wednesday"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["thursday"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["friday"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["saturday"] . ', ';
		// $debug = $debug . $area_temp["areaDTSchedule"]["sunday"];

		//notam area definitions insert
		$notam_area_sql = 'insert into tfruser.notam_area (notam_id, area_id, inst_id, area_name,  
			definition_or_name, allow_authorization, utc_time_zone, 
			scheduled, monday, tuesday, wednesday, thursday, friday, 
			saturday, sunday)
			values (:notam_id, :area_id, :inst_id, :area_name, :definition_or_name, 
				:allow_authorization, :utc_time_zone, :scheduled, 
				:monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday)';

		error_log(' \n ' .$notam_area_sql);
		$notam_area_parsed = oci_parse($db, $notam_area_sql);
		oci_bind_by_name($notam_area_parsed, ":notam_id", $notam_id);
		oci_bind_by_name($notam_area_parsed, ":area_id", $i);
		oci_bind_by_name($notam_area_parsed, ":area_name", $area_temp["areaName"]);
		oci_bind_by_name($notam_area_parsed, ":inst_id", $area_temp["instruction"]);
		oci_bind_by_name($notam_area_parsed, ":definition_or_name", $area_temp["frdOption"]);		
		oci_bind_by_name($notam_area_parsed, ":allow_authorization", $area_temp["allowFlightAuth"]);
		oci_bind_by_name($notam_area_parsed, ":utc_time_zone", $area_temp["areaDTOption"]);
		oci_bind_by_name($notam_area_parsed, ":scheduled", $area_temp["areaDTSchOption"]);
		oci_bind_by_name($notam_area_parsed, ":monday", $area_temp["areaDTSchedule"]["monday"]);
		oci_bind_by_name($notam_area_parsed, ":tuesday", $area_temp["areaDTSchedule"]["tuesday"]);
		oci_bind_by_name($notam_area_parsed, ":wednesday", $area_temp["areaDTSchedule"]["wednesday"]);
		oci_bind_by_name($notam_area_parsed, ":thursday", $area_temp["areaDTSchedule"]["thursday"]);
		oci_bind_by_name($notam_area_parsed, ":friday", $area_temp["areaDTSchedule"]["friday"]);
		oci_bind_by_name($notam_area_parsed, ":saturday", $area_temp["areaDTSchedule"]["saturday"]);
		oci_bind_by_name($notam_area_parsed, ":sunday", $area_temp["areaDTSchedule"]["sunday"]);	

		oci_execute($notam_area_parsed);

		//pull date/time information from either scheduled or not scheduled object
		if($area_temp['areaDTSchOption'] == "non-schedule"){
			$date_temp = $area_temp["areaDateTimes"]["non-schedule"];
			$num_dates = count($date_temp);
		} else {
			$date_temp = $area_temp["areaDateTimes"]["schedule"];
			$num_dates = count($date_temp);		
		}
		
		//iterate through all possible date/times in the json object passed form interface and insert into datbase
		$obj = new ArrayObject($date_temp);
		$it = $obj->getIterator();
		$x = 1;
		foreach($it as $key=>$val){
			$notam_tfr_date_time_sql = 'insert into tfruser.notam_tfr_date_time (notam_id, area_id, date_time_id, 
				effective_date_time, expiration_date_time)
				values (:notam_id, :area_id, :date_time_id, :effective_date_time, :expiration_date_time)';
			$notam_tfr_date_time_parsed = oci_parse($db, $notam_tfr_date_time_sql);
			if((isset($date_temp[$key]) && isset($date_temp[$key]["eff"])) && array_key_exists('eff', $date_temp[$key])){
				$effDate = $date_temp[$key]["eff"];
				$expDate = $date_temp[$key]["exp"];
				oci_bind_by_name($notam_tfr_date_time_parsed, ":notam_id", $notam_id);
				oci_bind_by_name($notam_tfr_date_time_parsed, ":area_id", $i);
				oci_bind_by_name($notam_tfr_date_time_parsed, ":date_time_id", $x);
				oci_bind_by_name($notam_tfr_date_time_parsed, ":effective_date_time", $effDate);
				oci_bind_by_name($notam_tfr_date_time_parsed, ":expiration_date_time", $expDate);

				oci_execute($notam_tfr_date_time_parsed);
				$x++;
			}
		}
		//iterate through all area/shape information and insert into database
		if(isset($area_temp["shape"])) {				
			$areaShape = $area_temp["shape"];

			//make sure its not empty
			if (count($areaShape) > 0) {
				// $areaShapesArray = json_decode($areaShape, true);
				$areaShapesArray = $areaShape;

                foreach($areaShapesArray as $item) { 
    				$shapeId = $item['OBJECTID'];					
					$vor = $item['VOR'];					

					$notam_shape_sql = 'insert into tfruser.notam_area_shapes (notam_id, area_id, shape_id, reference_vor)
						values (:notam_id, :area_id, :shape_id, :reference_vor)';
					$notam_shape_parsed = oci_parse($db, $notam_shape_sql);
					oci_bind_by_name($notam_shape_parsed, ":notam_id", $notam_id);
					oci_bind_by_name($notam_shape_parsed, ":area_id", $i);
					oci_bind_by_name($notam_shape_parsed, ":shape_id", $shapeId);
					oci_bind_by_name($notam_shape_parsed, ":reference_vor", $vor);
                   
					oci_execute($notam_shape_parsed);
				}
			}
		}		

		// //iterate through all instructions and insert into database
		// if(isset($area_temp["instructionText"])){
		// 	$num_inst = count($area_temp["instructionText"]);
		// 	//g = 1 to account for index offset
		// 	for($g = 0; $g < $num_inst; $g++){
		// 		$notam_inst_sql = 'insert into tfruser.notam_area_inst (notam_id, area_id, instruction_id, instruction_text)
		// 			values (:notam_id, :area_id, :instruction_id, :instruction_text)';
		// 		$notam_inst_parsed = oci_parse($db, $notam_inst_sql);
		// 		oci_bind_by_name($notam_inst_parsed, ":notam_id", $notam_id);
		// 		oci_bind_by_name($notam_inst_parsed, ":area_id", $i);
		// 		oci_bind_by_name($notam_inst_parsed, ":instruction_id", $g);
		// 		oci_bind_by_name($notam_inst_parsed, ":instruction_text", $area_temp["instructionText"][$g]);

		// 		oci_execute($notam_inst_parsed);
		// 	}
		// }

	}

	//Extraneous NOTAM information, such as point of contact, coordinating and control facilities, as well as modified NOTAM IDs
	$lookup_tfr_types = 'select id, name, ref from tfruser.lookup_tfr_types order by id asc';
	$lookup_tfr_types_parsed = oci_parse($db, $lookup_tfr_types);

	oci_execute($lookup_tfr_types_parsed);

	oci_fetch_all($lookup_tfr_types_parsed, $tfr_types, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

	//-1 because 0-based array vs first ID being 1 in lookup table.
	$type_name = $tfr_types[$notam_type-1]['NAME'];
	//error_log("type_name " . $type_name);

	//bind posted values to local variables
	$notam_type_sql = '';
	$poc_name = $_REQUEST['poc_name'];
	$poc_org = $_REQUEST['poc_org'];
	$poc_phone = $_REQUEST['poc_phone'];
	$poc_freq = $_REQUEST['poc_freq'];
	$coord_id = $_REQUEST['coord_id'];
	$coord_name = $_REQUEST['coord_name'];
	if(gettype(json_decode($_REQUEST['coord_type'])) == 'object'){
		$coord_type = json_decode($_REQUEST['coord_type'], true);	
	} else {
		$coord_type = null;
	}	
	$coord_phone = $_REQUEST['coord_phone'];
	$coord_freq = $_REQUEST['coord_freq'];
	$ctrl_name = $_REQUEST['ctrl_name'];
	$ctrl_type = $_REQUEST['ctrl_type'];
	$ctrl_freq = $_REQUEST['ctrl_freq'];
	$tfr_reason = json_decode($_REQUEST['tfr_reason'], true);
	if(isset($tfr_reason['name']) && isset($tfr_reason['id']) && is_numeric($tfr_reason['id'])){
		$tfr_reason = $tfr_reason['name'];
	}
	$modify_id = $_REQUEST['modify_id'];
	$modify_reason = $_REQUEST['modify_reason'];


	$notam_type_delete = 'delete from ';

	switch (strtoupper($type_name)) {
		case 'HAZARDS':
			$notam_type_sql = 'insert into tfruser.notam_type_hazards (notam_id, point_of_contact_name, poc_organization,
				poc_phones, poc_frequencies, coordinating_facility_id, coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones,
				coord_fac_frequencies, reason, modify_id, modify_reason) 
			values (:notam_id, :point_of_contact_name, :poc_organization, :poc_phones, :poc_frequencies, 
				:coordinating_facility_id, :coord_fac_name, :coord_fac_type, :coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :reason, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':point_of_contact_name', $poc_name);
			oci_bind_by_name($notam_type_parsed, ':poc_organization', $poc_org);
			oci_bind_by_name($notam_type_parsed, ':poc_phones', $poc_phone);
			oci_bind_by_name($notam_type_parsed, ':poc_frequencies', $poc_freq);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':reason', $tfr_reason);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= ' tfruser.notam_type_hazards where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		case 'VIP':
			$notam_type_sql = 'insert into tfruser.notam_type_vip (notam_id, coordinating_facility_id, 
				coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones,
				coord_fac_frequencies, modify_id, modify_reason) 
			values (:notam_id, :coordinating_facility_id, :coord_fac_name, :coord_fac_type, 
				:coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= ' tfruser.notam_type_vip where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		case 'AIRSHOW':
			$notam_type_sql = 'insert into tfruser.notam_type_airshow (notam_id, point_of_contact_name, poc_organization,
				poc_phones, poc_frequencies, coordinating_facility_id, coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones,
				coord_fac_frequencies, controlling_facility_name, cntrl_fac_type, cntrl_fac_freq, reason, modify_id, modify_reason) 
			values (:notam_id, :point_of_contact_name, :poc_organization, :poc_phones, :poc_frequencies, 
				:coordinating_facility_id, :coord_fac_name, :coord_fac_type, :coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :cntrl_fac_name, :cntrl_fac_type, :cntrl_fac_freq, 
				:reason, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':point_of_contact_name', $poc_name);
			oci_bind_by_name($notam_type_parsed, ':poc_organization', $poc_org);
			oci_bind_by_name($notam_type_parsed, ':poc_phones', $poc_phone);
			oci_bind_by_name($notam_type_parsed, ':poc_frequencies', $poc_freq);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':cntrl_fac_name', $ctrl_name);
			oci_bind_by_name($notam_type_parsed, ':cntrl_fac_type', $ctrl_type);
			oci_bind_by_name($notam_type_parsed, ':cntrl_fac_freq', $ctrl_freq);
			oci_bind_by_name($notam_type_parsed, ':reason', $tfr_reason);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= 'tfruser.notam_type_airshow where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		case 'SECURITY':
			$notam_type_sql = 'insert into tfruser.notam_type_security (notam_id, point_of_contact_name, poc_organization,
				poc_phones, poc_frequencies, coordinating_facility_id, coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones,
				coord_fac_frequencies, reason, modify_id, modify_reason)
			values (:notam_id, :point_of_contact_name, :poc_organization, :poc_phones, :poc_frequencies, 
				:coordinating_facility_id, :coord_fac_name, :coord_fac_type, :coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :reason, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':point_of_contact_name', $poc_name);
			oci_bind_by_name($notam_type_parsed, ':poc_organization', $poc_org);
			oci_bind_by_name($notam_type_parsed, ':poc_phones', $poc_phone);
			oci_bind_by_name($notam_type_parsed, ':poc_frequencies', $poc_freq);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':reason', $tfr_reason);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= 'tfruser.notam_type_security where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		case 'SPACE OPERATIONS':
			$notam_type_sql = 'insert into tfruser.notam_type_space_operations (notam_id, coordinating_facility_id,
				coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones, coord_fac_frequencies, reason, 
				modify_id, modify_reason)
			values (:notam_id, :coordinating_facility_id, :coord_fac_name, :coord_fac_type, :coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :reason, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':reason', $tfr_reason);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= 'tfruser.notam_type_space_operations where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		case 'HAWAII':
			$notam_type_sql = 'insert into tfruser.notam_type_hawaii (notam_id, coordinating_facility_id,
				coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones, coord_fac_frequencies, 
				modify_id, modify_reason)
			values (:notam_id, :coordinating_facility_id, :coord_fac_name, :coord_fac_type, :coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= 'tfruser.notam_type_hawaii where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		case 'EMERGENCY':
			$notam_type_sql = 'insert into tfruser.notam_type_emergency_traffic (notam_id, coordinating_facility_id,
				coord_fac_name, coord_fac_type, coord_fac_type_id, coord_fac_phones, coord_fac_frequencies, 
				modify_id, modify_reason)
			values (:notam_id, :coordinating_facility_id, :coord_fac_name, :coord_fac_type, :coord_fac_type_id, :coord_fac_phones,
				:coord_fac_frequencies, :modify_id, :modify_reason)';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			oci_bind_by_name($notam_type_parsed, ':notam_id', $notam_id);
			oci_bind_by_name($notam_type_parsed, ':coordinating_facility_id', $coord_id);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_name', $coord_name);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type', $coord_type['NAME']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_type_id', $coord_type['ID']);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_phones', $coord_phone);
			oci_bind_by_name($notam_type_parsed, ':coord_fac_frequencies', $coord_freq);
			oci_bind_by_name($notam_type_parsed, ':modify_id', $modify_id);
			oci_bind_by_name($notam_type_parsed, ':modify_reason', $modify_reason);

			$notam_type_delete .= ' tfruser.notam_type_emergency_traffic where notam_id = :notam_id';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
			oci_bind_by_name($notam_type_delete_parsed, ':notam_id', $notam_id);
			break;
		default:
			error_log("NOTAM Type not found: " . strtoupper($type_name) . "| It may need to be added to tfr-save.php");
			$notam_type_sql = 'select null from dual';
			$notam_type_parsed = oci_parse($db, $notam_type_sql);
			$notam_type_delete = 'select null from dual';
			$notam_type_delete_parsed = oci_parse($db, $notam_type_delete);
	}

	oci_execute($notam_type_delete_parsed);
	oci_execute($notam_type_parsed);
	exit($notam_id);
?>