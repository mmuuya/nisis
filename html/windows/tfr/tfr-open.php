<?php
/* PHP code for retrieving NOTAM information from the database */

	//Retrieve the confirgurations and global stuff, or fail if we can't.
	if ((include '../../../handler.php') === FALSE){
		exit(json_encode(array('return' => 'Misconfigured Server')));
	}
	if(!isset($_SESSION['userid'])){
		return false;
	}
	$userid = $_SESSION['userid'];

	global $db;
	$results = array();
	$to_return = array();
	$admin = 0;

	//Find out if the current user belongs to the admin group, specifically.
	$is_user_admin_sql = "select groupid from tfruser.user_group where userid = :user_id and groupid = 0";
	$is_user_admin_parsed = oci_parse($db, $is_user_admin_sql);
	oci_bind_by_name($is_user_admin_parsed, ":user_id", $userid);
	oci_execute($is_user_admin_parsed);
	oci_fetch($is_user_admin_parsed);
	if(oci_result($is_user_admin_parsed, 'GROUPID') != "") {		
		$admin = 1;
	} 

	// error_log($admin . " oci_result = " . oci_result($is_user_admin_parsed, 'GROUPID') . " count(null) = ". count(''));

	$notam_list_sql = "select distinct notam_id, notam_type, notam_name, work_number_id, notam_status, usns_status, last_update, user_name from (
		select n.notam_id, 
            n.notam_name, n.work_number_id, g.group_id,
            (select name || case when details is not null then ' (' || details || ')' else null end from tfruser.lookup_tfr_types where id = n.notam_type) notam_type,
            (select status_text from tfruser.lookup_status where status_id = n.status) notam_status, 
            usns_status,
            nvl(update_date, load_date) last_update, 
            (select lname || ', ' || fname || ' (' || username || ')' from appuser.users where userid = n.user_id) user_name 
        from tfruser.notam_body n 
        left join tfruser.notam_groups g on n.notam_id = g.notam_id";
    //If user is an admin, return all records, else filter by subscribed groups
    if($admin != 1){
    	$notam_list_sql .= " where g.group_id in (select u.groupid from tfruser.user_group u where u.userid = :user_id)";
    }
    $notam_list_sql .= ") order by last_update desc";

	// error_log($notam_list_sql);

	$notam_list_parsed = oci_parse($db, $notam_list_sql);
	if($admin !=1) {
		oci_bind_by_name($notam_list_parsed, ":user_id", $userid);
	}
	oci_execute($notam_list_parsed);
	oci_fetch_all($notam_list_parsed, $results, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
	exit(json_encode($results));
	
?>