<!-- 
	Add this item to the TFR Menu
	<li>Status</li> 

	Add this div to TFR Body
	<div class="tfr-status">
		<?php //include "tfr-status.php" ?>
	</div>
-->

<!-- status desc-->
<div class="m-b15 left of-h">
	<div class="clr of-h">
		<div class="txt-b marg left">TFR Status: </div>
		<div class="txt-b txt-u marg left"
			data-bind="text:tfrStatus"></div>
	</div>
	<div class="m-l clr of-h">
		<div class="marg clr">Status Description</div>
		<div class="w-400 h-100 b-rnd b-shd marg m-t15 pad"
			data-bind="text:tfrStatusExp"></div>
	</div>
</div>

<!-- Review -->
<div class="m-b15 clr of-h">
	<div class="txt-b marg clr of-h">Submitting the TFR for <span data-bind="text:workflowLabel">Review</span></div>
	<div class="marg clr">
		<div class="marg clr">Task feedback</div>
		<div class="w-400 h-100 marg m-t10 pad b-rnd b-shd clr"
			data-bind="text:tfrFeedbacks"></div>

		<div class="marg clr">
			<button 
				data-role="button"
				data-bind="click:submitTfr,enabled:checkWorkflow">Submit for <span data-bind="text:workflowLabel">Review</span></button>
			<button 
				data-role="button"
				data-bind="click:rejectTfr,enabled:checkRecycle">Reject TFR</button>
			<button 
				data-role="button"
				data-bind="click:checkTfrErrors">Show Errors</button>
		</div>
	</div>
</div>

<!-- review -->
<div class="m-b15 clr of-h">
	<div class="txt-b marg clr of-h">View TFR History</div>
	<div class="marg clr">
		<div class="w-400 h-100 marg m-t10 pad b-rnd b-shd clr"
			data-bind="text:tfrHisCmts"></div>

		<div class="marg clr">
			<button 
				data-role="button"
				data-bind="click:showTfrHistory">Show TFR History</button>
			<button 
				data-role="button"
				data-bind="click:getTfrComments">Get Comments</button>
		</div>
	</div>
</div>

<!-- add comments -->
<div class="m-b15 clr of-h">
	<div class="txt-b marg clr of-h">Add New NOTAM Comments</div>
	<div class="marg clr">
		<div class="marg m-t10 clr">
			<textarea class="w-400 h-100 b-rnd b-shd"
				data-value-update="keyup"
				data-bind="value:userComments,events:{keyup:checkCmtInput}"></textarea>
		</div>
		<div class="marg clr">
			<button 
				data-role="button"
				data-bind="click:addTfrComments,enabled:validComments">Add Comment</button>
			<button 
				data-role="button"
				data-bind="click:clearTfrComments">Clear</button>
		</div>
	</div>
</div>