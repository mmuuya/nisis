<!-- find change password div parent and add this class, so I can hack the crap out of these garbage kendo styles -->

<script src="./scripts/app/windows/changePassword.js"></script>

<script>
$('#change-password-link').on('click', function(e){
	e.preventDefault();
	var $parent = $('#change-password-div').parent();
	$parent.addClass('change-password-wrapper');
});
</script>

<div id="change-password">
	<form id="chPwd" class="change-pass-form">
	    <fieldset>
			<div class="field">
				<input type="password" id="password" name="password" placeholder="password" required data-bind="value: selected.password">
				<label for="password">Password</label>
			</div>

			<div class="change-pass-footer padd-me-10">
				<button id="submit" class="primary" data-bind="events:{ click: changePassword }">Submit</button>
				<!-- <button type="cancel" class="default">Cancel</button> -->
			</div>
		</fieldset>
	</form>
	<div id="validation"></div>
	<!-- <div class="password-error">Sorry! please check password.</div> -->
</div>