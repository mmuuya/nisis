<div id="dash-panel-left" class="">
		<!-- Header Definition -->
	<div class="k-header of-h clear">
		<div class="left">
			<span>Analysis Management Panel</span>
		</div>
		<div class="right">
			<!--<span id="dash-review-iwl" class="pointer" title="Create / Manage IWL(s)"
				data-bind="click:manageIWLs">
				<i class="fa fa-tasks"></i>-->
				<!-- <img id="filter" class="" src="images/menu/funnel.png" alt="Filter Tools" width="20" height="20"/> -->
			<!--</span>			
			<span>&nbsp;</span>
			<span class="pointer" title="Apply Filter"
				data-bind="click:populateTable,enabled:lyrReady">
				<i class="fa fa-filter"></i>-->
				<!-- <img id="filter" class="" src="images/menu/funnel.png" alt="Filter Tools" width="15" height="15"/> -->
			<!--</span>-->
		</div>
	</div>
	<!-- Analysis selections 
		data-bind="source:layers,value:selLayer,events:{}"-->
	<div id="dash-panel-refresh" class="marg of-h">
		<div class="margBottom margTop clear">
			<span>Target Data Set:</span>
		</div>	
		<div id="dash-panel-target-data" class="clear">
			<input id="dash-targets" class="tbl-input"
				data-role="dropdownlist"
				data-bind="source:targetData,value:targetLayer,events:{change:changeLayer}"
				data-text-field="name"
				data-value-field="value"/>
		</div>
		<div class="margBottom margTop clear">
			<span>IWL Filter Tools (optional):</span>
		</div>	
		 
		<!--data-bind="source:layers,value:selLayer,events:{}"-->
		<div id="dash-panel-filter" class="clear">
			<input id="dash-filter" class="tbl-input"
				data-role="dropdownlist"
				data-text-field="name"
				data-bind="source:filterDataSource,enabled:enableDashFilter,events:{change:filterData}"
				data-value-field="value"/>
		</div>

		<div class="margBottom margTop clear">
			<span>Analysis Tools:</span>
		</div>	
		<div id="dash-panel-analysis" class="margBottom clear">
			<input id="dash-analysis" class="tbl-input"
				data-role="dropdownlist"
				data-text-field="name"
				data-bind="source:analysisDataSource,enabled:enableDashAnalysis,events:{change:filterData}"
				data-value-field="value"/>
		</div>
	</div>
</div>

<!--dashboard container -->		
<div id="dashwrapper"  >
	<!---->
	<div id='dashboardwrapper'class=''>
		<div id='dash-ovr-status-chart'></div>
		<div id='dash-ovr-sts-ad-chart'></div>
	</div>
</div>