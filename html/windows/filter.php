<!--<div id="filter-options" class="dialog-options">
	<input type="radio" name="filter" id="filterAttr" class="filter-option" checked="checked"><label for="filterAttr">Attributes</label>
	<input type="radio" name="filter" id="filterArea" class="filter-option"><label for="filterArea">Spatial Area</label>
</div>-->
<div id="filter-layers-options" class="dialog-options-div marg">
	<p>Select * features from <span id="sel-fLayer">....</span></p>
    <select id="f-lyrs" name="filterLayers" class="featLayersList form-control"> 
        <option value="">Select a Layer</option>
    </select>
</div>
<div id="filter-options-div" class="dialog-options-div marg">
	<div id="filterByAttr" class="dialog-option-div">
	    <p>Where</p>
		<div class="margBottom left">
			<div id="filterLayersFields-wrap" class="input-group">
			    <select id="filterLayersFields" name="filterLayersFields" class="form-control">
			        <option value="">Select a field</option>
			    </select>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-refresh"></span>
				</span>
			</div>
			<div class="margLeft left">
				<!--<button id="loadFilterValues" class="btn">Load</button>-->
			</div>
		</div>
		<!--<p>Build your query</p>-->
		<div id="filterByAttr-options" class="clear">
			<div id="filter-criteria" class="left">
				<ul id="f-criteria-list" class="">
					<li><span id="f-eq" class="f-criteria btn margRight margBottom">&#61;</span></li>
					<li><span id="f-neq" class="f-criteria btn margRight margBottom">&#60;&#62;</span></li>
					<li><span id="f-like" class="f-criteria btn margRight margBottom">LIKE</span></li>
					<li><span id="f-lt" class="f-criteria btn margRight margBottom">&#60;</span></li>
					<li><span id="f-lteq" class="f-criteria btn margRight margBottom">&#60;&#61;</span></li>
					<li><span id="f-or" class="f-criteria btn margRight margBottom">OR</span></li>
					<li><span id="f-gt" class="f-criteria btn margRight margBottom">&#62;</span></li>
					<li><span id="f-gteq" class="f-criteria btn margRight margBottom">&#62;&#61;</span></li>
					<li><span id="f-not" class="f-criteria btn margRight margBottom">NOT</span></li>
					<li><span id="f-perc" class="f-criteria btn margRight margBottom">&#37;</span></li>
					<li><span id="f-par" class="f-criteria btn margRight margBottom">( )</span></li>
					<li><span id="f-pipe" class="f-criteria btn margRight margBottom">!&#61;</span></li>
				</ul>
			</div>
			<div id="fField-values-wrap" class="dialog-option-div right">
				<select id="fField-values" class="dropDown">
					<option value="">Load values</option>
				</select>
			</div>
		</div>
		<div class="margTop margBottom">
			<textarea id="filterQuery" class="form-control" placeholder="Enter the where clause of your query string here ..."> </textarea>
		</div>
		<p><button id="runAttrFilter" class="btn">Filter</button>
		<button id="clearAttrFilter" class="btn">Clear Filter</button></p>
	</div>
	<div id="filterByArea" class="dialog-option-div">
		<p>Where features</p>
		<select id="fShapeCriteria" class="dropDown">
			<option value="SPATIAL_REL_INTERSECTS">Intersects</option>
			<option value="SPATIAL_REL_CONTAINS">Contains</option>
			<option value="SPATIAL_REL_CROSSES">Crosses</option>
			<option value="SPATIAL_REL_OVERLAPS">Overlaps</option>
			<option value="SPATIAL_REL_TOUCHES">Touches</option>
			<option value="SPATIAL_REL_WITHIN">Within</option>
		</select>
		<p>Geospatial Query Polygon(s)</p>
		<div class="margBottom">
			<div id="fShapeList-wrap" class="margBottom left">
				<select id="fShapeList" class="listBox" multiple="multiple">
					
				</select>
			</div>
			<div class="right">
				<div class="margBottom right clear">
					<button id="loadGQP" class="btn">Refesh</button>
				</div>
				<div class="margBottom left clear">
					<input type="radio" id="unionGQPs" name="gqpGroup" class="radioBox" title="Union of GQP(s)" checked="checked"> OR
				</div>
				<div class="margBottom left clear">
					<input type="radio" id="interGQPs" name="gqpGroup" class="radioBox" title="Intersection of GQP(s)"> AND
				</div>
			</div>
		</div>
		<p><button id="runSpFilter" class="btn">Filter</button>
		<button id="clearSpFilter" class="btn">Clear Filter</button></p>
	</div>
	<p id="filterMsg"> </p>
    <div id="filterResults"> </div>
</div>