<div id="dir-tabs" data-role="tabstrip">
	<ul>
		<li class="k-state-active">Locations</li>
		<li>Directions</li>
	</ul>
	<!-- Geocoding Tab -->
	<div class="geocode-tab">
		<div id="geocode-address" class="">
			<div class="marg clr">
				<ul class="tb-list">
					<li><label class="w-100">Street Address: </label></li>
					<li>
						<input type="text" class="w-410 k-textbox" 
							data-bind="value:streetAddress" 
							placeholder="800 Independence Avenue SW" />
					</li>
					<li>
						<label class="w-200">City: </label>
						<label class="w-200 m-l10">State: </label>
					</li>
					<li>
						<input type="text" class="w-200 k-textbox" 
							data-bind="value:city" 
							placeholder="Washington" />
						<input type="text" class="w-200 m-l10 k-textbox" 
							data-bind="value:state" 
							placeholder="DC" />
					</li>
					<li>
						<label class="w-200">Zip Code: </label>
						<label class="w-200 m-l10">Country: </label>
					</li>
					<li>
						<input type="text" class="w-200 k-textbox" 
							data-bind="value:zipcode" 
							placeholder="20024" />
						<input type="text" class="w-200 m-l10 k-textbox" 
							data-bind="value:country" 
							placeholder="US" />
					</li>
					<li>
						<label>
							<input type="checkbox" class="" 
							data-bind="attr{checked:useMapExtent}" /> 
							Limit search to map Extent </label>
					</li>
				</ul>
			</div>
			<div class="marg clr">
				<span class="marg"
					data-bind="text:locMsg"></span>
			</div>
			<div class="marg clr">
				<button class="w-100" 
					data-role="button"
					data-bind="enabled:btnEnabled,events{click:geocodeAddress}">
						<i class="fa fa-search"></i> Find</button>
				<button class="w-100" 
					data-role="button"
					data-bind="enabled:btnEnabled,events{click:resetAddress}">
						<i class="fa fa-times"></i> Clear</button>
			</div>
			<div class="marg clr" data-bind="visible:showResults">
				<p class="ft-b">Matched locations: (<span data-bind="text:countResults"></span>)</p>
			</div>
			<div class="h-300 marg m-t10 clr of-a" 
				data-bind="visible:showResults">

				<ul id="geocode-results" class="tb-list"></ul>
			</div>
		</div>
	</div>
	<!-- Directions Tab -->
	<div class="directions-tab">
		<div id="directions-content"></div>
	</div>
</div>