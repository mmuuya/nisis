<!-- <div id="features-tabs" data-role="tabstrip" data-bind="events:{load:loadMyFeaturesTab,select:onSelectTabStrip}"> -->
<div id="features-tabs" data-role="tabstrip" data-bind="events:{load:activateShapesManagementTool,select:onSelectTabStrip}">
	<ul>
		<li class="f-tab k-state-active">My Features</li>
		<li class="f-tab">Group Features</li>
		<li class="f-tab">Arrange Order</li>
	</ul>
	<div id="myFeaturesContent">
		<div class="f-menu">
			<span class="details"><span id="items">&nbsp;</span></span>
			<ul>
				<li><button title="Refresh" id="fmrefresh" data-bind="events:{click:refreshData}"><i class="fa fa-refresh"></i></button></li>
				<li><button title="Create folder" id="fmcreatefolder" data-bind="events:{click:createFolder}"><i class="fa fa-folder-o"></i></button></li>
				<li><button title="Edit item" id="fmedit" data-bind="events:{click:editItem}"><i class="fa fa-pencil"></i></button></li>
				<li><button title="Delete item" id="fmdel" data-bind="events:{click:deleteItem}"><i class="fa fa-trash-o"></i></button></li>
				<li><button title="Go to shape" id="fmFlyTo" data-bind="events:{click:flyToFeature}"><i class="fa fa-crosshairs"></i></button></li>				<!-- <li> -->
				<li>
					<button id="fmshare" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-share-alt"></i>
						<div class="dropdown-menu">
							<ul>
								<li><a href="#" title="Share with users"><i class="fa fa-user fa-fw" id="user" data-bind="events:{click: shareItem}"></i></a></li>
								<li><a href="#" title="Share with groups"><i class="fa fa-users fa-fw" id="group" data-bind="events:{click: shareItem}"></i></a></li>
							</ul>
						</div>
					</button>
				</li>
			</ul>
		</div>

		<div id='myFeaturesTree'
			data-role="treeview"
			data-drag-and-drop="true"
			data-text-field="ITEMNAME"
			data-value-field="ITEMID"
			data-imageUrl-field="IMAGE"
			data-spritecssclass-field="type"
			data-load-on-demand="false"
			data-checkboxes="{checkChildren:true}"
			data-scrollable="true"
			data-bind="source:myFeaturesDataSource, events:{check:onCheck,select:selectMyFeature,dragstart:dragStartItem,drop:dropItem}"
			data-autosync="true">
		</div>

	</div>
	<div id="groupFeaturesContent">
		<!-- Menu -->
		<div class="f-menu">
			<span class="details"><span id="items">&nbsp;</span></span>
			<ul>
				<li><button title="Refresh" id="fmrefresh2" data-bind="events:{click:refreshData}"><i class="fa fa-refresh"></i></button></li>
				<li><button title="Create folder" id="fmcreatefolder2" data-bind="events:{click:createFolder}"><i class="fa fa-folder-o"></i></button></li>
				<li><button title="Edit item" id="fmedit2" data-bind="events:{click:editItem}"><i class="fa fa-pencil"></i></button></li>
				<li><button title="Delete item" id="fmdel2" data-bind="events:{click:deleteItem}"><i class="fa fa-trash-o"></i></button></li>
				<li><button title="Go to shape" id="fmFlyTo2" data-bind="events:{click:flyToFeature}"><i class="fa fa-crosshairs"></i></button></li>
				<li>
					<button id="fmshare2" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<i id="fmshare2" class="fa fa-share-alt"></i>
						<div class="dropdown-menu">
							<ul>
								<li><a href="#" title="Share with users"><i class="fa fa-user fa-fw" id="user" data-bind="events:{click: shareItem}"></i></a></li>
								<li><a href="#" title="Share with groups"><i class="fa fa-users fa-fw" id="group" data-bind="events:{click: shareItem}"></i></a></li>
							</ul>
						</div>
					</button>
				</li>
			</ul>
		</div>

		<!-- <div>Select a Group:</div> -->
		<div class="g-container">
			<input id="groupsDropDown" data-role="dropdownlist"
						   data-text-field="GNAME"
						   data-value-field="GROUPID"
						   data-bind="value: selectedGroup, visible:groupsTreeVisible,
										source: groupsDatasource,
										events: {change:groupsDropdownOnChange}"/>
		</div>
		<!-- <div>GROUP FEATURES:</div> -->
		<div id='groupFeaturesTree'
			data-role="treeview"
			data-drag-and-drop="true"
			data-text-field="ITEMNAME"
			data-value-field="ITEMID"
			data-imageUrl-field="IMAGE"
			data-spritecssclass-field="type"
			data-load-on-demand="false"
			data-checkboxes="{checkChildren:true}"
			data-bind="source:groupFeaturesDataSource, visible:groupsTreeVisible, events:{check:onCheck,select:selectGroupFeature,dragstart:dragStartItem,drop:dropItem}"
			data-autosync="true">
		</div>
	</div>
	<div id="shapesManagerContent">
		<!-- Menu -->
		<div class="f-menu">
			<span class="details"><span id="items">&nbsp;</span></span>
			<ul>
				
				<li><button title="Go to shape" id="fmFlyTo3" data-bind="events:{click:flyToFeature}"><i class="fa fa-crosshairs"></i></button></li>
				
			</ul>
		</div>

		<div id='shapes-manager-div'
			data-title='Shapes Management'
			data-bind="events:{load:activateShapesManagementTool, click:shapesManagerID}">

			<div id='shapes-manager-content'>
				<div id='shapes-manager-list' ></div>
				<div id='shapes-manager-pager' ></div>
				<div id='shapes-manager-actions-bar'>
					<button id="sendToBack" data-bind="events:{click:moveShape}" tabindex="0" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button" >
						<span class="k-icon"></span>Send To Back
					</button>
					<button id="sendBackward"  data-bind="events:{click:moveShape}" tabindex="1" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button"  >
						<span class="k-icon"></span>Send Backward
					</button>
					<button id="bringForward"  data-bind="events:{click:moveShape}" tabindex="2" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button" >
						<span class="k-icon"></span>Bring Forward
					</button>
					<button id="bringToFront"  data-bind="events:{click:moveShape}" tabindex="3" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button" >
						<span class="k-icon"></span>Bring to Front
					</button>
				</div>
			</div>
		</div>
	</div>
</div>