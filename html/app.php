<?php
$isAdmin = userIsAdmin();
$isGroupAdmin = userIsGroupAdmin();
$isTFRAdmin = userIsTFRAdmin();
//check url
$url = $_SERVER["SERVER_NAME"];
?>
<!-- map styles -->
<link rel="stylesheet" type="text/css" href="styles/app.css" />
<!-- feature templates styles -->
<link rel="stylesheet" type="text/css" href="styles/templates.css" />
<!-- app header -->
<div id="app">
    <div id="openBtn">
		<span id="open-menu" class="user-action" title="Open Menu">
			<i class="fa fa-arrow-circle-right"></i>
		</span>
	</div>
	<div id='header' class='k-header noselect'>
		<div id="title-box">
		<?php
			if(isset($url)){
				if($url == 'dotmap.dot.gov'){
				echo'<div class="title-logo" data-bind="click:openDOTPage">
						<img id="app-dot-image" src="./images/logos/dot_app_logo.png" alt="DOTMap">
					</div>';
				}else{
				echo'<div class="title-logo" data-bind="click:openFAAPage">
						<img id="app-faa-image" src="./images/logos/nisis_logo.png" alt="NISIS">
					</div>';
				}
			}
		?>
		</div>
		<!-- main menu -->
		<div id="menu-bar" class='menu-bar'>

		</div>
		<!-- admin -->
		<div id="search-and-admin" class="right">
			<div title="User name" class="uname-bar">
				<?php
					echo $_SESSION['username'];
				?>
			</div>
			<div id="user-controls" class="right">
				<ul data-role="menu">
					<?php
					if($isAdmin || $isGroupAdmin) {
					?>
					<li data-target="admin-panel-div"
						data-bind="click:toggleWindow">
						<span id="admin-menu-link" class="user-action" title="User Administration">
							<i class="fa fa-users"></i>
						</span>
					</li>
					<li data-target="change-password-div"
						data-bind="click:toggleWindow">
						<span id="change-password-link" class="user-action" title="Change Password">
							<i class="fa fa-cog"></i>
						</span>
					</li>
					<?php
					}
					?>
					<li data-bind="click:toggleSearchBox">
						<span id="search-btn" class="user-action" title="Search">
							<i class="fa fa-search"></i>
						</span>
					</li>
					<li data-bind="click:toggleFullScreen">
						<span id="full-screen" class="user-action" title="Full Screen">
							<i class="fa fa-arrows-alt"></i>
						</span>
					</li>
					<li data-bind="click:clpseMenu">
						<span id="collapse-header" class="user-action" title="Collapse Header">
							<i class="fa fa-arrow-circle-left"></i>
						</span>
					</li>
					<li data-bind="click:userLogOut">
						<span id="logout" class="user-action" title="Sign Out">
							<i class="fa fa-power-off"></i>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Search -->
	<div id="search" class="search right hide"
		data-bind="visible:showSearch">
		<input type="search" id="search-box" class="search-box"
			data-role="autocomplete"
			data-placeholder="Find features or places"
			data-text-field='label'
			data-text-value='value'
			data-min-length="2"
			data-filter="contains"
			data-suggest="true"
			data-serverFiltering="true"
			data-bind="source:searchSource,value:searchKey,visible:showSearch,
				events:{select:searchFeatures,focus:extendSearch}" />
	</div>
	<!-- toggle app header -->
	<div id="toggle-header" class="hide" data-bind="click:toggleMapHeader">
		<span><i class="fa fa-toggle-right fa-2x"></i><span>
	</div>
	<!-- Map Content -->
	<div id="map" class="noselect">
		<div id='user-info' class='hide'>
			<?php
			    echo "<input type='hidden' id='user-userid' value='" . $_SESSION['userid'] . "'/>";
				echo "<input type='hidden' id='user-username' value='" . $_SESSION['username'] . "'/>";
				echo "<input type='hidden' id='user-full-name' value='" . $_SESSION['name'] . "'/>";
			?>
		</div>
		<!-- map navigations -->
	    <div id="nav-div" class="hide" data-bind="visible:mapReady">
	        <div id="panDiv" class="navItem">
	            <div id="panUp" class="panDivs navItem" data-bind="click:panUp">
	            	<img alt="Pan Up" src="images/nav/up.png" width="20" height="20"/>
				</div>
	            <div id="panUpperRight" class="panDivs navItem" data-bind="click:panUpperRight"> </div>
	            <div id="panRight" class="panDivs navItem" data-bind="click:panRight">
	            	<img alt="Pan Right" src="images/nav/right.png" width="20" height="20"/>
				</div>
	            <div id="panLowerRight" class="panDivs navItem" data-bind="click:panLowerRight"> </div>
	            <div id="panDown" class="panDivs navItem" data-bind="click:panDown">
	            	<img alt="Pan Down" src="images/nav/down.png" width="20" height="20"/>
				</div>
	            <div id="panLowerLeft" class="panDivs navItem" data-bind="click:panLowerLeft"> </div>
	            <div id="panLeft" class="panDivs navItem" data-bind="click:panLeft">
	            	<img alt="Pan Left" src="images/nav/left.png" width="20" height="20"/>
				</div>
	            <div id="panUpperLeft" class="panDivs navItem" data-bind="click:panUpperLeft"> </div>
	            <div id="fullExtent" class="panDivs navItem" data-bind="click:fullExtent">
	            	<img alt="Full Extent" src="images/nav/world3.png" width="20" height="20"/>
				</div>
	        </div>
	        <div id="extentHistory" class="">
	            <div id="nav-previous" class="hisDivs navItem" data-bind="click:previousExtent">
	            	<!-- <img alt="Previous" src="images/nav/previous7.png" width="20" height="15"/> -->
	            	<span><i class="fa fa-chevron-left fa-2x"></i></span>
				</div>
	            <div id="nav-next" class="hisDivs navItem" data-bind="click:nextExtent">
	            	<!-- <img alt="Next" src="images/nav/next7.png" width="20" height="15"/> -->
	            	<span><i class="fa fa-chevron-right fa-2x"></i></span>
				</div>
	        </div>
	        <div id="zoomSlider" class="panDivs navItem">
	        	<!--<div id="zoomRules"></div>-->
			</div>
	        <!-- <div id="info" class="navDivs navItem" data-bind="click:info">
				<span><i class="fa fa-info-circle"></i></span>
			</div> -->
			<div id="navToggleTools">
		        <div id="nav-pan" class="navDivs navItem clear" data-bind="click:pantool">
		        	<span><i class="fa fa-arrows"></i></span>
				</div>
		        <div id="nav-zoominbox" class="navDivs navItem clear" data-bind="click:zoomInBox">
		        	<span><i class="fa fa-plus-square"></i></span>
				</div>
		        <div id="nav-zoomoutbox" class="navDivs navItem clear" data-bind="click:zoomOutBox">
		        	<span><i class="fa fa-minus-square"></i></span>
				</div>
			</div>
	    </div>
	    <!-- map basemaps -->
		<div id='basemaps-div' class='dialog'
			data-role='window'
			data-title='Basemaps'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='320'
			data-visible='false'
			data-position='{top:55,left:10}'>
            <div id="refLayerToggler" class="disabled">
                <label class="check">
                    <input checked="checked" type="checkbox">
                    <div class="boxCheck"></div>
                    <span class="boxText">Show Labels</span>
                </label>
            </div>
			<div id='basemaps-content' class='marg'>
			</div>
		</div>
		<!-- map layers -->
		<div id='layers-div' class='dialog'
			data-role='window'
			data-title='Map Layers'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='600'
			data-visible='false'
			data-position='{top:55,left:60}'>

			<div id='layers-content'>
				<?php include "windows/layers.php";?>
			</div>
		</div>
		<!-- My features / Group Features -->
		<div id='features-div' class='dialog'
			data-role='window'
			data-title='Manage Features'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='440'
			data-height='480'
			data-visible='false'
			data-position='{top:55,left:60}'>

			<div id='features-content'>
				<?php include "windows/features.php";?>
			</div>
		</div>
		<!-- map legend -->
		<div id='legend-div' class='dialog'
			data-role='window'
			data-title='Map Legend'
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='350'
			data-maxWidth='350'
			data-height='300'
			data-maxHeight='500'
			data-visible='false'
			data-position='{top:55,left:110}'>

			<div id='legend-content' class='marg'></div>
		</div>
		<!-- map view options -->
		<div id='view-options-div' class='dialog'
			data-role='window'
			data-title='View Options'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='250'
			data-height='auto'
			data-visible='false'
			data-position='{top:55,left:110}'>

			<div id='view-options-content' class='marg'>
				<ul data-role='menu' data-orientation='vertical'>
					<li class='user-action' data-action='hide'
						data-bind="click:showTblViewFilter">
						<span>Filtered Objects (Hide)</span></li>
					<li class='user-action' data-action='gray'
						data-bind="click:showTblViewFilter">
						<span><span>Filtered Objects (De-emphasize)</span></li>
					<li class='user-action' data-action='hide'
						data-bind="click:showIwlFeatures">
						<span >IWL(s) Objects (Hide)</span></li>
					<li class='user-action' data-action='gray'
						data-bind="click:showIwlFeatures">
						<span><span>IWL(s) Objects (De-emphasize)</span></li>
					<li class='user-action'
						data-action='clear'
						data-bind="click:resetDefaultView">
						<span>Reset to default View</span></li>
				</ul>
			</div>
		</div>
		<!-- drawing shapes -->
		<div id="drawing-div" class="dialog"
			data-role='window'
			data-title="Shape Tools"
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='600'
			data-height='auto'
			data-maxHeight='500'
			data-visible='false'
			data-position='{top:55,left:160}'>

			<div id="drawing-content" class="">
				<?php include "windows/shapetools.php";?>
			</div>
		</div>
		<!-- shapes management -->
		<!-- <div id='shapes-manager-div' class='dialog'
			data-role='window'
			data-title='Shapes Management'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='600'
			data-height='auto'
			data-visible='false'
			data-position='{top:55,left:160}'
			data-bind="events:{open:shapesManager.activateShapesManagementTool}">

			<div id='shapes-manager-content' class='marg'>
				<div id='shapes-manager-list' ></div>
				<div id='shapes-manager-pager' ></div>
				<div id='shapes-manager-actions-bar' >
					<button id="sendToBack" data-bind="events:{click:moveShape}" tabindex="0" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button" >
						<span class="k-icon"></span>Send To Back
					</button>
					<button id="sendBackward"  data-bind="events:{click:moveShape}" tabindex="1" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button"  >
						<span class="k-icon"></span>Send Backward
					</button>
					<button id="bringForward"  data-bind="events:{click:moveShape}" tabindex="2" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button" >
						<span class="k-icon"></span>Bring Forward
					</button>
					<button id="bringToFront"  data-bind="events:{click:moveShape}" tabindex="3" aria-disabled="false" role="button" class="k-button k-button-icontext" data-role="button" type="button" >
						<span class="k-icon"></span>Bring to Front
					</button>
				</div>
			</div>
		</div> -->
		<!-- measurements -->
		<div id="measure-div" class="dialog"
			data-role='window'
			data-title="Measurements"
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='600'
			data-height='auto'
			data-maxHeight='500'
			data-visible='false'
			data-position='{top:55,left:160}'>

			<!-- Measurement tools -->
			<div id="measuretools" class="clear">
				<span>Select a tool to activate measurement</span>
				<div id="measurements" class="clear"></div>
			</div>
		</div>
		<!-- Defunct Filter View -->
		<div id="filter-div" class="dialog"
			data-role='window'
			data-title="Filter Features"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='600'
			data-minWidth='500'
			data-maxWidth='100%'
			data-height='auto'
			data-minHeight='50%'
			data-maxHeight='100%'
			data-visible='false'
			data-iframe='true'
			data-position='{top:55,left:210}'
			data-bind="events:{resize:resizeFilterTable}">

			<div id="filter-content" class="clear of-h">
				<?php //include "windows/filter.php";?>
			</div>
		</div>
		<!-- Directions & Navigations -->
		<div id='directions-div' class='dialog'
			data-role='window'
			data-title='Locations &amp; Directions'
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='false'
			data-width='500'
			data-maxWidth='500'
			data-height='auto'
			data-maxheight='90%'
			data-visible='false'
			data-position='{top:55,left:260}'>

			<div class'marg clear'>
				<?php include "windows/locations.php";?>
			</div>
		</div>

		<!-- Dasboard-view -->
		<div id='dashboard-div' class='dialog'
			data-role='window'
			data-center="true"
			data-title='Dashboard View'
			data-actions='["Minimize","Close"]'
			data-resizable='true'
			data-min-width='800px'
			data-width='60%'
			data-maxWidth='80%'
			data-min-height='330px'
			data-maxheight='100%'
			data-visible='false'
			data-bind="events:{resize:resizeSplitter}"
			data-position='{top:55,left:380}'>
			<br/>
			<div id="dashboard-content"
			    class="of-h"
			    data-reset="true"
			    data-role="splitter"
			    data-orientation="horizontal"
			    data-bind="events:{contentLoad:dashSplitterReady}"
			    data-panes="[{size:'250px',collapsed:true,collapsible:true,resizable:false,scrollable:false},
			    {size:'auto',min:'500px',collapsed:false,collapsible:false,resizable:false,scrollable:true}]">

			    <?php include "windows/dashboardview.php";?>
			</div>
		</div>
		<!-- table-view -->
		<div id="tableview-div" class="dialog"
			data-role="window"
			data-center="true"
			data-title="Table View"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='80%'
			data-min-width='60%'
			data-height='60%'
			data-min-height='60%'
			data-visible='false'
			data-position='{top:55,left:310}'>

			<div id="tableview-content" class="of-h"
				data-reset="true"
				data-role="splitter"
				data-orientation="horizontal"
				data-bind="events:{contentLoad:tblSplitterReady}"
				data-panes="[{size:'350px',collapsed:true,collapsible:true,resizable:false,scrollable:true},
				{size:'auto',min:'500px',collapsed:false,collapsible:false,resizable:false,scrollable:false}]">

					<?php include "windows/tableview.php";?>

			</div>
		</div>
		<!-- alerts view -->
		<div id="alerts-div" class="dialog"
			data-role='window'
			data-center='true'
			data-title="Reminders"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='1500'
			data-maxWidth='100%'
			data-maxHeight='100%'
			data-visible='false'
			data-position='{top:55,left:400}'>

			<?php include "alerts/html/main_panel.php"; ?>
		</div>
		<!-- Help Window -->
		<!-- <div id='help-div' class='dialog'
			data-role='window'
			data-title='User Guide'
			data-actions='["Close"]'
			data-resizable='false'
			data-width='60%'
			data-height='60%'
			data-visible='false'>
			<div>Click the url to begin your download:<br/>
			<?php
				//$helpURL = $root . '/html/help.php';
				//echo '<a target="_blank" href="' . $helpURL . '">';
				//echo $helpURL;
				//echo '</a>';
			?>
			</div>
		</div> -->
		<!-- history view -->
		<div id="history-div" class="dialog"
			data-role='window'
			data-center='true'
			data-title="History"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-maxWidth='100%'
			data-maxHeight='100%'
			data-visible='false'
			data-position='{top:150,left:200}'>

			<div id="right-pane">
					<div id="historyTable"></div>
					<script type="text/xkendo-template" id="history-toolbar-template">
						<div class="toolbar">
							<span class="k-textbox search-template">
								<input type="text" id="searchbox"
								placeholder="Search"/>
							</span>

							<button id="tbl-quicksearch-button" title="Search"
								data-role="button">
								<span><i class="k-icon k-i-search"></i></span>
							</button>

							<button id="export-button"
								title="Export to CSV"
								data-role="button"
								style="float:right">Export to CSV
								<span>
									<i class="fa fa-download"></i>
								</span>
							</button>
							<br>
						</div>



						<div id="history-multiselect-filters">
							<div class="left">
									<input id="iwl-multiselect" style="width:300px;margin-top:5px"></input>
							</div>
							<div class="marg left">
									<span id="iwl-refresh" class="k-icon k-i-refresh" style="margin-top:5px"></span>
							</div>
							<div class="marg right">
									<span id="shape-filter-refresh" class="k-icon k-i-refresh" style="margin-top:5px"></span>
							</div>
							<div class="right">
									<input id="shape-filter-multiselect" style="width:300px;margin-top:5px"></input>
							</div>
						</div>



					</script>
			</div>
		</div>
		<!-- TFR Builder -->
		<div id="tfr-div" class="dialog"
			data-role='window'
			data-title="TFR Builder"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='900'
			data-min-width='650'
			data-maxWidth='100%'
			data-height='80%'
			data-maxHeight='100%'
			data-minHeight='700'
			data-visible='false'
			data-position='{top:55,left:10}'
			data-refresh="dialogReady" >

			<?php include "windows/tfr/tfr-builder.php"; ?>
		</div>
		<!--- IWLS -->
		<div id="iwl-div" class="dialog"
			data-role="window"
			data-center="true"
			data-title="Incident Watch List"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='800'
			data-min-width='400'
			data-visible='false'>
				<?php include "windows/iwl.php";?>
		</div>
		<!-- print widget-->
		<div id='print-div' class='dialog'
			data-role='window'
			data-title='Print Map'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='300'
			data-max-width='300'
			data-height='auto'
			data-visible='false'
			data-position='{top:55,left:10}'>

			<div id='print-content'>
				<div class="marg">
					<input type='text' id="print-title" class='k-textbox print-input'
						placeholder='Enter a title'
						data-bind="value:title"/>
				</div>
				<div class="marg">
					<select id="page-format" class="print-input"
						data-role='dropdownlist'
						data-bind="source:pageLayouts,value:pageLayout,events:{change:setPrint}"
						data-text-field="label"
						data-value-field="value">
					</select>
				</div>
				<div class="marg">
					<select id='file-format' class="print-input"
						data-role="dropdownlist"
						data-bind="source:fileFormats,value:fileFormat,events:{change:setPrint}"
						data-text-field="label"
						data-value-field="value">
					</select>
				</div>
				<div class="marg">
					<button id='printing' class="print-input"
						data-role="button"
						data-bind="enabled:isReady,click:print">Print</button>
				</div>
				<div class="marg">
					<span id="print-msg"
						data-bind="text:printMsg"></span>
				</div>
				<div class="marg clear">
					<a class="print-link" data-bind="text:printName,attr:{href:printOutput},visible:printReady"
						target="_blank"></a>
				</div>
			</div>
		</div>
		<!-- Profile windows -->
		<div id="nisis-profiles" class="dialog"
			data-role="window"
			data-title="NISIS CI Profile View"
			data-actions='["Minimize","Maximize","Close"]'
			data-resizable='true'
			data-width='1000'
			data-minWidth='800'
			data-height='600'
			data-minHeight='600'
			data-visible='false'
			data-position='{top:55,left:310}'>

			<div id="profiles-content" class="p-content marg of-h">

			</div>
		</div>
		<!-- user profile widget -->
		<!-- <div id='profile-div' class='dialog'
			data-role='window'
			data-title='User Profile'
			data-actions='["Minimize","Close"]'
			data-resizable='false'
			data-width='500'
			data-maxWidth='500'
			data-visible='false'
			data-position='{top:55,left:10}'>

			<div id="profile-content" class="">

			</div>
		</div> -->
		<!-- data blocks -->
		<div id="data-blocks"></div>
		<!-- costom scale map -->
		<div id="dpi-info" class="clear of-h" style="width:1in,display:hidden"></div>
		<!-- costom scale map -->
		<!-- <div id="scale-bar" class="clear of-h" data-bind="style:{width:scaleBarWidth}">
			<div id="scale-value" data-bind="text:scaleValue"></div>
		</div> -->
	</div>
	<!-- app footer -->
	<div id='footer' class="k-header">
		<!-- Left Side -->
		<div class="footer-left w-a marg left of-h">
			<!-- Scale -->
			<div id="numScale" class="left">
				<span>
					<span id='numScaleCst' data-bind="text:scaleBarWidth"></span>
					<!-- <span id='numScaleVal' data-bind="text:scaleNumValue"></span>  -->
					<span id='numScaleUnits' class='pointer' data-bind="click:showNumScaleUnits,text:scaleLabel"></span>
				</span>
			</div>
			<!-- Scale Units -->
			<div id="num-scale-units" class="hide left">
				<ul data-role="menu"
					data-orientation="vertical">
					<!-- <li data-unit="cm" data-bind="click:setNumScaleUnit">Centimeters</li> -->
					<li data-unit="ft" data-bind="click:setNumScaleUnit">Feet</li>
					<!-- <li data-unit="in" data-bind="click:setNumScaleUnit">Inches</li> -->
					<li data-unit="km" data-bind="click:setNumScaleUnit">Kilometers</li>
					<li data-unit="me" data-bind="click:setNumScaleUnit">Meters</li>
					<li data-unit="mi" data-bind="click:setNumScaleUnit">Miles</li>
					<!-- <li data-unit="mm" data-bind="click:setNumScaleUnit">Milimeters</li> -->
					<li data-unit="nm" data-bind="click:setNumScaleUnit">Nautical Miles</li>
					<!-- <li data-unit="yd" data-bind="click:setNumScaleUnit">Yard</li> -->
				</ul>
			</div>
		</div>
		<!-- Right Side -->
		<div class="footer-right w-a marg right of-h">
			<!-- UTC Clock -->
			<div id="utc-clock" class="m-r15 left">
				<span id="live-clock" data-bind="text:appClock">April 9, 2015 09:43:25Z</span>
			</div>
			<!-- Mouse Position lat/lon-->
			<div id="mouseLatLon" class="footer-section left">
				<span>
				<span data-bind="visible:mouseGARS,text:mouseGARSCoords"></span>
				<span data-bind="visible:mouseUTM,text:mouseUTMCoords"></span>
				<span data-bind="visible:mouseLatLon">
				Lat: <span id="mouseLat" data-bind="text:mouseLatitude"></span> &nbsp;&nbsp;
				Lon: <span id="mouseLon"data-bind="text:mouseLongitude"></span> &nbsp;&nbsp;
				</span>
					 <span id="mCoordsUnit" class="pointer"
						data-bind="text:mouseLatLonUnits,click:showMouseCoordUnits"></span>
				</span>
			</div>
			<!-- Mouse Position Units -->
			<div id="mouse-pos-units" class="left hide">
				<ul data-role="menu"
					data-orientation="vertical" data-target="mCoordsUnit">
					<li data-unit="DD" data-bind="click:setMouseCoordUnits" title="Decimal Degrees">DD</li>
					<li data-unit="DDM" data-bind="click:setMouseCoordUnits" title="Degrees Decimal Minutes">DDM</li>
					<li data-unit="DMS" data-bind="click:setMouseCoordUnits" title="Degrees Minutes Seconds">DMS</li>
					<li data-unit="GARS" data-bind="click:setMouseCoordUnits" title="Global Area Reference System">GARS</li>
					<!-- <li data-unit="UTM" data-bind="click:setMouseCoordUnits" title="Universal Transverse Mercator">UTM</li> -->
				</ul>
			</div>
		</div>
	</div>
	<?php
	if($isAdmin || $isGroupAdmin) {
		?>
		<!-- Admin panel -->
		<div id='admin-panel-div' class="dialog of-h"
			data-title='User Administration'
			data-role='window'
			data-actions='["Minimize","Maximize","Close"]'
			data-modal='true'
			data-height='547'
	 		data-width='75%'
			data-visible='false'>
		</div>
		<div id='change-password-div' style='display:none' class='dialog of-h'
			data-title='Change password'
			data-role='window'
			data-actions='["Close"]'
			data-modal='true'
			data-visible='false'>
			
				<?php include "windows/changePassword.php";?>
			
			
		</div>
	<?php
	}
	?>

	<div id="profileArea" class="dialog"><!-- facility profile placeholder --></div>
	<div id="iwlFormArea" class="dialog"><!-- profile -> iwl assignment form placeholder --></div>
	<div id="historyArea" class="dialog"><!-- profile field history placeholder --></div>
	<div id="imageUploadArea" class="dialog"><!-- profile snapshot image upload placeholder --></div>
</div>
<!-- used for inch > px convertion -->
<div id="screen-ref" style="width:1in"></div>


<script type="text/javascript" src="scripts/history.js"></script>
<!-- used to parse kml files-->
<script type="text/javascript" src="deps/jquery.xml2json.js"></script>
<script type="text/javascript" src="deps/togeojson.js"></script>
<!-- mapview -->
<script type="text/javascript" src="scripts/mapview.js"></script>
<script type="text/javascript" src="scripts/mapview.layers.js"></script>
<script type="text/javascript" src="scripts/mapview.util.js"></script>
<script type="text/javascript" src="scripts/app.js"></script>
