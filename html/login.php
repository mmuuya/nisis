<?php
	require "lib/check_pw_token.php";
?>
<script type="text/javascript" src="scripts/login.js"></script>
<script type="text/javascript" src="admin/scripts/reset.js"></script>


<!-- Modal window for intro warning -->
<div class="lgn-ol"></div>
<div class="lgn-mdl">
	<div class="lgn-mdl-hdr">
		Welcome to NISIS
	</div>
	<div class="lgn-mdl-bdy">
		<?php include 'faa_dsclmr.html'; ?>
	</div>
	<div class="lgn-mdl-ftr">
		<a class="lgn-mdl-btn" id="continue">Continue</a><a class="lgn-mdl-btn" id="cancel">Cancel</a>
	</div>
</div>
<!-- end of Modal -->

<!-- Main content -->
<div id="welcome" class=""></div>

<div class="flex-container nisis">
	<!-- Warning text -->
	<div class="flex-body">
		<div class="flex-item">
			<?php include "faa_warning.html"; ?>
		</div>
		
		<!-- Form -->
		<div class="flex-item">
			<div class="form form-shadow" id="login-form">
				<div class="form-header">
					<h1>NISIS</h1>
					<h3>NAS Integrated Status Insight System</h3>
				</div>

				<div class="field">
					<input type="text" id="username" name="username" class="form-input" placeholder="Enter username" required="required" disabled>
					<label for="username" title="Username">Username</label>
				</div>

				<div class="field">
					<input type="password" id="password" name="password" class="form-input" placeholder="Enter password"  required="required" disabled>
					<label for="password" title="Password">password</label>
				</div>
				
				<div class="button-field">
					<a id="reset" class="login-button clear">Clear</a>
					<a id="submit" class="login-button submit">Submit</a>
				</div>
				
			
				<div class="login-footer">Forgot your password? <a id="newpass"> Reset here</a></div>				
			</div>
		</div>
	</div>
	<!-- Footer -->
	<footer>
		<div><img src="images/logos/FAA_logo.png" alt=""><span>Federal Aviation Administration</span></div>	
	</footer>
</div>





<!-- Reset Password Forms -->
<div id="resetForms" class="dialog"
	data-role="window"
	data-title="Reset Password"
	data-visible="false"
	data-modal="true"
	data-width="50%">

<?php
	$showDefaultForm = true;
	if (array_key_exists('token', $_GET)){
		if(checkPwToken($_GET['token'])){
			$showDefaultForm = false;
?>
			<div id="newPwForm">
				<div class="form" id="pass-reset-form">
					Your new password must:
					<ul>
						<li>Be longer than 8 characters</li>
						<li>Contain at least one upper-case character</li>
						<li>Contain at least one lower-case character</li>
						<li>Contain at least one number</li>
						<li>Contain at least one special character: !#@$%&+=?:^~{}[]<>()</li>
					</ul>
					<span id="inputToken" style="display:none"><?php echo $_GET['token'] ?></span>

					<div class="field">
						<input type="password" name="password" class="form-input" placeholder="Enter New Password" data-bind="value:nupass1"/><br/>
					</div>

					<div class="field">
					<input type="password" class="form-input" placeholder="Confirm New Password" data-bind="value:nupass2"/><br/>
					</div>

					<div class="button-field">
					<button data-bind="click:submitNuPass" class="login-button">Change Password</button><br/>
					</div>

					<div id="pwErrArea" style="color:red;font-size:small;padding-top:5px">&nbsp;</div>
				</div>
			</div>
<?php
		} else {
			echo '<span id="badToken" style="display:none"></span>';
		}
	}
	if($showDefaultForm){
?>
		<div id="getNameEmailForm">
			<div class="form" id="pass-reset">

				<div class="field">
					<input type="text" name="username" class="form-input" placeholder="Enter username" data-bind="value: username" required="required"/>
					<label for="username">Username</label>
				</div>
				
				<div class="field">
					<input type="email" name="email" class="form-input" placeholder="Enter Email" data-bind="value:email" required="required"/>
					<label for="email">Email</label>
				</div>

				<div class="button-field">
				<button data-bind="click:requestToken" class="login-button email">Send Email</button>
				</div>

			</div> <!-- end of form -->
		</div>
<?php
	}

	//write out a hidden message which reset.js will display if it exists in DOM
	if(isset($_SESSION['pwResetMsg'])){
		echo '<span id="resetPwMsg" style="display:none">' . $_SESSION['pwResetMsg'] . '</span>';
		unset($_SESSION['pwResetMsg']);
	}
?>

</div>