<?php
require '../settings/settings.php';

if(isset($db)){
	if(!$db){
		$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
	}
} else {
	$db = oci_pconnect($dbuser, $dbpass, "//$dbhost:$dbport/$dbname");
}

if (!$db) {
    $e = oci_error();
    report(($e['message']), "error");
}

/*$id=0;
$sql = "SELECT * FROM SYMBOL_SIMPLELINE WHERE OUTLINEID IS NOT NULL";
$parsed_sql = oci_parse($db,$sql);

oci_execute($parsed_sql);
oci_fetch_all($parsed_sql, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);
foreach($results as $entry) {
	$id++;
}
$id++;

oci_free_statement($parsed_sql);*/


if ( isset($_POST['symStyle']) && isset($_POST['symColor']) ) {
	$symStyle=$_POST['symStyle'];
	$symColor=$_POST['symColor'];
}

if( isset($_POST['symSize']) ) {
	$symSize=$_POST['symSize'];
}

$outStyle=$_POST['outStyle'];
$outWidth=$_POST['outWidth'];
$outColor=$_POST['outColor'];
$objid=$_POST['objid'];

if($_POST['symType']=='point'){
	$sql = "INSERT INTO SYMBOL_SIMPLEMARKER (OBJECTID,STYLE,SYMBOLSIZE,COLOR,OUTLINE) VALUES ('$objid','$symStyle','$symSize','$symColor','1')";
}
else if($_POST['symType']=='polyline'){
	$sql = "INSERT INTO SYMBOL_SIMPLELINE (OBJECTID,STYLE,WIDTH,COLOR,OUTLINEID) VALUES ('$objid','$outStyle','$outWidth','$outColor','0')";
}
else if($_POST['symType']=='polygon'){
	$sql = "INSERT INTO SYMBOL_SIMPLEFILL (OBJECTID,STYLE,COLOR,OUTLINE) VALUES ('$objid','$symStyle','$symColor','1')";
}

$parsed_sql = oci_parse($db, $sql);
if(!$parsed_sql){
	echo "<h1>ERROR - Could not parse SQL statement. </h1>";
	exit;
}
//else {echo "Inserted into database"; echo $id;}

oci_execute($parsed_sql);

if($_POST['symType']!='polyline'){
	oci_free_statement($parsed_sql);
	$sql = "INSERT INTO SYMBOL_SIMPLELINE (OBJECTID,STYLE,WIDTH,COLOR,OUTLINEID) VALUES ('$objid','$outStyle','$outWidth','$outColor','1')";
	$parsed_sql = oci_parse($db, $sql);
	if(!$parsed_sql){
		//echo "<h1>ERROR - Could not parse SQL statement. </h1>";
		$msg = "Could not parse SQL statement.";
		kill($msg, $log=TRUE)
		//exit;
	}
	oci_execute($parsed_sql);
}

echo "Style saved to database";

//else echo 'Failed';

/*oci_fetch_all($parsed_sql, $results, 0 , -1, OCI_FETCHSTATEMENT_BY_ROW);

foreach($results as $entry) {
	foreach ($entry as $col_val) {
		echo $col_val;
		echo '*';
	}
}*/

?>