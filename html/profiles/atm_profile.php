<?php
$TABLE = "NISIS_ATM";
require "builder.php";
?>
<div>
	<div class="p-content marg">
        <div class="txtCenter">
            <div id="atmProfileDirtyForm" class="hidden">You have unsaved changes</div>
        </div>
        <div id="stats_and_buttons" class="il-block" >
            <div class="clear of-h">
                <div class="margLeft of-h">
                    <span data-bind="text:feat.attributes.ATM_ID"></span> - <span data-bind="text:feat.attributes.LG_NAME"></span>
                    <br />
                </div>            
            </div>
    		<div class="p-sec-header marg clear of-h">
                <span class=''
                    data-role="button"
                    data-title="Save Profile"
                    data-bind="click:saveChanges">
                    <img class="k-icon" src="images/menu/save_as.png" width="16" height="16" alt="Save ATM Profile">
                </span>
                <span class="facHistoryButton"
                    data-role="button"
                    data-title="View Facility History"
                    class="f-his pointer">
                    <img class="k-icon" src="images/icons/history.png" width="16" height="16" alt="View ATM History">
                </span>
            </div>
        </div>
        <?php
            imgDiv($_REQUEST['facId'], $TABLE, 1);
            imgDiv($_REQUEST['facId'], $TABLE, 2);
        ?>
		<div class='p-panels'>
		<?php echo '<ul id="profilePanelBar-' . $facType . $facId . '">' ?>
			<li><span>ATM Facility Snapshot</span>
			<div class="p-panel" title="ATM Facility Snapshot">
				<div class="p-sec">
					<div class="p-sec-header"> 
						<span class="p-sec-title left txt-u">ATM Facility Profile Snapshot</span>
					</div>
					<div class="p-sec-content">
						<div class="p-sec-row">
							<?php
								textDiv("Long Name", "LG_NAME");
								textDiv("Facility ID", "ATM_ID");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Associated Airport ID (FAA)", "ARP_ID");
								textDiv("Basic Type", "BASIC_TP");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Associated City", "ASSOC_CITY");
								textDiv("Associated State", "ASSOC_ST");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Detailed Type", "DTL_TYPE");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("En Route ATC", "ENRTE_ATC");
								textDiv("Responsible SU", "RESP_SU");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("ATM Ops Status", "OPS_STATUS", array("ATM", "253"), false, false, "statusExplanationChange", "OPS_STS_EX");
								statusExplanationSpan("ATM Operations Status Explanation", "OPS_STS_EX");
							?>
						</div>
						<hr />
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Ops Staffing Level", "OPS_SL", array("ATM", "264"), false, false, "statusExplanationChange", "OPS_SL_EX");
								statusExplanationSpan("Ops Staffing Level Explanation", "OPS_SL_EX");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Personnel Accounting Status", "ATO_PA_STS", array("ATM", "267"), false, false, "statusExplanationChange", "ATO_PAS_EX");
								statusExplanationSpan("ATO Personnel Account For Status Explanation", "ATO_PAS_EX");
							?>
						</div>
						<hr />
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Current Readiness Level", "CRT_RL", array("ATM", "276"), false, false, "statusExplanationChange", "CRT_RL_EX");
								statusExplanationSpan("Current Readiness level Explanation", "CRT_RL_EX", false, false, 250);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Current SECON Level", "CRT_SL", array("ATM", "280"),  false, false, "statusExplanationChange", "CRT_SL_EX");
								statusExplanationSpan("Current SECON level Explanation", "CRT_SL_EX", false, false, 250);
							?>
						</div>
						<hr />						
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATM Facility Other Notes", "ATM_NOTES", $FULLSEC);
                        	?>
                        </div>
					</div>
				</div>
			</div>
			</li>

            <li><span>ATM Facility Baseline Information</span>
            <div class="p-panel">
                <div class="p-sec clear">
                    <div class="p-sec-header">
                        <span class="p-sec-title left txt-u">Basic ID, Use &amp; Location</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Facility Long Name", "LG_NAME");
                            	textDiv("Facility ID", "ATM_ID");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                        	<?php
                        		textDiv("Associated City", "ASSOC_CITY");
                        		textDiv("Associated State", "ASSOC_ST");
                    		?>
                    	</div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Latitude", "LATITUDE");
                                textDiv("Longitude", "LONGITUDE");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                concatTextDiv("Location Coordinates", "LATITUDE", "LONGITUDE", " / ");
                            	textDiv("Airport Elevation", "ELEVATION");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Basic Type", "BASIC_TP");
                            	textDiv("Facility Street Address", "ADDRESS");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Detail Type", "DTL_TYPE");
                            	textDiv("Associated County", "ASSOC_CNTY");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Airport Tower Type", "TOWER_TYPE");
                            	textDiv("Time Zone", "TIME_ZONE");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Associated Airport ID (FAA)", "ARP_ID");
                            	textDiv("FAA Region", "FAA_REG");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Responsible Service Unit", "RESP_SU");
                            	textDiv("ATO Service Area", "ATO_SA");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Contract ATC Vendor", "CATC_VD");
                            	textDiv("ATO Terminal District", "ATO_TD");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Primary Phone Number", "PRIM_PH");
                            	textDiv("FEMA Region", "FEMA_REG");
                        	?>
                        </div>
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATC Level", "ATC_LEVEL");
                            	textDiv("ATM Facility ID - En Route ATC", "ENRTE_ATC");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATM Facility ID - Approach Control", "APPR_CTRL");
                            	textDiv("ATM Facility ID - Flight Services", "FLIGHT_SVC");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Supporting Tech Ops Facilities", "STOF_FAC");
                            	// textDiv("", "");
                        	?>
                        </div>
                    </div>
                </div>
                <div class="p-sec">
                    <div class="p-sec-header txt-u">
                        <span class="p-sec-title">Associated Airports</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("atm_arpts");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header txt-u">
                        <span class="p-sec-title">Contact Information</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Primary Phone", "PRIM_PH");
                            	textDiv("Facility Manager", "MANAGER");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("Ops Alternate Phone Number", "ALT_PH");
                            	textDiv("Satellite Phone Number", "SAT_PH");
                        	?>
                        </div>
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATO Terminal District", "ATO_TD");
                            	textDiv("Terminal District Manager", "TD_MGR");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATO Tech Ops District", "TO_DIST");
                            	textDiv("Tech Ops District Manager", "TOD_MGR");
                        	?>
                        </div>
                    </div>
                </div>
				<hr />
               	<div class="p-sec">
                    <div class="p-sec-header txt-u">
                        <span class="p-sec-title">Assigned Personnel</span>                       
                    </div>
					<div class="p-sec-row of-h clear">
                        <?php
                        	textDiv("Assigned Personnel Total", "ASS_P_TTL");
                        	textDiv("Assigned Controllers", "XXX");
                    	?>
                    </div>
               	</div>
            </div>
    		</li>
			<li><span>ATM Facility Status Information</span>
            <div class="p-panel">
                <div class="p-sec">
                    <div class="p-sec-header txt-u">
                        <span class="p-sec-title">ATM Facility Status</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("ATM Ops Status", "OPS_STATUS", array("ATM", "253"), false, false, "statusExplanationChange", "OPS_STS_EX");
                            	// Div("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("ATM Ops Status Explanation", "OPS_STS_EX");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <!-- <div class="p-sec-row of-h clear"> -->
                            <?php
                            	// textDiv("ATM Ops Status Additional Info Tags", "OPS_STS_AD");
                            	// textDiv("", "");
                        	?>
                        <!-- </div> -->
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("ATM Ops Status Supplemental Notes", "OPS_STS_SN");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("EG Support", "EG_SUP");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("Power Status", "PWR_STS", array("ATM", "259"), false, false, "statusExplanationChange", "PWR_STS_EX");
                            	// Div("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("Power Status Explanation ", "PWR_STS_EX");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <!-- <div class="p-sec-row of-h clear"> -->
                            <?php
                            	// textDiv("Power Status Additional Info Tags", "PWR_STS_AD_TAGS");
                            	// textDiv("", "");
                        	?>
                        <!-- </div> -->
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("Power Status Supplemental Notes", "PWR_STS_SN");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATM Facility Other Notes", "ATM_NOTES");                            	
                        	?>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="p-sec">
                    <div class="p-sec-header txt-u">
                        <span class="p-sec-title">ATO Personnel Accounting</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("Ops Staffing Level", "OPS_SL", array("", ""),  false, false, "statusExplanationChange", "OPS_STS_EX");
                            	// Div("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("Ops Status Explanation", "OPS_STS_EX");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("Ops Staffing Level Supplemental Notes", "OPS_STS_SN");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("Personnel Accounting Status", "ATO_PA_STS", array("ATM", "267"), false, false, "statusExplanationChange", "ATO_PAT");
                            	// Div("", "");
                        	?>
                        </div>                       
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("Personnel Accounting Explanation", "ATO_PAT");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("Personnel Accounting Supplemental Notes", "ATO_PAS_EX");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Assigned Personnel Total", "ASS_P_TTL");
                                // textDiv("", "");
                            ?>
                        <div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Personnel Accounted For Total", "ATO_PA_TTL");
                                // textDiv("", "");
                            ?>
                        </div>
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("Personnel Sig Impact", "ATO_PSI", array("", ""), false, false, "statusExplanationChange", "ATO_PSI_EX");
                            	// Div("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("Personnel Sig Impact Explanation", "ATO_PSI_EX");
                            	// textDiv("", "");
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("Personnel Sig Impact Supplemental Notes", "ATO_PSI_SN");
                            	// textDiv("", "");
                        	?>
                        </div>
                    </div>
                </div>
            </div>
            </li>

            <li><span>Incident Management Information</span>
            <div class="p-panel">
                <div class="p-sec">
                    <div class="p-sec-header">
                        <span class="p-sec-title txt-u">Emergency Ops Levels</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("Current Readiness Level", "CRT_RL", array("", ""), false, false, "statusExplanationChange", "CRT_RL_EX");                            	
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("Current Readiness Level Explanation", "CRT_RL_EX");                            	
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("Current Readiness Supplemental Notes", "CRT_RL_SN");                            	
                        	?>
                        </div>
<!-- SKIPPING FUTURE STATUS ALARM
                        <div class="p-sec-row of-h clear">
                            <div class="p-sub-sec">
                                <div class="f-label">Current Readiness Level Future Status Alarm 
                                    <span data-field="CRT_RL_AL" class="f-his pointer"><i class="fa fa-tag"></i></span> 
                                </div>
                                <div class="f-value">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="images/status/status_unknown.png" width="16" height="16" alt="Current Readiness Level"/>
                                        </span>
                                        <select class='f-status'
                                        	data-role='dropdownlist'
                                            data-bind="value:feat.attributes.CRT_RL_AL">
                                            <option data-icon='status_unknown' value=''>Update Status</option>
                                            <option data-icon='status_open' value='0'>RL-Alpha</option>
                                            <option data-icon='status_warnings' value='1'>RL-Bravo</option>
                                            <option data-icon='status_warnings' value='2'>RL-Charlie</option>
                                            <option data-icon='status_closed' value='3'>RL-Delta</option>
                                            <option data-icon='status_unknown' value='4'>Unkown</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    -->
                        <hr />
                        <div class="p-sec-row of-h clear">
                            <?php
                            	dropDownDiv("Current SECON Level", "CRT_SL", array("ATM", "280"), false, false, "statusExplanationChange", "CRT_SL_EX");                            	
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	statusExplanationSpan("Current SECON Level Explanation", "CRT_SL_EX");                            	
                        	?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textSupplementalNotesSpan("Current SECON Level Supplemental Notes", "CRT_SL_SN");                            	
                        	?>
                        </div>
<!-- SKIPPING FUTURE STATUS ALARM
                        <div class="p-sec-row of-h clear">
                            <div class="p-sub-sec">
                                <div class="f-label">SECON Level Future Status Alarm 
                                    <span data-field="CRT_SL_AL" class="f-his pointer"><i class="fa fa-tag"></i></span>
                                </div>
                                <div class="f-value">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="images/status/status_unknown.png" width="16" height="16" alt="Current SECON Level"/>
                                        </span>
                                        <select class='f-status'
                                            data-role='dropdownlist'
                                            data-bind='value:feat.attributes.CRT_SL_AL'>
                                            <option data-icon='status_unknown' value=''>Update Status</option>
                                            <option data-icon='status_open' value='0'>SECON-Green</option>
                                            <option data-icon='status_warnings' value='1'>SECON-Blue</option>
                                            <option data-icon='status_warnings' value='2'>SECON-Orange</option>
                                            <option data-icon='status_warnings' value='3'>SECON-Yellow</option>
                                            <option data-icon='status_closed' value='4'>SECON-Red</option>
                                            <option data-icon='status_unknown' value='5'>Unknown</option>
                                            <option data-icon='status_unknown' value='6'>N/A</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
--> 
                    </div>
                </div>
                <!-- ATO Incident Management -->
                <div class="p-sec">
                    <div class="p-sec-header txt-u">
                        <span class="p-sec-title">ATO Incident Management</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                            	iwlDiv("IWL Membership", "IWL_MBER");
                        	?>
                        </div>                       
                        <div class="p-sec-row of-h clear">
                            <?php
                            	textDiv("ATM Facility Other Notes", "ATM_NOTES");                            	
                        	?>
                        </div>
                     </div>             
                </div>
            </div>
            </li>
        </ul>
        </div>
    </div>
</div>