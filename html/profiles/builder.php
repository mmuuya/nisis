<?php

if(!isset($TABLE)) {
    kill("ERROR - Code which requires this file must set the variable $TABLE");
}

$debug = FALSE;

//Represents the permissions a user can have on a field. Hidden means
//the user has neither read nor write access. In order to hide a field,
//an administrator must ensure a group has neither read nor write permission.
$HIDDEN = 0;
$WRITABLE = "w";
$READABLE = "r";

//Some handy constants which clearly describe the arguments passed when looking at the profile php code.
$FULLSEC = TRUE;
$NOALERT = NULL;
$READONLY = TRUE;
$DROPDOWN = TRUE;

require "../lib/user_field_perms.php";
require "process_profile.php";

//-------- Functions

//Write out the html for a simple div with a value. If user can edit,
//a text input field will be included (unless $readonly is TRUE).
//
//The section will be a "full" section if $full is TRUE
function textDiv($label, $colName, $full=FALSE, $readonly=FALSE, $maxlength=0) {
    global $WRITABLE, $READABLE, $HIDDEN, $debug;

    if($label === "" || $colName === ""){
        error_log("ERROR label or column is missing textDiv() | $label $colName");
    }

    $perm = getPermission($colName);
    if($perm === $HIDDEN){
        hiddenDiv($label, $colName);
        return;
    } else if($perm === $WRITABLE && $readonly === TRUE){
        $perm = $READABLE;
    }

    if($full){
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

    echo '<div class="f-label">';
    echo $label;
    if($debug){ echo '[' . $colName . ']'; }
    echo '</div>';

    if($perm === $WRITABLE) {
        echo '<div class="f-value">';
        if ($maxlength===0) {
            echo     '<input type="text" class="k-textbox"';
        } else {
            echo '<input type="text" class="k-textbox" maxlength="'.$maxlength.'"';
        }
        echo         'data-bind="value:feat.attributes.'.$colName.'" />';
        echo '</div>';
    } else if($perm === $READABLE) {
        echo '<div class="f-value">';
        if ($colName === 'LAST_EDITED_DATE') {
            echo '<span data-bind="text:editDate"></span>';
        } else {
            echo '<span data-bind="text:feat.attributes.'.$colName.'"></span>';
        }
        echo '</div>';
    } else {
        error_log("ERROR - BUG in valDiv()");
    }

    echo '</div>';

    echo PHP_EOL;
}
//save as div but span with custom data bind and an a pencil icon
function textSupplementalNotesSpan($label, $colName, $full=FALSE, $readonly=FALSE) {
    global $WRITABLE, $READABLE, $HIDDEN, $debug, $TABLE, $facId, $facType;

    if($label === "" || $colName === ""){
        error_log("ERROR label or column is missing textDiv() | $label $colName");
    }

    $perm = getPermission($colName);
    if($perm === $HIDDEN){
        hiddenDiv($label, $colName);
        return;
    } else if($perm === $WRITABLE && $readonly === TRUE){
        $perm = $READABLE;
    }

    if($full){
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

    echo '<div class="f-label">';
    echo $label;
    if($debug){ echo '[' . $colName . ']'; }
    echo '</div>';


    $statusExplanation = getProfileField($colName);
    if($perm === $WRITABLE) {
        echo '<div class="f-value">';
        echo     '<span type="text" title="Optional data field to add information unrelated to an actual status change, e.g., updates on repair efforts" class="k-textbox supplemental-notes-'.$colName.'" >';
        echo     "$statusExplanation";
        echo '</span>';
        echo '</div>';
        echo '<ul class="ul_pencil_history" ><li><span
                    data-title="Enter Supplemental Notes for Status Change"
                    data-bind="click:openSupplementalNotesWindow" data-attr='.$colName.'>
                    <i class="fa fa-pencil" title="Enter or modify supplemental notes"></i>
                </span> </li>';
        echo '<li><span class="statusHistoryButton"
                data-title="View Status History"
                data-label="' . $label . '"
                data-field="' . $colName . '"
                data-status-explanation-type="supplemental">
              </span></li></ul>';

    } else if($perm === $READABLE) {
        echo '<div class="f-value">';
        if ($colName === 'LAST_EDITED_DATE') {
            echo '<span data-bind="text:editDate"></span>';
        } else {
            echo '<span type="text" title="Optional data field to add information unrelated to an actual status change, e.g., updates on repair efforts" class="k-textbox supplemental-notes-'.$colName.'" >';
            echo     "$statusExplanation";
            echo '</span>';
        }
        echo '</div>';
        echo '<span class="statusHistoryButton"
                data-title="View Status History"
                data-label="' . $label . '"
                data-field="' . $colName . '"
                data-status-explanation-type="supplemental">
              </span>';

    } else {
        error_log("ERROR - BUG in valDiv()");
    }

    echo '</div>';

    echo PHP_EOL;
}


function statusExplanationSpan($label, $colName, $full=FALSE, $readonly=FALSE) {
    global $WRITABLE, $READABLE, $HIDDEN, $debug, $TABLE, $facId, $facType;

    if($label === "" || $colName === ""){
        error_log("ERROR label or column is missing textDiv() | $label $colName");
    }

    $perm = getPermission($colName);
    if($perm === $HIDDEN){
        hiddenDiv($label, $colName);
        return;
    } else if($perm === $WRITABLE && $readonly === TRUE){
        $perm = $READABLE;
    }

    if($full){
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

    echo '<div class="f-label">';
    echo $label;
    if($debug){ echo '[' . $colName . ']'; }
    echo '</div>';

    $statusExplanation = getProfileField($colName);
    if($perm === $WRITABLE) {
        echo '<div class="f-value">';
        echo     '<span type="text" class="k-textbox status-explanation-'.$colName.'" >';
        echo     "$statusExplanation";
        echo '</span>';
        echo '</div>';
        echo '<ul class="ul_pencil_history" ><li><span
                    data-bind="click:statusExplanationChange" data-attr="'.$colName.'">
                    <i class="fa fa-pencil" title="Enter or modify status explanation"></i>
              </span></li><li></li></ul>';
    } else if($perm === $READABLE) {
        echo '<div class="f-value">';
        if ($colName === 'LAST_EDITED_DATE') {
            echo '<span data-bind="text:editDate"></span>';
        } else {
            echo '<span type="text" class="k-textbox status-explanation-'.$colName.'" >';
            echo     "$statusExplanation";
            echo '</span>';
        }
        echo '</div>';

    } else {
        error_log("ERROR - BUG in valDiv()");
    }

    echo '</div>';

    echo PHP_EOL;
}

//Concatenate two fields together, with specified $separator between the values
//
//This value can be hidden from the user (if at least one column has hidden permissions) but will never be written as a text input.
function concatTextDiv($label, $colNm1, $colNm2, $separator, $full=FALSE){
    global $WRITABLE, $READABLE, $HIDDEN, $debug;

    if(getPermission($colNm1) === $HIDDEN || getPermission($colNm2) === $HIDDEN){
        hiddenDiv($label, $colNm1 . "-" . $colNm2);
    }

    if($full){
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

    echo '<div class="f-label">';
    echo $label;
    if($debug){ echo '[' . $colNm1 . '-' . $colNm2. ']'; }
    echo '</div>';

    echo '<div class="f-value">';
    echo '<span data-bind="text:feat.attributes.' . $colNm1 . '"></span>';
    echo $separator;
    echo '<span data-bind="text:feat.attributes.' . $colNm2 . '"></span>';
    echo '</div>';

    echo '</div>';
    echo PHP_EOL;
}

/**
 * Write out the html for a dropdown select div.
 * If the user cannot edit, the dropdown is disabled. If $readonly is true, the dropdown is disabled.
 * Each dropdown also affects the icon beside it, using the changeProfileIcon //function found in mapview.util.js
 * If an alert button needs to be included, pass an array.
 * For example, passing ["APT", "1"] gives an alert button span with facType="APT" and statusType="1"
 * The section will be a "full" section if $full is TRUE
**/
function dropDownDiv($label, $colName, $alert=NULL, $full=FALSE, $readonly=FALSE, $customChangeFunction=NULL, $customChangeData=NULL) {
    global $db, $WRITABLE, $READABLE, $HIDDEN, $debug;
    global $facType, $facId; //set in get_profile.php
    require "../api/process_picklists.php";

    if($label === "" || $colName === ""){
        error_log("ERROR - label or column is missing [dropDownDiv] | $label $colName");
    }

    $perm = getPermission($colName);
    if($perm === $HIDDEN) {
        hiddenDiv($label, $colName);
        return;
    }

    //get the picklist for our options
    $fieldPicklist = $_SESSION['profile']['picklist'][$colName];
    if(!isset($fieldPicklist)){
        error_log("ERROR rendering the dropdown div. No picklist found in the SESSION - $colName | " . var_export($options, TRUE));
        error_log("------------------------");
        $fieldPicklist = array("" => "DB ERR no picklist");
    }

    //write out the html
    if($full){
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

        echo '<div class="f-label left">' . $label;
        if($alert !== NULL) {
            echo '<span class="alertsButton" ';
                echo 'facType="' . $alert[0] . '" ';
                echo 'statusType="' . $alert[1] . '" ';
                echo 'data-field="' . $colName . '">';
                echo '&nbsp;';
                echo '<img src="images/menu/alerts.png" width="16" height="16" ';
                    echo 'alt="Scheduled Alerts"/>';
            echo '</span>';
        }

        if($debug){ echo '[' . $colName . ']'; }
        echo '</div>';

        //The hashtag sign (#) is to use javascript code inside the template
        //"data.image" is defined in this template and added it to the datasource to be used in "selected value template"
        echo "<script type='text/x-kendo-template' id='" . $colName . "-default'>
                # var path;
                if (data.text === '' || data.value === '' ){
                    data.image = 'images/dropdowns/Unknown.png';
                }else{
                    path = 'images/dropdowns/' + data.text.replace(/\//g, '_') + '.png';
                    data.image = path;
                }
                //console.log('default: ', data);
                #";
        echo "<span><img src='#=data.image#' width='16' height='16'/> #=data.text#</span>";
        echo "</script>";

        //selected value template
        echo "<script type='text/x-kendo-template' id='" . $colName . "-value'>
            #if (data.text === '' || data.value === ''){
                data.image = 'images/dropdowns/Unknown.png';
            }
            //console.log('select: ', data);
            #";
            echo "<span><img src='#=data.image#' width='16' height='16'/> #=data.text#</span>";
        echo "</script>";

        //dropdownlist
        echo '<div class="f-value">';
            echo '<div class="input-group">';
                echo '<select ';
                if($readonly || $perm === $READABLE) {
                    echo 'disabled ';
                }
                    echo "class='f-status form-control' ";
                    echo "data-role='dropdownlist' ";

                    //echo "data-template='" . $colName . "'";

                    echo "data-value-template='" . $colName . "-value'";
                    echo "data-template='" . $colName . "-default' ";
                    if ($customChangeFunction!== NULL) {
                        echo " data-attr='$customChangeData' ";
                        echo 'data-bind="value:feat.attributes.' . $colName . ',  events:{select:'. $customChangeFunction . '}">';
                    }
                    else {
                        echo 'data-bind="value:feat.attributes.' . $colName . '">';
                    }


                $fieldPicklist = $_SESSION['profile']['picklist'][$colName];

                if(!isset($fieldPicklist)){
                    error_log("ERROR rendering the dropdown div. No picklist found in the SESSION - $colName ");
                    $fieldPicklist = array("" => "DB ERR no picklist");
                }

                foreach ($fieldPicklist as $numVal => $txtVal) {
                    echo "<option value='". $numVal . "'>";
                    if($debug) { echo $numVal . " - "; }
                    echo $txtVal;
                    echo "</option>";
                }
                echo '</select>';
                echo '</div>';
                echo '</div>';
                if ($colName!=="BPWR_USTS") {
                echo '<span class="statusHistoryButton"
                        data-title="View Status History"
                        data-label="' . $label . '"
                        data-field="' . $colName . '"
                        data-status-explanation-type="explanation">
                      </span>';
                }
    echo '</div>';
    echo PHP_EOL;
}

//Write out the html for a text field that mirrors a specific ATM status. The ATM displayed will depend on the type specified.
//
//NOTE - these kinds of divs cannot be hidden from the user!
function atmMirrorDiv($label, $type){
    global $db, $facId, $facType, $debug;

    $idKey = "ATM_ID";
    $shortIDkey = "shortId";
    $statusKey = "OPS_STATUS";
    $typeKey = "DTL_TYPE";

    $atmSeshVar = &$_SESSION['profile'][$facType . $facId . "-atms"];

    //Query for the related ATMs only as necessary
    if(!isset($atmSeshVar)){
        //Get all the ATMs related to this airport
        $query = "SELECT A.$idKey, A.$statusKey, A.$typeKey
                    FROM NISIS.NISIS_AIRPORT_ATM_VW A
                    JOIN NISIS_GEODATA.NISIS_AIRPORTS B ON B.FAA_ID=A.ARPT_ID
                    WHERE B.OBJECTID=:facid";
        $parsed = oci_parse($db, $query);
        oci_bind_by_name($parsed, ":facId", $facId);
        oci_execute($parsed);
        oci_fetch_all($parsed, $queryResults, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);

        //Process all the ATMs, categorizing them
        foreach ($queryResults as $curATM) {
            $atmParts = explode("-", $curATM[$idKey]);
            switch($atmParts[1]){
                case "ARTCC":
                case "CERAP":
                    $category = "enrt";
                break;
                case "MILTA":
                case "TRACON":
                    $category = "approach";
                break;
                default: //anything else must be a tower
                    $category = "tower";
            }
            if(isset($results[$category])){
                error_log("WARNING - atmMirrorDiv @ builder.php - multiple " . $category . "types found: ", var_export($queryResults, TRUE));
            }
            $results[$category][$idKey] = $curATM[$idKey];
            $results[$category][$shortIDkey] = $atmParts[0];
            $results[$category][$statusKey] = $curATM[$statusKey];
            $results[$category][$typeKey] = $curATM[$typeKey];
        }

        $atmSeshVar = $results;
        if ($debug){
            error_log("Processed results in mirrorDiv: " . var_export($atmSeshVar, TRUE));
        }
    }

    //Get the info for the ATM we're displaying, falling back to artcc if not set
    if(!isset($atmSeshVar[$type])){
        $type = "enrt";
        error_log("Error: atmSeshVar does not  exist" );
    }

    if(!isset($atmSeshVar[$type][$shortIDkey]) || !isset($atmSeshVar[$type][$typeKey]) ){
        error_log("Error: atmSeshVar shortIDkey or typekey not set" );
    }
    else {
    	$atmNameStr = $atmSeshVar[$type][$shortIDkey] . "-" . $atmSeshVar[$type][$typeKey];
    	$atmStatusValue = $atmSeshVar[$type][$statusKey];

	    //Get the string value and image for the ATM's status (array keys are super ridiculous - my b)
	    require "../api/process_picklists.php";
	    $atmStatusStr = $_SESSION['profile']['picklist'][$statusKey][$atmStatusValue];
	    $atmIcon = "images/status/" . $_SESSION['profile']['picklist']['image'][$atmStatusStr];
	}


    //Write out the HTML
    echo '<div class="p-sub-sec left">';

    echo '<div class="f-label">' . $label . '</div>';
    echo '<div class="f-value">';
    if(!isset($atmStatusStr)) {
    	echo 'none';
    }
    else {
	    echo '<span style="display:inline-block;min-width:100px">' . $atmNameStr . '</span>';
	    echo '&nbsp;<span>' . $atmStatusStr . '</span>';
	    picklistIcon($atmIcon);
	}
    echo '</div>';

    echo '</div>';
}

//Write out the html for a picklist icon, passing the image path
function picklistIcon($imgPath){
    $size = "17";
    echo '&nbsp;<img class="dropicon" src="' . $imgPath . '" width="' . $size . '" height="' . $size . '"/>';
}

//Write out the html for an IWL section. User will see a list of all
//IWLs currently assigned to that object and the user's groups.
//
//If editable, writes the html which allows the user to add the object
//to a group's IWL.
function iwlDiv($label, $colName, $full=FALSE){
    global $WRITABLE, $READABLE, $HIDDEN;

    if($label === "" || $colName === "") {
        error_log("ERROR label or column is missing in IwlDiv() | $label $colName");
    }

    $perm = getPermission($colName);
    //currently there's no need to hide the IWL membership
    // if($perm === $HIDDEN) {
    //     hiddenDiv($label, $colName);
    //     return;
    // }

    if($full){
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

        echo '<div class="f-label">';
        echo $label;
        if($perm === $WRITABLE) {
            echo '<a title="Add this facility to an IWL" class="k-button small">';
            echo '<span class="k-icon k-si-plus" data-target="iwl-profile-div" data-bind="click:addToIWL">';
            echo '</span>';
            echo '</a>';
        }
        echo '</div>';

        echo '<div class="f-value" data-bind="text:getIWLs">';
        echo '</div>';

    echo '</div>';
    echo PHP_EOL;
}

//Write out the HTML for a div placeholder for NOTAMs. The div will be bound using kendo
function notamsDiv(){
    global $WRITABLE, $READABLE, $HIDDEN;

    $notamCol = "CRT_NOTAMS";
    $perm = getPermission($notamCol);
    if($perm === $HIDDEN) {
        hiddenDiv("NOTAMS", $notamCol);
        return;
    }

    echo '<div class="p-full-sec left">';

        //we don't need the label, it ends up getting layed out kinda funny
        // echo '<div class="f-label">';
        // echo $label;
        // echo '</div>';

        echo '<div class="f-value" data-bind="html:getNtms">';
        echo '</div>';

    echo '</div>';
}

//Write out the HTML table placeholder for associated systems. The table will be kendo-ized when the profile is built.
function associationGrid($type) {
    global $facType, $facId, $debug; //set in get_profile.php
    $htm = '<table id="assoc-' . $type . '-' . $facType . $facId . '"><tbody></tbody></table>';
    echo $htm;

    // error_log("association grid: " . $htm);
}

//Write out the HTML for a url. Uses the 'urls' array from the SESSION (which was set up during login)
function url($label, $linkTxt, $type, $full=FALSE) {
    global $HIDDEN, $facId, $facType, $debug; //facType and facId set in get_profile.php

    //do not check permission, procedures doesn't have a column - all url's are readable!
    // if(getPermission($type) === $HIDDEN){
    //     hiddenDiv("URL", $type);
    //     return;
    // }

    //setup the url string
    switch($type) {
        case "procedures":
        case "DIAGRAM":
        case "AFD_INFO":
        case "SECT_CHART":
        case "NFDC_LINK":
            //other cases should fall through to this block
            $replacement = $_SESSION['profile']['history'][$facType.$facId][0]['OBJECTID'];
            $key = "[FAA_ID]";
        break;
        case "GGLE_MAP":
            $replacement = $_SESSION['profile']['history'][$facType.$facId][0]['LG_NAME'];
            $key = "[AIRPORT+FULL+NAME]";
        break;
        default:
            error_log("Unable to set up URL for type: " . $type);
            return;
    }
    $urlStr = str_replace($key, $replacement, $_SESSION['urls'][$type]);
    if(substr($urlStr, 0, 4) !== 'http') {
        //must start with 'http://' or 'https://'
        $urlStr = 'http://' . $urlStr;
    }

    //write out the html
    if($full) {
        echo '<div class="p-full-sec left">';
    } else {
        echo '<div class="p-sub-sec left">';
    }

    echo '<div class="f-label">';
    echo $label;
    echo '</div>';

    echo '<div class="f-value">';
    echo '<a href="' . $urlStr . '" target="_blank">' . $linkTxt . '</a>';
    echo '</div>';

    echo '</div>';
    echo PHP_EOL;
}

//Writes out an empty space. Generally the fields use the "left" class to align
//on the left side. This creates an empty area. Useful for pushing the next div
//to the right.
function emptyDiv() {
    echo '<div class="p-sub-sec left">';
        echo '<div class="f-label left">&nbsp;</div>';
    echo '</div>';
}

//Write out the HTML for a hidden field.
function hiddenDiv($label, $colName){
    global $debug;

    echo PHP_EOL . "<!-- $colName HIDDEN -->" . PHP_EOL;

    if ($debug) {
        echo '<div class="p-sub-sec">';
            echo '<div class="f-label">' . $label;
            echo '</div>';
            echo '<div class="f-value">';
            echo '~ DEBUG "' . $colName . '" hidden or missing ~';
            echo '</div>';
        echo '</div>';
    }
}

//Write out the HTML for a IMG block, and accept parameters for display.
//This is specifically designed to display images from the nisis.snapshot_uploads table
function imgDiv($ref_objid, $ref_table, $display_type){
    global $debug, $db;
$query = 'SELECT file_nm, file_contents, comments FROM NISIS.SNAPSHOT_UPLOADS WHERE
                REF_OBJID=:ref_objid
                AND REF_TABLE=:ref_table
                AND DISPLAY_TYPE=:display_type';
    $parsed = oci_parse($db, $query);
    oci_bind_by_name($parsed, ':ref_objid', $ref_objid);
    oci_bind_by_name($parsed, ':ref_table', $ref_table);
    oci_bind_by_name($parsed, ':display_type', $display_type);

    oci_execute($parsed);

    echo ' <div class="margLeft of-h il-block"
                id="snapshot_"' . $display_type . '
                data-bind="events:{mouseover:showImageEdits,mouseout:hideImageEdits}"
                style="vertical-align:top;">
                <div class="clear">  ';

    echo '<div class="il-block" style="min-width:70px">';
    $base64_img;
    if ($row = oci_fetch_array($parsed, OCI_ASSOC+OCI_RETURN_LOBS)) {
        $base64_img = "data:image/jpeg;base64," . base64_encode($row['FILE_CONTENTS']);
        echo '<img width="80" height="80" src="'.$base64_img. '" alt="'.$row['FILE_NM'] .'" />';
    } else {
        switch($display_type) {
            case 2:
                echo '<i class="fa fa-compass fa-4x"
                     data-bind="visible:defaultImage"></i>';
            break;
            default:
                echo '<i class="fa fa-plane fa-4x"
                    data-bind="visible:defaultImage"></i>';
        }
    }

    echo '<div class="clear of-h">
                <span class=""
                    id="'. $display_type . '_upload"
                    style="display:none;"
                    data-role="button"
                    data-title="Upload Image"
                    data-bind="click:uploadImage,visible:isEditable">
                    <i class="fa fa-upload"></i>
                </span>
           </div>';
    echo '</div>';
    echo '</div>
    </div>';

oci_free_statement($parsed);
}

function rebuildImgDiv($ref_objid, $ref_table, $display_type){
   $query = 'SELECT file_nm, file_contents, comments FROM NISIS.SNAPSHOT_UPLOADS WHERE
                REF_OBJID=:ref_objid
                AND REF_TABLE=:ref_table
                AND DISPLAY_TYPE=:display_type';
    $parsed = oci_parse($db, $query);
    oci_bind_by_name($parsed, ':ref_objid', $ref_objid);
    oci_bind_by_name($parsed, ':ref_table', $ref_table);
    oci_bind_by_name($parsed, ':display_type', $display_type);

    oci_execute($parsed);

    $base64_img;
    if ($row = oci_fetch_array($parsed, OCI_ASSOC+OCI_RETURN_LOBS)) {
        $base64_img = "data:image/jpeg;base64," . base64_encode($row['FILE_CONTENTS']);
        echo '
        <script>
            $("snapshot_" + '. $display_type . ' ).setAttributes("src", "'.$base64_img.'");
        </script>';
    }

    echo ("successfully called builder.php");
}

?>