<?php
$TABLE = "NISIS_AIRPORTS";
require "builder.php";
?>
<div>
    <div class="p-content marg">
        <div class="txtCenter">
            <div id="airportsProfileDirtyForm" class="hidden">You have unsaved changes</div>
        </div>        
        <div id="stats_and_buttons" class="il-block" >
            <div class="clear of-h">
                <div class="margLeft of-h">
                    <!-- <span>FAA ID: <span data-bind="text:feat.attributes.FAA_ID"></span></span>
                    <br>
                    <span>Name: <span data-bind="text:feat.attributes.LG_NAME"></span></span>
                    <br> -->
                    <span data-bind="text:feat.attributes.FAA_ID"></span> - <span data-bind="text:feat.attributes.LG_NAME"></span>
                    <br />
                </div>            
            </div>
            <div class="p-sec-header marg clear of-h">
                <span class=''
                    data-role="button"
                    data-title="Save Profile"
                    data-bind="click:saveChanges">
                    <img class="k-icon" src="images/menu/save_as.png" width="16" height="16" alt="Save Airport Profile">
                </span>
                <span class="facHistoryButton"
                    data-role="button"
                    data-title="View Airport History">
                    <img class="k-icon" src="images/icons/history.png" width="16" height="16" alt="View Airport History">
                </span>
            </div>
        </div> <!-- end of stats_and_buttons -->
        <?php
            imgDiv($_REQUEST['facId'], $TABLE, 1);
            imgDiv($_REQUEST['facId'], $TABLE, 2);
        ?>        
        <!-- processing info -->
        <div id='' class='p-panels'>
        <?php echo '<ul id="profilePanelBar-' . $facType . $facId . '">' ?>
            <!-- Profile Snapshot Info -->
            <li><span>Airport Snapshot</span>
            <div class="p-panel" title="Airport Snapshot">
                <!-- Snapshot -->
                <div class="p-sec">
                    <div class="p-sec-content clear ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <!--
                            <div class="p-sub-sec left">
                                <div class="f-label left">Airport Long Name</div>
                                <div class="f-value left" data-bind="text:feat.attributes.LG_NAME"> </div>
                            </div>
                            <div class="p-sub-sec right">
                                <div class="f-label left">Airport ID (FAA)</div>
                                <div class="f-value left" data-bind="text:feat.attributes.FAA_ID"> </div>
                            </div>
                            -->
                            <?php
                                textDiv("Airport Long Name", "LG_NAME");
                                textDiv("Airport ID (FAA)", "FAA_ID");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php 
                                textDiv("Airport Classification", "ARP_CLASS");
                                textDiv("Airport ID (ICAO)", "ICAO_ID");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport Tower Type", "TOWER_TP");
                                textDiv("Airport ID (IATA)", "IATA_ID");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Associated City", "ASSOC_CITY");
                                textDiv("Associated State", "ASSOC_ST");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">                            
                            <?php
                                dropDownDiv("Airport Overall Status", "OVR_STATUS", array("APT", "77"), false, false, "statusExplanationChange", "OVR_STS_EX");
                            ?>
                        </div>

                        <div class="p-sec-row of-h clear">
                            <?php                               
                                 statusExplanationSpan("Airport Overall Status Explanation", "OVR_STS_EX");                                                             
                            ?>                          
                        </div>                        
                        <div class="p-sec-row of-h clear">
                            <?php                                
                                textSupplementalNotesSpan("Airport Overall Status Supplemental Notes", "OVR_STS_SN");                                                              
                            ?>
                        </div>
                        <span id="supp_notes_window"></span>
                        <hr>
                        <div class="p-sec-row of-h clear">
                            <?php
                                atmMirrorDiv("Airport Tower - ATM Operations Status", "tower");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                atmMirrorDiv("Approach Control - ATM Ops Status", "approach");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                atmMirrorDiv("En Route ATC - ATM Operations Status", "enrt");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("Airport Owner/Operator Operations Status", "OOO_STATUS", array("APT", "91"), false, false, "statusExplanationChange", "OOO_STS_EX");
                                statusExplanationSpan("Airport Owner/Operator Operations Status Explanation", "OOO_STS_EX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("Current Readiness Level", "CRT_RL", array("APT", "427"), false, false, "statusExplanationChange", "CRT_RL_EX");
                                statusExplanationSpan("Current Readiness level Explanation", "CRT_RL_EX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("Airport Overall ANS Systems Status", "ANS_STATUS", array("APT", "107"), false, false, "statusExplanationChange", "ANS_STS_EX");
                                statusExplanationSpan("Airport Overall ANS Systems Status Explanation", "ANS_STS_EX");
                            ?>
                        </div>
                        <hr>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Core 30 Status", "C30_STATUS");
                                textDiv("OEP 35 Status", "OEP35_STS");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("OPSNET 45 Status", "OPS45_STS");
                            ?>
                        </div>                       
                    </div>
                </div>
            </div>
            </li>

            <li><span>Basic ID, Use &amp; Location</span>
            <div class="p-panel" title="Basic ID, Use &amp; Location">
                <div class="p-sec">
                    <div class="p-sec-header marg clear of-h">    
                        <span class="p-sec-title left txt-u">Basic ID, Use, &amp; Location</span>                    
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport Long Name", "LG_NAME");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Associated City", "ASSOC_CITY");
                                textDiv("Associated State", "ASSOC_ST");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport ID (FAA)", "FAA_ID");
                                textDiv("Latitude", "LATITUDE");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                concatTextDiv("Airport Reference Coordinates", "LATITUDE", "LONGITUDE", " / ");
                                textDiv("Longitude", "LONGITUDE");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport ID (ICAO)", "ICAO_ID");
                                textDiv("Airport Elevation", "ELEVATION");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport ID (IATA)", "IATA_ID");
                                textDiv("Airport Street Address", "ADDRESS");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Basic Use", "BASIC_USE");
                                textDiv("Associated County", "COUNTY");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Attendance", "ATTENDANCE");
                                textDiv("Time Zone", "TIME_ZONE");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport Classification", "ARP_CLASS");
                                textDiv("FAA Region", "FAA_REG");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Hub Type", "HUB_TYPE");
                                textDiv("ATO Service Area", "ATO_SA");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Cargo Service", "CARGO_SVC");
                                textDiv("ATO Terminal District", "ATO_TD");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Reliever Status", "REL_STATUS");
                                textDiv("FEMA Region", "FEMA_REG");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("International Service", "INT_SVC");
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Interagency Operations</span>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Responsible TSA FSD", "RSP_TSAFSD");
                                textDiv("Responsible CBP POC", "RSP_CBPPOC");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("CDC Quarantine Lead Officer", "CDC_Q_LEAD");
                                textDiv("CDC Q-Station Present", "CDC_Q_ST");
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">ATM Facilities</span>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport Tower Present", "TOWER");
                                textDiv("Airport Tower Type", "TOWER_TP");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("ATM Facility ID - Airport Tower", "XXX");
                                textDiv("ATM Facility ID - Approach Control", "XXX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("ATM Facility ID - En Route ATC", "XXX");
                                textDiv("ATM Facility ID - Flight Services", "XXX");
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Normal Operations Performance</span>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Baseline AAR (VMC)", "BL_AAR");
                                textDiv("Baseline AAR (Low VMC)", "XXX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Baseline AAR (IMC)", "XXX");
                                textDiv("Baseline AAR (Low IMC)", "XXX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Average Daily Operations", "AVG_OPS_N");
                                textDiv("Average Daily Operations (Breakdown)", "XXX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Core 30 Status", "C30_STATUS");
                                textDiv("OEP 35 Status", "OEP35_STS");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("OPSNET 45 Status", "OPS45_STS");
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Airport Operations</span>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Airport Owner", "OWNER");
                                textDiv("Airport Manager", "MANAGER");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Fuel Types", "FT_AVAIL");
                                textDiv("ARFF Services", "ARFF_SVC");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("FBO", "FBO");
                            ?>
                        </div>
                        <hr>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Operator Emergency Contact Info", "OP_EG_INF");
                                textDiv("Operations Center Contact Informaton", "OP_CTR_INF");
                            ?>
                        </div>
                        <hr>
                        <div class="p-sec-row of-h clear">
                            <?php
                                url("Airport Diagram", "External Link: AeroNav", "DIAGRAM");
                                url("Map using Google Map", "External Link: Google", "GGLE_MAP");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                url("Overlying Sectional", "External Link: NFDC", "SECT_CHART");
                                url("Link to NFDC", "External Link: NFDC", "NFDC_LINK");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                url("A/FD Information", "External Link: AeroNav", "AFD_INFO");
                                url("Instrument Procedures", "External Link: AirNav", "procedures");
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </li>
            <li><span>Associated ANS Systems</span>
            <div class="p-panel">
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Associated ANS Systems</span>
                    </div>
                    <div class="p-sec-content clear ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("ans");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </li>
            <li><span>Associated ATM Facilities</span>
            <div class="p-panel">
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Associated ATM Facilities</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("atm");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </li>
            <li><span>Airport Operations</span>
            <div class="p-panel">
                <!-- Ground Based Precision Approaches -->
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Ground Based Precision Approaches</span>
                    </div>
                    <!-- Common Sytems -->
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("gbpa-com");
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- Details Sytems -->
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("gbpa");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- -->
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Arrival Rates</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("arr_rates");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Engineered Rates</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <?php
                                    associationGrid("eng_rates");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            </li>
            <li><span>Airport Overall Status</span>
            <div class="p-panel" title="Airport Overall Status">
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Airport Overall Status</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                               dropDownDiv("Airport Overall Status", "OVR_STATUS", array("APT", "77"), $FULLSEC, false, "statusExplanationChange", "OVR_STS_EX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                               //textDiv("Airport Overall Status Explanation", "OVR_STS_EX", $FULLSEC);                               
                               statusExplanationSpan("Airport Overall Status Explanation", "OVR_STS_EX", $FULLSEC); 
                            ?>                            
                        </div>
                        <!-- <div class="p-sec-row of-h clear"> -->
                            <?php
                                //textDiv("Airport Overall Status Additional Info Tags", "OVR_STS_AD", $FULLSEC);
                            ?>
                        <!-- </div> -->
                        <!-- <div class="p-sec-row of-h clear"> -->
                            <?php
                                // textDiv("Airport Overall Status Additional Info Tags", "OVR_STS_SN", $FULLSEC);
                            ?>
                        <!-- </div> -->
                        <!-- <div class="p-sec-row of-h clear"> -->
                            <?php
                                // textDiv("TFM Comments", "TFM_CMTS", $FULLSEC);
                            ?>
                        <!-- </div> -->
                        <!-- <div class="p-sec-row of-h clear"> -->
                            <?php
                                //textDiv("Airport Other Notes", "XXX", $FULLSEC);
                            ?>
                        <!-- </div> -->
                    </div>
                </div>
                <hr>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Aviation Operator Ops Status</span>
                    </div>
                    <div class="p-sec-content clear of-h">
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("Operator Ops Status", "AOO_STATUS", array("APT", "82"), $FULLSEC, false, "statusExplanationChange", "AOO_STS_EX");                                
                            ?>
                            <?php
                                //textDiv("Operator Ops Status Explanation", "AOO_STS_EX", $FULLSEC);
                                statusExplanationSpan("Operator Ops Status Explanation", "AOO_STS_EX", $FULLSEC); 
                            ?>
                            <?php
                                // textDiv("Operator Ops Status Additional Info Tags", "AOO_STS_AD", $FULLSEC);
                            ?>
                            <?php
                                textSupplementalNotesSpan("Operator Ops Status Supplemental Notes", "AOO_STS_SN", $FULLSEC);
                            ?>
                            <?php
                                //dropDownDiv("Operator Ops Future Status Alarm", "AOO_STS_AL", TRUE, NULL, TRUE);
                            ?>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Airport Owner/Operator Operations Status</span>
                    </div>
                    <div class="p-sec-content clear of-h">
                        <div class="p-sec-row of-h clear">
                            <?php                                
                                dropDownDiv("Airport Owner/Operator Operations Status", "OOO_STATUS", array("APT", "91"), $FULLSEC, false, "statusExplanationChange", "OOO_STS_EX");  
                            ?>
                            <?php
                                statusExplanationSpan("Airport Owner/Operator Operations Status Explanation", "OOO_STS_EX", $FULLSEC);
                            ?>
                            <?php
                                textSupplementalNotesSpan("Airport Owner/Operator Operations Supplemental Notes", "OOO_STS_SN", $FULLSEC);
                            ?>                          
                        </div>
                    </div>
                </div>
                <hr>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Airport Interagency Operations Status</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Responsible TSA FSD", "RSP_TSAFSD", $FULLSEC);
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("TSA Screening OPS Status", "TSA_STATUS", array("APT", "96"), $FULLSEC, false, "statusExplanationChange", "TSA_STS_EX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                statusExplanationSpan("Status Explanation", "TSA_STS_EX", $FULLSEC);
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textSupplementalNotesSpan("Supplemental Notes", "TSA_STS_SN", $FULLSEC);
                            ?>
                        </div>
                        <!-- SKIP THIS - future alarm status -->
                        <!--<div class="p-sec-row of-h clear">
                            <div class="p-full-sec left">
                                <div class="f-label left">TSA Screening OPS Status Future Status Alarm <span data-field="TSA_STS_AL" class="f-his pointer"><i class="fa fa-history"></i></span></div> -->
                                <!-- <div class="f-value select">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="images/status/status_unknown.png" width="16" height="16" alt="Airport Overall Status"/>
                                        </span>
                                        <select name='TSA_STS_AL' class='f-status' data-bind="value:feat.attributes.TSA_STS_AL" data-role="dropdownlist">
                                            <option data-icon='status_unknown' value=''>Update Status</option>
                                            <option data-icon='status_open' value='0'>Normal Throughput</option>
                                            <option data-icon='status_warnings' value='1'>Constrained Capacity</option>
                                            <option data-icon='status_closed' value='2'>Screening Suspended</option>
                                            <option data-icon='status_unknown' value='3'>Unknown</option>
                                            <option data-icon='status_unknown' value='4'>Other</option>
                                        </select>
                                    </div>
                                </div> -->
                                <?php //dropDownDiv("LABEL", "TSA_STS_AL"); ?>
                            <!-- </div>
                        </div> -->
                        <hr>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Responsible CBP POC", "RSP_CBPPOC", $FULLSEC);
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Fuel Supply Status</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Fuel Types", "FT_AVAIL", $FULLSEC);
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("Availability", "AFA_STATUS", $NOALERT, $FULLSEC, false, "statusExplanationChange", "AFA_STS_EX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                statusExplanationSpan("Explanation", "AFA_STS_EX", $FULLSEC);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </li>
            <li><span>Airport Operations - Supplemental Info</span>
            <div class="p-panel">
                <div class="p-sec clear">
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-header marg clear of-h">
                            <span class="p-sec-title left txt-u">Runways</span>
                        </div>
                        <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                            <div class="p-sec-row of-h clear">
                                <?php
                                    associationGrid("runways");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-header marg clear of-h">
                            <span class="p-sec-title left txt-u">TFM Information</span>
                            <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                                <span class='ui-icon ui-icon-pencil'> </span>
                            </a>
                            <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                                <span class='ui-icon ui-icon-carat-1-e'> </span>
                            </a>
                            <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                                <span class='ui-icon ui-icon-home'> </span>
                            </a>
                            <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                                <span class='ui-icon ui-icon-carat-1-w'> </span>
                            </a>
                        </div>
                        <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                            <div class="p-sec-row of-h clear">
                                <?php
                                    textDiv("TFM Comments", "TFM_CMTS", $FULLSEC);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Current NOTAMs</span>
                        <!-- don't think we need these
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                        -->
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                //textDiv("NOTAMs", "CRT_NOTAMS", $FULLSEC);
                                notamsDiv();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Aerodrome Weather</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("METAR", "CRT_M_TAFS", $FULLSEC);
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("TAF", "XXX", $FULLSEC);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </li>
            <li><span>ANS Status Information</span>
            <div class="p-panel" title="ANS Status Information">
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Associated ANS Systems - Status</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                dropDownDiv("Overall ANS Systems Status", "ANS_STATUS", array("APT", "107"), $FULLSEC, false, "statusExplanationChange", "ANS_STS_EX");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                statusExplanationSpan("Status Explanation", "ANS_STS_EX", $FULLSEC);
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textSupplementalNotesSpan("Status Supplemental Notes", "ANS_STS_SN", $FULLSEC);
                            ?>
                        </div>
                        <!-- <div class="p-sec-row of-h clear">
                            <div class="p-full-sec">
                            <!-- TODO not ready for textDiv or dropDownDiv -->
                                <!-- <table>
                                    <thead>
                                        <tr><th>Category</th><th>ID</th><th>Name</th><th>Type</th><th>RWY End</th></tr>    
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><div class="f-value left" data-bind="text:feat.attributes.XXX"></div></td><td class="f-value SUP_AG_COM"><div class="f-value left" data-bind="text:feat.attributes.XXX"></div></td><td><div class="f-value left" data-bind="text:feat.attributes.XXX"></div></td><td><div class="f-value left" data-bind="text:feat.attributes.XXX"></div></td><td><div class="f-value left" data-bind="text:feat.attributes.XXX"></div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                    </div>
                </div>
                <hr />
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Open RMLS Tickets</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                                associationGrid("rmls_open");
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Other RMLS Tickets</span>
                    </div>
                    <div class="p-sec-content">
                        <div class="p-sec-row of-h clear">
                            <?php
                                associationGrid("rmls_void");
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </li>
            <li><span>Incident Management Information</span>
            <div class="p-panel" title="Incident Management Information">
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Incident Management Information</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                iwlDiv("IWL Membership", "IWL_MBER");
                            ?>
                        </div>
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Last Update", "LAST_EDITED_DATE", false, true);
                                textDiv("Last Update By", "LAST_EDITED_USER", false, true);
                            ?>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="p-sec clear">
                    <div class="p-sec-header marg clear of-h">
                        <span class="p-sec-title left txt-u">Interagency Incident Management</span>
                        <a href='#' class='p-sec-edit pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Enable Editing'>
                            <span class='ui-icon ui-icon-pencil'> </span>
                        </a>
                        <a href='#' class='p-sec-previous pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Next Version'>
                            <span class='ui-icon ui-icon-carat-1-e'> </span>
                        </a>
                        <a href='#' class='p-sec-current pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Current Version'>
                            <span class='ui-icon ui-icon-home'> </span>
                        </a>
                        <a href='#' class='p-sec-next pui-panel-titlebar-icon ui-corner-all ui-state-default' role='button' title='Previous Version'>
                            <span class='ui-icon ui-icon-carat-1-w'> </span>
                        </a>
                    </div>
                    <div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
                        <div class="p-sec-row of-h clear">
                            <?php
                                textDiv("Response Airport Designation", "RES_DSG");
                                textDiv("Response Airport Designation Explanation", "RES_DSG_EX");
                            ?>
                        </div>
                    </div>
                </div>
            </li>
            </div>
        </div>
    </ul>
    </div>
</div>