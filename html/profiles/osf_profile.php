<?php
$TABLE = "NISIS_OSF";
require "builder.php";
?>
<div>    
	<div class="p-content marg">
		<div class="txtCenter">
	    	<div id="osfProfileDirtyForm" class="hidden">You have unsaved changes</div>
	    </div>
        <div id="stats_and_buttons" class="il-block">
        	<div class="clear of-h">
                <div class="margLeft of-h">
                    <span data-bind="text:feat.attributes.OSF_ID"></span> <span data-bind="text:feat.attributes.LG_NAME"></span>
                    <br />
                </div>            
            </div>
			<div class="p-sec-header marg clear of-h">
	            <span class=''
	                data-role="button"
	                data-title="Save Profile"
	                data-bind="click:saveChanges">
	                <img class="k-icon" src="images/menu/save_as.png" width="16" height="16" alt="Save OSF Profile">
	            </span>
	            <span class="facHistoryButton"
                    data-role="button"
                    data-title="View Facility History"
                    class="f-his pointer">
                    <img class="k-icon" src="images/icons/history.png" width="16" height="16" alt="View OSF History">
                </span>
	        </div>
	    </div>
        <?php
            imgDiv($_REQUEST['facId'], $TABLE, 1);
            imgDiv($_REQUEST['facId'], $TABLE, 2);
        ?>
		<div id='' class='p-panels'>
		<?php echo '<ul id="profilePanelBar-' . $facType . $facId . '">' ?>
			<li><span>OSF Snapshot</span>
			<div class="p-panel">
				<div class="p-sec">	
					<div class="p-sec-content clear ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("OSF ID", "OSF_ID");
								textDiv("OSF Type", "BASIC_TP");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("OSF Long Name", "LG_NAME");
								textDiv("Principal Using Org", "PPL_UORG");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Associated City", "ASSOC_CITY");
								textDiv("Associated State", "ASSOC_ST");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Facility Manager", "FAC_MGR");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Main Tel Numbers", "MAIN_TEL");
								textDiv("Hours of Operation", "HRS_OPS");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Personnel Assigned (All)", "PA_ALL");
								textDiv("Personnel Assigned (ATO)", "PA_ATO");
							?>
						</div>
						<hr>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Basic Operating Status", "BOPS_STS", array("OSF", "346"), false, false, "statusExplanationChange", "BOPS_EX");
								statusExplanationSpan("Operations Status Explanation", "BOPS_EX");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Staffing Level", "AOS_LVL", array("OSF", "360"), false, false, "statusExplanationChange", "AOS_L_EX");
								statusExplanationSpan("ATO Staffing Level Explanation", "AOS_L_EX");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Personnel Accounting Status", "AOPA_4S", array("OSF", "364"), false, false, "statusExplanationChange", "AOPA_4S_EX");
								statusExplanationSpan("ATO Personnel Accounting Status Explanation", "AOPA_4S_EX");
							?>
						</div>
						<hr/>
						<!-- Readiness Level and SECON Section -->
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Current Readiness Level", "CRT_RL", array("OSF", "370"), false, false, "statusExplanationChange", "CRT_RL_EX");
								statusExplanationSpan("Current Readiness level Explanation", "CRT_RL_EX");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Current SECON Level", "CRT_S_LVL", array("OSF", "374"), false, false, "statusExplanationChange", "CRT_SL_EX");
								statusExplanationSpan("Current SECON level Explanation", "CRT_SL_EX");
							?>
						</div>
						<hr>						
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("OSF Other Notes", "OSF_NOTES");
								// textDiv("", "");
							?>
						</div>
					</div>
				</div>
			</div>
			</li>

			<li><span>OSF Baseline Information</span>
			<div class="p-panel" title="OSF Baseline Information">				
				<div class="p-sec clear">
					<div class="p-sec-header marg clear of-h">
						<span class="p-sec-title left txt-u">Basic ID, Use &amp; Location</span>
					</div>
					<div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Facility Long Name", "LG_NAME");
								textDiv("Associated City and State", "ASSOC_CITY");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Facility ID", "OSF_ID");
								textDiv("Latitude", "LATITUDE");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								concatTextDiv("Location Coordinates", "LATITUDE", "LONGITUDE", " / ");
								textDiv("Longitude", "LONGITUDE");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("OSF Type", "BASIC_TP");
								textDiv("OSF Elevation", "ELEVATION");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Principal Using Org", "PPL_UORG");
								textDiv("Facility Street Address", "ADDRESS");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Hours of Operation", "HRS_OPS");
								textDiv("Associated County", "ASSOC_CNTY");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Main Tel Numbers", "MAIN_TEL");
								textDiv("Time Zone", "TIME_ZONE");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Facility Manager", "FAC_MGR");
								textDiv("FAA Region", "FAA_REG");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("ATO Service Area", "ATO_REG");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("ATO Terminal District", "TERM_DIST");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("ATO Technical Ops District", "TOPS_DIST");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("FEMA Region", "FEMA_REG");
								// textDiv("", "");
							?>
						</div>
					</div>
				</div>
				<hr />
				<div class="p-sec clear">
					<div class="p-sec-header marg clear of-h">
						<span class="p-sec-title left txt-u">Assigned Personnel</span>
					</div>
					<div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Personnel Assigned (All)", "PA_ALL");
								textDiv("Personnel Assigned (ATO)", "PA_ATO");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Personnel Assigned Notes", "PA_NOTES");
								// textDiv("", "");
							?>
						</div>
					</div>
				</div>
			</div>
			</li>

			<li><span>OSF Basic Operating Status</span>
			<div class="p-panel" title="OSF Basic Operating Status">
				<div class="p-sec clear">
					<!-- <div class="p-sec-header marg clear of-h">
						<span class="p-sec-title left txt-u">OSF Basic Operating Status</span>
					</div> -->
					<div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Basic Operating Status", "BOPS_STS", array("OSF", "346"), false, false, "statusExplanationChange", "BOPS_EX");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("Operations Status Explanation", "BOPS_EX");
								// textDiv("", "");
							?>
						</div>
						<!-- <div class="p-sec-row of-h clear"> -->
							<?php
								// textDiv("Basic Operating Status Additional Info Tags", "BOPS_AD");
								// textDiv("", "");
							?>
						<!-- </div>	 -->
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("Basic Operating Status Supplemental Notes", "BOPS_SN");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								// dropDownDiv("Basic Operating Future Status Alarm", "SOPS_AL");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Basic Operating Future Status Alarm Time", "SOPS_AL_TM");
								// textDiv("", "");
							?>
						</div>
						<!-- <hr>
						<div class="p-sec-row of-h clear">
							<?php
								//textDiv("ANS System Other Notes", "ANS_OTHER_NOTE");
								// textDiv("", "");
							?>
						</div> -->
					</div>
				</div>
			</div>
			</li>

			<li><span>OSF Status Information</span>
			<div class="p-panel" title="OSF Status Information">
				<div class="p-sec clear">
					<div class="p-sec-header marg clear of-h">
						<span class="p-sec-title left txt-u">ATO Personnel Accounting</span>
					</div>
					<div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Staffing Level", "AOS_LVL", array("OSF", "360"), false, false, "statusExplanationChange", "AOS_L_EX");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("ATO Staffing Level Explanation", "AOS_L_EX");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("ATO Staffing Level Supplemental Notes", "AOS_L_SN");
								// textDiv("", "");
							?>
						</div>
<!-- SKIP FUTURE STATUS ALARM -->
						<!-- <div class="p-sec-row of-h clear"> -->
							<!-- <div class="p-sub-sec left">
								<div class="f-label left">Staffing Level Future Status Alarm * <span data-field="OSF_STAFF_STAT_ALM" class="f-his pointer"><i class="fa fa-tag"></i></span></div> -->
								<!-- <div class="f-value">
									<div class="input-group">
										<span class="input-group-addon">
											<img src="images/status/status_unknown.png" width="16" height="16" alt="ATM Ops Staffing Level"/>
										</span>
										<select class='f-status form-control'
											data-role='dropdownlist'
											data-bind='value:feat.attributes.OSF_STAFF_STAT_ALM'>
											<option data-icon='status_unknown' value=''>Update Status</option>
											<option data-icon='status_open' value='0'>Fully Staffed</option>
											<option data-icon='status_warnings' value='1'>Limited Staffing</option>
											<option data-icon='status_warnings' value='2'>Critically Low Staffing</option>
											<option data-icon='status_closed' value='3'>No Staffing Available</option>
											<option data-icon='status_unknown' value='4'>Unkown</option>
										</select>
									</div>
								</div> -->
							<!-- </div> -->
						<!-- </div> -->
						<!-- <div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Staffing Level Future Status Alarm Time * <span data-field="ATM_STAFF_LVL_AL_TM" class="f-his pointer"><i class="fa fa-tag"></i></span></div>
								<div class="f-value" data-bind="text:feat.attributes.ATM_STAFF_LVL_AL_TM"> </div>
							</div>
						</div> -->
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("OSF Personnel Assigned Notes", "PA_NOTES");
								// textDiv("", "");
							?>
						</div>
						<hr>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Personnel Accounting Status", "AOPA_4S", array("OSF", "364"), false, false, "statusExplanationChange", "AOPA_4S_EX");
								// dropDownDiv("", "", $);
							?>
						</div>						
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("Personnel Accounting Explanation", "AOPA_4S_EX");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("Personnel Accounting Supplemental Notes", "AOPA_4S_SN");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Personnel Assigned (ATO)", "PA_ATO");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Personnel Assigned (All)", "PA_ALL");
								// textDiv("", "");
							?>
						</div>
						<!-- SKIPPING FUTURE ALARM STATUS -->
						<!-- <div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Personnel Accounting Status Future Status Alarm * <span data-field="AOPA_4S_AL" class="f-his pointer"><i class="fa fa-tag"></i></span></div>
								<div class="f-value">
									<div class="input-group">
										<span class="input-group-addon">
											<img src="images/status/status_unknown.png" width="16" height="16" alt="Ops Staffing Level"/>
										</span>
										<select class='f-status form-control'
											data-role='dropdownlist'
											data-bind='value:feat.attributes.AOPA_4S_AL'>
											<option data-icon='status_unknown' value=''>Update Status</option>
											<option data-icon='status_open' value='0'>All Accounted For</option>
											<option data-icon='status_warnings' value='1'>Some Accounted For</option>
											<option data-icon='status_warnings' value='2'>Few Accounted For</option>
											<option data-icon='status_closed' value='3'>None Accounted For</option>
											<option data-icon='status_unknown' value='4'>Unkown</option>
										</select>
									</div>
								</div>
							</div>
						</div> -->
						<!-- <div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">ATO Personnel Future Status Alarm Time *</div>
								<div class="f-value" data-bind="text:feat.attributes.AOPA_4S_AL_TM"> </div>
							</div>
						</div> -->
						<hr>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Personnel Sig Impact", "AOPS_IMP", array("", ""), false, false, "statusExplanationChange", "AOPS_IP_EX");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("Personnel Sig Impact Explanation", "AOPS_IP_EX");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("Personnel Sig Impact Supplemental Notes", "AOPS_IP_SN");
								// textDiv("", "");
							?>
						</div>
					</div>
				</div>
			</div>
			</li>

			<li><span>OSF Power Status Information</span>
			<div class="p-panel" title="OSF Status Information">
				<div class="p-sec clear">
					<div class="p-sec-header marg clear of-h">
						<span class="p-sec-title left txt-u">Power Status</span>
					</div>
					<div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("EG/UPS Supported", "EG_UPS_SUP");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("Backup Power Type", "BPWR_TP");
								// textDiv("", "");
							?>
						</div>
						<hr>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Backup Power Status", "BPWR_STS", array("OSF", "353"), false, false, "statusExplanationChange", "BPWR_EX");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("Backup Power Status Explanation", "BPWR_EX");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("Backup Power Status Supplemental Notes", "BPWR_SN");
								// textDiv("", "");
							?>
						</div>
						<hr>
						<div class="p-sec-row of-h clear">
							<?php							   
								dropDownDiv("Backup Power Use Status", "BPWR_USTS");
							?>
						</div>						
						<!-- Don't need future status alarms
						<div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Backup Power Future Status Alarm * <span data-field="OSF_BKUP_PWR_STS_ALM" class="f-his pointer"><i class="fa fa-tag"></i></span></div>
								<div class="f-value">
									<div class="input-group">
										<span class="input-group-addon">
											<img src="images/status/status_unknown.png" width="16" height="16" alt="ATM Overall Status"/>
										</span>
										<select class='f-status form-control'
											data-role='dropdownlist'
											data-bind='value:feat.attributes.OSF_BKUP_PWR_STS_ALM'>
											<option data-icon='status_unknown' value=''>Update Status</option>
											<option data-icon='status_open' value='0'>Normal Functioning</option>
											<option data-icon='status_warnings' value='1'>Limited/Degraded Functioning</option>
											<option data-icon='status_warnings' value='2'>Out of Service</option>
											<option data-icon='status_closed' value='3'>Other</option>
											<option data-icon='status_unknown' value='4'>Unknown</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Backup Power Future Status Alarm Time * </div>
								<div class="f-value" data-bind="text:feat.attributes.BKUP_FUT_STAT_AL"> </div>
							</div>
						</div>
						-->
					</div>				
				</div>
			</div>
			</li>

			<li><span>ATO Personnel Accounting</span>
			<div class="p-panel" title="ATO Personnel Accounting">
				<!-- ATO Personnel Accounting -->
				<div class="p-sec clear">
					<div class="p-sec-header marg clear of-h">
						<span class="p-sec-title left txt-u">Emergency Ops Levels</span>
					</div>
					<div class="p-sec-content clear of-h ui-widget-content ui-corner-all">
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Current Readiness Level", "CRT_RL", array("OSF", "370"), false, false, "statusExplanationChange", "CRT_RL_EX");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("Current Readiness Level Explanation", "CRT_RL_EX");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("Current Readiness Level Supplemental Notes", "CRT_RL_SN");
								// textDiv("", "");
							?>
						</div>
						<!-- No future alarms
						<div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Current Readiness Level Future Status Alarm<span data-field="CRT_RL_AL" class="f-his pointer"><i class="fa fa-tag"></i></span></div>
								<div class="f-value">
									<div class="input-group">
										<span class="input-group-addon">
											<img src="images/status/status_unknown.png" width="16" height="16" alt="Current Readiness Level Alarm"/>
										</span>
										<select class='f-status form-control'
											data-role='dropdownlist'
											data-bind='value:feat.attributes.CRT_RL_AL'>
											<option data-icon='status_unknown' value=''>Update Status</option>
											<option data-icon='status_open' value='0'>RL-Alpha</option>
											<option data-icon='status_warnings' value='1'>RL-Bravo</option>
											<option data-icon='status_warnings' value='2'>RL-Charlie</option>
											<option data-icon='status_closed' value='3'>RL-Delta</option>
											<option data-icon='status_unknown' value='4'>Unkown</option>
										</select>
									</div>
								</div>
							</div>
						</div>
							
						<div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Current Readiness Level Future Status Alarm Time * <span data-field="CRT_RL_AL_TM" class="f-his pointer"><i class="fa fa-tag"></i></span></div>
								<div class="f-value" data-bind="text:feat.attributes.CRT_RL_AL_TM"> </div>
							</div>
						</div>
						-->
						<hr>
						<div class="p-sec-row of-h clear">
							<?php
								dropDownDiv("Current SECON Level", "CRT_S_LVL", array("OSF", "374"), false, false, "statusExplanationChange", "CRT_SL_EX");
								// dropDownDiv("", "", $);
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								statusExplanationSpan("Current SECON level Explanation", "CRT_SL_EX");
								// textDiv("", "");
							?>
						</div>
						<div class="p-sec-row of-h clear">
							<?php
								textSupplementalNotesSpan("Current SECON Level Supplemental Notes", "CRT_SL_SN");
								// textDiv("", "");
							?>
						</div>
						<!-- No future status alarms
						<div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">SECON Level Future Status Alarm<span data-field="CRT_SL_AL" class="f-his pointer"><i class="fa fa-tag"></i></span></div>
								<div class="f-value">
									<div class="input-group">
										<span class="input-group-addon">
											<img src="images/status/status_unknown.png" width="16" height="16" alt="SECON alarm"/>
										</span>
										<select class='f-status form-control'
											data-role='dropdownlist'
											data-bind='value:feat.attributes.CRT_SL_AL'>
											<option data-icon='status_unknown' value=''>Update Status</option>
											<option data-icon='status_open' value='0'>SECON-Green</option>
											<option data-icon='status_warnings' value='1'>SECON-Blue</option>
											<option data-icon='status_warnings' value='2'>SECON-Orange</option>
											<option data-icon='status_warnings' value='3'>SECON-Yellow</option>
											<option data-icon='status_closed' value='4'>SECON-Red</option>
											<option data-icon='status_unknown' value='5'>Unknown</option>
											<option data-icon='status_unknown' value='6'>N/A</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="p-sec-row of-h clear">
							<div class="p-sub-sec left">
								<div class="f-label left">Current Readiness Level Future Status Alarm Time * <span data-field="OSF_SECON_ALM_TIME"></span></div>
								<div class="f-value" data-bind="text:feat.attributes.OSF_SECON_ALM_TIME"> </div>
							</div>
						</div> -->
						<hr>
						<div class="p-sec-row of-h clear">
							<?php
								iwlDiv("IWL Membership", "IWL_MBER");
							?>
						</div>						
						<div class="p-sec-row of-h clear">
							<?php
								textDiv("OSF Other Notes", "OSF_NOTES");
								// textDiv("", "");
							?>
						</div>
					</div>
				</div>
			</div>
			</li>
		</ul>
		</div>
	</div>
</div>