<!-- map styles -->
<link rel="stylesheet" type="text/css" href="deps/ColorPicker/css/colpick.css" />
<link rel="stylesheet" type="text/css" href="styles/app.css" />
<!-- map view -->
<div id="map-header" class="ui-widget-header">
	<div id="menu-bar" class=''>
		<div id="menu-map-items" class="menu-group">
			<div class='menu-item'>
				<img id="basemaps" class="" src="images/menu/map1.png" title="Basemaps" alt="Basemaps" width="26" height="26" />
				<div class='menu-label'>Maps</div>
			</div>
			<div class='menu-item'>
				<img id="layers" class="" src="images/menu/layers1.png" title="Map Layers" alt="Layers" width="26" height="26" />
				<div class='menu-label'>Layers</div>
			</div>
			<div class='menu-item'>
				<img id="legend" class="" src="images/menu/legend1.png" title="Map Legend" alt="Legend" width="26" height="26" />
				<div class='menu-label'>Legend</div>
			</div>
			<div class='menu-item'>
				<img id="drawing" class="" src="images/menu/tools1.png" title="Shape Tools" alt="Shape Tools" width="26" height="26" />
				<div class='menu-label'>Shapes</div>
			</div>
			<div class='menu-item'>
				<img id="filter" class="" src="images/menu/find3.png" title="Filter Tools" alt="Filter Tools" width="26" height="26" />
				<div class='menu-label'>Filter</div>
			</div>
			<div class='menu-item'>
				<img id="directions" class="" src="images/menu/directions.png" title="Directions" alt="Directions" width="26" height="26" />
				<div class='menu-label'>Routes</div>
			</div>
		</div>
		<div id="menu-view-items" class="menu-group">
			<div class='menu-item'>
				<img id="tableview" class="" src="images/menu/table1.png" title="Table View" alt="Table View" width="26" height="26" />
				<div class='menu-label'>Table</div>
			</div>
			<div class='menu-item'>
				<img id="dashboard" class="" src="images/menu/office-chart-bar.png" title="Dashboard" alt="Dashboard" width="26" height="26" />
				<div class='menu-label'>Board</div>
			</div>
			<div class='menu-item'>
				<img id="reportsview" class="" src="images/menu/reports1.png" title="Reports" alt="Reports" width="26" height="26" />
				<div class='menu-label'>Reports</div>
			</div>
		</div>
		<div id="menu-utils-items" class="menu-group">
			<!--<div class='menu-item'>
				<img id="save" class="" src="images/menu/document-save-5.png" title="Save" width="26" height="26" />
				<div class='menu-label'>Save</div>
			</div>
			<div class='menu-item'>
				<img id="load" class="" src="images/menu/document-open-5.png" title="Load" width="26" height="26" />
				<div class='menu-label'>Load</div>
			</div>-->
			<div class='menu-item'>
				<img id="print" class="" src="images/menu/document-print-5.png" title="Print" alt="Print" width="26" height="26" />
				<div class='menu-label'>Print</div>
			</div>
			<div class='menu-item'>
				<img id="help" class="" src="images/menu/help1.png" title="Help" alt="Help" width="26" height="26" />
				<div class='menu-label'>Help</div>
			</div>
		</div>
	</div>
	<div id="" class="search-box right">
		<input id="search-box" type="search" placeholder="Find Places"/>
		<div class='menu-label'>Search &amp; Geocoding</div>
	</div>
</div>

<div id="map" class="views ui-widget-content">
	<!-- Find location candiates -->
	<div id="locations-div" class="tools-dialog dialog" title="Locations Results">
		<div id="addresses"></div>
	</div>
	<!-- map navigations -->
    <div id="nav-div"> 
        <div id="panDiv" class="navItem">
            <div id="panUp" class="panDivs navItem">
            	<img alt="Pan Up" src="images/nav/up.png" width="20" height="20"/>
			</div>
            <div id="panUpperRight" class="panDivs navItem"> </div>
            <div id="panRight" class="panDivs navItem">
            	<img alt="Pan Right" src="images/nav/right.png" width="20" height="20"/>
			</div>
            <div id="panLowerRight" class="panDivs navItem"> </div>
            <div id="panDown" class="panDivs navItem">
            	<img alt="Pan Down" src="images/nav/down.png" width="20" height="20"/>
			</div>
            <div id="panLowerLeft" class="panDivs navItem"> </div>
            <div id="panLeft" class="panDivs navItem">
            	<img alt="Pan Left" src="images/nav/left.png" width="20" height="20"/>
			</div>
            <div id="panUpperLeft" class="panDivs navItem"> </div> 
            <div id="fullExtent" class="panDivs navItem">
            	<img alt="Full Extent" src="images/nav/world3.png" width="20" height="20"/>
			</div> 
        </div>
        <div id="extentHistory" class="">
            <div id="nav-previous" class="hisDivs navItem">
            	<img alt="Previous" src="images/nav/previous7.png" width="20" height="15"/>
			</div>
            <div id="nav-next" class="hisDivs navItem">
            	<img alt="Next" src="images/nav/next7.png" width="20" height="15"/>
			</div>
        </div>
        <div id="zoomSlider" class="panDivs navItem">
        	<!--<div id="zoomRules"></div>-->
		</div>
        <div id="info" class="navDivs navItem">
        	<img alt="Info" src="images/nav/info7.png" width="24" height="24"/>
		</div>
		<div id="navToggleTools">
        <div id="nav-pan" class="navDivs navItem">
        	<img alt="Pan" src="images/nav/pan4.png" width="24" height="24"/>
		</div>
        <div id="nav-zoominbox" class="navDivs navItem">
        	<img alt="Zoom In Box" src="images/nav/zoominbox1.png" width="24" height="24"/>
		</div>
        <div id="nav-zoomoutbox" class="navDivs navItem">
        	<img alt="Zoom Out Box" src="images/nav/zoomoutbox1.png" width="24" height="24"/>
		</div>
		</div>
    </div>
	<!-- drawing shapes -->
	<div id="drawing-div" class="tools-dialog dialog" title="Shape Tools"> 
		<div id="draw-options" class="dialog-options">
			<input type="radio" name="drawings" id="template-tools" class="draw-option" checked="checked"><label for="template-tools">Templates</label>
			<input type="radio" name="drawings" id="draw-tools" class="draw-option"><label for="draw-tools">Draw</label>
			<input type="radio" name="drawings" id="style-tools" class="draw-option"><label for="style-tools">Styles</label>
			<input type="radio" name="drawings" id="file-tools" class="draw-option"><label for="file-tools">Upload</label>
			<input type="radio" name="drawings" id="list-tools" class="draw-option"><label for="list-tools">List</label>
			<input type="radio" name="drawings" id="measure-tools" class="draw-option"><label for="measure-tools">Measure</label>
		</div>
		<!-- Feature editor -->
		<div id="featEditor"> </div>
		<!-- Wrapper for drawing options -->
		<div id="draw-options-div" class="dialog-options-div pad">
			<!-- draw grphics based on a template -->
			<div id="templatetools" class="draw-type clear show">
				<!--<p>Use Templates to create features</p>-->
				<div id="featTemplates" class=""> </div>
			</div>
			<!-- draw grphics with shape tools -->
			<div id="drawtools" class="draw-type clear hide">
			   	<!--<div id="shapes">
			   		<img alt="Point" id="point" class="shape" src="images/draw/draw_point.png" title="Point" width="20" height="20" />
					<img alt="Polyline" id="polyline" class="shape" src="images/draw/draw_line.png" title="Polyline" width="20" height="20" />
					<img alt="Freehand Polyline" id="freehand_polyline" class="shape" src="images/draw/draw_freeline.png" title="Freehand Polyline" width="20" height="20" />
					<img alt="Rectangle" id="rectangle" class="shape" src="images/draw/draw_rect.png" title="Rectangle" width="20" height="20" />
					<img alt="Triangle" id="triangle" class="shape" src="images/draw/draw_triangle.png" title="Triangle" width="20" height="20" />
					<img alt="Arrow" id="arrow" class="shape" src="images/draw/draw_arrow.png" title="Arrow" width="20" height="20" />
					<img alt="Circle" id="circle" class="shape" src="images/draw/draw_circle.png" title="Circle" width="20" height="20" />
					<img alt="Ellipse" id="ellipse" class="shape" src="images/draw/draw_ellipse.png" title="Ellipse" width="20" height="20" />
					<img alt="Polygon" id="polygon" class="shape" src="images/draw/draw_poly.png" title="Polygon" width="20" height="20" />
					<img alt="Freehand Polygon" id="freehand_polygon" class="shape" src="images/draw/draw_freepoly.png" title="Freehand Polygon" width="20" height="20" />
					<img alt="Text" id="text" class="shape" src="images/draw/draw_text.png" title="Text" width="20" height="20" />
					<img alt="Clear Graphics" id="cleargraphics" class="shape" src="images/draw/draw_clear.png" title="Clear" width="20" height="20" />
					<img alt="Deactivate Tool" id="deactivateDT" class="shape" src="images/draw/draw_deactivate.png" title="Deactivate" width="20" height="20" />
			   	</div>-->
				<div id="" class="marg clear">
					<div id='' class='left'>
						<select id="shapes" class="dt-entry"></select>
					</div>
					<div id='' class='marg left'>
						<input type='radio' id='shape-entry-draw' name='shape-entry' checked='checked'/> Draw
						<input type='radio' id='shape-entry-manual' name='shape-entry'/> Enter Vertices
					</div>
				</div>
				<!-- Cleate butter -->
			   	<div id="shape-options" class="marg clear">
					<!-- draw buffers -->
					<div id="buffertools" class="shape-options">
						<div class="clear">
							<div class="left">
								<p>Latitude</p>
								<input type="text" id="bLat" name="bLatitude" class="textField bLatLon" value="" />
							</div>
							<div class="left margLeft">
								<p>Longitude</p>
								<input type="text" id="bLon" name="bLongitude" class="textField bLatLon" value="" />
							</div>
							<div class="left margLeft">
								<p>Units</p>
								<select id="bLatLonUnit" class="dropDown">
									<option value="">Degrees</option>
									<option value="">Meters</option>
								</select>
							</div>
						</div>
						<div class="left clear">
							<p>Buffer Radius</p>
							<p><input type="text" id="bufferRadius" name="bufferRadius" class="spinner" value="50" /></p>
						</div>
						<div class="left margLeft">
							<p>Buffer Radius Unit</p>
							<select id="bufferRadiusUnit" class="dropDown">
								<option value="UNIT_STATUTE_MILE">Miles</option>
								<option value="UNIT_NAUTICAL_MILE">Nautical Miles</option>
								<option value="UNIT_KILOMETER">KM</option>
								<option value="UNIT_METER">Meter</option>
								<option value="UNIT_FOOT">Foot</option>
							</select>
						</div>
						<p><button id="createBuffer" class="btn">Create Buffer</button>
							<button id="clearBuffer" class="btn">Clear</button></p>
					</div>
					<!-- draw service areas -->
					<div id="svcAreaTools" class="shape-options">
						<div class="clear">
							<div class="left">
								<p>Latitude</p>
								<input type="text" id="saLat" name="saLatitude" class="textField bLatLon" value="" />
							</div>
							<div class="left margLeft">
								<p>Longitude</p>
								<input type="text" id="saLon" name="saLongitude" class="textField bLatLon" value="" />
							</div>
							<div class="left margLeft">
								<p>Units</p>
								<select id="saLatLonUnit" class="dropDown">
									<option value="">Degrees</option>
									<option value="">Meters</option>
								</select>
							</select>
							</div>
						</div>
						<div class="left clear">
							<p>Service Radius</p>
							<p><input type="text" id="saRadius" name="saRadius" class="spinner" value="50" /></p>
						</div>
						<div class="left margLeft">
							<p>Distance Unit</p>
							<select id="saRadiusUnit" class="dropDown">
								<option value="UNIT_STATUTE_MILE">Miles</option>
								<option value="UNIT_NAUTICAL_MILE">Nautical Miles</option>
								<option value="UNIT_US_NAUTICAL_MILE">U.S. Nautical Miles</option>
								<option value="UNIT_KILOMETER">KM</option>
								<option value="UNIT_METER">Meter</option>
								<option value="UNIT_FOOT">Foot</option>
							</select>
						</div>
						<p><button id="createSA" class="btn">Create Service Area</button>
							<button id="clearSA" class="btn">Clear</button></p>
					</div>
					<!-- draw points-->
			   		<div id="point-shape" class="shape-options">
			   			<table summary="Point Shape Options">
			   				<tr>
			   					<td><span class="shape-options-label">Marker Style:</span></td>
								<td>
									<select id="marker-style" class="shapes-style dropDown">
										<option value="STYLE_CIRCLE">Circle</option>
										<option value="STYLE_SQUARE">Square</option>
										<option value="STYLE_DIAMOND">Diamond</option>
										<option value="STYLE_CROSS">Cross</option>
										<option value="STYLE_X">X</option>
									</select>
								</td>
								<td><span class="shape-options-label">Marker Color:</span></td>
								<td><div id="marker-color" class="shapes-color fill-color"></div></td>
			   				</tr>
							<tr>
			   					<td><span class="shape-options-label">Outline Style:</span></td>
								<td>
									<select id="marker-outline-style" class="shapes-style dropDown">
										<option value="STYLE_SOLID">Solid</option>
										<option value="STYLE_DASH">Dash</option>
										<option value="STYLE_DOT">Dot</option>
									</select>
								</td>
								<td><span class="shape-options-label">Outline Color:</span></td>
								<td><div id="marker-outline-color" class="shapes-color outline-color"></div></td>
			   				</tr>
							<tr>
			   					<td><span class="shape-options-label">Marker Size:</span></td>
								<td><input type="text" id="marker-size" class="shapes-size" value="15"/></td>
								<td><span class="shape-options-label">Size Units:</span></td>
								<td>
									<select id="marker-size-unit" class="size-units dropDown">
										<option value="px">Pixels</option>
										<option value="mi">Miles</option>
										<option value="km">Km</option>
									</select>
								</td>
			   				</tr>
							<tr>
								<td><span class="shape-options-label">Outline Width:</span></td>
								<td><input type="text" id="marker-outline-width" class="shapes-outline-width" value="2"/></td>
			   					<td><span class="shape-options-label">Transparency:</span></td>
								<td><input type="text" id="marker-trans" class="shapes-trans" value="0.75"/></td>
			   				</tr>
			   			</table>
			   		</div>
					<!-- draw lines-->
					<div id="line-shape" class="drawOptions">
			   			<table summary="Line Shape Options">
			   				<tr>
			   					<td><span class="shape-options-label">Line Style:</span></td>
								<td>
									<select id="line-style" class="shapes-style dropDown">
										<option value="STYLE_SOLID">Solid</option>
										<option value="STYLE_DASH">Dash</option>
										<option value="STYLE_DOT">Dot</option>
									</select>
								</td>
								<td><span class="shape-options-label">Line Color:</span></td>
								<td><div id="line-color" class="shapes-color fill-color"></div></td>
			   				</tr>
							<tr>
								<td><span class="shape-options-label">Outline Width:</span></td>
								<td><input type="text" id="line-width" class="shapes-outline-width" value="2"/></td>
			   					<td><span class="shape-options-label">Transparency:</span></td>
								<td><input type="text" id="line-trans" class="shapes-trans" value="0.75"/></td>
			   				</tr>
			   			</table>
			   		</div>
					<!-- draw polygons -->
					<div id="polygon-shape" class="drawOptions">
			   			<table summary="Polygon Shape Options">
			   				<tr>
			   					<td><span class="shape-options-label">Outline Style:</span></td>
								<td>
									<select id="polygon-outline-style" class="shapes-style dropDown">
										<option value="STYLE_SOLID">Solid</option>
										<option value="STYLE_DASH">Dash</option>
										<option value="STYLE_DOT">Dot</option>
									</select>
								</td>
								<td><span class="shape-options-label">Fill Color:</span></td>
								<td><div id="polygon-color" class="shapes-color fill-color"></div></td>
			   				</tr>
							<tr>
			   					<td><span class="shape-options-label">Outline Width:</span></td>
								<td><input type="text" id="polygon-outline-width" class="shapes-outline-width" value="1"/></td>
								<td><span class="shape-options-label">Outline Color:</span></td>
								<td><div id="polygon-outline-color" class="shapes-color outline-color"></div></td>
			   				</tr>
							<tr id="twoDims">
			   					<td><span class="shape-options-label">Width:</span></td>
								<td><input type="text" id="width-size" class="shapes-size polygon-size" value="10"/></td>
								<td><span class="shape-options-label">Height:</span></td>
								<td><input type="text" id="height-size" class="shapes-size polygon-size" value="10"/></td>
			   				</tr>
							<tr id="circleRadius">
			   					<td><span class="shape-options-label">Radius:</span></td>
								<td><input type="text" id="radius-size" class="shapes-size polygon-size" value="10"/></td>
								<td></td>
								<td></td>
			   				</tr>
							<tr id="triangleSide">
			   					<td><span class="shape-options-label">Side Length:</span></td>
								<td><input type="text" id="side-size" class="shapes-size polygon-size" value="10"/></td>
								<td></td>
								<td></td>
			   				</tr>
							<tr>
								<td><span class="shape-options-label">Transparency:</span></td>
								<td><input type="text" id="polygon-trans" class="shapes-trans" value="0.75"/></td>
								<td class="polygon-size-units"><span class="shape-options-label">Size Units:</span></td>
								<td class="polygon-size-units">
									<select id="polygon-size-unit" class="size-units dropDown">
										<option value="MILES">Miles</option>
										<option value="KILOMETERS">Km</option>
										<option value="METERS">Meters</option>
										<option value="FEET">Feet</option>
									</select>
								</td>
			   				</tr>
			   			</table>
			   		</div>
					<!-- draw labels -->
					<div id="text-shape" class="drawOptions">
			   			<table summary="Text Shape Options">
			   				<tr>
			   					<td><span class="shape-options-label">Text:</span></td>
								<td colspan="3"><input type="text" id="label-text" class="" value=""/></td>
			   				</tr>
							<tr>
			   					<td><span class="shape-options-label">Font:</span></td>
								<td colspan="3">
									<select id="label-font" class="font-style dropDown">
										<option value="Arial">Arial</option>
										<option value="Arial Black">Arial Black</option>
										<option value="Carrier New">Carrier New</option>
										<option value="Comic Sans MS">Comic Sans MS</option>
										<option value="Georgia">Georgia</option>
										<option value="Impact">Impact</option>
										<option value="Helvetica">Helvetica</option>
										<option value="Lucida Console">Lucida Console</option>
										<option value="Tahoma">Tahoma</option>
										<option value="Times New Roman">Times New Roman</option>
										<option value="Verdana">Verdana</option>
									</select>
								</td>
			   				</tr>
							<tr>
								<td><span class="shape-options-label">Color:</span></td>
								<td><div id="font-color" class="shapes-color fill-color"></div></td>
			   					<td><span class="shape-options-label">Size:</span></td>
								<td><input type="text" id="font-size" class="shapes-size" value="15"/></td>
			   				</tr>
							<tr>
								<td><span class="shape-options-label">Format:</span></td>
								<td>
									<div id="font-formats">
										<input type="checkbox" id="font-bold" class="font-format"><label for="font-bold" id="fbold">B</label>
										<input type="checkbox" id="font-italic" class="font-format"><label for="font-italic" id="fitalic">I</label>
										<input type="checkbox" id="font-underline" class="font-format"><label for="font-underline" id="funderline">U</label>
									</div>
								</td>
			   					<td><span class="shape-options-label">Transparency:</span></td>
								<td><input type="text" id="font-trans" class="shapes-trans" value="0.75"/></td>
			   				</tr>
			   			</table>
			   		</div>
			   	</div>
			</div>
			<!-- style features-->
			<div id="styletools" class="draw-type clear hide">
				<p id='style-guide'>There is no feature selected.</p>
				<div id="featureStyle" class='clear show'> 
					<div id='selFeatStyle' class='clear'>
						<div id='style-type' class='margBottom clear'>
							<select id='styleTypeChoice' class='w300 dropDown'>
								<option value=''>Select a style type</option>
								<option value='marker'>Marker Styles</option>
								<option value='picture'>Picture Styles</option>
							</select>
						</div>
						<div id='styles-settings' class='margBottom clear show'>
							<div id='marker-styles' class='clear hide'></div>
							<div id='picture-styles' class='clear show'>
								<form>
									<div id='' class='margBottom clear left'>
										<label for='widthPMS'>Width</label>
										<input type='number' id='widthPMS' class='w100' name='widthPMS' value='15'/>
										<label for='heightPMS'>Height</label>
										<input type='number' id='heightPMS' class='w100' name='heightPMS' value='15'/>
									</div>
									<div id='' class='margBottom clear left'>
										<label for='picturePMS'>Picture</label>
										<input type='file' id='picturePMS' name='picture'/>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div id='' class='clear'>
						<button id='' class='btn'>Update Style</button>
						<button id='' class='btn'>Reset</button>
						<button id='' class='btn hide'>Save as Template</button>
					</div>
				</div>
			</div>
			<!-- draw graphics baseed on file upload-->
			<div id="filetools" class="draw-type clear hide">
				<form  id="ft-uploadForm" method="post" enctype="multipart/form-data">
					<p>Upload zipped shapefile (kml or csv) files</p>
					<p><input id="ft-uploadFile" type="file" name="file"/></p>
					<p><input type="button" id="ft-submitFile" class="btn" value="Display" />
					<input type="reset" id="ft-clearFile" class="btn" value="Clear" /></p>
					<p id="ft-processMsg"> </p>
				</form>
			</div>
			<!-- draw graphics based on a list -->
			<div id="listtools" class="draw-type clear hide">
				<div class="margBottom clear">
					<select id="lt-layers" class="dropDown">
						<option value="">Select a layer ...</option>
						<option value="hsip_US_States">U.S. States</option>
						<option value="hsip_US_Counties">U.S. Counties</option>
						<option value="census_Zip_Codes">U.S. Zip Codes</option>
					</select>
				</div>
				<div class="margBottom clear">
					<textarea id="lt-featList" class="textArea" placeholder="Enter a list of features ..."></textarea>
				</div>
				<div class="clear">
					<button id="lt-submit" class="btn">Submit</button>
					<button id="lt-clear" class="btn">Clear</button>
				</div>
				<p id="lt-processMsg"> </p>
			</div>
			<!-- Measurement tools -->
			<div id="measuretools" class="draw-type clear hide">
				<span>Select a tool to activate measurement</span>
				<div id="measurements" class="clear"></div>
			</div>
		</div>
	</div>
	<!-- Defunct Filter View -->
	<div id="filter-div" title="Filter Features" class="tools-dialog dialog">
		<!--<div id="filter-options" class="dialog-options">
			<input type="radio" name="filter" id="filterAttr" class="filter-option" checked="checked"><label for="filterAttr">Attributes</label>
			<input type="radio" name="filter" id="filterArea" class="filter-option"><label for="filterArea">Spatial Area</label>
		</div>-->
		<div id="filter-layers-options" class="dialog-options-div marg">
			<p>Select * features from <span id="sel-fLayer">....</span></p>
		    <select id="filterLayers" name="filterLayers" class="featLayersList clearTop clearBottom"> 
		        <option value="">Select a Layer</option>
	        </select>
		</div>
		<div id="filter-options-div" class="dialog-options-div marg">
			<div id="filterByAttr" class="dialog-option-div">
			    <p>Where</p>
				<div class="margBottom left">
					<div id="filterLayersFields-wrap" class="margBottom left">
					    <select id="filterLayersFields" name="filterLayersFields" class="dropDown">
					        <option value="">Select a field</option>
					    </select>
					</div>
					<div class="margLeft left">
						<button id="loadFilterValues" class="btn">Load</button>
					</div>
				</div>
				<!--<p>Build your query</p>-->
				<div id="filterByAttr-options" class="clear">
					<div id="filter-criteria" class="left">
						<ul id="f-criteria-list" class="">
							<li><span id="f-eq" class="f-criteria btn margRight margBottom">&#61;</span></li>
							<li><span id="f-neq" class="f-criteria btn margRight margBottom">&#60;&#62;</span></li>
							<li><span id="f-like" class="f-criteria btn margRight margBottom">LIKE</span></li>
							<li><span id="f-lt" class="f-criteria btn margRight margBottom">&#60;</span></li>
							<li><span id="f-lteq" class="f-criteria btn margRight margBottom">&#60;&#61;</span></li>
							<li><span id="f-or" class="f-criteria btn margRight margBottom">OR</span></li>
							<li><span id="f-gt" class="f-criteria btn margRight margBottom">&#62;</span></li>
							<li><span id="f-gteq" class="f-criteria btn margRight margBottom">&#62;&#61;</span></li>
							<li><span id="f-not" class="f-criteria btn margRight margBottom">NOT</span></li>
							<li><span id="f-perc" class="f-criteria btn margRight margBottom">&#37;</span></li>
							<li><span id="f-par" class="f-criteria btn margRight margBottom">( )</span></li>
							<li><span id="f-pipe" class="f-criteria btn margRight margBottom">!&#61;</span></li>
						</ul>
					</div>
					<div id="fField-values-wrap" class="dialog-option-div right">
						<select id="fField-values" class="dropDown">
							<option value="">Load values</option>
						</select>
					</div>
				</div>
				<div class="margTop margBottom">
					<textarea id="filterQuery" class="textArea" placeholder="Enter the where clause of your query string here ..."> </textarea>
				</div>
				<p><button id="runAttrFilter" class="btn">Filter</button>
				<button id="clearAttrFilter" class="btn">Clear Filter</button></p>
			</div>
			<div id="filterByArea" class="dialog-option-div">
				<p>Where features</p>
				<select id="fShapeCriteria" class="dropDown">
					<option value="SPATIAL_REL_INTERSECTS">Intersects</option>
					<option value="SPATIAL_REL_CONTAINS">Contains</option>
					<option value="SPATIAL_REL_CROSSES">Crosses</option>
					<option value="SPATIAL_REL_OVERLAPS">Overlaps</option>
					<option value="SPATIAL_REL_TOUCHES">Touches</option>
					<option value="SPATIAL_REL_WITHIN">Within</option>
				</select>
				<p>Geospatial Query Polygon(s)</p>
				<div class="margBottom">
					<div id="fShapeList-wrap" class="margBottom left">
						<select id="fShapeList" class="listBox" multiple="multiple">
							
						</select>
					</div>
					<div class="right">
						<div class="margBottom right clear">
							<button id="loadGQP" class="btn">Refesh</button>
						</div>
						<div class="margBottom left clear">
							<input type="radio" id="unionGQPs" name="gqpGroup" class="radioBox" title="Union of GQP(s)" checked="checked"> OR
						</div>
						<div class="margBottom left clear">
							<input type="radio" id="interGQPs" name="gqpGroup" class="radioBox" title="Intersection of GQP(s)"> AND
						</div>
					</div>
				</div>
				<p><button id="runSpFilter" class="btn">Filter</button>
				<button id="clearSpFilter" class="btn">Clear Filter</button></p>
			</div>
			<p id="filterMsg"> </p>
		    <div id="filterResults"> </div>
		</div>
	</div>
</div>

<!-- table-view -->
<div id="tableview-div" class="dialog" title="Table View">
	<div id="tbl-panel" class="panel left" title='Query &amp; Filter Panel'>
		<!--<div id='' class="marg clear">
			<p>Change background transparency:</p>
			<div id='tbl-bg-color' class='marg'></div>
		</div>-->
		<div id="tbl-panel-query" class="clear abs">
			<div class="marg clear">
				<span>Select all features within:</span>
			</div>
			<div id="tbl-lyrs-wrap" class="clear marg">
				<select id="tbl-lyrs" class="">
					<option value=''>Select a layer</option>
				</select>
			</div>
			<div class="marg clear">
				<span id="tbl-filter-msg" class=""></span>
			</div>
			<div id="tbl-fields-picker-div" class="clear tbl-sub-panels" title="Fields Picker">
				<div id="tbl-fields-picker-wrap" class="marg">
					<div id="tbl-fields-picker" class="">
						<select id="tfpSourceData" name="tfpSourceData"></select>
						<select id="tfpTargetData" name="tfpTargetData"></select>
					</div>
				</div>
				<div class="clear marg txtCenter">
					<button id="tbl-update" class="btn">Update Table</button>
				</div>
			</div>
			<div id="" class="clear tbl-sub-panels" title="Filter by Areas">
				<div id="" class="clear">
					<div class="marg clear">
						<div id="" class="margBottom left">
							<select id="tbl-sp-rel" class="">
								<option value="">Query Relationship?</option>
								<option value="SPATIAL_REL_INTERSECTS" selected="selected">Intersects</option>
								<option value="SPATIAL_REL_WITHIN">Proximity</option>
							</select>
						</div>
						<div id="" class="margLeft margBottom left">
							<input type='number' id='tbl-sp-rel-val' name='tbl-sp-rel-val' class='' value='0' disabled='disabled'/> Miles
						</div>
					</div>
					<div id="tbl-filter-areas" class="marg clear">
						<input type="radio" id="tbl-map-extent" class="margBottom" name="tbl-map-extent" title="Result will only include features in current map extent" checked="checked" /> Map View 
						<input type="radio" id="tbl-full-extent" class="margBottom" name="tbl-map-extent" title="Result will include all features in the layer" /> Full Extent
						<input type="radio" id="tbl-filter-shapes" class="margBottom" name="tbl-map-extent" title="Result will only include features in selected filter shapes" /> Filter Shapes
					</div>
					<div id="tbl-gqps" class="marg clear">
						<div id="tbl-fs-list-wrap" class="left">
							<select id="tbl-fs-list" class="listbox">
								<option value="">Refresh Filter Shapes</option>
							</select>
						</div>
						<div id="" class="margLeft left">
							<span>FS Logic</span><br>
							<input type="radio" id="tbl-interGQPs" class="margTop margBottom" name="tbl-gqps-rel" value="AND" title="Return features within the intersection of the selected Filter Shapes" /> AND <br>
							<input type="radio" id="tbl-unionGQPs" class="margBottom" name="tbl-gqps-rel" value="OR" checked="checked" title="Return features within selected Filter Shapes"/> OR
						</div>
						<div id="" class="margTop clear">
							<button id="tbl-get-fs" class="btn tbl-btn margTop margBottom">Refresh</button>
							<button id="tbl-fs-filter" class="btn margTop margBottom">Apply Filter</button>
							<button id="tbl-fs-clear" class="btn tbl-btn margTop margBottom">Clear</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="" class="clear tbl-sub-panels" title="Filter by attributes">
			<div id="tbl-options" class="clear">
				<div class="left clear marg">
					<span>Select fields to filter:</span>
				</div>
				<div id="" class="left clear marg">
					<select id="tbl-select-fields" class="">
						<option>Select a field ...</option>
					</select>
				</div>
				<div id="tbl-columns" class="left marg clear"></div>
				<div class="left clear marg">
					<button id="tbl-attr-clear" class="btn">Clear Filter</button>
				</div>
			</div>
		</div>
		<div id="" class="clear tbl-sub-panels" title="Table Tools">
			<div id="" class="clear">
				<div class="left marg">
					<button id="tbl-update-map" class="btn left">Update Map</button>
				</div>
				<div class="left marg">
					<!--<span title="How to exclude features from filter result">Exclusion</span>-->
					<input type="radio" id="tbl-feat-hide" class="marg" name="tbl-feat-display" value="hide" checked="checked" title="Hide unmatched features"/> Hide
					<input type="radio" id="tbl-feat-gray" class="marg" name="tbl-feat-display" value="gray" title="Grayed unmatched features"/> Grayed
				</div>
			</div>
			<div class="clear marg">
				<button id="tbl-refresh" class="btn left">Save as IWL</button>
			</div>
			<div class="left clear marg">
				<span>Export Table Content</span>
			</div>
			<div class="clear">
				<!--<a id="dataDownload" download="NISISData.csv" class="btn tbl-btn margBottom">Export</a><br>-->
				<div id="tbl-exports" class="marg"></div>
			</div>
		</div>
	</div>
	
	<div id="tablewrapper" class="tbl-wrapper pad right">
		<table id="table" class="tablesorter" summary="">
			<!--<thead><tr><th></th></tr>
			</thead>
			<tbody><tr><td></td></tr>
			</tbody>
			<tfoot></tfoot>-->
		</table>
	</div>
</div>

<!-- reports-view -->

<!-- Contextual menu -->
<script type="text/javascript" src="deps/ColorPicker/js/colpick.js"></script>
<script type="text/javascript" src="deps/jquery.xml2json.js"></script>
<script type="text/javascript" src="deps/togeojson.js"></script>
<script type="text/javascript" src="scripts/mapview.js"></script>
<script type="text/javascript" src="scripts/mapview.renderer.js"></script>
<script type="text/javascript" src="scripts/mapview.layers.js"></script>
<script type="text/javascript" src="scripts/mapview.util.js"></script>
<script type="text/javascript" src="scripts/mapview.profiles.js"></script>
<script type="text/javascript" src="scripts/app.js"></script>