<?php
    //This file is shown to the user if the user guide is missing from the server
    require "../handler.php";

    error_log("NISIS User Guide is missing or cannot be read from the nisis directory: " . $userGuidePath);
?>
<html>
<head><title>NISIS - missing user guide</title></head>
<body style="text-align: center; font-size: x-large;">
<br />
<p>If you are seeing this page, the NISIS User Guide cannot be accessed.</p>
<p>Please contact NISIS Support.</p>
</body>
</html>