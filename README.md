NISIS
=====

NAS Integrated Status Insight System
Copyright 2013, Concept Solutions, Adapt team
All Rights Reserved


REQUIREMENTS
------------
 * A HTTP Server capable of serving up PHP 5.3+ (developed under Apache)
 * Oracle Database Server


SETUP
-----
1. Clone NISIS into your webserver's web-facing directory.
2. Modify settings.php file to set the database credentials.
3. Run setup/oracle.sql against your database.


SETTINGS FILES
-----
There's a settings file for each test environment. The code only references the "main" settings files, settings.php & settings.js, which contain the right settings for local development.

When deploying to a test environment, copy the appropriate env.settings files to overwrite the main files. This step should be automated in the git repository for that environment. In Dev, use the "post-merge" hook because we're still manually running "git pull" on the box. In UAT, use the "post-receive" hook since we've automated pushing a branch to that box.

Note that running "git status" on those test environments will show modified files for the main settings files. These can be ignored.


EXTENSIONS
-----
The following PHP extensions need to be enabled. If you are using WAMP, they can be checked on using the 'PHP extensions' menu:
	php_oci8
	php_oci8_11g
	php_openssl

You may be missing an Oracle client if you are seeing errors when 'oci' functions are called:
1. Download the Oracle Instant Client - Basic Lite (if not already installed)
    http://www.oracle.com/technetwork/topics/winsoft-085727.html      <-- 32-bit
    http://www.oracle.com/technetwork/topics/winx64soft-089540.html   <-- 64-bit
2. Unzip the file, and put the instant client directory it in your PATH
3. Restart WAMP after ensuring the oci extensions are enabled as described above.


EMAIL
-----
Some of the PHP code (such as the password reset) requires the ability to send email. In order to allow your
local environment to send email, you can follow these instructions to set up a local SMTP mailserver. Otherwise, you
may be able to connect the PHP settings to a different mailserver (you're on your own if you want to try that).
1. Download Mercury Mail from: http://www.pmail.com/downloads_s3_t.htm
    - You will want the 'Mercury/32 Mail Transport System for Win32'
2. Run the install.
    - Skip Novell
    - Skip Pegasus Mail Integration (unless you use Pegasus Mail)
    - When prompted for a list of modules, you only need SMTP
    - When prompted for which SMTP client, I chose MercuryE
    - When prompted for your domain name and postmaster, I used 'cscorp.local' for the domain, and used my Windows login name as the 'postmaster'
    - Use the least strict relaying option
3. Start up the SMTP server by running 'Mercury for Win32'
4. Goto Configuration -> MercuryE STMP Client -> Name Servers text field to enter your DNS Server info
    - if you don't know the details, run this from the command line: 'ipconfig /all' and look for lines starting with 'DNS Servers'
4. Your SMTP server will be running until you close the Mercury Window. PHP and Mercury should assume the defaults, so
    no further configuration necessary.


USER GUIDE / HELP
-----------------
Please note that the NISIS User Guide pdf is not tracked in Git. The file must exist at the location defined in handler.php - line 16 ($userGuidePath). The application will follow that path starting with the root nisis directory.

If the file exists, the user guide will appear in a new tab (or start a download dialog) when the help button is pressed.


Technologies
------------
ESRI ArcGIS Client
jQuery
jQueryUI
Kendo UI

