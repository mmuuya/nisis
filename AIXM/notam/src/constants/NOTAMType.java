package constants;

public enum NOTAMType {
    CANCEL("C", "CANCEL"),
    NEW("N", "NEW"),
    REPLACE("R", "REPLACE");
    
    private String id;
    private final String type;
    private NOTAMType(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public static NOTAMType getByKey(String id) {
        if(id==null) {
            return NEW;
        }
        for (NOTAMType one : NOTAMType.values()) {
            if (id.equalsIgnoreCase(one.id)) {
                return one;
            }
        }
        return NEW;
    }

    public static NOTAMType getByType(String type) {
        for (NOTAMType one : NOTAMType.values()) {
            if (type.equals(one.type)) {
                return one;
            }
        }
        System.out.println("NOTAMType: returning default NEW type, no suitable type provided");    
        return NEW;
    }

    public String getKey() {
        return id;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
