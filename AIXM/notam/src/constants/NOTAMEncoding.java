package constants;

public enum NOTAMEncoding {
    DIGITAL(1, "DIGITAL"),
    MIXED(2, "MIXED"),
    ANNOTATION(3, "ANNOTATION");
    
    private int id;
    private final String encoding;
    private NOTAMEncoding(int id, String encoding) {
        this.id = id;
        this.encoding = encoding;
    }

    public static NOTAMEncoding getByKey(Integer id) {
        if(id==null) {
            return MIXED;
        }
        for (NOTAMEncoding one : NOTAMEncoding.values()) {
            if (id == one.id) {
                return one;
            }
        }
        return MIXED;
    }

    public static NOTAMEncoding getByEncoding(String encoding) {
        for (NOTAMEncoding one : NOTAMEncoding.values()) {
            if (encoding.equals(one.encoding)) {
                return one;
            }
        }
        System.out.println("NOTAMEncoding: returning default MIXED encoding, no suitable encoding provided");    
        return MIXED;
    }

    public int getKey() {
        return id;
    }

    public String getEncoding() {
        return encoding;
    }

    @Override
    public String toString() {
        return encoding;
    }
}
