/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author irena.bucci
 */
public class Utils {
    /**
     * convert string 2010-07-10T15:25:40 to XMLGregorianCalendar
     */  
    public static XMLGregorianCalendar asXMLGregorianCalendar(String dateStr) {    //convert String to date
        XMLGregorianCalendar result = null;
       
        try {
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateStr);
            System.out.println("input: " + dateStr);    
            System.out.println("result: " + result);            
        } catch (Exception ex) {
            System.out.println("Exception in String to Gregorian Calendar Conversion.");
            ex.printStackTrace();
        }
    return result;
    }
}
