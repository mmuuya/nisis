/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author irena.bucci
 */
public class Notam {
    private NotamBody notamBody;
  
    public Notam(NotamBody body) {
        this.notamBody = body;
    }

   

    public NotamBody getBody() {
        return notamBody;
    }

    public void setBody(NotamBody body) {
        this.notamBody = body;
    }

    @Override
    public String toString() {
        return "Notam{" + "notamBody=" + notamBody + '}';
    }
    
}
