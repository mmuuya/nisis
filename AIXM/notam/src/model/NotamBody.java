/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author irena.bucci
 */
public class NotamBody {
    private String series;
    private String number;
    private String year;
    private String type;
    private String issued;
    private String referredSeries;
    private String referredNumber;
    private String referredYear;
    private String affectedFIR;
    private String selectionCode;
    private String traffic;
    private String purpose;
    private String scope;
    private String minimumFL;
    private String maximumFL;
    private String coordinates;
    private String radius;
    private String location;
    private String effectiveStart;
    private String effectiveEnd;
    private String schedule;
    private String text;
    private String lowerLimit;
    private String upperLimit;
    private String translation;
    
    private PublisherNOF publisherNOF;

    public NotamBody() {
    }

    public NotamBody(String series, String number, String year, String type, String issued, String referredSeries, String referredNumber, String referredYear, String affectedFIR, String selectionCode, String traffic, String purpose, String scope, String minimumFL, String maximumFL, String coordinates, String radius, String location, String effectiveStart, String effectiveEnd, String schedule, String text, String lowerLimit, String upperLimit, String translation) {
        this.series = series;
        this.number = number;
        this.year = year;
        this.type = type;
        this.issued = issued;
        this.referredSeries = referredSeries;
        this.referredNumber = referredNumber;
        this.referredYear = referredYear;
        this.affectedFIR = affectedFIR;
        this.selectionCode = selectionCode;
        this.traffic = traffic;
        this.purpose = purpose;
        this.scope = scope;
        this.minimumFL = minimumFL;
        this.maximumFL = maximumFL;
        this.coordinates = coordinates;
        this.radius = radius;
        this.location = location;
        this.effectiveStart = effectiveStart;
        this.effectiveEnd = effectiveEnd;
        this.schedule = schedule;
        this.text = text;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;        
        this.translation = translation;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getReferredSeries() {
        return referredSeries;
    }

    public void setReferredSeries(String referredSeries) {
        this.referredSeries = referredSeries;
    }

    public String getReferredNumber() {
        return referredNumber;
    }

    public void setReferredNumber(String referredNumber) {
        this.referredNumber = referredNumber;
    }

    public String getReferredYear() {
        return referredYear;
    }

    public void setReferredYear(String referredYear) {
        this.referredYear = referredYear;
    }

    public String getAffectedFIR() {
        return affectedFIR;
    }

    public void setAffectedFIR(String affectedFIR) {
        this.affectedFIR = affectedFIR;
    }

    public String getSelectionCode() {
        return selectionCode;
    }

    public void setSelectionCode(String selectionCode) {
        this.selectionCode = selectionCode;
    }

    public String getTraffic() {
        return traffic;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getMinimumFL() {
        return minimumFL;
    }

    public void setMinimumFL(String minimumFL) {
        this.minimumFL = minimumFL;
    }

    public String getMaximumFL() {
        return maximumFL;
    }

    public void setMaximumFL(String maximumFL) {
        this.maximumFL = maximumFL;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEffectiveStart() {
        return effectiveStart;
    }

    public void setEffectiveStart(String effectiveStart) {
        this.effectiveStart = effectiveStart;
    }

    public String getEffectiveEnd() {
        return effectiveEnd;
    }

    public void setEffectiveEnd(String effectiveEnd) {
        this.effectiveEnd = effectiveEnd;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(String upperLimit) {
        this.upperLimit = upperLimit;
    }

   

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public PublisherNOF getPublisherNOF() {
        return publisherNOF;
    }

    public void setPublisherNOF(PublisherNOF publisherNOF) {
        this.publisherNOF = publisherNOF;
    }

    @Override
    public String toString() {
        return "NotamBody{" + "series=" + series + ", number=" + number + ", year=" + year + ", type=" + type + ", issued=" + issued + ", referredSeries=" + referredSeries + ", referredNumber=" + referredNumber + ", referredYear=" + referredYear + ", affectedFIR=" + affectedFIR + ", selectionCode=" + selectionCode + ", traffic=" + traffic + ", purpose=" + purpose + ", scope=" + scope + ", minimumFL=" + minimumFL + ", maximumFL=" + maximumFL + ", coordinates=" + coordinates + ", radius=" + radius + ", location=" + location + ", effectiveStart=" + effectiveStart + ", effectiveEnd=" + effectiveEnd + ", schedule=" + schedule + ", text=" + text + ", lowerLimit=" + lowerLimit + ", upperLimit=" + upperLimit + ", translation=" + translation + ", publisherNOF=" + publisherNOF + '}';
    }

    
  
    
}
