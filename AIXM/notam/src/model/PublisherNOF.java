/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author irena.bucci
 */
public class PublisherNOF {

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    private String link;
    private String title;

    public PublisherNOF(String link, String title) {
        this.link = link;
        this.title = title;
    }

    @Override
    public String toString() {
        return "PublisherNOF{" + "link=" + link + ", title=" + title + '}';
    }
    
    
}
