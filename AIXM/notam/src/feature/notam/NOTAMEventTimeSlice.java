package feature.notam;

import aero.aixm.v510.DateTimeType;
import aero.aixm.v510.TextDesignatorType;
import aero.aixm.v510.TextNameType;
import aero.aixm.v510.event.CodeEventEncodingType;
import aero.aixm.v510.event.EventTimeSliceType;
import constants.NOTAMEncoding;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import net.opengis.gml.v321.AbstractTimePrimitiveType;
import net.opengis.gml.v321.TimeInstantType;
import net.opengis.gml.v321.TimePositionType;
import net.opengis.gml.v321.TimePrimitivePropertyType;
import net.opengis.gml.v321.TimePeriodType;
import utils.Utils;
/**
 * An class representing a NOTAM Time Slice. 
 * see Digital NOTAM Event Specification 1.0.doc
 * 
 * NOTAM events have special event properties:
 * •	name = an optional title or designator by which the event may be referred in aeronautical operations;
 * •	encoding = an indication of the extent by which the event information is provided as digital data versus free text notes and which can take the following values:
 *       o	DIGITAL = the information is digitally structured to the maximum possible extent allowed by the AIXM model;
 *       o	MIXED = the information is partially digitally structured. Some information is provided as a text Note, although the model has the capability to support the digital encoding of that piece of information;
 *       o	ANNOTATION = the information is provided as a free text Note associated with the feature and eventually the property concerned.
 * •	scenario = the identifier of the scenario that was followed for the data encoding, including the version of the specification;
 * •	revision = the most recent date and time when the information encoded in the Event was updated.
 *
 */
public class NOTAMEventTimeSlice extends EventTimeSliceType {
    private final aero.aixm.v510.event.ObjectFactory aixmEventObjectFactory;
    private final net.opengis.gml.v321.ObjectFactory gmlObjectFactory;
    public NOTAMEventTimeSlice() {
            gmlObjectFactory =  new net.opengis.gml.v321.ObjectFactory();
            aixmEventObjectFactory =  new aero.aixm.v510.event.ObjectFactory();
    }
       
    public void setNOTAMEventNameProperty(String name) {           
           //event:name
           TextNameType nameText = new TextNameType();
           //nameText.setValue("TEMPORARY RESTRICTED AREA NORTH OF SJAELLANDS ODDE");
           nameText.setValue(name);
           //add name to time slice         
           this.getRest().add(aixmEventObjectFactory.createEventTimeSliceTypeName(nameText));  
    }
    
    public void setNOTAMEventEncodingProperty(String encoding) {           
            //event:encoding
           CodeEventEncodingType encodingCode = new CodeEventEncodingType();   
           //make sure that encoding is one of the pre-defined values //DIGITAL//MIXED//ANNOTATION   
           encodingCode.setValue(NOTAMEncoding.getByEncoding(encoding).getEncoding());          
           this.getRest().add(aixmEventObjectFactory.createEventTimeSliceTypeEncoding(encodingCode));  
    }
    
    public void setNOTAMEventScenarioProperty(String scenario) {
           //like "SAA.NEW.1.0" - indicates that the ending was done according to the Ad-hoc special activity area - creation scenario as defined in the Digital NOTAM Event Specification version 1.0.
           //event:scenario
           TextDesignatorType scenarioText = new TextDesignatorType();
           scenarioText.setValue(scenario);  
           this.getRest().add(aixmEventObjectFactory.createEventTimeSliceTypeScenario(scenarioText));             
    }
    
    public void setNOTAMEventRevisionProperty(String revisionDate) {           
          //event:revision
           DateTimeType revision = new DateTimeType();
           XMLGregorianCalendar dateG = Utils.asXMLGregorianCalendar(revisionDate);
           revision.setValue(dateG);     
           this.getRest().add(aixmEventObjectFactory.createEventTimeSliceTypeRevision(revision)); 
    }
    //irena - TODO: need to generate gml:id
    public void setNOTAMEventValidTime(String validTimeStr) {    
            TimeInstantType timeInstant = new TimeInstantType();
            TimePositionType timePos = new TimePositionType();
            timePos.getValue().add(validTimeStr);
            timeInstant.setTimePosition(timePos);
            JAXBElement<? extends AbstractTimePrimitiveType> gmlTimePrimitiveJE = gmlObjectFactory.createTimeInstant(timeInstant);
            
            TimePrimitivePropertyType validtime = gmlObjectFactory.createTimePrimitivePropertyType(); 
            validtime.setAbstractTimePrimitive(gmlTimePrimitiveJE);
            this.setValidTime(validtime);           
    }
    //irena - TODO: need to generate gml:id
    public void setNOTAMFeatureLifetime(String start, String end) {           
            TimePeriodType timePeriod = gmlObjectFactory.createTimePeriodType();            
            TimePositionType timePos1 = gmlObjectFactory.createTimePositionType(); 
            TimePositionType timePos2 = gmlObjectFactory.createTimePositionType(); 
            timePos1.getValue().add(start);
            timePeriod.setBeginPosition(timePos1);
           
            timePos2.getValue().add(end);
            timePeriod.setEndPosition(timePos2);
           
            JAXBElement<? extends AbstractTimePrimitiveType> timePeriodJE= gmlObjectFactory.createTimePeriod(timePeriod);
            TimePrimitivePropertyType lifeTime= gmlObjectFactory.createTimePrimitivePropertyType();
                  
            lifeTime.setAbstractTimePrimitive(timePeriodJE);
            this.setFeatureLifetime(lifeTime);           
    }
   
}
