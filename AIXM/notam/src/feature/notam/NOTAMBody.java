package feature.notam;

import aero.aixm.v510.CodeUpperAlphaType;
import aero.aixm.v510.DateTimeType;
import aero.aixm.v510.DateYearType;
import aero.aixm.v510.NoNumberType;
import aero.aixm.v510.UnitPropertyType;
import aero.aixm.v510.event.CodeNOTAMType;
import aero.aixm.v510.event.NOTAMType;
import aero.aixm.v510.event.TextNOTAMType;
import javax.xml.datatype.XMLGregorianCalendar;
import model.NotamBody;
import model.PublisherNOF;
import utils.Utils;


/**
 * An class representing a NOTAM Body, ie event:NOTAM. 
 * see Digital NOTAM Event Specification 1.0.doc
 * 
 * NOTAM events have special event properties:
 * •	name = an optional title or designator by which the event may be referred in aeronautical operations;
 * •	encoding = an indication of the extent by which the event information is provided as digital data versus free text notes and which can take the following values:
 *       o	DIGITAL = the information is digitally structured to the maximum possible extent allowed by the AIXM model;
 *       o	MIXED = the information is partially digitally structured. Some information is provided as a text Note, although the model has the capability to support the digital encoding of that piece of information;
 *       o	ANNOTATION = the information is provided as a free text Note associated with the feature and eventually the property concerned.
 * •	scenario = the identifier of the scenario that was followed for the data encoding, including the version of the specification;
 * •	revision = the most recent date and time when the information encoded in the Event was updated.
 *
 */
public class NOTAMBody extends NOTAMType {
    private final aero.aixm.v510.event.ObjectFactory aixmEventObjectFactory;
  
    
    public NOTAMBody() {
        aixmEventObjectFactory =  new aero.aixm.v510.event.ObjectFactory();
    }
    
    /**
     * The identifier of the NOTAM Series, if applicable.
     */  
    //The identifier of the NOTAM Series - should it be always C? what else - need to revisit
    //• A-series: Aerodrome movement area NOTAMs
    //• B-series: Facilities and Services
    //• C-series: Airspace
    //• D-series: Obstructions
    //• F-series: Fuel (current military)
    //• G-series: GNSS
    //• I-series: International NOTAMs issued by US
    //• L-series: Local (current military)
    //• M-series: Flight Safety (current military)
    //• N-series: NavAids
    //• O-series: Other aeronautical information
    //• P-series: Procedural
    //• U-series: Unverified
    //• V-series: Procedural (current military)
    //• W-series: DAFIF (current military)
    //• X-series: DoD NOTAM Office
    //• SW-series: Snowtams
    //• VA-series: Ashtams
    public void setNOTAMBodySeries(String series) {           
           //event:series
           CodeUpperAlphaType seriesCode = new CodeUpperAlphaType();
           seriesCode.setValue(series);          
           this.setSeries(aixmEventObjectFactory.createNOTAMTypeSeries(seriesCode));
    }
    /**
     * The number of the NOTAM.
     */   
    public void setNOTAMBodyNumber(String number) {           
           //event:number
           NoNumberType num = new NoNumberType();
           Long lng = Long.parseLong(number);
           num.setValue(lng);            
           this.setNumber(aixmEventObjectFactory.createNOTAMTypeNumber(num));
    }
    /**
     * The year associated with the NOTAM number.
     */   
    public void setNOTAMBodyYear(String year) {           
           //event:year
           DateYearType dateyear = new DateYearType();
           if (year == null || year.length()!=4) {
              //invalid year
              return;
           }           
           dateyear.setValue(year); 
           this.setYear(aixmEventObjectFactory.createNOTAMTypeYear(dateyear));          
    }
    /**
     * The type of NOTAM message (new, cancel or replace).
     */   
    public void setNOTAMBodyType(String notamType) {           
           //event:type
           CodeNOTAMType typeCode = new CodeNOTAMType();   
           //make sure that type is one of the pre-defined values //NEW//CANCEL///REPLACE   
           typeCode.setValue(constants.NOTAMType.getByType(notamType).getKey());  
           this.setType(aixmEventObjectFactory.createNOTAMTypeType(typeCode));  
    }
    /**
     * The date and time when the NOTAM was issued.
     */   
    public void setNOTAMBodyIssued(String dateIssued) {           
           //event:issued
           DateTimeType issuedDT = new DateTimeType();
           //convert flat string to gregorian format
           XMLGregorianCalendar dateG = Utils.asXMLGregorianCalendar(dateIssued);           
           issuedDT.setValue(dateG); 
           this.setIssued(aixmEventObjectFactory.createNOTAMTypeIssued(issuedDT));  
    }
    /**
     * The identifier of the series of the referred NOTAM, in case of NOTAM type R or C.
     */   
    public void setNOTAMBodyReferredNOTAMSeries(String referredNOTAMid) {           
           //event:referredSeries
           CodeUpperAlphaType refSeries = new CodeUpperAlphaType();
           refSeries.setValue(referredNOTAMid); 
           this.setReferredSeries(aixmEventObjectFactory.createNOTAMTypeReferredSeries(refSeries));            
    }
    /**
     * The number of the referred NOTAM, in case of NOTAM type R or C.
     */   
    public void setNOTAMBodyReferredNOTAMNumber(String referredNOTAMNumber) {           
           //event:referredNumber
           NoNumberType referredNo = new NoNumberType();
           referredNo.setValue(Long.parseLong(referredNOTAMNumber)); 
           this.setReferredNumber(aixmEventObjectFactory.createNOTAMTypeReferredNumber(referredNo));    
    }
    /**
     * The year associated with the referred NOTAM, in case of NOTAM type R or C.
     */   
    public void setNOTAMBodyReferredNOTAMYear(String referredNOTAMYear) {           
           //event:referredNumber
           DateYearType refYear = new DateYearType();
           refYear.setValue(referredNOTAMYear); 
           this.setReferredYear(aixmEventObjectFactory.createNOTAMTypeReferredYear(refYear));    
    }
    
    /**
     * The ICAO location indicator of affected FIR or, if applicable to more than one FIR within a State, the first two letters of the ICAO location indicator of a State plus "XX"
     */   
    public void setNOTAMBodyAffectedFIR(String affectedFIR) {           
           //event:affectedFIR
           TextNOTAMType fir = new TextNOTAMType();
           fir.setValue(affectedFIR); 
           this.setAffectedFIR(aixmEventObjectFactory.createNOTAMTypeAffectedFIR(fir));             
    }
    
     /**
     * The 5 letter Q code of the NOTAM.
     */   
    public void setNOTAMBodySelectionCode(String code) {           
           //event:referredNumber
           TextNOTAMType selCode = new TextNOTAMType();
           selCode.setValue(code); 
           this.setSelectionCode(aixmEventObjectFactory.createNOTAMTypeSelectionCode(selCode));     
    }
    
    /**
     * Type of traffic affected by NOTAM (Q line).
     */   
    public void setNOTAMBodyTraffic(String notamTraffic) {           
           //event:traffic
           TextNOTAMType trafc = new TextNOTAMType();
           trafc.setValue(notamTraffic); 
           this.setTraffic(aixmEventObjectFactory.createNOTAMTypeTraffic(trafc));   
    }
    /**
     * Purpose of the NOTAM (Q line).
     */   
    public void setNOTAMBodyPurpose(String notamPurpose) {           
           //event:purpose
           TextNOTAMType purps = new TextNOTAMType();
           purps.setValue(notamPurpose); 
           this.setPurpose(aixmEventObjectFactory.createNOTAMTypePurpose(purps));
    }
    /**
     * The scope of the NOTAM (Q line).
     */   
    public void setNOTAMBodyScope(String notamScope) {           
           //event:scope
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(notamScope); 
           this.setPurpose(aixmEventObjectFactory.createNOTAMTypeScope(txt));
    }
    /**
     * The minimum flight level affected by the NOTAM (Q line).
     */   
    public void setNOTAMBodyMinimumFL(String notamMinimumFL) {           
           //event:minimumFL
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(notamMinimumFL); 
           this.setMinimumFL(aixmEventObjectFactory.createNOTAMTypeMinimumFL(txt));
    }
    /**
     * The maximum flight level affected by the NOTAM (Q line).
     */   
    public void setNOTAMBodyMaximumFL(String notamMaximumFL) {           
           //event:maximumFL
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(notamMaximumFL); 
           this.setMaximumFL(aixmEventObjectFactory.createNOTAMTypeMaximumFL(txt));
    }
    /**
     * A lat/long position that links the NOTAM to a geographical position - 11 characters, rounded up or down to the nearest minute (Q line).
     */   
    public void setNOTAMBodyCoordinates(String notamCoordinates) {           
           //event:coordinates
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(notamCoordinates); 
           this.setCoordinates(aixmEventObjectFactory.createNOTAMTypeCoordinates(txt));
    }
    /**
     * An estimated radius of influence for the NOTAM in Nautical Miles, around the position indicated by the "coordinates" attribute (Q line).
     */   
    public void setNOTAMBodyRadius(String notamRadius) {           
           //event:radius
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(notamRadius); 
           this.setRadius(aixmEventObjectFactory.createNOTAMTypeRadius(txt));
    }
    /**
     * The NOTAM location (item A).
     */   
    public void setNOTAMBodyLocation(String notamLocation) {           
           //event:location
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(notamLocation); 
           this.setLocation(aixmEventObjectFactory.createNOTAMTypeLocation(txt));
    }
    /**
     * The start day and time when the NOTAM becomes effective (item B).
     */   
    public void setNOTAMBodyEffectiveStart(String effectiveStart) {           
           //event:effectiveStart
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(effectiveStart); 
           this.setEffectiveStart(aixmEventObjectFactory.createNOTAMTypeEffectiveStart(txt));
    }
    /**
     * The end day and time when the NOTAM stops being effective (item C).
     */   
    public void setNOTAMBodyEffectiveEnd(String effectiveEnd) {           
           //event:effectiveEnd
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(effectiveEnd); 
           this.setEffectiveEnd(aixmEventObjectFactory.createNOTAMTypeEffectiveEnd(txt));
    }
    /**
     * The schedule of the NOTAM (item D).
     */   
    public void setNOTAMBodySchedule(String schedule) {           
           //event:schedule
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(schedule); 
           this.setSchedule(aixmEventObjectFactory.createNOTAMTypeSchedule(txt));
    }
    /**
     * The NOTAM text (item E).
     */   
    public void setNOTAMBodyText(String text) {           
           //event:text
           TextNOTAMType textNotam = new TextNOTAMType();
           textNotam.setValue(text);
           this.setText(aixmEventObjectFactory.createNOTAMTypeText(textNotam));  
    }
    /**
     * The lower limit of the NOTAM (item F).
     */   
    public void setNOTAMBodyLowerLimit(String lowerLimit) {           
           //event:lowerLimit
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(lowerLimit);
           this.setLowerLimit(aixmEventObjectFactory.createNOTAMTypeLowerLimit(txt));
    }
    /**
     * The upper limit of the NOTAM (item G).
     */   
    public void setNOTAMBodyUpperLimit(String upperLimit) {           
           //event:upperLimit
           TextNOTAMType txt = new TextNOTAMType();
           txt.setValue(upperLimit);
           this.setUpperLimit(aixmEventObjectFactory.createNOTAMTypeUpperLimit(txt));
    }
    /**
     * The NOTAM Office (Unit) that has issued the NOTAM.
     */   
    public void setNOTAMBodyPublisherNOF(String publisherNOFLink, String publisherNOFTitle) {           
           //event:publisherNOF
           UnitPropertyType unt = new UnitPropertyType();
           unt.setHref(publisherNOFLink);
           unt.setTitleAttribute(publisherNOFTitle);
           this.setPublisherNOF(aixmEventObjectFactory.createAISPublicationTypePublisher(unt));
    }
    
    /**
     * Create Notam based on the POJO
     */   
    public void setNOTAMBodyFromModel(NotamBody bod) {   
        if (bod == null) {
            return;
        }
        this.setNOTAMBodySeries(bod.getSeries());
        this.setNOTAMBodyNumber(bod.getNumber());
        this.setNOTAMBodyYear(bod.getYear());
        this.setNOTAMBodyIssued(bod.getIssued());
        this.setNOTAMBodyReferredNOTAMSeries(bod.getReferredSeries());
        this.setNOTAMBodyReferredNOTAMNumber(bod.getReferredNumber());
        this.setNOTAMBodyReferredNOTAMYear(bod.getReferredYear());
        this.setNOTAMBodyAffectedFIR(bod.getAffectedFIR());
        this.setNOTAMBodySelectionCode(bod.getSelectionCode());
        this.setNOTAMBodyTraffic(bod.getTraffic());
        this.setNOTAMBodyPurpose(bod.getPurpose());
        this.setNOTAMBodyScope(bod.getScope());
        this.setNOTAMBodyMinimumFL(bod.getMinimumFL());
        this.setNOTAMBodyMaximumFL(bod.getMaximumFL());
        this.setNOTAMBodyCoordinates(bod.getCoordinates());
        this.setNOTAMBodyRadius(bod.getRadius());
        this.setNOTAMBodyLocation(bod.getLocation());
        this.setNOTAMBodyEffectiveStart(bod.getEffectiveStart());
        this.setNOTAMBodyEffectiveEnd(bod.getEffectiveEnd());
        this.setNOTAMBodySchedule(bod.getSchedule());
        this.setNOTAMBodyText(bod.getText());
        this.setNOTAMBodyLowerLimit(bod.getLowerLimit());
        this.setNOTAMBodyUpperLimit(bod.getUpperLimit());
        PublisherNOF publisher = bod.getPublisherNOF();
        if (publisher !=null) {
            this.setNOTAMBodyPublisherNOF(bod.getPublisherNOF().getLink(), bod.getPublisherNOF().getTitle());
        }
    }
    
    
}
