/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import aero.aixm.v510.event.EventTimeSlicePropertyType;
import aero.aixm.v510.event.EventType;
import aero.aixm.v510.event.NOTAMPropertyType;
import aero.aixm.v510.message.AIXMBasicMessageType;
import aero.aixm.v510.message.BasicMessageMemberAIXMPropertyType;
import aero.aixm.v510.message.ObjectFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import feature.notam.NOTAMBody;
import feature.notam.NOTAMEventTimeSlice;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;
import javax.xml.bind.JAXBException;
import model.Notam;
import net.opengis.gml.v321.CodeWithAuthorityType;


/**
 *
 * @author irena.bucci
 */
public class Go {
    private static final String OUTPUT_FILE = "aixmNotam.xml";
    public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
        String json = args[0];
        Notam inputNotam = convertJsonArgumentsToPOJO(json);
              
        aero.aixm.v510.message.ObjectFactory aixmMsgObjectFactory = new ObjectFactory();
        aero.aixm.v510.event.ObjectFactory aixmEventObjectFactory =  new aero.aixm.v510.event.ObjectFactory();
        AIXMBasicMessageType aixmBasicMessage = aixmMsgObjectFactory.createAIXMBasicMessageType();
        BasicMessageMemberAIXMPropertyType messageMember = new BasicMessageMemberAIXMPropertyType(); 
         
        //Build gml identifier that event will reference
        //<gml:identifier codeSpace="urn:uuid:">f50faa4a-3037-45ea-a69d-5e982d74991a</gml:identifier>
        // gml:id
        //generate UUID 
         
        EventType event = new EventType();     
        CodeWithAuthorityType eventIdentifier = new CodeWithAuthorityType();
        UUID uuid = UUID.randomUUID();
        eventIdentifier.setValue(uuid.toString());
        eventIdentifier.setCodeSpace("urn:uuid:");         
        event.setIdentifier(eventIdentifier);
        event.setId(eventIdentifier.getValue());
                 
        EventTimeSlicePropertyType timeSlice = new EventTimeSlicePropertyType();
        NOTAMEventTimeSlice eventTimeSlice = new NOTAMEventTimeSlice();
        //add eventTimeSlice to event
        timeSlice.setEventTimeSlice(eventTimeSlice);
        //add timeSlide to Event
        event.getTimeSlice().add(timeSlice);
                 
        eventTimeSlice.setId("e01-1"); //irena - need to create local identifier
          
        //The incoming json will have several groups:
        //NOTAM body {}
        //
        //this needs work
        //String valid time instant
        String validTimeStr="2009-11-07T08:00:00";
        String lifeTimeEndPosition="2010-07-10T15:25";
        String lifeTimeBeginPosition="2010-07-10T07:00";
        eventTimeSlice.setNOTAMEventValidTime(validTimeStr);        
        eventTimeSlice.setInterpretation("PERMDELTA"); //or BASELINE or SNAPSHOT
        eventTimeSlice.setSequenceNumber(2L);
        eventTimeSlice.setNOTAMFeatureLifetime(lifeTimeBeginPosition, lifeTimeEndPosition); //begin and end position
          
        //----------------------------------NOTAM event properties ------------------------------
        //this alsp needs work
        //set NOTAM events properties: name, encoding, scenario, revision 
        eventTimeSlice.setNOTAMEventNameProperty("TEMPORARY RESTRICTED AREA NORTH OF SJAELLANDS ODDE");
        eventTimeSlice.setNOTAMEventEncodingProperty("DIGITAL");
        eventTimeSlice.setNOTAMEventScenarioProperty("SAA.NEW.1.0");
        eventTimeSlice.setNOTAMEventRevisionProperty("2010-07-10T15:25:00");
      
        //----------------------------------event: NOTAM body ------------------------------
        NOTAMPropertyType notam = new NOTAMPropertyType();
        NOTAMBody notamBody = new NOTAMBody();        
        /*notamBody.setNOTAMBodySeries("C");
        notamBody.setNOTAMBodyNumber("5198");
        notamBody.setNOTAMBodyYear("2010");
        notamBody.setNOTAMBodyType("C");
        notamBody.setNOTAMBodyText("AD ENSO CLOSED FOR ALL TFC");                   
        notam.setNOTAM(notamBody);
                */
        //irena - can I put the json model right into NOTAMBody class?
        notamBody.setNOTAMBodyFromModel(inputNotam.getBody());
        notam.setNOTAM(notamBody);
        //----------------------------------event: NOTAM body ------------------------------
        
        //add notam to time slice
        //eventTimeSlice.getRest().add(wrap("event:textNOTAM", notam));  
        eventTimeSlice.getRest().add(aixmEventObjectFactory.createEventTimeSliceTypeTextNOTAM(notam));
                   
        //messageMember.setAbstractAIXMFeature(wrap("event:Event", event));
        messageMember.setAbstractAIXMFeature(aixmEventObjectFactory.createEvent(event));
         
        //add it to the list
        aixmBasicMessage.getHasMember().add(messageMember);
               
        //----------------------------------done building NOTAM-----------------------------       
        System.out.println("Starting ..."); 
        File file = new File(OUTPUT_FILE);
        FileOutputStream fos = null;        
        try {
                        
            //XML Binding code using JAXB          
            AIXMUnmarshallerMarshaller aixmmarshaller = new AIXMUnmarshallerMarshaller();
            //os = aixmmarshaller.marshal( wrap("message:AIXMBasicMessage", aixmBasicMessage));
            ByteArrayOutputStream os = aixmmarshaller.marshal( aixmMsgObjectFactory.createAIXMBasicMessage(aixmBasicMessage));
            
            fos = new FileOutputStream(file);
            // Writes bytes from the specified byte array to this file output stream
            os.writeTo(fos);

            System.out.println(os.toString());  
       
        } catch (JAXBException ex) {
            //Logger.getLogger(JAXBXmlBindExample.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.toString());           
        } 
        catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        }
        finally {
			// close the streams using close method
			try {
				if (fos != null) {
					fos.close();
				}
			}
			catch (IOException ioe) {
				System.out.println("Error while closing stream: " + ioe);
			}

		}
        
    }
 
   
  //  public static <T> JAXBElement<T> wrap(String qName, final T object) {        
   //     return new JAXBElement<T>(new QName("uri",qName), (Class<T>) object.getClass(), object);
  //  }
    
    
    
    public static Notam convertJsonArgumentsToPOJO(String json) {        
        Gson gson = new GsonBuilder().create();        
        System.out.println(json);
        Notam result = gson.fromJson(json, Notam.class);
        System.out.println(result);
        return result;
    }
    
}
