/**
 
 */
package notam;

import java.util.HashMap;
import java.util.Map;

import org.dom4j.Namespace;
import org.dom4j.QName;

/**
 * Commonly used constants in AIXM.
 * 
 * @author SCHASE
 *
 */
public class AixmConstants {
    
    public static final String AIXM_URI = "http://www.aixm.aero/schema/5.1";
    public static final String GML_URI = "http://www.opengis.net/gml";
    public static final String XLINK_URI = "http://www.w3.org/1999/xlink";
    public static final String AIXM_PREFIX = "aixm";
    public static final String GML_PREFIX = "gml";
    public static final String XLINK_PREFIX = "xlink";
    public static final Namespace AIXM_NAMESPACE = new Namespace(AIXM_PREFIX, AIXM_URI);
    public static final Namespace GML_NAMESPACE = new Namespace(GML_PREFIX, GML_URI);
    public static final Namespace XLINK_NAMESPACE = new Namespace(XLINK_PREFIX, XLINK_URI);
    
    private static Map<String, String> URI_MAP = new HashMap<String, String>();
    
    public static final QName TIMESLICE_QNAME = QName.get("timeSlice", AIXM_NAMESPACE);

    static {
        URI_MAP.put(AIXM_PREFIX, AIXM_URI);
        URI_MAP.put(GML_PREFIX, GML_URI);
        URI_MAP.put(XLINK_PREFIX, XLINK_URI);
    }
    
    public static Map<String, String> getUriMap() {
        return URI_MAP;
    }
    
    public static String getGlobalGmlIdXPath(String gmlId) {
        StringBuilder sb = new StringBuilder("//*[@gml:id='");
        sb.append(gmlId);
        sb.append("']");
        return sb.toString();
    }
}
