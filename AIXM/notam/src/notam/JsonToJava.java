/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Notam;
import model.NotamBody;
import model.PublisherNOF;

/**
 *
 * @author irena.bucci
 */
public class JsonToJava {
/*
    public static void main(String[] args) throws IOException {
        
        PublisherNOF publisher = new PublisherNOF("6bdb97aa-3f04-41bc-ac10-64cede76b9ba", "title" );
        NotamBody bod = new NotamBody();
        bod.setAffectedFIR("EADD");
        bod.setCoordinates("3609N00630E");
        bod.setEffectiveEnd("1007101526");
        bod.setEffectiveStart("1007101525");
        bod.setIssued("2010-07-10T15:25:40");
        bod.setLocation("EADL");
        bod.setLowerLimit("FL 110");
        bod.setMaximumFL("195");
        bod.setMinimumFL("235");
        bod.setNumber("2");
        bod.setPublisherNOF(publisher);
        bod.setPurpose("BO");
        bod.setRadius("050");
        bod.setReferredNumber("1");
        bod.setReferredSeries("123");
        bod.setReferredYear("2010");
        bod.setSchedule("MON");
        bod.setScope("W");
        bod.setSelectionCode("QRRCN");
        bod.setSeries("C");
        bod.setText("TRA EAR23 ACTIVATION CANCELLED");
        bod.setTraffic("IV");
        bod.setTranslation("some translation");
        bod.setType("N");
        bod.setUpperLimit("FL 340");
        bod.setYear("2010");
        Notam note = new Notam(bod);
        
        String json = JsonUtils.getString(note)  ;
         System.out.println(json);  
        try(
             
                
            Reader reader = new InputStreamReader(JsonToJava.class.getResourceAsStream("/integration/irenaJSON_1.txt"), "UTF-8")){
            Gson gson = new GsonBuilder().create();
            Notam p = gson.fromJson(reader, Notam.class);
            System.out.println(p);
        }
        catch (Exception ex) {
             System.out.println(ex.toString());
            
        }
    }
    */
}