/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import aero.aixm.v510.CodeUpperAlphaType;
import aero.aixm.v510.DateTimeType;
import aero.aixm.v510.FeatureTimeSliceMetadataPropertyType;
import aero.aixm.v510.TextDesignatorType;
import aero.aixm.v510.TextNameType;
import aero.aixm.v510.event.CodeEventEncodingType;
import aero.aixm.v510.event.EventTimeSlicePropertyType;
import aero.aixm.v510.event.EventTimeSliceType;
import aero.aixm.v510.event.EventType;
import aero.aixm.v510.event.NOTAMPropertyType;
import aero.aixm.v510.event.NOTAMType;
import aero.aixm.v510.event.TextNOTAMType;
import aero.aixm.v510.message.AIXMBasicMessageType;
import aero.aixm.v510.message.BasicMessageMemberAIXMPropertyType;
import aero.aixm.v510.message.ObjectFactory;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.UUID;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import net.opengis.gml.v321.CodeWithAuthorityType;


/**
 *
 * @author irena.bucci
 */
public class Notam_Orig {

    
    public static void main(String[] args) throws UnsupportedEncodingException {
         AIXMBasicMessageType aixmBasicMessage = new ObjectFactory().createAIXMBasicMessageType();
         BasicMessageMemberAIXMPropertyType messageMember = new BasicMessageMemberAIXMPropertyType(); 
         
         //Build gml identifier that event will reference
         //<gml:identifier codeSpace="urn:uuid:">f50faa4a-3037-45ea-a69d-5e982d74991a</gml:identifier>
         // gml:id
         //generate UUID 
         
         UUID uuid = UUID.randomUUID();
         CodeWithAuthorityType eventIdentifier = new CodeWithAuthorityType();
         eventIdentifier.setValue(uuid.toString());
         eventIdentifier.setCodeSpace("urn:uuid:");
         
         //<event:Event gml:id="uuid.f50faa4a-3037-45ea-a69d-5e982d74991a">
         
                 
         EventType event = new EventType();     
         event.setIdentifier(eventIdentifier);
         event.setId(eventIdentifier.getValue());
         EventTimeSlicePropertyType timeSlice = new EventTimeSlicePropertyType();
         EventTimeSliceType eventTimeSlice = new EventTimeSliceType();
         
          FeatureTimeSliceMetadataPropertyType timeslicemetadata = new  FeatureTimeSliceMetadataPropertyType();
          eventTimeSlice.setId("e01-1"); //irena - need to create local identifier
          
          //timeslicemetadata.
          
          eventTimeSlice.setValidTime(null);
          eventTimeSlice.setInterpretation("PERMDELTA"); //or BASELINE or SNAPSHOT
          eventTimeSlice.setSequenceNumber(2L);
          eventTimeSlice.setFeatureLifetime(null);
          
          //filling out the EventPropertyGroup:
          //name - A title or designator by which the event is referred to in aeronautical operations.
          //encoding - An indication of the extent by which the event information is provided as digital data versus free text notes.
          //scenario - The identifier of the scenario that was followed for the data encoding, including the version of the specification
          //revision - The most recent date and time when the information encoded in the Event was updated.
          //textNOTAM - text of the NOTAM
          //causeEvent - The Event that has triggered the current Event.
          //theNote
          //documentAIS
          
          
          //timeslicemetadata.
          //eventTimeSlice.
          //eventTimeSlice..
          //timeslicemetadata.
          //ID00001
         // timeslicemetadata.
                //  eventIdentifier.
         //eventTimeSlice.setTimeSliceMetadata(null);
         /* MDMetadataType md = new MDMetadataType();
          //md.setMetadataStandardName("TEMPORARY RESTRICTED AREA NORTH OF SJAELLANDS ODDE");
          MDCharacterSetCodePropertyType encodingMD = new MDCharacterSetCodePropertyType();
          CodeListValueType endcoding = new CodeListValueType();
          encodingMD.setMDCharacterSetCode(endcoding);
          md.setCharacterSet(null);
           FeatureMetadataPropertyType ftMetadata =  new FeatureMetadataPropertyType();
           ftMetadata.setMDMetadata(md);
           event.setFeatureMetadata(ftMetadata);
           */ 
         
        // JAXBElement<NOTAMPropertyType> NOTAMPropertyType
      //    {@link JAXBElement }{@code <}{@link TextDesignatorType }{@code >}
     //* {@link JAXBElement }{@code <}{@link NotePropertyType }{@code >}
     //* {@link JAXBElement }{@code <}{@link DateTimeType }{@code >}
     //* {@link JAXBElement }{@code <}{@link NOTAMPropertyType }{@code >}
     //* {@link JAXBElement }{@code <}{@link EventPropertyType }{@code >}
     //* {@link JAXBElement }{@code <}{@link EventTimeSliceType.Extension }{@code >}
     //* {@link JAXBElement }{@code <}{@link TextNameType }{@code >}
     //* {@link JAXBElement }{@code <}{@link AISPublicationPropertyType }{@code >}
    // * {@link JAXBElement }{@code <}{@link CodeEventEncodingType }{@code >}
          
          
          // NotePropertyType property = new NotePropertyType();
          // AISPublicationPropertyType publication = new AISPublicationPropertyType();
         //  AISPublicationType pub = new AISPublicationType();
         //  pub.setSeries(null);
         //  publication.
         //  publication.setAISPublication(publication);
           //----------------------------------event: properties ------------------------------
           //event:name
           TextNameType name = new TextNameType();
           name.setValue("TEMPORARY RESTRICTED AREA NORTH OF SJAELLANDS ODDE");
           //add name to time slice          
           eventTimeSlice.getRest().add(wrap("event:name", name));
                      
           //event:encoding
           CodeEventEncodingType encoding = new CodeEventEncodingType();
           encoding.setValue("DIGITAL");   //DIGITAL< MIEXED< ANNOTATON EventDataTypes          
           //add encoding to time slice
           eventTimeSlice.getRest().add(wrap("event:encoding", encoding));
           
           //event:scenario
           TextDesignatorType scenario = new TextDesignatorType();
           scenario.setValue("SAA.NEW.1.0");              
           //add scenario to time slice
           eventTimeSlice.getRest().add(wrap("event:scenario", scenario));
                      
           //event:revision
           DateTimeType revision = new DateTimeType();
           XMLGregorianCalendar dateG = asXMLGregorianCalendar(new Date());           
           revision.setValue(dateG);   //DIGITAL< MIEXED< ANNOTATON EventDataTypes          
           //add revision to time slice
           eventTimeSlice.getRest().add(wrap("event:revision", revision));
          
           //----------------------------------event: properties ------------------------------
           
                      //----------------------------------event: properties ------------------------------

           NOTAMType notamBody = new NOTAMType();
           TextNOTAMType textNotam = new TextNOTAMType();
           textNotam.setValue("AD ENSO CLOSED FOR ALL TFC");
           notamBody.setText(wrap("event:text", textNotam));
           
           CodeUpperAlphaType series = new CodeUpperAlphaType();
           series.setValue("M");           
           notamBody.setSeries(wrap("event:series", series));
           
           NOTAMPropertyType notam = new NOTAMPropertyType();
           notam.setNOTAM(notamBody);
           
           //add notam to time slice
           eventTimeSlice.getRest().add(wrap("event:textNOTAM", notam));      
           
           //add eventTimeSlice to event
           timeSlice.setEventTimeSlice(eventTimeSlice);
           //add timeSlide to Event
           event.getTimeSlice().add(timeSlice);
         
           messageMember.setAbstractAIXMFeature(wrap("event:Event", event));
         
           //add it to the list
           aixmBasicMessage.getHasMember().add(messageMember);
        
         
         
       
         System.out.println("Starting ..."); 
         JAXBContext jaxbCtx = null;
        StringWriter xmlWriter = new StringWriter();
       ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            
           
            //XML Binding code using JAXB
           
            
         /*   jaxbCtx = JAXBContext.newInstance(AIXMBasicMessageType.class);
            Marshaller marshaller = jaxbCtx.createMarshaller();
                        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);  
            marshaller.marshal(new JAXBElement<AIXMBasicMessageType>(new QName("uri","message:AIXMBasicMessage"), AIXMBasicMessageType.class, aixmBasicMessage), xmlWriter);
            System.out.println(xmlWriter.toString());
            //File file = new File("C:\\Test\\test.xml");
            //save string to file
         */  
            jaxbCtx = JAXBContext.newInstance(AIXMBasicMessageType.class);
            AIXMUnmarshallerMarshaller aixmmarshaller = new AIXMUnmarshallerMarshaller();
            //remove the setProperty
            JAXBElement<?> g = new JAXBElement<AIXMBasicMessageType>(new QName("uri","message:AIXMBasicMessage"), AIXMBasicMessageType.class, aixmBasicMessage);
            os = aixmmarshaller.marshal(g);
        
            //os = aixmmarshaller.marshal(new JAXBElement<AIXMBasicMessageType>(new QName("uri","message:AIXMBasicMessage"), AIXMBasicMessageType.class, aixmBasicMessage));
            System.out.println(os.toString());
            
           
          /*  AIXMUnmarshallerMarshaller aixmMarchaller = new AIXMUnmarshallerMarshaller();
            
            JAXBElement je =  new JAXBElement<AIXMBasicMessageType>( new QName("uri","message:AIXMBasicMessage"),AIXMBasicMessageType.class, aixmBasicMessage);
            //JAXBElement<?> je, OutputStream os
            aixmMarchaller.marshal(je, os);
            
            String aString = new String(os.toByteArray(),"UTF-8");
            System.out.println(aString);
           this.output = new GenericFileData(new ByteArrayInputStream(os.toByteArray()), "application/xml");
            */
             
           // marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
           // marshaller.marshal(new JAXBElement<AIXMBasicMessageType>(new QName("uri","message:AIXMBasicMessage"), AIXMBasicMessageType.class, aixmBasicMessage), xmlWriter);
           
            //marshaller.marshal(aixmBasicMessage, xmlWriter);
            //System.out.println("XML Marshal example in Java");            
            //System.out.println(xmlWriter.toString());

           
          
           // BasicMessageMemberAIXMPropertyType b = (BasicMessageMemberAIXMPropertyType) jaxbCtx.createUnmarshaller().unmarshal(
            //                                   new StringReader(xmlWriter.toString()));
            
            
           //JAXBElement<BasicMessageMemberAIXMPropertyType> userElement = unmarshaller.unmarshall(someSource, BasicMessageMemberAIXMPropertyType.class);
           // UserType user = userElement.getValue();
            
       
           
        } catch (JAXBException ex) {
            //Logger.getLogger(JAXBXmlBindExample.class.getName()).log(Level.SEVERE, null, ex);
             System.out.println(ex.toString());
            
        }
      
        
        
    /*    BasicMessageMemberAIXMPropertyType messageMember = new BasicMessageMemberAIXMPropertyType();
        //messageMember.setAbstractAIXMFeature(new NOTAMType());
        aixmBasicMessage.getHasMember().add(messageMember);
        
        Element root = DocumentHelper.createElement(aixmBasicMessage.toString());
        document.setRootElement(root);        
        
        //AixmFeature runway = createTestRunway();   
        NOTAMType notam = new NOTAMType();
        //notam.setNumber();
        
        QName qname = QName.get("Notam", AixmConstants.AIXM_NAMESPACE);
        Element rootElement = DocumentHelper.createElement(qname);
        rootElement.add(AixmConstants.AIXM_NAMESPACE);
        rootElement.add(AixmConstants.GML_NAMESPACE);
        rootElement.add(AixmConstants.XLINK_NAMESPACE);
   */   
        // need to set the root element of the document first so that
        // XPath can search the document.
        //document.setRootElement(rootElement);

        // Add the Elements that make up this object
       // rootElement.
    //    Element element = aixmBasicMessage.addElements(rootElement, document);
    //    Namespace ns = new Namespace("gml", "http://www.opengis.net/gml");
    //    element.add(ns);
        
    //    ns = new Namespace("aixm", "http://www.aixm.aero");
    //    element.add(ns);
    //    ns = new Namespace("xlink", "http://www.w3.org/1999/xlink");
    //    element.add(ns);
    // 
        // Prepares output format
        //document.setRootElement((Element)aixmBasicMessage);

        // pretty print the document
    /*    OutputFormat formatter = OutputFormat.createPrettyPrint();

        try {
            XMLWriter writer =
                new XMLWriter(System.out, formatter);
            writer.write(document);
            writer.flush();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        System.out.println();
    */    
        // convert the Element back to an AixmFeature object
       // AixmFeature rwy = new AixmFeature(AixmFeatureType.Runway).parseElements(element);
        //System.out.println(rwy);
    }
    /**
     * @param args the command line arguments
     */
  /*  public static void main(String[] args) {
       // create document
        Document document = DocumentHelper.createDocument();
        AIXMBasicMessageType aixmBasicMessage = new ObjectFactory().createAIXMBasicMessageType();
        
        
        
        
        BasicMessageMemberAIXMPropertyType messageMember = new BasicMessageMemberAIXMPropertyType();
        //messageMember.setAbstractAIXMFeature(new NOTAMType());
        aixmBasicMessage.getHasMember().add(messageMember);
        
        Element root = DocumentHelper.createElement(aixmBasicMessage.toString());
        document.setRootElement(root);        
        
        //AixmFeature runway = createTestRunway();   
        NOTAMType notam = new NOTAMType();
        //notam.setNumber();
        
        QName qname = QName.get("Notam", AixmConstants.AIXM_NAMESPACE);
        Element rootElement = DocumentHelper.createElement(qname);
        rootElement.add(AixmConstants.AIXM_NAMESPACE);
        rootElement.add(AixmConstants.GML_NAMESPACE);
        rootElement.add(AixmConstants.XLINK_NAMESPACE);
      
        // need to set the root element of the document first so that
        // XPath can search the document.
        //document.setRootElement(rootElement);

        // Add the Elements that make up this object
       // rootElement.
    //    Element element = aixmBasicMessage.addElements(rootElement, document);
    //    Namespace ns = new Namespace("gml", "http://www.opengis.net/gml");
    //    element.add(ns);
        
    //    ns = new Namespace("aixm", "http://www.aixm.aero");
    //    element.add(ns);
    //    ns = new Namespace("xlink", "http://www.w3.org/1999/xlink");
    //    element.add(ns);
    // 
        // Prepares output format
        //document.setRootElement((Element)aixmBasicMessage);

        // pretty print the document
        OutputFormat formatter = OutputFormat.createPrettyPrint();

        try {
            XMLWriter writer =
                new XMLWriter(System.out, formatter);
            writer.write(document);
            writer.flush();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        System.out.println();
        
        // convert the Element back to an AixmFeature object
       // AixmFeature rwy = new AixmFeature(AixmFeatureType.Runway).parseElements(element);
        //System.out.println(rwy);
    }
 */   
  /*  public static AixmFeature createNOTAM() {
        AixmFeature runway = new AixmFeature(AixmFeatureType.);
        AixmRunwayTimeSlice runwayTimeSlice = new AixmRunwayTimeSlice();
        
        runwayTimeSlice.setDesignator("KIAD RWY 36L/18R");
        runwayTimeSlice.setLength(new AixmHorizontalDistance(5000.d, AixmUomDistanceType.FT));
        runwayTimeSlice.setWidth(new AixmHorizontalDistance(150.d, AixmUomDistanceType.FT));
        runwayTimeSlice.setWidthShoulder(new AixmHorizontalDistance(20.d, AixmUomDistanceType.FT));
        runwayTimeSlice.setLengthStrip(new AixmHorizontalDistance(5000.d, AixmUomDistanceType.FT));
        runwayTimeSlice.setWidthStrip(new AixmHorizontalDistance(150.d, AixmUomDistanceType.FT));
        runway.addTimeSlice(runwayTimeSlice);                

        return runway;
    }
    */
     
    public static XMLGregorianCalendar asXMLGregorianCalendar(Date date) {
    java.util.GregorianCalendar calDate = new java.util.GregorianCalendar();        
    calDate.setTime(date);
    javax.xml.datatype.XMLGregorianCalendar calendar = null;
        try {
            javax.xml.datatype.DatatypeFactory factory = javax.xml.datatype.DatatypeFactory.newInstance();
            calendar = factory.newXMLGregorianCalendar(
            calDate.get(java.util.GregorianCalendar.YEAR),
            calDate.get(java.util.GregorianCalendar.MONTH) + 1,
            calDate.get(java.util.GregorianCalendar.DAY_OF_MONTH),
            calDate.get(java.util.GregorianCalendar.HOUR_OF_DAY),
            calDate.get(java.util.GregorianCalendar.MINUTE),
            calDate.get(java.util.GregorianCalendar.SECOND),
            calDate.get(java.util.GregorianCalendar.MILLISECOND), 0);
        } catch (DatatypeConfigurationException dce) {
            //handle or throw
            dce.printStackTrace();
        }   
    return calendar;
}
    
   
    public static <T> JAXBElement<T> wrap(String qName, final T object) {        
        return new JAXBElement<T>(new QName("uri",qName), (Class<T>) object.getClass(), object);
    }
    public static <T> JAXBElement<T> wrap2(final QName qName, final T object) {
        return new JAXBElement<T>(qName, (Class<T>) object.getClass(), object);
    }
    
  
    
}
