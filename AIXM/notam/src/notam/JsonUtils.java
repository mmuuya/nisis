/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notam;

import com.google.gson.Gson;

/**
 *
 * @author irena.bucci
 */

public final class JsonUtils {  
      private JsonUtils() {  
      }  
      public static <T> T getObject(final String jsonString, final Class<T> objectClass) {  
           Gson gson = new Gson();  
           return gson.fromJson(jsonString, objectClass);  
      }  
      public static String getString(final Object object) {  
           Gson gson = new Gson();  
           return gson.toJson(object);  
      }  
 }  