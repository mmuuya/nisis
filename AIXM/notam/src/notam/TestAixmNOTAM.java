/**
 * Copyright (C) 2008 The MITRE Corporation.
 * 
 * This file is part of aixm-j.
 *
 * aixm-j is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * aixm-j is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with aixm-j.  If not, see <http://www.gnu.org/licenses/>.
 */
package notam;

import aero.aixm.v510.message.AIXMBasicMessageType;
import aero.aixm.v510.message.BasicMessageMemberAIXMPropertyType;
import aero.aixm.v510.message.ObjectFactory;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.QName;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;


/**
 * Example of creating an AixmFeature containing an AixmNavaidTimeSlice, 
 * representing it in a DOM, and then generating AIXM XML. The DOM is then parsed 
 * back into an AixmFeature.
 * 
 * @author SCHASE
 *
 */
public class TestAixmNOTAM {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        // create document
        Document document = DocumentHelper.createDocument();
        
        //AIXMBasicMessageType notam = createTestNOTAM();
        
        QName qname = QName.get("Navaid", AixmConstants.AIXM_NAMESPACE);
        Element rootElement = DocumentHelper.createElement(qname);
        rootElement.add(AixmConstants.AIXM_NAMESPACE);
        rootElement.add(AixmConstants.GML_NAMESPACE);
        rootElement.add(AixmConstants.XLINK_NAMESPACE);
        
        // need to set the root element of the document first so that
        // XPath can search the document.
        document.setRootElement(rootElement);
        
        // Add the Elements that make up this object
        //Element element = navaid.addElements(rootElement, document);

        // pretty print the document
        OutputFormat formatter = OutputFormat.createPrettyPrint();

        try {
            XMLWriter writer =
                new XMLWriter(System.out, formatter);
            writer.write(document);
            writer.flush();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        System.out.println();
        
        // convert the Element back to an AixmFeature object
       // AixmFeature nav = new AixmFeature(AixmFeatureType.Navaid).parseElements(element);
        //System.out.println(nav);
	}

    /**
     * Create an AixmFeature containing an AixmNavaidTimeSlice
     * @return AixmFeature
     */
   /* public static AixmFeature createTestNavaid() {
        AixmFeature navaid = new AixmFeature(AixmFeatureType.Navaid);
        AixmNavaidTimeSlice timeSlice = new AixmNavaidTimeSlice();
        timeSlice.setDesignator("KIAD");
        timeSlice.setFlightChecked(AixmCodeYesNoType.YES);
        timeSlice.setLandingCategory(AixmCodeLandingAidCategoryType.II);
        AixmElevatedPoint location = TestAixmElevatedPoint.createTestElevatedPoint();
        timeSlice.setLocation(location);
        timeSlice.setOperationalStatus(AixmCodeStatusNavaidType.OPERATIONAL);
        AixmFeature runwayDirection = TestAixmRunwayDirection.createTestRunwayDirection();
        timeSlice.setRunwayDirection(runwayDirection);
        timeSlice.setType(AixmCodeNavaidServiceType.ILS);
        navaid.addTimeSlice(timeSlice);

        return navaid;
    }*/
        
         /**
     * Create an AixmNOTAM message containing an AixmNavaidTimeSlice
     * @return AixmFeature
     */
  /*  public static AIXMBasicMessageType createTestNOTAM() {
         AIXMBasicMessageType aixmBasicMessage = new ObjectFactory().createAIXMBasicMessageType();
         BasicMessageMemberAIXMPropertyType messageMember = new BasicMessageMemberAIXMPropertyType(); 
        
        AixmFeature navaid = new AixmFeature(AixmFeatureType.Navaid);
        AixmNavaidTimeSlice timeSlice = new AixmNavaidTimeSlice();
        timeSlice.setDesignator("KIAD");
        timeSlice.setFlightChecked(AixmCodeYesNoType.YES);
        timeSlice.setLandingCategory(AixmCodeLandingAidCategoryType.II);
        AixmElevatedPoint location = TestAixmElevatedPoint.createTestElevatedPoint();
        timeSlice.setLocation(location);
        timeSlice.setOperationalStatus(AixmCodeStatusNavaidType.OPERATIONAL);
        AixmFeature runwayDirection = TestAixmRunwayDirection.createTestRunwayDirection();
        timeSlice.setRunwayDirection(runwayDirection);
        timeSlice.setType(AixmCodeNavaidServiceType.ILS);
        navaid.addTimeSlice(timeSlice);

        return navaid;
    }
        */
}
