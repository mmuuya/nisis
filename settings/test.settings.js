//
// test javascript settings
//
//var apphost = "localhost"; //address of app server
var nisisProtocol = 'https';

var apphost = window.location.host; // better for localhost with non default ports
var agsHost = '192.168.19.211'; //address of arcgis server
//var agsHost = 'arcgissvr.ndpreston.net';
// var agsPort = nisisProtocol === 'https' ? '6443' : '6080';  //arcgis content is secured, only https will work

var agsServer = 'https://' + agsHost + ':6443/arcgis/';


var wsServer = nisisProtocol + '://gisweb.wdtinc.com/arcgis/';

// if (document.location.nisisProtocol != 'https:') {
// 	//agsServer = 'http://' + agsHost + ':6080/arcgis/';
// }

var geoServer = document.location.protocol + '//192.168.19.193:8080/';

var dojoConfig = { //dojo AMD config
  paths: {
    'app': '../../../../../../../../../../scripts/app',
    'nisis': '../../../../../../../../../../scripts',
    'libs': '../../../../../../../../../../deps',
  },
};
