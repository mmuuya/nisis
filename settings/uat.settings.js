//
// uat javascript settings
//

//var apphost = "localhost"; //address of app server
var nisisProtocol = 'https';

var apphost = window.location.host; // better for localhost with non default ports
var agsHost = 'ndpuat.faa.gov'; //address of arcgis server
var agsServer = 'https://' + agsHost + '/arcgis/';


var wsServer = nisisProtocol + '://gisweb.wdtinc.com/arcgis/';

// if (document.location.nisisProtocol != 'https:') {
// 	//agsServer = 'http://' + agsHost + ':6080/arcgis/';
// }
//var geoServer = document.location.protocol + "//192.168.19.193:8080/";
var geoServer = "https://ndpuat.faa.gov/geoserver";

var dojoConfig = { //dojo AMD config

    paths: {
        "app": "../../../../../../../../../../scripts/app",
        "nisis": "../../../../../../../../../../scripts",
        "libs": "../../../../../../../../../../deps"
    }
};
