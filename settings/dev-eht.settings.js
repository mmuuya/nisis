// These javascript settings are hooked to overwrite settings.js on EHT dev
// Do not alter this for your local environment
// This is only altered if a change has been made to the EHT dev that requires update

// all environments use https to avoid security blocks, configure your dev to allow https
const nisisProtocol = 'https'

// this is the host name for the application server
const apphost = 'nisis.ndp.net'

// address of arcgis server, since we proxy with nginx the name here is the same as hostname
const agsHost = 'nisis.ndp.net'
const agsServer = `https://${agsHost}/arcgis/`

// This is the URL for WDT arcgis token service
const wsServer = `${nisisProtocol}://gisweb.wdtinc.com/arcgis/`

// webadapt 2 geoserver for tracks. This probably doens't work anymore,
// and we will be tossing webadapt 2 soon
const geoServer = `${document.location.protocol}//192.168.19.193:8080/`

// dojo AMD config
var dojoConfig = {
  paths: {
    app: '../../../../../../../../../../scripts/app',
    nisis: '../../../../../../../../../../scripts',
    libs: '../../../../../../../../../../deps',
  },
}
