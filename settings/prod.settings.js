//
// production javascript settings
//

//var apphost = "localhost"; //address of app server
var nisisProtocol = 'https';

var apphost = window.location.host; 
var agsHost = 'nisis.faa.gov'; //address of arcgis server
var agsServer = 'https://' + agsHost + '/arcgis/';

var wsServer = nisisProtocol + '://gisweb.wdtinc.com/arcgis/';

// if (document.location.nisisProtocol != 'https:') {
// 	//agsServer = 'http://' + agsHost + ':6080/arcgis/';
// }

//settings for rollback
//var agsHost = "192.168.101.40";
//var agsServer = "https://webadapt.faa.gov/arcgis/";
//var geoServer = "https://webadapt.faa.gov/geoserver";
//split database settings
// var agsHost = "192.168.101.45";
// var agsServer = "https://nisis.faa.gov/arcgis/";
var geoServer = "https://nisis.faa.gov/geoserver";
// var wsServer = "https://gisweb.wdtinc.com/arcgis/";

var dojoConfig = {
    paths: {
        "app": "../../../../../../../../../../scripts/app",
        "nisis": "../../../../../../../../../../scripts",
	"libs": "../../../../../../../../../../deps"
    }
};
