This is a cache file that is used for caching rarely changing database data. If any of the data from the queries specified below changes, this file needs to be deleted.

The data cached so far:

1. SELECT value,label,path FROM DYNAMIC_SYMBOLS ORDER BY value (phpSymbols.php)
2. SELECT ID, NAME, ABBR, LAT, LON FROM NISIS.LOOKUP_STATES
3. SELECT TZID AS ID, TZ_NAME AS NAME, UTC_OFFSET AS OFFSET FROM TFRUSER.TFR_TIMEZONES
4. select id, REF || ' ' || name  || case when details is null THEN  '' else ' (' || details || ')' end as type, ref, name, details, description, template from tfruser.lookup_tfr_types order by id asc
5. select id, artcc_id as artcc, artcc_name as name, lat, lon from nisis.lookup_artccs
6. select typeid as id, type_name as name from tfruser.factypes order by type_name asc